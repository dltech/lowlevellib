/*
 * running lights effect for ws2811.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "ws2812.h"
#include "avr_rand.h"
#include "runLights.h"

void black(wsRgbTyp *c);
void iridescentParam(wsRgbTyp *c, int *aimColour);
void iridescent(wsRgbTyp *c);
void brightness(wsRgbTyp *c, uint8_t bright);


wsRgbTyp line[LINE_SIZE];

void lightsInit()
{
    for(int i=0 ; i<LINE_SIZE ; ++i) {
        black(&line[i]);
    }
    line[8].type = WS2811;
    wsInit();
    wsUpdateCascade(line,LINE_SIZE);
    srand(200);
}

void iridescentParam(wsRgbTyp *c, int *aimColour)
{
    switch(*aimColour)
    {
        case BLACK:
            black(c);
            *aimColour = BLACK_TO_RED;
            break;
        case BLACK_TO_RED:
            if( (c->red) < 255 ) {
                c->red = c->red + 1;
            } else {
                *aimColour = RED_TO_MAGENTA;
            }
            break;
        case RED_TO_MAGENTA:
            if( (c->blue) < 255 ) {
                c->blue = c->blue + 1;
            } else {
                *aimColour = MAGENTA_TO_BLUE;
            }
            break;
        case MAGENTA_TO_BLUE:
            if( (c->red) > 0 ) {
                c->red = c->red - 1;
            } else {
                *aimColour = BLUE_TO_CYAN;
            }
            break;
        case BLUE_TO_CYAN:
            if( (c->green) < 255 ) {
                c->green = c->green + 1;
            } else {
                *aimColour = CYAN_TO_GREEN;
            }
            break;
        case CYAN_TO_GREEN:
            if( (c->blue) > 0 ) {
                c->blue = c->blue - 1;
            } else {
                *aimColour = GREEN_TO_YELLOW;
            }
            break;
        case GREEN_TO_YELLOW:
            if( (c->red) < 255 ) {
                c->red = c->red + 1;
            } else {
                *aimColour = YELLOW_TO_RED;
            }
            break;
        case YELLOW_TO_RED:
            if( (c->green) > 0 ) {
                c->green = c->green - 1;
            } else {
                *aimColour = RED_TO_MAGENTA;
            }
            break;
    }
}

void iridescent(wsRgbTyp *c)
{
    static int aimColour = 0;
    iridescentParam(c,&aimColour);
}

void brightness(wsRgbTyp *c, uint8_t bright)
{
    c->red = (uint8_t)(((uint32_t)c->red)*((uint32_t)bright)/((uint32_t)255));
    c->green = (uint8_t)(((uint32_t)c->green)*((uint32_t)bright)/((uint32_t)255));
    c->blue = (uint8_t)(((uint32_t)c->blue)*((uint32_t)bright)/((uint32_t)255));
}

void black(wsRgbTyp *c)
{
    c->red = 0;
    c->green = 0;
    c->blue = 0;
}

void effect1()
{
    static int pos = 0;
    static int dir = 0;
    static int stayCnt = 0;
    static wsRgbTyp colour;
    static wsRgbTyp prevColour;
    if(stayCnt < 10) {
        iridescent(&colour);
        ++stayCnt;
        return;
    }
    stayCnt = 0;
    if( dir == 0 ) {
        if(pos == 0) {
            black(&line[1]);
            black(&line[2]);
            black(&line[3]);
        }
        wsCopy(&line[pos], &colour);

        if(pos>=1) {
            wsCopy(&line[pos-1], &prevColour);
            brightness(&line[pos-1], 32);
        }
        if(pos>=2) {
            wsCopy(&line[pos-2], &prevColour);
            brightness(&line[pos-2], 4);
        }
        if(pos>=3) {
            black(&line[pos-3]);
        }
        if(pos<(LINE_SIZE-1)) {
            ++pos;
        } else {
            dir = 1;
        }
    } else {
        if(pos == (LINE_SIZE-1)) {
            black(&line[7]);
            black(&line[6]);
            black(&line[5]);
        }
        wsCopy(&line[pos], &colour);

        if(pos<(LINE_SIZE-1)) {
            wsCopy(&line[pos+1], &prevColour);
            brightness(&line[pos+1], 32);
        }
        if(pos<(LINE_SIZE-2)) {
            wsCopy(&line[pos+2], &prevColour);
            brightness(&line[pos+2], 4);
        }
        if(pos<(LINE_SIZE-3)) {
            black(&line[pos+3]);
        }
        if(pos>0) {
            --pos;
        } else {
            dir = 0;
        }
    }
    prevColour = colour;
    wsUpdateCascade(line,LINE_SIZE);
}

void effect2()
{
    static int stayCnt = 0;
    if(stayCnt < 4) {
        ++stayCnt;
        return;
    }
    stayCnt = 0;
    for(int i=0 ; i<LINE_SIZE ; ++i)
    {
        line[i].red = (rand()*255)/RAND_MAX;
        line[i].green = (rand()*8)/RAND_MAX;
        line[i].blue = 0;
        if( line[i].red > 250 ) {
            line[i].green = line[i].red;
            line[i].blue = line[i].red;
            line[i].red = 255;
        }
        if( (line[i].red/4) < line[i].green ) {
            line[i].green /= 4;
        }
    }
    wsUpdateCascade(line,LINE_SIZE);
}

void effect3()
{
    static int irp[LINE_SIZE] = {0,0,0,0,0,0,0,0,0};
    static int isFirst = 1;
    if(isFirst) {
        line[0].red = 0;
        line[0].green = 0;
        line[0].blue = 0;
        irp[0] = BLACK_TO_RED;
        line[1].red = 255;
        line[1].green = 0;
        line[1].blue = 0;
        irp[1] = RED_TO_MAGENTA;
        line[2].red = 255;
        line[2].green = 0;
        line[2].blue = 255;
        irp[2] = MAGENTA_TO_BLUE;
        line[3].red = 127;
        line[3].green = 0;
        line[3].blue = 255;
        irp[3] = MAGENTA_TO_BLUE;
        line[4].red = 0;
        line[4].green = 0;
        line[4].blue = 255;
        irp[4] = BLUE_TO_CYAN;
        line[5].red = 0;
        line[5].green = 255;
        line[5].blue = 255;
        irp[5] = CYAN_TO_GREEN;
        line[6].red = 0;
        line[6].green = 255;
        line[6].blue = 0;
        irp[6] = GREEN_TO_YELLOW;
        line[7].red = 255;
        line[7].green = 255;
        line[7].blue = 0;
        irp[7] = YELLOW_TO_RED;
        line[8].red = 255;
        line[8].green = 127;
        line[8].blue = 0;
        irp[8] = YELLOW_TO_RED;
        isFirst = 0;
    }
    for(int i=0 ; i<LINE_SIZE ; ++i) {
        iridescentParam(&(line[i]), &(irp[i]));
    }
    wsUpdateCascade(line,LINE_SIZE);
}
