/*
 * classic c random, obtained from avr-libc https://www.nongnu.org/avr-libc/
*/

#define RAND_MAX 0x7fff

int rand(void);
void srand(unsigned int seed);
