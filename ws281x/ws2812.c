/*
 * Part of chuika - cheapest tourists radio.
 * Here is ws2812 rgb led access functions.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "../stm32/delay.h"
#include "../stm32/regs/rcc_regs.h"
#include "../stm32/regs/tim_regs.h"
#include "ws2812.h"

// local functions
void setLine(void);
void resetLine(void);
void sendBit(uint8_t byte, int n);
//void sendByte(uint8_t byte);

void wsInit()
{
    RCC_APB2ENR |= IOPAEN;
    gpioSetPushPull(WS_PORT, WS_PIN);
    timDelayInit();
    resetLine();
}

void setLine()
{
    gpioSet(WS_PORT, WS_PIN);
}

void resetLine(void)
{
    gpioReset(WS_PORT, WS_PIN);
}


void sendBit(uint8_t byte, int n)
{
    if((byte & (1 << n)) == 0) {
        GPIOx_BSRR(WS_PORT) |= WS_PIN;
        //        setLine();
        timDelayNs(T0H);
        GPIOx_BRR(WS_PORT) |= WS_PIN;
//        resetLine();
        timDelayNs(T0L);
    } else {
        GPIOx_BSRR(WS_PORT) |= WS_PIN;
//        setLine();
        timDelayNs(T1H);
        GPIOx_BRR(WS_PORT) |= WS_PIN;
//        resetLine();
        timDelayNs(T1L);
    }
}

// void sendByte(uint8_t byte)
// {
//     // MSB first
//     for(int i=0 ; i<8 ; ++i) {
//         GPIOx_BSRR(WS_PORT) |= WS_PIN;
//         if((rgb->trans & (1 << (7-i))) == 0) {
//             th=T0H_TACTS;
//         } else {
//             th=T1H_TACTS;
//         }
//         while(TIM2_CNT < th);
//         GPIOx_BRR(WS_PORT) |= WS_PIN;
//         while(TIM2_CNT < TBIT_TACTS);
//         TIM2_EGR |= UG;
//         //sendBit(byte,i);
//     }
// }

// global functions
void wsResetLine()
{
    resetLine();
    timDelayUs(RES_US);
}

// void wsUpdateNoob(uint8_t red, uint8_t green, uint8_t blue)
// {
//     sendByte(green);
//     sendByte(red);
//     sendByte(blue);
// }

void wsUpdate(wsRgbTyp *rgb)
{
    uint32_t th;
    TIM2_EGR |= UG;
    for(int i=0 ; i<24 ; ++i) {
        GPIOx_BSRR(WS_PORT) |= WS_PIN;
        if((rgb->trans & (1 << (23-i))) == 0) {
            th=T0H_TACTS;
        } else {
            th=T1H_TACTS;
        }
        while(TIM2_CNT < th);
        GPIOx_BRR(WS_PORT) |= WS_PIN;
        while(TIM2_CNT < TBIT_TACTS);
        TIM2_EGR |= UG;
    }
}

void ws12to11(wsRgbTyp *rgb)
{
    uint8_t temp = rgb->red;
    rgb->red = rgb->green;
    rgb->green = temp;
}

void wsCopy(wsRgbTyp *in, wsRgbTyp *out)
{
    in->red = out->red;
    in->green = out->green;
    in->blue = out->blue;
}

void wsUpdateCascade(wsRgbTyp *rgb, int size)
{
    for(int i=0 ; i<size ; ++i) {
        if(rgb[i].type == WS2811) {
            ws12to11(&rgb[i]);
        }
    }
    wsResetLine();
    for(int i=0 ; i<size ; ++i) {
        wsUpdate(&rgb[i]);
    }
    wsResetLine();
    for(int i=0 ; i<size ; ++i) {
        if(rgb[i].type == WS2811) {
            ws12to11(&rgb[i]);
        }
    }
}
