/*
 * running lights effect for ws2811.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "../stm32/gpio.h"
#include "../stm32/delay.h"
#include "runLights.h"
#include "ws2812.h"
#include "usbcom/usbcom.h"

extern uint8_t vcpRxBuffer[64];
extern wsRgbTyp line[LINE_SIZE];

int main(void) {
    int prevRxCount = 0;
    int j = 0;
    usbComInit();
    gpioSetPullUp(GPIOA, GPIO1 | GPIO2 | GPIO3);

    lightsInit();

    while(1) {
       if(gpioIsActive(GPIOA, GPIO1) == 0) {
           effect1();
       }
        if(gpioIsActive(GPIOA, GPIO2) == 0) {
            effect2();
        }
        if(gpioIsActive(GPIOA, GPIO3) == 0) {
            effect3();
        }
        if( prevRxCount < getUsbComRxCount() ) {
            j=0;
            for(int i=0 ; i<LINE_SIZE ; ++i) {
                line[i].red = vcpRxBuffer[j++];
                line[i].green = vcpRxBuffer[j++];
                line[i].blue = vcpRxBuffer[j++];
                j++;
            }
            wsUpdateCascade(line,LINE_SIZE);
            prevRxCount = getUsbComRxCount();
        }
        delay_ms(10);
    }
}
