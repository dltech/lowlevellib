#ifndef H_RUNLIGHTS
#define H_RUNLIGHTS
/*
 * Part of chuika - cheapest tourists radio.
 * running lights effect.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LINE_SIZE   9
// timing of the effect in ms
#define EFFECT1_TIMING   16

enum{
    BLACK = 0,
    BLACK_TO_RED,
    RED_TO_MAGENTA,
    MAGENTA_TO_BLUE,
    BLUE_TO_CYAN,
    CYAN_TO_GREEN,
    GREEN_TO_YELLOW,
    YELLOW_TO_RED
};

void lightsInit(void);

void effect1(void);
void effect2(void);
void effect3(void);


#endif
