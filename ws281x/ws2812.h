#ifndef H_WS2812
#define H_WS2812
/*
 * Part of chuika - cheapest tourists radio.
 * Here is ws2812 1-wire rgb led access functions.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "../stm32/gpio.h"

// mcu-related settings
#define WS_PORT GPIOA
#define WS_PIN  GPIO8

enum{
    WS2812,
    WS2811
};

typedef union {
    struct {
        uint8_t blue;
        uint8_t red;
        uint8_t green;
        uint8_t type;
    };
    uint32_t trans;
}   wsRgbTyp;

// timings in ns
#define T0H     270
#define T1H     700
#define T0L     T1H
#define T1L     T0H
#define TBIT    900
#define RES     300000
#define RES_US  RES/1000
#define T0H_TACTS   T0H/NS_PER_TACT
#define T1H_TACTS   T1H/NS_PER_TACT
#define T0L_TACTS   T0L/NS_PER_TACT
#define T1L_TACTS   T1L/NS_PER_TACT
#define TBIT_TACTS  TBIT/NS_PER_TACT


void wsInit(void);
void wsResetLine(void);
void wsUpdateNoob(uint8_t red, uint8_t green, uint8_t blue);
void wsUpdate(wsRgbTyp *rgb);
void wsUpdateCascade(wsRgbTyp *rgb, int size);
void ws12to11(wsRgbTyp *rgb);
void wsCopy(wsRgbTyp *in, wsRgbTyp *out);

#endif
