#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QColor>
#include <QLabel>
#include <QWidget>
#include <QBoxLayout>
#include <QSlider>
#include <QSpinBox>
#include <QToolBar>
#include <QPushButton>
#include <QComboBox>
#include <QSerialPort>
#include <QStatusBar>
#include <QTimer>
#include <QCheckBox>

#include "rgbLed.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
public slots:
    void rChange(void);
    void gChange(void);
    void bChange(void);
    void rtChange(void);
    void gtChange(void);
    void btChange(void);
    void checked(int n);
    void update(void);
    void continuous(void);
    void allSlot(void);
    void reconnect(void);
    void updateList(void);
    void preset(void);

private:
    void copyToAll(void);

    QSerialPort *ledPort;
    QTimer *portUpdTim;
    QTimer *ledUpdTim;

    bool change;
    bool all;

    QStatusBar *stBar;
    QToolBar *toolBar;

    QPushButton *send;
    QCheckBox *cont;

    QComboBox *port;
    QPushButton *recon;

    QComboBox *colourCode;
    QCheckBox *allCheck;

    QWidget *mainMenu;
    QWidget *colour;
    QWidget *ledLine;

    QBoxLayout *mainMenuLay;    
    QBoxLayout *colourLay;
    QBoxLayout *rLay;
    QBoxLayout *gLay;
    QBoxLayout *bLay;
    QBoxLayout *lineLay;

    QSlider *r;
    QSlider *g;
    QSlider *b;

    QSpinBox *rt;
    QSpinBox *gt;
    QSpinBox *bt;

    QLabel *rl;
    QLabel *gl;
    QLabel *bl;

    static const int lineSize = 9;
    QColor *led[lineSize];
    RgbLed *ledLb[lineSize];
    int activeLed = 0;

    static const int coloursNumber = 18;
    typedef struct{
        QString *name;
        uint8_t r;
        uint8_t g;
        uint8_t b;
    } colourNameTyp;
    colourNameTyp colours[coloursNumber];
};
#endif // MAINWINDOW_H
