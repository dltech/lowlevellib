#ifndef RGBLED_H
#define RGBLED_H

#include <QLabel>
#include <QWidget>

class RgbLed : public QLabel
{
    Q_OBJECT
public:
    explicit RgbLed(QWidget* parent = Q_NULLPTR, int num = 0);
    ~RgbLed();

    void reset(void);
    void set(void);

    void setR(int r);
    void setG(int g);
    void setB(int b);
    int getR(void);
    int getG(void);
    int getB(void);
signals:
    void clicked(int num);

protected:
    void mousePressEvent(QMouseEvent* event);

private:
    int number;
    int r=0;
    int g=0;
    int b=0;
    bool active;
};



#endif // RGBLED_H
