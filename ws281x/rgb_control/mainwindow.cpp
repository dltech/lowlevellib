#include "mainwindow.h"
#include <QSerialPortInfo>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    this->setWindowTitle("led line controller");

    stBar = new QStatusBar(this);
    port = new QComboBox(this);
    ledPort = new QSerialPort(0);
    QList<QSerialPortInfo> list;
    list = QSerialPortInfo::availablePorts();
    int n=0;
    for( int i=0 ; i<list.length() ; ++i) {
        port->addItem(list[i].portName());
        if( list[i].portName().contains("ACM") ) {
            ledPort->setPortName(list[i].portName());
            n = i;
        }
    }
    port->setCurrentIndex(n);
    ledPort->setBaudRate(9600,QSerialPort::Output);
    if( ledPort->open(QIODevice::WriteOnly) ) {
        stBar->showMessage(list[port->currentIndex()].portName());
    } else {
        stBar->showMessage("port error");
    }

    colours[0].name = new QString("Red");
    colours[0].r = 255;
    colours[0].g = 0;
    colours[0].b = 0;
    colours[1].name = new QString("Green");
    colours[1].r = 0;
    colours[1].g = 255;
    colours[1].b = 0;
    colours[2].name = new QString("Blue");
    colours[2].r = 0;
    colours[2].g = 0;
    colours[2].b = 255;
    colours[3].name = new QString("Black");
    colours[3].r = 0;
    colours[3].g = 0;
    colours[3].b = 0;
    colours[4].name = new QString("White");
    colours[4].r = 255;
    colours[4].g = 255;
    colours[4].b = 255;
    colours[5].name = new QString("Yellow");
    colours[5].r = 255;
    colours[5].g = 255;
    colours[5].b = 0;
    colours[6].name = new QString("Cyan");
    colours[6].r = 0;
    colours[6].g = 255;
    colours[6].b = 255;
    colours[7].name = new QString("Magenta");
    colours[7].r = 255;
    colours[7].g = 0;
    colours[7].b = 255;
    colours[8].name = new QString("Orange");
    colours[8].r = 255;
    colours[8].g = 140;
    colours[8].b = 0;
    colours[9].name = new QString("Strawberry");
    colours[9].r = 192;
    colours[9].g = 38;
    colours[9].b = 38;
    colours[10].name = new QString("Gold");
    colours[10].r = 255;
    colours[10].g = 171;
    colours[10].b = 0;
    colours[11].name = new QString("Pacific Green");
    colours[11].r = 54;
    colours[11].g = 220;
    colours[11].b = 202;
    colours[12].name = new QString("Metallic Mint");
    colours[12].r = 56;
    colours[12].g = 253;
    colours[12].b = 253;
    colours[13].name = new QString("Neon Blue");
    colours[13].r = 102;
    colours[13].g = 200;
    colours[13].b = 255;
    colours[14].name = new QString("Hot pink");
    colours[14].r = 230;
    colours[14].g = 0;
    colours[14].b = 102;
    colours[15].name = new QString("Light pink");
    colours[15].r = 230;
    colours[15].g = 128;
    colours[15].b = 128;
    colours[16].name = new QString("Purple");
    colours[16].r = 255;
    colours[16].g = 154;
    colours[16].b = 255;
    colours[17].name = new QString("Pink Purple");
    colours[17].r = 255;
    colours[17].g = 77;
    colours[17].b = 230;

    toolBar = new QToolBar(this);
    send = new QPushButton("send", this);
    recon = new QPushButton("reconnect", this);
    cont = new QCheckBox("continue", this);
    colourCode = new QComboBox(this);
    for(int i=0 ; i<coloursNumber ; ++i) {
        colourCode->addItem(*colours[i].name);
    }
    colourCode->setCurrentIndex(3);
    allCheck = new QCheckBox("all", this);
    toolBar->addWidget(colourCode);
    toolBar->addWidget(allCheck);
    toolBar->addSeparator();
    toolBar->addWidget(send);
    toolBar->addWidget(cont);
    toolBar->addSeparator();
    toolBar->addWidget(port);
    toolBar->addWidget(recon);

    mainMenu = new QWidget(this);
    mainMenuLay = new QBoxLayout(QBoxLayout::BottomToTop, mainMenu);

    colour = new QWidget(this);
    colourLay = new QBoxLayout(QBoxLayout::BottomToTop, colour);
    rLay = new QBoxLayout(QBoxLayout::LeftToRight);
    gLay = new QBoxLayout(QBoxLayout::LeftToRight);
    bLay = new QBoxLayout(QBoxLayout::LeftToRight);
    r = new QSlider(Qt::Horizontal, this);
    r->setTickPosition(QSlider::TicksRight);
    r->setMinimum(0);
    r->setMaximum(255);
    r->setTickInterval(16);
    g = new QSlider(Qt::Horizontal, this);
    g->setTickPosition(QSlider::TicksRight);
    g->setMinimum(0);
    g->setMaximum(255);
    g->setTickInterval(16);
    b = new QSlider(Qt::Horizontal, this);
    b->setTickPosition(QSlider::TicksRight);
    b->setMinimum(0);
    b->setMaximum(255);
    b->setTickInterval(16);
    rt = new QSpinBox(this);
    rt->setMinimum(0);
    rt->setMaximum(255);
    gt = new QSpinBox(this);
    gt->setMinimum(0);
    gt->setMaximum(255);
    bt = new QSpinBox(this);
    bt->setMinimum(0);
    bt->setMaximum(255);
    rl = new QLabel(tr("r"));
    rl->setFixedWidth(10);
    gl = new QLabel(tr("g"));
    gl->setFixedWidth(10);
    bl = new QLabel(tr("b"));
    bl->setFixedWidth(10);
    rLay->addWidget(rl);
    rLay->addWidget(r);
    rLay->addWidget(rt);
    gLay->addWidget(gl);
    gLay->addWidget(g);
    gLay->addWidget(gt);
    bLay->addWidget(bl);
    bLay->addWidget(b);
    bLay->addWidget(bt);
    colourLay->addLayout(bLay);
    colourLay->addLayout(gLay);
    colourLay->addLayout(rLay);

    ledLine = new QWidget(this);
    lineLay = new QBoxLayout(QBoxLayout::LeftToRight, ledLine);
    for(int i=0 ; i<lineSize ; ++i)
    {
        ledLb[i] = new RgbLed(this, i);
        lineLay->addWidget(ledLb[i]);
        ledLb[i]->setFixedSize(50,100);
        connect(ledLb[i], SIGNAL(clicked(int)), this, SLOT(checked(int)));
    }
    mainMenuLay->addWidget(ledLine);
    mainMenuLay->addWidget(colour);
    portUpdTim = new QTimer(this);
    ledUpdTim = new QTimer(this);

    connect(r, SIGNAL(valueChanged(int)), this, SLOT(rChange()));
    connect(g, SIGNAL(valueChanged(int)), this, SLOT(gChange()));
    connect(b, SIGNAL(valueChanged(int)), this, SLOT(bChange()));
    connect(rt, SIGNAL(valueChanged(int)), this, SLOT(rtChange()));
    connect(gt, SIGNAL(valueChanged(int)), this, SLOT(gtChange()));
    connect(bt, SIGNAL(valueChanged(int)), this, SLOT(btChange()));
    connect(send, SIGNAL(released()), this, SLOT(update()));
    connect(recon, SIGNAL(released()), this, SLOT(reconnect()));
    connect(portUpdTim, SIGNAL(timeout()), this, SLOT(updateList()));
    connect(ledUpdTim, SIGNAL(timeout()), this, SLOT(update()));
    connect(cont, SIGNAL(stateChanged(int)), this, SLOT(continuous()));
    connect(allCheck, SIGNAL(stateChanged(int)), this, SLOT(allSlot()));
    connect(colourCode, SIGNAL(currentIndexChanged(int)), this, SLOT(preset()));


    all = false;
    change = true;
    addToolBar(toolBar);
    setStatusBar(stBar);
    setCentralWidget(mainMenu);
    portUpdTim->start(50000);

}

MainWindow::~MainWindow()
{
}

void MainWindow::copyToAll()
{
    for(int i=0 ; i<lineSize ; ++i) {
        ledLb[i]->setR(ledLb[activeLed]->getR());
        ledLb[i]->setG(ledLb[activeLed]->getG());
        ledLb[i]->setB(ledLb[activeLed]->getB());
    }
}


void MainWindow::preset()
{
    r->setValue(colours[colourCode->currentIndex()].r);
    g->setValue(colours[colourCode->currentIndex()].g);
    b->setValue(colours[colourCode->currentIndex()].b);
}


void MainWindow::continuous()
{
    if(cont->isChecked()) {
        ledUpdTim->start(50);
        stBar->showMessage("continuous update");
    } else {
        ledUpdTim->stop();
        stBar->showMessage("update on demand");
    }
}

void MainWindow::allSlot()
{
    all = allCheck->isChecked();
    if( all ) {
        for(int i=0 ; i<lineSize ; ++i) ledLb[i]->set();
        copyToAll();
        ledLine->setEnabled(false);
        change = true;
    } else {
        for(int i=0 ; i<lineSize ; ++i) ledLb[i]->reset();
        ledLine->setEnabled(true);
    }
}

void MainWindow::rChange()
{
    rt->setValue(r->value());
    ledLb[activeLed]->setR(r->value());
    if(all) copyToAll();
    change = true;
}

void MainWindow::gChange()
{
    gt->setValue(g->value());
    ledLb[activeLed]->setG(g->value());
    if(all) copyToAll();
    change = true;
}

void MainWindow::bChange()
{
    bt->setValue(b->value());
    ledLb[activeLed]->setB(b->value());
    if(all) copyToAll();
    change = true;
}

void MainWindow::rtChange()
{
    r->setValue(rt->value());
}

void MainWindow::gtChange()
{
    g->setValue(gt->value());
}

void MainWindow::btChange()
{
    b->setValue(bt->value());
}

void MainWindow::checked(int n)
{
    ledLb[activeLed]->reset();
    activeLed = n;
    r->setValue(ledLb[activeLed]->getR());
    g->setValue(ledLb[activeLed]->getG());
    b->setValue(ledLb[activeLed]->getB());
}

void MainWindow::update()
{
    if(!change) return;
    uint8_t msg[4*lineSize];
    for(int i=0; i<lineSize ; ++i) {
        msg[(i*4) + 0] = ledLb[i]->getR();
        msg[(i*4) + 1] = ledLb[i]->getG();
        msg[(i*4) + 2] = ledLb[i]->getB();
        msg[(i*4) + 3] = 0xee;
    }
    ledPort->write((const char*)msg, 4*lineSize);
    if( ledPort->waitForBytesWritten(1000) ) {
        stBar->showMessage("leds updated");
    } else {
        stBar->showMessage("device error");
    }
    change = false;
}

void MainWindow::reconnect()
{
    portUpdTim->stop();
    ledPort->close();
    QList<QSerialPortInfo> list;
    list = QSerialPortInfo::availablePorts();
    ledPort->setPortName(list[port->currentIndex()].portName());
    ledPort->setBaudRate(9600,QSerialPort::Output);
    if( ledPort->open(QIODevice::WriteOnly) ) {
        stBar->showMessage(list[port->currentIndex()].portName());
    } else {
        stBar->showMessage("port error");
    }
    portUpdTim->start();
}

void MainWindow::updateList()
{
    port->clear();
    QList<QSerialPortInfo> list;
    list = QSerialPortInfo::availablePorts();
    int n=0;
    for( int i=0 ; i<list.length() ; ++i) {
        port->addItem(list[i].portName());
        if( list[i].portName().contains("ACM") ) {
            ledPort->setPortName(list[i].portName());
            n = i;
        }
    }
    port->setCurrentIndex(n);
}
