#include "rgbLed.h"

RgbLed::RgbLed(QWidget* parent,int num)
    : QLabel(parent) {
    setStyleSheet("QLabel {border: 5px solid black; background-color: rgb(0, 0, 0)}");
    number = num;
}

RgbLed::~RgbLed() {}

void RgbLed::mousePressEvent(QMouseEvent* event) {
    emit clicked(number);
    setStyleSheet(QString::asprintf("QLabel {border: 5px solid red; background-color: rgb(%d, %d, %d)}",r,g,b));
    active = true;
}

void RgbLed::set()
{
    setStyleSheet(QString::asprintf("QLabel {border: 5px solid red; background-color: rgb(%d, %d, %d)}",r,g,b));
    active = true;
}

void RgbLed::reset()
{
    setStyleSheet(QString::asprintf("QLabel {border: 5px solid black; background-color: rgb(%d, %d, %d)}",r,g,b));
    active = false;
}

void RgbLed::setR(int r)
{
    if(r > 255) r = 255;
    this->r = r;
    if(active) {
        setStyleSheet(QString::asprintf("QLabel {border: 5px solid red; background-color: rgb(%d, %d, %d)}",r,g,b));
    }
}

void RgbLed::setG(int g)
{
    if(g > 255) g = 255;
    this->g = g;
    if(active) {
        setStyleSheet(QString::asprintf("QLabel {border: 5px solid red; background-color: rgb(%d, %d, %d)}",r,g,b));
    }
}

void RgbLed::setB(int b)
{
    if(b > 255) b = 255;
    this->b = b;
    if(active) {
        setStyleSheet(QString::asprintf("QLabel {border: 5px solid red; background-color: rgb(%d, %d, %d)}",r,g,b));
    }
}

int RgbLed::getR()
{
    return r;
}

int RgbLed::getG()
{
    return g;
}

int RgbLed::getB()
{
    return b;
}
