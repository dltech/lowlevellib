#ifndef H_TWAI_REGS
#define H_TWAI_REGS
/*
 * ESP32 alternative library, Two-wire Automotive Interface (TWAI®)
 * registers.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Mode Register */
#define TWAI_MODE_REG               MMIO32(TWAI_BASE + 0x00)
// This bit is used to configure the operating mode of the TWAI Controller.
#define TWAI_RESET_MODE         0x8
// 1: Listen only mode. In this mode the nodes will only receive messages
#define TWAI_LISTEN_ONLY_MODE   0x4
// 1: Self test mode. In this mode the TX nodes can perform a successful tx
#define TWAI_SELF_TEST_MODE     0x2
// This bit is used to configure the filter mode. 0: Dual filter mode; 1: Single
#define TWAI_RX_FILTER_MODE     0x1

/* Bus Timing Register */
#define TWAI_BUS_TIMING_0_REG       MMIO32(TWAI_BASE + 0x18)
// Baud Rate Prescaler, determines the frequency dividing ratio. (RO | R/W)
#define TWAI_BAUD_PRESC_MSK         0xc0
#define TWAI_BAUD_PRESC_SFT         6
#define TWAI_BAUD_PRESC_SET(n)      ((n<<TWAI_BAUD_PRESC_SFT)&TWAI_BAUD_PRESC_MSK)
// Synchronization Jump Width (SJW), 1 ~ 4 Tq wide. (RO | R/W)
#define TWAI_SYNC_JUMP_WIDTH_MSK    0x3f
#define TWAI_SYNC_JUMP_WIDTH_SET(n) (n&TWAI_SYNC_JUMP_WIDTH_MSK)

#define TWAI_BUS_TIMING_1_REG       MMIO32(TWAI_BASE + 0x1c)
// The number of sample points. 0: the bus is sampled once; 1: three times
#define TWAI_TIME_SAMP          0x80
// The width of PBS2. (RO | R/W)
#define TWAI_TIME_SEG2_MSK      0x70
#define TWAI_TIME_SEG2_SFT      4
#define TWAI_TIME_SEG2_SET(n)   ((n<<TWAI_TIME_SEG2_SFT)&TWAI_TIME_SEG2_MSK)
// The width of PBS1. (RO | R/W)
#define TWAI_TIME_SEG1_MSK      0x0f
#define TWAI_TIME_SEG1_SET(n)   (n&TWAI_TIME_SAMP_MSK)

/* Error Warning Limit Register */
#define TWAI_ERR_WARNING_LIMIT_REG  MMIO32(TWAI_BASE + 0x34)
// Error warning threshold.
#define TWAI_ERR_WARNING_LIMIT_MSG  0xff

/* Data Register 0 */
#define TWAI_DATA_0_REG             MMIO32(TWAI_BASE + 0x40)
#define TWAI_DATA_1_REG             MMIO32(TWAI_BASE + 0x44)
#define TWAI_DATA_2_REG             MMIO32(TWAI_BASE + 0x48)
#define TWAI_DATA_3_REG             MMIO32(TWAI_BASE + 0x4c)
#define TWAI_DATA_4_REG             MMIO32(TWAI_BASE + 0x50)
#define TWAI_DATA_5_REG             MMIO32(TWAI_BASE + 0x54)
#define TWAI_DATA_6_REG             MMIO32(TWAI_BASE + 0x58)
#define TWAI_DATA_7_REG             MMIO32(TWAI_BASE + 0x5c)
#define TWAI_DATA_8_REG             MMIO32(TWAI_BASE + 0x60)
#define TWAI_DATA_9_REG             MMIO32(TWAI_BASE + 0x64)
#define TWAI_DATA_10_REG            MMIO32(TWAI_BASE + 0x68)
#define TWAI_DATA_11_REG            MMIO32(TWAI_BASE + 0x6c)
#define TWAI_DATA_12_REG            MMIO32(TWAI_BASE + 0x70)
// Stored the nth byte info of the data to be transmitted under operating mode. (WO)
#define TWAI_TX_BYTE_MSG    0xff

/* Clock Divider Register */
#define TWAI_CLOCK_DIVIDER_REG      MMIO32(TWAI_BASE + 0x7c)
// bit can be configured under reset mode. 1: Extended mode, comp with CAN2.0B;
#define TWAI_EXT_MODE   0x80
// This bit can be configured under reset mode. 1: Disable the external CLKOUT pin
#define TWAI_CLOCK_OFF  0x08
// frequency dividing coefficients of the external CLKOUT pin. (R/W)
#define TWAI_CD_MSK     0x07
#define TWAI_CD_SET(n)  (n&TWAI_CD_MSK)

/* Command Register */
#define TWAI_CMD_REG                MMIO32(TWAI_BASE + 0x04)
// Self reception req cmmd. Set the bit to 1 to allow a msg be tx and rx simul.
#define TWAI_SELF_RX_REQ    0x10
// Set the bit to 1 to clear the data overrun status bit. (WO)
#define TWAI_CLR_OVERRUN    0x08
// Set the bit to 1 to release the RX buffer. (WO)
#define TWAI_RELEASE_BUF    0x04
// Set the bit to 1 to cancel a pending transmission request. (WO)
#define TWAI_ABORT_TX       0x02
// Set the bit to 1 to allow the driving nodes start transmission. (WO)
#define TWAI_TX_REQ         0x01

/* Status Register */
#define TWAI_STATUS_REG             MMIO32(TWAI_BASE + 0x08)
// 1: In bus-off status, the TWAI Cntrl is no longer involved in bus activities.
#define TWAI_BUS_OFF_ST     0x80
// 1: one of the RX/TX err cnt has reached or exceeded TWAI_ERR_WARNING_LIMIT_REG.
#define TWAI_ERR_ST         0x40
// 1: The TWAI Controller is transmitting a message to the bus. (RO)
#define TWAI_TX_ST          0x20
// 1: The TWAI Controller is receiving a message from the bus. (RO)
#define TWAI_RX_ST          0x10
// 1: The TWAI controller has successfully received a packet from the bus. (RO)
#define TWAI_TX_COMPLETE    0x08
// 1: The TX buffer is empty, the CPU may write a message into it. (RO)
#define TWAI_TX_BUF_ST      0x04
// 1: The RX FIFO is full and data overrun has occurred. (RO)
#define TWAI_OVERRUN_ST     0x02
// 1: The data in the RX buffer is not empty, with at least one received data pack.
#define TWAI_RX_BUF_ST      0x01

/* Arbitration Lost Capture Register */
#define TWAI_ARB_LOST_CAP_REG       MMIO32(TWAI_BASE + 0x2c)
// This register contains information about the bit position of lost arbitration.
#define TWAI_ARB_LOST_CAP_MSK   0x1f

/* Error Code Capture Register */
#define TWAI_ERR_CODE_CAP_REG       MMIO32(TWAI_BASE + 0x30)
// This register contains information about error types
#define TWAI_ECC_TYPE_BIT_ERR   0x00
#define TWAI_ECC_TYPE_FORM_ERR  0x40
#define TWAI_ECC_TYPE_STUFF_ERR 0x80
#define TWAI_ECC_TYPE_OTHER_ERR 0xc0
// information about transmission direction of the node when error occurs.
#define TWAI_ECC_DIRECTION      0x20
// This register contains information about the location of errors
#define TWAI_ECC_SEGMENT_MSG    0x1f
#define TWAI_ECC_SEGMENT_GET    (TWAI_ERR_CODE_CAP_REG&TWAI_ECC_SEGMENT_MSG)

/* Receive Error Counter Register */
#define TWAI_RX_ERR_CNT_REG         MMIO32(TWAI_BASE + 0x38)
// The RX error counter register, reflects value changes under reception status.
#define TWAI_RX_ERR_CNT_MSK 0xff

/* Transmit Error Counter Register */
#define TWAI_TX_ERR_CNT_REG         MMIO32(TWAI_BASE + 0x3c)
// The TX error counter register, reflects value changes under transmission status.
#define TWAI_TX_ERR_CNT_MSK 0xff

/* Receive Message Counter Register */
#define TWAI_RX_MESSAGE_CNT_REG     MMIO32(TWAI_BASE + 0x74)
// This register reflects the number of messages available within the RX FIFO. (RO)
#define TWAI_RX_MESSAGE_COUNTER_MSG 0x7f

/* Interrupt Register */
#define TWAI_INT_RAW_REG            MMIO32(TWAI_BASE + 0x0c)
// Error interrupt.
#define TWAI_BUS_ERR_INT_RAW        0x80
// Arbitration lost interrupt.
#define TWAI_ARB_LOST_INT_RAW       0x40
// Error passive interrupt.
#define TWAI_ERR_PASSIVE_INT_RAW    0x20
// Data overrun interrupt.
#define TWAI_OVERRUN_INT_RAW        0x08
// Error warning interrupt.
#define TWAI_ERR_WARN_INT_RAW       0x04
// Transmit interrupt.
#define TWAI_TX_INT_RAW             0x02
// Receive interrupt.
#define TWAI_RX_INT_RAW             0x01

/* Interrupt Enable Register */
#define TWAI_INT_ENA_REG            MMIO32(TWAI_BASE + 0x10)
// Set this bit to 1 to enable error interrupt. (R/W)
#define TWAI_BUS_ERR_INT_ENA        0x80
// Set this bit to 1 to enable arbitration lost interrupt. (R/W)
#define TWAI_ARB_LOST_INT_ENA       0x40
// Set this bit to 1 to enable error passive interrupt. (R/W)
#define TWAI_ERR_PASSIVE_INT_ENA    0x20
// Set this bit to 1 to enable data overrun interrupt. (R/W)
#define TWAI_OVERRUN_INT_ENA        0x08
// Set this bit to 1 to enable error warning interrupt. (R/W)
#define TWAI_ERR_WARN_INT_ENA       0x04
// Set this bit to 1 to enable transmit interrupt. (R/W)
#define TWAI_TX_INT_ENA             0x02
// Set this bit to 1 to enable receive interrupt. (R/W)
#define TWAI_RX_INT_ENA             0x01







#endif
