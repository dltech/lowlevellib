#ifndef H_LEDC
#define H_LEDC
/*
 * ESP32 alternative library, LED PWM Controller (LEDC) registers.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Global ledc configuration register */
#define LEDC_CONF_REG           MMIO32(LED_PWM_BASE + 0x190)
// This bit is used to set the frequency of RTC_SLOW_CLK. (R/W)
#define LEDC_APB_CLK_SEL    0x1

/* Configuration register 0 for high-speed channel n */
#define LEDC_HSCH0_CONF0_REG    MMIO32(LED_PWM_BASE + 0x000)
#define LEDC_HSCH1_CONF0_REG    MMIO32(LED_PWM_BASE + 0x014)
#define LEDC_HSCH2_CONF0_REG    MMIO32(LED_PWM_BASE + 0x028)
#define LEDC_HSCH3_CONF0_REG    MMIO32(LED_PWM_BASE + 0x03c)
#define LEDC_HSCH4_CONF0_REG    MMIO32(LED_PWM_BASE + 0x050)
#define LEDC_HSCH5_CONF0_REG    MMIO32(LED_PWM_BASE + 0x064)
#define LEDC_HSCH6_CONF0_REG    MMIO32(LED_PWM_BASE + 0x078)
#define LEDC_HSCH7_CONF0_REG    MMIO32(LED_PWM_BASE + 0x08c)
// control the output value when high-speed channel n is inactive. (R/W)
#define LEDC_IDLE_LV_HSCH       0x8
// This is the output enable control bit for high-speed channel n. (R/W)
#define LEDC_SIG_OUT_EN_HSCH    0x4
// select one of them for high-speed channel n: (R/W)
#define LEDC_TIMER_SEL_HSCH_0   0x0
#define LEDC_TIMER_SEL_HSCH_1   0x1
#define LEDC_TIMER_SEL_HSCH_2   0x2
#define LEDC_TIMER_SEL_HSCH_3   0x3

/* Configuration register 1 for high-speed channel n */
#define LEDC_HSCH0_CONF1_REG    MMIO32(LED_PWM_BASE + 0x00c)
#define LEDC_HSCH1_CONF1_REG    MMIO32(LED_PWM_BASE + 0x020)
#define LEDC_HSCH2_CONF1_REG    MMIO32(LED_PWM_BASE + 0x034)
#define LEDC_HSCH3_CONF1_REG    MMIO32(LED_PWM_BASE + 0x048)
#define LEDC_HSCH4_CONF1_REG    MMIO32(LED_PWM_BASE + 0x05c)
#define LEDC_HSCH5_CONF1_REG    MMIO32(LED_PWM_BASE + 0x070)
#define LEDC_HSCH6_CONF1_REG    MMIO32(LED_PWM_BASE + 0x084)
#define LEDC_HSCH7_CONF1_REG    MMIO32(LED_PWM_BASE + 0x098)
// these register will not take effect until LEDC_DUTY_START_HSCHn is set.
#define LEDC_DUTY_START_HSCH        0x80000000
// increase or decrease the duty of output signal for high-speed channel n. (R/W)
#define LEDC_DUTY_INC_HSCH          0x40000000
// number of times the duty cycle is increased or decreased for high-speed ch n.
#define LEDC_DUTY_NUM_HSCH_MSK      0x3ff00000
#define LEDC_DUTY_NUM_HSCH_SFT      20
#define LEDC_DUTY_NUM_HSCH_SET(n) ((n<<LEDC_DUTY_NUM_HSCH_SFT)&LEDC_DUTY_NUM_HSCH_MSK)
// inc or dec the duty cycle every time LEDC_DUTY_CYCLE_HSCHn cyc for channel n.
#define LEDC_DUTY_CYCLE_HSCH_MSK    0x000ffc00
#define LEDC_DUTY_CYCLE_HSCH_SFT    10
#define LEDC_DUTY_CYCLE_HSCH_SET(n) ((n<<LEDC_DUTY_CYCLE_HSCH_SFT)&LEDC_DUTY_CYCLE_HSCH_MSK)
// increase or decrease the step scale for highspeed channel n. (R/W)
#define LEDC_DUTY_SCALE_HSCH_MSK    0x000003ff
#define LEDC_DUTY_SCALE_HSCH_SET(n) (n&LEDC_DUTY_SCALE_HSCH_MSK)

/* Configuration register 0 for low-speed channel n */
#define LEDC_LSCH0_CONF0_REG    MMIO32(LED_PWM_BASE + 0x0a0)
#define LEDC_LSCH1_CONF0_REG    MMIO32(LED_PWM_BASE + 0x0b4)
#define LEDC_LSCH2_CONF0_REG    MMIO32(LED_PWM_BASE + 0x0c8)
#define LEDC_LSCH3_CONF0_REG    MMIO32(LED_PWM_BASE + 0x0dc)
#define LEDC_LSCH4_CONF0_REG    MMIO32(LED_PWM_BASE + 0x0f0)
#define LEDC_LSCH5_CONF0_REG    MMIO32(LED_PWM_BASE + 0x104)
#define LEDC_LSCH6_CONF0_REG    MMIO32(LED_PWM_BASE + 0x118)
#define LEDC_LSCH7_CONF0_REG    MMIO32(LED_PWM_BASE + 0x12c)
// update register LEDC_LSCHn_HPOINT and LEDC_LSCHn_DUTY for low-speed channel n.
#define LEDC_PARA_UP_LSCH       0x10
// control the output value, when low-speed channel n is inactive. (R/W)
#define LEDC_IDLE_LV_LSCH       0x08
// This is the output enable control bit for low-speed channel n. (R/W)
#define LEDC_SIG_OUT_EN_LSCH    0x04
// select one of them for low-speed channel n: (R/W)
#define LEDC_TIMER_SEL_LSCH_0   0x00
#define LEDC_TIMER_SEL_LSCH_1   0x01
#define LEDC_TIMER_SEL_LSCH_2   0x02
#define LEDC_TIMER_SEL_LSCH_3   0x03

/* Configuration register 1 for low-speed channel n */
#define LEDC_LSCH0_CONF1_REG    MMIO32(LED_PWM_BASE + 0x0ac)
#define LEDC_LSCH1_CONF1_REG    MMIO32(LED_PWM_BASE + 0x0c0)
#define LEDC_LSCH2_CONF1_REG    MMIO32(LED_PWM_BASE + 0x0d4)
#define LEDC_LSCH3_CONF1_REG    MMIO32(LED_PWM_BASE + 0x0e8)
#define LEDC_LSCH4_CONF1_REG    MMIO32(LED_PWM_BASE + 0x0fc)
#define LEDC_LSCH5_CONF1_REG    MMIO32(LED_PWM_BASE + 0x110)
#define LEDC_LSCH6_CONF1_REG    MMIO32(LED_PWM_BASE + 0x124)
#define LEDC_LSCH7_CONF1_REG    MMIO32(LED_PWM_BASE + 0x138)
// these register will not take effect until LEDC_DUTY_START_LSCHn is set.
#define LEDC_DUTY_START_LSCH        0x80000000
// increase or decrease the duty of output signal for low-speed channel n. (R/W)
#define LEDC_DUTY_INC_LSCH          0x40000000
// number of times the duty cycle is increased or decreased for low-speed ch n.
#define LEDC_DUTY_NUM_LSCH_MSK      0x3ff00000
#define LEDC_DUTY_NUM_LSCH_SFT      20
#define LEDC_DUTY_NUM_LSCH_SET(n) ((n<<LEDC_DUTY_NUM_LSCH_SFT)&LEDC_DUTY_NUM_LSCH_MSK)
// inc or dec the duty cycle every time LEDC_DUTY_CYCLE_LSCHn cyc for channel n.
#define LEDC_DUTY_CYCLE_LSCH_MSK    0x000ffc00
#define LEDC_DUTY_CYCLE_LSCH_SFT    10
#define LEDC_DUTY_CYCLE_LSCH_SET(n) ((n<<LEDC_DUTY_CYCLE_LSCH_SFT)&LEDC_DUTY_CYCLE_LSCH_MSK)
// increase or decrease the step scale for lowspeed channel n. (R/W)
#define LEDC_DUTY_SCALE_LSCH_MSK    0x000003ff
#define LEDC_DUTY_SCALE_LSCH_SET(n) (n&LEDC_DUTY_SCALE_LSCH_MSK)

/*  */
#define LEDC_HSCH0_HPOINT_REG   MMIO32(LED_PWM_BASE + 0x020)
#define LEDC_HSCH1_HPOINT_REG   MMIO32(LED_PWM_BASE + 0x030)
#define LEDC_HSCH2_HPOINT_REG   MMIO32(LED_PWM_BASE + 0x040)
#define LEDC_HSCH3_HPOINT_REG   MMIO32(LED_PWM_BASE + 0x050)
#define LEDC_HSCH4_HPOINT_REG   MMIO32(LED_PWM_BASE + 0x060)
#define LEDC_HSCH5_HPOINT_REG   MMIO32(LED_PWM_BASE + 0x070)
#define LEDC_HSCH6_HPOINT_REG   MMIO32(LED_PWM_BASE + 0x080)
#define LEDC_HSCH7_HPOINT_REG   MMIO32(LED_PWM_BASE + 0x090)
// The output value changes to high when htimerx(x=[0,3])
#define LEDC_HPOINT_HSCH_MSK  0xffffff

/*  */
#define LEDC_LSCH0_HPOINT_REG   MMIO32(LED_PWM_BASE + 0x0c0)
#define LEDC_LSCH1_HPOINT_REG   MMIO32(LED_PWM_BASE + 0x0d0)
#define LEDC_LSCH2_HPOINT_REG   MMIO32(LED_PWM_BASE + 0x0e0)
#define LEDC_LSCH3_HPOINT_REG   MMIO32(LED_PWM_BASE + 0x0f0)
#define LEDC_LSCH4_HPOINT_REG   MMIO32(LED_PWM_BASE + 0x100)
#define LEDC_LSCH5_HPOINT_REG   MMIO32(LED_PWM_BASE + 0x110)
#define LEDC_LSCH6_HPOINT_REG   MMIO32(LED_PWM_BASE + 0x120)
#define LEDC_LSCH7_HPOINT_REG   MMIO32(LED_PWM_BASE + 0x130)
// The output value changes to high when htimerx(x=[0,3])
#define LEDC_HPOINT_LSCH_MSK  0xffffff

/* Initial duty cycle for high-speed channel n */
#define LEDC_HSCH0_DUTY_REG     MMIO32(LED_PWM_BASE + 0x008)
#define LEDC_HSCH1_DUTY_REG     MMIO32(LED_PWM_BASE + 0x01c)
#define LEDC_HSCH2_DUTY_REG     MMIO32(LED_PWM_BASE + 0x030)
#define LEDC_HSCH3_DUTY_REG     MMIO32(LED_PWM_BASE + 0x044)
#define LEDC_HSCH4_DUTY_REG     MMIO32(LED_PWM_BASE + 0x058)
#define LEDC_HSCH5_DUTY_REG     MMIO32(LED_PWM_BASE + 0x06c)
#define LEDC_HSCH6_DUTY_REG     MMIO32(LED_PWM_BASE + 0x080)
#define LEDC_HSCH7_DUTY_REG     MMIO32(LED_PWM_BASE + 0x094)
// The register is used to control output duty.
#define LEDC_DUTY_HSCH_MSK  0x1ffffff

/* Current duty cycle for high-speed channel n */
#define LEDC_HSCH0_DUTY_R_REG   MMIO32(LED_PWM_BASE + 0x010)
#define LEDC_HSCH1_DUTY_R_REG   MMIO32(LED_PWM_BASE + 0x024)
#define LEDC_HSCH2_DUTY_R_REG   MMIO32(LED_PWM_BASE + 0x038)
#define LEDC_HSCH3_DUTY_R_REG   MMIO32(LED_PWM_BASE + 0x04c)
#define LEDC_HSCH4_DUTY_R_REG   MMIO32(LED_PWM_BASE + 0x060)
#define LEDC_HSCH5_DUTY_R_REG   MMIO32(LED_PWM_BASE + 0x074)
#define LEDC_HSCH6_DUTY_R_REG   MMIO32(LED_PWM_BASE + 0x088)
#define LEDC_HSCH7_DUTY_R_REG   MMIO32(LED_PWM_BASE + 0x09c)
// current duty cycle of the output signal for high-speed channel n. (RO)
#define LEDC_DUTY_HSCH_R_MSK    0x1ffffff

/* Initial duty cycle for low-speed channel n */
#define LEDC_LSCH0_DUTY_REG     MMIO32(LED_PWM_BASE + 0x0a8)
#define LEDC_LSCH1_DUTY_REG     MMIO32(LED_PWM_BASE + 0x0bc)
#define LEDC_LSCH2_DUTY_REG     MMIO32(LED_PWM_BASE + 0x0d0)
#define LEDC_LSCH3_DUTY_REG     MMIO32(LED_PWM_BASE + 0x0e4)
#define LEDC_LSCH4_DUTY_REG     MMIO32(LED_PWM_BASE + 0x0f8)
#define LEDC_LSCH5_DUTY_REG     MMIO32(LED_PWM_BASE + 0x10c)
#define LEDC_LSCH6_DUTY_REG     MMIO32(LED_PWM_BASE + 0x120)
#define LEDC_LSCH7_DUTY_REG     MMIO32(LED_PWM_BASE + 0x134)
// The register is used to control output duty.
#define LEDC_DUTY_LSCH_MSK  0x1ffffff

/* Current duty cycle for low-speed channel n */
#define LEDC_LSCH0_DUTY_R_REG   MMIO32(LED_PWM_BASE + 0x0b0)
#define LEDC_LSCH1_DUTY_R_REG   MMIO32(LED_PWM_BASE + 0x0c4)
#define LEDC_LSCH2_DUTY_R_REG   MMIO32(LED_PWM_BASE + 0x0d8)
#define LEDC_LSCH3_DUTY_R_REG   MMIO32(LED_PWM_BASE + 0x0ec)
#define LEDC_LSCH4_DUTY_R_REG   MMIO32(LED_PWM_BASE + 0x100)
#define LEDC_LSCH5_DUTY_R_REG   MMIO32(LED_PWM_BASE + 0x114)
#define LEDC_LSCH6_DUTY_R_REG   MMIO32(LED_PWM_BASE + 0x128)
#define LEDC_LSCH7_DUTY_R_REG   MMIO32(LED_PWM_BASE + 0x13c)
// current duty of the output signal for low-speed channel n. (RO)
#define LEDC_DUTY_LSCH_R_MSK    0x1ffffff

/* High-speed timer x configuration */
#define LEDC_HSTIMER0_CONF_REG  MMIO32(LED_PWM_BASE + 0x140)
#define LEDC_HSTIMER1_CONF_REG  MMIO32(LED_PWM_BASE + 0x148)
#define LEDC_HSTIMER2_CONF_REG  MMIO32(LED_PWM_BASE + 0x150)
#define LEDC_HSTIMER3_CONF_REG  MMIO32(LED_PWM_BASE + 0x158)
// select APB_CLK or REF_TICK for high-speed timer x. (R/W)
#define LEDC_TICK_SEL_HSTIMER           0x2000000
// This bit is used to reset high-speed timer x.
#define LEDC_HSTIMER_RST                0x1000000
// This bit is used to suspend the counter in high-speed timer x. (R/W)
#define LEDC_HSTIMER_PAUSE              0x0800000
// configure the division factor for the divider in high-speed timer x.
#define LEDC_CLK_DIV_NUM_HSTIMER_MSK    0x07fffe0
#define LEDC_CLK_DIV_NUM_HSTIMER_SFT    5
#define LEDC_CLK_DIV_NUM_HSTIMER_SET(n) ((n<<LEDC_CLK_DIV_NUM_HSTIMER_SFT)&LEDC_CLK_DIV_NUM_HSTIMER_MSK)
// ange of the counter in high-speed timer x.
#define LEDC_HSTIMER_DUTY_RES_MSK       0x000001f
#define LEDC_HSTIMER_DUTY_RES_SET(n)    (n&LEDC_HSTIMER_DUTY_RES_MSK)

/* High-speed timer x current counter value */
#define LEDC_HSTIMER0_VALUE_REG MMIO32(LED_PWM_BASE + 0x144)
#define LEDC_HSTIMER1_VALUE_REG MMIO32(LED_PWM_BASE + 0x14c)
#define LEDC_HSTIMER2_VALUE_REG MMIO32(LED_PWM_BASE + 0x154)
#define LEDC_HSTIMER3_VALUE_REG MMIO32(LED_PWM_BASE + 0x15c)
// get the current counter value of high-speed timer x. (RO)
#define LEDC_HSTIMER_CNT_MSK    0xffffff

/* Low-speed timer x configuration */
#define LEDC_LSTIMER0_CONF_REG  MMIO32(LED_PWM_BASE + 0x160)
#define LEDC_LSTIMER1_CONF_REG  MMIO32(LED_PWM_BASE + 0x168)
#define LEDC_LSTIMER2_CONF_REG  MMIO32(LED_PWM_BASE + 0x170)
#define LEDC_LSTIMER3_CONF_REG  MMIO32(LED_PWM_BASE + 0x178)
// update LEDC_CLK_DIV_NUM_LSTIMEx and LEDC_LSTIMERx_DUTY_RES. (R/W)
#define LEDC_LSTIMER_PARA_UP            0x4000000
// select RTC_SLOW_CLK or REF_TICK for low-speed timer x. (R/W)
#define LEDC_TICK_SEL_LSTIMER           0x2000000
// This bit is used to reset low-speed timer x.
#define LEDC_LSTIMER_RST                0x1000000
// This bit is used to suspend the counter in low-speed timer x. (R/W)
#define LEDC_LSTIMER_PAUSE              0x0800000
// configure the division factor for the divider in low-speed timer x.
#define LEDC_CLK_DIV_NUM_LSTIMER_MSK    0x07fffe0
#define LEDC_CLK_DIV_NUM_LSTIMER_SFT    5
#define LEDC_CLK_DIV_NUM_LSTIMER_SET(n) ((n<<LEDC_CLK_DIV_NUM_LSTIMER_SFT)&LEDC_CLK_DIV_NUM_LSTIMER_MSK)
// range of the counter in low-speed timer x.
#define LEDC_LSTIMER_DUTY_RES_MSK       0x000001f
#define LEDC_LSTIMER_DUTY_RES_SET(n)    (n&LEDC_LSTIMER_DUTY_RES_MSK)

/* Low-speed timer x current counter value */
#define LEDC_LSTIMER0_VALUE_REG MMIO32(LED_PWM_BASE + 0x164)
#define LEDC_LSTIMER1_VALUE_REG MMIO32(LED_PWM_BASE + 0x16c)
#define LEDC_LSTIMER2_VALUE_REG MMIO32(LED_PWM_BASE + 0x174)
#define LEDC_LSTIMER3_VALUE_REG MMIO32(LED_PWM_BASE + 0x17c)
// get the current counter value of low-speed timer x. (RO)
#define LEDC_LSTIMER_CNT_MSK    0xffffff

/* Raw interrupt status */
#define LEDC_INT_RAW_REG        MMIO32(LED_PWM_BASE + 0x180)
// The raw interrupt status bit for the LEDC_DUTY_CHNG_END_LSCHn_INT interrupt. (RO)
#define LEDC_DUTY_CHNG_END_LSCH7_INT_RAW    0x800000
#define LEDC_DUTY_CHNG_END_LSCH6_INT_RAW    0x400000
#define LEDC_DUTY_CHNG_END_LSCH5_INT_RAW    0x200000
#define LEDC_DUTY_CHNG_END_LSCH4_INT_RAW    0x100000
#define LEDC_DUTY_CHNG_END_LSCH3_INT_RAW    0x080000
#define LEDC_DUTY_CHNG_END_LSCH2_INT_RAW    0x040000
#define LEDC_DUTY_CHNG_END_LSCH1_INT_RAW    0x020000
#define LEDC_DUTY_CHNG_END_LSCH0_INT_RAW    0x010000
// The raw interrupt status bit for the LEDC_DUTY_CHNG_END_HSCHn_INT interrupt. (RO)
#define LEDC_DUTY_CHNG_END_HSCH7_INT_RAW    0x008000
#define LEDC_DUTY_CHNG_END_HSCH6_INT_RAW    0x004000
#define LEDC_DUTY_CHNG_END_HSCH5_INT_RAW    0x002000
#define LEDC_DUTY_CHNG_END_HSCH4_INT_RAW    0x001000
#define LEDC_DUTY_CHNG_END_HSCH3_INT_RAW    0x000800
#define LEDC_DUTY_CHNG_END_HSCH2_INT_RAW    0x000400
#define LEDC_DUTY_CHNG_END_HSCH1_INT_RAW    0x000200
#define LEDC_DUTY_CHNG_END_HSCH0_INT_RAW    0x000100
// The raw interrupt status bit for the LEDC_LSTIMERx_OVF_INT interrupt. (RO)
#define LEDC_LSTIMER3_OVF_INT_RAW           0x000080
#define LEDC_LSTIMER2_OVF_INT_RAW           0x000040
#define LEDC_LSTIMER1_OVF_INT_RAW           0x000020
#define LEDC_LSTIMER0_OVF_INT_RAW           0x000010
// The raw interrupt status bit for the LEDC_HSTIMERx_OVF_INT interrupt. (RO)
#define LEDC_HSTIMER3_OVF_INT_RAW           0x000008
#define LEDC_HSTIMER2_OVF_INT_RAW           0x000004
#define LEDC_HSTIMER1_OVF_INT_RAW           0x000002
#define LEDC_HSTIMER0_OVF_INT_RAW           0x000001

/* Masked interrupt status */
#define LEDC_INT_ST_REG         MMIO32(LED_PWM_BASE + 0x184)
// The masked interrupt status bit for the LEDC_DUTY_CHNG_END_LSCHn_INT interrupt.
#define LEDC_DUTY_CHNG_END_LSCH7_INT_ST    0x800000
#define LEDC_DUTY_CHNG_END_LSCH6_INT_ST    0x400000
#define LEDC_DUTY_CHNG_END_LSCH5_INT_ST    0x200000
#define LEDC_DUTY_CHNG_END_LSCH4_INT_ST    0x100000
#define LEDC_DUTY_CHNG_END_LSCH3_INT_ST    0x080000
#define LEDC_DUTY_CHNG_END_LSCH2_INT_ST    0x040000
#define LEDC_DUTY_CHNG_END_LSCH1_INT_ST    0x020000
#define LEDC_DUTY_CHNG_END_LSCH0_INT_ST    0x010000
// The masked interrupt status bit for the LEDC_DUTY_CHNG_END_HSCHn_INT interrupt.
#define LEDC_DUTY_CHNG_END_HSCH7_INT_ST    0x008000
#define LEDC_DUTY_CHNG_END_HSCH6_INT_ST    0x004000
#define LEDC_DUTY_CHNG_END_HSCH5_INT_ST    0x002000
#define LEDC_DUTY_CHNG_END_HSCH4_INT_ST    0x001000
#define LEDC_DUTY_CHNG_END_HSCH3_INT_ST    0x000800
#define LEDC_DUTY_CHNG_END_HSCH2_INT_ST    0x000400
#define LEDC_DUTY_CHNG_END_HSCH1_INT_ST    0x000200
#define LEDC_DUTY_CHNG_END_HSCH0_INT_ST    0x000100
// The masked interrupt status bit for the LEDC_LSTIMERx_OVF_INT interrupt. (RO)
#define LEDC_LSTIMER3_OVF_INT_ST           0x000080
#define LEDC_LSTIMER2_OVF_INT_ST           0x000040
#define LEDC_LSTIMER1_OVF_INT_ST           0x000020
#define LEDC_LSTIMER0_OVF_INT_ST           0x000010
// The masked interrupt status bit for the LEDC_HSTIMERx_OVF_INT interrupt. (RO)
#define LEDC_HSTIMER3_OVF_INT_ST           0x000008
#define LEDC_HSTIMER2_OVF_INT_ST           0x000004
#define LEDC_HSTIMER1_OVF_INT_ST           0x000002
#define LEDC_HSTIMER0_OVF_INT_ST           0x000001

/* Interrupt enable bits */
#define LEDC_INT_ENA_REG        MMIO32(LED_PWM_BASE + 0x188)
// The interrupt enable bit for the LEDC_DUTY_CHNG_END_LSCHn_INT interrupt. (R/W)
#define LEDC_DUTY_CHNG_END_LSCH7_INT_ENA    0x800000
#define LEDC_DUTY_CHNG_END_LSCH6_INT_ENA    0x400000
#define LEDC_DUTY_CHNG_END_LSCH5_INT_ENA    0x200000
#define LEDC_DUTY_CHNG_END_LSCH4_INT_ENA    0x100000
#define LEDC_DUTY_CHNG_END_LSCH3_INT_ENA    0x080000
#define LEDC_DUTY_CHNG_END_LSCH2_INT_ENA    0x040000
#define LEDC_DUTY_CHNG_END_LSCH1_INT_ENA    0x020000
#define LEDC_DUTY_CHNG_END_LSCH0_INT_ENA    0x010000
// The interrupt enable bit for the LEDC_DUTY_CHNG_END_HSCHn_INT interrupt. (R/W)
#define LEDC_DUTY_CHNG_END_HSCH7_INT_ENA    0x008000
#define LEDC_DUTY_CHNG_END_HSCH6_INT_ENA    0x004000
#define LEDC_DUTY_CHNG_END_HSCH5_INT_ENA    0x002000
#define LEDC_DUTY_CHNG_END_HSCH4_INT_ENA    0x001000
#define LEDC_DUTY_CHNG_END_HSCH3_INT_ENA    0x000800
#define LEDC_DUTY_CHNG_END_HSCH2_INT_ENA    0x000400
#define LEDC_DUTY_CHNG_END_HSCH1_INT_ENA    0x000200
#define LEDC_DUTY_CHNG_END_HSCH0_INT_ENA    0x000100
// The interrupt enable bit for the LEDC_LSTIMERx_OVF_INT interrupt. (R/W)
#define LEDC_LSTIMER3_OVF_INT_ENA           0x000080
#define LEDC_LSTIMER2_OVF_INT_ENA           0x000040
#define LEDC_LSTIMER1_OVF_INT_ENA           0x000020
#define LEDC_LSTIMER0_OVF_INT_ENA           0x000010
// The interrupt enable bit for the LEDC_HSTIMERx_OVF_INT interrupt. (R/W)
#define LEDC_HSTIMER3_OVF_INT_ENA           0x000008
#define LEDC_HSTIMER2_OVF_INT_ENA           0x000004
#define LEDC_HSTIMER1_OVF_INT_ENA           0x000002
#define LEDC_HSTIMER0_OVF_INT_ENA           0x000001

/* Interrupt clear bits */
#define LEDC_INT_CLR_REG        MMIO32(LED_PWM_BASE + 0x18c)
// Set this bit to clear the LEDC_DUTY_CHNG_END_LSCHn_INT interrupt. (RW)
#define LEDC_DUTY_CHNG_END_LSCH7_INT_CLR    0x800000
#define LEDC_DUTY_CHNG_END_LSCH6_INT_CLR    0x400000
#define LEDC_DUTY_CHNG_END_LSCH5_INT_CLR    0x200000
#define LEDC_DUTY_CHNG_END_LSCH4_INT_CLR    0x100000
#define LEDC_DUTY_CHNG_END_LSCH3_INT_CLR    0x080000
#define LEDC_DUTY_CHNG_END_LSCH2_INT_CLR    0x040000
#define LEDC_DUTY_CHNG_END_LSCH1_INT_CLR    0x020000
#define LEDC_DUTY_CHNG_END_LSCH0_INT_CLR    0x010000
// Set this bit to clear the LEDC_DUTY_CHNG_END_HSCHn_INT interrupt. (RW)
#define LEDC_DUTY_CHNG_END_HSCH7_INT_CLR    0x008000
#define LEDC_DUTY_CHNG_END_HSCH6_INT_CLR    0x004000
#define LEDC_DUTY_CHNG_END_HSCH5_INT_CLR    0x002000
#define LEDC_DUTY_CHNG_END_HSCH4_INT_CLR    0x001000
#define LEDC_DUTY_CHNG_END_HSCH3_INT_CLR    0x000800
#define LEDC_DUTY_CHNG_END_HSCH2_INT_CLR    0x000400
#define LEDC_DUTY_CHNG_END_HSCH1_INT_CLR    0x000200
#define LEDC_DUTY_CHNG_END_HSCH0_INT_CLR    0x000100
// Set this bit to clear the LEDC_LSTIMERx_OVF_INT interrupt. (RW)
#define LEDC_LSTIMER3_OVF_INT_CLR           0x000080
#define LEDC_LSTIMER2_OVF_INT_CLR           0x000040
#define LEDC_LSTIMER1_OVF_INT_CLR           0x000020
#define LEDC_LSTIMER0_OVF_INT_CLR           0x000010
// Set this bit to clear the LEDC_HSTIMERx_OVF_INT interrupt. (RW)
#define LEDC_HSTIMER3_OVF_INT_CLR           0x000008
#define LEDC_HSTIMER2_OVF_INT_CLR           0x000004
#define LEDC_HSTIMER1_OVF_INT_CLR           0x000002
#define LEDC_HSTIMER0_OVF_INT_CLR           0x000001

#endif
