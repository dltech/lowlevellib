#ifndef H_FLASH_REGS
#define H_FLASH_REGS
/*
 * ESP32 alternative library, External Memory Encryption and Decryption
 * module registers.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Flash encryption buffer register 0 */
#define FLASH_ENCRYPTION_BUFFER_0_REG   MMIO32(FLASH_ENCRYPTION + 0x00)
#define FLASH_ENCRYPTION_BUFFER_1_REG   MMIO32(FLASH_ENCRYPTION + 0x04)
#define FLASH_ENCRYPTION_BUFFER_2_REG   MMIO32(FLASH_ENCRYPTION + 0x08)
#define FLASH_ENCRYPTION_BUFFER_3_REG   MMIO32(FLASH_ENCRYPTION + 0x0c)
#define FLASH_ENCRYPTION_BUFFER_4_REG   MMIO32(FLASH_ENCRYPTION + 0x10)
#define FLASH_ENCRYPTION_BUFFER_5_REG   MMIO32(FLASH_ENCRYPTION + 0x14)
#define FLASH_ENCRYPTION_BUFFER_6_REG   MMIO32(FLASH_ENCRYPTION + 0x18)
#define FLASH_ENCRYPTION_BUFFER_7_REG   MMIO32(FLASH_ENCRYPTION + 0x1c)
// Data buffers for encryption. (WO)

/* Encrypt operation control register */
#define FLASH_ENCRYPTION_START_REG      MMIO32(FLASH_ENCRYPTION + 0x20)
// Set this bit to start encryption operation on data buffer. (WO)
#define FLASH_START 0x1

/* External flash address register */
#define FLASH_ENCRYPTION_ADDRESS_REG    MMIO32(FLASH_ENCRYPTION + 0x24)
// The physical address on the off-chip flash must be 8-word boundary aligned. (WO)

/* Encrypt operation status register */
#define FLASH_ENCRYPTION_DONE_REG       MMIO32(FLASH_ENCRYPTION + 0x28)
// Set this bit when encryption operation is complete. (RO)
#define FLASH_DONE  0x1


#endif
