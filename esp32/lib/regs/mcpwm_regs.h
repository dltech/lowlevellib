#ifndef H_MCPWM_REGS
#define H_MCPWM_REGS
/*
 * ESP32 alternative library, The Motor Control Pulse Width Modulator
 * (MCPWM) peripheral registers.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Configuration of the prescaler */
#define PWM0_CLK_CFG_REG            MMIO32(MCPWM0_BASE + 0x000)
#define PWM1_CLK_CFG_REG            MMIO32(MCPWM1_BASE + 0x000)
// Period of PWM_clk = 6.25ns * (PWM_CLK_PRESCALE + 1). (R/W)
#define PWM_CLK_PRESCALE_MSK    0xff

/* Timer period and update method */
#define PWM0_TIMER0_CFG0_REG        MMIO32(MCPWM0_BASE + 0x004)
#define PWM1_TIMER0_CFG0_REG        MMIO32(MCPWM1_BASE + 0x004)
// Updating method for active register of PWM timer0 period.
#define PWM_TIMER0_PERIOD_UPMETHOD_IMMIDEATELY  0x0000000
#define PWM_TIMER0_PERIOD_UPMETHOD_AT_TEZ       0x1000000
#define PWM_TIMER0_PERIOD_UPMETHOD_AT_SYNC      0x2000000
#define PWM_TIMER0_PERIOD_UPMETHOD_TEZ_SYNC     0x3000000
// Period shadow register of PWM timer0. (R/W)
#define PWM_TIMER0_PERIOD_MSK                   0x0ffff00
#define PWM_TIMER0_PERIOD_SFT                   8
#define PWM_TIMER0_PERIOD_SET(n) ((n<<PWM_TIMER0_PERIOD_SFT)&PWM_TIMER0_PERIOD_MSK)
// Period of PT0_clk = Period of PWM_clk * (PWM_TIMER0_PRESCALE+ 1). (R/W)
#define PWM_TIMER0_PRESCALE_MSK                 0x00000ff
#define PWM_TIMER0_PRESCALE_SET(n) (n&PWM_TIMER0_PRESCALE_MSK)

/* Working mode and start/stop control */
#define PWM0_TIMER0_CFG1_REG        MMIO32(MCPWM0_BASE + 0x008)
#define PWM1_TIMER0_CFG1_REG        MMIO32(MCPWM1_BASE + 0x008)
// PWM timer0 working mode.
#define PWM_TIMER0_MOD_FREEZE   0x00
#define PWM_TIMER0_MOD_INC      0x08
#define PWM_TIMER0_MOD_DEC      0x10
#define PWM_TIMER0_MOD_UPDOWN   0x18
// PWM timer0 start and stop control.
#define PWM_TIMER0_START_STOP_TEZ       0x00
#define PWM_TIMER0_START_STOP_TEP       0x01
#define PWM_TIMER0_START_RUN            0x02
#define PWM_TIMER0_START_STOP_NEXT_TEZ  0x03
#define PWM_TIMER0_START_STOP_NEXT_TEP  0x04

/* Synchronization settings */
#define PWM0_TIMER0_SYNC_REG        MMIO32(MCPWM0_BASE + 0x00c)
#define PWM1_TIMER0_SYNC_REG        MMIO32(MCPWM1_BASE + 0x00c)
// 0: increase; 1: decrease. (R/W)
#define PWM_TIMER0_PHASE_DIRECTION  0x100000
// Phase for timer reload at sync event. (R/W)
#define PWM_TIMER0_PHASE_MSK        0x0ffff0
#define PWM_TIMER0_PHASE_SFT        4
#define PWM_TIMER0_PHASE_SET(n)     ((n<<PWM_TIMER0_PHASE_SFT)&PWM_TIMER0_PHASE_MSK)
// PWM timer0 sync_out selection.
#define PWM_TIMER0_SYNCO_SEL_IN     0x000000
#define PWM_TIMER0_SYNCO_SEL_TEZ    0x000004
#define PWM_TIMER0_SYNCO_SEL_TEP    0x000008
// Toggling this bit will trigger a software sync. (R/W)
#define PWM_TIMER0_SYNC_SW          0x000002
// When set, timer reloading with phase on sync input event is enabled. (R/W)
#define PWM_TIMER0_SYNCI_EN         0x000001

/* Timer status */
#define PWM0_TIMER0_STATUS_REG      MMIO32(MCPWM0_BASE + 0x010)
#define PWM1_TIMER0_STATUS_REG      MMIO32(MCPWM1_BASE + 0x010)
// Current direction of the PWM timer0 counter.
#define PWM_TIMER0_DIRECTION    0x10000
// Current value of the PWM timer0 counter. (RO)
#define PWM_TIMER0_VALUE_MSK    0x0ffff
#define PWM_TIMER0_VALUE_SET(n) (n&PWM_TIMER0_VALUE_MSK)

/* Timer update method and period */
#define PWM0_TIMER1_CFG0_REG        MMIO32(MCPWM0_BASE + 0x014)
#define PWM1_TIMER1_CFG0_REG        MMIO32(MCPWM1_BASE + 0x014)
// Updating method for active register of PWM timer1 period.
#define PWM_TIMER1_PERIOD_UPMETHOD_IMMIDEATELY  0x0000000
#define PWM_TIMER1_PERIOD_UPMETHOD_AT_TEZ       0x1000000
#define PWM_TIMER1_PERIOD_UPMETHOD_AT_SYNC      0x2000000
#define PWM_TIMER1_PERIOD_UPMETHOD_TEZ_SYNC     0x3000000
// Period shadow register of PWM timer1. (R/W)
#define PWM_TIMER1_PERIOD_MSK                   0x0ffff00
#define PWM_TIMER1_PERIOD_SFT                   8
#define PWM_TIMER1_PERIOD_SET(n) ((n<<PWM_TIMER1_PERIOD_SFT)&PWM_TIMER1_PERIOD_MSK)
// Period of PT1_clk = Period of PWM_clk * (PWM_TIMER1_PRESCALE+ 1). (R/W)
#define PWM_TIMER1_PRESCALE_MSK                 0x00000ff
#define PWM_TIMER1_PRESCALE_SET(n) (n&PWM_TIMER1_PRESCALE_MSK)

/* Working mode and start/stop control */
#define PWM0_TIMER1_CFG1_REG        MMIO32(MCPWM0_BASE + 0x018)
#define PWM1_TIMER1_CFG1_REG        MMIO32(MCPWM1_BASE + 0x018)
// PWM timer1 working mode.
#define PWM_TIMER1_MOD_FREEZE   0x00
#define PWM_TIMER1_MOD_INC      0x08
#define PWM_TIMER1_MOD_DEC      0x10
#define PWM_TIMER1_MOD_UPDOWN   0x18
// PWM timer0 start and stop control.
#define PWM_TIMER1_START_STOP_TEZ       0x00
#define PWM_TIMER1_START_STOP_TEP       0x01
#define PWM_TIMER1_START_RUN            0x02
#define PWM_TIMER1_START_STOP_NEXT_TEZ  0x03
#define PWM_TIMER1_START_STOP_NEXT_TEP  0x04

/* Synchronization settings */
#define PWM0_TIMER1_SYNC_REG        MMIO32(MCPWM0_BASE + 0x01c)
#define PWM1_TIMER1_SYNC_REG        MMIO32(MCPWM1_BASE + 0x01c)
// 0: increase; 1: decrease. (R/W)
#define PWM_TIMER1_PHASE_DIRECTION  0x100000
// Phase for timer reload at sync event. (R/W)
#define PWM_TIMER1_PHASE_MSK        0x0ffff0
#define PWM_TIMER1_PHASE_SFT        4
#define PWM_TIMER1_PHASE_SET(n)     ((n<<PWM_TIMER0_PHASE_SFT)&PWM_TIMER0_PHASE_MSK)
// PWM timer0 sync_out selection.
#define PWM_TIMER1_SYNCO_SEL_IN     0x000000
#define PWM_TIMER1_SYNCO_SEL_TEZ    0x000004
#define PWM_TIMER1_SYNCO_SEL_TEP    0x000008
// Toggling this bit will trigger a software sync. (R/W)
#define PWM_TIMER1_SYNC_SW          0x000002
// When set, timer reloading with phase on sync input event is enabled. (R/W)
#define PWM_TIMER1_SYNCI_EN         0x000001

/* Timer status */
#define PWM0_TIMER1_STATUS_REG      MMIO32(MCPWM0_BASE + 0x020)
#define PWM1_TIMER1_STATUS_REG      MMIO32(MCPWM1_BASE + 0x020)
// Current direction of the PWM timer1 counter.
#define PWM_TIMER1_DIRECTION    0x10000
// Current value of the PWM timer1 counter. (RO)
#define PWM_TIMER1_VALUE_MSK    0x0ffff
#define PWM_TIMER1_VALUE_SET(n) (n&PWM_TIMER0_VALUE_MSK)

/* Timer update method and period */
#define PWM0_TIMER2_CFG0_REG        MMIO32(MCPWM0_BASE + 0x024)
#define PWM1_TIMER2_CFG0_REG        MMIO32(MCPWM1_BASE + 0x024)
// Updating method for active register of PWM timer2 period.
#define PWM_TIMER2_PERIOD_UPMETHOD_IMMIDEATELY  0x0000000
#define PWM_TIMER2_PERIOD_UPMETHOD_AT_TEZ       0x1000000
#define PWM_TIMER2_PERIOD_UPMETHOD_AT_SYNC      0x2000000
#define PWM_TIMER2_PERIOD_UPMETHOD_TEZ_SYNC     0x3000000
// Period shadow register of PWM timer2. (R/W)
#define PWM_TIMER2_PERIOD_MSK                   0x0ffff00
#define PWM_TIMER2_PERIOD_SFT                   8
#define PWM_TIMER2_PERIOD_SET(n) ((n<<PWM_TIMER0_PERIOD_SFT)&PWM_TIMER0_PERIOD_MSK)
// Period of PT0_clk = Period of PWM_clk * (PWM_TIMER0_PRESCALE+ 1). (R/W)
#define PWM_TIMER2_PRESCALE_MSK                 0x00000ff
#define PWM_TIMER2_PRESCALE_SET(n) (n&PWM_TIMER0_PRESCALE_MSK)

/* Working mode and start/stop control */
#define PWM0_TIMER2_CFG1_REG        MMIO32(MCPWM0_BASE + 0x028)
#define PWM1_TIMER2_CFG1_REG        MMIO32(MCPWM1_BASE + 0x028)
// PWM timer2 working mode.
#define PWM_TIMER2_MOD_FREEZE   0x00
#define PWM_TIMER2_MOD_INC      0x08
#define PWM_TIMER2_MOD_DEC      0x10
#define PWM_TIMER2_MOD_UPDOWN   0x18
// PWM timer0 start and stop control.
#define PWM_TIMER2_START_STOP_TEZ       0x00
#define PWM_TIMER2_START_STOP_TEP       0x01
#define PWM_TIMER2_START_RUN            0x02
#define PWM_TIMER2_START_STOP_NEXT_TEZ  0x03
#define PWM_TIMER2_START_STOP_NEXT_TEP  0x04

/* Synchronization settings */
#define PWM0_TIMER2_SYNC_REG        MMIO32(MCPWM0_BASE + 0x02c)
#define PWM1_TIMER2_SYNC_REG        MMIO32(MCPWM1_BASE + 0x02c)
// 0: increase; 1: decrease. (R/W)
#define PWM_TIMER2_PHASE_DIRECTION  0x100000
// Phase for timer reload at sync event. (R/W)
#define PWM_TIMER2_PHASE_MSK        0x0ffff0
#define PWM_TIMER2_PHASE_SFT        4
#define PWM_TIMER2_PHASE_SET(n)     ((n<<PWM_TIMER0_PHASE_SFT)&PWM_TIMER0_PHASE_MSK)
// PWM timer0 sync_out selection.
#define PWM_TIMER2_SYNCO_SEL_IN     0x000000
#define PWM_TIMER2_SYNCO_SEL_TEZ    0x000004
#define PWM_TIMER2_SYNCO_SEL_TEP    0x000008
// Toggling this bit will trigger a software sync. (R/W)
#define PWM_TIMER2_SYNC_SW          0x000002
// When set, timer reloading with phase on sync input event is enabled. (R/W)
#define PWM_TIMER2_SYNCI_EN         0x000001

/* Timer status */
#define PWM0_TIMER2_STATUS_REG      MMIO32(MCPWM0_BASE + 0x030)
#define PWM1_TIMER2_STATUS_REG      MMIO32(MCPWM1_BASE + 0x030)
// Current direction of the PWM timer0 counter.
#define PWM_TIMER2_DIRECTION    0x10000
// Current value of the PWM timer0 counter. (RO)
#define PWM_TIMER2_VALUE_MSK    0x0ffff
#define PWM_TIMER2_VALUE_SET(n) (n&PWM_TIMER0_VALUE_MSK)

/* Synchronization input selection for timers */
#define PWM0_TIMER_SYNCI_CFG_REG    MMIO32(MCPWM0_BASE + 0x034)
#define PWM1_TIMER_SYNCI_CFG_REG    MMIO32(MCPWM1_BASE + 0x034)
// Invert SYNC2 from GPIO matrix. (R/W)
#define PWM_EXTERNAL_SYNCI2_INVERT  0x800
// Invert SYNC1 from GPIO matrix. (R/W)
#define PWM_EXTERNAL_SYNCI1_INVERT  0x400
// Invert SYNC0 from GPIO matrix. (R/W)
#define PWM_EXTERNAL_SYNCI0_INVERT  0x200
// Select sync input for PWM timer2.
#define PWM_TIMER2_SYNCISEL_TIMER0  0x040
#define PWM_TIMER2_SYNCISEL_TIMER1  0x080
#define PWM_TIMER2_SYNCISEL_TIMER2  0x0c0
#define PWM_TIMER2_SYNCISEL_SYNC0   0x100
#define PWM_TIMER2_SYNCISEL_SYNC1   0x140
#define PWM_TIMER2_SYNCISEL_SYNC2   0x180
// Select sync input for PWM timer1.
#define PWM_TIMER1_SYNCISEL_TIMER0  0x08
#define PWM_TIMER1_SYNCISEL_TIMER1  0x10
#define PWM_TIMER1_SYNCISEL_TIMER2  0x18
#define PWM_TIMER1_SYNCISEL_SYNC0   0x20
#define PWM_TIMER1_SYNCISEL_SYNC1   0x28
#define PWM_TIMER1_SYNCISEL_SYNC2   0x30
// Select sync input for PWM timer0.
#define PWM_TIMER0_SYNCISEL_TIMER0  0x01
#define PWM_TIMER0_SYNCISEL_TIMER1  0x02
#define PWM_TIMER0_SYNCISEL_TIMER2  0x03
#define PWM_TIMER0_SYNCISEL_SYNC0   0x04
#define PWM_TIMER0_SYNCISEL_SYNC1   0x05
#define PWM_TIMER0_SYNCISEL_SYNC2   0x06

/* Select specific timer for PWM operators */
#define PWM0_OPERATOR_TIMERSEL_REG  MMIO32(MCPWM0_BASE + 0x038)
#define PWM1_OPERATOR_TIMERSEL_REG  MMIO32(MCPWM1_BASE + 0x038)
// Select the PWM timer for PWM operator2’s timing reference.
#define PWM_OPERATOR2_TIMERSEL_TIM0 0x00
#define PWM_OPERATOR2_TIMERSEL_TIM1 0x10
#define PWM_OPERATOR2_TIMERSEL_TIM2 0x20
// Select the PWM timer for PWM operator1’s timing reference.
#define PWM_OPERATOR1_TIMERSEL_TIM0 0x00
#define PWM_OPERATOR1_TIMERSEL_TIM1 0x04
#define PWM_OPERATOR1_TIMERSEL_TIM2 0x08
// Select the PWM timer for PWM operator0’s timing reference.
#define PWM_OPERATOR0_TIMERSEL_TIM0 0x00
#define PWM_OPERATOR0_TIMERSEL_TIM1 0x01
#define PWM_OPERATOR0_TIMERSEL_TIM2 0x02

/* Transfer status and update method for time stamp registers A and B */
#define PWM0_GEN0_STMP_CFG_REG      MMIO32(MCPWM0_BASE + 0x03c)
#define PWM1_GEN0_STMP_CFG_REG      MMIO32(MCPWM1_BASE + 0x03c)
// PWM generator 0 time stamp B’s shadow reg is fill and to be trans to time stamp
#define PWM_GEN0_B_SHDW_FULL        0x200
// PWM generator 0 time stamp A’s shadow reg is fill and to be trans to time stamp
#define PWM_GEN0_A_SHDW_FULL        0x100
// Updating method for PWM generator 0 time stamp B’s active register.
#define PWM_GEN0_B_UPMETHOD_TEZ     0x010
#define PWM_GEN0_B_UPMETHOD_TEP     0x020
#define PWM_GEN0_B_UPMETHOD_SYNC    0x040
#define PWM_GEN0_B_UPMETHOD_DIS_UPD 0x080
// Updating method for PWM generator 0 time stamp A’s active register.
#define PWM_GEN0_A_UPMETHOD_TEZ     0x001
#define PWM_GEN0_A_UPMETHOD_TEP     0x002
#define PWM_GEN0_A_UPMETHOD_SYNC    0x004
#define PWM_GEN0_A_UPMETHOD_DIS_UPD 0x008

/* Shadow register for register A */
#define PWM0_GEN0_TSTMP_A_REG       MMIO32(MCPWM0_BASE + 0x040)
#define PWM1_GEN0_TSTMP_A_REG       MMIO32(MCPWM1_BASE + 0x040)
// PWM generator 0 time stamp A’s shadow register. (R/W)
#define PWM_GEN0_A_MSK  0xffff

/* Shadow register for register B */
#define PWM0_GEN0_TSTMP_B_REG       MMIO32(MCPWM0_BASE + 0x044)
#define PWM1_GEN0_TSTMP_B_REG       MMIO32(MCPWM1_BASE + 0x044)
// PWM generator 0 time stamp B’s shadow register. (R/W)
#define PWM_GEN0_B_MSK  0xffff

/* Fault event T0 and T1 handling */
#define PWM0_GEN0_CFG0_REG          MMIO32(MCPWM0_BASE + 0x048)
#define PWM1_GEN0_CFG0_REG          MMIO32(MCPWM1_BASE + 0x048)
// Source selection for PWM generator 0 event_t1, taking effect immediately.
#define PWM_GEN0_T1_SEL_FAULT_EVENT0    0x000
#define PWM_GEN0_T1_SEL_FAULT_EVENT1    0x080
#define PWM_GEN0_T1_SEL_FAULT_EVENT2    0x100
#define PWM_GEN0_T1_SEL_SYNC_TAKEN      0x180
// Source selection for PWM generator 0 event_t0, taking effect immediately,
#define PWM_GEN0_T0_SEL_FAULT_EVENT0    0x000
#define PWM_GEN0_T0_SEL_FAULT_EVENT1    0x010
#define PWM_GEN0_T0_SEL_FAULT_EVENT2    0x020
#define PWM_GEN0_T0_SEL_SYNC_TAKEN      0x030
// Updating method for PWM generator 0’s active register of configuration.
#define PWM_GEN0_CFG_UPMETHOD_TEZ       0x001
#define PWM_GEN0_CFG_UPMETHOD_TEP       0x002
#define PWM_GEN0_CFG_UPMETHOD_SYNC      0x004
#define PWM_GEN0_CFG_UPMETHOD_DIS_UPD   0x008

/* Permissives to force PWM0A and PWM0B outputs by software */
#define PWM0_GEN0_FORCE_REG         MMIO32(MCPWM0_BASE + 0x04c)
#define PWM1_GEN0_FORCE_REG         MMIO32(MCPWM1_BASE + 0x04c)
// Non-continuous immediate software-force mode for PWM0B.
#define PWM_GEN0_B_NCIFORCE_MODE_DIS            0x0000
#define PWM_GEN0_B_NCIFORCE_MODE_LOW            0x4000
#define PWM_GEN0_B_NCIFORCE_MODE_HIGH           0x8000
// Trigger of non-continuous immediate software-force event for PWM0B;
#define PWM_GEN0_B_NCIFORCE                     0x2000
// Non-continuous immediate software-force mode for PWM0A,
#define PWM_GEN0_A_NCIFORCE_MODE_DIS            0x0000
#define PWM_GEN0_A_NCIFORCE_MODE_LOW            0x0800
#define PWM_GEN0_A_NCIFORCE_MODE_HIGH           0x1000
// Trigger of non-continuous immediate software-force event for PWM0A;
#define PWM_GEN0_A_NCIFORCE                     0x0400
// Continuous software-force mode for PWM0B.
#define PWM_GEN0_B_CNTUFORCE_MODE_DIS           0x0000
#define PWM_GEN0_B_CNTUFORCE_MODE_LOW           0x0100
#define PWM_GEN0_B_CNTUFORCE_MODE_HIGH          0x0200
// Continuous software-force mode for PWM0A.
#define PWM_GEN0_A_CNTUFORCE_MODE_DIS           0x0000
#define PWM_GEN0_A_CNTUFORCE_MODE_LOW           0x0040
#define PWM_GEN0_A_CNTUFORCE_MODE_HIGH          0x0080
// Updating method for continuous software force of PWM generator0.
#define PWM_GEN0_CNTUFORCE_UPMETHOD_IMMIDEATELY 0x0000
#define PWM_GEN0_CNTUFORCE_UPMETHOD_AT_TEZ      0x0001
#define PWM_GEN0_CNTUFORCE_UPMETHOD_AT_TEP      0x0002
#define PWM_GEN0_CNTUFORCE_UPMETHOD_AT_TEA      0x0004
#define PWM_GEN0_CNTUFORCE_UPMETHOD_AT_TEB      0x0008
#define PWM_GEN0_CNTUFORCE_UPMETHOD_SYNC        0x0010
#define PWM_GEN0_CNTUFORCE_UPMETHOD_DISABLE     0x0020

/* Actions triggered by events on PWM0A */
#define PWM0_GEN0_A_REG             MMIO32(MCPWM0_BASE + 0x050)
#define PWM1_GEN0_A_REG             MMIO32(MCPWM1_BASE + 0x050)
// Action on PWM0A triggered by event_t1 when the timer decreases.
#define PWM_GEN0_A_DT1_NO_CHANGE    0x000000
#define PWM_GEN0_A_DT1_LOW          0x400000
#define PWM_GEN0_A_DT1_HIGH         0x800000
#define PWM_GEN0_A_DT1_TOGGLE       0xc00000
// Action on PWM0A triggered by event_t0 when the timer decreases. (R/W)
#define PWM_GEN0_A_DT0_NO_CHANGE    0x000000
#define PWM_GEN0_A_DT0_LOW          0x100000
#define PWM_GEN0_A_DT0_HIGH         0x200000
#define PWM_GEN0_A_DT0_TOGGLE       0x300000
// Action on PWM0A triggered by event TEB when the timer decreases. (R/W)
#define PWM_GEN0_A_DTEB_NO_CHANGE   0x000000
#define PWM_GEN0_A_DTEB_LOW         0x040000
#define PWM_GEN0_A_DTEB_HIGH        0x080000
#define PWM_GEN0_A_DTEB_TOGGLE      0x0c0000
// Action on PWM0A triggered by event TEA when the timer decreases. (R/W)
#define PWM_GEN0_A_DTEA_NO_CHANGE   0x000000
#define PWM_GEN0_A_DTEA_LOW         0x010000
#define PWM_GEN0_A_DTEA_HIGH        0x020000
#define PWM_GEN0_A_DTEA_TOGGLE      0x030000
// Action on PWM0A triggered by event TEP when the timer decreases. (R/W)
#define PWM_GEN0_A_DTEP_NO_CHANGE   0x000000
#define PWM_GEN0_A_DTEP_LOW         0x004000
#define PWM_GEN0_A_DTEP_HIGH        0x008000
#define PWM_GEN0_A_DTEP_TOGGLE      0x00c000
// Action on PWM0A triggered by event TEZ when the timer decreases. (R/W)
#define PWM_GEN0_A_DTEZ_NO_CHANGE   0x000000
#define PWM_GEN0_A_DTEZ_LOW         0x001000
#define PWM_GEN0_A_DTEZ_HIGH        0x002000
#define PWM_GEN0_A_DTEZ_TOGGLE      0x003000
// Action on PWM0A triggered by event_t1 when the timer increases. (R/W)
#define PWM_GEN0_A_UT1_NO_CHANGE    0x000000
#define PWM_GEN0_A_UT1_LOW          0x000400
#define PWM_GEN0_A_UT1_HIGH         0x000800
#define PWM_GEN0_A_UT1_TOGGLE       0x000c00
// Action on PWM0A triggered by event_t0 when the timer increases. (R/W)
#define PWM_GEN0_A_UT0_NO_CHANGE    0x000000
#define PWM_GEN0_A_UT0_LOW          0x000100
#define PWM_GEN0_A_UT0_HIGH         0x000200
#define PWM_GEN0_A_UT0_TOGGLE       0x000300
// Action on PWM0A triggered by event TEB when the timer increases. (R/W)
#define PWM_GEN0_A_UTEB_NO_CHANGE   0x000000
#define PWM_GEN0_A_UTEB_LOW         0x000040
#define PWM_GEN0_A_UTEB_HIGH        0x000080
#define PWM_GEN0_A_UTEB_TOGGLE      0x0000c0
// Action on PWM0A triggered by event TEA when the timer increases. (R/W)
#define PWM_GEN0_A_UTEA_NO_CHANGE   0x000000
#define PWM_GEN0_A_UTEA_LOW         0x000010
#define PWM_GEN0_A_UTEA_HIGH        0x000020
#define PWM_GEN0_A_UTEA_TOGGLE      0x000030
// Action on PWM0A triggered by event TEP when the timer increases. (R/W)
#define PWM_GEN0_A_UTEP_NO_CHANGE   0x000000
#define PWM_GEN0_A_UTEP_LOW         0x000004
#define PWM_GEN0_A_UTEP_HIGH        0x000008
#define PWM_GEN0_A_UTEP_TOGGLE      0x00000c
// Action on PWM0A triggered by event TEZ when the timer increases. (R/W)
#define PWM_GEN0_A_UTEZ_NO_CHANGE   0x000000
#define PWM_GEN0_A_UTEZ_LOW         0x000001
#define PWM_GEN0_A_UTEZ_HIGH        0x000002
#define PWM_GEN0_A_UTEZ_TOGGLE      0x000003

/* Actions triggered by events on PWM0B */
#define PWM0_GEN0_B_REG             MMIO32(MCPWM0_BASE + 0x054)
#define PWM1_GEN0_B_REG             MMIO32(MCPWM1_BASE + 0x054)
// Action on PWM0B triggered by event_t1 when the timer decreases.
#define PWM_GEN0_B_DT1_NO_CHANGE    0x000000
#define PWM_GEN0_B_DT1_LOW          0x400000
#define PWM_GEN0_B_DT1_HIGH         0x800000
#define PWM_GEN0_B_DT1_TOGGLE       0xc00000
// Action on PWM0B triggered by event_t0 when the timer decreases. (R/W)
#define PWM_GEN0_B_DT0_NO_CHANGE    0x000000
#define PWM_GEN0_B_DT0_LOW          0x100000
#define PWM_GEN0_B_DT0_HIGH         0x200000
#define PWM_GEN0_B_DT0_TOGGLE       0x300000
// Action on PWM0B triggered by event TEB when the timer decreases. (R/W)
#define PWM_GEN0_B_DTEB_NO_CHANGE   0x000000
#define PWM_GEN0_B_DTEB_LOW         0x040000
#define PWM_GEN0_B_DTEB_HIGH        0x080000
#define PWM_GEN0_B_DTEB_TOGGLE      0x0c0000
// Action on PWM0B triggered by event TEA when the timer decreases. (R/W)
#define PWM_GEN0_B_DTEA_NO_CHANGE   0x000000
#define PWM_GEN0_B_DTEA_LOW         0x010000
#define PWM_GEN0_B_DTEA_HIGH        0x020000
#define PWM_GEN0_B_DTEA_TOGGLE      0x030000
// Action on PWM0B triggered by event TEP when the timer decreases. (R/W)
#define PWM_GEN0_B_DTEP_NO_CHANGE   0x000000
#define PWM_GEN0_B_DTEP_LOW         0x004000
#define PWM_GEN0_B_DTEP_HIGH        0x008000
#define PWM_GEN0_B_DTEP_TOGGLE      0x00c000
// Action on PWM0B triggered by event TEZ when the timer decreases. (R/W)
#define PWM_GEN0_B_DTEZ_NO_CHANGE   0x000000
#define PWM_GEN0_B_DTEZ_LOW         0x001000
#define PWM_GEN0_B_DTEZ_HIGH        0x002000
#define PWM_GEN0_B_DTEZ_TOGGLE      0x003000
// Action on PWM0B triggered by event_t1 when the timer increases. (R/W)
#define PWM_GEN0_B_UT1_NO_CHANGE    0x000000
#define PWM_GEN0_B_UT1_LOW          0x000400
#define PWM_GEN0_B_UT1_HIGH         0x000800
#define PWM_GEN0_B_UT1_TOGGLE       0x000c00
// Action on PWM0B triggered by event_t0 when the timer increases. (R/W)
#define PWM_GEN0_B_UT0_NO_CHANGE    0x000000
#define PWM_GEN0_B_UT0_LOW          0x000100
#define PWM_GEN0_B_UT0_HIGH         0x000200
#define PWM_GEN0_B_UT0_TOGGLE       0x000300
// Action on PWM0B triggered by event TEB when the timer increases. (R/W)
#define PWM_GEN0_B_UTEB_NO_CHANGE   0x000000
#define PWM_GEN0_B_UTEB_LOW         0x000040
#define PWM_GEN0_B_UTEB_HIGH        0x000080
#define PWM_GEN0_B_UTEB_TOGGLE      0x0000c0
// Action on PWM0B triggered by event TEA when the timer increases. (R/W)
#define PWM_GEN0_B_UTEA_NO_CHANGE   0x000000
#define PWM_GEN0_B_UTEA_LOW         0x000010
#define PWM_GEN0_B_UTEA_HIGH        0x000020
#define PWM_GEN0_B_UTEA_TOGGLE      0x000030
// Action on PWM0B triggered by event TEP when the timer increases. (R/W)
#define PWM_GEN0_B_UTEP_NO_CHANGE   0x000000
#define PWM_GEN0_B_UTEP_LOW         0x000004
#define PWM_GEN0_B_UTEP_HIGH        0x000008
#define PWM_GEN0_B_UTEP_TOGGLE      0x00000c
// Action on PWM0B triggered by event TEZ when the timer increases. (R/W)
#define PWM_GEN0_B_UTEZ_NO_CHANGE   0x000000
#define PWM_GEN0_B_UTEZ_LOW         0x000001
#define PWM_GEN0_B_UTEZ_HIGH        0x000002
#define PWM_GEN0_B_UTEZ_TOGGLE      0x000003

/* Dead time type selection and configuration */
#define PWM0_DT0_CFG_REG            MMIO32(MCPWM0_BASE + 0x058)
#define PWM1_DT0_CFG_REG            MMIO32(MCPWM1_BASE + 0x058)
// Dead time generator 0 clock selection.
#define PWM_DT0_CLK_SEL                     0x20000
// S0 in Table 16-5. (R/W)
#define PWM_DT0_B_OUTBYPASS                 0x10000
// S1 in Table 16-5. (R/W)
#define PWM_DT0_A_OUTBYPASS                 0x08000
// S3 in Table 16-5. (R/W)
#define PWM_DT0_FED_OUTINVERT               0x04000
// S2 in Table 16-5. (R/W)
#define PWM_DT0_RED_OUTINVERT               0x02000
// S5 in Table 16-5. (R/W)
#define PWM_DT0_FED_INSEL                   0x01000
// S4 in Table 16-5. (R/W)
#define PWM_DT0_RED_INSEL                   0x00800
// S7 in Table 16-5. (R/W)
#define PWM_DT0_B_OUTSWAP                   0x00400
// S6 in Table 16-5. (R/W)
#define PWM_DT0_A_OUTSWAP                   0x00200
// S8 in Table 16-5
#define PWM_DT0_DEB_MODE                    0x00100
// Updating method for RED (rising edge delay) active register.
#define PWM_DT0_RED_UPMETHOD_IMMEDIATELY    0x00000
#define PWM_DT0_RED_UPMETHOD_TEZ            0x00010
#define PWM_DT0_RED_UPMETHOD_TEP            0x00020
#define PWM_DT0_RED_UPMETHOD_SYNC           0x00040
#define PWM_DT0_RED_UPMETHOD_DISABLE        0x00080
// Updating method for FED (falling edge delay) active register.
#define PWM_DT0_FED_UPMETHOD_IMMEDIATELY    0x00000
#define PWM_DT0_FED_UPMETHOD_TEZ            0x00001
#define PWM_DT0_FED_UPMETHOD_TEP            0x00002
#define PWM_DT0_FED_UPMETHOD_SYNC           0x00004
#define PWM_DT0_FED_UPMETHOD_DISABLE        0x00008

/* Shadow register for falling edge delay (FED) */
#define PWM0_DT0_FED_CFG_REG        MMIO32(MCPWM0_BASE + 0x05c)
#define PWM1_DT0_FED_CFG_REG        MMIO32(MCPWM1_BASE + 0x05c)
// Shadow register for FED. (R/W)
#define PWM_DT0_FED_MSK 0xffff

/* Shadow register for rising edge delay (RED) */
#define PWM0_DT0_RED_CFG_REG        MMIO32(MCPWM0_BASE + 0x060)
#define PWM1_DT0_RED_CFG_REG        MMIO32(MCPWM1_BASE + 0x060)
// Shadow register for RED. (R/W)
#define PWM_DT0_RED_MSK 0xffff

/* Carrier enable and configuration */
#define PWM0_CARRIER0_CFG_REG       MMIO32(MCPWM0_BASE + 0x064)
#define PWM1_CARRIER0_CFG_REG       MMIO32(MCPWM1_BASE + 0x064)
// When set, invert the input of PWM0A and PWM0B for this submodule. (R/W)
#define PWM_CARRIER0_IN_INVERT          0x2000
// When set, invert the output of PWM0A and PWM0B for this submodule. (R/W)
#define PWM_CARRIER0_OUT_INVERT         0x1000
// Width of the first pulse in number of periods of the carrier. (R/W)
#define PWM_CARRIER0_OSHWTH_MSK         0x0f00
#define PWM_CARRIER0_OSHWTH_SFT         8
#define PWM_CARRIER0_OSHWTH_SET(n) ((n<<PWM_CARRIER0_OSHWTH_SFT)&PWM_CARRIER0_OSHWTH_MSK)
// Carrier duty selection. Duty = PWM_CARRIER0_DUTY/8. (R/W)
#define PWM_CARRIER0_DUTY_MSK           0x00e0
#define PWM_CARRIER0_DUTY_SFT           5
#define PWM_CARRIER0_DUTY_SET(n) ((n<<PWM_CARRIER0_DUTY_SFT)&PWM_CARRIER0_DUTY_MSK)
// PWM carrier0 clock (PC_clk) prescale value.
#define PWM_CARRIER0_PRESCALE_MSK       0x001e
#define PWM_CARRIER0_PRESCALE_SFT       1
#define PWM_CARRIER0_PRESCALE_SET(n) ((n<<PWM_CARRIER0_PRESCALE_SFT)&PWM_CARRIER0_PRESCALE_MSK)
// When set, carrier0 function is enabled. When cleared, carrier0 is bypassed.
#define PWM_CARRIER0_EN                 0x0001

/* Actions on PWM0A and PWM0B on trip events */
#define PWM0_FH0_CFG0_REG           MMIO32(MCPWM0_BASE + 0x068)
#define PWM1_FH0_CFG0_REG           MMIO32(MCPWM1_BASE + 0x068)
// One-shot mode action on PWM0B when a fault event occurs and the timer is inc.
#define PWM_FH0_B_OST_U_NO_CHANGE   0x000000
#define PWM_FH0_B_OST_U_LOW         0x400000
#define PWM_FH0_B_OST_U_HIGH        0x800000
#define PWM_FH0_B_OST_U_TOGGLE      0xc00000
// One-shot mode action on PWM0B when a fault event occurs and the timer is decr.
#define PWM_FH0_B_OST_D_NO_CHANGE   0x000000
#define PWM_FH0_B_OST_D_LOW         0x100000
#define PWM_FH0_B_OST_D_HIGH        0x200000
#define PWM_FH0_B_OST_D_TOGGLE      0x300000
// Cycle-by-cycle mode action on PWM0B when a fault event occurs and the tim is inc.
#define PWM_FH0_B_CBC_U_NO_CHANGE   0x000000
#define PWM_FH0_B_CBC_U_LOW         0x040000
#define PWM_FH0_B_CBC_U_HIGH        0x080000
#define PWM_FH0_B_CBC_U_TOGGLE      0x0c0000
// Cycle-by-cycle mode action on PWM0B when a fault ev occurs and the tim is decr.
#define PWM_FH0_B_CBC_D_NO_CHANGE   0x000000
#define PWM_FH0_B_CBC_D_LOW         0x010000
#define PWM_FH0_B_CBC_D_HIGH        0x020000
#define PWM_FH0_B_CBC_D_TOGGLE      0x030000
// One-shot mode action on PWM0A when a fault event occurs and the tim is inc.
#define PWM_FH0_A_OST_U_NO_CHANGE   0x000000
#define PWM_FH0_A_OST_U_LOW         0x004000
#define PWM_FH0_A_OST_U_HIGH        0x008000
#define PWM_FH0_A_OST_U_TOGGLE      0x00c000
// One-shot mode action on PWM0A when a fault event occurs and the timer is decr
#define PWM_FH0_A_OST_D_NO_CHANGE   0x000000
#define PWM_FH0_A_OST_D_LOW         0x001000
#define PWM_FH0_A_OST_D_HIGH        0x002000
#define PWM_FH0_A_OST_D_TOGGLE      0x003000
// Cycle-by-cycle mode action on PWM0A when a fault event occurs and the tim is inc.
#define PWM_FH0_A_CBC_U_NO_CHANGE   0x000000
#define PWM_FH0_A_CBC_U_LOW         0x000400
#define PWM_FH0_A_CBC_U_HIGH        0x000800
#define PWM_FH0_A_CBC_U_TOGGLE      0x000c00
// Cycle-by-cycle mode action on PWM0A when a fault ev occurs and the tim is decr.
#define PWM_FH0_A_CBC_D_NO_CHANGE   0x000000
#define PWM_FH0_A_CBC_D_LOW         0x000100
#define PWM_FH0_A_CBC_D_HIGH        0x000200
#define PWM_FH0_A_CBC_D_TOGGLE      0x000300
// event_f0 will trigger one-shot mode action.
#define PWM_FH0_F0_OST              0x000080
// event_f1 will trigger one-shot mode action.
#define PWM_FH0_F1_OST              0x000040
// event_f2 will trigger one-shot mode action.
#define PWM_FH0_F2_OST              0x000020
// Enable register for software-forced one-shot mode action.
#define PWM_FH0_SW_OST              0x000010
// event_f0 will trigger cycle-by-cycle mode action.
#define PWM_FH0_F0_CBC              0x000008
// event_f1 will trigger cycle-by-cycle mode action.
#define PWM_FH0_F1_CBC              0x000004
// event_f2 will trigger cycle-by-cycle mode action.
#define PWM_FH0_F2_CBC              0x000002
// Enable register for software-forced cycle-by-cycle mode action.
#define PWM_FH0_SW_CBC              0x000001

/* Software triggers for fault handler actions */
#define PWM0_FH0_CFG1_REG           MMIO32(MCPWM0_BASE + 0x06c)
#define PWM1_FH0_CFG1_REG           MMIO32(MCPWM1_BASE + 0x06c)
// A toggle (software negation of this bit’s value) triggers a one-shot mode action.
#define PWM_FH0_FORCE_OST       0x10
// A toggle triggers a cycle-by-cycle mode action. (R/W)
#define PWM_FH0_FORCE_CBC       0x08
// The cycle-by-cycle mode action refresh moment selection.
#define PWM_FH0_CBCPULSE_TEZ    0x02
#define PWM_FH0_CBCPULSE_TEP    0x04
// A toggle will clear on-going one-shot mode action. (R/W)
#define PWM_FH0_CLR_OST         0x01

/* Status of fault events */
#define PWM0_FH0_STATUS_REG         MMIO32(MCPWM0_BASE + 0x070)
#define PWM1_FH0_STATUS_REG         MMIO32(MCPWM1_BASE + 0x070)
// Set and reset by hardware. If set, a one-shot mode action is on-going. (RO)
#define PWM_FH0_OST_ON  0x2
// Set and reset by hardware. If set, a cycle-by-cycle mode action is on-going.
#define PWM_FH0_CBC_ON  0x1

/* Transfer status and update method for time stamp registers A and B */
#define PWM0_GEN1_STMP_CFG_REG      MMIO32(MCPWM0_BASE + 0x074)
#define PWM1_GEN1_STMP_CFG_REG      MMIO32(MCPWM1_BASE + 0x074)
// PWM generator 0 time stamp B’s shadow reg is fill and to be trans to time stamp
#define PWM_GEN1_B_SHDW_FULL        0x200
// PWM generator 0 time stamp A’s shadow reg is fill and to be trans to time stamp
#define PWM_GEN1_A_SHDW_FULL        0x100
// Updating method for PWM generator 0 time stamp B’s active register.
#define PWM_GEN1_B_UPMETHOD_TEZ     0x010
#define PWM_GEN1_B_UPMETHOD_TEP     0x020
#define PWM_GEN1_B_UPMETHOD_SYNC    0x040
#define PWM_GEN1_B_UPMETHOD_DIS_UPD 0x080
// Updating method for PWM generator 0 time stamp A’s active register.
#define PWM_GEN1_A_UPMETHOD_TEZ     0x001
#define PWM_GEN1_A_UPMETHOD_TEP     0x002
#define PWM_GEN1_A_UPMETHOD_SYNC    0x004
#define PWM_GEN1_A_UPMETHOD_DIS_UPD 0x008

/* Shadow register for register A */
#define PWM0_GEN1_TSTMP_A_REG       MMIO32(MCPWM0_BASE + 0x078)
#define PWM1_GEN1_TSTMP_A_REG       MMIO32(MCPWM1_BASE + 0x078)
// PWM generator 1 time stamp A’s shadow register. (R/W)
#define PWM_GEN1_A_MSK  0xffff

/* Shadow register for register B */
#define PWM0_GEN1_TSTMP_B_REG       MMIO32(MCPWM0_BASE + 0x07c)
#define PWM1_GEN1_TSTMP_B_REG       MMIO32(MCPWM1_BASE + 0x07c)
// PWM generator 1 time stamp B’s shadow register. (R/W)
#define PWM_GEN1_B_MSK  0xffff

/* Fault event T0 and T1 handling */
#define PWM0_GEN1_CFG0_REG          MMIO32(MCPWM0_BASE + 0x080)
#define PWM1_GEN1_CFG0_REG          MMIO32(MCPWM1_BASE + 0x080)
// Source selection for PWM generator 1 event_t1, taking effect immediately.
#define PWM_GEN1_T1_SEL_FAULT_EVENT0    0x000
#define PWM_GEN1_T1_SEL_FAULT_EVENT1    0x080
#define PWM_GEN1_T1_SEL_FAULT_EVENT2    0x100
#define PWM_GEN1_T1_SEL_SYNC_TAKEN      0x180
// Source selection for PWM generator 1 event_t0, taking effect immediately,
#define PWM_GEN1_T0_SEL_FAULT_EVENT0    0x000
#define PWM_GEN1_T0_SEL_FAULT_EVENT1    0x010
#define PWM_GEN1_T0_SEL_FAULT_EVENT2    0x020
#define PWM_GEN1_T0_SEL_SYNC_TAKEN      0x030
// Updating method for PWM generator 1’s active register of configuration.
#define PWM_GEN1_CFG_UPMETHOD_TEZ       0x001
#define PWM_GEN1_CFG_UPMETHOD_TEP       0x002
#define PWM_GEN1_CFG_UPMETHOD_SYNC      0x004
#define PWM_GEN1_CFG_UPMETHOD_DIS_UPD   0x008

/* Permissives to force PWM0A and PWM0B outputs by software */
#define PWM0_GEN1_FORCE_REG         MMIO32(MCPWM0_BASE + 0x084)
#define PWM1_GEN1_FORCE_REG         MMIO32(MCPWM1_BASE + 0x084)
// Non-continuous immediate software-force mode for PWM1B.
#define PWM_GEN1_B_NCIFORCE_MODE_DIS            0x0000
#define PWM_GEN1_B_NCIFORCE_MODE_LOW            0x4000
#define PWM_GEN1_B_NCIFORCE_MODE_HIGH           0x8000
// Trigger of non-continuous immediate software-force event for PWM1B;
#define PWM_GEN1_B_NCIFORCE                     0x2000
// Non-continuous immediate software-force mode for PWM1A,
#define PWM_GEN1_A_NCIFORCE_MODE_DIS            0x0000
#define PWM_GEN1_A_NCIFORCE_MODE_LOW            0x0800
#define PWM_GEN1_A_NCIFORCE_MODE_HIGH           0x1000
// Trigger of non-continuous immediate software-force event for PWM1A;
#define PWM_GEN1_A_NCIFORCE                     0x0400
// Continuous software-force mode for PWM1B.
#define PWM_GEN1_B_CNTUFORCE_MODE_DIS           0x0000
#define PWM_GEN1_B_CNTUFORCE_MODE_LOW           0x0100
#define PWM_GEN1_B_CNTUFORCE_MODE_HIGH          0x0200
// Continuous software-force mode for PWM1A.
#define PWM_GEN1_A_CNTUFORCE_MODE_DIS           0x0000
#define PWM_GEN1_A_CNTUFORCE_MODE_LOW           0x0040
#define PWM_GEN1_A_CNTUFORCE_MODE_HIGH          0x0080
// Updating method for continuous software force of PWM generator1.
#define PWM_GEN1_CNTUFORCE_UPMETHOD_IMMIDEATELY 0x0000
#define PWM_GEN1_CNTUFORCE_UPMETHOD_AT_TEZ      0x0001
#define PWM_GEN1_CNTUFORCE_UPMETHOD_AT_TEP      0x0002
#define PWM_GEN1_CNTUFORCE_UPMETHOD_AT_TEA      0x0004
#define PWM_GEN1_CNTUFORCE_UPMETHOD_AT_TEB      0x0008
#define PWM_GEN1_CNTUFORCE_UPMETHOD_SYNC        0x0010
#define PWM_GEN1_CNTUFORCE_UPMETHOD_DISABLE     0x0020

/* Actions triggered by events on PWM0A */
#define PWM0_GEN1_A_REG             MMIO32(MCPWM0_BASE + 0x088)
#define PWM1_GEN1_A_REG             MMIO32(MCPWM1_BASE + 0x088)
// Action on PWM1A triggered by event_t1 when the timer decreases.
#define PWM_GEN1_A_DT1_NO_CHANGE    0x000000
#define PWM_GEN1_A_DT1_LOW          0x400000
#define PWM_GEN1_A_DT1_HIGH         0x800000
#define PWM_GEN1_A_DT1_TOGGLE       0xc00000
// Action on PWM1A triggered by event_t0 when the timer decreases. (R/W)
#define PWM_GEN1_A_DT0_NO_CHANGE    0x000000
#define PWM_GEN1_A_DT0_LOW          0x100000
#define PWM_GEN1_A_DT0_HIGH         0x200000
#define PWM_GEN1_A_DT0_TOGGLE       0x300000
// Action on PWM1A triggered by event TEB when the timer decreases. (R/W)
#define PWM_GEN1_A_DTEB_NO_CHANGE   0x000000
#define PWM_GEN1_A_DTEB_LOW         0x040000
#define PWM_GEN1_A_DTEB_HIGH        0x080000
#define PWM_GEN1_A_DTEB_TOGGLE      0x0c0000
// Action on PWM1A triggered by event TEA when the timer decreases. (R/W)
#define PWM_GEN1_A_DTEA_NO_CHANGE   0x000000
#define PWM_GEN1_A_DTEA_LOW         0x010000
#define PWM_GEN1_A_DTEA_HIGH        0x020000
#define PWM_GEN1_A_DTEA_TOGGLE      0x030000
// Action on PWM1A triggered by event TEP when the timer decreases. (R/W)
#define PWM_GEN1_A_DTEP_NO_CHANGE   0x000000
#define PWM_GEN1_A_DTEP_LOW         0x004000
#define PWM_GEN1_A_DTEP_HIGH        0x008000
#define PWM_GEN1_A_DTEP_TOGGLE      0x00c000
// Action on PWM1A triggered by event TEZ when the timer decreases. (R/W)
#define PWM_GEN1_A_DTEZ_NO_CHANGE   0x000000
#define PWM_GEN1_A_DTEZ_LOW         0x001000
#define PWM_GEN1_A_DTEZ_HIGH        0x002000
#define PWM_GEN1_A_DTEZ_TOGGLE      0x003000
// Action on PWM1A triggered by event_t1 when the timer increases. (R/W)
#define PWM_GEN1_A_UT1_NO_CHANGE    0x000000
#define PWM_GEN1_A_UT1_LOW          0x000400
#define PWM_GEN1_A_UT1_HIGH         0x000800
#define PWM_GEN1_A_UT1_TOGGLE       0x000c00
// Action on PWM1A triggered by event_t0 when the timer increases. (R/W)
#define PWM_GEN1_A_UT0_NO_CHANGE    0x000000
#define PWM_GEN1_A_UT0_LOW          0x000100
#define PWM_GEN1_A_UT0_HIGH         0x000200
#define PWM_GEN1_A_UT0_TOGGLE       0x000300
// Action on PWM1A triggered by event TEB when the timer increases. (R/W)
#define PWM_GEN1_A_UTEB_NO_CHANGE   0x000000
#define PWM_GEN1_A_UTEB_LOW         0x000040
#define PWM_GEN1_A_UTEB_HIGH        0x000080
#define PWM_GEN1_A_UTEB_TOGGLE      0x0000c0
// Action on PWM1A triggered by event TEA when the timer increases. (R/W)
#define PWM_GEN1_A_UTEA_NO_CHANGE   0x000000
#define PWM_GEN1_A_UTEA_LOW         0x000010
#define PWM_GEN1_A_UTEA_HIGH        0x000020
#define PWM_GEN1_A_UTEA_TOGGLE      0x000030
// Action on PWM1A triggered by event TEP when the timer increases. (R/W)
#define PWM_GEN1_A_UTEP_NO_CHANGE   0x000000
#define PWM_GEN1_A_UTEP_LOW         0x000004
#define PWM_GEN1_A_UTEP_HIGH        0x000008
#define PWM_GEN1_A_UTEP_TOGGLE      0x00000c
// Action on PWM1A triggered by event TEZ when the timer increases. (R/W)
#define PWM_GEN1_A_UTEZ_NO_CHANGE   0x000000
#define PWM_GEN1_A_UTEZ_LOW         0x000001
#define PWM_GEN1_A_UTEZ_HIGH        0x000002
#define PWM_GEN1_A_UTEZ_TOGGLE      0x000003

/* Actions triggered by events on PWM0B */
#define PWM0_GEN1_B_REG             MMIO32(MCPWM0_BASE + 0x08c)
#define PWM1_GEN1_B_REG             MMIO32(MCPWM1_BASE + 0x08c)
// Action on PWM1B triggered by event_t1 when the timer decreases.
#define PWM_GEN1_B_DT1_NO_CHANGE    0x000000
#define PWM_GEN1_B_DT1_LOW          0x400000
#define PWM_GEN1_B_DT1_HIGH         0x800000
#define PWM_GEN1_B_DT1_TOGGLE       0xc00000
// Action on PWM1B triggered by event_t0 when the timer decreases. (R/W)
#define PWM_GEN1_B_DT0_NO_CHANGE    0x000000
#define PWM_GEN1_B_DT0_LOW          0x100000
#define PWM_GEN1_B_DT0_HIGH         0x200000
#define PWM_GEN1_B_DT0_TOGGLE       0x300000
// Action on PWM1B triggered by event TEB when the timer decreases. (R/W)
#define PWM_GEN1_B_DTEB_NO_CHANGE   0x000000
#define PWM_GEN1_B_DTEB_LOW         0x040000
#define PWM_GEN1_B_DTEB_HIGH        0x080000
#define PWM_GEN1_B_DTEB_TOGGLE      0x0c0000
// Action on PWM1B triggered by event TEA when the timer decreases. (R/W)
#define PWM_GEN1_B_DTEA_NO_CHANGE   0x000000
#define PWM_GEN1_B_DTEA_LOW         0x010000
#define PWM_GEN1_B_DTEA_HIGH        0x020000
#define PWM_GEN1_B_DTEA_TOGGLE      0x030000
// Action on PWM1B triggered by event TEP when the timer decreases. (R/W)
#define PWM_GEN1_B_DTEP_NO_CHANGE   0x000000
#define PWM_GEN1_B_DTEP_LOW         0x004000
#define PWM_GEN1_B_DTEP_HIGH        0x008000
#define PWM_GEN1_B_DTEP_TOGGLE      0x00c000
// Action on PWM1B triggered by event TEZ when the timer decreases. (R/W)
#define PWM_GEN1_B_DTEZ_NO_CHANGE   0x000000
#define PWM_GEN1_B_DTEZ_LOW         0x001000
#define PWM_GEN1_B_DTEZ_HIGH        0x002000
#define PWM_GEN1_B_DTEZ_TOGGLE      0x003000
// Action on PWM1B triggered by event_t1 when the timer increases. (R/W)
#define PWM_GEN1_B_UT1_NO_CHANGE    0x000000
#define PWM_GEN1_B_UT1_LOW          0x000400
#define PWM_GEN1_B_UT1_HIGH         0x000800
#define PWM_GEN1_B_UT1_TOGGLE       0x000c00
// Action on PWM1B triggered by event_t0 when the timer increases. (R/W)
#define PWM_GEN1_B_UT0_NO_CHANGE    0x000000
#define PWM_GEN1_B_UT0_LOW          0x000100
#define PWM_GEN1_B_UT0_HIGH         0x000200
#define PWM_GEN1_B_UT0_TOGGLE       0x000300
// Action on PWM1B triggered by event TEB when the timer increases. (R/W)
#define PWM_GEN1_B_UTEB_NO_CHANGE   0x000000
#define PWM_GEN1_B_UTEB_LOW         0x000040
#define PWM_GEN1_B_UTEB_HIGH        0x000080
#define PWM_GEN1_B_UTEB_TOGGLE      0x0000c0
// Action on PWM1B triggered by event TEA when the timer increases. (R/W)
#define PWM_GEN1_B_UTEA_NO_CHANGE   0x000000
#define PWM_GEN1_B_UTEA_LOW         0x000010
#define PWM_GEN1_B_UTEA_HIGH        0x000020
#define PWM_GEN1_B_UTEA_TOGGLE      0x000030
// Action on PWM1B triggered by event TEP when the timer increases. (R/W)
#define PWM_GEN1_B_UTEP_NO_CHANGE   0x000000
#define PWM_GEN1_B_UTEP_LOW         0x000004
#define PWM_GEN1_B_UTEP_HIGH        0x000008
#define PWM_GEN1_B_UTEP_TOGGLE      0x00000c
// Action on PWM1B triggered by event TEZ when the timer increases. (R/W)
#define PWM_GEN1_B_UTEZ_NO_CHANGE   0x000000
#define PWM_GEN1_B_UTEZ_LOW         0x000001
#define PWM_GEN1_B_UTEZ_HIGH        0x000002
#define PWM_GEN1_B_UTEZ_TOGGLE      0x000003

/* Dead time type selection and configuration */
#define PWM0_DT1_CFG_REG            MMIO32(MCPWM0_BASE + 0x090)
#define PWM1_DT1_CFG_REG            MMIO32(MCPWM1_BASE + 0x090)
// Dead time generator 1 clock selection.
#define PWM_DT1_CLK_SEL                     0x20000
// S0 in Table 16-5. (R/W)
#define PWM_DT1_B_OUTBYPASS                 0x10000
// S1 in Table 16-5. (R/W)
#define PWM_DT1_A_OUTBYPASS                 0x08000
// S3 in Table 16-5. (R/W)
#define PWM_DT1_FED_OUTINVERT               0x04000
// S2 in Table 16-5. (R/W)
#define PWM_DT1_RED_OUTINVERT               0x02000
// S5 in Table 16-5. (R/W)
#define PWM_DT1_FED_INSEL                   0x01000
// S4 in Table 16-5. (R/W)
#define PWM_DT1_RED_INSEL                   0x00800
// S7 in Table 16-5. (R/W)
#define PWM_DT1_B_OUTSWAP                   0x00400
// S6 in Table 16-5. (R/W)
#define PWM_DT1_A_OUTSWAP                   0x00200
// S8 in Table 16-5
#define PWM_DT1_DEB_MODE                    0x00100
// Updating method for RED (rising edge delay) active register.
#define PWM_DT1_RED_UPMETHOD_IMMEDIATELY    0x00000
#define PWM_DT1_RED_UPMETHOD_TEZ            0x00010
#define PWM_DT1_RED_UPMETHOD_TEP            0x00020
#define PWM_DT1_RED_UPMETHOD_SYNC           0x00040
#define PWM_DT1_RED_UPMETHOD_DISABLE        0x00080
// Updating method for FED (falling edge delay) active register.
#define PWM_DT1_FED_UPMETHOD_IMMEDIATELY    0x00000
#define PWM_DT1_FED_UPMETHOD_TEZ            0x00001
#define PWM_DT1_FED_UPMETHOD_TEP            0x00002
#define PWM_DT1_FED_UPMETHOD_SYNC           0x00004
#define PWM_DT1_FED_UPMETHOD_DISABLE        0x00008

/* Shadow register for falling edge delay (FED) */
#define PWM0_DT1_FED_CFG_REG        MMIO32(MCPWM0_BASE + 0x094)
#define PWM1_DT1_FED_CFG_REG        MMIO32(MCPWM1_BASE + 0x094)
// Shadow register for FED. (R/W)
#define PWM_DT1_FED_MSK 0xffff

/* Shadow register for rising edge delay (RED) */
#define PWM0_DT1_RED_CFG_REG        MMIO32(MCPWM0_BASE + 0x098)
#define PWM1_DT1_RED_CFG_REG        MMIO32(MCPWM1_BASE + 0x098)
// Shadow register for RED. (R/W)
#define PWM_DT1_RED_MSK 0xffff

/* Carrier enable and configuration */
#define PWM0_CARRIER1_CFG_REG       MMIO32(MCPWM0_BASE + 0x09c)
#define PWM1_CARRIER1_CFG_REG       MMIO32(MCPWM1_BASE + 0x09c)
// When set, invert the input of PWM1A and PWM1B for this submodule. (R/W)
#define PWM_CARRIER1_IN_INVERT          0x2000
// When set, invert the output of PWM1A and PWM1B for this submodule. (R/W)
#define PWM_CARRIER1_OUT_INVERT         0x1000
// Width of the first pulse in number of periods of the carrier. (R/W)
#define PWM_CARRIER1_OSHWTH_MSK         0x0f00
#define PWM_CARRIER1_OSHWTH_SFT         8
#define PWM_CARRIER1_OSHWTH_SET(n) ((n<<PWM_CARRIER1_OSHWTH_SFT)&PWM_CARRIER1_OSHWTH_MSK)
// Carrier duty selection. Duty = PWM_CARRIER1_DUTY/8. (R/W)
#define PWM_CARRIER1_DUTY_MSK           0x00e0
#define PWM_CARRIER1_DUTY_SFT           5
#define PWM_CARRIER1_DUTY_SET(n) ((n<<PWM_CARRIER1_DUTY_SFT)&PWM_CARRIER1_DUTY_MSK)
// PWM carrier1 clock (PC_clk) prescale value.
#define PWM_CARRIER1_PRESCALE_MSK       0x001e
#define PWM_CARRIER1_PRESCALE_SFT       1
#define PWM_CARRIER1_PRESCALE_SET(n) ((n<<PWM_CARRIER1_PRESCALE_SFT)&PWM_CARRIER1_PRESCALE_MSK)
// When set, carrier1 function is enabled. When cleared, carrier1 is bypassed.
#define PWM_CARRIER1_EN                 0x0001

/* Actions on PWM1A and PWM1B on trip events */
#define PWM0_FH1_CFG0_REG           MMIO32(MCPWM0_BASE + 0x0a0)
#define PWM1_FH1_CFG0_REG           MMIO32(MCPWM1_BASE + 0x0a0)
// One-shot mode action on PWM1B when a fault event occurs and the timer is inc.
#define PWM_FH1_B_OST_U_NO_CHANGE   0x000000
#define PWM_FH1_B_OST_U_LOW         0x400000
#define PWM_FH1_B_OST_U_HIGH        0x800000
#define PWM_FH1_B_OST_U_TOGGLE      0xc00000
// One-shot mode action on PWM1B when a fault event occurs and the timer is decr.
#define PWM_FH1_B_OST_D_NO_CHANGE   0x000000
#define PWM_FH1_B_OST_D_LOW         0x100000
#define PWM_FH1_B_OST_D_HIGH        0x200000
#define PWM_FH1_B_OST_D_TOGGLE      0x300000
// Cycle-by-cycle mode action on PWM1B when a fault ev occurs and the tim is inc.
#define PWM_FH1_B_CBC_U_NO_CHANGE   0x000000
#define PWM_FH1_B_CBC_U_LOW         0x040000
#define PWM_FH1_B_CBC_U_HIGH        0x080000
#define PWM_FH1_B_CBC_U_TOGGLE      0x0c0000
// Cycle-by-cycle mode action on PWM1B when a fault ev occurs and the tim is decr.
#define PWM_FH1_B_CBC_D_NO_CHANGE   0x000000
#define PWM_FH1_B_CBC_D_LOW         0x010000
#define PWM_FH1_B_CBC_D_HIGH        0x020000
#define PWM_FH1_B_CBC_D_TOGGLE      0x030000
// One-shot mode action on PWM1A when a fault event occurs and the tim is inc.
#define PWM_FH1_A_OST_U_NO_CHANGE   0x000000
#define PWM_FH1_A_OST_U_LOW         0x004000
#define PWM_FH1_A_OST_U_HIGH        0x008000
#define PWM_FH1_A_OST_U_TOGGLE      0x00c000
// One-shot mode action on PWM1A when a fault event occurs and the timer is decr
#define PWM_FH1_A_OST_D_NO_CHANGE   0x000000
#define PWM_FH1_A_OST_D_LOW         0x001000
#define PWM_FH1_A_OST_D_HIGH        0x002000
#define PWM_FH1_A_OST_D_TOGGLE      0x003000
// Cycle-by-cycle mode action on PWM1A when a fault ev occurs and the tim is inc.
#define PWM_FH1_A_CBC_U_NO_CHANGE   0x000000
#define PWM_FH1_A_CBC_U_LOW         0x000400
#define PWM_FH1_A_CBC_U_HIGH        0x000800
#define PWM_FH1_A_CBC_U_TOGGLE      0x000c00
// Cycle-by-cycle mode action on PWM1A when a fault ev occurs and the tim is decr.
#define PWM_FH1_A_CBC_D_NO_CHANGE   0x000000
#define PWM_FH1_A_CBC_D_LOW         0x000100
#define PWM_FH1_A_CBC_D_HIGH        0x000200
#define PWM_FH1_A_CBC_D_TOGGLE      0x000300
// event_f0 will trigger one-shot mode action.
#define PWM_FH1_F0_OST              0x000080
// event_f1 will trigger one-shot mode action.
#define PWM_FH1_F1_OST              0x000040
// event_f2 will trigger one-shot mode action.
#define PWM_FH1_F2_OST              0x000020
// Enable register for software-forced one-shot mode action.
#define PWM_FH1_SW_OST              0x000010
// event_f0 will trigger cycle-by-cycle mode action.
#define PWM_FH1_F0_CBC              0x000008
// event_f1 will trigger cycle-by-cycle mode action.
#define PWM_FH1_F1_CBC              0x000004
// event_f2 will trigger cycle-by-cycle mode action.
#define PWM_FH1_F2_CBC              0x000002
// Enable register for software-forced cycle-by-cycle mode action.
#define PWM_FH1_SW_CBC              0x000001

/* Software triggers for fault handler actions */
#define PWM0_FH1_CFG1_REG           MMIO32(MCPWM0_BASE + 0x0a4)
#define PWM1_FH1_CFG1_REG           MMIO32(MCPWM1_BASE + 0x0a4)
// A toggle (software negation of this bit’s val) triggers a one-shot mode action.
#define PWM_FH1_FORCE_OST       0x10
// A toggle triggers a cycle-by-cycle mode action. (R/W)
#define PWM_FH1_FORCE_CBC       0x08
// The cycle-by-cycle mode action refresh moment selection.
#define PWM_FH1_CBCPULSE_TEZ    0x02
#define PWM_FH1_CBCPULSE_TEP    0x04
// A toggle will clear on-going one-shot mode action. (R/W)
#define PWM_FH1_CLR_OST         0x01

/* Status of fault events */
#define PWM0_FH1_STATUS_REG         MMIO32(MCPWM0_BASE + 0x0a8)
#define PWM1_FH1_STATUS_REG         MMIO32(MCPWM1_BASE + 0x0a8)
// Set and reset by hardware. If set, a one-shot mode action is on-going. (RO)
#define PWM_FH1_OST_ON  0x2
// Set and reset by hardware. If set, a cycle-by-cycle mode action is on-going.
#define PWM_FH1_CBC_ON  0x1

/* Transfer status and update method for time stamp registers A and B */
#define PWM0_GEN2_STMP_CFG_REG      MMIO32(MCPWM0_BASE + 0x0ac)
#define PWM1_GEN2_STMP_CFG_REG      MMIO32(MCPWM1_BASE + 0x0ac)
// PWM generator 2 time stamp B’s shadow reg is fill and to be trans to time stamp
#define PWM_GEN2_B_SHDW_FULL        0x200
// PWM generator 2 time stamp A’s shadow reg is fill and to be trans to time stamp
#define PWM_GEN2_A_SHDW_FULL        0x100
// Updating method for PWM generator 2 time stamp B’s active register.
#define PWM_GEN2_B_UPMETHOD_TEZ     0x010
#define PWM_GEN2_B_UPMETHOD_TEP     0x020
#define PWM_GEN2_B_UPMETHOD_SYNC    0x040
#define PWM_GEN2_B_UPMETHOD_DIS_UPD 0x080
// Updating method for PWM generator 2 time stamp A’s active register.
#define PWM_GEN2_A_UPMETHOD_TEZ     0x001
#define PWM_GEN2_A_UPMETHOD_TEP     0x002
#define PWM_GEN2_A_UPMETHOD_SYNC    0x004
#define PWM_GEN2_A_UPMETHOD_DIS_UPD 0x008

/* Shadow register for register A */
#define PWM0_GEN2_TSTMP_A_REG       MMIO32(MCPWM0_BASE + 0x0b0)
#define PWM1_GEN2_TSTMP_A_REG       MMIO32(MCPWM1_BASE + 0x0b0)
// PWM generator 2 time stamp A’s shadow register. (R/W)
#define PWM_GEN2_A_MSK  0xffff

/* Shadow register for register B */
#define PWM0_GEN2_TSTMP_B_REG       MMIO32(MCPWM0_BASE + 0x0b4)
#define PWM1_GEN2_TSTMP_B_REG       MMIO32(MCPWM1_BASE + 0x0b4)
// PWM generator 2 time stamp B’s shadow register. (R/W)
#define PWM_GEN2_B_MSK  0xffff

//TODO: check the error in datasheet with register address
/* Fault event T0 and T1 handling */
#define PWM0_GEN2_CFG0_REG          MMIO32(MCPWM0_BASE + 0x0b8)
#define PWM1_GEN2_CFG0_REG          MMIO32(MCPWM1_BASE + 0x0b8)
// Source selection for PWM generator 2 event_t1, taking effect immediately.
#define PWM_GEN2_T1_SEL_FAULT_EVENT0    0x000
#define PWM_GEN2_T1_SEL_FAULT_EVENT1    0x080
#define PWM_GEN2_T1_SEL_FAULT_EVENT2    0x100
#define PWM_GEN2_T1_SEL_SYNC_TAKEN      0x180
// Source selection for PWM generator 2 event_t0, taking effect immediately,
#define PWM_GEN2_T0_SEL_FAULT_EVENT0    0x000
#define PWM_GEN2_T0_SEL_FAULT_EVENT1    0x010
#define PWM_GEN2_T0_SEL_FAULT_EVENT2    0x020
#define PWM_GEN2_T0_SEL_SYNC_TAKEN      0x030
// Updating method for PWM generator 2’s active register of configuration.
#define PWM_GEN2_CFG_UPMETHOD_TEZ       0x001
#define PWM_GEN2_CFG_UPMETHOD_TEP       0x002
#define PWM_GEN2_CFG_UPMETHOD_SYNC      0x004
#define PWM_GEN2_CFG_UPMETHOD_DIS_UPD   0x008

/* Permissives to force PWM0A and PWM0B outputs by software */
#define PWM0_GEN2_FORCE_REG         MMIO32(MCPWM0_BASE + 0x0bc)
#define PWM1_GEN2_FORCE_REG         MMIO32(MCPWM1_BASE + 0x0bc)
// Non-continuous immediate software-force mode for PWM2B.
#define PWM_GEN2_B_NCIFORCE_MODE_DIS            0x0000
#define PWM_GEN2_B_NCIFORCE_MODE_LOW            0x4000
#define PWM_GEN2_B_NCIFORCE_MODE_HIGH           0x8000
// Trigger of non-continuous immediate software-force event for PWM2B;
#define PWM_GEN2_B_NCIFORCE                     0x2000
// Non-continuous immediate software-force mode for PWM1A,
#define PWM_GEN2_A_NCIFORCE_MODE_DIS            0x0000
#define PWM_GEN2_A_NCIFORCE_MODE_LOW            0x0800
#define PWM_GEN2_A_NCIFORCE_MODE_HIGH           0x1000
// Trigger of non-continuous immediate software-force event for PWM2A;
#define PWM_GEN2_A_NCIFORCE                     0x0400
// Continuous software-force mode for PWM2B.
#define PWM_GEN2_B_CNTUFORCE_MODE_DIS           0x0000
#define PWM_GEN2_B_CNTUFORCE_MODE_LOW           0x0100
#define PWM_GEN2_B_CNTUFORCE_MODE_HIGH          0x0200
// Continuous software-force mode for PWM2A.
#define PWM_GEN2_A_CNTUFORCE_MODE_DIS           0x0000
#define PWM_GEN2_A_CNTUFORCE_MODE_LOW           0x0040
#define PWM_GEN2_A_CNTUFORCE_MODE_HIGH          0x0080
// Updating method for continuous software force of PWM generator2.
#define PWM_GEN2_CNTUFORCE_UPMETHOD_IMMIDEATELY 0x0000
#define PWM_GEN2_CNTUFORCE_UPMETHOD_AT_TEZ      0x0001
#define PWM_GEN2_CNTUFORCE_UPMETHOD_AT_TEP      0x0002
#define PWM_GEN2_CNTUFORCE_UPMETHOD_AT_TEA      0x0004
#define PWM_GEN2_CNTUFORCE_UPMETHOD_AT_TEB      0x0008
#define PWM_GEN2_CNTUFORCE_UPMETHOD_SYNC        0x0010
#define PWM_GEN2_CNTUFORCE_UPMETHOD_DISABLE     0x0020

/* Actions triggered by events on PWM0A */
#define PWM0_GEN2_A_REG             MMIO32(MCPWM0_BASE + 0x0c0)
#define PWM1_GEN2_A_REG             MMIO32(MCPWM1_BASE + 0x0c0)
// Action on PWM2A triggered by event_t1 when the timer decreases.
#define PWM_GEN2_A_DT1_NO_CHANGE    0x000000
#define PWM_GEN2_A_DT1_LOW          0x400000
#define PWM_GEN2_A_DT1_HIGH         0x800000
#define PWM_GEN2_A_DT1_TOGGLE       0xc00000
// Action on PWM2A triggered by event_t0 when the timer decreases. (R/W)
#define PWM_GEN2_A_DT0_NO_CHANGE    0x000000
#define PWM_GEN2_A_DT0_LOW          0x100000
#define PWM_GEN2_A_DT0_HIGH         0x200000
#define PWM_GEN2_A_DT0_TOGGLE       0x300000
// Action on PWM2A triggered by event TEB when the timer decreases. (R/W)
#define PWM_GEN2_A_DTEB_NO_CHANGE   0x000000
#define PWM_GEN2_A_DTEB_LOW         0x040000
#define PWM_GEN2_A_DTEB_HIGH        0x080000
#define PWM_GEN2_A_DTEB_TOGGLE      0x0c0000
// Action on PWM2A triggered by event TEA when the timer decreases. (R/W)
#define PWM_GEN2_A_DTEA_NO_CHANGE   0x000000
#define PWM_GEN2_A_DTEA_LOW         0x010000
#define PWM_GEN2_A_DTEA_HIGH        0x020000
#define PWM_GEN2_A_DTEA_TOGGLE      0x030000
// Action on PWM2A triggered by event TEP when the timer decreases. (R/W)
#define PWM_GEN2_A_DTEP_NO_CHANGE   0x000000
#define PWM_GEN2_A_DTEP_LOW         0x004000
#define PWM_GEN2_A_DTEP_HIGH        0x008000
#define PWM_GEN2_A_DTEP_TOGGLE      0x00c000
// Action on PWM2A triggered by event TEZ when the timer decreases. (R/W)
#define PWM_GEN2_A_DTEZ_NO_CHANGE   0x000000
#define PWM_GEN2_A_DTEZ_LOW         0x001000
#define PWM_GEN2_A_DTEZ_HIGH        0x002000
#define PWM_GEN2_A_DTEZ_TOGGLE      0x003000
// Action on PWM2A triggered by event_t1 when the timer increases. (R/W)
#define PWM_GEN2_A_UT1_NO_CHANGE    0x000000
#define PWM_GEN2_A_UT1_LOW          0x000400
#define PWM_GEN2_A_UT1_HIGH         0x000800
#define PWM_GEN2_A_UT1_TOGGLE       0x000c00
// Action on PWM2A triggered by event_t0 when the timer increases. (R/W)
#define PWM_GEN2_A_UT0_NO_CHANGE    0x000000
#define PWM_GEN2_A_UT0_LOW          0x000100
#define PWM_GEN2_A_UT0_HIGH         0x000200
#define PWM_GEN2_A_UT0_TOGGLE       0x000300
// Action on PWM2A triggered by event TEB when the timer increases. (R/W)
#define PWM_GEN2_A_UTEB_NO_CHANGE   0x000000
#define PWM_GEN2_A_UTEB_LOW         0x000040
#define PWM_GEN2_A_UTEB_HIGH        0x000080
#define PWM_GEN2_A_UTEB_TOGGLE      0x0000c0
// Action on PWM2A triggered by event TEA when the timer increases. (R/W)
#define PWM_GEN2_A_UTEA_NO_CHANGE   0x000000
#define PWM_GEN2_A_UTEA_LOW         0x000010
#define PWM_GEN2_A_UTEA_HIGH        0x000020
#define PWM_GEN2_A_UTEA_TOGGLE      0x000030
// Action on PWM2A triggered by event TEP when the timer increases. (R/W)
#define PWM_GEN2_A_UTEP_NO_CHANGE   0x000000
#define PWM_GEN2_A_UTEP_LOW         0x000004
#define PWM_GEN2_A_UTEP_HIGH        0x000008
#define PWM_GEN2_A_UTEP_TOGGLE      0x00000c
// Action on PWM2A triggered by event TEZ when the timer increases. (R/W)
#define PWM_GEN2_A_UTEZ_NO_CHANGE   0x000000
#define PWM_GEN2_A_UTEZ_LOW         0x000001
#define PWM_GEN2_A_UTEZ_HIGH        0x000002
#define PWM_GEN2_A_UTEZ_TOGGLE      0x000003

/* Actions triggered by events on PWM0B */
#define PWM0_GEN2_B_REG             MMIO32(MCPWM0_BASE + 0x0c4)
#define PWM1_GEN2_B_REG             MMIO32(MCPWM1_BASE + 0x0c4)
// Action on PWM2B triggered by event_t1 when the timer decreases.
#define PWM_GEN2_B_DT1_NO_CHANGE    0x000000
#define PWM_GEN2_B_DT1_LOW          0x400000
#define PWM_GEN2_B_DT1_HIGH         0x800000
#define PWM_GEN2_B_DT1_TOGGLE       0xc00000
// Action on PWM2B triggered by event_t0 when the timer decreases. (R/W)
#define PWM_GEN2_B_DT0_NO_CHANGE    0x000000
#define PWM_GEN2_B_DT0_LOW          0x100000
#define PWM_GEN2_B_DT0_HIGH         0x200000
#define PWM_GEN2_B_DT0_TOGGLE       0x300000
// Action on PWM2B triggered by event TEB when the timer decreases. (R/W)
#define PWM_GEN2_B_DTEB_NO_CHANGE   0x000000
#define PWM_GEN2_B_DTEB_LOW         0x040000
#define PWM_GEN2_B_DTEB_HIGH        0x080000
#define PWM_GEN2_B_DTEB_TOGGLE      0x0c0000
// Action on PWM2B triggered by event TEA when the timer decreases. (R/W)
#define PWM_GEN2_B_DTEA_NO_CHANGE   0x000000
#define PWM_GEN2_B_DTEA_LOW         0x010000
#define PWM_GEN2_B_DTEA_HIGH        0x020000
#define PWM_GEN2_B_DTEA_TOGGLE      0x030000
// Action on PWM2B triggered by event TEP when the timer decreases. (R/W)
#define PWM_GEN2_B_DTEP_NO_CHANGE   0x000000
#define PWM_GEN2_B_DTEP_LOW         0x004000
#define PWM_GEN2_B_DTEP_HIGH        0x008000
#define PWM_GEN2_B_DTEP_TOGGLE      0x00c000
// Action on PWM2B triggered by event TEZ when the timer decreases. (R/W)
#define PWM_GEN2_B_DTEZ_NO_CHANGE   0x000000
#define PWM_GEN2_B_DTEZ_LOW         0x001000
#define PWM_GEN2_B_DTEZ_HIGH        0x002000
#define PWM_GEN2_B_DTEZ_TOGGLE      0x003000
// Action on PWM2B triggered by event_t1 when the timer increases. (R/W)
#define PWM_GEN2_B_UT1_NO_CHANGE    0x000000
#define PWM_GEN2_B_UT1_LOW          0x000400
#define PWM_GEN2_B_UT1_HIGH         0x000800
#define PWM_GEN2_B_UT1_TOGGLE       0x000c00
// Action on PWM2B triggered by event_t0 when the timer increases. (R/W)
#define PWM_GEN2_B_UT0_NO_CHANGE    0x000000
#define PWM_GEN2_B_UT0_LOW          0x000100
#define PWM_GEN2_B_UT0_HIGH         0x000200
#define PWM_GEN2_B_UT0_TOGGLE       0x000300
// Action on PWM2B triggered by event TEB when the timer increases. (R/W)
#define PWM_GEN2_B_UTEB_NO_CHANGE   0x000000
#define PWM_GEN2_B_UTEB_LOW         0x000040
#define PWM_GEN2_B_UTEB_HIGH        0x000080
#define PWM_GEN2_B_UTEB_TOGGLE      0x0000c0
// Action on PWM2B triggered by event TEA when the timer increases. (R/W)
#define PWM_GEN2_B_UTEA_NO_CHANGE   0x000000
#define PWM_GEN2_B_UTEA_LOW         0x000010
#define PWM_GEN2_B_UTEA_HIGH        0x000020
#define PWM_GEN2_B_UTEA_TOGGLE      0x000030
// Action on PWM2B triggered by event TEP when the timer increases. (R/W)
#define PWM_GEN2_B_UTEP_NO_CHANGE   0x000000
#define PWM_GEN2_B_UTEP_LOW         0x000004
#define PWM_GEN2_B_UTEP_HIGH        0x000008
#define PWM_GEN2_B_UTEP_TOGGLE      0x00000c
// Action on PWM2B triggered by event TEZ when the timer increases. (R/W)
#define PWM_GEN2_B_UTEZ_NO_CHANGE   0x000000
#define PWM_GEN2_B_UTEZ_LOW         0x000001
#define PWM_GEN2_B_UTEZ_HIGH        0x000002
#define PWM_GEN2_B_UTEZ_TOGGLE      0x000003

/* Dead time type selection and configuration */
#define PWM0_DT2_CFG_REG            MMIO32(MCPWM0_BASE + 0x0c8)
#define PWM1_DT2_CFG_REG            MMIO32(MCPWM1_BASE + 0x0c8)
// Dead time generator 2 clock selection.
#define PWM_DT2_CLK_SEL                     0x20000
// S0 in Table 16-5. (R/W)
#define PWM_DT2_B_OUTBYPASS                 0x10000
// S1 in Table 16-5. (R/W)
#define PWM_DT2_A_OUTBYPASS                 0x08000
// S3 in Table 16-5. (R/W)
#define PWM_DT2_FED_OUTINVERT               0x04000
// S2 in Table 16-5. (R/W)
#define PWM_DT2_RED_OUTINVERT               0x02000
// S5 in Table 16-5. (R/W)
#define PWM_DT2_FED_INSEL                   0x01000
// S4 in Table 16-5. (R/W)
#define PWM_DT2_RED_INSEL                   0x00800
// S7 in Table 16-5. (R/W)
#define PWM_DT2_B_OUTSWAP                   0x00400
// S6 in Table 16-5. (R/W)
#define PWM_DT2_A_OUTSWAP                   0x00200
// S8 in Table 16-5
#define PWM_DT2_DEB_MODE                    0x00100
// Updating method for RED (rising edge delay) active register.
#define PWM_DT2_RED_UPMETHOD_IMMEDIATELY    0x00000
#define PWM_DT2_RED_UPMETHOD_TEZ            0x00010
#define PWM_DT2_RED_UPMETHOD_TEP            0x00020
#define PWM_DT2_RED_UPMETHOD_SYNC           0x00040
#define PWM_DT2_RED_UPMETHOD_DISABLE        0x00080
// Updating method for FED (falling edge delay) active register.
#define PWM_DT2_FED_UPMETHOD_IMMEDIATELY    0x00000
#define PWM_DT2_FED_UPMETHOD_TEZ            0x00001
#define PWM_DT2_FED_UPMETHOD_TEP            0x00002
#define PWM_DT2_FED_UPMETHOD_SYNC           0x00004
#define PWM_DT2_FED_UPMETHOD_DISABLE        0x00008

/* Shadow register for falling edge delay (FED) */
#define PWM0_DT2_FED_CFG_REG        MMIO32(MCPWM0_BASE + 0x0cc)
#define PWM1_DT2_FED_CFG_REG        MMIO32(MCPWM1_BASE + 0x0cc)
// Shadow register for FED. (R/W)
#define PWM_DT2_FED_MSK 0xffff

/* Shadow register for rising edge delay (RED) */
#define PWM0_DT2_RED_CFG_REG        MMIO32(MCPWM0_BASE + 0x0d0)
#define PWM1_DT2_RED_CFG_REG        MMIO32(MCPWM1_BASE + 0x0d0)
// Shadow register for RED. (R/W)
#define PWM_DT2_RED_MSK 0xffff

/* Carrier enable and configuration */
#define PWM0_CARRIER2_CFG_REG       MMIO32(MCPWM0_BASE + 0x0d4)
#define PWM1_CARRIER2_CFG_REG       MMIO32(MCPWM1_BASE + 0x0d4)
// When set, invert the input of PWM2A and PWM2B for this submodule. (R/W)
#define PWM_CARRIER2_IN_INVERT          0x2000
// When set, invert the output of PWM2A and PWM2B for this submodule. (R/W)
#define PWM_CARRIER2_OUT_INVERT         0x1000
// Width of the first pulse in number of periods of the carrier. (R/W)
#define PWM_CARRIER2_OSHWTH_MSK         0x0f00
#define PWM_CARRIER2_OSHWTH_SFT         8
#define PWM_CARRIER2_OSHWTH_SET(n) ((n<<PWM_CARRIER2_OSHWTH_SFT)&PWM_CARRIER2_OSHWTH_MSK)
// Carrier duty selection. Duty = PWM_CARRIER2_DUTY/8. (R/W)
#define PWM_CARRIER2_DUTY_MSK           0x00e0
#define PWM_CARRIER2_DUTY_SFT           5
#define PWM_CARRIER2_DUTY_SET(n) ((n<<PWM_CARRIER2_DUTY_SFT)&PWM_CARRIER2_DUTY_MSK)
// PWM carrier2 clock (PC_clk) prescale value.
#define PWM_CARRIER2_PRESCALE_MSK       0x001e
#define PWM_CARRIER2_PRESCALE_SFT       1
#define PWM_CARRIER2_PRESCALE_SET(n) ((n<<PWM_CARRIER2_PRESCALE_SFT)&PWM_CARRIER2_PRESCALE_MSK)
// When set, carrier2 function is enabled. When cleared, carrier2 is bypassed.
#define PWM_CARRIER2_EN  0x0001

/* Actions on PWM2A and PWM2B on trip events */
#define PWM0_FH2_CFG0_REG           MMIO32(MCPWM0_BASE + 0x0d8)
#define PWM1_FH2_CFG0_REG           MMIO32(MCPWM1_BASE + 0x0d8)
// One-shot mode action on PWM2B when a fault event occurs and the timer is inc.
#define PWM_FH2_B_OST_U_NO_CHANGE   0x000000
#define PWM_FH2_B_OST_U_LOW         0x400000
#define PWM_FH2_B_OST_U_HIGH        0x800000
#define PWM_FH2_B_OST_U_TOGGLE      0xc00000
// One-shot mode action on PWM2B when a fault event occurs and the timer is decr.
#define PWM_FH2_B_OST_D_NO_CHANGE   0x000000
#define PWM_FH2_B_OST_D_LOW         0x100000
#define PWM_FH2_B_OST_D_HIGH        0x200000
#define PWM_FH2_B_OST_D_TOGGLE      0x300000
// Cycle-by-cycle mode action on PWM2B when a fault ev occurs and the tim is inc.
#define PWM_FH2_B_CBC_U_NO_CHANGE   0x000000
#define PWM_FH2_B_CBC_U_LOW         0x040000
#define PWM_FH2_B_CBC_U_HIGH        0x080000
#define PWM_FH2_B_CBC_U_TOGGLE      0x0c0000
// Cycle-by-cycle mode action on PWM2B when a fault ev occurs and the tim is decr.
#define PWM_FH2_B_CBC_D_NO_CHANGE   0x000000
#define PWM_FH2_B_CBC_D_LOW         0x010000
#define PWM_FH2_B_CBC_D_HIGH        0x020000
#define PWM_FH2_B_CBC_D_TOGGLE      0x030000
// One-shot mode action on PWM2A when a fault event occurs and the tim is inc.
#define PWM_FH2_A_OST_U_NO_CHANGE   0x000000
#define PWM_FH2_A_OST_U_LOW         0x004000
#define PWM_FH2_A_OST_U_HIGH        0x008000
#define PWM_FH2_A_OST_U_TOGGLE      0x00c000
// One-shot mode action on PWM2A when a fault event occurs and the timer is decr
#define PWM_FH2_A_OST_D_NO_CHANGE   0x000000
#define PWM_FH2_A_OST_D_LOW         0x001000
#define PWM_FH2_A_OST_D_HIGH        0x002000
#define PWM_FH2_A_OST_D_TOGGLE      0x003000
// Cycle-by-cycle mode action on PWM2A when a fault ev occurs and the tim is inc.
#define PWM_FH2_A_CBC_U_NO_CHANGE   0x000000
#define PWM_FH2_A_CBC_U_LOW         0x000400
#define PWM_FH2_A_CBC_U_HIGH        0x000800
#define PWM_FH2_A_CBC_U_TOGGLE      0x000c00
// Cycle-by-cycle mode action on PWM2A when a fault ev occurs and the tim is decr.
#define PWM_FH2_A_CBC_D_NO_CHANGE   0x000000
#define PWM_FH2_A_CBC_D_LOW         0x000100
#define PWM_FH2_A_CBC_D_HIGH        0x000200
#define PWM_FH2_A_CBC_D_TOGGLE      0x000300
// event_f0 will trigger one-shot mode action.
#define PWM_FH2_F0_OST              0x000080
// event_f1 will trigger one-shot mode action.
#define PWM_FH2_F1_OST              0x000040
// event_f2 will trigger one-shot mode action.
#define PWM_FH2_F2_OST              0x000020
// Enable register for software-forced one-shot mode action.
#define PWM_FH2_SW_OST              0x000010
// event_f0 will trigger cycle-by-cycle mode action.
#define PWM_FH2_F0_CBC              0x000008
// event_f1 will trigger cycle-by-cycle mode action.
#define PWM_FH2_F1_CBC              0x000004
// event_f2 will trigger cycle-by-cycle mode action.
#define PWM_FH2_F2_CBC              0x000002
// Enable register for software-forced cycle-by-cycle mode action.
#define PWM_FH2_SW_CBC              0x000001

/* Software triggers for fault handler actions */
#define PWM0_FH2_CFG1_REG           MMIO32(MCPWM0_BASE + 0x0dc)
#define PWM1_FH2_CFG1_REG           MMIO32(MCPWM1_BASE + 0x0dc)
// A toggle (software negation of this bit’s val) triggers a one-shot mode action.
#define PWM_FH2_FORCE_OST       0x10
// A toggle triggers a cycle-by-cycle mode action. (R/W)
#define PWM_FH2_FORCE_CBC       0x08
// The cycle-by-cycle mode action refresh moment selection.
#define PWM_FH2_CBCPULSE_TEZ    0x02
#define PWM_FH2_CBCPULSE_TEP    0x04
// A toggle will clear on-going one-shot mode action. (R/W)
#define PWM_FH2_CLR_OST         0x01

/* Status of fault events */
#define PWM0_FH2_STATUS_REG         MMIO32(MCPWM0_BASE + 0x0e0)
#define PWM1_FH2_STATUS_REG         MMIO32(MCPWM1_BASE + 0x0e0)
// Set and reset by hardware. If set, a one-shot mode action is on-going. (RO)
#define PWM_FH2_OST_ON  0x2
// Set and reset by hardware. If set, a cycle-by-cycle mode action is on-going.
#define PWM_FH2_CBC_ON  0x1

/* Fault detection configuration and status */
#define PWM0_FAULT_DETECT_REG       MMIO32(MCPWM0_BASE + 0x0e4)
#define PWM1_FAULT_DETECT_REG       MMIO32(MCPWM1_BASE + 0x0e4)
// Set and reset by hardware. If set, event_f2 is on-going. (RO)
#define PWM_EVENT_F2    0x100
// Set and reset by hardware. If set, event_f1 is on-going. (RO)
#define PWM_EVENT_F1    0x080
// Set and reset by hardware. If set, event_f0 is on-going. (RO)
#define PWM_EVENT_F0    0x040
// Set event_f2 trigger polarity on FAULT2 source from GPIO matrix.
#define PWM_F2_POLE     0x020
// Set event_f1 trigger polarity on FAULT2 source from GPIO matrix.
#define PWM_F1_POLE     0x010
// Set event_f0 trigger polarity on FAULT2 source from GPIO matrix.
#define PWM_F0_POLE     0x008
// Set to enable the generation of event_f2. (R/W)
#define PWM_F2_EN       0x004
// Set to enable the generation of event_f1. (R/W)
#define PWM_F1_EN       0x002
// Set to enable the generation of event_f0. (R/W)
#define PWM_F0_EN       0x001

/* Configure capture timer */
#define PWM0_CAP_TIMER_CFG_REG      MMIO32(MCPWM0_BASE + 0x0e8)
#define PWM1_CAP_TIMER_CFG_REG      MMIO32(MCPWM1_BASE + 0x0e8)
// Set this bit to force a capture timer sync;
#define PWM_CAP_SYNC_SW         0x20
// Capture module sync input selection.
#define PWM_CAP_SYNCI_SEL_NONE  0x00
#define PWM_CAP_SYNCI_SEL_TIM0  0x04
#define PWM_CAP_SYNCI_SEL_TIM1  0x08
#define PWM_CAP_SYNCI_SEL_TIM2  0x0c
#define PWM_CAP_SYNCI_SEL_SYNC0 0x10
#define PWM_CAP_SYNCI_SEL_SYNC1 0x14
#define PWM_CAP_SYNCI_SEL_SYNC2 0x18
// When set, the capture timer sync is enabled. (R/W)
#define PWM_CAP_SYNCI_EN        0x02
// When set, the capture timer incrementing under APB_clk is enabled. (R/W)
#define PWM_CAP_TIMER_EN        0x01

/* Phase for capture timer sync */
#define PWM0_CAP_TIMER_PHASE_REG    MMIO32(MCPWM0_BASE + 0x0ec)
#define PWM1_CAP_TIMER_PHASE_REG    MMIO32(MCPWM1_BASE + 0x0ec)
// Phase value for the capture timer sync operation. (R/W)

/* Capture channel 0 configuration and */
#define PWM0_CAP_CH0_CFG_REG        MMIO32(MCPWM0_BASE + 0x0f0)
#define PWM1_CAP_CH0_CFG_REG        MMIO32(MCPWM1_BASE + 0x0f0)
// When set, a software-forced capture on channel 0 is triggered. (WO)
#define PWM_CAP0_SW                 0x1000
// When set, CAP0 form GPIO matrix is inverted before prescaling. (R/W)
#define PWM_CAP0_IN_INVERT          0x0800
// Prescaling value on the positive edge of CAP0.
#define PWM_CAP0_PRESCALE_MSK       0x07f8
#define PWM_CAP0_PRESCALE_SFT       3
#define PWM_CAP0_PRESCALE_SET(n) ((n<<PWM_CAP0_PRESCALE_SFT)&PWM_CAP0_PRESCALE_MSK)
// Edge of capture on channel 0 after prescaling.
#define PWM_CAP0_MODE_NEG           0x0002
#define PWM_CAP0_MODE_POS           0x0004
// When set, capture on channel 0 is enabled. (R/W)
#define PWM_CAP0_EN                 0x0001

/* Capture channel 0 configuration and */
#define PWM0_CAP_CH1_CFG_REG        MMIO32(MCPWM0_BASE + 0x0f4)
#define PWM1_CAP_CH1_CFG_REG        MMIO32(MCPWM1_BASE + 0x0f4)
// When set, a software-forced capture on channel 1 is triggered. (WO)
#define PWM_CAP1_SW                 0x1000
// When set, CAP1 form GPIO matrix is inverted before prescaling. (R/W)
#define PWM_CAP1_IN_INVERT          0x0800
// Prescaling value on the positive edge of CAP1.
#define PWM_CAP1_PRESCALE_MSK       0x07f8
#define PWM_CAP1_PRESCALE_SFT       3
#define PWM_CAP1_PRESCALE_SET(n) ((n<<PWM_CAP1_PRESCALE_SFT)&PWM_CAP1_PRESCALE_MSK)
// Edge of capture on channel 1 after prescaling.
#define PWM_CAP1_MODE_NEG           0x0002
#define PWM_CAP1_MODE_POS           0x0004
// When set, capture on channel 1 is enabled. (R/W)
#define PWM_CAP1_EN                 0x0001

/* Capture channel 0 configuration and */
#define PWM0_CAP_CH2_CFG_REG        MMIO32(MCPWM0_BASE + 0x0f8)
#define PWM1_CAP_CH2_CFG_REG        MMIO32(MCPWM1_BASE + 0x0f8)
// When set, a software-forced capture on channel 2 is triggered. (WO)
#define PWM_CAP2_SW                 0x1000
// When set, CAP2 form GPIO matrix is inverted before prescaling. (R/W)
#define PWM_CAP2_IN_INVERT          0x0800
// Prescaling value on the positive edge of CAP2.
#define PWM_CAP2_PRESCALE_MSK       0x07f8
#define PWM_CAP2_PRESCALE_SFT       3
#define PWM_CAP2_PRESCALE_SET(n) ((n<<PWM_CAP2_PRESCALE_SFT)&PWM_CAP2_PRESCALE_MSK)
// Edge of capture on channel 2 after prescaling.
#define PWM_CAP2_MODE_NEG           0x0002
#define PWM_CAP2_MODE_POS           0x0004
// When set, capture on channel 2 is enabled. (R/W)
#define PWM_CAP2_EN                 0x0001

/* Value of last capture on channel 0 */
#define PWM0_CAP_CH0_REG            MMIO32(MCPWM0_BASE + 0x0fc)
#define PWM1_CAP_CH0_REG            MMIO32(MCPWM1_BASE + 0x0fc)
// Value of the last capture on channel 0. (RO)

/* Value of last capture on channel 0 */
#define PWM0_CAP_CH1_REG            MMIO32(MCPWM0_BASE + 0x100)
#define PWM1_CAP_CH1_REG            MMIO32(MCPWM1_BASE + 0x100)
// Value of the last capture on channel 1. (RO)

/* Value of last capture on channel 0 */
#define PWM0_CAP_CH2_REG            MMIO32(MCPWM0_BASE + 0x104)
#define PWM1_CAP_CH2_REG            MMIO32(MCPWM1_BASE + 0x104)
// Value of the last capture on channel 2. (RO)

/* Edge of last capture trigger */
#define PWM0_CAP_STATUS_REG         MMIO32(MCPWM0_BASE + 0x108)
#define PWM1_CAP_STATUS_REG         MMIO32(MCPWM1_BASE + 0x108)
// Edge of the last capture trigger on channel 2. 0: posedge; 1: negedge. (RO)
#define PWM_CAP2_EDGE   0x4
// Edge of the last capture trigger on channel 1. 0: posedge; 1: negedge. (RO)
#define PWM_CAP1_EDGE   0x2
// Edge of the last capture trigger on channel 0. 0: posedge; 1: negedge. (RO)
#define PWM_CAP0_EDGE   0x1

/* Enable update */
#define PWM0_UPDATE_CFG_REG         MMIO32(MCPWM0_BASE + 0x10c)
#define PWM1_UPDATE_CFG_REG         MMIO32(MCPWM1_BASE + 0x10c)
// A toggle will trigger a forced update of active registers in PWM operator 2.
#define PWM_OP2_FORCE_UP    0x80
// When set and PWM_GLOBAL_UP_EN is set, upd of active regs in PWM op 2 are en
#define PWM_OP2_UP_EN       0x40
// A toggle will trigger a forced update of active registers in PWM operator 1.
#define PWM_OP1_FORCE_UP    0x20
// When set and PWM_GLOBAL_UP_EN is set, upd of active regs in PWM op 1 are en
#define PWM_OP1_UP_EN       0x10
// A toggle will trigger a forced update of active registers in PWM operator 0
#define PWM_OP0_FORCE_UP    0x08
// When set and PWM_GLOBAL_UP_EN is set, upd of active regs in PWM op 0 are en.
#define PWM_OP0_UP_EN       0x04
// A toggle will trigger a forced update of all active regs in the MCPWM module.
#define PWM_GLOBAL_FORCE_UP 0x02
// The global enable of update of all active registers in the MCPWM module.
#define PWM_GLOBAL_UP_EN    0x01

/* Interrupt enable bits */
#define INT0_ENA_PWM_REG            MMIO32(MCPWM0_BASE + 0x110)
#define INT1_ENA_PWM_REG            MMIO32(MCPWM1_BASE + 0x110)
// The enable bit for the interrupt triggered by capture on channel 2. (R/W)
#define INT_CAP2_INT_ENA        0x20000000
// The enable bit for the interrupt triggered by capture on channel 1. (R/W)
#define INT_CAP1_INT_ENA        0x10000000
// The enable bit for the interrupt triggered by capture on channel 0. (R/W)
#define INT_CAP0_INT_ENA        0x08000000
// The enable bit for the interrupt triggered by a one-shot mode action on PWM2.
#define INT_FH2_OST_INT_ENA     0x04000000
// The enable bit for the interrupt triggered by a one-shot mode action on PWM1.
#define INT_FH1_OST_INT_ENA     0x02000000
// The enable bit for the interrupt triggered by a one-shot mode action on PWM0.
#define INT_FH0_OST_INT_ENA     0x01000000
// The enable bit for the int triggered by a cycle-by-cycle mode action on PWM2.
#define INT_FH2_CBC_INT_ENA     0x00800000
// The enable bit for the int triggered by a cycle-by-cycle mode action on PWM1.
#define INT_FH1_CBC_INT_ENA     0x00400000
// The enable bit for the int triggered by a cycle-by-cycle mode action on PWM0.
#define INT_FH0_CBC_INT_ENA     0x00200000
// The enable bit for the interrupt triggered by a PWM operator 2 TEB event (R/W)
#define INT_OP2_TEB_INT_ENA     0x00100000
// The enable bit for the interrupt triggered by a PWM operator 1 TEB event (R/W)
#define INT_OP1_TEB_INT_ENA     0x00080000
// The enable bit for the interrupt triggered by a PWM operator 0 TEB event (R/W)
#define INT_OP0_TEB_INT_ENA     0x00040000
// The enable bit for the interrupt triggered by a PWM operator 2 TEA event (R/W)
#define INT_OP2_TEA_INT_ENA     0x00020000
// The enable bit for the interrupt triggered by a PWM operator 1 TEA event (R/W)
#define INT_OP1_TEA_INT_ENA     0x00010000
// The enable bit for the interrupt triggered by a PWM operator 0 TEA event (R/W)
#define INT_OP0_TEA_INT_ENA     0x00008000
// The enable bit for the interrupt triggered when event_f2 ends. (R/W)
#define INT_FAULT2_CLR_INT_ENA  0x00004000
// The enable bit for the interrupt triggered when event_f1 ends. (R/W)
#define INT_FAULT1_CLR_INT_ENA  0x00002000
// The enable bit for the interrupt triggered when event_f0 ends. (R/W)
#define INT_FAULT0_CLR_INT_ENA  0x00001000
// The enable bit for the interrupt triggered when event_f2 starts. (R/W)
#define INT_FAULT2_INT_ENA      0x00000800
// The enable bit for the interrupt triggered when event_f1 starts. (R/W)
#define INT_FAULT1_INT_ENA      0x00000400
// The enable bit for the interrupt triggered when event_f0 starts. (R/W)
#define INT_FAULT0_INT_ENA      0x00000200
// The enable bit for the interrupt triggered by a PWM timer 2 TEP event. (R/W)
#define INT_TIMER2_TEP_INT_ENA  0x00000100
// The enable bit for the interrupt triggered by a PWM timer 1 TEP event. (R/W)
#define INT_TIMER1_TEP_INT_ENA  0x00000080
// The enable bit for the interrupt triggered by a PWM timer 0 TEP event. (R/W)
#define INT_TIMER0_TEP_INT_ENA  0x00000040
// The enable bit for the interrupt triggered by a PWM timer 2 TEZ event. (R/W)
#define INT_TIMER2_TEZ_INT_ENA  0x00000020
// The enable bit for the interrupt triggered by a PWM timer 1 TEZ event. (R/W)
#define INT_TIMER1_TEZ_INT_ENA  0x00000010
// The enable bit for the interrupt triggered by a PWM timer 0 TEZ event. (R/W)
#define INT_TIMER0_TEZ_INT_ENA  0x00000008
// The enable bit for the interrupt triggered when the timer 2 stops. (R/W)
#define INT_TIMER2_STOP_INT_ENA 0x00000004
// The enable bit for the interrupt triggered when the timer 1 stops. (R/W)
#define INT_TIMER1_STOP_INT_ENA 0x00000002
// The enable bit for the interrupt triggered when the timer 0 stops. (R/W)
#define INT_TIMER0_STOP_INT_ENA 0x00000001

/* Raw interrupt status */
#define INT0_RAW_PWM_REG            MMIO32(MCPWM0_BASE + 0x114)
#define INT1_RAW_PWM_REG            MMIO32(MCPWM1_BASE + 0x114)
// The raw status bit for the interrupt triggered by capture on channel 2. (RO)
#define INT_CAP2_INT_RAW        0x20000000
// The raw status bit for the interrupt triggered by capture on channel 1. (RO)
#define INT_CAP1_INT_RAW        0x10000000
// The raw status bit for the interrupt triggered by capture on channel 0. (RO)
#define INT_CAP0_INT_RAW        0x08000000
// The raw status bit for the int triggered by a one-shot mode action on PWM2.
#define INT_FH2_OST_INT_RAW     0x04000000
// The raw status bit for the int triggered by a one-shot mode action on PWM1.
#define INT_FH1_OST_INT_RAW     0x02000000
// The raw status bit for the int triggered by a one-shot mode action on PWM0.
#define INT_FH0_OST_INT_RAW     0x01000000
// The raw status bit for the int trig by a cycle-by-cycle mode action on PWM2.
#define INT_FH2_CBC_INT_RAW     0x00800000
// The raw status bit for the int trig by a cycle-by-cycle mode action on PWM1.
#define INT_FH1_CBC_INT_RAW     0x00400000
// The raw status bit for the int trig by a cycle-by-cycle mode action on PWM0.
#define INT_FH0_CBC_INT_RAW     0x00200000
// The raw status bit for the interrupt triggered by a PWM operator 2 TEB event
#define INT_OP2_TEB_INT_RAW     0x00100000
// The raw status bit for the interrupt triggered by a PWM operator 1 TEB event
#define INT_OP1_TEB_INT_RAW     0x00080000
// The raw status bit for the interrupt triggered by a PWM operator 0 TEB event
#define INT_OP0_TEB_INT_RAW     0x00040000
// The raw status bit for the interrupt triggered by a PWM operator 2 TEA event
#define INT_OP2_TEA_INT_RAW     0x00020000
// The raw status bit for the interrupt triggered by a PWM operator 1 TEA event
#define INT_OP1_TEA_INT_RAW     0x00010000
// The raw status bit for the interrupt triggered by a PWM operator 0 TEA event
#define INT_OP0_TEA_INT_RAW     0x00008000
// The raw status bit for the interrupt triggered when event_f2 ends. (RO)
#define INT_FAULT2_CLR_INT_RAW  0x00004000
// The raw status bit for the interrupt triggered when event_f1 ends. (RO)
#define INT_FAULT1_CLR_INT_RAW  0x00002000
// The raw status bit for the interrupt triggered when event_f0 ends. (RO)
#define INT_FAULT0_CLR_INT_RAW  0x00001000
// The raw status bit for the interrupt triggered when event_f2 starts. (RO)
#define INT_FAULT2_INT_RAW      0x00000800
// The raw status bit for the interrupt triggered when event_f1 starts. (RO)
#define INT_FAULT1_INT_RAW      0x00000400
// The raw status bit for the interrupt triggered when event_f0 starts. (RO)
#define INT_FAULT0_INT_RAW      0x00000200
// The raw status bit for the interrupt triggered by a PWM timer 2 TEP event.
#define INT_TIMER2_TEP_INT_RAW  0x00000100
// The raw status bit for the interrupt triggered by a PWM timer 1 TEP event.
#define INT_TIMER1_TEP_INT_RAW  0x00000080
// The raw status bit for the interrupt triggered by a PWM timer 0 TEP event.
#define INT_TIMER0_TEP_INT_RAW  0x00000040
// The raw status bit for the interrupt triggered by a PWM timer 2 TEZ event.
#define INT_TIMER2_TEZ_INT_RAW  0x00000020
// The raw status bit for the interrupt triggered by a PWM timer 1 TEZ event.
#define INT_TIMER1_TEZ_INT_RAW  0x00000010
// The raw status bit for the interrupt triggered by a PWM timer 0 TEZ event.
#define INT_TIMER0_TEZ_INT_RAW  0x00000008
// The raw status bit for the interrupt triggered when the timer 2 stops. (RO)
#define INT_TIMER2_STOP_INT_RAW 0x00000004
// The raw status bit for the interrupt triggered when the timer 1 stops. (RO)
#define INT_TIMER1_STOP_INT_RAW 0x00000002
// The raw status bit for the interrupt triggered when the timer 0 stops. (RO)
#define INT_TIMER0_STOP_INT_RAW 0x00000001

/* Masked interrupt status */
#define INT0_ST_PWM_REG             MMIO32(MCPWM0_BASE + 0x118)
#define INT1_ST_PWM_REG             MMIO32(MCPWM1_BASE + 0x118)
// The masked status bit for the interrupt triggered by capture on channel 2.
#define INT_CAP2_INT_ST        0x20000000
// The masked status bit for the interrupt triggered by capture on channel 1.
#define INT_CAP1_INT_ST        0x10000000
// The masked status bit for the interrupt triggered by capture on channel 0.
#define INT_CAP0_INT_ST        0x08000000
// The masked status bit for the int triggered by a one-shot mode action on PWM2.
#define INT_FH2_OST_INT_ST     0x04000000
// The masked status bit for the int triggered by a one-shot mode action on PWM1.
#define INT_FH1_OST_INT_ST     0x02000000
// The masked status bit for the int triggered by a one-shot mode action on PWM0.
#define INT_FH0_OST_INT_ST     0x01000000
// The masked status bit for the int trig by a cycle-by-cycle mode action on PWM2.
#define INT_FH2_CBC_INT_ST     0x00800000
// The masked status bit for the int trig by a cycle-by-cycle mode action on PWM1.
#define INT_FH1_CBC_INT_ST     0x00400000
// The masked status bit for the int trig by a cycle-by-cycle mode action on PWM0.
#define INT_FH0_CBC_INT_ST     0x00200000
// The masked status bit for the interrupt triggered by a PWM operator 2 TEB event
#define INT_OP2_TEB_INT_ST     0x00100000
// The masked status bit for the interrupt triggered by a PWM operator 1 TEB event
#define INT_OP1_TEB_INT_ST     0x00080000
// The masked status bit for the interrupt triggered by a PWM operator 0 TEB event
#define INT_OP0_TEB_INT_ST     0x00040000
// The masked status bit for the interrupt triggered by a PWM operator 2 TEA event
#define INT_OP2_TEA_INT_ST     0x00020000
// The masked status bit for the interrupt triggered by a PWM operator 1 TEA event
#define INT_OP1_TEA_INT_ST     0x00010000
// The masked status bit for the interrupt triggered by a PWM operator 0 TEA event
#define INT_OP0_TEA_INT_ST     0x00008000
// The masked status bit for the interrupt triggered when event_f2 ends. (RO)
#define INT_FAULT2_CLR_INT_ST  0x00004000
// The masked status bit for the interrupt triggered when event_f1 ends. (RO)
#define INT_FAULT1_CLR_INT_ST  0x00002000
// The masked status bit for the interrupt triggered when event_f0 ends. (RO)
#define INT_FAULT0_CLR_INT_ST  0x00001000
// The masked status bit for the interrupt triggered when event_f2 starts. (RO)
#define INT_FAULT2_INT_ST      0x00000800
// The masked status bit for the interrupt triggered when event_f1 starts. (RO)
#define INT_FAULT1_INT_ST      0x00000400
// The masked status bit for the interrupt triggered when event_f0 starts. (RO)
#define INT_FAULT0_INT_ST      0x00000200
// The masked status bit for the interrupt triggered by a PWM timer 2 TEP event.
#define INT_TIMER2_TEP_INT_ST  0x00000100
// The masked status bit for the interrupt triggered by a PWM timer 1 TEP event.
#define INT_TIMER1_TEP_INT_ST  0x00000080
// The masked status bit for the interrupt triggered by a PWM timer 0 TEP event.
#define INT_TIMER0_TEP_INT_ST  0x00000040
// The masked status bit for the interrupt triggered by a PWM timer 2 TEZ event.
#define INT_TIMER2_TEZ_INT_ST  0x00000020
// The masked status bit for the interrupt triggered by a PWM timer 1 TEZ event.
#define INT_TIMER1_TEZ_INT_ST  0x00000010
// The masked status bit for the interrupt triggered by a PWM timer 0 TEZ event.
#define INT_TIMER0_TEZ_INT_ST  0x00000008
// The masked status bit for the interrupt triggered when the timer 2 stops. (RO)
#define INT_TIMER2_STOP_INT_ST 0x00000004
// The masked status bit for the interrupt triggered when the timer 1 stops. (RO)
#define INT_TIMER1_STOP_INT_ST 0x00000002
// The masked status bit for the interrupt triggered when the timer 0 stops. (RO)
#define INT_TIMER0_STOP_INT_ST 0x00000001

/* Interrupt clear bits */
#define INT0_CLR_PWM_REG            MMIO32(MCPWM0_BASE + 0x11c)
#define INT1_CLR_PWM_REG            MMIO32(MCPWM1_BASE + 0x11c)
// Set this bit to clear interrupt triggered by capture on channel 2. (WO)
#define INT_CAP2_INT_CLR        0x20000000
// Set this bit to clear interrupt triggered by capture on channel 1. (WO)
#define INT_CAP1_INT_CLR        0x10000000
// Set this bit to clear interrupt triggered by capture on channel 0. (WO)
#define INT_CAP0_INT_CLR        0x08000000
// Set this bit to clear interrupt triggered by a one-shot mode action on PWM2.
#define INT_FH2_OST_INT_CLR     0x04000000
// Set this bit to clear interrupt triggered by a one-shot mode action on PWM1.
#define INT_FH1_OST_INT_CLR     0x02000000
// Set this bit to clear interrupt triggered by a one-shot mode action on PWM0.
#define INT_FH0_OST_INT_CLR     0x01000000
// Set this bit to clear int triggered by a cycle-by-cycle mode action on PWM2.
#define INT_FH2_CBC_INT_CLR     0x00800000
// Set this bit to clear int triggered by a cycle-by-cycle mode action on PWM1.
#define INT_FH1_CBC_INT_CLR     0x00400000
// Set this bit to clear int triggered by a cycle-by-cycle mode action on PWM0.
#define INT_FH0_CBC_INT_CLR     0x00200000
// Set this bit to clear interrupt triggered by a PWM operator 2 TEB event (WO)
#define INT_OP2_TEB_INT_CLR     0x00100000
// Set this bit to clear interrupt triggered by a PWM operator 1 TEB event (WO)
#define INT_OP1_TEB_INT_CLR     0x00080000
// Set this bit to clear interrupt triggered by a PWM operator 0 TEB event (WO)
#define INT_OP0_TEB_INT_CLR     0x00040000
// Set this bit to clear interrupt triggered by a PWM operator 2 TEA event (WO)
#define INT_OP2_TEA_INT_CLR     0x00020000
// Set this bit to clear interrupt triggered by a PWM operator 1 TEA event (WO)
#define INT_OP1_TEA_INT_CLR     0x00010000
// Set this bit to clear interrupt triggered by a PWM operator 0 TEA event (WO)
#define INT_OP0_TEA_INT_CLR     0x00008000
// Set this bit to clear interrupt triggered when event_f2 ends. (WO)
#define INT_FAULT2_CLR_INT_CLR  0x00004000
// Set this bit to clear interrupt triggered when event_f1 ends. (WO)
#define INT_FAULT1_CLR_INT_CLR  0x00002000
// Set this bit to clear interrupt triggered when event_f0 ends. (WO)
#define INT_FAULT0_CLR_INT_CLR  0x00001000
// Set this bit to clear interrupt triggered when event_f2 starts. (WO)
#define INT_FAULT2_INT_CLR      0x00000800
// Set this bit to clear interrupt triggered when event_f1 starts. (WO)
#define INT_FAULT1_INT_CLR      0x00000400
// Set this bit to clear interrupt triggered when event_f0 starts. (WO)
#define INT_FAULT0_INT_CLR      0x00000200
// Set this bit to clear interrupt triggered by a PWM timer 2 TEP event. (WO)
#define INT_TIMER2_TEP_INT_CLR  0x00000100
// Set this bit to clear interrupt triggered by a PWM timer 1 TEP event. (WO)
#define INT_TIMER1_TEP_INT_CLR  0x00000080
// Set this bit to clear interrupt triggered by a PWM timer 0 TEP event. (WO)
#define INT_TIMER0_TEP_INT_CLR  0x00000040
// Set this bit to clear interrupt triggered by a PWM timer 2 TEZ event. (WO)
#define INT_TIMER2_TEZ_INT_CLR  0x00000020
// Set this bit to clear interrupt triggered by a PWM timer 1 TEZ event. (WO)
#define INT_TIMER1_TEZ_INT_CLR  0x00000010
// Set this bit to clear interrupt triggered by a PWM timer 0 TEZ event. (WO)
#define INT_TIMER0_TEZ_INT_CLR  0x00000008
// Set this bit to clear interrupt triggered when the timer 2 stops. (WO)
#define INT_TIMER2_STOP_INT_CLR 0x00000004
// Set this bit to clear interrupt triggered when the timer 1 stops. (WO)
#define INT_TIMER1_STOP_INT_CLR 0x00000002
// Set this bit to clear interrupt triggered when the timer 0 stops. (WO)
#define INT_TIMER0_STOP_INT_CLR 0x00000001

#endif
