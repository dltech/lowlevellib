#ifndef H_SPI_REGS
#define H_SPI_REGS
/*
 * ESP32 alternative library, SPI Controller registers.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Start user-defined command */
#define SPI0_CMD_REG                    MMIO32(SPI0_BASE + 0x000)
#define SPI1_CMD_REG                    MMIO32(SPI1_BASE + 0x000)
#define SPI2_CMD_REG                    MMIO32(SPI2_BASE + 0x000)
#define SPI3_CMD_REG                    MMIO32(SPI3_BASE + 0x000)
// An SPI operation will be triggered when this bit is set
#define SPI_USR 0x40000

/* Address data */
#define SPI0_ADDR_REG                   MMIO32(SPI0_BASE + 0x004)
#define SPI1_ADDR_REG                   MMIO32(SPI1_BASE + 0x004)
#define SPI2_ADDR_REG                   MMIO32(SPI2_BASE + 0x004)
#define SPI3_ADDR_REG                   MMIO32(SPI3_BASE + 0x004)
// stores the transmitting address when master is in half-duplex mode or QSPI mode.

/* Bit order and QIO/DIO/QOUT/DOUT mode settings */
#define SPI0_CTRL_REG                   MMIO32(SPI0_BASE + 0x008)
#define SPI1_CTRL_REG                   MMIO32(SPI1_BASE + 0x008)
#define SPI2_CTRL_REG                   MMIO32(SPI2_BASE + 0x008)
#define SPI3_CTRL_REG                   MMIO32(SPI3_BASE + 0x008)
// bit order for command, address and data in transmitted signal.
#define SPI_WR_BIT_ORDER    0x4000000
// This bit determines the bit order for received data in received signal.
#define SPI_RD_BIT_ORDER    0x2000000
// This bit is used to enable four-line address writes and data reads in QSPI mode.
#define SPI_FREAD_QIO       0x1000000
// This bit is used to enable two-line address writes and data reads in QSPI mode.
#define SPI_FREAD_DIO       0x0800000
// This bit determines the wr-prot signal output when SPI is idle in QSPI mode.
#define SPI_WP              0x0200000
// This bit is used to enable four-line data reads in QSPI mode. (R/W)
#define SPI_FREAD_QUAD      0x0100000
// This bit is used to enable two-line data reads in QSPI mode. (R/W)
#define SPI_FREAD_DUAL      0x0004000
// Reserved.
#define SPI_FASTRD_MODE     0x0002000

/* Timing configuration */
#define SPI0_CTRL1_REG                  MMIO32(SPI0_BASE + 0x00c)
#define SPI1_CTRL1_REG                  MMIO32(SPI1_BASE + 0x00c)
#define SPI2_CTRL1_REG                  MMIO32(SPI2_BASE + 0x00c)
#define SPI3_CTRL1_REG                  MMIO32(SPI3_BASE + 0x00c)
#define SPI_CS_HOLD_DELAY_MSK       0xf0000000
#define SPI_CS_HOLD_DELAY_SFT       28
#define SPI_CS_HOLD_DELAY_SET(n)  ((n<<SPI_CS_HOLD_DELAY_SFT)&SPI_CS_HOLD_DELAY_MSK)

/* Timing configuration */
#define SPI0_RD_STATUS_REG                  MMIO32(SPI0_BASE + 0x014)
#define SPI1_RD_STATUS_REG                  MMIO32(SPI1_BASE + 0x014)
#define SPI2_RD_STATUS_REG                  MMIO32(SPI2_BASE + 0x014)
#define SPI3_RD_STATUS_REG                  MMIO32(SPI3_BASE + 0x014)
#define SPI_STATUS_EXT_MSK  0xff000000
#define SPI_STATUS_EXT_SFT  24
#define SPI0_STATUS_EXT_GET ((SPI0_RD_STATUS_REG>>SPI_STATUS_EXT_SFT)&0xff)
#define SPI1_STATUS_EXT_GET ((SPI0_RD_STATUS_REG>>SPI_STATUS_EXT_SFT)&0xff)
#define SPI2_STATUS_EXT_GET ((SPI0_RD_STATUS_REG>>SPI_STATUS_EXT_SFT)&0xff)
#define SPI3_STATUS_EXT_GET ((SPI0_RD_STATUS_REG>>SPI_STATUS_EXT_SFT)&0xff)
#define SPI_STATUS_MSK      0x0000ffff
#define SPI0_STATUS_GET     (SPI0_RD_STATUS_REG&SPI_STATUS_MSK)
#define SPI1_STATUS_GET     (SPI0_RD_STATUS_REG&SPI_STATUS_MSK)
#define SPI2_STATUS_GET     (SPI0_RD_STATUS_REG&SPI_STATUS_MSK)
#define SPI3_STATUS_GET     (SPI0_RD_STATUS_REG&SPI_STATUS_MSK)

/* Timing configuration */
#define SPI0_CTRL2_REG                  MMIO32(SPI0_BASE + 0x018)
#define SPI1_CTRL2_REG                  MMIO32(SPI1_BASE + 0x018)
#define SPI2_CTRL2_REG                  MMIO32(SPI2_BASE + 0x018)
#define SPI3_CTRL2_REG                  MMIO32(SPI3_BASE + 0x018)
#define SPI_CS_DELAY_NUM_MSK        0xf0000000
#define SPI_CS_DELAY_NUM_SFT        28
#define SPI_CS_DELAY_NUM_SET(n)     ((n<<SPI_CS_DELAY_NUM_SFT)&SPI_CS_DELAY_NUM_MSK)
#define SPI_CS_DELAY_MODE_MSK       0x0c000000
#define SPI_CS_DELAY_MODE_SFT       26
#define SPI_CS_DELAY_MODE_SET(n)  ((n<<SPI_CS_DELAY_MODE_SFT)&SPI_CS_DELAY_MODE_MSK)
// number of system clock cycles by which the MOSI signals are delayed. (R/W)
#define SPI_MOSI_DELAY_NUM_MSK      0x03800000
#define SPI_MOSI_DELAY_NUM_SFT      23
#define SPI_MOSI_DELAY_NUM_SET(n) ((n<<SPI_MOSI_DELAY_NUM_SFT)&SPI_MOSI_DELAY_NUM_MSK)
// determines the way the MOSI signals are delayed by SPI clock. (R/W)
#define SPI_MOSI_DELAY_MODE_MSK     0x00600000
#define SPI_MOSI_DELAY_MODE_SFT     21
#define SPI_MOSI_DELAY_MODE_SET(n)((n<<SPI_MOSI_DELAY_MODE_SFT)&SPI_MOSI_DELAY_MODE_MSK)
// number of system clock cycles by which the MISO signals are delayed. (R/W)
#define SPI_MISO_DELAY_NUM_MSK      0x001c0000
#define SPI_MISO_DELAY_NUM_SFT      18
#define SPI_MISO_DELAY_NUM_SET(n) ((n<<SPI_MISO_DELAY_NUM_SFT)&SPI_MISO_DELAY_NUM_MSK)
// determines the way MISO signals are delayed by SPI clock. (R/W)
#define SPI_MISO_DELAY_MODE_MSK     0x00030000
#define SPI_MISO_DELAY_MODE_SFT     16
#define SPI_MISO_DELAY_MODE_SET(n)  ((n<<SPI_MISO_DELAY_MODE_SFT)&SPI_MISO_DELAY_MODE_MSK)
// number of SPI clock cycles by which CS pin signals are delayed.
#define SPI_CK_OUT_HIGH_MODE_MSK    0x0000f000
#define SPI_CK_OUT_HIGH_MODE_SFT    12
#define SPI_CK_OUT_HIGH_MODE_SET(n)        ((n<<SPI_CK_OUT_HIGH_MODE_SFT)&SPI_CK_OUT_HIGH_MODE_MSK)
// number of SPI clock cycles by which CS pin signals are delayed.
#define SPI_HOLD_TIME_MSK           0x000000f0
#define SPI_HOLD_TIME_SFT           4
#define SPI_HOLD_TIME_SET(n)        ((n<<SPI_HOLD_TIME_SFT)&SPI_HOLD_TIME_MSK)
// configure the time between the CS signal active edge
#define SPI_SETUP_TIME_MSK          0x0000000f
#define SPI_SETUP_TIME_SET(n)       (n&SPI_SETUP_TIME_MSK)

/* Clock configuration */
#define SPI0_CLOCK_REG                  MMIO32(SPI0_BASE + 0x018)
#define SPI1_CLOCK_REG                  MMIO32(SPI1_BASE + 0x018)
#define SPI2_CLOCK_REG                  MMIO32(SPI2_BASE + 0x018)
#define SPI3_CLOCK_REG                  MMIO32(SPI3_BASE + 0x018)
// SPI output clock is equal to system clock;
#define SPI_CLK_EQU_SYSCLK      0x80000000
// it is used to configure the pre-divider value for SPI output clock.
#define SPI_CLKDIV_PRE_MSK      0x7ffc0000
#define SPI_CLKDIV_PRE_SFT      18
#define SPI_CLKDIV_PRE_SET(n)   ((n<<SPI_CLKDIV_PRE_SFT)&SPI_CLKDIV_PRE_MSK)
// it is used to configure the divider for SPI output clock.
#define SPI_CLKCNT_N_MSK        0x0003f000
#define SPI_CLKCNT_N_SFT        12
#define SPI_CLKCNT_N_SET(n)     ((n<<SPI_CLKCNT_N_SFT)&SPI_CLKCNT_N_MSK)
// In master mode, SPI_CLKCNT_H =
#define SPI_CLKCNT_H_MSK        0x00000fc0
#define SPI_CLKCNT_H_SFT        6
#define SPI_CLKCNT_H_SET(n)     ((n<<SPI_CLKCNT_H_SFT)&SPI_CLKCNT_H_MSK)
#define SPI_CLKCNT_H(n)         (((n+1)/2)-1)
// In master mode, it is equal to SPI_CLKCNT_N.
#define SPI_CLKCNT_L_MSK        0x0000003f
#define SPI_CLKCNT_L_SET(n)     (n&SPI_CLKCNT_L_MSK)

/* User defined command configuration */
#define SPI0_USER_REG                   MMIO32(SPI0_BASE + 0x01c)
#define SPI1_USER_REG                   MMIO32(SPI1_BASE + 0x01c)
#define SPI2_USER_REG                   MMIO32(SPI2_BASE + 0x01c)
#define SPI3_USER_REG                   MMIO32(SPI3_BASE + 0x01c)
// command phase of an SPI operation in SPI half-duplex mode and QSPI mode. (R/W)
#define SPI_USR_COMMAND         0x80000000
// address phase of an SPI operation in SPI half-duplex mode and QSPI mode. (R/W)
#define SPI_USR_ADDR            0x40000000
// dummy phase of an SPI operation in SPI half-duplex mode and QSPI mode. (R/W)
#define SPI_USR_DUMMY           0x20000000
// read-data phase of an SPI operation in SPI half-duplex mode and QSPI mode. (R/W)
#define SPI_USR_MISO            0x10000000
// write-data phase of an SPI operation in SPI half-duplex mode and QSPI mode. (R/W)
#define SPI_USR_MOSI            0x08000000
// SPI clock signal is dis in the dummy ph when the bit is set in SPI half-duplex
#define SPI_USR_DUMMY_IDLE      0x04000000
// MOSI data is stored in SPI_W8 ~ SPI_W15 of the SPI buffer. (R/W)
#define SPI_USR_MOSI_HIGHPART   0x02000000
// If set, MISO data is stored in SPI_W8 ~ SPI_W15 of the SPI buffer. (R/W)
#define SPI_USR_MISO_HIGHPART   0x01000000
// Set this bit to enable three-line half-duplex communication. (R/W)
#define SPI_SIO                 0x00001000
#define SPI_FWRITE_QIO          0x00000800
#define SPI_FWRITE_DIO          0x00000400
#define SPI_FWRITE_QUAD         0x00000080
#define SPI_FWRITE_DUAL         0x00000040
// This bit determines the byte order of the command, address and data in tx signal.
#define SPI_WR_BYTE_ORDER       0x00000020
// sets the MOSI signal delay mode. It is only valid in master mode. (R/W)
#define SPI_CK_OUT_EDGE         0x00000010
// bit is the same as SPI_CK_OUT_EDGE in master mode.
#define SPI_CK_I_EDGE           0x00000001

/* Address and dummy cycle configuration */
#define SPI0_USER1_REG                  MMIO32(SPI0_BASE + 0x020)
#define SPI1_USER1_REG                  MMIO32(SPI1_BASE + 0x020)
#define SPI2_USER1_REG                  MMIO32(SPI2_BASE + 0x020)
#define SPI3_USER1_REG                  MMIO32(SPI3_BASE + 0x020)
// bit length of the transmitted address minus one in half-duplex mode and QSPI mode
#define SPI_USR_ADDR_BITLEN_MSK     0xfc000000
#define SPI_USR_ADDR_BITLEN_SFT     26
#define SPI0_USR_ADDR_BITLEN_GET    ((SPI0_USER1_REG>>SPI_USR_ADDR_BITLEN_SFT)&0x3f)
#define SPI1_USR_ADDR_BITLEN_GET    ((SPI0_USER1_REG>>SPI_USR_ADDR_BITLEN_SFT)&0x3f)
#define SPI2_USR_ADDR_BITLEN_GET    ((SPI0_USER1_REG>>SPI_USR_ADDR_BITLEN_SFT)&0x3f)
#define SPI3_USR_ADDR_BITLEN_GET    ((SPI0_USER1_REG>>SPI_USR_ADDR_BITLEN_SFT)&0x3f)
// num of SPI clk cycles for the dummy ph -1 in SPI half-duplex mode and QSPI mode.
#define SPI_USR_DUMMY_CYCLELEN_MSK  0x000000ff
#define SPI0_USR_DUMMY_CYCLELEN_GET (SPI0_USER1_REG&SPI_USR_DUMMY_CYCLELEN_MSK)
#define SPI1_USR_DUMMY_CYCLELEN_GET (SPI1_USER1_REG&SPI_USR_DUMMY_CYCLELEN_MSK)
#define SPI2_USR_DUMMY_CYCLELEN_GET (SPI2_USER1_REG&SPI_USR_DUMMY_CYCLELEN_MSK)
#define SPI3_USR_DUMMY_CYCLELEN_GET (SPI3_USER1_REG&SPI_USR_DUMMY_CYCLELEN_MSK)

/* Command length and value configuration */
#define SPI0_USER2_REG                  MMIO32(SPI0_BASE + 0x024)
#define SPI1_USER2_REG                  MMIO32(SPI1_BASE + 0x024)
#define SPI2_USER2_REG                  MMIO32(SPI2_BASE + 0x024)
#define SPI3_USER2_REG                  MMIO32(SPI3_BASE + 0x024)
// bit length of the command phase minus one in SPI half-duplex mode and QSPI mode.
#define SPI_USR_COMMAND_BITLEN_MSK      0xf0000000
#define SPI_USR_COMMAND_BITLEN_SFT      28
#define SPI_USR_COMMAND_BITLEN_SET(n) ((n<<SPI_USR_COMMAND_BITLEN_SFT)&SPI_USR_COMMAND_BITLEN_SFT)
// value of the command to be transmitted in SPI half-duplex mode and QSPI mode.
#define SPI_USR_COMMAND_VALUE_MSK       0x0000ffff
#define SPI_USR_COMMAND_VALUE_SET(n)    (n&SPI_USR_COMMAND_VALUE_MSK)


/* MOSI length */
#define SPI0_MOSI_DLEN_REG              MMIO32(SPI0_BASE + 0x028)
#define SPI1_MOSI_DLEN_REG              MMIO32(SPI1_BASE + 0x028)
#define SPI2_MOSI_DLEN_REG              MMIO32(SPI2_BASE + 0x028)
#define SPI3_MOSI_DLEN_REG              MMIO32(SPI3_BASE + 0x028)
// length of MOSI data minus one, in multiples of one bit.
#define SPI_USR_MOSI_DBITLEN_MSK    0xff000000
#define SPI_USR_MOSI_DBITLEN_SFT    24
#define SPI_USR_MOSI_DBITLEN_SET(n) ((n<<SPI_USR_MOSI_DBITLEN_SFT)&SPI_USR_MOSI_DBITLEN_MSK)

/* MOSI length */
#define SPI0_MISO_DLEN_REG              MMIO32(SPI0_BASE + 0x02c)
#define SPI1_MISO_DLEN_REG              MMIO32(SPI1_BASE + 0x02c)
#define SPI2_MISO_DLEN_REG              MMIO32(SPI2_BASE + 0x02c)
#define SPI3_MISO_DLEN_REG              MMIO32(SPI3_BASE + 0x02c)
// length of MOSI data minus one, in multiples of one bit.
#define SPI_USR_MISO_DBITLEN_MSK    0xff000000
#define SPI_USR_MISO_DBITLEN_SFT    24
#define SPI_USR_MISO_DBITLEN_SET(n) ((n<<SPI_USR_MISO_DBITLEN_SFT)&SPI_USR_MISO_DBITLEN_MSK)

/* Slave status/Part of lower master address */
#define SPI0_SLV_WR_STATUS_REG          MMIO32(SPI0_BASE + 0x030)
#define SPI1_SLV_WR_STATUS_REG          MMIO32(SPI1_BASE + 0x030)
#define SPI2_SLV_WR_STATUS_REG          MMIO32(SPI2_BASE + 0x030)
#define SPI3_SLV_WR_STATUS_REG          MMIO32(SPI3_BASE + 0x030)
// status register for the master to write the slave.

/* Polarity and CS configuration */
#define SPI0_PIN_REG                    MMIO32(SPI0_BASE + 0x034)
#define SPI1_PIN_REG                    MMIO32(SPI1_BASE + 0x034)
#define SPI2_PIN_REG                    MMIO32(SPI2_BASE + 0x034)
#define SPI3_PIN_REG                    MMIO32(SPI3_BASE + 0x034)
// the CS signal will keep active. (R/W)
#define SPI_CS_KEEP_ACTIVE          0x40000000
// logicl level of SPI output clock in idle state. (R/W)
#define SPI_CK_IDLE_EDGE            0x20000000
#define SPI_MASTER_CK_SEL_MSK       0x00003c00
#define SPI_MASTER_CK_SEL_SFT       11
#define SPI_MASTER_CK_SEL_SET(n)  ((n<<SPI_MASTER_CK_SEL_SFT)&SPI_MASTER_CK_SEL_MSK)
#define SPI_MASTER_CS_POL_MSK
#define SPI_MASTER_CS_POL_SFT       0x000003c0
#define SPI_MASTER_CS_POL_SET(n)  ((n<<SPI_MASTER_CS_POL_SFT)&SPI_MASTER_CS_POL_MSK)
#define SPI_CK_DIS                  0x00000020
// This bit enables the SPI CS2 signal.
#define SPI_CS2_DIS                 0x00000004
// This bit enables the SPI CS1 signal.
#define SPI_CS1_DIS                 0x00000002
// This bit enables the SPI CS0 signal.
#define SPI_CS0_DIS                 0x00000001

/* Slave mode configuration and interrupt status */
#define SPI0_SLAVE_REG                  MMIO32(SPI0_BASE + 0x038)
#define SPI1_SLAVE_REG                  MMIO32(SPI1_BASE + 0x038)
#define SPI2_SLAVE_REG                  MMIO32(SPI2_BASE + 0x038)
#define SPI3_SLAVE_REG                  MMIO32(SPI3_BASE + 0x038)
// resets the latched values of the SPI clock line, CS line and data line.
#define SPI_SYNC_RESET              0x80000000
// This bit is used to set the mode of the SPI device. (R/W)
#define SPI_SLAVE_MODE              0x40000000
// the write and read data commands are enabled. (R/W)
#define SPI_SLV_WR_RD_BUF_EN        0x20000000
// the write and read status commands are enabled. (R/W)
#define SPI_SLV_WR_RD_STA_EN        0x10000000
#define SPI_SLV_CMD_DEFINE          0x08000000
// The counter for operations in both the master mode and the slave mode. (RO)
#define SPI_TRANS_CNT_MSK           0x07800000
#define SPI_TRANS_CNT_SFT           23
#define SPI0_TRANS_CNT_GET          ((SPI0_SLAVE_REG>>SPI_TRANS_CNT_SFT)&0xf)
#define SPI1_TRANS_CNT_GET          ((SPI1_SLAVE_REG>>SPI_TRANS_CNT_SFT)&0xf)
#define SPI2_TRANS_CNT_GET          ((SPI2_SLAVE_REG>>SPI_TRANS_CNT_SFT)&0xf)
#define SPI3_TRANS_CNT_GET          ((SPI3_SLAVE_REG>>SPI_TRANS_CNT_SFT)&0xf)
// In slave mode, this contains the state of the SPI state machine. (RO)
#define SPI_SLV_LAST_STATE_MSK      0x00700000
#define SPI_SLV_LAST_STATE_SFT      20
#define SPI0_SLV_LAST_STATE_GET     ((SPI0_SLAVE_REG>>SPI_SLV_LAST_STATE_SFT)&0x7)
#define SPI1_SLV_LAST_STATE_GET     ((SPI1_SLAVE_REG>>SPI_SLV_LAST_STATE_SFT)&0x7)
#define SPI2_SLV_LAST_STATE_GET     ((SPI2_SLAVE_REG>>SPI_SLV_LAST_STATE_SFT)&0x7)
#define SPI3_SLV_LAST_STATE_GET     ((SPI3_SLAVE_REG>>SPI_SLV_LAST_STATE_SFT)&0x7)
#define SPI_SLV_LAST_COMMAND_MSK    0x000e0000
#define SPI_SLV_LAST_COMMAND_SFT    17
#define SPI_SLV_LAST_COMMAND_SET(n) ((n<<SPI_SLV_LAST_COMMAND_SFT)&SPI_SLV_LAST_COMMAND_MSK)
#define SPI_CS_I_MODE_MSK           0x00000c00
#define SPI_CS_I_MODE_SFT           10
#define SPI_CS_I_MODE_SET(n)        ((n<<SPI_CS_I_MODE_SFT)&SPI_CS_I_MODE_MSK)
// The interrupt enable bit for the SPI_TRANS_DONE_INT interrupt. (R/W)
#define SPI_TRANS_INTEN             0x00000200
// The interrupt enable bit for the SPI_SLV_WR_STA_INT interrupt. (R/W)
#define SPI_SLV_WR_STA_INTEN        0x00000100
// The interrupt enable bit for the SPI_SLV_RD_STA_INT interrupt. (R/W)
#define SPI_SLV_RD_STA_INTEN        0x00000080
// The interrupt enable bit for the SPI_SLV_WR_BUF_INT interrupt. (R/W)
#define SPI_SLV_WR_BUF_INTEN        0x00000040
// The interrupt enable bit for the SPI_SLV_RD_BUF_INT interrupt. (R/W)
#define SPI_SLV_RD_BUF_INTEN        0x00000020
// The raw interrupt status bit for the SPI_TRANS_DONE_INT interrupt.
#define SPI_TRANS_DONE              0x00000010
// The raw interrupt status bit for the SPI_SLV_WR_STA_INT interrupt.
#define SPI_SLV_WR_STA_DONE         0x00000008
// The raw interrupt status bit for the SPI_SLV_RD_STA_INT interrupt.
#define SPI_SLV_RD_STA_DONE         0x00000004
// The raw interrupt status bit for the SPI_SLV_WR_BUF_INT interrupt.
#define SPI_SLV_WR_BUF_DONE         0x00000002
// The raw interrupt status bit for the SPI_SLV_RD_BUF_INT interrupt.
#define SPI_SLV_RD_BUF_DONE         0x00000001

/* Slave mode configuration and interrupt status */
#define SPI0_SLAVE1_REG                 MMIO32(SPI0_BASE + 0x03c)
#define SPI1_SLAVE1_REG                 MMIO32(SPI1_BASE + 0x03c)
#define SPI2_SLAVE1_REG                 MMIO32(SPI2_BASE + 0x03c)
#define SPI3_SLAVE1_REG                 MMIO32(SPI3_BASE + 0x03c)
// length of the master writing into the status register. (R/W)
#define SPI_SLV_STATUS_BITLEN_MSK       0xf8000000
#define SPI_SLV_STATUS_BITLEN_SFT       27
#define SPI_SLV_STATUS_BITLEN_SET(n)  ((n>>SPI_SLV_STATUS_BITLEN_SFT)&SPI_SLV_STATUS_BITLEN_MSK)
#define SPI_SLV_STATUS_FAST_EN          0x04000000
#define SPI_SLV_STATUS_READBACK         0x02000000
// address length in bits minus one for a slave-read operation.
#define SPI_SLV_RD_ADDR_BITLEN_MSK      0x0000fc00
#define SPI_SLV_RD_ADDR_BITLEN_SFT      10
#define SPI_SLV_RD_ADDR_BITLEN_SET(n)  ((n<<SPI_SLV_RD_ADDR_BITLEN_SFT)&SPI_SLV_RD_ADDR_BITLEN_MSK)
// address length in bits minus one for a slave-write operation.
#define SPI_SLV_WR_ADDR_BITLEN_MSK      0x000003f0
#define SPI_SLV_WR_ADDR_BITLEN_SFT      4
#define SPI_SLV_WR_ADDR_BITLEN_SET(n)  ((n<<SPI_SLV_WR_ADDR_BITLEN_SFT)&SPI_SLV_WR_ADDR_BITLEN_MSK)
// enables the dummy phase for write-status operations.
#define SPI_SLV_WRSTA_DUMMY_EN          0x00000008
// enables the dummy phase for read-status operations
#define SPI_SLV_RDSTA_DUMMY_EN          0x00000004
// enables the dummy phase for write-buffer operations
#define SPI_SLV_WRBUF_DUMMY_EN          0x00000002
// enables the dummy phase for read-buffer operations.
#define SPI_SLV_RDBUF_DUMMY_EN          0x00000001

/* Slave mode configuration and interrupt status */
#define SPI0_SLAVE2_REG                 MMIO32(SPI0_BASE + 0x040)
#define SPI1_SLAVE2_REG                 MMIO32(SPI1_BASE + 0x040)
#define SPI2_SLAVE2_REG                 MMIO32(SPI2_BASE + 0x040)
#define SPI3_SLAVE2_REG                 MMIO32(SPI3_BASE + 0x040)
// number of SPI clock cycles minus one for the dummy phase for write-data op.
#define SPI_SLV_WRBUF_DUMMY_CYCLELEN_MSK    0xff000000
#define SPI_SLV_WRBUF_DUMMY_CYCLELEN_SFT    24
#define SPI_SLV_WRBUF_DUMMY_CYCLELEN_SET(n) ((n<<SPI_SLV_WRBUF_DUMMY_CYCLELEN_SFT)&SPI_SLV_WRBUF_DUMMY_CYCLELEN_MSK)
// number of SPI clock cycles minus one for the dummy phase for read-data op.
#define SPI_SLV_RDBUF_DUMMY_CYCLELEN_MSK    0x00ff0000
#define SPI_SLV_RDBUF_DUMMY_CYCLELEN_SFT    16
#define SPI_SLV_RDBUF_DUMMY_CYCLELEN_SET(n) ((n<<SPI_SLV_RDBUF_DUMMY_CYCLELEN_SFT)&SPI_SLV_RDBUF_DUMMY_CYCLELEN_MSK)
// number of SPI clock cycles minus one for the dummy phase for write-status reg op.
#define SPI_SLV_WRSTA_DUMMY_CYCLELEN_MSK    0x0000ff00
#define SPI_SLV_WRSTA_DUMMY_CYCLELEN_SFT    8
#define SPI_SLV_WRSTA_DUMMY_CYCLELEN_SET(n) ((n<<SPI_SLV_WRSTA_DUMMY_CYCLELEN_SFT)&SPI_SLV_WRSTA_DUMMY_CYCLELEN_MSK)
// number of SPI clock cycles minus one for the dummy phase for read-status reg op.
#define SPI_SLV_RDSTA_DUMMY_CYCLELEN_MSK    0x000000ff
#define SPI_SLV_RDSTA_DUMMY_CYCLELEN_SET(n) (n&SPI_SLV_RDSTA_DUMMY_CYCLELEN_MSK)

/* Slave mode configuration and interrupt status */
#define SPI0_SLAVE3_REG                 MMIO32(SPI0_BASE + 0x048)
#define SPI1_SLAVE3_REG                 MMIO32(SPI1_BASE + 0x048)
#define SPI2_SLAVE3_REG                 MMIO32(SPI2_BASE + 0x048)
#define SPI3_SLAVE3_REG                 MMIO32(SPI3_BASE + 0x048)
#define SPI_SLV_WRSTA_CMD_VALUE_MSK    0xff000000
#define SPI_SLV_WRSTA_CMD_VALUE_SFT    24
#define SPI_SLV_WRSTA_CMD_VALUE_SET(n) ((n<<SPI_SLV_WRSTA_CMD_VALUE_SFT)&SPI_SLV_WRSTA_CMD_VALUE_MSK)
#define SPI_SLV_RDSTA_CMD_VALUE_MSK    0x00ff0000
#define SPI_SLV_RDSTA_CMD_VALUE_SFT    16
#define SPI_SLV_RDSTA_CMD_VALUE_SET(n) ((n<<SPI_SLV_RDSTA_CMD_VALUE_SFT)&SPI_SLV_RDSTA_CMD_VALUE_MSK)
#define SPI_SLV_WRBUF_CMD_VALUE_MSK    0x0000ff00
#define SPI_SLV_WRBUF_CMD_VALUE_SFT    8
#define SPI_SLV_WRBUF_CMD_VALUE_SET(n) ((n<<SPI_SLV_WRBUF_CMD_VALUE_SFT)&SPI_SLV_WRBUF_CMD_VALUE_MSK)
#define SPI_SLV_RDBUF_CMD_VALUE_MSK    0x000000ff
#define SPI_SLV_RDBUF_CMD_VALUE_SET(n) (n&SPI_SLV_RDBUF_CMD_VALUE_MSK)

/* Write-buffer operation length */
#define SPI0_SLV_WRBUF_DLEN_REG         MMIO32(SPI0_BASE + 0x048)
#define SPI1_SLV_WRBUF_DLEN_REG         MMIO32(SPI1_BASE + 0x048)
#define SPI2_SLV_WRBUF_DLEN_REG         MMIO32(SPI2_BASE + 0x048)
#define SPI3_SLV_WRBUF_DLEN_REG         MMIO32(SPI3_BASE + 0x048)
// length of written data minus one, in multiples of one bit.
#define SPI_SLV_WRBUF_DBITLEN   0xffffff

/* Read-buffer operation length */
#define SPI0_SLV_RDBUF_DLEN_REG         MMIO32(SPI0_BASE + 0x04c)
#define SPI1_SLV_RDBUF_DLEN_REG         MMIO32(SPI1_BASE + 0x04c)
#define SPI2_SLV_RDBUF_DLEN_REG         MMIO32(SPI2_BASE + 0x04c)
#define SPI3_SLV_RDBUF_DLEN_REG         MMIO32(SPI3_BASE + 0x04c)
// It indicates the length of read data minus one, in multiples of one bit.
#define SPI_SLV_RDBUF_DBITLEN   0xffffff

/* Read-buffer operation length */
#define SPI0_SLV_RD_BIT_REG             MMIO32(SPI0_BASE + 0x064)
#define SPI1_SLV_RD_BIT_REG             MMIO32(SPI1_BASE + 0x064)
#define SPI2_SLV_RD_BIT_REG             MMIO32(SPI2_BASE + 0x064)
#define SPI3_SLV_RD_BIT_REG             MMIO32(SPI3_BASE + 0x064)
// the bit length of data the master reads from the slave, minus one.
#define SPI_SLV_RDATA_BIT   0xffffff

/* SPI data register n */
#define SPI0_W0_REG                     MMIO32(SPI0_BASE + 0x080)
#define SPI1_W0_REG                     MMIO32(SPI1_BASE + 0x080)
#define SPI2_W0_REG                     MMIO32(SPI2_BASE + 0x080)
#define SPI3_W0_REG                     MMIO32(SPI3_BASE + 0x080)
#define SPI0_W1_REG                     MMIO32(SPI0_BASE + 0x084)
#define SPI1_W1_REG                     MMIO32(SPI1_BASE + 0x084)
#define SPI2_W1_REG                     MMIO32(SPI2_BASE + 0x084)
#define SPI3_W1_REG                     MMIO32(SPI3_BASE + 0x084)
#define SPI0_W2_REG                     MMIO32(SPI0_BASE + 0x088)
#define SPI1_W2_REG                     MMIO32(SPI1_BASE + 0x088)
#define SPI2_W2_REG                     MMIO32(SPI2_BASE + 0x088)
#define SPI3_W2_REG                     MMIO32(SPI3_BASE + 0x088)
#define SPI0_W3_REG                     MMIO32(SPI0_BASE + 0x08c)
#define SPI1_W3_REG                     MMIO32(SPI1_BASE + 0x08c)
#define SPI2_W3_REG                     MMIO32(SPI2_BASE + 0x08c)
#define SPI3_W3_REG                     MMIO32(SPI3_BASE + 0x08c)
#define SPI0_W4_REG                     MMIO32(SPI0_BASE + 0x090)
#define SPI1_W4_REG                     MMIO32(SPI1_BASE + 0x090)
#define SPI2_W4_REG                     MMIO32(SPI2_BASE + 0x090)
#define SPI3_W4_REG                     MMIO32(SPI3_BASE + 0x090)
#define SPI0_W5_REG                     MMIO32(SPI0_BASE + 0x094)
#define SPI1_W5_REG                     MMIO32(SPI1_BASE + 0x094)
#define SPI2_W5_REG                     MMIO32(SPI2_BASE + 0x094)
#define SPI3_W5_REG                     MMIO32(SPI3_BASE + 0x094)
#define SPI0_W6_REG                     MMIO32(SPI0_BASE + 0x098)
#define SPI1_W6_REG                     MMIO32(SPI1_BASE + 0x098)
#define SPI2_W6_REG                     MMIO32(SPI2_BASE + 0x098)
#define SPI3_W6_REG                     MMIO32(SPI3_BASE + 0x098)
#define SPI0_W7_REG                     MMIO32(SPI0_BASE + 0x09c)
#define SPI1_W7_REG                     MMIO32(SPI1_BASE + 0x09c)
#define SPI2_W7_REG                     MMIO32(SPI2_BASE + 0x09c)
#define SPI3_W7_REG                     MMIO32(SPI3_BASE + 0x09c)
#define SPI0_W8_REG                     MMIO32(SPI0_BASE + 0x0a0)
#define SPI1_W8_REG                     MMIO32(SPI1_BASE + 0x0a0)
#define SPI2_W8_REG                     MMIO32(SPI2_BASE + 0x0a0)
#define SPI3_W8_REG                     MMIO32(SPI3_BASE + 0x0a0)
#define SPI0_W9_REG                     MMIO32(SPI0_BASE + 0x0a4)
#define SPI1_W9_REG                     MMIO32(SPI1_BASE + 0x0a4)
#define SPI2_W9_REG                     MMIO32(SPI2_BASE + 0x0a4)
#define SPI3_W9_REG                     MMIO32(SPI3_BASE + 0x0a4)
#define SPI0_W10_REG                    MMIO32(SPI0_BASE + 0x0a8)
#define SPI1_W10_REG                    MMIO32(SPI1_BASE + 0x0a8)
#define SPI2_W10_REG                    MMIO32(SPI2_BASE + 0x0a8)
#define SPI3_W10_REG                    MMIO32(SPI3_BASE + 0x0a8)
#define SPI0_W11_REG                    MMIO32(SPI0_BASE + 0x0ac)
#define SPI1_W11_REG                    MMIO32(SPI1_BASE + 0x0ac)
#define SPI2_W11_REG                    MMIO32(SPI2_BASE + 0x0ac)
#define SPI3_W11_REG                    MMIO32(SPI3_BASE + 0x0ac)
#define SPI0_W12_REG                    MMIO32(SPI0_BASE + 0x0b0)
#define SPI1_W12_REG                    MMIO32(SPI1_BASE + 0x0b0)
#define SPI2_W12_REG                    MMIO32(SPI2_BASE + 0x0b0)
#define SPI3_W12_REG                    MMIO32(SPI3_BASE + 0x0b0)
#define SPI0_W13_REG                    MMIO32(SPI0_BASE + 0x0b4)
#define SPI1_W13_REG                    MMIO32(SPI1_BASE + 0x0b4)
#define SPI2_W13_REG                    MMIO32(SPI2_BASE + 0x0b4)
#define SPI3_W13_REG                    MMIO32(SPI3_BASE + 0x0b4)
#define SPI0_W14_REG                    MMIO32(SPI0_BASE + 0x0b8)
#define SPI1_W14_REG                    MMIO32(SPI1_BASE + 0x0b8)
#define SPI2_W14_REG                    MMIO32(SPI2_BASE + 0x0b8)
#define SPI3_W14_REG                    MMIO32(SPI3_BASE + 0x0b8)
#define SPI0_W15_REG                    MMIO32(SPI0_BASE + 0x0bc)
#define SPI1_W15_REG                    MMIO32(SPI1_BASE + 0x0bc)
#define SPI2_W15_REG                    MMIO32(SPI2_BASE + 0x0bc)
#define SPI3_W15_REG                    MMIO32(SPI3_BASE + 0x0bc)
// Data buffer. (R/W)

/*  */
#define SPI0_TX_CRC_REG                 MMIO32(SPI0_BASE + 0x0c0)
#define SPI1_TX_CRC_REG                 MMIO32(SPI1_BASE + 0x0c0)
#define SPI2_TX_CRC_REG                 MMIO32(SPI2_BASE + 0x0c0)
#define SPI3_TX_CRC_REG                 MMIO32(SPI3_BASE + 0x0c0)

/*  */
#define SPI0_EXT2_REG                   MMIO32(SPI0_BASE + 0x0f8)
#define SPI1_EXT2_REG                   MMIO32(SPI1_BASE + 0x0f8)
#define SPI2_EXT2_REG                   MMIO32(SPI2_BASE + 0x0f8)
#define SPI3_EXT2_REG                   MMIO32(SPI3_BASE + 0x0f8)
// The current state of the SPI state machine: (RO)
#define SPI_ST_IDLE         0x0
#define SPI_ST_PREPARATION  0x1
#define SPI_ST_SEND_CMD     0x2
#define SPI_ST_SEND_DATA    0x3
#define SPI_ST_READ_DATA    0x4
#define SPI_ST_WRITE_DATA   0x5
#define SPI_ST_WAIT_STATE   0x6
#define SPI_ST_DONE_STATE   0x7

/* DMA configuration register */
#define SPI0_DMA_CONF_REG               MMIO32(SPI0_BASE + 0x100)
#define SPI1_DMA_CONF_REG               MMIO32(SPI1_BASE + 0x100)
#define SPI2_DMA_CONF_REG               MMIO32(SPI2_BASE + 0x100)
#define SPI3_DMA_CONF_REG               MMIO32(SPI3_BASE + 0x100)
// This bit enables SPI DMA continuous data TX/RX mode. (R/W)
#define SPI_DMA_CONTINUE        0x10000
// When in continuous TX/RX mode, setting this bit stops sending data. (R/W)
#define SPI_DMA_TX_STOP         0x08000
// When in continuous TX/RX mode, setting this bit stops receiving data. (R/W)
#define SPI_DMA_RX_STOP         0x04000
// SPI DMA reads data from memory in burst mode. (R/W)
#define SPI_OUT_DATA_BURST_EN   0x02000
// SPI DMA reads inlink descriptor in burst mode. (R/W)
#define SPI_INDSCR_BURST_EN     0x00800
// SPI DMA reads outlink descriptor in burst mode. (R/W)
#define SPI_OUTDSCR_BURST_EN    0x00400
// DMA out-EOF-flag generation mode. (R/W)
#define SPI_OUT_EOF_MODE        0x00200
// reset SPI DMA AHB master. (R/W)
#define SPI_AHBM_RST            0x00020
// This bit is used to reset SPI DMA AHB master FIFO pointer. (R/W)
#define SPI_AHBM_FIFO_RST       0x00010
// The bit is used to reset DMA out-FSM and out-data FIFO pointer. (R/W)
#define SPI_OUT_RST             0x00008
// The bit is used to reset DMA in-DSM and in-data FIFO pointer. (R/W)
#define SPI_IN_RST              0x00004

/* DMA outlink address and configuration */
#define SPI0_DMA_OUT_LINK_REG           MMIO32(SPI0_BASE + 0x104)
#define SPI1_DMA_OUT_LINK_REG           MMIO32(SPI1_BASE + 0x104)
#define SPI2_DMA_OUT_LINK_REG           MMIO32(SPI2_BASE + 0x104)
#define SPI3_DMA_OUT_LINK_REG           MMIO32(SPI3_BASE + 0x104)
// Set the bit to add new outlink descriptors. (R/W)
#define SPI_OUTLINK_RESTART     0x40000000
// Set the bit to start to use outlink descriptor. (R/W)
#define SPI_OUTLINK_START       0x20000000
// Set the bit to stop to use outlink descriptor. (R/W)
#define SPI_OUTLINK_STOP        0x10000000
// The address of the first outlink descriptor. (R/W)
#define SPI_OUTLINK_ADDR_MSK    0x000fffff
#define SPI_OUTLINK_ADDR_SET(n) (n&SPI_OUTLINK_ADDR_MSK)

/* DMA inlink address and configuration */
#define SPI0_DMA_IN_LINK_REG            MMIO32(SPI0_BASE + 0x108)
#define SPI1_DMA_IN_LINK_REG            MMIO32(SPI1_BASE + 0x108)
#define SPI2_DMA_IN_LINK_REG            MMIO32(SPI2_BASE + 0x108)
#define SPI3_DMA_IN_LINK_REG            MMIO32(SPI3_BASE + 0x108)
// Set the bit to add new inlink descriptors. (R/W)
#define SPI_INLINK_RESTART      0x40000000
// Set the bit to start to use inlink descriptor. (R/W)
#define SPI_INLINK_START        0x20000000
// Set the bit to stop to use inlink descriptor. (R/W)
#define SPI_INLINK_STOP         0x10000000
// inlink descriptor jumps to the next descriptor when a packet is invalid. (R/W)
#define SPI_INLINK_AUTO_RET     0x08000000
// The address of the first inlink descriptor. (R/W)
#define SPI_INLINK_ADDR_MSK     0x000fffff
#define SPI_INLINK_ADDR_SET(n)  (n&SPI_INLINK_ADDR_MSK)

/* DMA status */
#define SPI0_DMA_STATUS_REG             MMIO32(SPI0_BASE + 0x10c)
#define SPI1_DMA_STATUS_REG             MMIO32(SPI1_BASE + 0x10c)
#define SPI2_DMA_STATUS_REG             MMIO32(SPI2_BASE + 0x10c)
#define SPI3_DMA_STATUS_REG             MMIO32(SPI3_BASE + 0x10c)
// SPI DMA write-data status bit. (RO)
#define SPI_DMA_TX_EN   0x2
// SPI DMA read-data status bit. (RO)
#define SPI_DMA_RX_EN   0x1

/* Interrupt enable bits */
#define SPI0_DMA_INT_ENA_REG            MMIO32(SPI0_BASE + 0x110)
#define SPI1_DMA_INT_ENA_REG            MMIO32(SPI1_BASE + 0x110)
#define SPI2_DMA_INT_ENA_REG            MMIO32(SPI2_BASE + 0x110)
#define SPI3_DMA_INT_ENA_REG            MMIO32(SPI3_BASE + 0x110)
// interrupt enable bit for the SPI_OUT_TOTAL_EOF_INT interrupt. (R/W)
#define SPI_OUT_TOTAL_EOF_INT_ENA       0x100
// The interrupt enable bit for the SPI_OUT_EOF_INT interrupt. (R/W)
#define SPI_OUT_EOF_INT_ENA             0x080
// The interrupt enable bit for the SPI_OUT_DONE_INT interrupt. (R/W)
#define SPI_OUT_DONE_INT_ENA            0x040
// The interrupt enable bit for the SPI_IN_SUC_EOF_INT interrupt. (R/W)
#define SPI_IN_SUC_EOF_INT_ENA          0x020
// The interrupt enable bit for the SPI_IN_ERR_EOF_INT interrupt. (R/W)
#define SPI_IN_ERR_EOF_INT_ENA          0x010
// The interrupt enable bit for the SPI_IN_DONE_INT interrupt. (R/W)
#define SPI_IN_DONE_INT_ENA             0x008
// The interrupt enable bit for the SPI_INLINK_DSCR_ERROR_INT interrupt. (R/W)
#define SPI_INLINK_DSCR_ERROR_INT_ENA   0x004
// The interrupt enable bit for the SPI_OUTLINK_DSCR_ERROR_INT interrupt. (R/W)
#define SPI_OUTLINK_DSCR_ERROR_INT_ENA  0x002
// The interrupt enable bit for the SPI_INLINK_DSCR_EMPTY_INT interrupt. (R/W)
#define SPI_INLINK_DSCR_EMPTY_INT_ENA   0x001

/* Raw interrupt status */
#define SPI0_DMA_INT_RAW_REG            MMIO32(SPI0_BASE + 0x114)
#define SPI1_DMA_INT_RAW_REG            MMIO32(SPI1_BASE + 0x114)
#define SPI2_DMA_INT_RAW_REG            MMIO32(SPI2_BASE + 0x114)
#define SPI3_DMA_INT_RAW_REG            MMIO32(SPI3_BASE + 0x114)
// The raw interrupt status bit for the SPI_OUT_TOTAL_EOF_INT interrupt. (R/W)
#define SPI_OUT_TOTAL_EOF_INT_RAW       0x100
// The raw interrupt status bit for the SPI_OUT_EOF_INT interrupt. (R/W)
#define SPI_OUT_EOF_INT_RAW             0x080
// The raw interrupt status bit for the SPI_OUT_DONE_INT interrupt. (R/W)
#define SPI_OUT_DONE_INT_RAW            0x040
// The raw interrupt status bit for the SPI_IN_SUC_EOF_INT interrupt. (R/W)
#define SPI_IN_SUC_EOF_INT_RAW          0x020
// The raw interrupt status bit for the SPI_IN_ERR_EOF_INT interrupt. (R/W)
#define SPI_IN_ERR_EOF_INT_RAW          0x010
// TThe raw interrupt status bit for the SPI_IN_DONE_INT interrupt. (R/W)
#define SPI_IN_DONE_INT_RAW             0x008
// The raw interrupt status bit for the SPI_INLINK_DSCR_ERROR_INT interrupt. (R/W)
#define SPI_INLINK_DSCR_ERROR_INT_RAW   0x004
// The raw interrupt status bit for the SPI_OUTLINK_DSCR_ERROR_INT interrupt. (R/W)
#define SPI_OUTLINK_DSCR_ERROR_INT_RAW  0x002
// The raw interrupt status bit for the SPI_INLINK_DSCR_EMPTY_INT interrupt. (R/W)
#define SPI_INLINK_DSCR_EMPTY_INT_RAW   0x001

/* Masked interrupt status */
#define SPI0_DMA_INT_ST_REG             MMIO32(SPI0_BASE + 0x118)
#define SPI1_DMA_INT_ST_REG             MMIO32(SPI1_BASE + 0x118)
#define SPI2_DMA_INT_ST_REG             MMIO32(SPI2_BASE + 0x118)
#define SPI3_DMA_INT_ST_REG             MMIO32(SPI3_BASE + 0x118)
// The masked interrupt status bit for the SPI_OUT_TOTAL_EOF_INT interrupt. (R/W)
#define SPI_OUT_TOTAL_EOF_INT_ST        0x100
// The masked interrupt status bit for the SPI_OUT_EOF_INT interrupt. (R/W)
#define SPI_OUT_EOF_INT_ST              0x080
// The masked interrupt status bit for the SPI_OUT_DONE_INT interrupt. (R/W)
#define SPI_OUT_DONE_INT_ST             0x040
// The masked interrupt status bit for the SPI_IN_SUC_EOF_INT interrupt. (R/W)
#define SPI_IN_SUC_EOF_INT_ST           0x020
// The masked interrupt status bit for the SPI_IN_ERR_EOF_INT interrupt. (R/W)
#define SPI_IN_ERR_EOF_INT_ST           0x010
// The masked interrupt status bit for the SPI_IN_DONE_INT interrupt. (R/W)
#define SPI_IN_DONE_INT_ST              0x008
// The masked interrupt status bit for the SPI_INLINK_DSCR_ERROR_INT int. (R/W)
#define SPI_INLINK_DSCR_ERROR_INT_ST    0x004
// The masked interrupt status bit for the SPI_OUTLINK_DSCR_ERROR_INT int. (R/W)
#define SPI_OUTLINK_DSCR_ERROR_INT_ST   0x002
// The masked interrupt status bit for the SPI_INLINK_DSCR_EMPTY_INT int. (R/W)
#define SPI_INLINK_DSCR_EMPTY_INTR_ST   0x001

/* Interrupt clear bits */
#define SPI0_DMA_INT_CLR_REG            MMIO32(SPI0_BASE + 0x11c)
#define SPI1_DMA_INT_CLR_REG            MMIO32(SPI1_BASE + 0x11c)
#define SPI2_DMA_INT_CLR_REG            MMIO32(SPI2_BASE + 0x11c)
#define SPI3_DMA_INT_CLR_REG            MMIO32(SPI3_BASE + 0x11c)
// Set this bit to clear the SPI_OUT_TOTAL_EOF_INT interrupt. (R/W)
#define SPI_OUT_TOTAL_EOF_INT_CLR       0x100
// TSet this bit to clear the SPI_OUT_EOF_INT interrupt. (R/W)
#define SPI_OUT_EOF_INT_CLR             0x080
// Set this bit to clear the SPI_OUT_DONE_INT interrupt. (R/W)
#define SPI_OUT_DONE_INT_CLR            0x040
// Set this bit to clear the SPI_IN_SUC_EOF_INT interrupt. (R/W)
#define SPI_IN_SUC_EOF_INT_CLR          0x020
// Set this bit to clear the SPI_IN_ERR_EOF_INT interrupt. (R/W)
#define SPI_IN_ERR_EOF_INT_CLR          0x010
// Set this bit to clear the SPI_IN_DONE_INT interrupt. (R/W)
#define SPI_IN_DONE_INT_CLR             0x008
// Set this bit to clear the SPI_INLINK_DSCR_ERROR_INT int. (R/W)
#define SPI_INLINK_DSCR_ERROR_INT_CLR   0x004
// Set this bit to clear the SPI_OUTLINK_DSCR_ERROR_INT int. (R/W)
#define SPI_OUTLINK_DSCR_ERROR_INT_CLR  0x002
// Set this bit to clear the SPI_INLINK_DSCR_EMPTY_INT int. (R/W)
#define SPI_INLINK_DSCR_EMPTY_INTR_CLR  0x001

/* Descriptor address where an error occurs */
#define SPI0_IN_ERR_EOF_DES_ADDR_REG    MMIO32(SPI0_BASE + 0x120)
#define SPI1_IN_ERR_EOF_DES_ADDR_REG    MMIO32(SPI1_BASE + 0x120)
#define SPI2_IN_ERR_EOF_DES_ADDR_REG    MMIO32(SPI2_BASE + 0x120)
#define SPI3_IN_ERR_EOF_DES_ADDR_REG    MMIO32(SPI3_BASE + 0x120)
// The inlink descriptor address when SPI DMA encountered an err in rx data. (RO)

/* Descriptor address where EOF occurs */
#define SPI0_IN_SUC_EOF_DES_ADDR_REG    MMIO32(SPI0_BASE + 0x124)
#define SPI1_IN_SUC_EOF_DES_ADDR_REG    MMIO32(SPI1_BASE + 0x124)
#define SPI2_IN_SUC_EOF_DES_ADDR_REG    MMIO32(SPI2_BASE + 0x124)
#define SPI3_IN_SUC_EOF_DES_ADDR_REG    MMIO32(SPI3_BASE + 0x124)
// The last inlink descriptor address when SPI DMA encountered EOF. (RO)

/* Current descriptor pointer */
#define SPI0_INLINK_DSCR_REG            MMIO32(SPI0_BASE + 0x128)
#define SPI1_INLINK_DSCR_REG            MMIO32(SPI1_BASE + 0x128)
#define SPI2_INLINK_DSCR_REG            MMIO32(SPI2_BASE + 0x128)
#define SPI3_INLINK_DSCR_REG            MMIO32(SPI3_BASE + 0x128)
// The address of the current inlink descriptor. (RO)

/* Next descriptor data  pointer */
#define SPI0_INLINK_DSCR_BF0_REG        MMIO32(SPI0_BASE + 0x12c)
#define SPI1_INLINK_DSCR_BF0_REG        MMIO32(SPI1_BASE + 0x12c)
#define SPI2_INLINK_DSCR_BF0_REG        MMIO32(SPI2_BASE + 0x12c)
#define SPI3_INLINK_DSCR_BF0_REG        MMIO32(SPI3_BASE + 0x12c)
// The address of the next inlink descriptor. (RO)

/* Current descriptor data pointer */
#define SPI0_INLINK_DSCR_BF1_REG        MMIO32(SPI0_BASE + 0x130)
#define SPI1_INLINK_DSCR_BF1_REG        MMIO32(SPI1_BASE + 0x130)
#define SPI2_INLINK_DSCR_BF1_REG        MMIO32(SPI2_BASE + 0x130)
#define SPI3_INLINK_DSCR_BF1_REG        MMIO32(SPI3_BASE + 0x130)
// The address of the next inlink data buffer. (RO)

/* Relative buffer address where EOF occurs */
#define SPI0_OUT_EOF_BFR_DES_ADDR_REG   MMIO32(SPI0_BASE + 0x134)
#define SPI1_OUT_EOF_BFR_DES_ADDR_REG   MMIO32(SPI1_BASE + 0x134)
#define SPI2_OUT_EOF_BFR_DES_ADDR_REG   MMIO32(SPI2_BASE + 0x134)
#define SPI3_OUT_EOF_BFR_DES_ADDR_REG   MMIO32(SPI3_BASE + 0x134)
// The buffer address corresponding to the outlink descriptor that prod EOF. (RO)

/* Descriptor address where EOF occurs */
#define SPI0_OUT_EOF_DES_ADDR_REG       MMIO32(SPI0_BASE + 0x138)
#define SPI1_OUT_EOF_DES_ADDR_REG       MMIO32(SPI1_BASE + 0x138)
#define SPI2_OUT_EOF_DES_ADDR_REG       MMIO32(SPI2_BASE + 0x138)
#define SPI3_OUT_EOF_DES_ADDR_REG       MMIO32(SPI3_BASE + 0x138)
// The last outlink descriptor address when SPI DMA encountered EOF. (RO)

/* Current descriptor pointer */
#define SPI0_OUTLINK_DSCR_REG           MMIO32(SPI0_BASE + 0x13c)
#define SPI1_OUTLINK_DSCR_REG           MMIO32(SPI1_BASE + 0x13c)
#define SPI2_OUTLINK_DSCR_REG           MMIO32(SPI2_BASE + 0x13c)
#define SPI3_OUTLINK_DSCR_REG           MMIO32(SPI3_BASE + 0x13c)
// The address of the current outlink descriptor. (RO)

/* Next descriptor data pointer */
#define SPI0_OUTLINK_DSCR_BF0_REG       MMIO32(SPI0_BASE + 0x140)
#define SPI1_OUTLINK_DSCR_BF0_REG       MMIO32(SPI1_BASE + 0x140)
#define SPI2_OUTLINK_DSCR_BF0_REG       MMIO32(SPI2_BASE + 0x140)
#define SPI3_OUTLINK_DSCR_BF0_REG       MMIO32(SPI3_BASE + 0x140)
// The address of the next outlink descriptor. (RO)

/* Current descriptor data pointer */
#define SPI0_OUTLINK_DSCR_BF1_REG       MMIO32(SPI0_BASE + 0x144)
#define SPI1_OUTLINK_DSCR_BF1_REG       MMIO32(SPI1_BASE + 0x144)
#define SPI2_OUTLINK_DSCR_BF1_REG       MMIO32(SPI2_BASE + 0x144)
#define SPI3_OUTLINK_DSCR_BF1_REG       MMIO32(SPI3_BASE + 0x144)
// The address of the next outlink data buffer. (RO)

/* DMA memory read status */
#define SPI0_DMA_RSTATUS_REG            MMIO32(SPI0_BASE + 0x148)
#define SPI1_DMA_RSTATUS_REG            MMIO32(SPI1_BASE + 0x148)
#define SPI2_DMA_RSTATUS_REG            MMIO32(SPI2_BASE + 0x148)
#define SPI3_DMA_RSTATUS_REG            MMIO32(SPI3_BASE + 0x148)
// The SPI DMA TX FIFO is empty. (RO)
#define TX_FIFO_EMPTY           0x80000000
// The SPI DMA TX FIFO is full. (RO)
#define TX_FIFO_FULL            0x40000000
// The LSB of the SPI DMA outlink descriptor address. (RO)
#define TX_DES_ADDRESS_MSK      0x000fffff
#define TX_DES_ADDRESS0_GET     (SPI0_DMA_RSTATUS_REG&TX_DES_ADDRESS_MSK)
#define TX_DES_ADDRESS1_GET     (SPI1_DMA_RSTATUS_REG&TX_DES_ADDRESS_MSK)
#define TX_DES_ADDRESS2_GET     (SPI2_DMA_RSTATUS_REG&TX_DES_ADDRESS_MSK)
#define TX_DES_ADDRESS3_GET     (SPI3_DMA_RSTATUS_REG&TX_DES_ADDRESS_MSK)

/* DMA memory write status */
#define SPI0_DMA_TSTATUS_REG            MMIO32(SPI0_BASE + 0x14c)
#define SPI1_DMA_TSTATUS_REG            MMIO32(SPI1_BASE + 0x14c)
#define SPI2_DMA_TSTATUS_REG            MMIO32(SPI2_BASE + 0x14c)
#define SPI3_DMA_TSTATUS_REG            MMIO32(SPI3_BASE + 0x14c)
// The SPI DMA TX FIFO is empty. (RO)
#define RX_FIFO_EMPTY           0x80000000
// The SPI DMA TX FIFO is full. (RO)
#define RX_FIFO_FULL            0x40000000
// The LSB of the SPI DMA outlink descriptor address. (RO)
#define RX_DES_ADDRESS_MSK      0x000fffff
#define RX_DES_ADDRESS0_GET     (SPI0_DMA_TSTATUS_REG&RX_DES_ADDRESS_MSK)
#define RX_DES_ADDRESS1_GET     (SPI1_DMA_TSTATUS_REG&RX_DES_ADDRESS_MSK)
#define RX_DES_ADDRESS2_GET     (SPI2_DMA_TSTATUS_REG&RX_DES_ADDRESS_MSK)
#define RX_DES_ADDRESS3_GET     (SPI3_DMA_TSTATUS_REG&RX_DES_ADDRESS_MSK)

#endif
