#ifndef H_RESET_CLOCK
#define H_RESET_CLOCK
/*
 * ESP32 alternative library,
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Configures system clock frequency */
#define APB_CTRL_SYSCLK_CONF_REG    MMIO32(SYSCON + 0x00)
// divider value of CPU_CLK when the source of CPU_CLK is XTL_CLK or RC_FAST_CLK.
#define APB_CTRL_PRE_DIV_CNT_MSK    0x3ff

/* Configures the divider value of REF_TICK */
#define APB_CTRL_XTAL_TICK_CONF_REG MMIO32(SYSCON + 0x04)
// divider value of REF_TICK when the source of APB_CLK is XTL_CLK.
#define APB_CTRL_XTAL_TICK_NUM_MSK  0xff

/* Configures the divider value of REF_TICK */
#define APB_CTRL_PLL_TICK_CONF_REG  MMIO32(SYSCON + 0x08)
// divider value of REF_TICK when the source of APB_CLK is PLL_CLK.
#define APB_CTRL_PLL_TICK_NUM_MSK   0xff

/* Configures the divider value of REF_TICK */
#define APB_CTRL_CK8M_TICK_CONF_REG MMIO32(SYSCON + 0x0c)
// divider value of REF_TICK when the source of APB_CLK is FOSC_CLK.
#define APB_CTRL_CK8M_TICK_NUM_MSK  0xff

/* Configures the divider value of REF_TICK */
#define APB_CTRL_APLL_TICK_CONF_REG MMIO32(SYSCON + 0x3c)
// divider value of REF_TICK when the source of APB_CLK is APLL_CLK.
#define APB_CTRL_APLL_TICK_NUM_MSK  0xff

/* Chip revision register */
#define APB_CTRL_DATE_REG           MMIO32(SYSCON + 0x7c)

#endif
