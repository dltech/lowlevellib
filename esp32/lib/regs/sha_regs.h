#ifndef H_SHA_REGS
#define H_SHA_REGS
/*
 * ESP32 alternative library, SHA Accelerator registers.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* SHA encrypted/decrypted data register n */
#define SHA_TEXT_0_REG          MMIO32(SHA_ACCELERATOR + 0x00)
#define SHA_TEXT_1_REG          MMIO32(SHA_ACCELERATOR + 0x04)
#define SHA_TEXT_2_REG          MMIO32(SHA_ACCELERATOR + 0x08)
#define SHA_TEXT_3_REG          MMIO32(SHA_ACCELERATOR + 0x0c)
#define SHA_TEXT_4_REG          MMIO32(SHA_ACCELERATOR + 0x10)
#define SHA_TEXT_5_REG          MMIO32(SHA_ACCELERATOR + 0x14)
#define SHA_TEXT_6_REG          MMIO32(SHA_ACCELERATOR + 0x18)
#define SHA_TEXT_7_REG          MMIO32(SHA_ACCELERATOR + 0x1c)
#define SHA_TEXT_8_REG          MMIO32(SHA_ACCELERATOR + 0x20)
#define SHA_TEXT_9_REG          MMIO32(SHA_ACCELERATOR + 0x24)
#define SHA_TEXT_10_REG         MMIO32(SHA_ACCELERATOR + 0x28)
#define SHA_TEXT_11_REG         MMIO32(SHA_ACCELERATOR + 0x2c)
#define SHA_TEXT_12_REG         MMIO32(SHA_ACCELERATOR + 0x30)
#define SHA_TEXT_13_REG         MMIO32(SHA_ACCELERATOR + 0x34)
#define SHA_TEXT_14_REG         MMIO32(SHA_ACCELERATOR + 0x38)
#define SHA_TEXT_15_REG         MMIO32(SHA_ACCELERATOR + 0x3c)
#define SHA_TEXT_16_REG         MMIO32(SHA_ACCELERATOR + 0x40)
#define SHA_TEXT_17_REG         MMIO32(SHA_ACCELERATOR + 0x44)
#define SHA_TEXT_18_REG         MMIO32(SHA_ACCELERATOR + 0x48)
#define SHA_TEXT_19_REG         MMIO32(SHA_ACCELERATOR + 0x4c)
#define SHA_TEXT_20_REG         MMIO32(SHA_ACCELERATOR + 0x50)
#define SHA_TEXT_21_REG         MMIO32(SHA_ACCELERATOR + 0x54)
#define SHA_TEXT_22_REG         MMIO32(SHA_ACCELERATOR + 0x58)
#define SHA_TEXT_23_REG         MMIO32(SHA_ACCELERATOR + 0x5c)
#define SHA_TEXT_24_REG         MMIO32(SHA_ACCELERATOR + 0x60)
#define SHA_TEXT_25_REG         MMIO32(SHA_ACCELERATOR + 0x64)
#define SHA_TEXT_26_REG         MMIO32(SHA_ACCELERATOR + 0x68)
#define SHA_TEXT_27_REG         MMIO32(SHA_ACCELERATOR + 0x6c)
#define SHA_TEXT_28_REG         MMIO32(SHA_ACCELERATOR + 0x70)
#define SHA_TEXT_29_REG         MMIO32(SHA_ACCELERATOR + 0x74)
#define SHA_TEXT_30_REG         MMIO32(SHA_ACCELERATOR + 0x78)
#define SHA_TEXT_31_REG         MMIO32(SHA_ACCELERATOR + 0x7c)
// SHA Message block and hash result register. (R/W)

/* Control register to initiate SHA1 operation */
#define SHA_SHA1_START_REG      MMIO32(SHA_ACCELERATOR + 0x80)
// Write 1 to start an SHA-1 operation on the first message block. (WO)
#define SHA_SHA1_START  0x1

/* Control register to continue SHA1 operation */
#define SHA_SHA1_CONTINUE_REG   MMIO32(SHA_ACCELERATOR + 0x84)
// Write 1 to continue the SHA-1 operation with subsequent blocks. (WO)
#define SHA_SHA1_CONTINUE   0x1

/* Control register to calculate the final SHA1 hash */
#define SHA_SHA1_LOAD_REG       MMIO32(SHA_ACCELERATOR + 0x88)
// Write 1 to finish the SHA-1 operation to calculate the final message hash. (WO)
#define SHA_SHA1_LOAD   0x1

/* Status register for SHA1 operation */
#define SHA_SHA1_BUSY_REG       MMIO32(SHA_ACCELERATOR + 0x8c)
// SHA-1 oper status: 1 if the SHA accelerator is processing data, 0 if it is idle.
#define SHA_SHA1_BUSY   0x1

/* Control register to initiate SHA256 operation */
#define SHA_SHA256_START_REG    MMIO32(SHA_ACCELERATOR + 0x90)
// Write 1 to start an SHA-256 operation on the first message block. (WO)
#define SHA_SHA256_START    0x1

/* Control register to continue SHA256 operation */
#define SHA_SHA256_CONTINUE_REG MMIO32(SHA_ACCELERATOR + 0x94)
// Write 1 to continue the SHA-256 operation with subsequent blocks. (WO)
#define SHA_SHA256_CONTINUE 0x1

/* Control register to calculate the final SHA256 hash */
#define SHA_SHA256_LOAD_REG     MMIO32(SHA_ACCELERATOR + 0x98)
// Write 1 to finish the SHA-256 operation to calculate the final message hash.
#define SHA_SHA256_LOAD 0x1

/* Status register for SHA256 operation */
#define SHA_SHA256_BUSY_REG     MMIO32(SHA_ACCELERATOR + 0x9c)
// SHA-256 oper status: 1 if the SHA accelerator is processing data, 0 if it is idle
#define SHA_SHA256_BUSY 0x1

/* Control register to initiate SHA384 operation */
#define SHA_SHA384_START_REG    MMIO32(SHA_ACCELERATOR + 0xa0)
// Write 1 to start an SHA-384 operation on the first message block. (WO)
#define SHA_SHA384_START    0x1

/* Control register to continue SHA384 operation */
#define SHA_SHA384_CONTINUE_REG MMIO32(SHA_ACCELERATOR + 0xa4)
// Write 1 to continue the SHA-384 operation with subsequent blocks. (WO)
#define SHA_SHA384_CONTINUE 0x1

/* Control register to calculate the final SHA384 hash */
#define SHA_SHA384_LOAD_REG     MMIO32(SHA_ACCELERATOR + 0xa8)
// Write 1 to finish the SHA-384 operation to calculate the final message hash.
#define SHA_SHA384_LOAD 0x1

/* Status register for SHA384 operation */
#define SHA_SHA384_BUSY_REG     MMIO32(SHA_ACCELERATOR + 0xac)
// SHA-384 oper status: 1 if the SHA accelerator is processing data, 0 if it is idle
#define SHA_SHA384_BUSY 0x1

/* Control register to initiate SHA512 operation */
#define SHA_SHA512_START_REG    MMIO32(SHA_ACCELERATOR + 0xb0)
// Write 1 to start an SHA-512 operation on the first message block. (WO)
#define SHA_SHA512_START    0x1

/* Control register to continue SHA512 operation */
#define SHA_SHA512_CONTINUE_REG MMIO32(SHA_ACCELERATOR + 0xb4)
// Write 1 to continue the SHA-512 operation with subsequent blocks. (WO)
#define SHA_SHA512_CONTINUE 0x1

/* Control register to calculate the final SHA512 hash */
#define SHA_SHA512_LOAD_REG     MMIO32(SHA_ACCELERATOR + 0xb8)
// Write 1 to finish the SHA-512 operation to calculate the final message hash.
#define SHA_SHA512_LOAD 0x1

/* Status register for SHA512 operation */
#define SHA_SHA512_BUSY_REG     MMIO32(SHA_ACCELERATOR + 0xbc)
// SHA-512 oper status: 1 if the SHA accelerator is processing data, 0 if it is idle
#define SHA_SHA512_BUSY 0x1

#endif
