#ifndef H_I2S_REGS
#define H_I2S_REGS
/*
 * ESP32 alternative library, I2S Controller registers
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Writes the data sent by I2S into FIFO */
#define I2S0_FIFO_WR_REG                MMIO32(I2S0_BASE + 0x00)
#define I2S1_FIFO_WR_REG                MMIO32(I2S1_BASE + 0x00)
// Writes the data sent by I2S into FIFO. (WO)

/* Stores the data that I2S receives from FIFO */
#define I2S0_FIFO_RD_REG                MMIO32(I2S0_BASE + 0x04)
#define I2S1_FIFO_RD_REG                MMIO32(I2S1_BASE + 0x04)
// Stores the data that I2S receives from FIFO. (RO)

/* Configuration and start/stop bits */
#define I2S0_CONF_REG                   MMIO32(I2S0_BASE + 0x08)
#define I2S1_CONF_REG                   MMIO32(I2S1_BASE + 0x08)
// Enable signal loopback mode
#define I2S_SIG_LOOPBACK    0x40000
// Set this to place right-channel data at the MSB in the receive FIFO. (R/W)
#define I2S_RX_MSB_RIGHT    0x20000
// Set this bit to place right-channel data at the MSB in the transmit FIFO. (R/W)
#define I2S_TX_MSB_RIGHT    0x10000
// Set this bit to enable receiver’s mono mode in PCM standard mode. (R/W)
#define I2S_RX_MONO         0x08000
// Set this bit to enable transmitter’s mono mode in PCM standard mode. (R/W)
#define I2S_TX_MONO         0x04000
// Set this bit to enable receiver in PCM standard mode. (R/W)
#define I2S_RX_SHORT_SYNC   0x02000
// Set this bit to enable transmitter in PCM standard mode. (R/W)
#define I2S_TX_SHORT_SYNC   0x01000
// Set this bit to enable receiver in Philips standard mode. (R/W)
#define I2S_RX_MSB_SHIFT    0x00800
// Set this bit to enable transmitter in Philips standard mode. (R/W)
#define I2S_TX_MSB_SHIFT    0x00400
// Set this bit to receive right-channel data first. (R/W)
#define I2S_RX_RIGHT_FIRST  0x00200
// Set this bit to transmit right-channel data first. (R/W)
#define I2S_TX_RIGHT_FIRST  0x00100
// Set this bit to enable slave receiver mode. (R/W)
#define I2S_RX_SLAVE_MOD    0x00080
// Set this bit to enable slave transmitter mode. (R/W)
#define I2S_TX_SLAVE_MOD    0x00040
// Set this bit to start receiving data. (R/W)
#define I2S_RX_START        0x00020
// Set this bit to start transmitting data. (R/W)
#define I2S_TX_START        0x00010
// Set this bit to reset the receive FIFO. (R/W)
#define I2S_RX_FIFO_RESET   0x00008
// Set this bit to reset the transmit FIFO. (R/W)
#define I2S_TX_FIFO_RESET   0x00004
// Set this bit to reset the receiver. (R/W)
#define I2S_RX_RESET        0x00002
// Set this bit to reset the transmitter. (R/W)
#define I2S_TX_RESET        0x00001

/* PCM configuration register */
#define I2S0_CONF1_REG                  MMIO32(I2S0_BASE + 0xa0)
#define I2S1_CONF1_REG                  MMIO32(I2S1_BASE + 0xa0)
// transmitter will stop tx BCK signal and WS signal when tx FIFO is empty. (R/W)
#define I2S_TX_STOP_EN      0x100
// Set this bit to bypass the Compress/Decompress module for the rx data. (R/W)
#define I2S_RX_PCM_BYPASS   0x080
// Compress/Decompress module configuration bit. (R/W)
#define I2S_RX_PCM_CONF     0x010
// Set this bit to bypass the Compress/Decompress module for the tx data. (R/W)
#define I2S_TX_PCM_BYPASS   0x008
// Compress/Decompress module configuration bit. (R/W)
#define I2S_TX_PCM_CONF     0x001

/* ADC/LCD/camera configuration register */
#define I2S0_CONF2_REG                  MMIO32(I2S0_BASE + 0xa8)
#define I2S1_CONF2_REG                  MMIO32(I2S1_BASE + 0xa8)
// Set this bit to enable camera’s internal validation. (R/W)
#define I2S_INTER_VALID_EN      0x80
// Set this bit to enable the start of external ADC . (R/W)
#define I2S_EXT_ADC_START_EN    0x40
// Set this bit to enable LCD mode. (R/W)
#define I2S_LCD_EN              0x20
// Set this bit to duplicate data pairs (Data Frame, Form 2) in LCD mode. (R/W)
#define I2S_LCD_TX_SDX2_EN      0x04
// One datum will be written twice in LCD mode. (R/W)
#define I2S_LCD_TX_WRX2_EN      0x02
// Set this bit to enable camera mode. (R/W)
#define I2S_CAMERA_EN           0x01

/* Signal delay and timing parameters */
#define I2S0_TIMING_REG                 MMIO32(I2S0_BASE + 0x1c)
#define I2S1_TIMING_REG                 MMIO32(I2S1_BASE + 0x1c)
// Set this bit to invert the BCK signal into the slave transmitter. (R/W)
#define I2S_TX_BCK_IN_INV           0x1000000
// Number of delay cycles for data valid flag. (R/W)
#define I2S_DATA_ENABLE_DELAY_MSK   0x0c00000
#define I2S_DATA_ENABLE_DELAY_SFT   22
#define I2S_DATA_ENABLE_DELAY_SET(n) ((n<<I2S_DATA_ENABLE_DELAY_SFT)&I2S_DATA_ENABLE_DELAY_MSK)
// Set this bit to synchronize signals into the receiver in double sync method.
#define I2S_RX_DSYNC_SW             0x0200000
// Set this bit to synchronize signals into the transmitter in double sync method.
#define I2S_TX_DSYNC_SW             0x0100000
// Number of delay cycles for BCK signal out of the receiver. (R/W)
#define I2S_RX_BCK_OUT_DELAY_MSK    0x00c0000
#define I2S_RX_BCK_OUT_DELAY_SFT    18
#define I2S_RX_BCK_OUT_DELAY_SET(n) ((n<<I2S_RX_BCK_OUT_DELAY_SFT)&I2S_RX_BCK_OUT_DELAY_MSK)
// Number of delay cycles for WS signal out of the receiver. (R/W)
#define I2S_RX_WS_OUT_DELAY_MSK     0x0030000
#define I2S_RX_WS_OUT_DELAY_SFT     16
#define I2S_RX_WS_OUT_DELAY_SET(n) ((n<<I2S_RX_WS_OUT_DELAY_SFT)&I2S_RX_WS_OUT_DELAY_MSK)
// Number of delay cycles for SD signal out of the transmitter. (R/W)
#define I2S_TX_SD_OUT_DELAY_MSK     0x000c000
#define I2S_TX_SD_OUT_DELAY_SFT     14
#define I2S_TX_SD_OUT_DELAY_SET(n) ((n<<I2S_TX_SD_OUT_DELAY_SFT)&I2S_TX_SD_OUT_DELAY_MSK)
// Number of delay cycles for WS signal out of the transmitter. (R/W)
#define I2S_TX_WS_OUT_DELAY_MSK     0x0003000
#define I2S_TX_WS_OUT_DELAY_SFT     12
#define I2S_TX_WS_OUT_DELAY_SET(n) ((n<<I2S_TX_WS_OUT_DELAY_SFT)&I2S_TX_WS_OUT_DELAY_MSK)
// Number of delay cycles for BCK signal out of the transmitter. (R/W)
#define I2S_TX_BCK_OUT_DELAY_MSK    0x0000c00
#define I2S_TX_BCK_OUT_DELAY_SFT    10
#define I2S_TX_BCK_OUT_DELAY_SET(n) ((n<<I2S_TX_BCK_OUT_DELAY_SFT)&I2S_TX_BCK_OUT_DELAY_MSK)
// Number of delay cycles for SD signal into the receiver. (R/W)
#define I2S_RX_SD_IN_DELAY_MSK      0x0000300
#define I2S_RX_SD_IN_DELAY_SFT      8
#define I2S_RX_SD_IN_DELAY_SET(n) ((n<<I2S_RX_SD_IN_DELAY_SFT)&I2S_RX_SD_IN_DELAY_MSK)
// Number of delay cycles for WS signal into the receiver. (R/W)
#define I2S_RX_WS_IN_DELAY_MSK      0x00000c0
#define I2S_RX_WS_IN_DELAY_SFT      6
#define I2S_RX_WS_IN_DELAY_SET(n) ((n<<I2S_RX_WS_IN_DELAY_SFT)&I2S_RX_WS_IN_DELAY_MSK)
// Number of delay cycles for BCK signal into the receiver. (R/W)
#define I2S_RX_BCK_IN_DELAY_MSK     0x0000030
#define I2S_RX_BCK_IN_DELAY_SFT     4
#define I2S_RX_BCK_IN_DELAY_SET(n) ((n<<I2S_RX_BCK_IN_DELAY_SFT)&I2S_RX_BCK_IN_DELAY_MSK)
// Number of delay cycles for WS signal into the transmitter. (R/W)
#define I2S_TX_WS_IN_DELAY_MSK      0x000000c
#define I2S_TX_WS_IN_DELAY_SFT      2
#define I2S_TX_WS_IN_DELAY_SET(n) ((n<<I2S_TX_WS_IN_DELAY_SFT)&I2S_TX_WS_IN_DELAY_MSK)
// Number of delay cycles for BCK signal into the transmitter. (R/W)
#define I2S_TX_BCK_IN_DELAY_MSK     0x0000003
#define I2S_TX_BCK_IN_DELAY_SET(n)  (n&I2S_TX_BCK_IN_DELAY_MSK)

/* FIFO configuration */
#define I2S0_FIFO_CONF_REG              MMIO32(I2S0_BASE + 0x20)
#define I2S1_FIFO_CONF_REG              MMIO32(I2S1_BASE + 0x20)
// The bit should always be set to 1. (R/W)
#define I2S_RX_FIFO_MOD_FORCE_EN    0x100000
// The bit should always be set to 1. (R/W)
#define I2S_TX_FIFO_MOD_FORCE_EN    0x080000
// Receive FIFO mode configuration bit. (R/W)
#define I2S_RX_FIFO_MOD_MSK         0x070000
#define I2S_RX_FIFO_MOD_SFT         16
#define I2S_RX_FIFO_MOD_SET(n)  ((n<<I2S_RX_FIFO_MOD_SFT)&I2S_RX_FIFO_MOD_MSK)
// Transmit FIFO mode configuration bit. (R/W)
#define I2S_TX_FIFO_MOD_MSK         0x00e000
#define I2S_TX_FIFO_MOD_SFT         13
#define I2S_TX_FIFO_MOD_SET(n)  ((n<<I2S_TX_FIFO_MOD_SFT)&I2S_TX_FIFO_MOD_MSK)
// Set this bit to enable I2S DMA mode. (R/W)
#define I2S_DSCR_EN                 0x001000
// Threshold of data length in the transmit FIFO. (R/W)
#define I2S_TX_DATA_NUM_MSK         0x000fc0
#define I2S_TX_DATA_NUM_SFT         6
#define I2S_TX_DATA_NUM_SET(n)  ((n<<I2S_TX_DATA_NUM_SFT)&I2S_TX_DATA_NUM_MSK)
// Threshold of data length in the receive FIFO. (R/W)
#define I2S_RX_DATA_NUM_MSK         0x00003f
#define I2S_RX_DATA_NUM_SET(n)  (n&I2S_RX_DATA_NUM_MSK)

/* Receive data count */
#define I2S0_RXEOF_NUM_REG              MMIO32(I2S0_BASE + 0x24)
#define I2S1_RXEOF_NUM_REG              MMIO32(I2S1_BASE + 0x24)
// The length of the data to be received. It will trigger I2S_IN_SUC_EOF_INT. (R/W)

/* Static channel output value */
#define I2S0_CONF_SINGLE_DATA_REG       MMIO32(I2S0_BASE + 0x28)
#define I2S1_CONF_SINGLE_DATA_REG       MMIO32(I2S1_BASE + 0x28)
// The right channel or the left channel outputs constant values stored

/* Channel configuration */
#define I2S0_CONF_CHAN_REG              MMIO32(I2S0_BASE + 0x2c)
#define I2S1_CONF_CHAN_REG              MMIO32(I2S1_BASE + 0x2c)
// I2S receiver channel mode configuration bits.
#define I2S_RX_CHAN_MOD_MSK     0x18
#define I2S_RX_CHAN_MOD_SFT     3
#define I2S_RX_CHAN_MOD_SET(n)  ((n<<I2S_RX_CHAN_MOD_SFT)&I2S_RX_CHAN_MOD_MSK)
// I2S transmitter channel mode configuration bits.
#define I2S_TX_CHAN_MOD_MSK     0x07
#define I2S_TX_CHAN_MOD_SET(n)  (n&I2S_TX_CHAN_MOD_MSK)

/* Timeout detection configuration */
#define I2S0_LC_HUNG_CONF_REG           MMIO32(I2S0_BASE + 0x74)
#define I2S1_LC_HUNG_CONF_REG           MMIO32(I2S1_BASE + 0x74)
// The enable bit for FIFO timeout. (R/W)
#define I2S_LC_FIFO_TIMEOUT_ENA             0x800
// the tick counter threshold.
#define I2S_LC_FIFO_TIMEOUT_SHIFT_MSK       0x700
#define I2S_LC_FIFO_TIMEOUT_SHIFT_SFT
#define I2S_LC_FIFO_TIMEOUT_SHIFT_SET(n)  ((n<<I2S_LC_FIFO_TIMEOUT_SHIFT_SFT)&I2S_LC_FIFO_TIMEOUT_SHIFT_MSK)
// When the value of FIFO hung cnt is equal, sending data-timeout interrupt
#define I2S_LC_FIFO_TIMEOUT_MSK             0x0ff
#define I2S_LC_FIFO_TIMEOUT_SET(n)          (n&I2S_LC_FIFO_TIMEOUT_MSK)

/* Bitclock configuration */
#define I2S0_CLKM_CONF_REG              MMIO32(I2S0_BASE + 0xac)
#define I2S1_CLKM_CONF_REG              MMIO32(I2S1_BASE + 0xac)
// Set this bit to enable APLL_CLK. Default is PLL_F160M_CLK. (R/W)
#define I2S_CLKA_ENA            0x200000
// Fractional clock divider’s denominator value. (R/W)
#define I2S_CLKM_DIV_A_MSK      0x0fc000
#define I2S_CLKM_DIV_A_SFT      14
#define I2S_CLKM_DIV_A_SET(n)   ((n<<I2S_CLKM_DIV_A_SFT)&I2S_CLKM_DIV_A_MSK)
// Fractional clock divider’s numerator value. (R/W)
#define I2S_CLKM_DIV_B_MSK      0x003f00
#define I2S_CLKM_DIV_B_SFT      8
#define I2S_CLKM_DIV_B_SET(n)   ((n<<I2S_CLKM_DIV_B_SFT)&I2S_CLKM_DIV_B_MSK)
// I2S clock divider’s integral value. (R/W)
#define I2S_CLKM_DIV_NUM_MSK    0x0000ff
#define I2S_CLKM_DIV_NUM_SET(n) (n&I2S_CLKM_DIV_NUM_MSK)

/* Sample rate configuration */
#define I2S0_SAMPLE_RATE_CONF_REG       MMIO32(I2S0_BASE + 0xb0)
#define I2S1_SAMPLE_RATE_CONF_REG       MMIO32(I2S1_BASE + 0xb0)
// Set the bits to configure the bit length of I2S receiver channel. (R/W)
#define I2S_RX_BITS_MOD_MSK         0xfc0000
#define I2S_RX_BITS_MOD_SFT         18
#define I2S_RX_BITS_MOD_SET(n) ((n<<I2S_RX_BITS_MOD_SFT)&I2S_RX_BITS_MOD_MSK)
// Set the bits to configure the bit length of I2S transmitter channel. (R/W)
#define I2S_TX_BITS_MOD_MSK         0x03f000
#define I2S_TX_BITS_MOD_SFT         12
#define I2S_TX_BITS_MOD_SET(n) ((n<<I2S_TX_BITS_MOD_SFT)&I2S_TX_BITS_MOD_MSK)
// Bit clock configuration bit in receiver mode. (R/W)
#define I2S_RX_BCK_DIV_NUM_MSK      0x000fc0
#define I2S_RX_BCK_DIV_NUM_SFT      6
#define I2S_RX_BCK_DIV_NUM_SET(n) ((n<<I2S_RX_BCK_DIV_NUM_SFT)&I2S_RX_BCK_DIV_NUM_MSK)
// Bit clock configuration bit in transmitter mode. (R/W)
#define I2S_TX_BCK_DIV_NUM_MSK      0x00003f
#define I2S_TX_BCK_DIV_NUM_SET(n)   (n&I2S_TX_BCK_DIV_NUM_MSK)

/* Power-down register */
#define I2S0_PD_CONF_REG                MMIO32(I2S0_BASE + 0xa4)
#define I2S1_PD_CONF_REG                MMIO32(I2S1_BASE + 0xa4)
// Force FIFO power-up. (R/W)
#define I2S_FIFO_FORCE_PU   0x2
// Force FIFO power-down. (R/W)
#define I2S_FIFO_FORCE_PD   0x1

/* I2S status register */
#define I2S0_STATE_REG                  MMIO32(I2S0_BASE + 0xbc)
#define I2S1_STATE_REG                  MMIO32(I2S1_BASE + 0xbc)
// This bit is used to confirm if the Rx FIFO reset is done.
#define I2S_RX_FIFO_RESET_BACK  0x4
// This bit is used to confirm if the Tx FIFO reset is done.
#define I2S_TX_FIFO_RESET_BACK  0x2
// The status bit of the transmitter.
#define I2S_TX_IDLE             0x1

/* DMA configuration register */
#define I2S0_LC_CONF_REG                MMIO32(I2S0_BASE + 0x60)
#define I2S1_LC_CONF_REG                MMIO32(I2S1_BASE + 0x60)
// Set this bit to check the owner bit by hardware. (R/W)
#define I2S_CHECK_OWNER         0x1000
// Transmitter data transfer mode configuration bit. (R/W)
#define I2S_OUT_DATA_BURST_EN   0x0800
// DMA inlink descriptor transfer mode configuration bit. (R/W)
#define I2S_INDSCR_BURST_EN     0x0400
// DMA outlink descriptor transfer mode configuration bit. (R/W)
#define I2S_OUTDSCR_BURST_EN    0x0200
// DMA I2S_OUT_EOF_INT generation mode. (R/W)
#define I2S_OUT_EOF_MODE        0x0100
// automatic outlink-writeback when all the data in tx buffer has been tx. (R/W)
#define I2S_OUT_AUTO_WRBACK     0x0040
// Set this bit to loop test outlink. (R/W)
#define I2S_OUT_LOOP_TEST       0x0020
// Set this bit to loop test inlink. (R/W)
#define I2S_IN_LOOP_TEST        0x0010
// Set this bit to reset AHB interface of DMA. (R/W)
#define I2S_AHBM_RST            0x0008
// Set this bit to reset AHB interface cmdFIFO of DMA. (R/W)
#define I2S_AHBM_FIFO_RST       0x0004
// Set this bit to reset out DMA FSM. (R/W)
#define I2S_OUT_RST             0x0002
// Set this bit to reset in DMA FSM. (R/W)
#define I2S_IN_RST              0x0001

/* DMA transmit linked list configuration and address */
#define I2S0_OUT_LINK_REG               MMIO32(I2S0_BASE + 0x30)
#define I2S1_OUT_LINK_REG               MMIO32(I2S1_BASE + 0x30)
// Set this bit to restart outlink descriptor. (R/W)
#define I2S_OUTLINK_RESTART     0x40000000
// Set this bit to start outlink descriptor. (R/W)
#define I2S_OUTLINK_START       0x20000000
// Set this bit to stop outlink descriptor. (R/W)
#define I2S_OUTLINK_STOP        0x10000000
// The address of first outlink descriptor. (R/W)
#define I2S_OUTLINK_ADDR_MSK    0x000fffff
#define I2S_OUTLINK_ADDR_SET(n) (n&I2S_OUTLINK_ADDR_MSK)

/* DMA receive linked list configuration and address */
#define I2S0_IN_LINK_REG                MMIO32(I2S0_BASE + 0x34)
#define I2S1_IN_LINK_REG                MMIO32(I2S1_BASE + 0x34)
// Set this bit to restart link descriptor. (R/W)
#define I2S_INLINK_RESTART     0x40000000
// Set this bit to start link descriptor. (R/W)
#define I2S_INLINK_START       0x20000000
// Set this bit to stop link descriptor. (R/W)
#define I2S_INLINK_STOP        0x10000000
// The address of first link descriptor. (R/W)
#define I2S_INLINK_ADDR_MSK    0x000fffff
#define I2S_INLINK_ADDR_SET(n) (n&I2S_OUTLINK_ADDR_MSK)

/* The address of transmit link descriptor producing EOF */
#define I2S0_OUT_EOF_DES_ADDR_REG       MMIO32(I2S0_BASE + 0x38)
#define I2S1_OUT_EOF_DES_ADDR_REG       MMIO32(I2S1_BASE + 0x38)
// The address of outlink descriptor that produces EOF. (RO)

/* The address of receive link descriptor producing EOF */
#define I2S0_IN_EOF_DES_ADDR_REG        MMIO32(I2S0_BASE + 0x3c)
#define I2S1_IN_EOF_DES_ADDR_REG        MMIO32(I2S1_BASE + 0x3c)
// The address of inlink descriptor that produces EOF. (RO)

/* The address of transmit buffer producing EOF */
#define I2S0_OUT_EOF_BFR_DES_ADDR_REG   MMIO32(I2S0_BASE + 0x40)
#define I2S1_OUT_EOF_BFR_DES_ADDR_REG   MMIO32(I2S1_BASE + 0x40)
// The address of the buffer corresponding to the outlink descr that produces EOF.

/* The address of current inlink descriptor */
#define I2S0_INLINK_DSCR_REG            MMIO32(I2S0_BASE + 0x48)
#define I2S1_INLINK_DSCR_REG            MMIO32(I2S1_BASE + 0x48)
// The address of current inlink descriptor. (RO)

/* The address of next inlink descriptor */
#define I2S0_INLINK_DSCR_BF0_REG        MMIO32(I2S0_BASE + 0x4c)
#define I2S1_INLINK_DSCR_BF0_REG        MMIO32(I2S1_BASE + 0x4c)
// The address of next inlink descriptor. (RO)

/* The address of next inlink data buffer */
#define I2S0_INLINK_DSCR_BF1_REG        MMIO32(I2S0_BASE + 0x50)
#define I2S1_INLINK_DSCR_BF1_REG        MMIO32(I2S1_BASE + 0x50)
// The address of next inlink data buffer. (RO)

/* The address of current outlink descriptor */
#define I2S0_OUTLINK_DSCR_REG           MMIO32(I2S0_BASE + 0x54)
#define I2S1_OUTLINK_DSCR_REG           MMIO32(I2S1_BASE + 0x54)
// The address of current outlink descriptor. (RO)

/* The address of next outlink descriptor */
#define I2S0_OUTLINK_DSCR_BF0_REG       MMIO32(I2S0_BASE + 0x58)
#define I2S1_OUTLINK_DSCR_BF0_REG       MMIO32(I2S1_BASE + 0x58)
// The address of next outlink descriptor. (RO)

/* The address of next outlink data buffer */
#define I2S0_OUTLINK_DSCR_BF1_REG       MMIO32(I2S0_BASE + 0x5c)
#define I2S1_OUTLINK_DSCR_BF1_REG       MMIO32(I2S1_BASE + 0x5c)
// The address of next outlink data buffer. (RO)

/* DMA receive status */
#define I2S0_LC_STATE0_REG              MMIO32(I2S0_BASE + 0x6c)
#define I2S1_LC_STATE0_REG              MMIO32(I2S1_BASE + 0x6c)
// Receiver DMA channel status register. (RO)

/* DMA transmit status */
#define I2S0_LC_STATE1_REG              MMIO32(I2S0_BASE + 0x70)
#define I2S1_LC_STATE1_REG              MMIO32(I2S1_BASE + 0x70)
// Transmitter DMA channel status register. (RO)

/* PDM configuration */
#define I2S0_PDM_CONF_REG               MMIO32(I2S0_BASE + 0xb4)
#define I2S1_PDM_CONF_REG               MMIO32(I2S1_BASE + 0xb4)
// Set this bit to bypass the transmitter’s PDM HP filter. (R/W)
#define I2S_TX_PDM_HP_BYPASS                    0x2000000
// PDM downsampling rate for filter group 1 in receiver mode. (R/W)
#define I2S_RX_PDM_SINC_DSR_16_EN               0x1000000
// Adjust the size of the input signal into filter module. (R/W)
#define I2S_TX_PDM_SIGMADELTA_IN_SHIFT_MSK      0x0c00000
#define I2S_TX_PDM_SIGMADELTA_IN_SHIFT_SFT      22
#define I2S_TX_PDM_SIGMADELTA_IN_SHIFT_SET(n) ((n<<I2S_TX_PDM_SIGMADELTA_IN_SHIFT_SFT)&I2S_TX_PDM_SIGMADELTA_IN_SHIFT_MSK)
// Adjust the size of the input signal into filter module. (R/W)
#define I2S_TX_PDM_SINC_IN_SHIFT_MSK            0x0300000
#define I2S_TX_PDM_SINC_IN_SHIFT_SFT            20
#define I2S_TX_PDM_SINC_IN_SHIFT_SET(n) ((n<<I2S_TX_PDM_SINC_IN_SHIFT_SFT)&I2S_TX_PDM_SINC_IN_SHIFT_MSK)
// Adjust the size of the input signal into filter module. (R/W)
#define I2S_TX_PDM_LP_IN_SHIFT_MSK              0x00c0000
#define I2S_TX_PDM_LP_IN_SHIFT_SFT              18
#define I2S_TX_PDM_LP_IN_SHIFT_SET(n) ((n<<I2S_TX_PDM_LP_IN_SHIFT_SFT)&I2S_TX_PDM_LP_IN_SHIFT_MSK)
// Adjust the size of the input signal into filter module. (R/W)
#define I2S_TX_PDM_HP_IN_SHIFT_MSK              0x0030000
#define I2S_TX_PDM_HP_IN_SHIFT_SFT              16
#define I2S_TX_PDM_HP_IN_SHIFT_SET(n) ((n<<I2S_TX_PDM_HP_IN_SHIFT_SFT)&I2S_TX_PDM_HP_IN_SHIFT_MSK)
// Upsampling rate = 64×i2s_tx_pdm_sinc_osr2 (R/W)
#define I2S_TX_PDM_SINC_OSR2_MSK                0x00000f0
#define I2S_TX_PDM_SINC_OSR2_SFT                4
#define I2S_TX_PDM_SINC_OSR2_SET(n) ((n<<I2S_TX_PDM_SINC_OSR2_SFT)&I2S_TX_PDM_SINC_OSR2_MSK)
// Set this bit to enable PDM-to-PCM converter. (R/W)
#define I2S_PDM2PCM_CONV_EN                     0x0000008
// Set this bit to enable PCM-to-PDM converter. (R/W)
#define I2S_PCM2PDM_CONV_EN                     0x0000004
// Set this bit to enable receiver’s PDM mode. (R/W)
#define I2S_RX_PDM_EN                           0x0000002
// Set this bit to enable transmitter’s PDM mode. (R/W)
#define I2S_TX_PDM_EN                           0x0000001

/* PDM frequencies */
#define I2S0_PDM_FREQ_CONF_REG          MMIO32(I2S0_BASE + 0xb8)
#define I2S1_PDM_FREQ_CONF_REG          MMIO32(I2S1_BASE + 0xb8)
// PCM-to-PDM converter’s PDM frequency parameter. (R/W)
#define I2S_TX_PDM_FP_MSK       0xffc00
#define I2S_TX_PDM_FP_SFT       10
#define I2S_TX_PDM_FP_SET(n)    ((n<<I2S_TX_PDM_FP_SFT)&I2S_TX_PDM_FP_MSK)
// PCM-to-PDM converter’s PCM frequency parameter. (R/W)
#define I2S_TX_PDM_FS_MSK       0x003ff
#define I2S_TX_PDM_FS_SET(n)    (n&I2S_TX_PDM_FS_MSK)

/* Raw interrupt status */
#define I2S0_INT_RAW_REG                MMIO32(I2S0_BASE + 0x0c)
#define I2S1_INT_RAW_REG                MMIO32(I2S1_BASE + 0x0c)
// The raw interrupt status bit for the I2S_OUT_TOTAL_EOF_INT interrupt. (RO)
#define I2S_OUT_TOTAL_EOF_INT_RAW   0x10000
// The raw interrupt status bit for the I2S_IN_DSCR_EMPTY_INT interrupt. (RO)
#define I2S_IN_DSCR_EMPTY_INT_RAW   0x08000
// The raw interrupt status bit for the I2S_OUT_DSCR_ERR_INT interrupt. (RO)
#define I2S_OUT_DSCR_ERR_INT_RAW    0x04000
// The raw interrupt status bit for the I2S_IN_DSCR_ERR_INT interrupt. (RO)
#define I2S_IN_DSCR_ERR_INT_RAW     0x02000
// The raw interrupt status bit for the I2S_OUT_EOF_INT interrupt. (RO)
#define I2S_OUT_EOF_INT_RAW         0x01000
// The raw interrupt status bit for the I2S_OUT_DONE_INT interrupt. (RO)
#define I2S_OUT_DONE_INT_RAW        0x00800
// The raw interrupt status bit for the I2S_IN_SUC_EOF_INT interrupt. (RO)
#define I2S_IN_SUC_EOF_INT_RAW      0x00200
// The raw interrupt status bit for the I2S_IN_DONE_INT interrupt. (RO)
#define I2S_IN_DONE_INT_RAW         0x00100
// The raw interrupt status bit for the I2S_TX_HUNG_INT interrupt. (RO)
#define I2S_TX_HUNG_INT_RAW         0x00080
// The raw interrupt status bit for the I2S_RX_HUNG_INT interrupt. (RO)
#define I2S_RX_HUNG_INT_RAW         0x00040
// The raw interrupt status bit for the I2S_TX_REMPTY_INT interrupt. (RO)
#define I2S_TX_REMPTY_INT_RAW       0x00020
// The raw interrupt status bit for the I2S_TX_WFULL_INT interrupt. (RO)
#define I2S_TX_WFULL_INT_RAW        0x00010
// The raw interrupt status bit for the I2S_RX_REMPTY_INT interrupt. (RO)
#define I2S_RX_REMPTY_INT_RAW       0x00008
// The raw interrupt status bit for the I2S_RX_WFULL_INT interrupt. (RO)
#define I2S_RX_WFULL_INT_RAW        0x00004
// The raw interrupt status bit for the I2S_TX_PUT_DATA_INT interrupt. (RO)
#define I2S_TX_PUT_DATA_INT_RAW     0x00002
// The raw interrupt status bit for the I2S_RX_TAKE_DATA_INT interrupt. (RO)
#define I2S_RX_TAKE_DATA_INT_RAW    0x00001

/* Masked interrupt status */
#define I2S0_INT_ST_REG                 MMIO32(I2S0_BASE + 0x10)
#define I2S1_INT_ST_REG                 MMIO32(I2S1_BASE + 0x10)
// The masked interrupt status bit for the I2S_OUT_TOTAL_EOF_INT interrupt. (RO)
#define I2S_OUT_TOTAL_EOF_INT_ST   0x10000
// The masked interrupt status bit for the I2S_IN_DSCR_EMPTY_INT interrupt. (RO)
#define I2S_IN_DSCR_EMPTY_INT_ST   0x08000
// The masked interrupt status bit for the I2S_OUT_DSCR_ERR_INT interrupt. (RO)
#define I2S_OUT_DSCR_ERR_INT_ST    0x04000
// The masked interrupt status bit for the I2S_IN_DSCR_ERR_INT interrupt. (RO)
#define I2S_IN_DSCR_ERR_INT_ST     0x02000
// The masked interrupt status bit for the I2S_OUT_EOF_INT interrupt. (RO)
#define I2S_OUT_EOF_INT_ST         0x01000
// The masked interrupt status bit for the I2S_OUT_DONE_INT interrupt. (RO)
#define I2S_OUT_DONE_INT_ST        0x00800
// The masked interrupt status bit for the I2S_IN_SUC_EOF_INT interrupt. (RO)
#define I2S_IN_SUC_EOF_INT_ST      0x00200
// The masked interrupt status bit for the I2S_IN_DONE_INT interrupt. (RO)
#define I2S_IN_DONE_INT_ST         0x00100
// The masked interrupt status bit for the I2S_TX_HUNG_INT interrupt. (RO)
#define I2S_TX_HUNG_INT_ST         0x00080
// The masked interrupt status bit for the I2S_RX_HUNG_INT interrupt. (RO)
#define I2S_RX_HUNG_INT_ST         0x00040
// The masked interrupt status bit for the I2S_TX_REMPTY_INT interrupt. (RO)
#define I2S_TX_REMPTY_INT_ST       0x00020
// The masked interrupt status bit for the I2S_TX_WFULL_INT interrupt. (RO)
#define I2S_TX_WFULL_INT_ST        0x00010
// The masked interrupt status bit for the I2S_RX_REMPTY_INT interrupt. (RO)
#define I2S_RX_REMPTY_INT_ST       0x00008
// The masked interrupt status bit for the I2S_RX_WFULL_INT interrupt. (RO)
#define I2S_RX_WFULL_INT_ST        0x00004
// The masked interrupt status bit for the I2S_TX_PUT_DATA_INT interrupt. (RO)
#define I2S_TX_PUT_DATA_INT_ST     0x00002
// The masked interrupt status bit for the I2S_RX_TAKE_DATA_INT interrupt. (RO)
#define I2S_RX_TAKE_DATA_INT_ST    0x00001

/* Interrupt enable bits */
#define I2S0_INT_ENA_REG                MMIO32(I2S0_BASE + 0x14)
#define I2S1_INT_ENA_REG                MMIO32(I2S1_BASE + 0x14)
// The interrupt enable bit for the I2S_OUT_TOTAL_EOF_INT interrupt. (RO)
#define I2S_OUT_TOTAL_EOF_INT_ENA   0x10000
// The interrupt enable bit for the I2S_IN_DSCR_EMPTY_INT interrupt. (RO)
#define I2S_IN_DSCR_EMPTY_INT_ENA   0x08000
// The interrupt enable bit for the I2S_OUT_DSCR_ERR_INT interrupt. (RO)
#define I2S_OUT_DSCR_ERR_INT_ENA    0x04000
// The interrupt enable bit for the I2S_IN_DSCR_ERR_INT interrupt. (RO)
#define I2S_IN_DSCR_ERR_INT_ENA     0x02000
// The interrupt enable bit for the I2S_OUT_EOF_INT interrupt. (RO)
#define I2S_OUT_EOF_INT_ENA         0x01000
// The interrupt enable bit for the I2S_OUT_DONE_INT interrupt. (RO)
#define I2S_OUT_DONE_INT_ENA        0x00800
// The interrupt enable bit for the I2S_IN_SUC_EOF_INT interrupt. (RO)
#define I2S_IN_SUC_EOF_INT_ENA      0x00200
// The interrupt enable bit for the I2S_IN_DONE_INT interrupt. (RO)
#define I2S_IN_DONE_INT_ENA         0x00100
// The interrupt enable bit for the I2S_TX_HUNG_INT interrupt. (RO)
#define I2S_TX_HUNG_INT_ENA         0x00080
// The interrupt enable bit for the I2S_RX_HUNG_INT interrupt. (RO)
#define I2S_RX_HUNG_INT_ENA         0x00040
// The interrupt enable bit for the I2S_TX_REMPTY_INT interrupt. (RO)
#define I2S_TX_REMPTY_INT_ENA       0x00020
// The interrupt enable bit for the I2S_TX_WFULL_INT interrupt. (RO)
#define I2S_TX_WFULL_INT_ENA        0x00010
// The interrupt enable bit for the I2S_RX_REMPTY_INT interrupt. (RO)
#define I2S_RX_REMPTY_INT_ENA       0x00008
// The interrupt enable bit for the I2S_RX_WFULL_INT interrupt. (RO)
#define I2S_RX_WFULL_INT_ENA        0x00004
// The interrupt enable bit for the I2S_TX_PUT_DATA_INT interrupt. (RO)
#define I2S_TX_PUT_DATA_INT_ENA     0x00002
// The interrupt enable bit for the I2S_RX_TAKE_DATA_INT interrupt. (RO)
#define I2S_RX_TAKE_DATA_INT_ENA    0x00001

/* Interrupt clear bits */
#define I2S0_INT_CLR_REG                MMIO32(I2S0_BASE + 0x18)
#define I2S1_INT_CLR_REG                MMIO32(I2S1_BASE + 0x18)
// Set this bit to clear the I2S_OUT_TOTAL_EOF_INT interrupt. (RO)
#define I2S_OUT_TOTAL_EOF_INT_CLR   0x10000
// Set this bit to clear the I2S_IN_DSCR_EMPTY_INT interrupt. (RO)
#define I2S_IN_DSCR_EMPTY_INT_CLR   0x08000
// Set this bit to clear the I2S_OUT_DSCR_ERR_INT interrupt. (RO)
#define I2S_OUT_DSCR_ERR_INT_CLR    0x04000
// Set this bit to clear the I2S_IN_DSCR_ERR_INT interrupt. (RO)
#define I2S_IN_DSCR_ERR_INT_CLR     0x02000
// Set this bit to clear the I2S_OUT_EOF_INT interrupt. (RO)
#define I2S_OUT_EOF_INT_CLR         0x01000
// Set this bit to clear the I2S_OUT_DONE_INT interrupt. (RO)
#define I2S_OUT_DONE_INT_CLR        0x00800
// Set this bit to clear the I2S_IN_SUC_EOF_INT interrupt. (RO)
#define I2S_IN_SUC_EOF_INT_CLR      0x00200
// Set this bit to clear the I2S_IN_DONE_INT interrupt. (RO)
#define I2S_IN_DONE_INT_CLR         0x00100
// Set this bit to clear the I2S_TX_HUNG_INT interrupt. (RO)
#define I2S_TX_HUNG_INT_CLR         0x00080
// Set this bit to clear the I2S_RX_HUNG_INT interrupt. (RO)
#define I2S_RX_HUNG_INT_CLR         0x00040
// Set this bit to clear the I2S_TX_REMPTY_INT interrupt. (RO)
#define I2S_TX_REMPTY_INT_CLR       0x00020
// Set this bit to clear the I2S_TX_WFULL_INT interrupt. (RO)
#define I2S_TX_WFULL_INT_CLR        0x00010
// Set this bit to clear the I2S_RX_REMPTY_INT interrupt. (RO)
#define I2S_RX_REMPTY_INT_CLR       0x00008
// Set this bit to clear the I2S_RX_WFULL_INT interrupt. (RO)
#define I2S_RX_WFULL_INT_CLR        0x00004
// Set this bit to clear the I2S_TX_PUT_DATA_INT interrupt. (RO)
#define I2S_TX_PUT_DATA_INT_CLR     0x00002
// Set this bit to clear the I2S_RX_TAKE_DATA_INT interrupt. (RO)
#define I2S_RX_TAKE_DATA_INT_CLR    0x00001

#endif
