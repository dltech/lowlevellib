#ifndef H_MEMORYMAP
#define H_MEMORYMAP
/*
 * ESP32 alternative library, memorymap
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Address Mapping
#define EXTERNAL_MEMORY1                    0x3f400000
#define EXTERNAL_MEMORY2                    0x3f800000
#define PERIPHERIAL                         0x3ff00000
#define EMBEDDED_MEMORY_DATA                0x3ff80000
#define EMBEDDED_MEMORY_INSTRUCTION         0x40000000
#define EXTERNAL_MEMORY_INSTRUCTION         0x400c2000
#define EMBEDDED_MEMORY_DATA_INSTRUCTION    0x50000000
// Embedded Memory Address Mapping
#define RTC_FAST_MEMORY                     0x3ff80000
#define INTERNAL_ROM                        0x3ff90000
#define INTERNAL_SRAM2                      0x3ffae000
#define INTERNAL_SRAM1_DMA                  0x3ffe0000
#define INTERNAL_ROM0_REMAP                 0x40000000
#define INTERNAL_ROM0                       0x40008000
#define INTERNAL_SRAM0_CACHE                0x40070000
#define INTERNAL_SRAM0                      0x40080000
#define INTERNAL_SRAM1                      0x400a0000
#define INTERNAL_SRAM1_REMAP                0x400b0000
#define INTERNAL_SRAM1_1                    0x400b8000
#define RTC_FAST_MEMORY_PRO_CPU             0x400c0000
#define RTC_SLOW_MEMORY                     0x50000000
// External Memory Address Mapping
#define EXTERNAL_FLASH_READ                 0x3f400000
#define EXTERNAL_SRAM_READ_WRITE            0x3f800000
#define EXTERNAL_FLASH_READ_1               0x400c0000

// Peripheral Address Mapping
#define DPORT_REGISTER      0x3ff00000
#define AES_ACCELERATOR     0x3ff01000
#define RSA_ACCELERATOR     0x3ff02000
#define SHA_ACCELERATOR     0x3ff03000
#define SECURE_BOOT         0x3ff04000
#define CACHE_MMU_TABLE     0x3ff10000
#define PID_CONTROLLER      0x3ff1f000
#define UART0_BASE          0x3ff40000
#define SPI1_BASE           0x3ff42000
#define SPI0_BASE           0x3ff43000
#define GPIO_BASE           0x3ff44000
#define RTC_BASE            0x3ff48000
#define IO_MUX_BASE         0x3ff49000
#define SDIO_SLAVE1         0x3ff4b000
#define UDMA1_BASE          0x3ff4c000
#define I2S0_BASE           0x3ff4f000
#define UART1_BASE          0x3ff50000
#define I2C0_BASE           0x3ff53000
#define UDMA0_BASE          0x3ff54000
#define SDIO_SLAVE2         0x3ff55000
#define RMT_BASE            0x3ff56000
#define PCNT_BASE           0x3ff57000
#define SDIO_SLAVE3         0x3ff58000
#define LED_PWM_BASE        0x3ff59000
#define EFUSE_CONTROLLER    0x3ff5a000
#define FLASH_ENCRYPTION    0x3ff5b000
#define MCPWM0_BASE         0x3ff5e000
#define TIMG0_BASE          0x3ff5f000
#define TIMG1_BASE          0x3ff60000
#define SPI2_BASE           0x3ff64000
#define SPI3_BASE           0x3ff65000
#define SYSCON              0x3ff66000
#define I2C1_BASE           0x3ff67000
#define SDMMC_BASE          0x3ff68000
#define EMAC_BASE           0x3ff69000
#define TWAI_BASE           0x3ff6b000
#define MCPWM1_BASE         0x3ff6c000
#define I2S1_BASE           0x3ff6d000
#define UART2_BASE          0x3ff6e000
#define RNG_BASE            0x3ff75000

// access to register with specified address
#define  MMIO32(addr)		(*(volatile uint32_t *)(addr))

#endif
