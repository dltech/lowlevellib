#ifndef H_PCNT_REGS
#define H_PCNT_REGS
/*
 * ESP32 alternative library, Pulse Count Controller (PCNT) module
 * registers.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Configuration register 0 for unit n */
#define PCNT_U0_CONF0_REG   MMIO32(PCNT_BASE + 0x00)
#define PCNT_U1_CONF0_REG   MMIO32(PCNT_BASE + 0x0c)
#define PCNT_U2_CONF0_REG   MMIO32(PCNT_BASE + 0x18)
#define PCNT_U3_CONF0_REG   MMIO32(PCNT_BASE + 0x24)
#define PCNT_U4_CONF0_REG   MMIO32(PCNT_BASE + 0x30)
#define PCNT_U5_CONF0_REG   MMIO32(PCNT_BASE + 0x3c)
#define PCNT_U6_CONF0_REG   MMIO32(PCNT_BASE + 0x48)
#define PCNT_U7_CONF0_REG   MMIO32(PCNT_BASE + 0x54)
// how the CH1_POS_MODE/CH1_NEG_MODE sets will be mod when the control sig is low.
#define PCNT_CH1_LCTRL_MODE_U_NO_MODE   0x00000000
#define PCNT_CH1_LCTRL_MODE_U_INVERT    0x40000000
#define PCNT_CH1_LCTRL_MODE_U_INHIBIT   0x80000000
// how the CH1_POS_MODE/CH1_NEG_MODE sets will be mod when the control sig is high.
#define PCNT_CH1_HCTRL_MODE_U_NO_MODE   0x00000000
#define PCNT_CH1_HCTRL_MODE_U_INVERT    0x10000000
#define PCNT_CH1_HCTRL_MODE_U_INHIBIT   0x20000000
// behaviour when the signal input of channel 1 detects a positive edge. (R/W)
#define PCNT_CH1_POS_MODE_U_INC         0x04000000
#define PCNT_CH1_POS_MODE_U_DEC         0x08000000
#define PCNT_CH1_POS_MODE_U_NO_EFFECT   0x00000000
// behaviour when the signal input of channel 1 detects a negative edge. (R/W)
#define PCNT_CH1_NEG_MODE_U_INC         0x01000000
#define PCNT_CH1_NEG_MODE_U_DEC         0x02000000
#define PCNT_CH1_NEG_MODE_U_NO_EFFECT   0x00000000
// how the CH0_POS_MODE/CH0_NEG_MODE set will be mod when the control sig is low.
#define PCNT_CH0_LCTRL_MODE_U_NO_MODE   0x00000000
#define PCNT_CH0_LCTRL_MODE_U_INVERT    0x00400000
#define PCNT_CH0_LCTRL_MODE_U_INHIBIT   0x00800000
// how the CH0_POS_MODE/CH0_NEG_MODE set will be mod when the ctrl sig is high.
#define PCNT_CH0_HCTRL_MODE_U_NO_MODE   0x00000000
#define PCNT_CH0_HCTRL_MODE_U_INVERT    0x00100000
#define PCNT_CH0_HCTRL_MODE_U_INHIBIT   0x00200000
// behaviour when the signal input of channel 0 detects a positive edge. (R/W)
#define PCNT_CH0_POS_MODE_U_INC         0x00040000
#define PCNT_CH0_POS_MODE_U_DEC         0x00080000
#define PCNT_CH0_POS_MODE_U_NO_EFFECT   0x00000000
// behaviour when the signal input of channel 0 detects a negative edge. (R/W)
#define PCNT_CH0_NEG_MODE_U_INC         0x00010000
#define PCNT_CH0_NEG_MODE_U_DEC         0x00020000
#define PCNT_CH0_NEG_MODE_U_NO_EFFECT   0x00000000
// This is the enable bit for unit n’s thres1 comparator. (R/W)
#define PCNT_THR_THRES1_EN_U            0x00008000
// This is the enable bit for unit n’s thres0 comparator. (R/W)
#define PCNT_THR_THRES0_EN_U            0x00004000
// This is the enable bit for unit n’s thr_l_lim comparator. (R/W)
#define PCNT_THR_L_LIM_EN_U             0x00002000
// This is the enable bit for unit n’s thr_h_lim comparator. (R/W)
#define PCNT_THR_H_LIM_EN_U             0x00001000
// This is the enable bit for unit n’s zero comparator. (R/W)
#define PCNT_THR_ZERO_EN_U              0x00000800
// This is the enable bit for unit n’s input filter. (R/W)
#define PCNT_FILTER_EN_U                0x00000400
// This sets the maximum threshold, in APB_CLK cycles, for the filter.
#define PCNT_FILTER_THRES_U_MSK         0x000003ff
#define PCNT_FILTER_THRES_U_SET(n)      (n&PCNT_FILTER_THRES_U_MSK)

/* Configuration register 1 for unit n */
#define PCNT_U0_CONF1_REG   MMIO32(PCNT_BASE + 0x04)
#define PCNT_U1_CONF1_REG   MMIO32(PCNT_BASE + 0x10)
#define PCNT_U2_CONF1_REG   MMIO32(PCNT_BASE + 0x1c)
#define PCNT_U3_CONF1_REG   MMIO32(PCNT_BASE + 0x28)
#define PCNT_U4_CONF1_REG   MMIO32(PCNT_BASE + 0x34)
#define PCNT_U5_CONF1_REG   MMIO32(PCNT_BASE + 0x40)
#define PCNT_U6_CONF1_REG   MMIO32(PCNT_BASE + 0x4c)
#define PCNT_U7_CONF1_REG   MMIO32(PCNT_BASE + 0x58)
// This register is used to configure the thres1 value for unit n. (R/W)
#define PCNT_CNT_THRES1_U_MSK       0xffff0000
#define PCNT_CNT_THRES1_U_SFT       16
#define PCNT_CNT_THRES1_U_SET(n) ((n<<PCNT_CNT_THRES1_U_SFT)&PCNT_CNT_THRES1_U_MSK)
// This register is used to configure the thres0 value for unit n. (R/W)
#define PCNT_CNT_THRES0_U_MSK       0x0000ffff
#define PCNT_CNT_THRES0_U_SET(n)    (n&PCNT_CNT_THRES0_U_MSK)

/* Configuration register 2 for unit n */
#define PCNT_U0_CONF2_REG   MMIO32(PCNT_BASE + 0x08)
#define PCNT_U1_CONF2_REG   MMIO32(PCNT_BASE + 0x14)
#define PCNT_U2_CONF2_REG   MMIO32(PCNT_BASE + 0x20)
#define PCNT_U3_CONF2_REG   MMIO32(PCNT_BASE + 0x2c)
#define PCNT_U4_CONF2_REG   MMIO32(PCNT_BASE + 0x38)
#define PCNT_U5_CONF2_REG   MMIO32(PCNT_BASE + 0x44)
#define PCNT_U6_CONF2_REG   MMIO32(PCNT_BASE + 0x50)
#define PCNT_U7_CONF2_REG   MMIO32(PCNT_BASE + 0x5c)
// This register is used to configure the thr_l_lim value for unit n. (R/W)
#define PCNT_CNT_L_LIM_U_MSK    0xffff0000
#define PCNT_CNT_L_LIM_U_SFT    16
#define PCNT_CNT_L_LIM_U_SET(n) ((n<<PCNT_CNT_THRES1_U_SFT)&PCNT_CNT_THRES1_U_MSK)
// This register is used to configure the thr_h_lim value for unit n. (R/W)
#define PCNT_CNT_H_LIM_U_MSK    0x0000ffff
#define PCNT_CNT_H_LIM_U_SET(n) (n&PCNT_CNT_H_LIM_U_MSK)

/* Counter value for unit n */
#define PCNT_U0_CNT_REG     MMIO32(PCNT_BASE + 0x60)
#define PCNT_U1_CNT_REG     MMIO32(PCNT_BASE + 0x64)
#define PCNT_U2_CNT_REG     MMIO32(PCNT_BASE + 0x68)
#define PCNT_U3_CNT_REG     MMIO32(PCNT_BASE + 0x6c)
#define PCNT_U4_CNT_REG     MMIO32(PCNT_BASE + 0x70)
#define PCNT_U5_CNT_REG     MMIO32(PCNT_BASE + 0x74)
#define PCNT_U6_CNT_REG     MMIO32(PCNT_BASE + 0x78)
#define PCNT_U7_CNT_REG     MMIO32(PCNT_BASE + 0x7c)
// This register stores the current pulse count value for unit n. (RO)
#define PCNT_PLUS_CNT_U_MSK 0xffff

/* Control register for all counters */
#define PCNT_CTRL_REG       MMIO32(PCNT_BASE + 0xb0)
// Set this bit to freeze unit n’s counter. (R/W)
#define PCNT_CNT_PAUSE_U7       0x8000
#define PCNT_CNT_PAUSE_U6       0x2000
#define PCNT_CNT_PAUSE_U5       0x0800
#define PCNT_CNT_PAUSE_U4       0x0200
#define PCNT_CNT_PAUSE_U3       0x0080
#define PCNT_CNT_PAUSE_U2       0x0020
#define PCNT_CNT_PAUSE_U1       0x0008
#define PCNT_CNT_PAUSE_U0       0x0002
// Set this bit to clear unit n’s counter. (R/W)
#define PCNT_PLUS_CNT_RST_U7    0x4000
#define PCNT_PLUS_CNT_RST_U6    0x1000
#define PCNT_PLUS_CNT_RST_U5    0x0400
#define PCNT_PLUS_CNT_RST_U4    0x0100
#define PCNT_PLUS_CNT_RST_U3    0x0040
#define PCNT_PLUS_CNT_RST_U2    0x0010
#define PCNT_PLUS_CNT_RST_U1    0x0004
#define PCNT_PLUS_CNT_RST_U0    0x0001

/* Raw interrupt status */
#define PCNT_INT_RAW_REG    MMIO32(PCNT_BASE + 0x80)
// The raw interrupt status bit for the PCNT_CNT_THR_EVENT_Un_INT interrupt. (RO)
#define PCNT_CNT_THR_EVENT_U7_INT_RAW   0x80
#define PCNT_CNT_THR_EVENT_U6_INT_RAW   0x40
#define PCNT_CNT_THR_EVENT_U5_INT_RAW   0x20
#define PCNT_CNT_THR_EVENT_U4_INT_RAW   0x10
#define PCNT_CNT_THR_EVENT_U3_INT_RAW   0x08
#define PCNT_CNT_THR_EVENT_U2_INT_RAW   0x04
#define PCNT_CNT_THR_EVENT_U1_INT_RAW   0x02
#define PCNT_CNT_THR_EVENT_U0_INT_RAW   0x01

/* Masked interrupt status */
#define PCNT_INT_ST_REG     MMIO32(PCNT_BASE + 0x84)
// The masked interrupt status bit for the PCNT_CNT_THR_EVENT_Un_INT interrupt. (RO)
#define PCNT_CNT_THR_EVENT_U7_INT_ST    0x80
#define PCNT_CNT_THR_EVENT_U6_INT_ST    0x40
#define PCNT_CNT_THR_EVENT_U5_INT_ST    0x20
#define PCNT_CNT_THR_EVENT_U4_INT_ST    0x10
#define PCNT_CNT_THR_EVENT_U3_INT_ST    0x08
#define PCNT_CNT_THR_EVENT_U2_INT_ST    0x04
#define PCNT_CNT_THR_EVENT_U1_INT_ST    0x02
#define PCNT_CNT_THR_EVENT_U0_INT_ST    0x01

/* Interrupt enable bits */
#define PCNT_INT_ENA_REG    MMIO32(PCNT_BASE + 0x88)
// The interrupt enable bit for the PCNT_CNT_THR_EVENT_Un_INT interrupt. (R/W)
#define PCNT_CNT_THR_EVENT_U7_INT_ENA   0x80
#define PCNT_CNT_THR_EVENT_U6_INT_ENA   0x40
#define PCNT_CNT_THR_EVENT_U5_INT_ENA   0x20
#define PCNT_CNT_THR_EVENT_U4_INT_ENA   0x10
#define PCNT_CNT_THR_EVENT_U3_INT_ENA   0x08
#define PCNT_CNT_THR_EVENT_U2_INT_ENA   0x04
#define PCNT_CNT_THR_EVENT_U1_INT_ENA   0x02
#define PCNT_CNT_THR_EVENT_U0_INT_ENA   0x01

/* Interrupt clear bits */
#define PCNT_INT_CLR_REG    MMIO32(PCNT_BASE + 0x8c)
// Set this bit to clear the PCNT_CNT_THR_EVENT_Un_INT interrupt. (WO)
#define PCNT_CNT_THR_EVENT_U7_INT_CLR   0x80
#define PCNT_CNT_THR_EVENT_U6_INT_CLR   0x40
#define PCNT_CNT_THR_EVENT_U5_INT_CLR   0x20
#define PCNT_CNT_THR_EVENT_U4_INT_CLR   0x10
#define PCNT_CNT_THR_EVENT_U3_INT_CLR   0x08
#define PCNT_CNT_THR_EVENT_U2_INT_CLR   0x04
#define PCNT_CNT_THR_EVENT_U1_INT_CLR   0x02
#define PCNT_CNT_THR_EVENT_U0_INT_CLR   0x01

/* Indicate the status of counter */
#define PCNT_U0_STATUS_REG  MMIO32(PCNT_BASE + 0x90)
#define PCNT_U1_STATUS_REG  MMIO32(PCNT_BASE + 0x9c)
#define PCNT_U2_STATUS_REG  MMIO32(PCNT_BASE + 0xa8)
#define PCNT_U3_STATUS_REG  MMIO32(PCNT_BASE + 0xb4)
#define PCNT_U4_STATUS_REG  MMIO32(PCNT_BASE + 0xc0)
#define PCNT_U5_STATUS_REG  MMIO32(PCNT_BASE + 0xcc)
#define PCNT_U6_STATUS_REG  MMIO32(PCNT_BASE + 0xd8)
#define PCNT_U7_STATUS_REG  MMIO32(PCNT_BASE + 0xe4)
// The last interrupt happened on counter for unit n reaching 0. (RO)
#define PCNT_THR_ZERO_LAT_U         0x40
// The last interrupt happened on counter for unit n reaching thr_h_lim. (RO)
#define PCNT_THR_H_LIM_LAT_U        0x20
// The last interrupt happened on counter for unit n reaching thr_l_lim. (RO)
#define PCNT_THR_L_LIM_LAT_U        0x10
// The last interrupt happened on counter for unit n reaching thres0. (RO)
#define PCNT_THR_THRES0_LAT_U       0x08
// The last interrupt happened on counter for unit n reaching thres1. (RO)
#define PCNT_THR_THRES1_LAT_U       0x04
// This register stores the current status of the counter.
#define PCNT_THR_ZERO_MODE_U_PLUS0  0x00
#define PCNT_THR_ZERO_MODE_U_MINUS0 0x01
#define PCNT_THR_ZERO_MODE_U_POS    0x02
#define PCNT_THR_ZERO_MODE_U_NEG    0x03

#endif
