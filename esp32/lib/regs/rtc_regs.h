#ifndef H_RTC_REGS
#define H_RTC_REGS
/*
 * ESP32 alternative library, Low­Power Management (RTC_CNTL) module
 * registers.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Configure RTC options */
#define RTC_CNTL_OPTIONS0_REG           MMIO32(RTC_BASE + 0x00)
// SW system reset. (WO)
#define RTC_CNTL_SW_SYS_RST                 0x80000000
// The digital core forces no reset in deep sleep. (R/W)
#define RTC_CNTL_DG_WRAP_FORCE_NORST        0x40000000
// The digital core can force a reset in deep sleep. (R/W)
#define RTC_CNTL_DG_WRAP_FORCE_RST          0x20000000
// BIAS_CORE force power up. (R/W)
#define RTC_CNTL_BIAS_CORE_FORCE_PU         0x00400000
// BIAS_CORE force power down. (R/W)
#define RTC_CNTL_BIAS_CORE_FORCE_PD         0x00200000
// BIAS_CORE follow CK8M. (R/W)
#define RTC_CNTL_BIAS_CORE_FOLW_8M          0x00100000
// BIAS_I2C force power up. (R/W)
#define RTC_CNTL_BIAS_I2C_FORCE_PU          0x00080000
// BIAS_I2C force power down. (R/W)
#define RTC_CNTL_BIAS_I2C_FORCE_PD          0x00040000
// BIAS_I2C follow CK8M. (R/W)
#define RTC_CNTL_BIAS_I2C_FOLW_8M           0x00020000
// BIAS_SLEEP force no sleep. (R/W)
#define RTC_CNTL_BIAS_FORCE_NOSLEEP         0x00010000
// BIAS_SLEEP force sleep. (R/W)
#define RTC_CNTL_BIAS_FORCE_SLEEP           0x00008000
// BIAS_SLEEP follow CK8M. (R/W)
#define RTC_CNTL_BIAS_SLEEP_FOLW_8M         0x00004000
// Crystal force power up. (R/W)
#define RTC_CNTL_XTL_FORCE_PU               0x00002000
// Crystal force power down. (R/W)
#define RTC_CNTL_XTL_FORCE_PD               0x00001000
// BB_PLL force power up. (R/W)
#define RTC_CNTL_BBPLL_FORCE_PU             0x00000800
// BB_PLL force power down. (R/W)
#define RTC_CNTL_BBPLL_FORCE_PD             0x00000400
// BB_PLL_I2C force power up. (R/W)
#define RTC_CNTL_BBPLL_I2C_FORCE_PU         0x00000200
// BB_PLL _I2C force power down. (R/W)
#define RTC_CNTL_BBPLL_I2C_FORCE_PD         0x00000100
// BB_I2C force power up. (R/W)
#define RTC_CNTL_BB_I2C_FORCE_PU            0x00000080
// BB_I2C force power down. (R/W)
#define RTC_CNTL_BB_I2C_FORCE_PD            0x00000040
// PRO_CPU SW reset. (WO)
#define RTC_CNTL_SW_PROCPU_RST              0x00000020
// APP_CPU SW reset. (WO)
#define RTC_CNTL_SW_APPCPU_RST              0x00000010
// described under RTC_CNTL_SW_CPU_STALL_REG. (R/W)
#define RTC_CNTL_SW_STALL_PROCPU_C0_MSK     0x0000000c
#define RTC_CNTL_SW_STALL_PROCPU_C0_SFT     2
#define RTC_CNTL_SW_STALL_PROCPU_C0_SET(n) ((n<<RTC_CNTL_SW_STALL_PROCPU_C0_SFT)&RTC_CNTL_SW_STALL_PROCPU_C0_MSK)
// described under RTC_CNTL_SW_CPU_STALL_REG. (R/W)
#define RTC_CNTL_SW_STALL_APPCPU_C0_MSK     0x00000003
#define RTC_CNTL_SW_STALL_APPCPU_C0_SET(n)  (n&RTC_CNTL_SW_STALL_APPCPU_C0_MSK)

/* RTC sleep timer */
#define RTC_CNTL_SLP_TIMER0_REG         MMIO32(RTC_BASE + 0x04)
// RTC sleep timer low 32 bits. (R/W)

/* RTC sleep timer, alarm and control */
#define RTC_CNTL_SLP_TIMER1_REG         MMIO32(RTC_BASE + 0x08)
// Timer alarm enable bit. (R/W)
#define RTC_CNTL_MAIN_TIMER_ALARM_EN    0x10000
// RTC sleep timer high 16 bits. (R/W)
#define RTC_CNTL_SLP_VAL_HI_MSK         0x0ffff

/* Update control of RTC timer */
#define RTC_CNTL_TIME_UPDATE_REG        MMIO32(RTC_BASE + 0x0c)
// Set 1: to update register with RTC timer. (WO)
#define RTC_CNTL_TIME_UPDATE    0x80000000
// Indicates that the register is updated. (RO)
#define RTC_CNTL_TIME_VALID     0x40000000

/* RTC timer low 32 bits */
#define RTC_CNTL_TIME0_REG              MMIO32(RTC_BASE + 0x10)
// RTC timer low 32 bits. (RO)

/* RTC timer high 16 bits */
#define RTC_CNTL_TIME1_REG              MMIO32(RTC_BASE + 0x14)
// RTC timer high 16 bits. (RO)
#define RTC_CNTL_TIME_HI    0xffff

/* RTC sleep, SDIO and ULP control */
#define RTC_CNTL_STATE0_REG             MMIO32(RTC_BASE + 0x18)
// Sleep enable bit. (R/W)
#define RTC_CNTL_SLEEP_EN               0x80000000
// Sleep reject bit. (R/W)
#define RTC_CNTL_SLP_REJECT             0x40000000
// Sleep wake-up bit. (R/W)
#define RTC_CNTL_SLP_WAKEUP             0x20000000
// SDIO active indication. (RO)
#define RTC_CNTL_SDIO_ACTIVE_IND        0x10000000
// ULP coprocessor timer enable bit. (R/W)
#define RTC_CNTL_ULP_CP_SLP_TIMER_EN    0x01000000
// Touch timer enable bit. (R/W)
#define RTC_CNTL_TOUCH_SLP_TIMER_EN     0x00800000

/* CPU stall enable */
#define RTC_CNTL_TIMER1_REG             MMIO32(RTC_BASE + 0x1c)
// CPU stall enable bit. (R/W)

/* Slow clock and touch controller configuration */
#define RTC_CNTL_TIMER2_REG             MMIO32(RTC_BASE + 0x20)
// Minimal amount of cycles in RTC_SLOW_CLK to power down CK8M. (R/W)
#define RTC_CNTL_MIN_TIME_CK8M_OFF_MSK          0xff000000
#define RTC_CNTL_MIN_TIME_CK8M_OFF_SFT          24
#define RTC_CNTL_MIN_TIME_CK8M_OFF_SET(n) ((n<<RTC_CNTL_MIN_TIME_CK8M_OFF_SFT)&RTC_CNTL_MIN_TIME_CK8M_OFF_MSK)
// Awaited cycles in RTC_SLOW_CLK before ULP coprocessor/touch ctrller stas working.
#define RTC_CNTL_ULPCP_TOUCH_START_WAIT_MSK     0x00ff0000
#define RTC_CNTL_ULPCP_TOUCH_START_WAIT_SFT     15
#define RTC_CNTL_ULPCP_TOUCH_START_WAIT_SET(n) ((n<<RTC_CNTL_ULPCP_TOUCH_START_WAIT_SFT)&RTC_CNTL_ULPCP_TOUCH_START_WAIT_MSK)

/* Minimal sleep cycles in slow clock */
#define RTC_CNTL_TIMER5_REG             MMIO32(RTC_BASE + 0x2c)
// Minimal amount of sleep cycles in RTC_SLOW_CLK. (R/W)
#define RTC_CNTL_MIN_SLP_VAL_MSK        0xff00
#define RTC_CNTL_MIN_SLP_VAL_SFT        8
#define RTC_CNTL_MIN_SLP_VAL_SET(n) ((n<<RTC_CNTL_MIN_SLP_VAL_SFT)&RTC_CNTL_MIN_SLP_VAL_MSK)


/* Reset state control and cause of CPUs */
#define RTC_CNTL_RESET_STATE_REG        MMIO32(RTC_BASE + 0x34)
// PRO_CPU state vector selection. (R/W)
#define RTC_CNTL_PROCPU_STAT_VECTOR_SEL     0x2000
// APP_CPU state vector selection. (R/W)
#define RTC_CNTL_APPCPU_STAT_VECTOR_SEL     0x1000
// Reset cause for APP_CPU. (RO)
#define RTC_CNTL_RESET_CAUSE_APPCPU_MSK     0x0fc0
#define RTC_CNTL_RESET_CAUSE_APPCPU_SFT     6
#define RTC_CNTL_RESET_CAUSE_APPCPU_SET(n) ((n<<RTC_CNTL_RESET_CAUSE_APPCPU_SFT)&RTC_CNTL_RESET_CAUSE_APPCPU_MSK)
// Reset cause for PRO_CPU. (RO)
#define RTC_CNTL_RESET_CAUSE_PROCPU_MSK     0x003f
#define RTC_CNTL_RESET_CAUSE_PROCPU_SET(n) (n&RTC_CNTL_RESET_CAUSE_PROCPU_MSK)

/* Wake-up filter, enable and cause */
#define RTC_CNTL_WAKEUP_STATE_REG       MMIO32(RTC_BASE + 0x38)
// Enable filter for GPIO wake-up event. (R/W)
#define RTC_CNTL_GPIO_WAKEUP_FILTER     0x400000
// Wake-up enable bitmap. (R/W)
#define RTC_CNTL_WAKEUP_ENA_MSK         0x3ff800
#define RTC_CNTL_WAKEUP_ENA_SFT         11
#define RTC_CNTL_WAKEUP_ENA_SET(n) ((n<<RTC_CNTL_WAKEUP_ENA_SFT)&RTC_CNTL_WAKEUP_ENA_MSK)
// Wake-up cause. (RO)
#define RTC_CNTL_WAKEUP_CAUSE_MSK       0x0007ff
#define RTC_CNTL_WAKEUP_CAUSE_SET(n) (n&RTC_CNTL_WAKEUP_CAUSE_MSK)

/* Configuration of wake-up at low/high level */
#define RTC_CNTL_EXT_WAKEUP_CONF_REG    MMIO32(RTC_BASE + 0x60)
// Indicates the RTC is ready to be triggered by any wakeup source. (RO)
#define RTC_CNTL_RTC_RDY_FOR_WAKEUP 0x08000000
// Indicates the RTC state.
#define RTC_CNTL_MAIN_STATE_IN_IDLE 0x00008000

/* Selection of pads for external wake-up and wake-up clear bit */
#define RTC_CNTL_EXT_WAKEUP1_REG        MMIO32(RTC_BASE + 0xcc)
// Clear external wakeup1 status. (WO)
#define RTC_CNTL_EXT_WAKEUP1_STATUS_CLR 0x40000
// Bitmap to select RTC pads for external wakeup1. (R/W)
#define RTC_CNTL_EXT_WAKEUP1_SEL_MSK    0x3ffff
#define RTC_CNTL_EXT_WAKEUP1_SEL_SET(n) (n&RTC_CNTL_EXT_WAKEUP1_SEL_MSK)

/* External wake-up status */
#define RTC_CNTL_EXT_WAKEUP1_STATUS_REG MMIO32(RTC_BASE + 0xd0)
// External wakeup1 status. (RO)
#define RTC_CNTL_EXT_WAKEUP1_STATUS_MSK 0x3ffff

/* Interrupt enable bits */
#define RTC_CNTL_INT_ENA_REG            MMIO32(RTC_BASE + 0x3c)
// The interrupt enable bit for the RTC_CNTL_MAIN_TIMER_INT interrupt. (R/W)
#define RTC_CNTL_MAIN_TIMER_INT_ENA     0x100
// The interrupt enable bit for the RTC_CNTL_BROWN_OUT_INT interrupt. (R/W)
#define RTC_CNTL_BROWN_OUT_INT_ENA      0x080
// The interrupt enable bit for the RTC_CNTL_TOUCH_INT interrupt. (R/W)
#define RTC_CNTL_TOUCH_INT_ENA          0x040
// The interrupt enable bit for the RTC_CNTL_ULP_CP_INT interrupt. (R/W)
#define RTC_CNTL_ULP_CP_INT_ENA         0x020
// The interrupt enable bit for the RTC_CNTL_TIME_VALID_INT interrupt. (R/W)
#define RTC_CNTL_TIME_VALID_INT_ENA     0x010
// The interrupt enable bit for the RTC_CNTL_WDT_INT interrupt. (R/W)
#define RTC_CNTL_WDT_INT_ENA            0x008
// The interrupt enable bit for the RTC_CNTL_SDIO_IDLE_INT interrupt. (R/W)
#define RTC_CNTL_SDIO_IDLE_INT_ENA      0x004
// The interrupt enable bit for the RTC_CNTL_SLP_REJECT_INT interrupt. (R/W)
#define RTC_CNTL_SLP_REJECT_INT_ENA     0x002
// The interrupt enable bit for the RTC_CNTL_SLP_WAKEUP_INT interrupt. (R/W)
#define RTC_CNTL_SLP_WAKEUP_INT_ENA     0x001

/* Raw interrupt status */
#define RTC_CNTL_INT_RAW_REG            MMIO32(RTC_BASE + 0x40)
// The raw interrupt status bit for the RTC_CNTL_MAIN_TIMER_INT interrupt. (RO)
#define RTC_CNTL_MAIN_TIMER_INT_RAW     0x100
// The raw interrupt status bit for the RTC_CNTL_BROWN_OUT_INT interrupt. (RO)
#define RTC_CNTL_BROWN_OUT_INT_RAW      0x080
// The raw interrupt status bit for the RTC_CNTL_TOUCH_INT interrupt. (RO)
#define RTC_CNTL_TOUCH_INT_RAW          0x040
// The raw interrupt status bit for the RTC_CNTL_ULP_CP_INT interrupt. (RO)
#define RTC_CNTL_ULP_CP_INT_RAW         0x020
// The raw interrupt status bit for the RTC_CNTL_TIME_VALID_INT interrupt. (RO)
#define RTC_CNTL_TIME_VALID_INT_RAW     0x010
// The raw interrupt status bit for the RTC_CNTL_WDT_INT interrupt. (RO)
#define RTC_CNTL_WDT_INT_RAW            0x008
// The raw interrupt status bit for the RTC_CNTL_SDIO_IDLE_INT interrupt. (RO)
#define RTC_CNTL_SDIO_IDLE_INT_RAW      0x004
// The raw interrupt status bit for the RTC_CNTL_SLP_REJECT_INT interrupt. (RO)
#define RTC_CNTL_SLP_REJECT_INT_RAW     0x002
// The raw interrupt status bit for the RTC_CNTL_SLP_WAKEUP_INT interrupt. (RO)
#define RTC_CNTL_SLP_WAKEUP_INT_RAW     0x001

/* Masked interrupt status */
#define RTC_CNTL_INT_ST_REG             MMIO32(RTC_BASE + 0x44)
// The masked interrupt status bit for the RTC_CNTL_MAIN_TIMER_INT interrupt. (RO)
#define RTC_CNTL_MAIN_TIMER_INT_ST      0x100
// The masked interrupt status bit for the RTC_CNTL_BROWN_OUT_INT interrupt. (RO)
#define RTC_CNTL_BROWN_OUT_INT_ST       0x080
// The masked interrupt status bit for the RTC_CNTL_TOUCH_INT interrupt. (RO)
#define RTC_CNTL_TOUCH_INT_ST           0x040
// The masked interrupt status bit for the RTC_CNTL_ULP_CP_INT interrupt. (RO)
#define RTC_CNTL_ULP_CP_INT_ST          0x020
// The masked interrupt status bit for the RTC_CNTL_TIME_VALID_INT interrupt. (RO)
#define RTC_CNTL_TIME_VALID_INT_ST      0x010
// The masked interrupt status bit for the RTC_CNTL_WDT_INT interrupt. (RO)
#define RTC_CNTL_WDT_INT_ST             0x008
// The masked interrupt status bit for the RTC_CNTL_SDIO_IDLE_INT interrupt. (RO)
#define RTC_CNTL_SDIO_IDLE_INT_ST       0x004
// The masked interrupt status bit for the RTC_CNTL_SLP_REJECT_INT interrupt. (RO)
#define RTC_CNTL_SLP_REJECT_INT_ST      0x002
// The masked interrupt status bit for the RTC_CNTL_SLP_WAKEUP_INT interrupt. (RO)
#define RTC_CNTL_SLP_WAKEUP_INT_ST      0x001

/* Interrupt clear bits */
#define RTC_CNTL_INT_CLR_REG            MMIO32(RTC_BASE + 0x48)
// Set this bit to clear the RTC_CNTL_MAIN_TIMER_INT interrupt. (WO)
#define RTC_CNTL_MAIN_TIMER_INT_CLR     0x100
// Set this bit to clear the RTC_CNTL_BROWN_OUT_INT interrupt. (WO)
#define RTC_CNTL_BROWN_OUT_INT_CLR      0x080
// Set this bit to clear the RTC_CNTL_TOUCH_INT interrupt. (WO)
#define RTC_CNTL_TOUCH_INT_CLR          0x040
// Set this bit to clear the RTC_CNTL_ULP_CP_INT interrupt. (WO)
#define RTC_CNTL_ULP_CP_INT_CLR         0x020
// Set this bit to clear the RTC_CNTL_TIME_VALID_INT interrupt. (WO)
#define RTC_CNTL_TIME_VALID_INT_CLR     0x010
// Set this bit to clear the RTC_CNTL_WDT_INT interrupt. (WO)
#define RTC_CNTL_WDT_INT_CLR            0x008
// Set this bit to clear the RTC_CNTL_SDIO_IDLE_INT interrupt. (WO)
#define RTC_CNTL_SDIO_IDLE_INT_CLR      0x004
// Set this bit to clear the RTC_CNTL_SLP_REJECT_INT interrupt. (WO)
#define RTC_CNTL_SLP_REJECT_INT_CLR     0x002
// Set this bit to clear the RTC_CNTL_SLP_WAKEUP_INT interrupt. (WO)
#define RTC_CNTL_SLP_WAKEUP_INT_CLR     0x001

/* General purpose retention register n */
#define RTC_CNTL_STORE0_REG             MMIO32(RTC_BASE + 0x4c)
#define RTC_CNTL_STORE1_REG             MMIO32(RTC_BASE + 0x50)
#define RTC_CNTL_STORE2_REG             MMIO32(RTC_BASE + 0x54)
#define RTC_CNTL_STORE3_REG             MMIO32(RTC_BASE + 0x58)
#define RTC_CNTL_STORE4_REG             MMIO32(RTC_BASE + 0xb0)
#define RTC_CNTL_STORE5_REG             MMIO32(RTC_BASE + 0xb4)
#define RTC_CNTL_STORE6_REG             MMIO32(RTC_BASE + 0xb8)
#define RTC_CNTL_STORE7_REG             MMIO32(RTC_BASE + 0xbc)
// 32-bit general-purpose retention register. (R/W)

/* Power-up/down configuration */
#define RTC_CNTL_ANA_CONF_REG           MMIO32(RTC_BASE + 0x30)
// 1: PLL_I2C power up, otherwise power down. (R/W)
#define RTC_CNTL_PLL_I2C_PU     0x80000000
// 1: CKGEN_I2C power up, otherwise power down. (R/W)
#define RTC_CNTL_CKGEN_I2C_PU   0x40000000
// 1: RFRX_PBUS power up, otherwise power down. (R/W)
#define RTC_CNTL_RFRX_PBUS_PU   0x10000000
// 1: TXRF_I2C power up, otherwise power down. (R/W)
#define RTC_CNTL_TXRF_I2C_PU    0x08000000
// 1: PVTMON power up, otherwise power down. (R/W)
#define RTC_CNTL_PVTMON_PU      0x04000000
// PLLA force power up. (R/W)
#define RTC_CNTL_PLLA_FORCE_PU  0x01000000
// PLLA force power down. (R/W)
#define RTC_CNTL_PLLA_FORCE_PD  0x00800000

/* Internal power distribution and control */
#define RTC_CNTL_VREG_REG               MMIO32(RTC_BASE + 0x7c)
// RTC voltage regulator - force power up. (R/W)
#define RTC_CNTL_VREG_FORCE_PU              0x80000000
// RTC voltage regulator - force power down (voltage to 0.8V or lower). (R/W)
#define RTC_CNTL_VREG_FORCE_PD              0x40000000
// RTC_DBOOST force power up. (R/W)
#define RTC_CNTL_DBOOST_FORCE_PU            0x20000000
// RTC_DBOOST force power down. (R/W)
#define RTC_CNTL_DBOOST_FORCE_PD            0x10000000
// RTC_DBIAS during wake-up. (R/W)
#define RTC_CNTL_DBIAS_WAK_MSK              0x0e000000
#define RTC_CNTL_DBIAS_WAK_SFT              25
#define RTC_CNTL_DBIAS_WAK_SET(n) ((n<<RTC_CNTL_DBIAS_WAK_SFT)&RTC_CNTL_DBIAS_WAK_MSK)
// RTC_DBIAS during sleep. (R/W)
#define RTC_CNTL_DBIAS_SLP_MSK              0x01c00000
#define RTC_CNTL_DBIAS_SLP_SFT              22
#define RTC_CNTL_DBIAS_SLP_SET(n) ((n<<RTC_CNTL_DBIAS_SLP_SFT)&RTC_CNTL_DBIAS_SLP_MSK)
// Used to adjust the frequency of RTC slow clock. (R/W)
#define RTC_CNTL_SCK_DCAP_MSK               0x003fc000
#define RTC_CNTL_SCK_DCAP_SFT               14
#define RTC_CNTL_SCK_DCAP_SET(n) ((n<<RTC_CNTL_SCK_DCAP_SFT)&RTC_CNTL_SCK_DCAP_MSK)
// Digital voltage regulator DBIAS during wake-up. (R/W)
#define RTC_CNTL_DIG_VREG_DBIAS_WAK_MSK     0x00003800
#define RTC_CNTL_DIG_VREG_DBIAS_WAK_SFT     11
#define RTC_CNTL_DIG_VREG_DBIAS_WAK_SET(n) ((n<<RTC_CNTL_DIG_VREG_DBIAS_WAK_SFT)&RTC_CNTL_DIG_VREG_DBIAS_WAK_MSK)
// Digital voltage regulator DBIAS during sleep. (R/W)
#define RTC_CNTL_DIG_VREG_DBIAS_SLP_MSK     0x00000700
#define RTC_CNTL_DIG_VREG_DBIAS_SLP_SFT     8
#define RTC_CNTL_DIG_VREG_DBIAS_SLP_SET(n) ((n<<RTC_CNTL_DIG_VREG_DBIAS_SLP_SFT)&RTC_CNTL_DIG_VREG_DBIAS_SLP_MSK)

/* RTC domain power management */
#define RTC_CNTL_PWC_REG                MMIO32(RTC_BASE + 0x80)
// Enable power down rtc_peri in sleep. (R/W)
#define RTC_CNTL_PD_EN                  0x100000
// rtc_peri force power up. (R/W)
#define RTC_CNTL_FORCE_PU               0x080000
// rtc_peri force power down. (R/W)
#define RTC_CNTL_FORCE_PD               0x040000
// Enable power down RTC memory in sleep. (R/W)
#define RTC_CNTL_SLOWMEM_PD_EN          0x020000
// RTC memory force power up. (R/W)
#define RTC_CNTL_SLOWMEM_FORCE_PU       0x010000
// RTC memory force power down. (R/W)
#define RTC_CNTL_SLOWMEM_FORCE_PD       0x008000
// Enable power down fast RTC memory in sleep. (R/W)
#define RTC_CNTL_FASTMEM_PD_EN          0x004000
// Fast RTC memory force power up. (R/W)
#define RTC_CNTL_FASTMEM_FORCE_PU       0x002000
// Fast RTC memory force power down. (R/W)
#define RTC_CNTL_FASTMEM_FORCE_PD       0x001000
// RTC memory force power up in low-power mode. (R/W)
#define RTC_CNTL_SLOWMEM_FORCE_LPU      0x000800
// RTC memory force power down in low-power mode. (R/W)
#define RTC_CNTL_SLOWMEM_FORCE_LPD      0x000400
// 1: RTC memory low-power mode PD following CPU;
#define RTC_CNTL_SLOWMEM_FOLW_CPU       0x000200
// Fast RTC memory force power up in low-power mode. (R/W)
#define RTC_CNTL_FASTMEM_FORCE_LPU      0x000100
// Fast RTC memory force power down in low-power mode. (R/W)
#define RTC_CNTL_FASTMEM_FORCE_LPD      0x000080
// 1: Fast RTC memory low-power mode PD following CPU;
#define RTC_CNTL_FASTMEM_FOLW_CPU       0x000040
// rtc_peri force no isolation. (R/W)
#define RTC_CNTL_FORCE_NOISO            0x000020
// rtc_peri force isolation. (R/W)
#define RTC_CNTL_FORCE_ISO              0x000010
// RTC memory force isolation. (R/W)
#define RTC_CNTL_SLOWMEM_FORCE_ISO      0x000008
// RTC memory force no isolation. (R/W)
#define RTC_CNTL_SLOWMEM_FORCE_NOISO    0x000004
// Fast RTC memory force isolation. (R/W)
#define RTC_CNTL_FASTMEM_FORCE_ISO      0x000002
// Fast RTC memory force no isolation. (R/W)
#define RTC_CNTL_FASTMEM_FORCE_NOISO    0x000001

/* Digital domain power management */
#define RTC_CNTL_DIG_PWC_REG            MMIO32(RTC_BASE + 0x84)
// Enable power down digital core in sleep mode. (R/W)
#define RTC_CNTL_DG_WRAP_PD_EN          0x80000000
// Enable power down Wi-Fi in sleep. (R/W)
#define RTC_CNTL_WIFI_PD_EN             0x40000000
// Enable power down internal SRAM 4 in sleep mode. (R/W)
#define RTC_CNTL_INTER_RAM4_PD_EN       0x20000000
// Enable power down internal SRAM 3 in sleep mode. (R/W)
#define RTC_CNTL_INTER_RAM3_PD_EN       0x10000000
// Enable power down internal SRAM 2 in sleep mode. (R/W)
#define RTC_CNTL_INTER_RAM2_PD_EN       0x08000000
// Enable power down internal SRAM 1 in sleep mode. (R/W)
#define RTC_CNTL_INTER_RAM1_PD_EN       0x04000000
// Enable power down internal SRAM 0 in sleep mode. (R/W)
#define RTC_CNTL_INTER_RAM0_PD_EN       0x02000000
// Enable power down ROM in sleep mode. (R/W)
#define RTC_CNTL_ROM0_PD_EN             0x01000000
// Digital core force power up. (R/W)
#define RTC_CNTL_DG_WRAP_FORCE_PU       0x00100000
// Digital core force power down. (R/W)
#define RTC_CNTL_DG_WRAP_FORCE_PD       0x00080000
// Wi-Fi force power up. (R/W)
#define RTC_CNTL_WIFI_FORCE_PU          0x00040000
// Wi-Fi force power down. (R/W)
#define RTC_CNTL_WIFI_FORCE_PD          0x00020000
// Internal SRAM 4 force power up. (R/W)
#define RTC_CNTL_INTER_RAM4_FORCE_PU    0x00010000
// Internal SRAM 4 force power down. (R/W)
#define RTC_CNTL_INTER_RAM4_FORCE_PD    0x00008000
// Internal SRAM 3 force power up. (R/W)
#define RTC_CNTL_INTER_RAM3_FORCE_PU    0x00004000
// Internal SRAM 3 force power down. (R/W)
#define RTC_CNTL_INTER_RAM3_FORCE_PD    0x00002000
// Internal SRAM 2 force power up. (R/W)
#define RTC_CNTL_INTER_RAM2_FORCE_PU    0x00001000
// Internal SRAM 2 force power down. (R/W)
#define RTC_CNTL_INTER_RAM2_FORCE_PD    0x00000800
// Internal SRAM 1 force power up. (R/W)
#define RTC_CNTL_INTER_RAM1_FORCE_PU    0x00000400
// Internal SRAM 1 force power down. (R/W)
#define RTC_CNTL_INTER_RAM1_FORCE_PD    0x00000200
// Internal SRAM 0 force power up. (R/W)
#define RTC_CNTL_INTER_RAM0_FORCE_PU    0x00000100
// Internal SRAM 0 force power down. (R/W)
#define RTC_CNTL_INTER_RAM0_FORCE_PD    0x00000080
// ROM force power up. (R/W)
#define RTC_CNTL_ROM0_FORCE_PU          0x00000040
// ROM force power down. (R/W)
#define RTC_CNTL_ROM0_FORCE_PD          0x00000020
// Memories in digital core force power up in sleep mode. (R/W)
#define RTC_CNTL_LSLP_MEM_FORCE_PU      0x00000010
// Memories in digital core force power down in sleep mode. (R/W)
#define RTC_CNTL_LSLP_MEM_FORCE_PD      0x00000008

/* Digital domain isolation control */
#define RTC_CNTL_DIG_ISO_REG            MMIO32(RTC_BASE + 0x88)
// Digital core force no isolation. (R/W)
#define RTC_CNTL_DG_WRAP_FORCE_NOISO                0x80000000
// Digital core force isolation. (R/W)
#define RTC_CNTL_DG_WRAP_FORCE_ISO                  0x40000000
// Wi-Fi force no isolation. (R/W)
#define RTC_CNTL_WIFI_FORCE_NOISO                   0x20000000
// Wi-Fi force isolation. (R/W)
#define RTC_CNTL_WIFI_FORCE_ISO                     0x10000000
// Internal SRAM 4 force no isolation. (R/W)
#define RTC_CNTL_INTER_RAM4_FORCE_NOISO             0x08000000
// Internal SRAM 4 force isolation. (R/W)
#define RTC_CNTL_INTER_RAM4_FORCE_ISO               0x04000000
// Internal SRAM 3 force no isolation. (R/W)
#define RTC_CNTL_INTER_RAM3_FORCE_NOISO             0x02000000
// Internal SRAM 3 force isolation. (R/W)
#define RTC_CNTL_INTER_RAM3_FORCE_ISO               0x01000000
// Internal SRAM 2 force no isolation. (R/W)
#define RTC_CNTL_INTER_RAM2_FORCE_NOISO             0x00800000
// Internal SRAM 2 force isolation. (R/W)
#define RTC_CNTL_INTER_RAM2_FORCE_ISO               0x00400000
// Internal SRAM 1 force no isolation. (R/W)
#define RTC_CNTL_INTER_RAM1_FORCE_NOISO             0x00200000
// Internal SRAM 1 force isolation. (R/W)
#define RTC_CNTL_INTER_RAM1_FORCE_ISO               0x00100000
// Internal SRAM 0 force no isolation. (R/W)
#define RTC_CNTL_INTER_RAM0_FORCE_NOISO             0x00080000
// Internal SRAM 0 force isolation. (R/W)
#define RTC_CNTL_INTER_RAM0_FORCE_ISO               0x00040000
// ROM force no isolation. (R/W)
#define RTC_CNTL_ROM0_FORCE_NOISO                   0x00020000
// ROM force isolation. (R/W)
#define RTC_CNTL_ROM0_FORCE_ISO                     0x00010000
// Digital pad force hold. (R/W)
#define RTC_CNTL_DG_PAD_FORCE_HOLD                  0x00008000
// Digital pad force un-hold. (R/W)
#define RTC_CNTL_DG_PAD_FORCE_UNHOLD                0x00004000
// Digital pad force isolation. (R/W)
#define RTC_CNTL_DG_PAD_FORCE_ISO                   0x00002000
// Digital pad force no isolation. (R/W)
#define RTC_CNTL_DG_PAD_FORCE_NOISO                 0x00001000
// Digital pad enable auto-hold. (R/W)
#define RTC_CNTL_REG_RTC_CNTL_DG_PAD_AUTOHOLD_EN    0x00000800
// Write-only register clears digital pad auto-hold. (WO)
#define RTC_CNTL_CLR_REG_RTC_CNTL_DG_PAD_AUTOHOLD   0x00000400
// Read-only register indicates digital pad auto-hold status. (RO)
#define RTC_CNTL_DG_PAD_AUTOHOLD                    0x00000200

/* WDT Configuration register n */
#define RTC_CNTL_WDTCONFIG0_REG         MMIO32(RTC_BASE + 0x8c)
#define RTC_CNTL_WDTCONFIG1_REG         MMIO32(RTC_BASE + 0x90)
#define RTC_CNTL_WDTCONFIG2_REG         MMIO32(RTC_BASE + 0x94)
#define RTC_CNTL_WDTCONFIG3_REG         MMIO32(RTC_BASE + 0x98)
#define RTC_CNTL_WDTCONFIG4_REG         MMIO32(RTC_BASE + 0x9c)
// Enable RTC WDT. (R/W)
#define RTC_CNTL_WDT_EN                         0x80000000
//
#define RTC_CNTL_WDT_STG0_MSK                   0x70000000
#define RTC_CNTL_WDT_STG0_SFT                   28
#define RTC_CNTL_WDT_STG0_SET(n) ((n<<RTC_CNTL_WDT_STG0_SFT)&RTC_CNTL_WDT_STG0_MSK)
//
#define RTC_CNTL_WDT_STG1_MSK                   0x0e000000
#define RTC_CNTL_WDT_STG1_SFT                   25
#define RTC_CNTL_WDT_STG1_SET(n) ((n<<RTC_CNTL_WDT_STG1_SFT)&RTC_CNTL_WDT_STG1_MSK)
//
#define RTC_CNTL_WDT_STG2_MSK                   0x01c00000
#define RTC_CNTL_WDT_STG2_SFT                   22
#define RTC_CNTL_WDT_STG2_SET(n) ((n<<RTC_CNTL_WDT_STG2_SFT)&RTC_CNTL_WDT_STG2_MSK)
//
#define RTC_CNTL_WDT_STG3_MSK                   0x00380000
#define RTC_CNTL_WDT_STG3_SFT                   19
#define RTC_CNTL_WDT_STG3_SET(n) ((n<<RTC_CNTL_WDT_STG3_SFT)&RTC_CNTL_WDT_STG3_MSK)
// CPU reset counter length, unit: RTC_SLOW_CLK cycle.
#define RTC_CNTL_WDT_CPU_RESET_LENGTH_MSK       0x0001c000
#define RTC_CNTL_WDT_CPU_RESET_LENGTH_SFT       14
#define RTC_CNTL_WDT_CPU_RESET_LENGTH_SET(n) ((n<<RTC_CNTL_WDT_CPU_RESET_LENGTH_SFT)&RTC_CNTL_WDT_CPU_RESET_LENGTH_MSK)
// System reset counter length, unit: RTC_SLOW_CLK cycle.
#define RTC_CNTL_WDT_SYS_RESET_LENGTH_MSK       0x00003800
#define RTC_CNTL_WDT_SYS_RESET_LENGTH_SFT       11
#define RTC_CNTL_WDT_SYS_RESET_LENGTH_SET(n) ((n<<RTC_CNTL_WDT_SYS_RESET_LENGTH_SFT)&RTC_CNTL_WDT_SYS_RESET_LENGTH_MSK)
// Enable RTC WDT in flash boot. (R/W)
#define RTC_CNTL_WDT_FLASHBOOT_MOD_EN           0x00000400
// RTC WDT reset PRO_CPU enable. (R/W)
#define RTC_CNTL_WDT_PROCPU_RESET_EN            0x00000200
// RTC WDT reset APP_CPU enable. (R/W)
#define RTC_CNTL_WDT_APPCPU_RESET_EN            0x00000100
// Pause RTC WDT in sleep. (R/W)
#define RTC_CNTL_WDT_PAUSE_IN_SLP               0x00000080

/* Watchdog feed register */
#define RTC_CNTL_WDTFEED_REG            MMIO32(RTC_BASE + 0xa0)
// SW feeds WDT. (WO)
#define RTC_CNTL_WDT_FEED   0x80000000

/* Watchdog write protect register */
#define RTC_CNTL_WDTWPROTECT_REG        MMIO32(RTC_BASE + 0xa4)
// If RTC_CNTL_WDTWPROTECT is other than 0x50d83aa1, then the RTC wtd will be in a
// write-protected mode and RTC_CNTL_WDTCONFIGn_REG will be locked for mods.

/* XTAL control by external pads */
#define RTC_CNTL_EXT_XTL_CONF_REG       MMIO32(RTC_BASE + 0x5c)
// Enable control XTAL with external pads. (R/W)
#define RTC_CNTL_XTL_EXT_CTR_EN 0x80000000
// 0: power down XTAL at high level, 1: power down XTAL at low level. (R/W)
#define RTC_CNTL_XTL_EXT_CTR_LV 0x40000000

/* Reject cause and enable control */
#define RTC_CNTL_SLP_REJECT_CONF_REG    MMIO32(RTC_BASE + 0x64)
// Sleep reject cause. (RO)
#define RTC_CNTL_REJECT_CAUSE_MSK       0xf0000000
#define RTC_CNTL_REJECT_CAUSE_SFT       28
#define RTC_CNTL_REJECT_CAUSE_SET(n) ((n<<RTC_CNTL_REJECT_CAUSE_SFT)&RTC_CNTL_REJECT_CAUSE_MSK)
// Enable reject for deep sleep. (R/W)
#define RTC_CNTL_DEEP_SLP_REJECT_EN     0x08000000
// Enable reject for light sleep. (R/W)
#define RTC_CNTL_LIGHT_SLP_REJECT_EN    0x04000000
// Enable SDIO reject. (R/W)
#define RTC_CNTL_SDIO_REJECT_EN         0x02000000
// Enable GPIO reject. (R/W)
#define RTC_CNTL_GPIO_REJECT_EN         0x01000000

/* CPU period select */
#define RTC_CNTL_CPU_PERIOD_CONF_REG    MMIO32(RTC_BASE + 0x68)
// CPU period selection. (R/W)
#define RTC_CNTL_RTC_CPUPERIOD_SEL_MSK      0xc0000000
#define RTC_CNTL_RTC_CPUPERIOD_SEL_SFT      30
#define RTC_CNTL_RTC_CPUPERIOD_SEL_SET(n) ((n<<RTC_CNTL_RTC_CPUPERIOD_SEL_SFT)&RTC_CNTL_RTC_CPUPERIOD_SEL_MSK)
// CPU selection option. (R/W)
#define RTC_CNTL_CPUSEL_CONF                0x20000000

/* Configuration of RTC clocks */
#define RTC_CNTL_CLK_CONF_REG           MMIO32(RTC_BASE + 0x70)
// RTC_SLOW_CLK sel.
#define RTC_CNTL_ANA_CLK_RTC_SEL_RC_SLOW_CLK        0x00000000
#define RTC_CNTL_ANA_CLK_RTC_SEL_XTL32K_CLK         0x40000000
#define RTC_CNTL_ANA_CLK_RTC_SEL_RC_FAST_DIV_CLK    0x80000000
// RTC_FAST_CLK sel. 0: XTAL div 4, 1: CK8M. (R/W)
#define RTC_CNTL_RTC_FAST_CLK_SEL                   0x20000000
// SoC clock selection.
#define RTC_CNTL_SOC_CLK_SEL_XTAL                   0x00000000
#define RTC_CNTL_SOC_CLK_SEL_PLL                    0x08000000
#define RTC_CNTL_SOC_CLK_SEL_CK8M                   0x10000000
#define RTC_CNTL_SOC_CLK_SEL_APLL                   0x18000000
// CK8M force power up. (R/W)
#define RTC_CNTL_CK8M_FORCE_PU                      0x04000000
// CK8M force power down. (R/W)
#define RTC_CNTL_CK8M_FORCE_PD                      0x02000000
// CK8M_DFREQ. (R/W)
#define RTC_CNTL_CK8M_DFREQ_MSK                     0x01fe0000
#define RTC_CNTL_CK8M_DFREQ_SFT                     17
#define RTC_CNTL_CK8M_DFREQ_SET(n) ((n<<RTC_CNTL_CK8M_DFREQ_SFT)&RTC_CNTL_CK8M_DFREQ_MSK)
// Divider = reg_rtc_cntl_ck8m_div_sel + 1. (R/W)
#define RTC_CNTL_CK8M_DIV_SEL_MSK                   0x00006000
#define RTC_CNTL_CK8M_DIV_SEL_SFT                   12
#define RTC_CNTL_CK8M_DIV_SEL_SET(n) ((n<<RTC_CNTL_CK8M_DIV_SEL_SFT)&RTC_CNTL_CK8M_DIV_SEL_MSK)
// Enable CK8M for digital core (no relation to RTC core). (R/W)
#define RTC_CNTL_DIG_CLK8M_EN                       0x00000400
// Enable RC_FAST_DIV_CLK for digital core (no relation to RTC core). (R/W)
#define RTC_CNTL_DIG_CLK8M_D256_EN                  0x00000200
// Enable XTL32K_CLK for digital core (no relation to RTC core). (R/W)
#define RTC_CNTL_DIG_XTAL32K_EN                     0x00000100
// 1: RC_FAST_DIV_CLK is actually CK8M, 0: RC_FAST_DIV_CLK is CK8M divided by 256.
#define RTC_CNTL_ENB_CK8M_DIV                       0x00000080
// Disable CK8M and RC_FAST_DIV_CLK. (R/W)
#define RTC_CNTL_ENB_CK8M                           0x00000040
// RC_FAST_DIV_CLK divider.
#define RTC_CNTL_CK8M_DIV128                        0x00000000
#define RTC_CNTL_CK8M_DIV256                        0x00000010
#define RTC_CNTL_CK8M_DIV512                        0x00000020
#define RTC_CNTL_CK8M_DIV1024                       0x00000030

/* SDIO configuration */
#define RTC_CNTL_SDIO_CONF_REG          MMIO32(RTC_BASE + 0x74)
// SW option for XPD_SDIO_VREG; active only when reg_rtc_cntl_sdio_force == 1. (R/W)
#define RTC_CNTL_XPD_SDIO_VREG      0x80000000
// SW option for DREFH_SDIO; active only when reg_rtc_cntl_sdio_force == 1. (R/W)
#define RTC_CNTL_DREFH_SDIO_MSK     0x60000000
#define RTC_CNTL_DREFH_SDIO_SFT     29
#define RTC_CNTL_DREFH_SDIO_SET(n) ((n<<RTC_CNTL_DREFH_SDIO_SFT)&RTC_CNTL_DREFH_SDIO_MSK)
// SW option for DREFM_SDIO; active only when reg_rtc_cntl_sdio_force == 1. (R/W)
#define RTC_CNTL_DREFM_SDIO_MSK     0x18000000
#define RTC_CNTL_DREFM_SDIO_SFT     27
#define RTC_CNTL_DREFM_SDIO_SET(n) ((n<<RTC_CNTL_DREFM_SDIO_SFT)&RTC_CNTL_DREFM_SDIO_MSK)
// SW option for DREFL_SDIO; active only when reg_rtc_cntl_sdio_force == 1. (R/W)
#define RTC_CNTL_DREFL_SDIO_MSK     0x06000000
#define RTC_CNTL_DREFL_SDIO_SFT     25
#define RTC_CNTL_DREFL_SDIO_SET(n) ((n<<RTC_CNTL_DREFL_SDIO_SFT)&RTC_CNTL_DREFL_SDIO_MSK)
// Read-only register for REG1P8_READY. (RO)
#define RTC_CNTL_REG1P8_READY       0x01000000
// SW option for SDIO_TIEH; active only when reg_rtc_cntl_sdio_force == 1. (R/W)
#define RTC_CNTL_SDIO_TIEH          0x00800000
// 1: use SW option to control SDIO_VREG; 0: use state machine to control SDIO_VREG.
#define RTC_CNTL_SDIO_FORCE         0x00400000
// Power down SDIO_VREG in sleep; active only when reg_rtc_cntl_sdio_force == 0.
#define RTC_CNTL_SDIO_VREG_PD_EN    0x00200000

/* Stall of CPUs */
#define RTC_CNTL_SW_CPU_STALL_REG       MMIO32(RTC_BASE + 0xac)
// reg_rtc_cntl_sw_stall_procpu_c1[5:0],
// reg_rtc_cntl_sw_stall_procpu_c0[1:0] == 0x86 (100001 10) will stall PRO_CPU
#define RTC_CNTL_SW_STALL_PROCPU_C1_MSK     0xfc000000
#define RTC_CNTL_SW_STALL_PROCPU_C1_SFT     26
#define RTC_CNTL_SW_STALL_PROCPU_C1_SET(n) ((n<<RTC_CNTL_SW_STALL_PROCPU_C1_SFT)&RTC_CNTL_SW_STALL_PROCPU_C1_MSK)
// reg_rtc_cntl_sw_stall_appcpu_c1[5:0],
// reg_rtc_cntl_sw_stall_appcpu_c0[1:0] == 0x86 (100001 10) will stall APP_CPU
#define RTC_CNTL_SW_STALL_APPCPU_C1_MSK     0x03f00000
#define RTC_CNTL_SW_STALL_APPCPU_C1_SFT     20
#define RTC_CNTL_SW_STALL_APPCPU_C1_SET(n) ((n<<RTC_CNTL_SW_STALL_APPCPU_C1_SFT)&RTC_CNTL_SW_STALL_APPCPU_C1_MSK)

/* RTC pad hold register */
#define RTC_CNTL_HOLD_FORCE_REG         MMIO32(RTC_BASE + 0xc8)
// Set to preserve pad’s state during hibernation. (R/W)
#define RTC_CNTL_X32N_HOLD_FORCE        0x20000
#define RTC_CNTL_X32P_HOLD_FORCE        0x10000
#define RTC_CNTL_TOUCH_PAD7_HOLD_FORCE  0x08000
#define RTC_CNTL_TOUCH_PAD6_HOLD_FORCE  0x04000
#define RTC_CNTL_TOUCH_PAD5_HOLD_FORCE  0x02000
#define RTC_CNTL_TOUCH_PAD4_HOLD_FORCE  0x01000
#define RTC_CNTL_TOUCH_PAD3_HOLD_FORCE  0x00800
#define RTC_CNTL_TOUCH_PAD2_HOLD_FORCE  0x00400
#define RTC_CNTL_TOUCH_PAD1_HOLD_FORCE  0x00200
#define RTC_CNTL_TOUCH_PAD0_HOLD_FORCE  0x00100
#define RTC_CNTL_SENSE4_HOLD_FORCE      0x00080
#define RTC_CNTL_SENSE3_HOLD_FORCE      0x00040
#define RTC_CNTL_SENSE2_HOLD_FORCE      0x00020
#define RTC_CNTL_SENSE1_HOLD_FORCE      0x00010
#define RTC_CNTL_PDAC2_HOLD_FORCE       0x00008
#define RTC_CNTL_PDAC1_HOLD_FORCE       0x00004
#define RTC_CNTL_ADC2_HOLD_FORCE        0x00002
#define RTC_CNTL_ADC1_HOLD_FORCE        0x00001

/* Brownout management */
#define RTC_CNTL_BROWN_OUT_REG          MMIO32(RTC_BASE + 0xd4)
// Brownout detect. (RO)
#define RTC_CNTL_BROWN_OUT_DET              0x80000000
// Enables brownout. (R/W)
#define RTC_CNTL_BROWN_OUT_ENA              0x40000000
// Brownout threshold. The brownout detector will reset the chip when the supply
#define RTC_CNTL_DBROWN_OUT_THRES2P43V      0x00000000
#define RTC_CNTL_DBROWN_OUT_THRES2P48V      0x08000000
#define RTC_CNTL_DBROWN_OUT_THRES2P58V      0x10000000
#define RTC_CNTL_DBROWN_OUT_THRES2P62V      0x18000000
#define RTC_CNTL_DBROWN_OUT_THRES2P67V      0x20000000
#define RTC_CNTL_DBROWN_OUT_THRES2P7V       0x28000000
#define RTC_CNTL_DBROWN_OUT_THRES2P77V      0x30000000
#define RTC_CNTL_DBROWN_OUT_THRES2P8V       0x38000000
#define RTC_CNTL_DBROWN_OUT_THRES_MSK       0x38000000
// Enables brownout reset. (R/W)
#define RTC_CNTL_BROWN_OUT_RST_ENA          0x04000000
// Brownout reset wait cycles. (R/W)
#define RTC_CNTL_BROWN_OUT_RST_WAIT_MSK     0x03ff0000
#define RTC_CNTL_BROWN_OUT_RST_WAIT_SFT     15
#define RTC_CNTL_BROWN_OUT_RST_WAIT_SET(n) ((n<<RTC_CNTL_BROWN_OUT_RST_WAIT_SFT)&RTC_CNTL_BROWN_OUT_RST_WAIT_MSK)
// Enables power down RF when brownout happens. (R/W)
#define RTC_CNTL_BROWN_OUT_PD_RF_ENA        0x00008000
// Sends suspend command to flash when brownout happens. (R/W)
#define RTC_CNTL_BROWN_OUT_CLOSE_FLASH_ENA  0x00004000

#endif
