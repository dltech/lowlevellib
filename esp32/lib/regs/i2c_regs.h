#ifndef H_I2C_REGS
#define H_I2C_REGS
/*
 * ESP32 alternative library, I2C (Inter-Integrated Circuit) bus regs.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Configures the low level width of the SCL clock */
#define I2C0_SCL_LOW_PERIOD_REG     MMIO32(I2C0_BASE + 0x00)
#define I2C1_SCL_LOW_PERIOD_REG     MMIO32(I2C1_BASE + 0x00)
// how long SCL remains low in master mode, in APB clock cycles. (R/W)
#define I2C_SCL_LOW_PERIOD_MSK  0x3fff

/*  */
#define I2C0_CTR_REG                MMIO32(I2C0_BASE + 0x04)
#define I2C1_CTR_REG                MMIO32(I2C1_BASE + 0x04)
// This bit is used to control the storage mode for received data. (R/W)
#define I2C_RX_LSB_FIRST        0x80
// This bit is used to control the sending mode for data needing to be sent. (R/W)
#define I2C_TX_LSB_FIRST        0x40
// Set this bit to start sending the data in txfifo. (R/W)
#define I2C_TRANS_START         0x20
// Set this bit to configure the module as an I2C Master.
#define I2C_MS_MODE             0x10
// 1: sample SDA data on the SCL low leve
#define I2C_SAMPLE_SCL_LEVEL    0x04
// 0: direct output; 1: open drain output. (R/W)
#define I2C_SCL_FORCE_OUT       0x02
// 0: direct output; 1: open drain output. (R/W)
#define I2C_SDA_FORCE_OUT       0x01

/*  */
#define I2C0_SR_REG                 MMIO32(I2C0_BASE + 0x08)
#define I2C1_SR_REG                 MMIO32(I2C1_BASE + 0x08)
// This field indicates the states of the state machine used to produce SCL. (RO)
#define I2C_SCL_STATE_LAST_IDLE             0x00000000
#define I2C_SCL_STATE_LAST_START            0x10000000
#define I2C_SCL_STATE_LAST_NEGATIVE_EDGE    0x20000000
#define I2C_SCL_STATE_LAST_LOW              0x30000000
#define I2C_SCL_STATE_LAST_POSITIVE_EDGE    0x40000000
#define I2C_SCL_STATE_LAST_HIGH             0x50000000
#define I2C_SCL_STATE_LAST_STOP             0x60000000
// his field indicates the states of the I2C module state machine. (RO)
#define I2C_SCL_MAIN_STATE_LAST_IDLE        0x00000000
#define I2C_SCL_MAIN_STATE_LAST_ADDR        0x01000000
#define I2C_SCL_MAIN_STATE_LAST_ACK_ADDR    0x02000000
#define I2C_SCL_MAIN_STATE_LAST_RX_DATA     0x03000000
#define I2C_SCL_MAIN_STATE_LAST_TX_DATA     0x04000000
#define I2C_SCL_MAIN_STATE_LAST_SEND_ACK    0x05000000
#define I2C_SCL_MAIN_STATE_LAST_WAIT_ACK    0x06000000
// This field stores the amount of received data in RAM. (RO)
#define I2C_TXFIFO_CNT_MSK                  0x00fc0000
#define I2C_TXFIFO_CNT_SFT                  18
#define I2C_TXFIFO_CNT_GET0 ((I2C0_FIFO_CONF_REG>>I2C_TXFIFO_CNT_SFT)&0x3f)
#define I2C_TXFIFO_CNT_GET1 ((I2C1_FIFO_CONF_REG>>I2C_TXFIFO_CNT_SFT)&0x3f)
// This field represents the amount of data needed to be sent. (RO)
#define I2C_RXFIFO_CNT_MSK                  0x00003f00
#define I2C_RXFIFO_CNT_SFT                  8
#define I2C_RXFIFO_CNT_GET0 ((I2C1_FIFO_CONF_REG>>I2C_RXFIFO_CNT_SFT)&0x3f)
#define I2C_RXFIFO_CNT_GET1 ((I2C1_FIFO_CONF_REG>>I2C_RXFIFO_CNT_SFT)&0x3f)
// This field changes to 1 when one byte is transferred. (RO)
#define I2C_BYTE_TRANS                      0x00000040
// When conf as an I2C Slave, and the addr sent by the master is equal to the addr  // of the slave, then this bit will be of high level. (RO)
#define I2C_SLAVE_ADDRESSED                 0x00000020
// 1: the I2C bus is busy transferring data; 0: the I2C bus is in idle state. (RO)
#define I2C_BUS_BUSY                        0x00000010
// When the I2C controller loses control of SCL line, this reg changes to 1. (RO)
#define I2C_ARB_LOST                        0x00000008
// When the I2C cntrl takes more than I2C_TIME_OUT clks to rx a data bit, to 1. (RO)
#define I2C_TIME_OUT                        0x00000004
// When in slave mode, 1: master reads from slave; 0: master writes to slave. (RO)
#define I2C_SLAVE_RW                        0x00000002
// This register stores the value of the received ACK bit. (RO)
#define I2C_ACK_REC                         0x00000001

/*  */
#define I2C0_TO_REG                 MMIO32(I2C0_BASE + 0x0c)
#define I2C1_TO_REG                 MMIO32(I2C1_BASE + 0x0c)
// timeout for receiving a data bit in APB clock cycles. (R/W)
#define I2C_TIME_OUT_REG_MSK    0xfffff

/* Configures the I2C slave address */
#define I2C0_SLAVE_ADDR_REG         MMIO32(I2C0_BASE + 0x10)
#define I2C1_SLAVE_ADDR_REG         MMIO32(I2C1_BASE + 0x10)
// This field is used to enable the slave 10-bit addr mode in master mode. (R/W)
#define I2C_SLAVE_ADDR_10BIT_EN 0x80000000
// When conf as an I2C Slave, this field is used to conf the slave  addr. (R/W)
#define I2C_SLAVE_ADDR_MSK      0x00007fff

/* FIFO status register */
#define I2C0_RXFIFO_ST_REG          MMIO32(I2C0_BASE + 0x14)
#define I2C1_RXFIFO_ST_REG          MMIO32(I2C1_BASE + 0x14)
// This is the offset address of the last sent data
#define I2C_TXFIFO_END_ADDR_MSK     0x1f0000
#define I2C_TXFIFO_END_ADDR_SFT     15
#define I2C_TXFIFO_END_ADDR_GET0 ((I2C0_RXFIFO_ST_REG>>I2C_TXFIFO_END_ADDR_SFT)&0x1f)
#define I2C_TXFIFO_END_ADDR_GET1 ((I2C1_RXFIFO_ST_REG>>I2C_TXFIFO_END_ADDR_SFT)&0x1f)
// This is the offset address of the first sent data
#define I2C_TXFIFO_START_ADDR_MSK   0x00fc00
#define I2C_TXFIFO_START_ADDR_SFT   10
#define I2C_TXFIFO_START_ADDR_GET0 ((I2C0_RXFIFO_ST_REG>>I2C_TXFIFO_START_ADDR_SFT)&0x1f)
#define I2C_TXFIFO_START_ADDR_GET1 ((I2C1_RXFIFO_ST_REG>>I2C_TXFIFO_START_ADDR_SFT)&0x1f)
// This is the offset address of the last received data
#define I2C_RXFIFO_END_ADDR_MSK     0x0003e0
#define I2C_RXFIFO_END_ADDR_SFT     5
#define I2C_RXFIFO_END_ADDR_GET0 ((I2C0_RXFIFO_ST_REG>>I2C_RXFIFO_END_ADDR_SFT)&0x1f)
#define I2C_RXFIFO_END_ADDR_GET1 ((I2C1_RXFIFO_ST_REG>>I2C_RXFIFO_END_ADDR_SFT)&0x1f)
// This is the offset address of the last received data
#define I2C_RXFIFO_START_ADDR_MSK   0x00001f
#define I2C_RXFIFO_START_ADDR_GET0  (I2C0_RXFIFO_ST_REG&I2C_RXFIFO_START_ADDR_MSK)
#define I2C_RXFIFO_START_ADDR_GET1  (I2C1_RXFIFO_ST_REG&I2C_RXFIFO_START_ADDR_MSK)

/* FIFO configuration register */
#define I2C0_FIFO_CONF_REG          MMIO32(I2C0_BASE + 0x18)
#define I2C1_FIFO_CONF_REG          MMIO32(I2C1_BASE + 0x18)
// When I2C sends more than nonfifo_tx_thres bytes of data
#define I2C_NONFIFO_TX_THRES_MSK    0x3f00000
#define I2C_NONFIFO_TX_THRES_SFT    20
#define I2C_NONFIFO_TX_THRES_SET(n) ((n<<I2C_NONFIFO_TX_THRES_SFT)&I2C_NONFIFO_TX_THRES_MSK)
// When I2C receives more than nonfifo_rx_thres bytes of data
#define I2C_NONFIFO_RX_THRES_MSK    0x00fc000
#define I2C_NONFIFO_RX_THRES_SFT    14
#define I2C_NONFIFO_RX_THRES_SET(n) ((n<<I2C_NONFIFO_RX_THRES_SFT)&I2C_NONFIFO_RX_THRES_MSK)
// byte received after the I2C addr byte repr the offs addr in the I2C Slave RAM.
#define I2C_FIFO_ADDR_CFG_EN        0x0000800
// Set this bit to enable APB nonfifo access. (R/W)
#define I2C_NONFIFO_EN              0x0000400

/* Configures the hold time after a negative SCL edge */
#define I2C0_SDA_HOLD_REG           MMIO32(I2C0_BASE + 0x30)
#define I2C1_SDA_HOLD_REG           MMIO32(I2C1_BASE + 0x30)
// time to hold the data after the negative edge of SCL, in APB clock cycles. (R/W)
#define I2C_SDA_HOLD_TIME_MSK   0x3ff

/* Configures the sample time after a positive SCL edge */
#define I2C0_SDA_SAMPLE_REG         MMIO32(I2C0_BASE + 0x34)
#define I2C1_SDA_SAMPLE_REG         MMIO32(I2C1_BASE + 0x34)
// how long SDA is sampled, in APB clock cycles. (R/W)
#define I2C_SDA_SAMPLE_TIME_MSK 0x3ff

/* Configures the high level width of the SCL clock */
#define I2C0_SCL_HIGH_PERIOD_REG    MMIO32(I2C0_BASE + 0x38)
#define I2C1_SCL_HIGH_PERIOD_REG    MMIO32(I2C1_BASE + 0x38)
// how long SCL remains high in master mode, in APB clock cycles. (R/W)
#define I2C_SCL_HIGH_PERIOD 0x3fff

/* Configures the delay between the SDA and SCL neg edge for a start condition */
#define I2C0_SCL_START_HOLD_REG     MMIO32(I2C0_BASE + 0x40)
#define I2C1_SCL_START_HOLD_REG     MMIO32(I2C1_BASE + 0x40)
// time between the neg edge of SDA and the neg edge of SCL for a START condition
#define I2C_SCL_START_HOLD_TIME 0x3ff

/* Configures the delay between the positive edge of SCL and the neg edge of SDA */
#define I2C0_SCL_RSTART_SETUP_REG   MMIO32(I2C0_BASE + 0x44)
#define I2C1_SCL_RSTART_SETUP_REG   MMIO32(I2C1_BASE + 0x44)
// time between the pos edge of SCL and the neg edge of SDA for a RESTART condition
#define I2C_SCL_RSTART_SETUP_TIME   0x3ff

/* Configures the delay after the SCL clock edge for a stop condition */
#define I2C0_SCL_STOP_HOLD_REG      MMIO32(I2C0_BASE + 0x48)
#define I2C1_SCL_STOP_HOLD_REG      MMIO32(I2C1_BASE + 0x48)
// delay after the STOP condition, in APB clock cycles. (R/W)
#define I2C_SCL_STOP_HOLD_TIME  0x3fff

/* Configures the delay between the SDA and SCL positive edge for a stop cond */
#define I2C0_SCL_STOP_SETUP_REG     MMIO32(I2C0_BASE + 0x4c)
#define I2C1_SCL_STOP_SETUP_REG     MMIO32(I2C1_BASE + 0x4c)
// time between the pos edge of SCL and the pos edge of SDA, in APB clock cycles.
#define I2C_SCL_STOP_SETUP_TIME_MSK 0x3ff

/* SCL filter configuration register */
#define I2C0_SCL_FILTER_CFG_REG     MMIO32(I2C0_BASE + 0x50)
#define I2C1_SCL_FILTER_CFG_REG     MMIO32(I2C1_BASE + 0x50)
// This is the filter enable bit for SCL. (R/W)
#define I2C_SCL_FILTER_EN           0x8
// When a pulse on the SCL input has small than this, the I2C will ign that pulse.
#define I2C_SCL_FILTER_THRES_MSK    0x7
#define I2C_SCL_FILTER_THRES_SET(n) (n&I2C_SDA_FILTER_THRES_MSK)

/* SDA filter configuration register */
#define I2C0_SDA_FILTER_CFG_REG     MMIO32(I2C0_BASE + 0x54)
#define I2C1_SDA_FILTER_CFG_REG     MMIO32(I2C1_BASE + 0x54)
// This is the filter enable bit for SDA. (R/W)
#define I2C_SDA_FILTER_EN           0x8
// When a pulse on the SDA input has small than this, the I2C will ign that pulse.
#define I2C_SDA_FILTER_THRES_MSK    0x7
#define I2C_SDA_FILTER_THRES_SET(n) (n&I2C_SDA_FILTER_THRES_MSK)

/* Raw interrupt status */
#define I2C0_INT_RAW_REG            MMIO32(I2C0_BASE + 0x20)
#define I2C1_INT_RAW_REG            MMIO32(I2C1_BASE + 0x20)
// The raw interrupt status bit for the I2C_TX_SEND_EMPTY_INT interrupt. (RO)
#define I2C_TX_SEND_EMPTY_INT_RAW       0x1000
// The raw interrupt status bit for the I2C_RX_REC_FULL_INT interrupt. (RO)
#define I2C_RX_REC_FULL_INT_RAW         0x0800
// The raw interrupt status bit for the I2C_ACK_ERR_INT interrupt. (RO)
#define I2C_ACK_ERR_INT_RAW             0x0400
// The raw interrupt status bit for the I2C_TRANS_START_INT interrupt. (RO)
#define I2C_TRANS_START_INT_RAW         0x0200
// The raw interrupt status bit for the I2C_TIME_OUT_INT interrupt. (RO)
#define I2C_TIME_OUT_INT_RAW            0x0100
// The raw interrupt status bit for the I2C_TRANS_COMPLETE_INT interrupt. (RO)
#define I2C_TRANS_COMPLETE_INT_RAW      0x0080
// The raw interrupt status bit for the I2C_MASTER_TRAN_COMP_INT interrupt. (RO)
#define I2C_MASTER_TRAN_COMP_INT_RAW    0x0040
// The raw interrupt status bit for the I2C_ARBITRATION_LOST_INT interrupt. (RO)
#define I2C_ARBITRATION_LOST_INT_RAW    0x0020
// The raw interrupt status bit for the I2C_END_DETECT_INT interrupt. (RO)
#define I2C_END_DETECT_INT_RAW          0x0008

/* Interrupt clear bits */
#define I2C0_INT_CLR_REG            MMIO32(I2C0_BASE + 0x24)
#define I2C1_INT_CLR_REG            MMIO32(I2C1_BASE + 0x24)
// Set this bit to clear the I2C_TX_SEND_EMPTY_INT interrupt. (RO)
#define I2C_TX_SEND_EMPTY_INT_CLR       0x1000
// Set this bit to clear the I2C_RX_REC_FULL_INT interrupt. (RO)
#define I2C_RX_REC_FULL_INT_CLR         0x0800
// Set this bit to clear the I2C_ACK_ERR_INT interrupt. (RO)
#define I2C_ACK_ERR_INT_CLR             0x0400
// Set this bit to clear the I2C_TRANS_START_INT interrupt. (RO)
#define I2C_TRANS_START_INT_CLR         0x0200
// Set this bit to clear the I2C_TIME_OUT_INT interrupt. (RO)
#define I2C_TIME_OUT_INT_CLR            0x0100
// Set this bit to clear the I2C_TRANS_COMPLETE_INT interrupt. (RO)
#define I2C_TRANS_COMPLETE_INT_CLR      0x0080
// Set this bit to clear the I2C_MASTER_TRAN_COMP_INT interrupt. (RO)
#define I2C_MASTER_TRAN_COMP_INT_CLR    0x0040
// Set this bit to clear the I2C_ARBITRATION_LOST_INT interrupt. (RO)
#define I2C_ARBITRATION_LOST_INT_CLR    0x0020
// Set this bit to clear the I2C_END_DETECT_INT interrupt. (RO)
#define I2C_END_DETECT_INT_CLR          0x0008

/* Interrupt enable bits */
#define I2C0_INT_ENA_REG            MMIO32(I2C0_BASE + 0x28)
#define I2C1_INT_ENA_REG            MMIO32(I2C1_BASE + 0x28)
// The interrupt enable bit for the I2C_TX_SEND_EMPTY_INT interrupt. (RO)
#define I2C_TX_SEND_EMPTY_INT_ENA       0x1000
// The interrupt enable bit for the I2C_RX_REC_FULL_INT interrupt. (RO)
#define I2C_RX_REC_FULL_INT_ENA         0x0800
// The interrupt enable bit for the I2C_ACK_ERR_INT interrupt. (RO)
#define I2C_ACK_ERR_INT_ENA             0x0400
// The interrupt enable bit for the I2C_TRANS_START_INT interrupt. (RO)
#define I2C_TRANS_START_INT_ENA         0x0200
// The interrupt enable bit for the I2C_TIME_OUT_INT interrupt. (RO)
#define I2C_TIME_OUT_INT_ENA            0x0100
// The interrupt enable bit for the I2C_TRANS_COMPLETE_INT interrupt. (RO)
#define I2C_TRANS_COMPLETE_INT_ENA      0x0080
// The interrupt enable bit for the I2C_MASTER_TRAN_COMP_INT interrupt. (RO)
#define I2C_MASTER_TRAN_COMP_INT_ENA    0x0040
// The interrupt enable bit for the I2C_ARBITRATION_LOST_INT interrupt. (RO)
#define I2C_ARBITRATION_LOST_INT_ENA    0x0020
// The interrupt enable bit for the I2C_END_DETECT_INT interrupt. (RO)
#define I2C_END_DETECT_INT_ENA          0x0008

/* Interrupt status bits */
#define I2C0_INT_STATUS_REG         MMIO32(I2C0_BASE + 0x2c)
#define I2C1_INT_STATUS_REG         MMIO32(I2C1_BASE + 0x2c)
// The masked interrupt status bit for the I2C_TX_SEND_EMPTY_INT interrupt. (RO)
#define I2C_TX_SEND_EMPTY_INT_ST       0x1000
// The masked interrupt status bit for the I2C_RX_REC_FULL_INT interrupt. (RO)
#define I2C_RX_REC_FULL_INT_ST         0x0800
// The masked interrupt status bit for the I2C_ACK_ERR_INT interrupt. (RO)
#define I2C_ACK_ERR_INT_ST             0x0400
// The masked interrupt status bit for the I2C_TRANS_START_INT interrupt. (RO)
#define I2C_TRANS_START_INT_ST         0x0200
// The masked interrupt status bit for the I2C_TIME_OUT_INT interrupt. (RO)
#define I2C_TIME_OUT_INT_ST            0x0100
// The masked interrupt status bit for the I2C_TRANS_COMPLETE_INT interrupt. (RO)
#define I2C_TRANS_COMPLETE_INT_ST      0x0080
// The masked interrupt status bit for the I2C_MASTER_TRAN_COMP_INT interrupt. (RO)
#define I2C_MASTER_TRAN_COMP_INT_ST    0x0040
// The masked interrupt status bit for the I2C_ARBITRATION_LOST_INT interrupt. (RO)
#define I2C_ARBITRATION_LOST_INT_ST    0x0020
// The masked interrupt status bit for the I2C_END_DETECT_INT interrupt. (RO)
#define I2C_END_DETECT_INT_ST          0x0008

/* I2C command register 0 */
#define I2C0_COMD0_REG              MMIO32(I2C0_BASE + 0x58)
#define I2C1_COMD0_REG              MMIO32(I2C1_BASE + 0x58)
#define I2C0_COMD1_REG              MMIO32(I2C0_BASE + 0x5c)
#define I2C1_COMD1_REG              MMIO32(I2C1_BASE + 0x5c)
#define I2C0_COMD2_REG              MMIO32(I2C0_BASE + 0x60)
#define I2C1_COMD2_REG              MMIO32(I2C1_BASE + 0x60)
#define I2C0_COMD3_REG              MMIO32(I2C0_BASE + 0x64)
#define I2C1_COMD3_REG              MMIO32(I2C1_BASE + 0x64)
#define I2C0_COMD4_REG              MMIO32(I2C0_BASE + 0x68)
#define I2C1_COMD4_REG              MMIO32(I2C1_BASE + 0x68)
#define I2C0_COMD5_REG              MMIO32(I2C0_BASE + 0x6c)
#define I2C1_COMD5_REG              MMIO32(I2C1_BASE + 0x6c)
#define I2C0_COMD6_REG              MMIO32(I2C0_BASE + 0x70)
#define I2C1_COMD6_REG              MMIO32(I2C1_BASE + 0x70)
#define I2C0_COMD7_REG              MMIO32(I2C0_BASE + 0x74)
#define I2C1_COMD7_REG              MMIO32(I2C1_BASE + 0x74)
#define I2C0_COMD8_REG              MMIO32(I2C0_BASE + 0x78)
#define I2C1_COMD8_REG              MMIO32(I2C1_BASE + 0x78)
#define I2C0_COMD9_REG              MMIO32(I2C0_BASE + 0x7c)
#define I2C1_COMD9_REG              MMIO32(I2C1_BASE + 0x7c)
#define I2C0_COMD10_REG             MMIO32(I2C0_BASE + 0x80)
#define I2C1_COMD10_REG             MMIO32(I2C1_BASE + 0x80)
#define I2C0_COMD11_REG             MMIO32(I2C0_BASE + 0x84)
#define I2C1_COMD11_REG             MMIO32(I2C1_BASE + 0x84)
#define I2C0_COMD12_REG             MMIO32(I2C0_BASE + 0x88)
#define I2C1_COMD12_REG             MMIO32(I2C1_BASE + 0x88)
#define I2C0_COMD13_REG             MMIO32(I2C0_BASE + 0x8c)
#define I2C1_COMD13_REG             MMIO32(I2C1_BASE + 0x8c)
#define I2C0_COMD14_REG             MMIO32(I2C0_BASE + 0x90)
#define I2C1_COMD14_REG             MMIO32(I2C1_BASE + 0x90)
#define I2C0_COMD15_REG             MMIO32(I2C0_BASE + 0x94)
#define I2C1_COMD15_REG             MMIO32(I2C1_BASE + 0x94)
// When command n is done in I2C Master mode, this bit changes to high level. (R/W)
#define I2C_COMMAND_DONE    0x80000000
// This is the content of command n.
#define I2C_COMMAND_MSK     0x00003fff
#define I2C_COMMAND_SET(n)  (n&I2C_COMMAND_MSK)

#endif
