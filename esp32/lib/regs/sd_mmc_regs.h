#ifndef H_SD_MMC_REGS
#define H_SD_MMC_REGS
/*
 * ESP32 alternative library, SDIO memory cards, MMC cards and
 * devices with a CE-ATA interface registers.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Control register */
#define CTRL_REG        MMIO32(SDMMC_BASE + 0x000)
// after the power-on reset or any other reset to the CE-ATA device.
#define CEATA_DEVICE_INTERRUPT_STATUS   0x800
// Always set send_auto_stop_ccsd and send_ccsd bits together;
#define SEND_AUTO_STOP_CCSD             0x400
// When set, SD/MMC sends CCSD to the CE-ATA device.
#define SEND_CCSD                       0x200
// After a suspend-command is issued during a read-operation
#define ABORT_READ_DATA                 0x100
// Bit automatically clears once response is sent.
#define SEND_IRQ_RESPONSE               0x080
// For sending read-wait to SDIO cards. (R/W)
#define READ_WAIT                       0x040
// Global interrupt enable/disable bit. 0: Disable; 1: Enable. (R/W)
#define INT_ENABLE                      0x010
// To reset DMA interface, firmware should set bit to 1.
#define DMA_RESET                       0x004
// To reset FIFO, firmware should set bit to 1.
#define FIFO_RESET                      0x002
// To reset controller, firmware should set this bit.
#define CONTROLLER_RESET                0x001

/* Clock divider configuration register */
#define CLKDIV_REG      MMIO32(SDMMC_BASE + 0x008)
// Clock divider-3 value.
#define CLK_DIVIDER3_MSK    0xff000000
#define CLK_DIVIDER3_SFT    24
#define CLK_DIVIDER3_SET(n) (((n/2)<<CLK_DIVIDER3_SFT)&CLK_DIVIDER3_MSK)
// Clock divider-3 value.
#define CLK_DIVIDER2_MSK    0x00ff0000
#define CLK_DIVIDER2_SFT    24
#define CLK_DIVIDER2_SET(n) (((n/2)<<CLK_DIVIDER3_SFT)&CLK_DIVIDER3_MSK)
// Clock divider-3 value.
#define CLK_DIVIDER1_MSK    0x0000ff00
#define CLK_DIVIDER1_SFT    24
#define CLK_DIVIDER1_SET(n) (((n/2)<<CLK_DIVIDER3_SFT)&CLK_DIVIDER3_MSK)
// Clock divider-3 value.
#define CLK_DIVIDER0_MSK    0x000000ff
#define CLK_DIVIDER0_SET(n) ((n/2)&CLK_DIVIDER3_MSK)

/* Clock source selection register */
#define CLKSRC_REG      MMIO32(SDMMC_BASE + 0x00c)
// Clock divider source for two SD cards is supported.
#define CLKSRC0_0   0x0
#define CLKSRC0_1   0x1
#define CLKSRC0_2   0x2
#define CLKSRC0_3   0x3
#define CLKSRC1_0   0x0
#define CLKSRC1_1   0x4
#define CLKSRC1_2   0x8
#define CLKSRC1_3   0xc

/* Clock enable register */
#define CLKENA_REG      MMIO32(SDMMC_BASE + 0x010)
// Clock-enable control for two SD card clocks and one MMC card clock is supported.
#define CCLK_ENABLE0    0x1
#define CCLK_ENABLE1    0x2

/* Data and response timeout configuration register */
#define TMOUT_REG       MMIO32(SDMMC_BASE + 0x014)
// Value for card data read timeout.
#define DATA_TIMEOUT_MSK        0xffffff00
#define DATA_TIMEOUT_SFT        8
#define DATA_TIMEOUT_SET(n)     ((n<<DATA_TIMEOUT_SFT)&DATA_TIMEOUT_MSK)
// Response timeout value.
#define RESPONSE_TIMEOUT_MSK    0x000000ff
#define RESPONSE_TIMEOUT_SET(n) (n&RESPONSE_TIMEOUT_MSK)

/* Card bus width configuration register */
#define CTYPE_REG       MMIO32(SDMMC_BASE + 0x018)
// One bit per card indicates if card is in 8-bit mode.
#define CARD_WIDTH8_0   0x10000
#define CARD_WIDTH8_1   0x20000
// One bit per card indicates if card is 1-bit or 4-bit mode.
#define CARD_WIDTH4_0   0x00001
#define CARD_WIDTH4_1   0x00002

/* Card data block size configuration register */
#define BLKSIZ_REG      MMIO32(SDMMC_BASE + 0x01c)
// Block size. (R/W)
#define BLOCK_SIZE_MSK  0xffff

/* Data transfer length configuration register */
#define BYTCNT_REG      MMIO32(SDMMC_BASE + 0x020)
// Num of bytes to be trans, should be an integral mult of Blck Size for blck trans.

/* SDIO interrupt mask register */
#define INTMASK_REG     MMIO32(SDMMC_BASE + 0x024)
// SDIO interrupt mask, one bit for each card.
#define SDIO_INT_MASK0  0x10000
#define SDIO_INT_MASK1  0x20000
// These bits used to mask unwanted interrupts.
#define INT_MASK_EBE        0x08000
#define INT_MASK_ACD        0x04000
#define INT_MASK_SBE_BCI    0x02000
#define INT_MASK_HLE        0x01000
#define INT_MASK_FRUN       0x00800
#define INT_MASK_HTO        0x00400
#define INT_MASK_DRTO       0x00200
#define INT_MASK_RTO        0x00100
#define INT_MASK_DCRC       0x00080
#define INT_MASK_RCRC       0x00040
#define INT_MASK_RXDR       0x00020
#define INT_MASK_TXDR       0x00010
#define INT_MASK_DTO        0x00008
#define INT_MASK_COMD       0x00004
#define INT_MASK_RE         0x00002
#define INT_MASK_CD         0x00001

/* Command argument data register */
#define CMDARG_REG      MMIO32(SDMMC_BASE + 0x028)
// Value indicates command argument to be passed to the card. (R/W)

/* Command and boot configuration register */
#define CMD_REG         MMIO32(SDMMC_BASE + 0x02c)
// Start command. Once command is served by the CIU, this bit is auto cleared.
#define START_CMD                   0x80000000
// Use Hold Register.
#define USE_HOLE                    0x20000000
// Expected Command Completion Signal (CCS) configuration. (R/W)
#define CCS_EXPECTED                0x00800000
// Read access flag. (R/W)
#define READ_CEATA_DEVICE           0x00400000
// Do not send commands, just update clock register value into card clock domain
#define UPDATE_CLOCK_REGISTERS_ONLY 0x00200000
// Card number in use. Represents physical slot number of card being accessed.
#define CARD_NUMBER_MSK             0x001f0000
#define CARD_NUMBER_SFT             16
#define CARD_NUMBER_SET(n)          ((n<<CARD_NUMBER_SFT)&CARD_NUMBER_MSK)
// 1: Send initialization sequence before sending this command.
#define SEND_INITIALIZATION         0x00008000
// 1: Stop or abort command intended to stop current data transfer in progress.
#define STOP_ABORT_CMD              0x00004000
// 1: Wait for previous data transfer to complete before sending Command.
#define WAIT_PRVDATA_COMPLETE       0x00002000
// 1: Send stop command at the end of data transfer.
#define SEND_AUTO_STOP              0x00001000
// 1: Stream data transfer command. Don’t care if no data expected.
#define TRANSFER_MODE               0x00000800
// 0: Read from card; 1: Write to card.
#define READ_WRITE                  0x00000400
// 0: No data transfer expected. 1: Data transfer expected.
#define DATA_EXPECTED               0x00000200
// 0: Do not check; 1: Check response CRC.
#define CHECK_RESPONSE_CRC          0x00000100
// 0: Short response expected from card; 1: Long response expected from card.
#define RESPONSE_LENGTH             0x00000080
// 0: No response expected from card; 1: Response expected from card.
#define RESPONSE_EXPECT             0x00000040
// Command index. (R/W)
#define CMD_INDEX_MSK               0x0000003f
#define CMD_INDEX_SET(n)            (n&CMD_INDEX_MSK)

/* Long response data register */
#define RESP0_REG       MMIO32(SDMMC_BASE + 0x030)
#define RESP1_REG       MMIO32(SDMMC_BASE + 0x034)
#define RESP2_REG       MMIO32(SDMMC_BASE + 0x038)
#define RESP3_REG       MMIO32(SDMMC_BASE + 0x03c)

/* Masked interrupt status register */
#define MINTSTS_REG     MMIO32(SDMMC_BASE + 0x040)
// Interrupt from SDIO card, one bit for each card.
#define SDIO_INTERRUPT_MASK0  0x10000
#define SDIO_INTERRUPT_MASK1  0x20000
// Interrupt enabled only if corresponding bit in interrupt mask reg is set. (RO)
#define INT_STATUS_MSK_EBE        0x08000
#define INT_STATUS_MSK_ACD        0x04000
#define INT_STATUS_MSK_SBE_BCI    0x02000
#define INT_STATUS_MSK_HLE        0x01000
#define INT_STATUS_MSK_FRUN       0x00800
#define INT_STATUS_MSK_HTO        0x00400
#define INT_STATUS_MSK_DRTO       0x00200
#define INT_STATUS_MSK_RTO        0x00100
#define INT_STATUS_MSK_DCRC       0x00080
#define INT_STATUS_MSK_RCRC       0x00040
#define INT_STATUS_MSK_RXDR       0x00020
#define INT_STATUS_MSK_TXDR       0x00010
#define INT_STATUS_MSK_DTO        0x00008
#define INT_STATUS_MSK_COMD       0x00004
#define INT_STATUS_MSK_RE         0x00002
#define INT_STATUS_MSK_CD         0x00001

/* Raw interrupt status register */
#define RINTSTS_REG     MMIO32(SDMMC_BASE + 0x044)
// Interrupt from SDIO card, one bit for each card.
#define SDIO_INTERRUPT_RAW0  0x10000
#define SDIO_INTERRUPT_RAW1  0x20000
// Setting a bit clears the corresponding interrupt and writing 0 has no effect.
#define INT_STATUS_RAW_EBE        0x08000
#define INT_STATUS_RAW_ACD        0x04000
#define INT_STATUS_RAW_SBE_BCI    0x02000
#define INT_STATUS_RAW_HLE        0x01000
#define INT_STATUS_RAWK_FRUN      0x00800
#define INT_STATUS_RAWK_HTO       0x00400
#define INT_STATUS_RAW_DRTO       0x00200
#define INT_STATUS_RAW_RTO        0x00100
#define INT_STATUS_RAW_DCRC       0x00080
#define INT_STATUS_RAW_RCRC       0x00040
#define INT_STATUS_RAW_RXDR       0x00020
#define INT_STATUS_RAW_TXDR       0x00010
#define INT_STATUS_RAW_DTO        0x00008
#define INT_STATUS_RAW_COMD       0x00004
#define INT_STATUS_RAW_RE         0x00002
#define INT_STATUS_RAW_CD         0x00001

/* SD/MMC status register */
#define STATUS_REG      MMIO32(SDMMC_BASE + 0x048)
// FIFO count, number of filled locations in FIFO. (RO)
#define FIFO_COUNT_MSK                  0x3ffe0000
#define FIFO_COUNT_SFT                  17
#define FIFO_COUNT_SET(n)               ((n<<FIFO_COUNT_SFT)&FIFO_COUNT_MSK)
// Index of previous response, including any auto-stop sent by core. (RO)
#define RESPONSE_INDEX_MSK              0x0001f800
#define RESPONSE_INDEX_SFT              11
#define RESPONSE_INDEX_SET(n)           ((n<<RESPONSE_INDEX_SFT)&RESPONSE_INDEX_MSK)
// Data transmit or receive state-machine is busy. (RO)
#define DATA_STATE_MC_BUSY              0x00000400
// Inverted version of raw selected card_data[0]. (RO)
#define DATA_BUSY                       0x00000200
// Raw selected card_data[3], checks whether card is present. (RO)
#define DATA_3_STATUS                   0x00000100
// Command FSM states. (RO)
#define COMMAND_FSM_STATES_IDLE         0x00000010
#define COMMAND_FSM_STATES_SEND_INIT    0x00000020
#define COMMAND_FSM_STATES_SEND_START   0x00000030
#define COMMAND_FSM_STATES_SEND_TX      0x00000040
#define COMMAND_FSM_STATES_SEND_INDEX   0x00000050
#define COMMAND_FSM_STATES_SEND_CRC7    0x00000060
#define COMMAND_FSM_STATES_SEND_END     0x00000070
#define COMMAND_FSM_STATES_RX_IRQ       0x00000080
#define COMMAND_FSM_STATES_RX_TX        0x00000090
#define COMMAND_FSM_STATES_RX_IDX       0x000000a0
#define COMMAND_FSM_STATES_RX_DATA      0x000000b0
#define COMMAND_FSM_STATES_RX_CRC7      0x000000c0
#define COMMAND_FSM_STATES_RX_END       0x000000d0
#define COMMAND_FSM_STATES_RX_NCC       0x000000e0
#define COMMAND_FSM_STATES_WID_RESP     0x000000f0
// FIFO is full status. (RO)
#define FIFO_FULL                       0x00000008
// FIFO is empty status. (RO)
#define FIFO_EMPTY                      0x00000004
// FIFO reached Transmit watermark level, not qualified with data transfer. (RO)
#define FIFO_TX_WATERMARK               0x00000002
// FIFO reached Receive watermark level, not qualified with data transfer. (RO)
#define FIFO_RX_WATERMARK               0x00000001


/* FIFO configuration register */
#define FIFOTH_REG      MMIO32(SDMMC_BASE + 0x04c)
// Burst size of multiple transaction
#define DMA_MULTIPLE_TRANSACTION_SIZE1BYTE      0x00000000
#define DMA_MULTIPLE_TRANSACTION_SIZE4BYTE      0x10000000
#define DMA_MULTIPLE_TRANSACTION_SIZE8BYTE      0x20000000
#define DMA_MULTIPLE_TRANSACTION_SIZE16BYTE     0x30000000
#define DMA_MULTIPLE_TRANSACTION_SIZE32BYTE     0x40000000
#define DMA_MULTIPLE_TRANSACTION_SIZE64BYTE     0x50000000
#define DMA_MULTIPLE_TRANSACTION_SIZE128BYTE    0x60000000
#define DMA_MULTIPLE_TRANSACTION_SIZE256BYTE    0x70000000
// FIFO threshold watermark level when receiving data to card.
#define RX_WMARK_MSK    0x07ff0000
#define RX_WMARK_SFT    16
#define RX_WMARK_SET(n) ((n<<RX_WMARK_SFT)&RX_WMARK_MSK)
// FIFO threshold watermark level when transmitting data to card.
#define TX_WMARK_MSK    0x00000fff
#define TX_WMARK_SET(n) (n&TX_WMARK_MSK)

/* Card detect register */
#define CDETECT_REG     MMIO32(SDMMC_BASE + 0x050)
// Value on card_detect_n input ports (1 bit per card), read-only bits.
#define CARD_DETECT_N0  0x1
#define CARD_DETECT_N1  0x2

/* Card write protection (WP) status register */
#define WRTPRT_REG      MMIO32(SDMMC_BASE + 0x054)
// Value on card_write_prt input ports (1 bit per card).

/* Transferred byte count register */
#define TCBCNT_REG      MMIO32(SDMMC_BASE + 0x05c)
// Number of bytes transferred by CIU unit to card. (RO)

/* Transferred byte count register */
#define TBBCNT_REG      MMIO32(SDMMC_BASE + 0x060)
// Number of bytes transferred between Host/DMA memory and BIU FIFO. (RO)

/* Debounce filter time configuration register */
#define DEBNCE_REG      MMIO32(SDMMC_BASE + 0x064)
// Number of host clocks (clk) used by debounce filter logic.
#define DEBOUNCE_COUNT_MSK  0xffffff

/* User ID (scratchpad) register */
#define USRID_REG       MMIO32(SDMMC_BASE + 0x068)
// User identification register, value set by user.

/* Card reset register */
#define RST_N_REG       MMIO32(SDMMC_BASE + 0x078)
// Hardware reset.1: Active mode; 0: Reset.
#define RST_CARD_RESET0 0x1
#define RST_CARD_RESET1 0x2

/* Burst mode transfer configuration register */
#define BMOD_REG        MMIO32(SDMMC_BASE + 0x080)
// Programmable Burst Length.
#define BMOD_PBL1BYTE   0x000
#define BMOD_PBL4BYTE   0x100
#define BMOD_PBL8BYTE   0x200
#define BMOD_PBL16BYTE  0x300
#define BMOD_PBL32BYTE  0x400
#define BMOD_PBL64BYTE  0x500
#define BMOD_PBL128BYTE 0x600
#define BMOD_PBL256BYTE 0x700
// IDMAC Enable. When set, the IDMAC is enabled. (R/W)
#define BMOD_DE         0x080
// Fixed Burst.
#define BMOD_FB         0x002
// Software Reset. When set, the DMA Controller resets all its internal registers.
#define BMOD_SWR        0x001

/* Poll demand configuration register */
#define PLDMND_REG      MMIO32(SDMMC_BASE + 0x084)
// Poll Demand.

/* Descriptor base address register */
#define DBADDR_REG      MMIO32(SDMMC_BASE + 0x088)
// Start of Descriptor List. Contains the base address of the First Descriptor.

/* IDMAC status register */
#define IDSTS_REG       MMIO32(SDMMC_BASE + 0x08c)
// DMAC FSM present state
#define IDSTS_FSM_DMA_IDLE          0x00000
#define IDSTS_FSM_DMA_SUSPEND       0x02000
#define IDSTS_FSM_DESC_RD           0x04000
#define IDSTS_FSM_DESC_CHK          0x06000
#define IDSTS_FSM_DMA_RD_REQ_WAIT   0x08000
#define IDSTS_FSM_DMA_WR_REQ_WAIT   0x0a000
#define IDSTS_FSM_DMA_RD            0x0c000
#define IDSTS_FSM_DMA_WR            0x0e000
#define IDSTS_FSM_DESC_CLOSE        0x10000
// Fatal Bus Error Code.
#define IDSTS_FBE_CODE_TX_ABORT     0x00400
#define IDSTS_FBE_CODE_RX_ABORT     0x00800
// Abnormal Interrupt Summary.
#define IDSTS_AIS                   0x00200
// Normal Interrupt Summary.
#define IDSTS_NIS                   0x00100
// Card Error Summary.
#define IDSTS_CES                   0x00020
// Descriptor Unavailable Interrupt.
#define IDSTS_DU                    0x00010
// Fatal Bus Error Interrupt. Indicates that a Bus Error occurred (IDSTS[12:10]) .
#define IDSTS_FBE                   0x00004
// Receive Interrupt. Indicates the completion of data reception for a descriptor.
#define IDSTS_RI                    0x00002
// Transmit Interrupt. Indicates that data transmission is finish for a descriptor.
#define IDSTS_TI                    0x00001

/* IDMAC interrupt enable register */
#define IDINTEN_REG     MMIO32(SDMMC_BASE + 0x090)
// Abnormal Interrupt Summary Enable. (R/W)
#define IDINTEN_AI  0x200
// Normal Interrupt Summary Enable. (R/W)
#define IDINTEN_NI  0x100
// Card Error summary Interrupt Enable.
#define IDINTEN_CES 0x020
// Descriptor Unavailable Interrupt.
#define IDINTEN_DU  0x010
// Fatal Bus Error Enable. When set with Abnormal Interrupt Summary Enable
#define IDINTEN_FBE 0x004
// Receive Interrupt Enable.
#define IDINTEN_RI  0x002
// Transmit Interrupt Enable.
#define IDINTEN_TI  0x001

/* Host descriptor address pointer */
#define DSCADDR_REG     MMIO32(SDMMC_BASE + 0x094)
// Host Descriptor Address Ptr, upd by IDMAC during operation and cleared on reset.

/* Host buffer address pointer register */
#define BUFADDR_REG     MMIO32(SDMMC_BASE + 0x098)
// Host Buffer Address Ptr, upd by IDMAC during operation and cleared on reset.

/* Clock phase selection register */
#define CLK_EDGE_SEL    MMIO32(SDMMC_BASE + 0x800)
// This value should be equal to CCLKIN_EDGE_L. (R/W)
#define CCLKIN_EDGE_N_MSK           0x1e0000
#define CCLKIN_EDGE_N_SFT           17
#define CCLKIN_EDGE_N_SET(n)        ((n<<CCLKIN_EDGE_N_SFT)&CCLKIN_EDGE_N_MSK)
// The low level of the divider clock.
#define CCLKIN_EDGE_L_MSK           0x01e000
#define CCLKIN_EDGE_L_SFT           13
#define CCLKIN_EDGE_L_SET(n)        ((n<<CCLKIN_EDGE_L_SFT)&CCLKIN_EDGE_L_MSK)
// The high level of the divider clock.
#define CCLKIN_EDGE_H_MSK           0x001e00
#define CCLKIN_EDGE_H_SFT           9
#define CCLKIN_EDGE_H_SET(n)        ((n<<CCLKIN_EDGE_H_SFT)&CCLKIN_EDGE_H_MSK)
// It is used to select the clk ph of the internal signal from ph90, ph80, or ph270.
#define CCLKIN_EDGE_SLF_SEL_MSK     0x0001c0
#define CCLKIN_EDGE_SLF_SEL_SFT     6
#define CCLKIN_EDGE_SLF_SEL_SET(n) ((n<<CCLKIN_EDGE_SLF_SEL_SFT)&CCLKIN_EDGE_SLF_SEL_MSK)
// It is used to select the clk ph of the input signal from ph90, ph80, or ph270.
#define CCLKIN_EDGE_SAM_SEL_MSK     0x000038
#define CCLKIN_EDGE_SAM_SEL_SFT     3
#define CCLKIN_EDGE_SAM_SEL_SET(n) ((n<<CCLKIN_EDGE_SAM_SEL_SFT)&CCLKIN_EDGE_SAM_SEL_MSK)
// It is used to select the clk ph of the output signal from ph90, ph80, or ph270.
#define CCLKIN_EDGE_DRV_SEL_MSK     0x000007
#define CCLKIN_EDGE_DRV_SEL_SET(n)  (n&CCLKIN_EDGE_DRV_SEL_MSK)

#endif
