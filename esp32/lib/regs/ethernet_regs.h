#ifndef H_ETHERNET
#define H_ETHERNET
/*
 * ESP32 alternative library, Ethernet Media Access Controller (MAC) regs.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Bus mode configuration */
#define DMABUSMODE_REG              MMIO32(EMAC_BASE + 0x0000)
// interface starts all bursts of a length more than 16 with INCR (undefined burst)
#define DMAMIXEDBURST           0x04000000
// AHB interface generates all bursts aligned to the start address LS bits.
#define DMAADDRALIBEA           0x02000000
// multiplies the programmed PBL(PROG_BURST_LEN) value eight times.
#define PBLX8_MODE              0x01000000
// configures the Rx DMA to use the value configured in Bits[22:17] as PBL.
#define USE_SEP_PBL             0x00800000
// maximum number of beats to be transferred in one Rx DMA transaction.
#define RX_DMA_PBL_MSK          0x007e0000
#define RX_DMA_PBL_SFT          17
#define RX_DMA_PBL_SET(n)       ((n<<RX_DMA_PBL_SFT)&RX_DMA_PBL_MSK)
// whether the AHB master interface performs fixed burst transfers or not.
#define FIXED_BURST             0x00010000
// the priority ratio in the weighted round-robin arbitration between Rx and Tx DMA.
#define PRI_RATIO_1_1           0x00000000
#define PRI_RATIO_2_0           0x00004000
#define PRI_RATIO_3_1           0x00008000
#define PRI_RATIO_4_1           0x0000c000
// the maximum number of beats to be transferred in one DMA transaction.
#define PROG_BURST_LEN_MSK      0x00003f00
#define PROG_BURST_LEN_SFT      8
#define PROG_BURST_LEN_SET(n)   ((n<<PROG_BURST_LEN_SFT)&PROG_BURST_LEN_MSK)
// the size of the alternate descriptor increases to 32 bytes. (R/W)
#define ALT_DESC_SIZE           0x00000080
// the number of Word to skip between two unchained descriptors.
#define DESC_SKIP_LEN_MSK       0x0000007c
#define DESC_SKIP_LEN_SFT       2
#define DESC_SKIP_LEN_SET(n)    ((n<<DESC_SKIP_LEN_SFT)&DESC_SKIP_LEN_MSK)
// specifies the arbitration scheme between the transmit and receive paths.
#define DMA_ARB_SCH             0x00000002
// resets the logic and all internal registers of the MAC.
#define SW_RST                  0x00000001

/* Pull demand for data transmit */
#define DMATXPOLLDEMAND_REG         MMIO32(EMAC_BASE + 0x0004)
/* Pull demand for data receive */
#define DMARXPOLLDEMAND_REG         MMIO32(EMAC_BASE + 0x0008)
// the DMA reads the current descriptor to which the Register is pointing.

/* Base address of the first receive descriptor */
#define DMARXBASEADDR_REG           MMIO32(EMAC_BASE + 0x000c)
/* Base address of the first transmit descriptor */
#define DMATXBASEADDR_REG           MMIO32(EMAC_BASE + 0x0010)
// base address of the first descriptor in the Descriptor list.

/* State of interrupts, errors and other events */
#define DMASTATUS_REG               MMIO32(EMAC_BASE + 0x0014)
// event in the Timestamp Generator block of the ETH_MAC.
#define TS_TRI_INT                              0x20000000
// interrupt event in the PMT module of the ETH_MAC.
#define EMAC_PMT_INT                            0x10000000
// type of error that caused a Bus Error
#define ERROR_BITS_RX_WRITE_DATA                0x00000000
#define ERROR_BITS_TX_READ_DATA                 0x01800000
#define ERROR_BITS_RX_DESCR_WRITE               0x02000000
#define ERROR_BITS_TX_DESCR_WRITE               0x02800000
#define ERROR_BITS_RX_DESCR_READ                0x03000000
#define ERROR_BITS_TX_DESCR_READ                0x03800000
// the Transmit DMA FSM state.
#define TRANS_PROC_STATE_STOPPED                0x00000000
#define TRANS_PROC_STATE_FETCHING_DESCR         0x00100000
#define TRANS_PROC_STATE_WAIT_TX                0x00300000
#define TRANS_PROC_STATE_RECEIVE_UNAVAIL_DESC   0x00400000
#define TRANS_PROC_STATE_CLOSE_TX_DESCR         0x00500000
#define TRANS_PROC_STATE_WRITE_TIMESTAMP        0x00600000
#define TRANS_PROC_STATE_TRANSFER_TX            0x00700000
// the Receive DMA FSM state.
#define RECV_PROC_STATE_STOPPED                 0x00000000
#define RECV_PROC_STATE_FETCHING_DESCR          0x00020000
#define RECV_PROC_STATE_WAIT_TX                 0x00060000
#define RECV_PROC_STATE_RECEIVE_UNAVAIL_DESC    0x00080000
#define RECV_PROC_STATE_CLOSE_TX_DESCR          0x000a0000
#define RECV_PROC_STATE_WRITE_TIMESTAMP         0x000c0000
#define RECV_PROC_STATE_TRANSFER_TX             0x000e0000
// Normal Interrupt Summary bit value
#define NORM_INT_SUMM                           0x00010000
// Abnormal Interrupt Summary bit value
#define ABN_INT_SUMM                            0x00008000
// DMA filled the first data buffer of the packet.
#define EARLY_RECV_INT                          0x00004000
// a bus error occurred
#define FATAL_BUS_ERR_INT                       0x00002000
// frame to be transmitted is fully transferred to the MTL Transmit FIFO. (R/SS/WC)
#define EARLY_TRANS_INT                         0x00000400
// Receive Watchdog Timer expired and frame is truncated
#define RECV_WDT_TO                             0x00000200
// Receive Process enters the Stopped state. (R/SS/WC)
#define RECV_PROC_STOP                          0x00000100
// host owns the Next Descriptor in the Receive List and the DMA cannot acquire it.
#define RECV_BUF_UNAVAIL                        0x00000080
// the frame reception is complete.
#define RECV_INT                                0x00000040
// Transmit Buffer had an Underflow during frame transmission.
#define TRANS_UNDFLOW                           0x00000020
// Receive Buffer had an Overflow during frame reception.
#define RECV_OVFLOW                             0x00000010
// Transmit Jabber Timer expired,
#define TRANS_JABBER_TO                         0x00000008
// host owns the Next Descriptor in the Transmit List and the DMA cannot acquire it.
#define TRANS_BUF_UNAVAIL                       0x00000004
// This bit is set when the transmission is stopped. (R/SS/WC)
#define TRANS_PROC_STOP                         0x00000002
// indicates that the frame transmission is complete.
#define TRANS_INT                               0x00000001

/* Receive and Transmit operating modes and command */
#define DMAOPERATION_MODE_REG       MMIO32(EMAC_BASE + 0x0018)
// MAC does not drop the frames which only have errors detected by the Rx crc.
#define DIS_DROP_TCPIP_ERR_FRAM         0x01000000
// MTL reads a frame from the Rx FIFO only after the comp frame has been wr to it.
#define RX_STORE_FORWARD                0x00800000
// Rx DMA does not flush any frame because of the unavailability of rx descr or buf.
#define DIS_FLUSH_RECV_FRAMES           0x00400000
// transmission starts when a full frame resides in the MTL Transmit FIFO.
#define TX_STR_FWD                      0x00200000
// transmit FIFO controller logic is reset to its default values
#define FLUSH_TX_FIFO                   0x00100000
// These bits control the threshold level of the MTL Transmit FIFO.
#define TX_THRESH_CTRL_MSK              0x0001c000
#define TX_THRESH_CTRL_SFT              14
#define TX_THRESH_CTRL_SET(n)           ((n<<TX_THRESH_CTRL_SFT)&TX_THRESH_CTRL_MSK)
// transmission is placed in the Running state, and the DMA checks the Transmit List
#define START_STOP_TRANSMISSION_COMMAND 0x00002000
// Rx FIFO drops frames with error status
#define FWD_ERR_FRAME                   0x00000080
// Rx FIFO forwards Undersized frames
#define FWD_UNDER_GF                    0x00000040
// MAC drops the received giant frames in the Rx FIFO
#define DROP_GFRM                       0x00000020
// control the threshold level of the MTL Receive FIFO.
#define RX_THRESH_CTRL64                0x00000000
#define RX_THRESH_CTRL32                0x00000008
#define RX_THRESH_CTRL96                0x00000010
#define RX_THRESH_CTRL128               0x00000018
// it instructs the DMA to process the second frame of the Transmit data
#define OPT_SECOND_FRAME                0x00000004
// the Receive process is placed in the Running state.
#define START_STOP_RX                   0x00000002

/* Enable / disable interrupts */
#define DMAIN_EN_REG                MMIO32(EMAC_BASE + 0x001c)
// When this bit is set, normal interrupt summary is enabled.
#define DMAIN_NISE  0x10000
// When this bit is set, abnormal interrupt summary is enabled.
#define DMAIN_AISE  0x08000
// with Normal Interrupt Summary Enable, the Early Receive Interrupt is enabled.
#define DMAIN_ERIE  0x04000
// with Abnormal Interrupt Summary Enable, the Fatal Bus Error Interrupt is en.
#define DMAIN_FBEE  0x02000
// with an Abnormal Interrupt Summary Enable, the Early Transmit Int is en.
#define DMAIN_ETIE  0x00400
// with Abnormal Interrupt Summary Enable, the Receive Wdt Tout Int is en.
#define DMAIN_RWTE  0x00200
// with Abnormal Interrupt Summary Enable, the Receive Wtd Tout Int is en.
#define DMAIN_RSE   0x00100
// with Abnormal Interrupt Summary Enable, the Receive Stopped Interrupt is en.
#define DMAIN_RBUE  0x00080
// with Normal Interrupt Summary Enable (Bit[16]), the Receive Interrupt is en.
#define DMAIN_RIE   0x00040
// with Abnormal Interrupt Summary Enable, the Transmit Underflow Int is enabled.
#define DMAIN_UIE   0x00020
// with Abnormal Interrupt Summary Enable, the Receive Overflow Interrupt is en.
#define DMAIN_OIE   0x00010
// with Abnormal Interrupt Summary Enable, the Transmit Jabber Tout Int is en.
#define DMAIN_TJTE  0x00008
// with Normal Interrupt Summary Enable, the Transmit Buffer Unavailable Int is en.
#define DMAIN_TBUE  0x00004
// with Abnormal Interrupt Summary Enable, the Transmission Stopped Interrupt is en
#define DMAIN_TSE   0x00002
// with Normal Interrupt Summary Enable, the Transmit Interrupt is enabled.
#define DMAIN_TIE   0x00001

/* Missed Frame and Buffer Overflow Counter Register */
#define DMAMISSEDFR_REG             MMIO32(EMAC_BASE + 0x0020)
// is set every time the Overflow Frame Counter (Bits[27:17]) overflows
#define OVERFLOW_BFOC   0x20000000
// the number of frames missed by the application.
#define OVERFLOW_FC_MSK 0x1ffc0000
#define OVERFLOW_FC_SFT 17
#define OVERFLOW_FC_GET ((DMAMISSEDFR_REG>>OVERFLOW_FC_SFT)&0x3ff)
// This bit is set every time Missed Frame Counter (Bits[15:0]) overflows
#define OVERFLOW_BMFC   0x00010000
// number of frames missed because of the Host Receive Buffer being unavailable.
#define MISSED_FC_MSK   0x000007ff
#define MISSED_FC_GET   (DMAMISSEDFR_REG&MISSED_FC_MSK)

/* Watchdog timer count on receive */
#define DMARINTWDTIMER_REG          MMIO32(EMAC_BASE + 0x0024)
// number of system clock cycles multiplied by 256 for which the wdt tim is set.
#define RIWTC_MSK   0xff

/* Pointer to current transmit descriptor */
#define DMATXCURRDESC_REG           MMIO32(EMAC_BASE + 0x0048)
/* Pointer to current receive descriptor */
#define DMARXCURRDESC_REG           MMIO32(EMAC_BASE + 0x004c)
/* Pointer to current transmit buffer */
#define DMATXCURRADDR_BUF_REG       MMIO32(EMAC_BASE + 0x0050)
/* Pointer to current receive buffer */
#define DMARXCURRADDR_BUF_REG       MMIO32(EMAC_BASE + 0x0054)
// Cleared on Reset. Pointer updated by the DMA during operation. (RO)

/* MAC configuration */
#define EMACCONFIG_REG              MMIO32(EMAC_BASE + 0x1000)
// source address insertion or replacement for all transmitted frames.
#define SAIRC0                      0x00000000
#define SAIRC2                      0x40000000
#define SAIRC3                      0x60000000
// MAC considers all frames, with up to 2,000 bytes length, as normal packets.
#define ASS2KP                      0x10000000
// MAC disables the watchdog timer on the receiver.
#define EMACWATCHDOG                0x00100000
// MAC disables the jabber timer on the transmitter
#define EMACJABBER                  0x00080000
// MAC allows Jumbo frames of 9,018 bytes (9,022 bytes for VLAN tagged frames)
#define EMACJUMBOFRAME              0x00040000
// minimum IFG between frames during transmission.
#define EMACINTERFRAMEGAP96         0x00000000
#define EMACINTERFRAMEGAP88         0x00010000
#define EMACINTERFRAMEGAP80         0x00020000
#define EMACINTERFRAMEGAP40         0x00030000
// MAC transmitter ignore the MII CRS signal during frame transmission in the half-duplex mode.
#define EMACDISABLECRS              0x00008000
// This bit selects the Ethernet line speed.
#define EMACMII                     0x00004000
// This bit selects the speed in the MII, RMII interface.
#define EMACFESPEED                 0x00002000
// MAC disables the reception of frames when the TX_EN is asserted in the half-duplex mode.
#define EMACRXOWN                   0x00001000
// MAC operates in the loopback mode MII.
#define EMACLOOPBACK                0x00000800
// MAC operates in the full-duplex mode where it can transmit and receive simultaneously.
#define EMACDUPLEX                  0x00000400
// MAC calc the 16-bit one’s complement of the one’s complement sum of all received Eth frame payloads.
#define EMACRXIPCOFFLOAD            0x00000200
// MAC attempts only one transmission.
#define EMACRETRY                   0x00000100
// MAC strips the Pad or FCS field on the incoming frames
#define EMACPADCRCSTRIP             0x00000080
// The Back-Off limit determines the random integer number (r) of slot time delays
#define EMACBACKOFFLIMIT_MIN_N_10   0x00000000
#define EMACBACKOFFLIMIT_MIN_N_8    0x00000020
#define EMACBACKOFFLIMIT_MIN_N_4    0x00000040
#define EMACBACKOFFLIMIT_MIN_N_1    0x00000060
// Deferral Check. (R/W)
#define EMACDEFERRALCHECK           0X00000010
// transmit state machine of the MAC is enabled for transmission on the MII
#define EMACTX                      0x00000008
// the receiver state machine of the MAC is enabled for receiving frames from the MII
#define EMACRX                      0x00000004
// number of preamble bytes that are added to the beginning of every Transmit frame.
#define PLTF7                       0x00000000
#define PLTF5                       0x00000001
#define PLTF3                       0x00000002

/* Frame filter settings */
#define EMACFF_REG                  MMIO32(EMAC_BASE + 0x1004)
// MAC Receiver module passes all received frames
#define RECEIVE_ALL                 0x80000000
// MAC compares the SA field of the received frames with the values programmed in SA registers
#define SAFE                        0x00000200
// Address Check block operates in inverse filtering mode for the SA address comparison.
#define SAIF                        0x00000100
// forwarding of all control frames
#define PCF_FILTER_ALL              0x00000000
#define PCF_FORWARD_ALL_EXC_PAUSE   0x00000040
#define PCF_FORWARD_ALL             0x00000080
#define PCF_CONTROL                 0x000000c0
// AFM(Address Filtering Module) module blocks all incoming broadcast frames.
#define DBF                         0x00000020
// indicates that all received frames with a multicast destination address
#define PAM                         0x00000010
// Address Check block operates in inverse filtering mode for the DA address comparison
#define DAIF                        0x00000008
// Address Filter module passes all incoming frames irrespective of the destination or source address.
#define PMODE                       0x00000001

/* PHY configuration access */
#define EMACGMIIADDR_REG            MMIO32(EMAC_BASE + 0x1010)
// indicates which of the 32 possible PHY devices are being accessed. (R/W)
#define MIIDEV_MSK      0xf800
#define MIIDEV_SFT      11
#define MIIDEV_SET(n)   ((n<<MIIREG_SFT)&MIIREG_MSK)
// selects the desired MII register in the selected PHY device. (R/W)
#define MIIREG_MSK      0x07c0
#define MIIREG_SFT      6
#define MIIREG_SET(n)   ((n<<MIIREG_SFT)&MIIREG_MSK)
// APB clock frequency.
#define MIICSRCLK80M    0x0000
#define MIICSRCLK40M    0x000c
// indicates to the PHY that this is a Write operation using MII_DATA.
#define MIIWRITE        0x0002
// This field is used in combination with MIIREG and MII_DATA.
#define MIIBUSY         0x0001

/* PHY data read write */
#define EMACMIIDATA_REG             MMIO32(EMAC_BASE + 0x1014)
// 16-bit data value read from the PHY after a Management Read operation
#define MII_DATA_MSK    0xffff

/* frame flow control */
#define EMACFC_REG                  MMIO32(EMAC_BASE + 0x1018)
// value to be used in the Pause Time field in the transmit control frame.
#define PAUSE_TIME_MSK      0xffff0000
#define PAUSE_TIME_SFT      16
#define PAUSE_TIME_SET(n)   ((n<<PAUSE_TIME_SFT)&PAUSE_TIME_MSK)
// threshold of the Pause timer automatic retransmission of the Pause frame.
#define PLT_PT4             0x00000000
#define PLT_PT28            0x00000010
#define PLT_PT144           0x00000020
#define PLT_PT256           0x00000030
// A pause frame is processed when it has the unique multicast address specified in the IEEE Std 802.3.
#define UPFD                0x00000008
// MAC decodes the received Pause frame and disables its transmitter for a specified (Pause) time.
#define RFCE                0x00000004
// MAC enables the flow control operation to transmit Pause frames.
#define TFCE                0x00000002
// initiates a Pause frame in the full-duplex mode and activates the backpressure function
#define FCBBA               0x00000001

/* Status debugging bits */
#define EMACDEBUG_REG               MMIO32(EMAC_BASE + 0x1024)
// this bit indicates that the MTL TxStatus FIFO is full.
#define MTLTSFFS                0x01000000
// bit indicates that the MTL Tx FIFO is not empty and some data is left for transmission. (RO)
#define MTLTFNES                0x00800000
// MTL Tx FIFO Write Controller is active and is transferring data to the Tx FIFO. (RO)
#define MTLTFWCS                0x00400000
// This field indicates the state of the Tx FIFO Read Controller: (RO)
#define MTLTFRCS_IDLE           0x00000000
#define MTLTFRCS_READ           0x00100000
#define MTLTFRCS_WAIT_TX_MAC    0x00200000
#define MTLTFRCS_WRITE_TX       0x00300000
// indicates that the MAC transmitter is in the Pause condition
#define MACTP                   0x00080000
// indicates the state of the MAC Transmit Frame Controller module: (RO)
#define MACTFCS_IDLE            0x00000000
#define MACTFCS_WAIT_STATUS     0x00020000
#define MACTFCS_TX_PAUSE        0x00040000
#define MACTFCS_TRANS_INPUT     0x00060000
// MAC MII transmit protocol engine is actively transmitting data and is not in the IDLE state. (RO)
#define MACTPES                 0x00010000
// This field gives the status of the fill-level of the Rx FIFO: (RO)
#define MTLRFFLS_EMPTY          0x00000000
#define MTLRFFLS_BELOW_TH       0x00000100
#define MTLRFFLS_ABOVE_TH       0x00000200
#define MTLRFFLS_FULL           0x00000300
// This field gives the state of the Rx FIFO read Controller: (RO)
#define MTLRFRCS_IDLE           0x00000000
#define MTLRFRCS_READ_DATA      0x00000020
#define MTLRFRCS_READ_STATUS    0x00000040
#define MTLRFRCS_FLUSH          0x00000060
// MTL Rx FIFO Write Controller is active and is transferring a received frame to the FIFO. (RO)
#define MTLRFWCAS               0x00000010
// active state of the FIFO Read and Write controllers of the MAC Receive Frame Controller Module.
#define MACRFFCS_READ           0x00000004
#define MACRFFCS_WRITE          0x00000002
// MAC MII receive protocol engine is actively receiving data and not in IDLE state. (RO)
#define MACRPES                 0x00000001

/* Remote Wake-Up Frame Filter */
#define PMT_RWUFFR_REG              MMIO32(EMAC_BASE + 0x1028)
// Bit j[30:0] is the byte mask.

/* PMT Control and Status */
#define PMT_CSR_REG                 MMIO32(EMAC_BASE + 0x102c)
// resets the remote RWKPTR register to 3’b000. (R/WS/SC)
#define RWKFILTRST      0x80000000
// please refer to PMT_RWUFFR.
#define RWKPTR_MSK      0x1f000000
#define RWKPTR_SFT      24
#define RWKPTR_SET(n)   ((n<<RWKPTR_SFT)&RWKPTR_MSK)
// enables any unicast packet filtered by the MAC (DAFilter) address recognition
#define GLBLUCAST       0x00000200
// power management event is generated because of the reception of a remote wake-up frame.
#define RWKPRCVD        0x00000040
// power management event is generated because of the reception of a magic packet.
#define MGKPRCVD        0x00000020
// enables generation of a power management event because of remote wake-up frame reception. (R/W)
#define RWKPKTEN        0x00000004
// enables generation of a power management event because of magic packet reception. (R/W)
#define MGKPKTEN        0x00000002
// MAC receiver drops all rxd frames until it rx the expected magic packet or remote wake-up frame.
#define PWRDWN          0x00000001

/* LPI Control and Status */
#define EMACLPI_CSR_REG             MMIO32(EMAC_BASE + 0x1030)
// behavior of the MAC when it is entering or coming out of the LPI mode on the transmit side.
#define LPITXA  0x80000
// This bit indicates the link status of the PHY.
#define PLS     0x20000
// instructs the MAC Transmitter to enter the LPI state.
#define LPIEN   0x10000
// indicates that the MAC is receiving the LPI pattern on the MII interface.
#define RLPIST  0x00200
// indicates that the MAC is receiving the LPI pattern on the MII interface.
#define TLPIST  0x00100
// ndicates that the MAC Receiver has stopped receiving the LPI pattern on the MII interface,
#define RLPIEX  0x00008
// ndicates that the MAC Receiver has received an LPI pattern and entered the LPI state.
#define RLPIEN  0x00004
// MAC transmitter has exited the LPI state after the user has cleared the LPIEN bit
#define TLPIEX  0x00002
// MAC Transmitter has entered the LPI state because of the setting of the LPIEN bit.
#define TLPIEN  0x00001

/* LPI Timers Control */
#define EMACLPITIMERSCONTROL_REG    MMIO32(EMAC_BASE + 0x1034)
// minimum time (in milliseconds) for which the link status from the PHY should be up (OKAY)
#define LPI_LS_TIMER_MSK    0x03ff0000
#define LPI_LS_TIMER_SFT    16
#define LPI_LS_TIMER_SET(n) ((n<<LPI_LS_TIMER_SFT)&LPI_LS_TIMER_MSK)
// time (in microseconds) for which the MAC waits after it stops transmitting the LPI pattern to the PHY
#define LPI_TW_TIMER_MSK    0x0000ffff
#define LPI_TW_TIMER_SET(n) (n&LPI_TW_TIMER_MSK)

/* Interrupt status */
#define EMACINTS_REG                MMIO32(EMAC_BASE + 0x1038)
// set for any LPI state entry or exit in the MAC Transmitter or Receiver.
#define LPIINTS 0x400
// magic packet or remote wake-up frame is received in the power-down mode
#define PMTINTS 0x008

/* Interrupt mask */
#define EMACINTMASK_REG             MMIO32(EMAC_BASE + 0x103c)
// disables the assertion of the interrupt signal because of the setting of the LPI Interrupt Status
#define LPIINTMASK 0x400
// disables the assertion of the interrupt signal because of the setting of PMT Interrupt Status
#define PMTINTMASK 0x008

/* 6-byte MAC address */
#define EMACADDR0HIGH_REG           MMIO32(EMAC_BASE + 0x1040)
#define EMACADDR1HIGH_REG           MMIO32(EMAC_BASE + 0x1048)
#define EMACADDR2HIGH_REG           MMIO32(EMAC_BASE + 0x1050)
#define EMACADDR3HIGH_REG           MMIO32(EMAC_BASE + 0x1058)
#define EMACADDR4HIGH_REG           MMIO32(EMAC_BASE + 0x1060)
#define EMACADDR5HIGH_REG           MMIO32(EMAC_BASE + 0x1068)
#define EMACADDR6HIGH_REG           MMIO32(EMAC_BASE + 0x1070)
#define EMACADDR7HIGH_REG           MMIO32(EMAC_BASE + 0x1078)
// address filter module uses the eighth MAC address for perfect filtering.
#define ADDRESS_ENABLE              0x80000000
// EMACADDR7[47:0] is used to compare with the SA fields of the received frame.
#define SOURCE_ADDRESS              0x40000000
// These bits are mask control bits for comparison of each of the EMACADDRn bytes.
#define MASK_BYTE_CONTROL_H15_8     0x20000000
#define MASK_BYTE_CONTROL_H7_0      0x20000000
#define MASK_BYTE_CONTROL_L31_24    0x20000000
#define MASK_BYTE_CONTROL_L7_0      0x20000000
// This field contains the upper 16 bits
#define MAC_ADDRESS_HI_MSK          0x0000ffff
#define EMACADDR0LOW_REG            MMIO32(EMAC_BASE + 0x1044)
#define EMACADDR1LOW_REG            MMIO32(EMAC_BASE + 0x104c)
#define EMACADDR2LOW_REG            MMIO32(EMAC_BASE + 0x1054)
#define EMACADDR3LOW_REG            MMIO32(EMAC_BASE + 0x105c)
#define EMACADDR4LOW_REG            MMIO32(EMAC_BASE + 0x1064)
#define EMACADDR5LOW_REG            MMIO32(EMAC_BASE + 0x106c)
#define EMACADDR6LOW_REG            MMIO32(EMAC_BASE + 0x1074)
#define EMACADDR7LOW_REG            MMIO32(EMAC_BASE + 0x107c)
// this field contains the lower 32 bits of the eighth 6-byte MAC address.

/* Link communication status */
#define EMACCSTATUS_REG             MMIO32(EMAC_BASE + 0x10d8)
// whether there is jabber timeout error (1’b1) in the received frame. (RO)
#define JABBER_TIMEOUT  0x10
// This bit indicates the current speed of the link: (RO)
#define LINK_SPEED2P5M  0x00
#define LINK_SPEED25M   0x02
#define LINK_SPEED125M  0x04
// This bit indicates the current mode of operation of the link: (RO)
#define LINK_MODE       0x01

/* Watchdog timeout control */
#define EMACWDOGTO_REG              MMIO32(EMAC_BASE + 0x10dc)
// When this bit is set and Bit[23] (WD) of EMACCONFIG_REG is reset (Bits[13:0]) is used
#define PWDOGEN     0x10000
// used as watchdog timeout for a received frame.
#define WDOGTO_MSK  0x0ffff

/* RMII clock divider setting */
#define EMAC_EX_CLKOUT_CONF_REG     MMIO32(EMAC_BASE + 0x1800)
// RMII CLK using internal APLL CLK, the half divider number, when using RMII PHY. (R/W)
#define EMAC_CLK_OUT_H_DIV_NUM_MSK      0xf0
#define EMAC_CLK_OUT_H_DIV_NUM_SFT      4
#define EMAC_CLK_OUT_H_DIV_NUM_SET(n)   ((n<<EMAC_CLK_OUT_H_DIV_NUM_SFT)&EMAC_CLK_OUT_H_DIV_NUM_MSK)
// RMII CLK using internal APLL CLK, the whole divider number, when using RMII PHY. (R/W)
#define EMAC_CLK_OUT_DIV_NUM_MSK        0x0f
#define EMAC_CLK_OUT_DIV_NUM_SET(n)     (n&EMAC_CLK_OUT_DIV_NUM_MSK)

/* RMII clock half and whole divider settings */
#define EMAC_EX_OSCCLK_CONF_REG     MMIO32(EMAC_BASE + 0x1804)
// Ethernet work using external PHY output clock or not for RMII CLK
#define EMAC_OSC_CLK_SEL                0x01000000
// RMII/MII half-integer divider, 100M
#define EMAC_OSC_H_DIV_NUM_100M_MSK     0x00fc0000
#define EMAC_OSC_H_DIV_NUM_100M_SFT     18
#define EMAC_OSC_H_DIV_NUM_100M_SET(n)  ((n<<EMAC_OSC_H_DIV_NUM_100M_SFT)&EMAC_OSC_H_DIV_NUM_100M_MSK)
// RMII/MII whole-integer divider, 100M
#define EMAC_OSC_DIV_NUM_100M_MSK       0x0003f000
#define EMAC_OSC_DIV_NUM_100M_SFT       12
#define EMAC_OSC_DIV_NUM_100M_SET(n)    ((n<<EMAC_OSC_DIV_NUM_100M_SFT)&EMAC_OSC_DIV_NUM_100M_MSK)
// RMII/MII half-integer divider, 10M
#define EMAC_OSC_H_DIV_NUM_10M_MSK      0x00000fc0
#define EMAC_OSC_H_DIV_NUM_10M_SFT      6
#define EMAC_OSC_H_DIV_NUM_10M_SET(n)   ((n<<EMAC_OSC_H_DIV_NUM_10M_SFT)&EMAC_OSC_H_DIV_NUM_10M_MSK)
// RMII/MII whole-integer divider, 10M
#define EMAC_OSC_DIV_NUM_10M_MSK        0x0000001f
#define EMAC_OSC_DIV_NUM_10M_SET(n)     (n&EMAC_OSC_DIV_NUM_10M_MSK)

/* Clock enable and external / internal clock selection */
#define EMAC_EX_CLK_CTRL_REG        MMIO32(EMAC_BASE + 0x1808)
// Enable Ethernet RX CLK. (R/W)
#define EMAC_MII_CLK_RX_EN  0x10
// Enable Ethernet TX CLK. (R/W)
#define EMAC_MII_CLK_TX_EN  0x08
// Using internal APLL CLK in RMII PHY mode. (R/W)
#define EMAC_INT_OSC_EN     0x02
// Using external APLL CLK in RMII PHY mode. (R/W)
#define EMAC_EXT_OSC_EN     0x01

/* Selection of MII / RMII phy */
#define EMAC_EX_PHYINF_CONF_REG     MMIO32(EMAC_BASE + 0x180c)
// The PHY interface selected. 0x0: PHY MII, 0x4: PHY RMII. (R/W)
#define EMAC_PHY_INTF_SEL_MII   0x0000
#define EMAC_PHY_INTF_SEL_RMII  0x8000

/* Ethernet RAM power-down enable */
#define EMAC_PD_SEL_REG             MMIO32(EMAC_BASE + 0x1810)
// Ethernet RAM power-down enable signal.
#define EMAC_RAM_PD_EN_TX   0x1
#define EMAC_RAM_PD_EN_RX   0x2

#endif
