#ifndef H_PID_REGS
#define H_PID_REGS
/*
 * ESP32 alternative library, Process ID Controller (PID) registers.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* PID interrupt identification enable */
#define PIDCTRL_INTERRUPT_ENABLE_REG    MMIO32(PID_CONTROLLER + 0x00)
// These bits are used to enable interrupt identification and processing. (R/W)
#define PIDCTRL_INTERRUPT_ENABLE7   0x80
#define PIDCTRL_INTERRUPT_ENABLE6   0x40
#define PIDCTRL_INTERRUPT_ENABLE5   0x20
#define PIDCTRL_INTERRUPT_ENABLE4   0x10
#define PIDCTRL_INTERRUPT_ENABLE3   0x08
#define PIDCTRL_INTERRUPT_ENABLE2   0x04
#define PIDCTRL_INTERRUPT_ENABLE1   0x02

/* Level n interrupt vector address */
#define PIDCTRL_INTERRUPT_ADDR_1_REG    MMIO32(PID_CONTROLLER + 0x04)
#define PIDCTRL_INTERRUPT_ADDR_2_REG    MMIO32(PID_CONTROLLER + 0x08)
#define PIDCTRL_INTERRUPT_ADDR_3_REG    MMIO32(PID_CONTROLLER + 0x0c)
#define PIDCTRL_INTERRUPT_ADDR_4_REG    MMIO32(PID_CONTROLLER + 0x10)
#define PIDCTRL_INTERRUPT_ADDR_5_REG    MMIO32(PID_CONTROLLER + 0x14)
#define PIDCTRL_INTERRUPT_ADDR_6_REG    MMIO32(PID_CONTROLLER + 0x18)
// Level n interrupt vector entry address. (R/W)
#define PIDCTRL_INTERRUPT_ADDR_7_REG    MMIO32(PID_CONTROLLER + 0x1c)
//NMI interrupt vector entry address. (R/W)

/* New PID valid delay */
#define PIDCTRL_PID_DELAY_REG           MMIO32(PID_CONTROLLER + 0x20)
// Delay until newly assigned PID is valid. (R/W)
#define PIDCTRL_PID_DELAY_MSK   0xfff

/* NMI mask signal disable delay */
#define PIDCTRL_NMI_DELAY_REG           MMIO32(PID_CONTROLLER + 0x24)
// Delay for disabling CPU NMI interrupt mask signal. (R/W)
#define PIDCTRL_NMI_DELAY_MSK   0xfff

/* Current interrupt priority */
#define PIDCTRL_LEVEL_REG               MMIO32(PID_CONTROLLER + 0x28)
// The current status of the system. (R/W)
#define PIDCTRL_CURRENT_STATUS  0xf

/* System status before Level n interrupt */
#define PIDCTRL_FROM_1_REG              MMIO32(PID_CONTROLLER + 0x2c)
#define PIDCTRL_FROM_2_REG              MMIO32(PID_CONTROLLER + 0x30)
#define PIDCTRL_FROM_3_REG              MMIO32(PID_CONTROLLER + 0x34)
#define PIDCTRL_FROM_4_REG              MMIO32(PID_CONTROLLER + 0x38)
#define PIDCTRL_FROM_5_REG              MMIO32(PID_CONTROLLER + 0x3c)
#define PIDCTRL_FROM_6_REG              MMIO32(PID_CONTROLLER + 0x40)
#define PIDCTRL_FROM_7_REG              MMIO32(PID_CONTROLLER + 0x44)
// System status before any of Level 1 to Level 6, NMI interrupts occurs. (R/W)
#define PIDCTRL_PREVIOUS_STATUS_MSK 0x7f

/* New PID configuration register */
#define PIDCTRL_PID_NEW_REG             MMIO32(PID_CONTROLLER + 0x48)
// New PID. (R/W)
#define PIDCTRL_PID_NEW_MSK 0x7

/* New PID confirmation register */
#define PIDCTRL_PID_CONFIRM_REG         MMIO32(PID_CONTROLLER + 0x4c)
// This bit is used to confirm the switch of PID. (WO)
#define PIDCTRL_PID_CONFIRM 0x1

/* NMI mask enable register */
#define PIDCTRL_NMI_MASK_ENABLE_REG     MMIO32(PID_CONTROLLER + 0x54)
// This bit is used to enable CPU NMI interrupt mask signal. (WO)
#define PIDCTRL_NMI_MASK_ENABLE 0x1

/* NMI mask disable register */
#define PIDCTRL_NMI_MASK_DISABLE_REG    MMIO32(PID_CONTROLLER + 0x58)
// This bit is used to disable CPU NMI interrupt mask signal. (WO)
#define PIDCTRL_NMI_MASK_DISABLE    0x1

#endif
