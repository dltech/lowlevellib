#ifndef H_RMT_REGS
#define H_RMT_REGS
/*
 * ESP32 alternative library, Remote Control Peripheral registers.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Channel n config register 0 */
#define RMT_CH0CONF0_REG        MMIO32(RMT_BASE + 0x20)
#define RMT_CH1CONF0_REG        MMIO32(RMT_BASE + 0x28)
#define RMT_CH2CONF0_REG        MMIO32(RMT_BASE + 0x30)
#define RMT_CH3CONF0_REG        MMIO32(RMT_BASE + 0x38)
#define RMT_CH4CONF0_REG        MMIO32(RMT_BASE + 0x40)
#define RMT_CH5CONF0_REG        MMIO32(RMT_BASE + 0x48)
#define RMT_CH6CONF0_REG        MMIO32(RMT_BASE + 0x50)
#define RMT_CH7CONF0_REG        MMIO32(RMT_BASE + 0x58)
// This bit is used to power down the entire RMT RAM block.
#define RMT_MEM_PD                  0x40000000
// This bit is used for configuration when the carrier wave is being transmitted.
#define RMT_CARRIER_OUT_LV_CH       0x20000000
// This is the carrier modulation enable-control bit for channel n.
#define RMT_CARRIER_EN_CH           0x10000000
// configure the amount of memory blocks allocated to channel n. (R/W)
#define RMT_MEM_SIZE_CH_MSK         0x0f000000
#define RMT_MEM_SIZE_CH_SFT         24
#define RMT_MEM_SIZE_CH_SET(n)      ((n<<RMT_MEM_SIZE_CH_SFT)&RMT_MEM_SIZE_CH_MSK)
// In rx, when no edge on the input for longer REG_IDLE_THRES_CHn, the rx is finish.
#define RMT_IDLE_THRES_CH_MSK       0x00ffff00
#define RMT_IDLE_THRES_CH_SFT       8
#define RMT_IDLE_THRES_CH_SET(n) ((n<<RMT_IDLE_THRES_CH_SFT)&RMT_IDLE_THRES_CH_MSK)
// This register is used to set the divider for the channel clock of channel n.
#define RMT_DIV_CNT_CH_MSK          0x000000ff
#define RMT_DIV_CNT_CH_SET(n)       (n&RMT_DIV_CNT_CH_MSK)

/* Channel 0 config register 1 */
#define RMT_CH0CONF1_REG        MMIO32(RMT_BASE + 0x24)
#define RMT_CH1CONF1_REG        MMIO32(RMT_BASE + 0x2c)
#define RMT_CH2CONF1_REG        MMIO32(RMT_BASE + 0x34)
#define RMT_CH3CONF1_REG        MMIO32(RMT_BASE + 0x3c)
#define RMT_CH4CONF1_REG        MMIO32(RMT_BASE + 0x44)
#define RMT_CH5CONF1_REG        MMIO32(RMT_BASE + 0x4c)
#define RMT_CH6CONF1_REG        MMIO32(RMT_BASE + 0x54)
#define RMT_CH7CONF1_REG        MMIO32(RMT_BASE + 0x5c)
// This is the output enable-control bit for channel n in IDLE state. (R/W)
#define RMT_IDLE_OUT_EN_CH              0x80000
// level of output signals in channel n when the latter is in IDLE state. (R/W)
#define RMT_IDLE_OUT_LV_CH              0x40000
// This bit is used to select the channel’s base clock.
#define RMT_REF_ALWAYS_ON_CH            0x20000
// Setting this bit resets the clock divider of channel n. (R/W)
#define RMT_REF_CNT_RST_CH              0x10000
// ch n ign in pulse when the pulse width is sm than this val in APB clock periods.
#define RMT_RX_FILTER_THRES_CH_MSK      0x0ff00
#define RMT_RX_FILTER_THRES_CH_SFT      8
#define RMT_RX_FILTER_THRES_CH_SET(n) ((n<<RMT_RX_FILTER_THRES_CH_SFT)&RMT_RX_FILTER_THRES_CH_MSK)
// This is the receive filter’s enable-bit for channel n. (R/W)
#define RMT_RX_FILTER_EN_CH             0x00080
// instead of going to an idle state when transmission ends, the tx will restart
#define RMT_TX_CONTI_MODE_CH            0x00040
// This bit marks channel n’s RAM block ownership.
#define RMT_MEM_OWNER_CH                0x00020
// reset the read-RAM address for channel n by accessing the transmitter. (R/W)
#define RMT_MEM_RD_RST_CH               0x00008
// reset the write-RAM address for channel n by accessing the receiver. (R/W)
#define RMT_MEM_WR_RST_CH               0x00004
// Set this bit to enable receiving data on channel n. (R/W)
#define RMT_RX_EN_CH                    0x00002
// Set this bit to start sending data on channel n. (R/W)
#define RMT_TX_START_CH                 0x00001

/* Raw interrupt status */
#define RMT_INT_RAW_REG         MMIO32(RMT_BASE + 0xa0)
// The raw interrupt status bit for the RMT_CHn_TX_THR_EVENT_INT interrupt. (RO)
#define RMT_CH7_TX_THR_EVENT_INT_RAW    0x80000000
#define RMT_CH6_TX_THR_EVENT_INT_RAW    0x40000000
#define RMT_CH5_TX_THR_EVENT_INT_RAW    0x20000000
#define RMT_CH4_TX_THR_EVENT_INT_RAW    0x10000000
#define RMT_CH3_TX_THR_EVENT_INT_RAW    0x08000000
#define RMT_CH2_TX_THR_EVENT_INT_RAW    0x04000000
#define RMT_CH1_TX_THR_EVENT_INT_RAW    0x02000000
#define RMT_CH0_TX_THR_EVENT_INT_RAW    0x01000000
// The raw interrupt status bit for the RMT_CHn_ERR_INT interrupt. (RO)
#define RMT_CH7_ERR_INT_RAW             0x00800000
#define RMT_CH6_ERR_INT_RAW             0x00400000
#define RMT_CH5_ERR_INT_RAW             0x00200000
#define RMT_CH4_ERR_INT_RAW             0x00100000
#define RMT_CH3_ERR_INT_RAW             0x00080000
#define RMT_CH2_ERR_INT_RAW             0x00040000
#define RMT_CH1_ERR_INT_RAW             0x00020000
#define RMT_CH0_ERR_INT_RAW             0x00010000
// The raw interrupt status bit for the RMT_CHn_RX_END_INT interrupt. (RO)
#define RMT_CH7_RX_END_INT_RAW          0x00008000
#define RMT_CH6_RX_END_INT_RAW          0x00004000
#define RMT_CH5_RX_END_INT_RAW          0x00002000
#define RMT_CH4_RX_END_INT_RAW          0x00001000
#define RMT_CH3_RX_END_INT_RAW          0x00000800
#define RMT_CH2_RX_END_INT_RAW          0x00000400
#define RMT_CH1_RX_END_INT_RAW          0x00000200
#define RMT_CH0_RX_END_INT_RAW          0x00000100
// The raw interrupt status bit for the RMT_CHn_TX_END_INT interrupt. (RO)
#define RMT_CH7_TX_END_INT_RAW          0x00000080
#define RMT_CH6_TX_END_INT_RAW          0x00000040
#define RMT_CH5_TX_END_INT_RAW          0x00000020
#define RMT_CH4_TX_END_INT_RAW          0x00000010
#define RMT_CH3_TX_END_INT_RAW          0x00000008
#define RMT_CH2_TX_END_INT_RAW          0x00000004
#define RMT_CH1_TX_END_INT_RAW          0x00000002
#define RMT_CH0_TX_END_INT_RAW          0x00000001

/* Masked interrupt status */
#define RMT_INT_ST_REG          MMIO32(RMT_BASE + 0xa4)
// The masked interrupt status bit for the RMT_CHn_TX_THR_EVENT_INT interrupt. (RO)
#define RMT_CH7_TX_THR_EVENT_INT_ST    0x80000000
#define RMT_CH6_TX_THR_EVENT_INT_ST    0x40000000
#define RMT_CH5_TX_THR_EVENT_INT_ST    0x20000000
#define RMT_CH4_TX_THR_EVENT_INT_ST    0x10000000
#define RMT_CH3_TX_THR_EVENT_INT_ST    0x08000000
#define RMT_CH2_TX_THR_EVENT_INT_ST    0x04000000
#define RMT_CH1_TX_THR_EVENT_INT_ST    0x02000000
#define RMT_CH0_TX_THR_EVENT_INT_ST    0x01000000
// The masked interrupt status bit for the RMT_CHn_ERR_INT interrupt. (RO)
#define RMT_CH7_ERR_INT_ST             0x00800000
#define RMT_CH6_ERR_INT_ST             0x00400000
#define RMT_CH5_ERR_INT_ST             0x00200000
#define RMT_CH4_ERR_INT_ST             0x00100000
#define RMT_CH3_ERR_INT_ST             0x00080000
#define RMT_CH2_ERR_INT_ST             0x00040000
#define RMT_CH1_ERR_INT_ST             0x00020000
#define RMT_CH0_ERR_INT_ST             0x00010000
// The masked interrupt status bit for the RMT_CHn_RX_END_INT interrupt. (RO)
#define RMT_CH7_RX_END_INT_ST          0x00008000
#define RMT_CH6_RX_END_INT_ST          0x00004000
#define RMT_CH5_RX_END_INT_ST          0x00002000
#define RMT_CH4_RX_END_INT_ST          0x00001000
#define RMT_CH3_RX_END_INT_ST          0x00000800
#define RMT_CH2_RX_END_INT_ST          0x00000400
#define RMT_CH1_RX_END_INT_ST          0x00000200
#define RMT_CH0_RX_END_INT_ST          0x00000100
// The masked interrupt status bit for the RMT_CHn_TX_END_INT interrupt. (RO)
#define RMT_CH7_TX_END_INT_ST          0x00000080
#define RMT_CH6_TX_END_INT_ST          0x00000040
#define RMT_CH5_TX_END_INT_ST          0x00000020
#define RMT_CH4_TX_END_INT_ST          0x00000010
#define RMT_CH3_TX_END_INT_ST          0x00000008
#define RMT_CH2_TX_END_INT_ST          0x00000004
#define RMT_CH1_TX_END_INT_ST          0x00000002
#define RMT_CH0_TX_END_INT_ST          0x00000001

/* Interrupt enable bits */
#define RMT_INT_ENA_REG         MMIO32(RMT_BASE + 0xa8)
// The interrupt enable bit for the RMT_CHn_TX_THR_EVENT_INT interrupt. (RO)
#define RMT_CH7_TX_THR_EVENT_INT_ENA    0x80000000
#define RMT_CH6_TX_THR_EVENT_INT_ENA    0x40000000
#define RMT_CH5_TX_THR_EVENT_INT_ENA    0x20000000
#define RMT_CH4_TX_THR_EVENT_INT_ENA    0x10000000
#define RMT_CH3_TX_THR_EVENT_INT_ENA    0x08000000
#define RMT_CH2_TX_THR_EVENT_INT_ENA    0x04000000
#define RMT_CH1_TX_THR_EVENT_INT_ENA    0x02000000
#define RMT_CH0_TX_THR_EVENT_INT_ENA    0x01000000
// The interrupt enable bit for the RMT_CHn_ERR_INT interrupt. (RO)
#define RMT_CH7_ERR_INT_ENA             0x00800000
#define RMT_CH6_ERR_INT_ENA             0x00400000
#define RMT_CH5_ERR_INT_ENA             0x00200000
#define RMT_CH4_ERR_INT_ENA             0x00100000
#define RMT_CH3_ERR_INT_ENA             0x00080000
#define RMT_CH2_ERR_INT_ENA             0x00040000
#define RMT_CH1_ERR_INT_ENA             0x00020000
#define RMT_CH0_ERR_INT_ENA             0x00010000
// The interrupt enable bit for the RMT_CHn_RX_END_INT interrupt. (RO)
#define RMT_CH7_RX_END_INT_ENA          0x00008000
#define RMT_CH6_RX_END_INT_ENA          0x00004000
#define RMT_CH5_RX_END_INT_ENA          0x00002000
#define RMT_CH4_RX_END_INT_ENA          0x00001000
#define RMT_CH3_RX_END_INT_ENA          0x00000800
#define RMT_CH2_RX_END_INT_ENA          0x00000400
#define RMT_CH1_RX_END_INT_ENA          0x00000200
#define RMT_CH0_RX_END_INT_ENA          0x00000100
// The interrupt enable bit for the RMT_CHn_TX_END_INT interrupt. (RO)
#define RMT_CH7_TX_END_INT_ENA          0x00000080
#define RMT_CH6_TX_END_INT_ENA          0x00000040
#define RMT_CH5_TX_END_INT_ENA          0x00000020
#define RMT_CH4_TX_END_INT_ENA          0x00000010
#define RMT_CH3_TX_END_INT_ENA          0x00000008
#define RMT_CH2_TX_END_INT_ENA          0x00000004
#define RMT_CH1_TX_END_INT_ENA          0x00000002
#define RMT_CH0_TX_END_INT_ENA          0x00000001

/* Interrupt clear bits */
#define RMT_INT_CLR_REG         MMIO32(RMT_BASE + 0xac)
// Set this bit to clear the RMT_CHn_TX_THR_EVENT_INT interrupt. (RO)
#define RMT_CH7_TX_THR_EVENT_INT_CLR    0x80000000
#define RMT_CH6_TX_THR_EVENT_INT_CLR    0x40000000
#define RMT_CH5_TX_THR_EVENT_INT_CLR    0x20000000
#define RMT_CH4_TX_THR_EVENT_INT_CLR    0x10000000
#define RMT_CH3_TX_THR_EVENT_INT_CLR    0x08000000
#define RMT_CH2_TX_THR_EVENT_INT_CLR    0x04000000
#define RMT_CH1_TX_THR_EVENT_INT_CLR    0x02000000
#define RMT_CH0_TX_THR_EVENT_INT_CLR    0x01000000
// Set this bit to clear the RMT_CHn_ERR_INT interrupt. (RO)
#define RMT_CH7_ERR_INT_CLR             0x00800000
#define RMT_CH6_ERR_INT_CLR             0x00400000
#define RMT_CH5_ERR_INT_CLR             0x00200000
#define RMT_CH4_ERR_INT_CLR             0x00100000
#define RMT_CH3_ERR_INT_CLR             0x00080000
#define RMT_CH2_ERR_INT_CLR             0x00040000
#define RMT_CH1_ERR_INT_CLR             0x00020000
#define RMT_CH0_ERR_INT_CLR             0x00010000
// Set this bit to clear the RMT_CHn_RX_END_INT interrupt. (RO)
#define RMT_CH7_RX_END_INT_CLR          0x00008000
#define RMT_CH6_RX_END_INT_CLR          0x00004000
#define RMT_CH5_RX_END_INT_CLR          0x00002000
#define RMT_CH4_RX_END_INT_CLR          0x00001000
#define RMT_CH3_RX_END_INT_CLR          0x00000800
#define RMT_CH2_RX_END_INT_CLR          0x00000400
#define RMT_CH1_RX_END_INT_CLR          0x00000200
#define RMT_CH0_RX_END_INT_CLR          0x00000100
// Set this bit to clear the RMT_CHn_TX_END_INT interrupt. (RO)
#define RMT_CH7_TX_END_INT_CLR          0x00000080
#define RMT_CH6_TX_END_INT_CLR          0x00000040
#define RMT_CH5_TX_END_INT_CLR          0x00000020
#define RMT_CH4_TX_END_INT_CLR          0x00000010
#define RMT_CH3_TX_END_INT_CLR          0x00000008
#define RMT_CH2_TX_END_INT_CLR          0x00000004
#define RMT_CH1_TX_END_INT_CLR          0x00000002
#define RMT_CH0_TX_END_INT_CLR          0x00000001

/* Channel n duty cycle configuration register */
#define RMT_CH0CARRIER_DUTY_REG MMIO32(RMT_BASE + 0xb0)
#define RMT_CH1CARRIER_DUTY_REG MMIO32(RMT_BASE + 0xb4)
#define RMT_CH2CARRIER_DUTY_REG MMIO32(RMT_BASE + 0xb8)
#define RMT_CH3CARRIER_DUTY_REG MMIO32(RMT_BASE + 0xbc)
#define RMT_CH4CARRIER_DUTY_REG MMIO32(RMT_BASE + 0xc0)
#define RMT_CH5CARRIER_DUTY_REG MMIO32(RMT_BASE + 0xc4)
#define RMT_CH6CARRIER_DUTY_REG MMIO32(RMT_BASE + 0xc8)
#define RMT_CH7CARRIER_DUTY_REG MMIO32(RMT_BASE + 0xcc)
// configure the carrier wave’s high-level clock period for channel n.
#define RMT_CARRIER_HIGH_CH_MSK     0xffff0000
#define RMT_CARRIER_HIGH_CH_SFT     16
#define RMT_CARRIER_HIGH_CH_SET(n) ((n<<RMT_CARRIER_HIGH_CH_SFT)&RMT_CARRIER_HIGH_CH_MSK)
// configure the carrier wave’s low-level clock period for channel n.
#define RMT_CARRIER_LOW_CH_MSK      0x0000ffff
#define RMT_CARRIER_LOW_CH_SET(n)   (n&RMT_CARRIER_LOW_CH_MSK)

/* Channel n Tx event configuration register */
#define RMT_CH0_TX_LIM_REG      MMIO32(RMT_BASE + 0xd0)
#define RMT_CH1_TX_LIM_REG      MMIO32(RMT_BASE + 0xd4)
#define RMT_CH2_TX_LIM_REG      MMIO32(RMT_BASE + 0xd8)
#define RMT_CH3_TX_LIM_REG      MMIO32(RMT_BASE + 0xdc)
#define RMT_CH4_TX_LIM_REG      MMIO32(RMT_BASE + 0xe0)
#define RMT_CH5_TX_LIM_REG      MMIO32(RMT_BASE + 0xe4)
#define RMT_CH6_TX_LIM_REG      MMIO32(RMT_BASE + 0xe8)
#define RMT_CH7_TX_LIM_REG      MMIO32(RMT_BASE + 0xec)
// When ch n sends more entries than spec here, it produces a TX_THR_EVENT int.
#define RMT_TX_LIM_CH_MSK   0x1ff

/* RMT-wide configuration register */
#define RMT_APB_CONF_REG        MMIO32(RMT_BASE + 0xf0)
// This bit enables wraparound mode:
#define RMT_MEM_TX_WRAP_EN  0x2
// This bit must be 1 in order to access the RMT memory.
#define RMT_MEM_ACCESS_EN   0x1

#endif
