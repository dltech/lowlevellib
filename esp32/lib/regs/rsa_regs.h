#ifndef H_RSA_REGS
#define H_RSA_REGS
/*
 * ESP32 alternative library, RSA asymmetric cipher algorithms
 * accelerator registers.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Register to store M’ */
#define RSA_M_PRIME_REG         MMIO32(RSA_ACCELERATOR + 0x800)
// This register contains M’. (R/W)

/* Modular exponentiation mode */
#define RSA_MODEXP_MODE_REG     MMIO32(RSA_ACCELERATOR + 0x804)
// This register contains the mode of modular exponentiation. (R/W)
#define RSA_MODEXP_MODE_MSK 0x7

/* Start bit */
#define RSA_MODEXP_START_REG    MMIO32(RSA_ACCELERATOR + 0x808)
// Write 1 to start modular exponentiation. (WO
#define RSA_MODEXP_START    0x1

/* Modular multiplication mode */
#define RSA_MULT_MODE_REG       MMIO32(RSA_ACCELERATOR + 0x80c)
// This register contains the mode of modular multiplication and multiplication.
#define RSA_MULT_MODE_MSK   0xf

/* Start bit */
#define RSA_MULT_START_REG      MMIO32(RSA_ACCELERATOR + 0x810)
// Write 1 to start modular multiplication or multiplication. (WO)
#define RSA_MULT_START  0x1

/* RSA interrupt register */
#define RSA_INTERRUPT_REG       MMIO32(RSA_ACCELERATOR + 0x814)
// RSA interrupt status register. Will read 1 once an operation has completed. (R/W)
#define RSA_INTERRUPT   0x1

/* RSA clean register */
#define RSA_CLEAN_REG           MMIO32(RSA_ACCELERATOR + 0x818)
// This bit will read 1 once the memory initialization is completed. (RO)
#define RSA_CLEAN   0x1

#endif
