#ifndef H_ULP_REGS
#define H_ULP_REGS
/*
 * ESP32 alternative library, ultra-low-power processor register.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Timer cycles setting n */
#define SENS_ULP_CP_SLEEP_CYC0_REG      MMIO32(RTC_BASE + 0x818)
#define SENS_ULP_CP_SLEEP_CYC1_REG      MMIO32(RTC_BASE + 0x81c)
#define SENS_ULP_CP_SLEEP_CYC2_REG      MMIO32(RTC_BASE + 0x820)
#define SENS_ULP_CP_SLEEP_CYC3_REG      MMIO32(RTC_BASE + 0x824)
#define SENS_ULP_CP_SLEEP_CYC4_REG      MMIO32(RTC_BASE + 0x828)
// the ULP coprocessor can select one of such registers by using the SLEEP instr.

/*  */
#define SENS_SAR_START_FORCE_REG        MMIO32(RTC_BASE + 0x82c)
// ULP PC entry address. (R/W)
#define SENS_PC_INIT_MSK            0x3ff800
#define SENS_PC_INIT_SFT            11
#define SENS_PC_INIT_SET(n)         ((n<<SENS_PC_INIT_SFT)&SENS_PC_INIT_MSK)
// Set this bit to start the ULP coprocessor;
#define SENS_ULP_CP_START_TOP       0x000200
// 1: ULP coprocessor is started by SENS_ULP_CP_START_TOP;
#define SENS_ULP_CP_FORCE_START_TOP 0x000100

/* I2C addresses 0 and 1 */
#define SENS_SAR_SLAVE_ADDR1_REG        MMIO32(RTC_BASE + 0x83c)
// I2C slave address 0. (R/W)
#define SENS_I2C_SLAVE_ADDR0_MSK    0x3ff800
#define SENS_I2C_SLAVE_ADDR0_SFT    11
#define SENS_I2C_SLAVE_ADDR0_SET(n) ((n<<SENS_I2C_SLAVE_ADDR0_SFT)&SENS_I2C_SLAVE_ADDR0_MSK)
// I2C slave address 1. (R/W)
#define SENS_I2C_SLAVE_ADDR1_MSK    0x0007ff
#define SENS_I2C_SLAVE_ADDR1_SET(n) (n&SENS_I2C_SLAVE_ADDR1_MSK)

/* I2C addresses 2 and 3 */
#define SENS_SAR_SLAVE_ADDR2_REG        MMIO32(RTC_BASE + 0x840)
// I2C slave address 2. (R/W)
#define SENS_I2C_SLAVE_ADDR2_MSK    0x3ff800
#define SENS_I2C_SLAVE_ADDR2_SFT    11
#define SENS_I2C_SLAVE_ADDR2_SET(n) ((n<<SENS_I2C_SLAVE_ADDR2_SFT)&SENS_I2C_SLAVE_ADDR2_MSK)
// I2C slave address 3. (R/W)
#define SENS_I2C_SLAVE_ADDR3_MSK    0x0007ff
#define SENS_I2C_SLAVE_ADDR3_SET(n) (n&SENS_I2C_SLAVE_ADDR3_MSK)

/* I2C addresses 4 and 5 */
#define SENS_SAR_SLAVE_ADDR3_REG        MMIO32(RTC_BASE + 0x844)
// I2C slave address 4. (R/W)
#define SENS_I2C_SLAVE_ADDR4_MSK    0x3ff800
#define SENS_I2C_SLAVE_ADDR4_SFT    11
#define SENS_I2C_SLAVE_ADDR4_SET(n) ((n<<SENS_I2C_SLAVE_ADDR4_SFT)&SENS_I2C_SLAVE_ADDR4_MSK)
// I2C slave address 5. (R/W)
#define SENS_I2C_SLAVE_ADDR5_MSK    0x0007ff
#define SENS_I2C_SLAVE_ADDR5_SET(n) (n&SENS_I2C_SLAVE_ADDR5_MSK)

/* I2C addresses 6 and 7 */
#define SENS_SAR_SLAVE_ADDR4_REG        MMIO32(RTC_BASE + 0x848)
// I2C slave address 6. (R/W)
#define SENS_I2C_SLAVE_ADDR6_MSK    0x3ff800
#define SENS_I2C_SLAVE_ADDR6_SFT    11
#define SENS_I2C_SLAVE_ADDR6_SET(n) ((n<<SENS_I2C_SLAVE_ADDR6_SFT)&SENS_I2C_SLAVE_ADDR6_MSK)
// I2C slave address 7. (R/W)
#define SENS_I2C_SLAVE_ADDR7_MSK    0x0007ff
#define SENS_I2C_SLAVE_ADDR7_SET(n) (n&SENS_I2C_SLAVE_ADDR7_MSK)

/* I2C control registers */
#define SENS_SAR_I2C_CTRL_REG           MMIO32(RTC_BASE + 0x850)
// 1: I2C started by SW, 0: I2C started by FSM. (R/W)
#define SENS_SAR_I2C_START_FORCE    0x20000000
// Start I2C; active only when SENS_SAR_I2C_START_FORCE = 1. (R/W)
#define SENS_SAR_I2C_START          0x10000000
// I2C control data; active only when SENS_SAR_I2C_START_FORCE = 1. (R/W)
#define SENS_SAR_I2C_CTRL_MSK       0x0fffffff
#define SENS_SAR_I2C_CTRL_SET(n)    (n&SENS_SAR_I2C_CTRL_MSK)

/* Transmission setting */
#define RTC_I2C_CTRL_REG                MMIO32(RTC_BASE + 0xc04)
// Receive LSB first. (R/W)
#define RTC_I2C_RX_LSB_FIRST    0x80
// Send LSB first. (R/W)
#define RTC_I2C_TX_LSB_FIRST    0x40
// Force to generate a start condition. (R/W)
#define RTC_I2C_TRANS_START     0x20
// Master (1), or slave (0). (R/W)
#define RTC_I2C_MS_MODE         0x10
// SCL is push-pull (1) or open-drain (0). (R/W)
#define RTC_I2C_SCL_FORCE_OUT   0x02
// SDA is push-pull (1) or open-drain (0). (R/W)
#define RTC_I2C_SDA_FORCE_OUT   0x01

/* Debug status */
#define RTC_I2C_DEBUG_STATUS_REG        MMIO32(RTC_BASE + 0xc08)
// State of SCL machine. (R/W)
#define RTC_I2C_SCL_STATE_MSK       0x70000000
#define RTC_I2C_SCL_STATE_SFT       28
#define RTC_I2C_SCL_STATE_SET(n) ((n<<RTC_I2C_SCL_STATE_SFT)&RTC_I2C_SCL_STATE_MSK)
// State of the main machine. (R/W)
#define RTC_I2C_MAIN_STATE_MSK      0x0e000000
#define RTC_I2C_MAIN_STATE_SFT      25
#define RTC_I2C_MAIN_STATE_SET(n) ((n<<RTC_I2C_MAIN_STATE_SFT)&RTC_I2C_MAIN_STATE_MSK)
// 8-bit transmit done. (R/W)
#define RTC_I2C_BYTE_TRANS          0x00000040
// Indicates whether the addresses are matched, when in slave mode. (R/W)
#define RTC_I2C_SLAVE_ADDR_MATCH    0x00000020
// Operation is in progress. (R/W)
#define RTC_I2C_BUS_BUSY            0x00000010
// Indicates the loss of I2C bus control, when in master mode. (R/W)
#define RTC_I2C_ARB_LOST            0x00000008
// Transfer has timed out. (R/W)
#define RTC_I2C_TIMED_OUT           0x00000004
// Indicates the value of the received R/W bit, when in slave mode. (R/W)
#define RTC_I2C_SLAVE_RW            0x00000002
// The value of ACK signal on the bus. (R/W)
#define RTC_I2C_ACK_VAL             0x00000001

/* Timeout setting */
#define RTC_I2C_TIMEOUT_REG             MMIO32(RTC_BASE + 0xc0c)
// Maximum number of RTC_FAST_CLK cycles that the transmission can take. (R/W)
#define RTC_I2C_TIMEOUT_MSK 0xfffff

/* Local slave address setting */
#define RTC_I2C_SLAVE_ADDR_REG          MMIO32(RTC_BASE + 0xc10)
// Set if local slave address is 10-bit. (R/W)
#define RTC_I2C_SLAVE_ADDR_10BIT    0x80000000
// Local slave address. (R/W)
#define RTC_I2C_SLAVE_ADDR_MSK      0x00007fff

/* Configures the SDA hold time after a negative SCL edge */
#define RTC_I2C_SDA_DUTY_REG            MMIO32(RTC_BASE + 0xc30)
// Number of RTC_FAST_CLK cycles between the SDA switch and the falling edge of SCL.
#define RTC_I2C_SDA_DUTY    0xfffff

/* Configures the low level width of SCL */
#define RTC_I2C_SCL_LOW_PERIOD_REG      MMIO32(RTC_BASE + 0xc00)
// Number of RTC_FAST_CLK cycles when SCL == 0. (R/W)
#define RTC_I2C_SCL_LOW_PERIOD_MSK  0x7ffff

/* Configures the high level width of SCL */
#define RTC_I2C_SCL_HIGH_PERIOD_REG     MMIO32(RTC_BASE + 0xc38)
// Number of RTC_FAST_CLK cycles when SCL == 1. (R/W)
#define RTC_I2C_SCL_HIGH_PERIOD_MSK 0xfffff

/* Configures the delay between the SDA and SCL negative edge for a start cond */
#define RTC_I2C_SCL_START_PERIOD_REG    MMIO32(RTC_BASE + 0xc40)
// Number of RTC_FAST_CLK cycles to wait before generating a start condition. (R/W)
#define RTC_I2C_SCL_START_PERIOD    0xfffff

/* Configures the delay between the SDA and SCL positive edge for a stop cond */
#define RTC_I2C_SCL_STOP_PERIOD_REG     MMIO32(RTC_BASE + 0xc44)
// Number of RTC_FAST_CLK cycles to wait before generating a stop condition. (R/W)
#define RTC_I2C_SCL_STOP_PERIOD     0xfffff

/* Clear status of I2C communication events */
#define RTC_I2C_INT_CLR_REG             MMIO32(RTC_BASE + 0xc24)
// Clear interrupt upon timeout. (R/W)
#define RTC_I2C_TIME_OUT_INT_CLR                0x100
// Clear interrupt upon detecting a stop pattern. (R/W)
#define RTC_I2C_TRANS_COMPLETE_INT_CLR          0x080
// Clear interrupt upon completion of transaction, when in master mode. (R/W)
#define RTC_I2C_MASTER_TRANS_COMPLETE_INT_CLR   0x040
// Clear interrupt upon losing control of the bus, when in master mode. (R/W)
#define RTC_I2C_ARBITRATION_LOST_INT_CLR        0x020
// Clear interrupt upon completion of transaction, when in slave mode. (R/W)
#define RTC_I2C_SLAVE_TRANS_COMPLETE_INT_CLR    0x010

/* Enable capture of I2C communication status events */
#define RTC_I2C_INT_EN_REG              MMIO32(RTC_BASE + 0xc28)
// Enable interrupt upon timeout. (R/W)
#define RTC_I2C_TIME_OUT_INT_ENA                0x100
// Enable interrupt upon detecting a stop pattern. (R/W)
#define RTC_I2C_TRANS_COMPLETE_INT_ENA          0x080
// Enable interrupt upon completion of transaction, when in master mode. (R/W)
#define RTC_I2C_MASTER_TRANS_COMPLETE_INT_ENA   0x040
// Enable interrupt upon losing control of the bus, when in master mode. (R/W)
#define RTC_I2C_ARBITRATION_LOST_INT_ENA        0x020


/* Status of captured I2C communication events */
#define RTC_I2C_INT_ST_REG              MMIO32(RTC_BASE + 0xc2c)
// Detected timeout. (R/O)
#define RTC_I2C_TIME_OUT_INT_ST         0x80
// Detected stop pattern on I2C bus. (R/O)
#define RTC_I2C_TRANS_COMPLETE_INT_ST   0x40
// Transaction completed, when in master mode. (R/O)
#define RTC_I2C_MASTER_TRAN_COMP_INT_ST 0x20
// Bus control lost, when in master mode. (R/O)
#define RTC_I2C_ARBITRATION_LOST_INT_ST 0x10

#endif
