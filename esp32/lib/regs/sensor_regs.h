#ifndef H_SENSOR_REGS
#define H_SENSOR_REGS
/*
 * ESP32 alternative library, On­Chip Sensors and Analog Signal Processing
 * registers.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Touch pad control */
#define SENS_SAR_TOUCH_CTRL1_REG        MMIO32(RTC_BASE + 0x858)
// 1: wakeup interrupt is generated if SET1 is touched
#define SENS_TOUCH_OUT_1EN              0x02000000
// 1: the touch pad is considered touched when the val of the cnt is gt threshold
#define SENS_TOUCH_OUT_SEL              0x01000000
// The waiting time (in 8 MHz cycles) between TOUCH_START and TOUCH_XPD. (R/W)
#define SENS_TOUCH_XPD_WAIT_MSK         0x00ff0000
#define SENS_TOUCH_XPD_WAIT_SFT         16
#define SENS_TOUCH_XPD_WAIT_SET(n) ((n<<SENS_TOUCH_XPD_WAIT_SFT)&SENS_TOUCH_XPD_WAIT_MSK)
// The measurement’s duration (in 8 MHz cycles). (R/W)
#define SENS_TOUCH_MEAS_DELAY_MSK       0x0000ffff
#define SENS_TOUCH_MEAS_DELAY_SET(n)    (n&SENS_TOUCH_MEAS_DELAY_MSK)

/* Touch pad control and status */
#define SENS_SAR_TOUCH_CTRL2_REG        MMIO32(RTC_BASE + 0x884)
// Set to clear reg_touch_meas_en. (WO)
#define SENS_TOUCH_MEAS_EN_CLR          0x40000000
// Sleep cycles for timer. (R/W)
#define SENS_TOUCH_SLEEP_CYCLES_MSK     0x3fffc000
#define SENS_TOUCH_SLEEP_CYCLES_SFT     14
#define SENS_TOUCH_SLEEP_CYCLES_SET(n) ((n<<SENS_TOUCH_SLEEP_CYCLES_SFT)&SENS_TOUCH_SLEEP_CYCLES_MSK)
// 1: starts the Touch FSM via software; 0: starts the Touch FSM via timer. (R/W)
#define SENS_TOUCH_START_FORCE          0x00002000
// 1: starts the Touch FSM; this is valid when reg_touch_start_force is set. (R/W)
#define SENS_TOUCH_START_EN             0x00001000
// 1: TOUCH_START & TOUCH_XPD are controlled by the Touch FSM;
#define SENS_TOUCH_START_FSM_EN         0x00000800
// Set to 1 by FSM, indicating that touch measurement is done. (RO)
#define SENS_TOUCH_MEAS_DONE            0x00000400
// 10-bit register indicating which pads are touched. (RO)
#define SENS_TOUCH_MEAS_EN_MSK          0x000003ff
#define SENS_TOUCH_MEAS_EN_GET  (SENS_SAR_TOUCH_CTRL2_REG&SENS_TOUCH_MEAS_EN_MSK)


/* Wakeup interrupt control and working set */
#define SENS_SAR_TOUCH_ENABLE_REG       MMIO32(RTC_BASE + 0x88c)
// Bitmap defining SET1 for generating a wakeup interrupt;
#define SENS_TOUCH_PAD_OUTEN1_MSK       0x3ff00000
#define SENS_TOUCH_PAD_OUTEN1_SFT       20
#define SENS_TOUCH_PAD_OUTEN1_SET(n) ((n<<SENS_TOUCH_PAD_OUTEN1_SFT)&SENS_TOUCH_PAD_OUTEN1_MSK)
// Bitmap defining SET2 for generating a wakeup interrupt;
#define SENS_TOUCH_PAD_OUTEN2_MSK       0x000ffc00
#define SENS_TOUCH_PAD_OUTEN2_SFT       10
#define SENS_TOUCH_PAD_OUTEN2_SET(n) ((n<<SENS_TOUCH_PAD_OUTEN2_SFT)&SENS_TOUCH_PAD_OUTEN2_MSK)
// Bitmap defining the working set during measurement. (R/W)
#define SENS_TOUCH_PAD_WORKEN_MSK       0x000003ff
#define SENS_TOUCH_PAD_WORKEN_SET(n)    (n&SENS_TOUCH_PAD_WORKEN_MSK)

/* Threshold setup for pads 0 and 1 */
#define SENS_SAR_TOUCH_THRES1_REG       MMIO32(RTC_BASE + 0x85c)
// The threshold for touch pad 0. (R/W)
#define SENS_TOUCH_OUT_TH0_MSK      0xffff0000
#define SENS_TOUCH_OUT_TH0_SFT      16
#define SENS_TOUCH_OUT_TH0_SET(n) ((n<<SENS_TOUCH_OUT_TH0_SFT)&SENS_TOUCH_OUT_TH0_MSK)
// The threshold for touch pad 1. (R/W)
#define SENS_TOUCH_OUT_TH1_MSK      0x0000ffff
#define SENS_TOUCH_OUT_TH1_SET(n)   (n&SENS_TOUCH_OUT_TH1_MSK)

/* Threshold setup for pads 2 and 3 */
#define SENS_SAR_TOUCH_THRES2_REG       MMIO32(RTC_BASE + 0x860)
// The threshold for touch pad 2. (R/W)
#define SENS_TOUCH_OUT_TH2_MSK      0xffff0000
#define SENS_TOUCH_OUT_TH2_SFT      16
#define SENS_TOUCH_OUT_TH2_SET(n) ((n<<SENS_TOUCH_OUT_TH2_SFT)&SENS_TOUCH_OUT_TH2_MSK)
// The threshold for touch pad 3. (R/W)
#define SENS_TOUCH_OUT_TH3_MSK      0x0000ffff
#define SENS_TOUCH_OUT_TH3_SET(n)   (n&SENS_TOUCH_OUT_TH3_MSK)

/* Threshold setup for pads 4 and 5 */
#define SENS_SAR_TOUCH_THRES3_REG       MMIO32(RTC_BASE + 0x864)
// The threshold for touch pad 4. (R/W)
#define SENS_TOUCH_OUT_TH4_MSK      0xffff0000
#define SENS_TOUCH_OUT_TH4_SFT      16
#define SENS_TOUCH_OUT_TH4_SET(n) ((n<<SENS_TOUCH_OUT_TH4_SFT)&SENS_TOUCH_OUT_TH4_MSK)
// The threshold for touch pad 5. (R/W)
#define SENS_TOUCH_OUT_TH5_MSK      0x0000ffff
#define SENS_TOUCH_OUT_TH5_SET(n)   (n&SENS_TOUCH_OUT_TH5_MSK)

/* Threshold setup for pads 6 and 7 */
#define SENS_SAR_TOUCH_THRES4_REG       MMIO32(RTC_BASE + 0x868)
// The threshold for touch pad 6. (R/W)
#define SENS_TOUCH_OUT_TH6_MSK      0xffff0000
#define SENS_TOUCH_OUT_TH6_SFT      16
#define SENS_TOUCH_OUT_TH6_SET(n) ((n<<SENS_TOUCH_OUT_TH6_SFT)&SENS_TOUCH_OUT_TH6_MSK)
// The threshold for touch pad 7. (R/W)
#define SENS_TOUCH_OUT_TH7_MSK      0x0000ffff
#define SENS_TOUCH_OUT_TH7_SET(n)   (n&SENS_TOUCH_OUT_TH7_MSK)

/* Threshold setup for pads 8 and 9 */
#define SENS_SAR_TOUCH_THRES5_REG       MMIO32(RTC_BASE + 0x86c)
// The threshold for touch pad 8. (R/W)
#define SENS_TOUCH_OUT_TH8_MSK      0xffff0000
#define SENS_TOUCH_OUT_TH8_SFT      16
#define SENS_TOUCH_OUT_TH8_SET(n) ((n<<SENS_TOUCH_OUT_TH8_SFT)&SENS_TOUCH_OUT_TH8_MSK)
// The threshold for touch pad 9. (R/W)
#define SENS_TOUCH_OUT_TH9_MSK      0x0000ffff
#define SENS_TOUCH_OUT_TH9_SET(n)   (n&SENS_TOUCH_OUT_TH9_MSK)

/* Counters for pads 0 and 1 */
#define SENS_SAR_TOUCH_OUT1_REG         MMIO32(RTC_BASE + 0x870)
// The counter for touch pad 0. (RO)
#define SENS_TOUCH_MEAS_OUT0_MSK      0xffff0000
#define SENS_TOUCH_MEAS_OUT0_SFT      16
#define SENS_TOUCH_MEAS_OUT0_GET ((SENS_SAR_TOUCH_OUT1_REG>>SENS_TOUCH_MEAS_OUT0_SFT)&0xffff)
// The counter for touch pad 1. (RO)
#define SENS_TOUCH_MEAS_OUT1_MSK      0x0000ffff
#define SENS_TOUCH_MEAS_OUT1_GET (SENS_SAR_TOUCH_OUT1_REG&SENS_TOUCH_MEAS_OUT1_MSK)

/* Counters for pads 2 and 3 */
#define SENS_SAR_TOUCH_OUT2_REG         MMIO32(RTC_BASE + 0x874)
// The counter for touch pad 2. (RO)
#define SENS_TOUCH_MEAS_OUT2_MSK      0xffff0000
#define SENS_TOUCH_MEAS_OUT2_SFT      16
#define SENS_TOUCH_MEAS_OUT2_GET ((SENS_SAR_TOUCH_OUT2_REG>>SENS_TOUCH_MEAS_OUT2_SFT)&0xffff)
// The counter for touch pad 3. (RO)
#define SENS_TOUCH_MEAS_OUT3_MSK      0x0000ffff
#define SENS_TOUCH_MEAS_OUT3_GET (SENS_SAR_TOUCH_OUT2_REG&SENS_TOUCH_MEAS_OUT3_MSK)

/* Counters for pads 4 and 5 */
#define SENS_SAR_TOUCH_OUT3_REG         MMIO32(RTC_BASE + 0x878)
// The counter for touch pad 4. (RO)
#define SENS_TOUCH_MEAS_OUT4_MSK      0xffff0000
#define SENS_TOUCH_MEAS_OUT4_SFT      16
#define SENS_TOUCH_MEAS_OUT4_GET ((SENS_SAR_TOUCH_OUT3_REG>>SENS_TOUCH_MEAS_OUT4_SFT)&0xffff)
// The counter for touch pad 5. (RO)
#define SENS_TOUCH_MEAS_OUT5_MSK      0x0000ffff
#define SENS_TOUCH_MEAS_OUT5_GET (SENS_SAR_TOUCH_OUT3_REG&SENS_TOUCH_MEAS_OUT5_MSK)

/* Counters for pads 6 and 7 */
#define SENS_SAR_TOUCH_OUT4_REG         MMIO32(RTC_BASE + 0x87c)
// The counter for touch pad 6. (RO)
#define SENS_TOUCH_MEAS_OUT6_MSK      0xffff0000
#define SENS_TOUCH_MEAS_OUT6_SFT      16
#define SENS_TOUCH_MEAS_OUT6_GET ((SENS_SAR_TOUCH_OUT4_REG>>SENS_TOUCH_MEAS_OUT6_SFT)&0xffff)
// The counter for touch pad 7. (RO)
#define SENS_TOUCH_MEAS_OUT7_MSK      0x0000ffff
#define SENS_TOUCH_MEAS_OUT7_GET (SENS_SAR_TOUCH_OUT4_REG&SENS_TOUCH_MEAS_OUT7_MSK)

/* Counters for pads 8 and 9 */
#define SENS_SAR_TOUCH_OUT5_REG         MMIO32(RTC_BASE + 0x880)
// The counter for touch pad 8. (RO)
#define SENS_TOUCH_MEAS_OUT8_MSK      0xffff0000
#define SENS_TOUCH_MEAS_OUT8_SFT      16
#define SENS_TOUCH_MEAS_OUT8_GET ((SENS_SAR_TOUCH_OUT5_REG>>SENS_TOUCH_MEAS_OUT8_SFT)&0xffff)
// The counter for touch pad 9. (RO)
#define SENS_TOUCH_MEAS_OUT9_MSK      0x0000ffff
#define SENS_TOUCH_MEAS_OUT9_GET (SENS_SAR_TOUCH_OUT5_REG&SENS_TOUCH_MEAS_OUT9_MSK)

/* SAR ADC1 and ADC2 control */
#define SENS_SAR_START_FORCE_REG        MMIO32(RTC_BASE + 0x82c)
// Stop SAR ADC1 conversion. (R/W)
#define SENS_SAR1_STOP              0x800000
// Stop SAR ADC2 conversion. (R/W)
#define SENS_SAR2_STOP              0x400000
// Initialized PC for ULP coprocessor. (R/W)
#define SENS_PC_INIT_MSK            0x3ff800
#define SENS_PC_INIT_SFT            11
#define SENS_PC_INIT_SET(n) ((n<<SENS_PC_INIT_SFT)&SENS_PC_INIT_MSK)
// Write 1 to start ULP coproc; it is act only when reg_ulp_cp_force_start_top = 1.
#define SENS_ULP_CP_START_TOP       0x000200
// : ULP coprocessor is started by SW, 0: ULP coprocessor is started by timer.
#define SENS_ULP_CP_FORCE_START_TOP 0x000100
// SAR2_PWDET_CCT, PA power detector capacitance tuning. (R/W)
#define SENS_SAR2_PWDET_CCT_MSK     0x0000e0
#define SENS_SAR2_PWDET_CCT_SFT     5
#define SENS_SAR2_PWDET_CCT_SET(n) ((n<<SENS_SAR2_PWDET_CCT_SFT)&SENS_SAR2_PWDET_CCT_MSK)
// SAR2_EN_TEST is active only when reg_sar2_dig_force = 0. (R/W)
#define SENS_SAR2_EN_TEST           0x000010
// Bit width of SAR ADC2
#define SENS_SAR2_BIT_WIDTH9        0x000000
#define SENS_SAR2_BIT_WIDTH10       0x000004
#define SENS_SAR2_BIT_WIDTH11       0x000008
#define SENS_SAR2_BIT_WIDTH12       0x00000c
// Bit width of SAR ADC1
#define SENS_SAR1_BIT_WIDTH9        0x000000
#define SENS_SAR1_BIT_WIDTH10       0x000001
#define SENS_SAR1_BIT_WIDTH11       0x000002
#define SENS_SAR1_BIT_WIDTH12       0x000003

/* SAR ADC1 data and sampling control */
#define SENS_SAR_READ_CTRL_REG          MMIO32(RTC_BASE + 0x800)
// Invert SAR ADC1 data. (R/W)
#define SENS_SAR1_DATA_INV              0x10000000
// 1: SAR ADC1 controlled by DIG ADC1 CTR, 0: SAR ADC1 controlled by RTC ADC1 CTRL.
#define SENS_SAR1_DIG_FORCE             0x08000000
// Bit width of SAR ADC1. (R/W)
#define SENS_SAR1_SAMPLE_BIT9           0x00000000
#define SENS_SAR1_SAMPLE_BIT10          0x00010000
#define SENS_SAR1_SAMPLE_BIT11          0x00020000
#define SENS_SAR1_SAMPLE_BIT12          0x00030000
// Sample cycles for SAR ADC1. (R/W)
#define SENS_SAR1_SAMPLE_CYCLE_MSK      0x0000ff00
#define SENS_SAR1_SAMPLE_CYCLE_SFT      8
#define SENS_SAR1_SAMPLE_CYCLE_SET(n) ((n<<SENS_SAR1_SAMPLE_CYCLE_SFT)&SENS_SAR1_SAMPLE_CYCLE_MSK)
// Clock divider. (R/W)
#define SENS_SAR1_CLK_DIV_MSK           0x000000ff
#define SENS_SAR1_CLK_DIV_SET(n)        (n&SENS_SAR1_CLK_DIV_MSK)

/* SAR ADC1 conversion control and status */
#define SENS_SAR_MEAS_START1_REG        MMIO32(RTC_BASE + 0x854)
// 1: SAR ADC1 pad enable bitmap is controlled by SW
#define SENS_SAR1_EN_PAD_FORCE      0x80000000
// SAR ADC1 pad enable bitmap; active only when reg_sar1_en_pad_force = 1. (R/W)
#define SENS_SAR1_EN_PAD_MSK        0x7ff80000
#define SENS_SAR1_EN_PAD_SFT        19
#define SENS_SAR1_EN_PAD_SET(n)     ((n<<SENS_SAR1_EN_PAD_SFT)&SENS_SAR1_EN_PAD_MSK)
// 1: SAR ADC1 controller (in RTC) is started by SW
#define SENS_MEAS1_START_FORCE      0x00040000
// SAR ADC1 controller (in RTC) starts conversion
#define SENS_MEAS1_START_SAR        0x00020000
// SAR ADC1 conversion-done indication. (RO)
#define SENS_MEAS1_DONE_SAR         0x00010000
// SAR ADC1 data. (RO)
#define SENS_MEAS1_DATA_SAR_MSK     0x0000ffff
#define SENS_MEAS1_DATA_SAR_SET(n)  (n&SENS_MEAS1_DATA_SAR_MSK)

/* SAR ADC2 data and sampling control */
#define SENS_SAR_READ_CTRL2_REG         MMIO32(RTC_BASE + 0x890)
// Invert SAR ADC2 data. (R/W)
#define SENS_SAR2_DATA_INV              0x20000000
// 1: SAR ADC2 controlled by DIG ADC2 CTRL or PWDET CTRL
#define SENS_SAR2_DIG_FORCE             0x10000000
// Bit width of SAR ADC2
#define SENS_SAR2_SAMPLE_BIT9           0x00000000
#define SENS_SAR2_SAMPLE_BIT10          0x00010000
#define SENS_SAR2_SAMPLE_BIT11          0x00020000
#define SENS_SAR2_SAMPLE_BIT12          0x00030000
// Sample cycles of SAR ADC2. (R/W)
#define SENS_SAR2_SAMPLE_CYCLE_MSK      0x0000ff00
#define SENS_SAR2_SAMPLE_CYCLE_SFT      8
#define SENS_SAR2_SAMPLE_CYCLE_SET(n) ((n<<SENS_SAR2_SAMPLE_CYCLE_SFT)&SENS_SAR2_SAMPLE_CYCLE_MSK)
// Clock divider. (R/W)
#define SENS_SAR2_CLK_DIV_MSK           0x000000ff
#define SENS_SAR2_CLK_DIV_SET(n)        (n&SENS_SAR2_CLK_DIV_MSK)

/* SAR ADC2 conversion control and status */
#define SENS_SAR_MEAS_START2_REG        MMIO32(RTC_BASE + 0x894)
// 1: SAR ADC2 pad enable bitmap is controlled by SW
#define SENS_SAR2_EN_PAD_FORCE      0x80000000
// SAR ADC2 pad enable bitmap; active only when reg_sar2_en_pad_force = 1. (R/W)
#define SENS_SAR2_EN_PAD_MSK        0x7ff80000
#define SENS_SAR2_EN_PAD_SFT        19
#define SENS_SAR2_EN_PAD_SET(n)     ((n<<SENS_SAR2_EN_PAD_SFT)&SENS_SAR2_EN_PAD_MSK)
// 1: SAR ADC2 controller (in RTC) is started by SW,
#define SENS_MEAS2_START_FORCE      0x00040000
// SAR ADC2 controller (in RTC) starts conv; actonly when reg_meas2_start_force = 1
#define SENS_MEAS2_START_SAR        0x00020000
// SAR ADC2-conversion-done indication. (RO)
#define SENS_MEAS2_DONE_SAR         0x00010000
// SAR ADC2 data. (RO)
#define SENS_MEAS2_DATA_SAR_MSK     0x0000ffff
#define SENS_MEAS2_DATA_SAR_SET(n)  (n&SENS_MEAS2_DATA_SAR_MSK)

/* Sleep cycles for ULP coprocessor */
#define SENS_ULP_CP_SLEEP_CYC0_REG      MMIO32(RTC_BASE + 0x818)
// Sleep cycles for ULP coprocessor timer. (R/W)

/* 2-bit attenuation for each pad */
#define SENS_SAR_ATTEN1_REG             MMIO32(RTC_BASE + 0x834)
#define SENS_SAR_ATTEN2_REG             MMIO32(RTC_BASE + 0x838)
// [1:0] is used for ADC1_CH0, [3:2] is used for ADC1_CH1, etc. (R/W)

/* DAC control */
#define SENS_SAR_DAC_CTRL1_REG          MMIO32(RTC_BASE + 0x898)
// 1: inverts PDAC_CLK, 0: no inversion. (R/W)
#define SENS_DAC_CLK_INV        0x2000000
// forces PDAC_CLK to be 1. (R/W)
#define SENS_DAC_CLK_FORCE_HIGH 0x1000000
// forces PDAC_CLK to be 0. (R/W)
#define SENS_DAC_CLK_FORCE_LOW  0x0800000
// 1: DAC1 & DAC2 use DMA, 0: DAC1 & DAC2 do not use DMA. (R/W)
#define SENS_DAC_DIG_FORCE      0x0400000
// 1: enable CW generator, 0: disable CW generator. (R/W)
#define SENS_SW_TONE_EN         0x0010000
// Frequency step for CW generator; can be used to adjust the frequency. (R/W)
#define SENS_SW_FSTEP_MSK       0x000ffff
#define SENS_SW_FSTEP_SET(n)    (n&SENS_SW_FSTEP_MSK)

/* DAC output control */
#define SENS_SAR_DAC_CTRL2_REG          MMIO32(RTC_BASE + 0x89c)
// 1: selects CW generator as source for PDAC2_DAC[7:0]
#define SENS_DAC_CW_EN2         0x2000000
// 1: selects CW generator as source for PDAC1_DAC[7:0]
#define SENS_DAC_CW_EN1         0x1000000
// DAC2
#define SENS_DAC_INV2_NOT       0x0000000
#define SENS_DAC_INV2_ALL       0x0400000
#define SENS_DAC_INV2_MSB       0x0800000
#define SENS_DAC_INV2_EXC_MSB   0x0c00000
// DAC1
#define SENS_DAC_INV1_NOT       0x0000000
#define SENS_DAC_INV1_ALL       0x0100000
#define SENS_DAC_INV1_MSB       0x0200000
#define SENS_DAC_INV1_EXC_MSB   0x0300000
// DAC2
#define SENS_DAC_SCALE2_NO      0x0000000
#define SENS_DAC_SCALE2_1DIV2   0x0040000
#define SENS_DAC_SCALE2_1DIV4   0x0080000
#define SENS_DAC_SCALE2_1DIV8   0x00c0000
// DAC1
#define SENS_DAC_SCALE1_NO      0x0000000
#define SENS_DAC_SCALE1_1DIV2   0x0010000
#define SENS_DAC_SCALE1_1DIV4   0x0020000
#define SENS_DAC_SCALE1_1DIV8   0x0030000
// DC offset for DAC2 CW generator. (R/W)
#define SENS_DAC_DC2_MSK        0x000ff00
#define SENS_DAC_DC2_SFT        8
#define SENS_DAC_DC2_SET(n)     ((n<<SENS_DAC_DC2_SFT)&SENS_DAC_DC2_MSK)
// DC offset for DAC1 CW generator. (R/W)
#define SENS_DAC_DC1_MSK        0x00000ff
#define SENS_DAC_DC1_SET(n)     (n&SENS_DAC_DC1_MSK)

/* SAR ADC common configuration */
#define APB_SARADC_CTRL_REG             MMIO32(TIMG1_BASE + 0x2610)
// 1: I2S input data is from SAR ADC (for DMA)
#define APB_SARADC_DATA_TO_I2S          0x4000000
// 1: sar_sel will be coded by the MSB of the 16-bit output data
#define APB_SARADC_DATA_SAR_SEL         0x2000000
// Clears the pointer of pattern table for DIG ADC2 CTRL. (R/W)
#define APB_SARADC_SAR2_PATT_P_CLEAR    0x1000000
// Clears the pointer of pattern table for DIG ADC1 CTRL. (R/W)
#define APB_SARADC_SAR1_PATT_P_CLEAR    0x0800000
// SAR ADC2, 0 - 15 means pattern table length of 1 - 16. (R/W)
#define APB_SARADC_SAR2_PATT_LEN_MSK    0x0780000
#define APB_SARADC_SAR2_PATT_LEN_SFT    19
#define APB_SARADC_SAR2_PATT_LEN_SET(n) ((n<<APB_SARADC_SAR2_PATT_LEN_SFT)&APB_SARADC_SAR2_PATT_LEN_MSK)
// SAR ADC1, 0 - 15 means pattern table length of 1 - 16. (R/W)
#define APB_SARADC_SAR1_PATT_LEN_MSK    0x0078000
#define APB_SARADC_SAR1_PATT_LEN_SFT    15
#define APB_SARADC_SAR1_PATT_LEN_SET(n) ((n<<APB_SARADC_SAR1_PATT_LEN_SFT)&APB_SARADC_SAR1_PATT_LEN_MSK)
// SAR clock divider. (R/W)
#define APB_SARADC_SAR_CLK_DIV_MSK      0x0007f80
#define APB_SARADC_SAR_CLK_DIV_SFT      7
#define APB_SARADC_SAR_CLK_DIV_SET(n) ((n<<APB_SARADC_SAR_CLK_DIV_SFT)&APB_SARADC_SAR_CLK_DIV_MSK)
// Reserved. Please initialize to 0b1 (R/W)
#define APB_SARADC_SAR_CLK_GATED        0x0000040
// 0: SAR1, 1: SAR2, this setting is applicable in the single SAR mode. (R/W)
#define APB_SARADC_SAR_SEL              0x0000020
//
#define APB_SARADC_WORK_MODE_SINGLE     0x0000000
#define APB_SARADC_WORK_MODE_DOUBLE     0x0000008
#define APB_SARADC_WORK_MODE_ALTERNATE  0x0000010
// 1: SAR ADC2 is controlled by DIG ADC2 CTRL
#define APB_SARADC_SAR2_MUX             0x0000004
// Reserved. Please initialize to 0 (R/W)
#define APB_SARADC_START                0x0000002
// Reserved. Please initialize to 0 (R/W)
#define APB_SARADC_START_FORCE          0x0000001

/* SAR ADC common configuration */
#define APB_SARADC_CTRL2_REG            MMIO32(TIMG1_BASE + 0x2614)
// 1: data to DIG ADC2 CTRL is inverted, 0: data is not inverted. (R/W)
#define APB_SARADC_SAR2_INV             0x400
// 1: data to DIG ADC1 CTRL is inverted, 0: data is not inverted. (R/W)
#define APB_SARADC_SAR1_INV             0x200
// Max conversion number. (R/W)
#define APB_SARADC_MAX_MEAS_NUM_MSK     0x1fe
#define APB_SARADC_MAX_MEAS_NUM_SFT     1
#define APB_SARADC_MAX_MEAS_NUM_SET(n) ((n<<APB_SARADC_MAX_MEAS_NUM_SFT)&APB_SARADC_MAX_MEAS_NUM_MSK)
// Reserved. Please initialize to 0b1 (R/W)
#define APB_SARADC_MEAS_NUM_LIMIT       0x001

/* SAR ADC FSM sample cycles configuration */
#define APB_SARADC_FSM_REG              MMIO32(TIMG1_BASE + 0x2618)
// Sample cycles. (R/W)
#define APB_SARADC_SAMPLE_CYCLE_SFT 24
#define APB_SARADC_SAMPLE_CYCLE_MSK 0xff000000
#define APB_SARADC_SAMPLE_CYCLE_SET(n) ((n<<APB_SARADC_SAMPLE_CYCLE_SFT)&APB_SARADC_SAMPLE_CYCLE_SFT)

/* Items 0 - 3 of pattern table */
#define APB_SARADC_SAR1_PATT_TAB1_REG   MMIO32(TIMG1_BASE + 0x261c)
#define APB_SARADC_SAR1_PATT_TAB2_REG   MMIO32(TIMG1_BASE + 0x2620)
#define APB_SARADC_SAR1_PATT_TAB3_REG   MMIO32(TIMG1_BASE + 0x2624)
#define APB_SARADC_SAR1_PATT_TAB4_REG   MMIO32(TIMG1_BASE + 0x2628)
#define APB_SARADC_SAR2_PATT_TAB1_REG   MMIO32(TIMG1_BASE + 0x262c)
#define APB_SARADC_SAR2_PATT_TAB2_REG   MMIO32(TIMG1_BASE + 0x2630)
#define APB_SARADC_SAR2_PATT_TAB3_REG   MMIO32(TIMG1_BASE + 0x2634)
#define APB_SARADC_SAR2_PATT_TAB4_REG   MMIO32(TIMG1_BASE + 0x2638)
// one byte for each pattern table

#endif
