#ifndef H_TIMG_REGS
#define H_TIMG_REGS
/*
 * ESP32 alternative library, general-purpose timers group registers.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Timer configuration register */
#define TIMG0_T0CONFIG_REG          MMIO32(TIMG0_BASE + 0x00)
#define TIMG0_T1CONFIG_REG          MMIO32(TIMG0_BASE + 0x24)
#define TIMG1_T0CONFIG_REG          MMIO32(TIMG1_BASE + 0x00)
#define TIMG1_T1CONFIG_REG          MMIO32(TIMG1_BASE + 0x24)
// When set, the timer x time-base counter is enabled. (R/W)
#define TIMG_EN             0x80000000
// When set, the timer x time-base counter will increment every clock tick.
#define TIMG_INCREASE       0x40000000
// When set, timer x auto-reload at alarm is enabled. (R/W)
#define TIMG_AUTORELOAD     0x20000000
// Timer x clock (Tx_clk) prescale value. (R/W)
#define TIMG_DIVIDER_MSK    0x1fffe000
#define TIMG_DIVIDER_SFT    13
#define TIMG_DIVIDER_SET(n) ((n<<TIMG_DIVIDER_SFT)&TIMG_DIVIDER_MSK)
// When set, an alarm will generate an edge type interrupt. (R/W)
#define TIMG_EDGE_INT_EN    0x00001000
// When set, an alarm will generate a level type interrupt. (R/W)
#define TIMG_LEVEL_INT_EN   0x00000800
// When set, the alarm is enabled.
#define TIMG_ALARM_EN       0x00000400

/* Timer current value, low 32 bits */
#define TIMG0_T0LO_REG              MMIO32(TIMG0_BASE + 0x04)
#define TIMG0_T1LO_REG              MMIO32(TIMG0_BASE + 0x28)
#define TIMG1_T0LO_REG              MMIO32(TIMG1_BASE + 0x04)
#define TIMG1_T1LO_REG              MMIO32(TIMG1_BASE + 0x28)
// low 32 bits of the time-base counter of timer x can be read here.

/* Timer current value, high 32 bits */
#define TIMG0_T0HI_REG              MMIO32(TIMG0_BASE + 0x08)
#define TIMG0_T1HI_REG              MMIO32(TIMG0_BASE + 0x2c)
#define TIMG1_T0HI_REG              MMIO32(TIMG1_BASE + 0x08)
#define TIMG1_T1HI_REG              MMIO32(TIMG1_BASE + 0x2c)
// the high 32 bits of the time-base counter of timer x can be read here. (RO)

/* Write to copy current timer value to TIMGn_T0_(LO/HI)_REG */
#define TIMG0_T0UPDATE_REG          MMIO32(TIMG0_BASE + 0x0c)
#define TIMG0_T1UPDATE_REG          MMIO32(TIMG0_BASE + 0x30)
#define TIMG1_T0UPDATE_REG          MMIO32(TIMG1_BASE + 0x0c)
#define TIMG1_T1UPDATE_REG          MMIO32(TIMG1_BASE + 0x30)
// Write any value to trigger a timer x time-base counter value update

/* Timer alarm value, low 32 bits */
#define TIMG0_T0ALARMLO_REG         MMIO32(TIMG0_BASE + 0x10)
#define TIMG0_T1ALARMLO_REG         MMIO32(TIMG0_BASE + 0x34)
#define TIMG1_T0ALARMLO_REG         MMIO32(TIMG1_BASE + 0x10)
#define TIMG1_T1ALARMLO_REG         MMIO32(TIMG1_BASE + 0x34)
// Timer x alarm trigger time-base counter value, low 32 bits. (R/W)

/* Timer alarm value, high 32 bits */
#define TIMG0_T0ALARMHI_REG         MMIO32(TIMG0_BASE + 0x14)
#define TIMG0_T1ALARMHI_REG         MMIO32(TIMG0_BASE + 0x38)
#define TIMG1_T0ALARMHI_REG         MMIO32(TIMG1_BASE + 0x14)
#define TIMG1_T1ALARMHI_REG         MMIO32(TIMG1_BASE + 0x38)
// Timer x alarm trigger time-base counter value, high 32 bits. (R/W)

/* Timer reload value, low 32 bits */
#define TIMG0_T0LOADLO_REG          MMIO32(TIMG0_BASE + 0x18)
#define TIMG0_T1LOADLO_REG          MMIO32(TIMG0_BASE + 0x3c)
#define TIMG1_T0LOADLO_REG          MMIO32(TIMG1_BASE + 0x18)
#define TIMG1_T1LOADLO_REG          MMIO32(TIMG1_BASE + 0x3c)
// Low 32 bits of the value that a reload will load onto timer x time-base counter.

/* Timer reload value, high 32 bits */
#define TIMG0_T0LOADHI_REG          MMIO32(TIMG0_BASE + 0x1c)
#define TIMG0_T1LOADHI_REG          MMIO32(TIMG0_BASE + 0x40)
#define TIMG1_T0LOADHI_REG          MMIO32(TIMG1_BASE + 0x1c)
#define TIMG1_T1LOADHI_REG          MMIO32(TIMG1_BASE + 0x40)
// High 32 bits of the value that a reload will load onto timer x time-base counter.

/* Write to reload timer from TIMGn_T0_(LOADLOLOADHI)_REG */
#define TIMG0_T0LOAD_REG            MMIO32(TIMG0_BASE + 0x20)
#define TIMG0_T1LOAD_REG            MMIO32(TIMG0_BASE + 0x44)
#define TIMG1_T0LOAD_REG            MMIO32(TIMG1_BASE + 0x20)
#define TIMG1_T1LOAD_REG            MMIO32(TIMG1_BASE + 0x44)
// Write any value to trigger a timer x time-base counter reload. (WO)

/* Watchdog timer configuration register */
#define TIMG0_Tx_WDTCONFIG0_REG     MMIO32(TIMG0_BASE + 0x48)
#define TIMG1_Tx_WDTCONFIG0_REG     MMIO32(TIMG1_BASE + 0x48)
// When set, MWDT is enabled. (R/W)
#define TIMG_WDT_EN                     0x80000000
// Stage 0 configuration.
#define TIMG_WDT_STG0_OFF               0x00000000
#define TIMG_WDT_STG0_INT               0x20000000
#define TIMG_WDT_STG0_RESET_CPU         0x40000000
#define TIMG_WDT_STG0_RESET_SYS         0x60000000
// Stage 1 configuration.
#define TIMG_WDT_STG1_OFF               0x00000000
#define TIMG_WDT_STG1_INT               0x08000000
#define TIMG_WDT_STG1_RESET_CPU         0x10000000
#define TIMG_WDT_STG1_RESET_SYS         0x18000000
// Stage 2 configuration.
#define TIMG_WDT_STG2_OFF               0x00000000
#define TIMG_WDT_STG2_INT               0x02000000
#define TIMG_WDT_STG2_RESET_CPU         0x04000000
#define TIMG_WDT_STG2_RESET_SYS         0x06000000
// Stage 3 configuration.
#define TIMG_WDT_STG3_OFF               0x00000000
#define TIMG_WDT_STG3_INT               0x00800000
#define TIMG_WDT_STG3_RESET_CPU         0x01000000
#define TIMG_WDT_STG3_RESET_SYS         0x01800000
// edge type interrupt will occur at the tout of a stg conf to generate an int.
#define TIMG_WDT_EDGE_INT_EN            0x00400000
// level type int will occur at the tout of a stg conf to generate an interrupt.
#define TIMG_WDT_LEVEL_INT_EN           0x00200000
// CPU reset signal length selection.
#define TIMG_WDT_CPU_RESET_LENGTH100NS  0x00000000
#define TIMG_WDT_CPU_RESET_LENGTH200NS  0x00040000
#define TIMG_WDT_CPU_RESET_LENGTH300NS  0x00080000
#define TIMG_WDT_CPU_RESET_LENGTH400NS  0x000c0000
#define TIMG_WDT_CPU_RESET_LENGTH500NS  0x00100000
#define TIMG_WDT_CPU_RESET_LENGTH800NS  0x00140000
#define TIMG_WDT_CPU_RESET_LENGTH1P6US  0x00180000
#define TIMG_WDT_CPU_RESET_LENGTH3P2US  0x001c0000
// System reset signal length selection.
#define TIMG_WDT_SYS_RESET_LENGTH100NS  0x00000000
#define TIMG_WDT_SYS_RESET_LENGTH200NS  0x00008000
#define TIMG_WDT_SYS_RESET_LENGTH300NS  0x00010000
#define TIMG_WDT_SYS_RESET_LENGTH400NS  0x00018000
#define TIMG_WDT_SYS_RESET_LENGTH500NS  0x00020000
#define TIMG_WDT_SYS_RESET_LENGTH800NS  0x00028000
#define TIMG_WDT_SYS_RESET_LENGTH1P6US  0x00030000
#define TIMG_WDT_SYS_RESET_LENGTH3P2US  0x00038000
// When set, Flash boot protection is enabled. (R/W)
#define TIMG_WDT_FLASHBOOT_MOD_EN       0x00004000


/* Watchdog timer prescaler register */
#define TIMG0_Tx_WDTCONFIG1_REG     MMIO32(TIMG0_BASE + 0x4c)
#define TIMG1_Tx_WDTCONFIG1_REG     MMIO32(TIMG1_BASE + 0x4c)
// MWDT clock prescale value.
// TODO: formula

/* Watchdog timer stage n timeout value */
#define TIMG0_Tx_WDTCONFIG2_REG     MMIO32(TIMG0_BASE + 0x50)
#define TIMG1_Tx_WDTCONFIG2_REG     MMIO32(TIMG1_BASE + 0x50)
#define TIMG0_Tx_WDTCONFIG3_REG     MMIO32(TIMG0_BASE + 0x54)
#define TIMG1_Tx_WDTCONFIG3_REG     MMIO32(TIMG1_BASE + 0x54)
#define TIMG0_Tx_WDTCONFIG4_REG     MMIO32(TIMG0_BASE + 0x58)
#define TIMG1_Tx_WDTCONFIG4_REG     MMIO32(TIMG1_BASE + 0x58)
#define TIMG0_Tx_WDTCONFIG5_REG     MMIO32(TIMG0_BASE + 0x5c)
#define TIMG1_Tx_WDTCONFIG5_REG     MMIO32(TIMG1_BASE + 0x5c)
// Stage 3 timeout value, in MWDT clock cycles. (R/W)

/* Write to feed the watchdog timer */
#define TIMG0_Tx_WDTFEED_REG        MMIO32(TIMG0_BASE + 0x60)
#define TIMG1_Tx_WDTFEED_REG        MMIO32(TIMG1_BASE + 0x60)
// Write any value to feed the MWDT. (WO)

/* Watchdog write protect register */
#define TIMG0_Tx_WDTWPROTECT_REG    MMIO32(TIMG0_BASE + 0x64)
#define TIMG1_Tx_WDTWPROTECT_REG    MMIO32(TIMG1_BASE + 0x64)
// If the reg contains a diff val than its reset val, write protection is enabled.

/* RTC calibration configuration register */
#define TIMG0_RTCCALICFG_REG        MMIO32(TIMG0_BASE + 0x68)
#define TIMG1_RTCCALICFG_REG        MMIO32(TIMG1_BASE + 0x68)
// Reserved. (R/W)
#define TIMG_RTC_CALI_START_CYCLING             0x00001000
// Used to select the clock to be calibrated.
#define TIMG_RTC_CALI_CLK_SEL_RC_SLOW_CLK       0x00000000
#define TIMG_RTC_CALI_CLK_SEL_RC_FAST_DIV_CLK   0x00002000
#define TIMG_RTC_CALI_CLK_SEL_XTAL32K_CLK       0x00004000
// Set this bit to mark the completion of calibration. (RO)
#define TIMG_RTC_CALI_RDY                       0x00008000
// Calibration time, in cycles of the clock to be calibrated. (R/W)
#define TIMG_RTC_CALI_MAX_MSK                   0x7fff0000
#define TIMG_RTC_CALI_MAX_SFT                   16
#define TIMG_RTC_CALI_MAX_SET(n) ((n<<TIMG_RTC_CALI_MAX_SFT)&TIMG_RTC_CALI_MAX_MSK)
// Set this bit to starts calibration. (R/W)
#define TIMG_RTC_CALI_START                     0x80000000

/* RTC calibration configuration register 1 */
#define TIMG0_RTCCALICFG1_REG       MMIO32(TIMG0_BASE + 0x6c)
#define TIMG1_RTCCALICFG1_REG       MMIO32(TIMG1_BASE + 0x6c)
// Cal val when cycl of clk to be cal reach TIMGn_RTC_CALI_MAX, in unit of XTAL_CLK

/* Interrupt enable bits */
#define TIMG0_Tx_INT_ENA_REG        MMIO32(TIMG0_BASE + 0x98)
#define TIMG1_Tx_INT_ENA_REG        MMIO32(TIMG1_BASE + 0x98)
// The interrupt enable bit for the TIMGn_Tx_INT_WDT_INT interrupt. (R/W)
#define TIMG_INT_WDT_INT_ENA    0x4
// The interrupt enable bit for the TIMGn_Tx_INT_T1_INT interrupt. (R/W)
#define TIMG_INT_T1_INT_ENA     0x2
// The interrupt enable bit for the TIMGn_Tx_INT_T0_INT interrupt. (R/W)
#define TIMG_INT_T0_INT_ENA     0x1

/* Raw interrupt status */
#define TIMG0_Tx_INT_RAW_REG        MMIO32(TIMG0_BASE + 0x9c)
#define TIMG1_Tx_INT_RAW_REG        MMIO32(TIMG1_BASE + 0x9c)
// The raw interrupt status bit for the TIMGn_Tx_INT_WDT_INT interrupt. (RO)
#define TIMG_INT_WDT_INT_RAW    0x4
// The raw interrupt status bit for the TIMGn_Tx_INT_T1_INT interrupt. (RO)
#define TIMG_INT_T1_INT_RAW     0x2
// The raw interrupt status bit for the TIMGn_Tx_INT_T0_INT interrupt. (RO)
#define TIMG_INT_T0_INT_RAW     0x1

/* Masked interrupt status */
#define TIMG0_Tx_INT_ST_REG         MMIO32(TIMG0_BASE + 0xa0)
#define TIMG1_Tx_INT_ST_REG         MMIO32(TIMG1_BASE + 0xa0)
// The masked interrupt status bit for the TIMGn_Tx_INT_WDT_INT interrupt. (RO)
#define TIMG_INT_WDT_INT_ST    0x4
// The masked interrupt status bit for the TIMGn_Tx_INT_T1_INT interrupt. (RO)
#define TIMG_INT_T1_INT_ST     0x2
// The masked interrupt status bit for the TIMGn_Tx_INT_T0_INT interrupt. (RO)
#define TIMG_INT_T0_INT_ST     0x1

/* Interrupt clear bits */
#define TIMG0_Tx_INT_CLR_REG        MMIO32(TIMG0_BASE + 0xa4)
#define TIMG1_Tx_INT_CLR_REG        MMIO32(TIMG1_BASE + 0xa4)
// Set this bit to clear the TIMGn_Tx_INT_WDT_INT interrupt. (WO)
#define TIMG_INT_WDT_INT_CLR    0x4
// Set this bit to clear the TIMGn_Tx_INT_T1_INT interrupt. (WO)
#define TIMG_INT_T1_INT_CLR     0x2
// Set this bit to clear the TIMGn_Tx_INT_T0_INT interrupt. (WO)
#define TIMG_INT_T0_INT_CLR     0x1

#endif
