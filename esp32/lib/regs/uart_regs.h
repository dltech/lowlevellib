#ifndef H_UART_REGS
#define H_UART_REGS
/*
 * ESP32 alternative library, UART Controller registers.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Configuration register 0 */
#define UART0_CONF0_REG             MMIO32(UART0_BASE + 0x20)
#define UART1_CONF0_REG             MMIO32(UART1_BASE + 0x20)
#define UART2_CONF0_REG             MMIO32(UART2_BASE + 0x20)
// This register is used to select the clock
#define UART_TICK_REF_ALWAYS_ON 0x8000000
// Set this bit to invert the level of the UART DTR signal. (R/W)
#define UART_DTR_INV            0x1000000
// Set this bit to invert the level of the UART RTS signal. (R/W)
#define UART_RTS_INV            0x0800000
// Set this bit to invert the level of the UART TxD signal. (R/W)
#define UART_TXD_INV            0x0400000
// Set this bit to invert the level of the UART DSR signal. (R/W)
#define UART_DSR_INV            0x0200000
// Set this bit to invert the level of the UART CTS signal. (R/W)
#define UART_CTS_INV            0x0100000
// Set this bit to invert the level of the UART Rxd signal. (R/W)
#define UART_RXD_INV            0x0080000
// Set this bit to reset the UART transmit-FIFO.
#define UART_TXFIFO_RST         0x0040000
// Set this bit to reset the UART receive-FIFO.
#define UART_RXFIFO_RST         0x0020000
// Set this bit to enable the IrDA protocol. (R/W)
#define UART_IRDA_EN            0x0010000
// Set this bit to enable the flow control function for the transmitter. (R/W)
#define UART_TX_FLOW_EN         0x0008000
// Set this bit to enable the UART loopback test mode. (R/W)
#define UART_LOOPBACK           0x0004000
// Set this bit to invert the level of the IrDA receiver. (R/W)
#define UART_IRDA_RX_INV        0x0002000
// Set this bit to invert the level of the IrDA transmitter. (R/W)
#define UART_IRDA_TX_INV        0x0001000
// 1: The IrDA transmitter’s 11th bit is the same as its 10th bit;
#define UART_IRDA_WCTL          0x0000800
// This is the start enable bit of the IrDA transmitter. (R/W)
#define UART_IRDA_TX_EN         0x0000400
// Set this bit to enable the IrDA loopback mode. (R/W)
#define UART_IRDA_DPLX          0x0000200
// enable the tx to send NULL, when the process of sending data is completed. (R/W)
#define UART_TXD_BRK            0x0000100
// configure the software DTR signal used in software flow control. (R/W)
#define UART_SW_DTR             0x0000080
// This bit is used in hardware flow control when UART_RX_FLOW_EN is 0.
#define UART_SW_RTS             0x0000040
// This register is used to set the length of the stop bit.
#define UART_STOP_BIT_NUM1BIT   0x0000010
#define UART_STOP_BIT_NUM1P5BIT 0x0000020
#define UART_STOP_BIT_NUM2BIT   0x0000030
// This register is used to set the length of data
#define UART_BIT_NUM5BIT        0x0000000
#define UART_BIT_NUM6BIT        0x0000004
#define UART_BIT_NUM7BIT        0x0000008
#define UART_BIT_NUM8BIT        0x000000c
// Set this bit to enable the UART parity check. (R/W)
#define UART_PARITY_EN          0x0000002
// This register is used to configure the parity check mode
#define UART_PARITY             0x0000001

/* Configuration register 1 */
#define UART0_CONF1_REG             MMIO32(UART0_BASE + 0x24)
#define UART1_CONF1_REG             MMIO32(UART1_BASE + 0x24)
#define UART2_CONF1_REG             MMIO32(UART2_BASE + 0x24)
// This is the enable bit for the UART receive-timeout function. (R/W)
#define UART_RX_TOUT_EN                 0x80000000
// UART receiver’s timeout value when receiving a byte.
#define UART_RX_TOUT_THRHD_MSK          0x7f000000
#define UART_RX_TOUT_THRHD_FSK          24
#define UART_RX_TOUT_THRHD_SET(n) ((n<<UART_RX_TOUT_THRHD_FSK)&UART_RX_TOUT_THRHD_MSK)
// This is the flow enable bit of the UART receiver;
#define UART_RX_FLOW_EN                 0x00800000
// When UART_RX_FLOW_EN is 1 and the rx gets more data than its th, the rx produces
// an rtsn_out sig that tells the tx to stop.
#define UART_RX_FLOW_THRHD_MSK          0x007f0000
#define UART_RX_FLOW_THRHD_FSK          16
#define UART_RX_FLOW_THRHD_SET(n) ((n<<UART_RX_FLOW_THRHD_FSK)&UART_RX_FLOW_THRHD_MSK)
// When the data amount in tx-FIFO is less than its th, it will produce a int.
#define UART_TXFIFO_EMPTY_THRHD_MSK     0x00007f00
#define UART_TXFIFO_EMPTY_THRHD_FSK     8
#define UART_TXFIFO_EMPTY_THRHD_SET(n)  ((n<<UART_TXFIFO_EMPTY_THRHD_FSK)&UART_TXFIFO_EMPTY_THRHD_MSK)
// When the rx gets more data than its th value, the rx will produce an interrupt.
#define UART_RXFIFO_FULL_THRHD_MSK      0x0000007f
#define UART_RXFIFO_FULL_THRHD_SET(n)   (n&UART_RXFIFO_FULL_THRHD_MSK)

/* Clock divider configuration */
#define UART0_CLKDIV_REG            MMIO32(UART0_BASE + 0x14)
#define UART1_CLKDIV_REG            MMIO32(UART1_BASE + 0x14)
#define UART2_CLKDIV_REG            MMIO32(UART2_BASE + 0x14)
// The decimal part of the frequency divider factor. (R/W)
#define UART_CLKDIV_FRAG_MSK    0xf00000
#define UART_CLKDIV_FRAG_SFT    20
#define UART_CLKDIV_FRAG_SET(n) ((n<<UART_CLKDIV_FRAG_SFT)&UART_CLKDIV_FRAG_MSK)
// The integral part of the frequency divider factor. (R/W)
#define UART_CLKDIV_MSK         0x0fffff
#define UART_CLKDIV_SET(n)      (n&UART_CLKDIV_MSK)

/* Software flow-control configuration */
#define UART0_FLOW_CONF_REG         MMIO32(UART0_BASE + 0x34)
#define UART1_FLOW_CONF_REG         MMIO32(UART1_BASE + 0x34)
#define UART2_FLOW_CONF_REG         MMIO32(UART2_BASE + 0x34)
// Hardware auto-clear; set to 1 to send Xoff char. (R/W)
#define UART_SEND_XOFF      0x20
// Hardware auto-clear; set to 1 to send Xon char. (R/W)
#define UART_SEND_XON       0x10
// Set this bit to set the internal CTSn and stop the transmitter from sending data.
#define UART_FORCE_XOFF     0x08
// clear the internal CTSn and enable the transmitter to continue sending data.
#define UART_FORCE_XON      0x04
// Set this bit to remove the flow-control char from the received data. (R/W)
#define UART_XONOFF_DEL     0x02
// Set this bit to enable software flow control.
#define UART_SW_FLOW_CON_EN 0x01

/* Software flow-control character configuration */
#define UART0_SWFC_CONF_REG         MMIO32(UART0_BASE + 0x3c)
#define UART1_SWFC_CONF_REG         MMIO32(UART1_BASE + 0x3c)
#define UART2_SWFC_CONF_REG         MMIO32(UART2_BASE + 0x3c)
// This register stores the Xoff flow control char. (R/W)
#define UART_XOFF_CHAR_MSK          0xff000000
#define UART_XOFF_CHAR_SFT          24
#define UART_XOFF_CHAR_SET(n)       ((n<<UART_XOFF_CHAR_SFT)&UART_XOFF_CHAR_MSK)
// This register stores the Xon flow control char. (R/W)
#define UART_XON_CHAR_MSK           0x00ff0000
#define UART_XON_CHAR_SFT           16
#define UART_XON_CHAR_SET(n)        ((n<<UART_XON_CHAR_SFT)&UART_XON_CHAR_MSK)
// When the data amount in rx-FIFO is more than what this reg indicates, it will
// send an Xoff char, with uart_sw_flow_con_en set to 1. (R/W)
#define UART_XOFF_THRESHOLD_MSK     0x0000ff00
#define UART_XOFF_THRESHOLD_SFT     8
#define UART_XOFF_THRESHOLD_SET(n)  ((n<<UART_XOFF_THRESHOLD_SFT)&UART_XOFF_THRESHOLD_MSK)
// When the data amount in rx-FIFO is less than what this reg indicates, it will
// send an Xon char, with uart_sw_flow_con_en set to 1. (R/W)
#define UART_XON_THRESHOLD_MSK      0x000000ff
#define UART_XON_THRESHOLD_SET(n)   (n&UART_XON_THRESHOLD_MSK)

/* Sleep-mode configuration */
#define UART0_SLEEP_CONF_REG        MMIO32(UART0_BASE + 0x38)
#define UART1_SLEEP_CONF_REG        MMIO32(UART1_BASE + 0x38)
#define UART2_SLEEP_CONF_REG        MMIO32(UART2_BASE + 0x38)
// When the num of pos edges of RxD sig is larger than or eq to
// (UART_ACTIVE_THRESHOLD+2), the system emerges from Light-sleep mode
#define UART_ACTIVE_THRESHOLD_MSK   0x3ff

/* Frame-end idle configuration */
#define UART0_IDLE_CONF_REG         MMIO32(UART0_BASE + 0x40)
#define UART1_IDLE_CONF_REG         MMIO32(UART1_BASE + 0x40)
#define UART2_IDLE_CONF_REG         MMIO32(UART2_BASE + 0x40)
// the number of zeros (0) sent, after the process of sending data is completed.
#define UART_TX_BRK_NUM_MSK         0xff00000
#define UART_TX_BRK_NUM_SFT         20
#define UART_TX_BRK_NUM_SET(n)      ((n<<UART_TX_BRK_NUM_SFT)&UART_TX_BRK_NUM_MSK)
// This register is used to configure the duration between transfers. (R/W)
#define UART_TX_IDLE_NUM_MSK        0x00ffc00
#define UART_TX_IDLE_NUM_SFT        10
#define UART_TX_IDLE_NUM_SET(n)     ((n<<UART_TX_IDLE_NUM_SFT)&UART_TX_IDLE_NUM_MSK)
// When the rx takes more time to rx data than what this reg, it will produce a
// frame-end signal. (R/W)
#define UART_RX_IDLE_THRHD_MSK      0x00003ff
#define UART_RX_IDLE_THRHD_SET(n)   (n&UART_RX_IDLE_THRHD_MSK)

/* RS485 mode configuration */
#define UART0_RS485_CONF_REG        MMIO32(UART0_BASE + 0x44)
#define UART1_RS485_CONF_REG        MMIO32(UART1_BASE + 0x44)
#define UART2_RS485_CONF_REG        MMIO32(UART2_BASE + 0x44)
// This register is used to delay the transmitter’s internal data signal. (R/W)
#define UART_RS485_TX_DLY_NUM_MSK       0x3c0
#define UART_RS485_TX_DLY_NUM_SFT       6
#define UART_RS485_TX_DLY_NUM_SET(n)  ((n<<UART_RS485_TX_DLY_NUM_SFT)&UART_RS485_TX_DLY_NUM_MSK)
// This register is used to delay the receiver’s internal data signal. (R/W)
#define UART_RS485_RX_DLY_NUM           0x020
// 1: enable the RS-485 tx to send data, when the RS-485 rx line is busy;
#define UART_RS485RXBY_TX_EN            0x010
// enable the tx’s output signal loop back to the receiver’s input signal. (R/W)
#define UART_RS485TX_RX_EN              0x008
// Set this bit to delay the STOP bit by 1 bit. (R/W)
#define UART_DL1_EN                     0x004
// Set this bit to delay the STOP bit by 1 bit after DL1. (R/W)
#define UART_DL0_EN                     0x002
// Set this bit to choose the RS-485 mode. (R/W)
#define UART_RS485_EN                   0x001

/* UART status register */
#define UART0_STATUS_REG            MMIO32(UART0_BASE + 0x1c)
#define UART1_STATUS_REG            MMIO32(UART1_BASE + 0x1c)
#define UART2_STATUS_REG            MMIO32(UART2_BASE + 0x1c)
// This bit represents the level of the internal UART RxD signal. (RO)
#define UART_TXD                0x80000000
// This bit corresponds to the level of the internal UART CTS signal. (RO)
#define UART_RTSN               0x40000000
// This bit corresponds to the level of the internal UAR DSR signal. (RO)
#define UART_DTRN               0x20000000
// This register stores the state of the transmitter’s finite state machine.
#define UART_ST_UTX_OUT_TX_IDLE 0x00000000
#define UART_ST_UTX_OUT_TX_STRT 0x01000000
#define UART_ST_UTX_OUT_TX_DAT0 0x02000000
#define UART_ST_UTX_OUT_TX_DAT1 0x03000000
#define UART_ST_UTX_OUT_TX_DAT2 0x04000000
#define UART_ST_UTX_OUT_TX_DAT3 0x05000000
#define UART_ST_UTX_OUT_TX_DAT4 0x06000000
#define UART_ST_UTX_OUT_TX_DAT5 0x07000000
#define UART_ST_UTX_OUT_TX_DAT6 0x08000000
#define UART_ST_UTX_OUT_TX_DAT7 0x09000000
#define UART_ST_UTX_OUT_TX_PRTY 0x0a000000
#define UART_ST_UTX_OUT_TX_STP1 0x0b000000
#define UART_ST_UTX_OUT_TX_STP2 0x0c000000
#define UART_ST_UTX_OUT_TX_DL0  0x0d000000
#define UART_ST_UTX_OUT_TX_DL1  0x0e000000
// number of bytes of valid data in transmit FIFO.
#define UART_TXFIFO_CNT_MSK     0x00ff0000
#define UART_TXFIFO_CNT_SFT     16
#define UART_TXFIFO_CNT_SET(n)  ((n<<UART_TXFIFO_CNT_SFT)&UART_TXFIFO_CNT_MSK)
// This bit corresponds to the level of the internal UART RxD signal. (RO)
#define UART_RXD                0x00008000
// This bit corresponds to the level of the internal UART CTS signal. (RO)
#define UART_CTSN               0x00004000
// This bit corresponds to the level of the internal UAR DSR signal. (RO)
#define UART_DSRN               0x00002000
// This register stores the value of the receiver’s finite state machine.
#define UART_ST_URX_OUT_RX_IDLE 0x00000000
#define UART_ST_URX_OUT_RX_STRT 0x00000100
#define UART_ST_URX_OUT_RX_DAT0 0x00000200
#define UART_ST_URX_OUT_RX_DAT1 0x00000300
#define UART_ST_URX_OUT_RX_DAT2 0x00000400
#define UART_ST_URX_OUT_RX_DAT3 0x00000500
#define UART_ST_URX_OUT_RX_DAT4 0x00000600
#define UART_ST_URX_OUT_RX_DAT5 0x00000700
#define UART_ST_URX_OUT_RX_DAT6 0x00000800
#define UART_ST_URX_OUT_RX_DAT7 0x00000900
#define UART_ST_URX_OUT_RX_PRTY 0x00000a00
#define UART_ST_URX_OUT_RX_STP1 0x00000b00
#define UART_ST_URX_OUT_RX_STP2 0x00000c00
#define UART_ST_URX_OUT_RX_DL1  0x00000d00
// number of bytes of valid data in the receive FIFO.
#define UART_RXFIFO_CNT_MSK     0x000000ff
#define UART_RXFIFO_CNT_SET(n)  (n&UART_RXFIFO_CNT_MSK)

/* TX FIFO write and read offset address */
#define UART0_MEM_TX_STATUS_REG     MMIO32(UART0_BASE + 0x5c)
#define UART1_MEM_TX_STATUS_REG     MMIO32(UART1_BASE + 0x5c)
#define UART2_MEM_TX_STATUS_REG     MMIO32(UART2_BASE + 0x5c)
// Represents the offset address to write TX FIFO. (RO)
#define UART_MEM_TX_WR_ADDR_MSK     0xffe000
#define UART_MEM_TX_WR_ADDR_SFT     13
#define UART_MEM_TX_WR_ADDR_SET(n) ((n<<UART_MEM_TX_WR_ADDR_SFT)&UART_MEM_TX_WR_ADDR_MSK)
// Represents the offset address to read TX FIFO. (RO)
#define UART_MEM_TX_RD_ADDR_MSK     0x001ffc
#define UART_MEM_TX_RD_ADDR_SFT     2
#define UART_MEM_TX_RD_ADDR_SET(n) ((n<<UART_MEM_TX_RD_ADDR_SFT)&UART_MEM_TX_RD_ADDR_MSK)

/* RX FIFO write and read offset address */
#define UART0_MEM_RX_STATUS_REG     MMIO32(UART0_BASE + 0x60)
#define UART1_MEM_RX_STATUS_REG     MMIO32(UART1_BASE + 0x60)
#define UART2_MEM_RX_STATUS_REG     MMIO32(UART2_BASE + 0x60)
// Represents the offset address to read RX FIFO. (RO)
#define UART_MEM_RX_WR_ADDR_MSK     0xffe000
#define UART_MEM_RX_WR_ADDR_SFT     13
#define UART_MEM_RX_WR_ADDR_SET(n) ((n<<UART_MEM_RX_WR_ADDR_SFT)&UART_MEM_RX_WR_ADDR_MSK)
// Represents the offset address to write RX FIFO. (RO)
#define UART_MEM_RX_RD_ADDR_MSK     0x001ffc
#define UART_MEM_RX_RD_ADDR_SFT     2
#define UART_MEM_RX_RD_ADDR_SET(n) ((n<<UART_MEM_RX_RD_ADDR_SFT)&UART_MEM_RX_RD_ADDR_MSK)

/* Autobaud configuration register */
#define UART0_AUTOBAUD_REG          MMIO32(UART0_BASE + 0x18)
#define UART1_AUTOBAUD_REG          MMIO32(UART1_BASE + 0x18)
#define UART2_AUTOBAUD_REG          MMIO32(UART2_BASE + 0x18)
// When the input pulse width is lower than this value, the pulse is ignored.
#define UART_GLITCH_FILT_MSK    0xff00
#define UART_GLITCH_FILT_SFT    8
#define UART_GLITCH_FILT_SET(n) ((n<<UART_GLITCH_FILT_SFT)&UART_GLITCH_FILT_MSK)
// This is the enable bit for autobaud. (R/W)
#define UART_AUTOBAUD_EN        0x0001

/* Autobaud minimum low pulse duration register */
#define UART0_LOWPULSE_REG          MMIO32(UART0_BASE + 0x28)
#define UART1_LOWPULSE_REG          MMIO32(UART1_BASE + 0x28)
#define UART2_LOWPULSE_REG          MMIO32(UART2_BASE + 0x28)
// minimum duration of the low-level pulse. It is used in the baud rate det process.
#define UART_LOWPULSE_MIN_CNT_MSK   0xfffff

/* Autobaud minimum high pulse duration register */
#define UART0_HIGHPULSE_REG         MMIO32(UART0_BASE + 0x2c)
#define UART1_HIGHPULSE_REG         MMIO32(UART1_BASE + 0x2c)
#define UART2_HIGHPULSE_REG         MMIO32(UART2_BASE + 0x2c)
// minimum duration of the high level pulse. It is used in baud rate det process.

/* Autobaud high pulse register */
#define UART0_POSPULSE_REG          MMIO32(UART0_BASE + 0x68)
#define UART1_POSPULSE_REG          MMIO32(UART1_BASE + 0x68)
#define UART2_POSPULSE_REG          MMIO32(UART2_BASE + 0x68)
// This register stores the count of RxD positive edges.
#define UART_POSEDGE_MIN_CNT_MSK    0xffffff

/* Autobaud low pulse register */
#define UART0_NEGPULSE_REG          MMIO32(UART0_BASE + 0x6c)
#define UART1_NEGPULSE_REG          MMIO32(UART1_BASE + 0x6c)
#define UART2_NEGPULSE_REG          MMIO32(UART2_BASE + 0x6c)
// This register stores the count of RxD negative edges.
#define UART_NEGEDGE_MIN_CNT_MSK    0xffffff

/* Autobaud edge change count register */
#define UART0_RXD_CNT_REG           MMIO32(UART0_BASE + 0x30)
#define UART1_RXD_CNT_REG           MMIO32(UART1_BASE + 0x30)
#define UART2_RXD_CNT_REG           MMIO32(UART2_BASE + 0x30)
// count of the RxD edge change. It is used in the baud rate detection process. (RO)

/* Pre-sequence timing configuration */
#define UART0_AT_CMD_PRECNT_REG     MMIO32(UART0_BASE + 0x48)
#define UART1_AT_CMD_PRECNT_REG     MMIO32(UART1_BASE + 0x48)
#define UART2_AT_CMD_PRECNT_REG     MMIO32(UART2_BASE + 0x48)
// idle-time duration before the first at_cmd is received by the receiver.
#define UART_PRE_IDLE_NUM_MSK   0xffffff

/* Post-sequence timing configuration */
#define UART0_AT_CMD_POSTCNT_REG    MMIO32(UART0_BASE + 0x4c)
#define UART1_AT_CMD_POSTCNT_REG    MMIO32(UART1_BASE + 0x4c)
#define UART2_AT_CMD_POSTCNT_REG    MMIO32(UART2_BASE + 0x4c)
// duration between the last at_cmd and the next data.
#define UART_POST_IDLE_NUM_MSK  0xffffff

/* Timeout configuration */
#define UART0_AT_CMD_GAPTOUT_REG    MMIO32(UART0_BASE + 0x50)
#define UART1_AT_CMD_GAPTOUT_REG    MMIO32(UART1_BASE + 0x50)
#define UART2_AT_CMD_GAPTOUT_REG    MMIO32(UART2_BASE + 0x50)
// interval between the at_cmd chars.
#define UART_RX_GAP_TOUT_MSK    0xffffff

/* AT escape sequence detection configuration */
#define UART0_AT_CMD_CHAR_REG       MMIO32(UART0_BASE + 0x54)
#define UART1_AT_CMD_CHAR_REG       MMIO32(UART1_BASE + 0x54)
#define UART2_AT_CMD_CHAR_REG       MMIO32(UART2_BASE + 0x54)
// number of continuous at_cmd chars received by the receiver. (R/W)
#define UART_CHAR_NUM_MSK       0xff00
#define UART_CHAR_NUM_SFT       8
#define UART_CHAR_NUM_SET(n)    ((n<<UART_CHAR_NUM_SFT)&UART_CHAR_NUM_MSK)
// This register is used to configure the content of an at_cmd char. (R/W)
#define UART_AT_CMD_CHAR_MSK    0x00ff
#define UART_AT_CMD_CHAR_SET(n) (n&UART_AT_CMD_CHAR_MSK)

/* FIFO data register */
#define UART0_FIFO_REG              MMIO32(UART0_BASE + 0x00)
#define UART1_FIFO_REG              MMIO32(UART1_BASE + 0x00)
#define UART2_FIFO_REG              MMIO32(UART2_BASE + 0x00)
// UART accesses FIFO via this register. (R/W)
#define UART_RXFIFO_RD_BYTE_MSK 0xff

/* UART threshold and allocation configuration */
#define UART0_MEM_CONF_REG          MMIO32(UART0_BASE + 0x58)
#define UART1_MEM_CONF_REG          MMIO32(UART1_BASE + 0x58)
#define UART2_MEM_CONF_REG          MMIO32(UART2_BASE + 0x58)
// Refer to the description of TXFIFO_EMPTY_THRHD. (R/W)
#define UART_TX_MEM_EMPTY_THRHD_MSK     0x70000000
#define UART_TX_MEM_EMPTY_THRHD_SFT     28
#define UART_TX_MEM_EMPTY_THRHD_SET(n) ((n<<UART_TX_MEM_EMPTY_THRHD_SFT)&UART_TX_MEM_EMPTY_THRHD_MSK)
// Refer to the description of RXFIFO_FULL_THRHD. (R/W)
#define UART_RX_MEM_FULL_THRHD_MSK      0x0e000000
#define UART_RX_MEM_FULL_THRHD_SFT      25
#define UART_RX_MEM_FULL_THRHD_SET(n) ((n<<UART_RX_MEM_FULL_THRHD_SFT)&UART_RX_MEM_FULL_THRHD_MSK)
// Refer to the description of UART_XOFF_THRESHOLD. (R/W)
#define UART_XOFF_THRESHOLD_H2_MSK      0x01800000
#define UART_XOFF_THRESHOLD_H2_SFT      23
#define UART_XOFF_THRESHOLD_H2_SET(n) ((n<<UART_XOFF_THRESHOLD_H2_SFT)&UART_XOFF_THRESHOLD_H2_MSK)
// Refer to the description of UART_XON_THRESHOLD. (R/W)
#define UART_XON_THRESHOLD_H2_MSK       0x00600000
#define UART_XON_THRESHOLD_H2_SFT       21
#define UART_XON_THRESHOLD_H2_SET(n) ((n<<UART_XON_THRESHOLD_H2_SFT)&UART_XON_THRESHOLD_H2_MSK)
// Refer to the description of RX_TOUT_THRHD. (R/W)
#define UART_RX_TOUT_THRHD_H3_MSK       0x001c0000
#define UART_RX_TOUT_THRHD_H3_SFT       18
#define UART_RX_TOUT_THRHD_H3_SET(n) ((n<<UART_RX_TOUT_THRHD_H3_SFT)&UART_RX_TOUT_THRHD_H3_MSK)
// Refer to the description of RX_FLOW_THRHD. (R/W)
#define UART_RX_FLOW_THRHD_H3_MSK       0x00038000
#define UART_RX_FLOW_THRHD_H3_SFT       15
#define UART_RX_FLOW_THRHD_H3_SET(n) ((n<<UART_RX_FLOW_THRHD_H3_SFT)&UART_RX_FLOW_THRHD_H3_MSK)
// amount of memory allocated to the transmit-FIFO.
#define UART_TX_SIZE_MSK                0x00000780
#define UART_TX_SIZE_SFT                7
#define UART_TX_SIZE_SET(n) ((n<<UART_TX_SIZE_SFT)&UART_TX_SIZE_MSK)
// amount of memory allocated to the receive-FIFO.
#define UART_RX_SIZE_MSK                0x00000078
#define UART_RX_SIZE_SFT                3
#define UART_RX_SIZE_SET(n) ((n<<UART_RX_SIZE_SFT)&UART_RX_SIZE_MSK)
// Set this bit to power down the memory.
#define UART_MEM_PD                     0x00000001

/* Receive and transmit memory configuration */
#define UART0_MEM_CNT_STATUS_REG    MMIO32(UART0_BASE + 0x64)
#define UART1_MEM_CNT_STATUS_REG    MMIO32(UART1_BASE + 0x64)
#define UART2_MEM_CNT_STATUS_REG    MMIO32(UART2_BASE + 0x64)
// Refer to the description of TXFIFO_CNT. (RO)
#define UART_TX_MEM_CNT_MSK     0x38
#define UART_TX_MEM_CNT_SFT     3
#define UART_TX_MEM_CNT_SET(n)  ((n<<UART_TX_MEM_CNT_SFT)&UART_TX_MEM_CNT_MSK)
// Refer to the description of RXFIFO_CNT. (RO)
#define UART_RX_MEM_CNT_MSK     0x07
#define UART_RX_MEM_CNT_SET(n)  (n&UART_RX_MEM_CNT_MSK)

/* Raw interrupt status */
#define UART0_INT_RAW_REG           MMIO32(UART0_BASE + 0x04)
#define UART1_INT_RAW_REG           MMIO32(UART1_BASE + 0x04)
#define UART2_INT_RAW_REG           MMIO32(UART2_BASE + 0x04)
// The raw interrupt status bit for the UART_AT_CMD_CHAR_DET_INT interrupt. (RO)
#define UART_AT_CMD_CHAR_DET_INT_RAW    0x40000
// The raw interrupt status bit for the UART_RS485_CLASH_INT interrupt. (RO)
#define UART_RS485_CLASH_INT_RAW        0x20000
// The raw interrupt status bit for the UART_RS485_FRM_ERR_INT interrupt. (RO)
#define UART_RS485_FRM_ERR_INT_RAW      0x10000
// The raw interrupt status bit for the UART_RS485_PARITY_ERR_INT interrupt. (RO)
#define UART_RS485_PARITY_ERR_INT_RAW   0x08000
// The raw interrupt status bit for the UART_TX_DONE_INT interrupt. (RO)
#define UART_TX_DONE_INT_RAW            0x04000
// The raw interrupt status bit for the UART_TX_BRK_IDLE_DONE_INT interrupt. (RO)
#define UART_TX_BRK_IDLE_DONE_INT_RAW   0x02000
// The raw interrupt status bit for the UART_TX_BRK_DONE_INT interrupt. (RO)
#define UART_TX_BRK_DONE_INT_RAW        0x01000
// The raw interrupt status bit for the UART_GLITCH_DET_INT interrupt. (RO)
#define UART_GLITCH_DET_INT_RAW         0x00800
// The raw interrupt status bit for the UART_SW_XOFF_INT interrupt. (RO)
#define UART_SW_XOFF_INT_RAW            0x00400
// The raw interrupt status bit for the UART_SW_XON_INT interrupt. (RO)
#define UART_SW_XON_INT_RAW             0x00200
// The raw interrupt status bit for the UART_RXFIFO_TOUT_INT interrupt. (RO)
#define UART_RXFIFO_TOUT_INT_RAW        0x00100
// The raw interrupt status bit for the UART_BRK_DET_INT interrupt. (RO)
#define UART_BRK_DET_INT_RAW            0x00080
// The raw interrupt status bit for the UART_CTS_CHG_INT interrupt. (RO)
#define UART_CTS_CHG_INT_RAW            0x00040
// The raw interrupt status bit for the UART_DSR_CHG_INT interrupt. (RO)
#define UART_DSR_CHG_INT_RAW            0x00020
// The raw interrupt status bit for the UART_RXFIFO_OVF_INT interrupt. (RO)
#define UART_RXFIFO_OVF_INT_RAW         0x00010
// The raw interrupt status bit for the UART_FRM_ERR_INT interrupt. (RO)
#define UART_FRM_ERR_INT_RAW            0x00008
// The raw interrupt status bit for the UART_PARITY_ERR_INT interrupt. (RO)
#define UART_PARITY_ERR_INT_RAW         0x00004
// The raw interrupt status bit for the UART_TXFIFO_EMPTY_INT interrupt. (RO)
#define UART_TXFIFO_EMPTY_INT_RAW       0x00002
// The raw interrupt status bit for the UART_RXFIFO_FULL_INT interrupt. (RO)
#define UART_RXFIFO_FULL_INT_RAW        0x00001

/* Masked interrupt status */
#define UART0_INT_ST_REG            MMIO32(UART0_BASE + 0x08)
#define UART1_INT_ST_REG            MMIO32(UART1_BASE + 0x08)
#define UART2_INT_ST_REG            MMIO32(UART2_BASE + 0x08)
// The masked interrupt status bit for the UART_AT_CMD_CHAR_DET_INT interrupt. (RO)
#define UART_AT_CMD_CHAR_DET_INT_ST    0x40000
// The masked interrupt status bit for the UART_RS485_CLASH_INT interrupt. (RO)
#define UART_RS485_CLASH_INT_ST        0x20000
// The masked interrupt status bit for the UART_RS485_FRM_ERR_INT interrupt. (RO)
#define UART_RS485_FRM_ERR_INT_ST      0x10000
// The masked interrupt status bit for the UART_RS485_PARITY_ERR_INT interrupt. (RO)
#define UART_RS485_PARITY_ERR_INT_ST   0x08000
// The masked interrupt status bit for the UART_TX_DONE_INT interrupt. (RO)
#define UART_TX_DONE_INT_ST            0x04000
// The masked interrupt status bit for the UART_TX_BRK_IDLE_DONE_INT interrupt. (RO)
#define UART_TX_BRK_IDLE_DONE_INT_ST   0x02000
// The masked interrupt status bit for the UART_TX_BRK_DONE_INT interrupt. (RO)
#define UART_TX_BRK_DONE_INT_ST        0x01000
// The masked interrupt status bit for the UART_GLITCH_DET_INT interrupt. (RO)
#define UART_GLITCH_DET_INT_ST         0x00800
// The masked interrupt status bit for the UART_SW_XOFF_INT interrupt. (RO)
#define UART_SW_XOFF_INT_ST            0x00400
// The masked interrupt status bit for the UART_SW_XON_INT interrupt. (RO)
#define UART_SW_XON_INT_ST             0x00200
// The masked interrupt status bit for the UART_RXFIFO_TOUT_INT interrupt. (RO)
#define UART_RXFIFO_TOUT_INT_ST        0x00100
// The masked interrupt status bit for the UART_BRK_DET_INT interrupt. (RO)
#define UART_BRK_DET_INT_ST            0x00080
// The masked interrupt status bit for the UART_CTS_CHG_INT interrupt. (RO)
#define UART_CTS_CHG_INT_ST            0x00040
// The masked interrupt status bit for the UART_DSR_CHG_INT interrupt. (RO)
#define UART_DSR_CHG_INT_ST            0x00020
// The masked interrupt status bit for the UART_RXFIFO_OVF_INT interrupt. (RO)
#define UART_RXFIFO_OVF_INT_ST         0x00010
// The masked interrupt status bit for the UART_FRM_ERR_INT interrupt. (RO)
#define UART_FRM_ERR_INT_ST            0x00008
// The masked interrupt status bit for the UART_PARITY_ERR_INT interrupt. (RO)
#define UART_PARITY_ERR_INT_ST         0x00004
// The masked interrupt status bit for the UART_TXFIFO_EMPTY_INT interrupt. (RO)
#define UART_TXFIFO_EMPTY_INT_ST       0x00002
// The masked interrupt status bit for the UART_RXFIFO_FULL_INT interrupt. (RO)
#define UART_RXFIFO_FULL_INT_ST        0x00001

/* Interrupt enable bits */
#define UART0_INT_ENA_REG           MMIO32(UART0_BASE + 0x0c)
#define UART1_INT_ENA_REG           MMIO32(UART1_BASE + 0x0c)
#define UART2_INT_ENA_REG           MMIO32(UART2_BASE + 0x0c)
// The interrupt enable bit for the UART_AT_CMD_CHAR_DET_INT interrupt. (R/W)
#define UART_AT_CMD_CHAR_DET_INT_ENA    0x40000
// The interrupt enable bit for the UART_RS485_CLASH_INT interrupt. (R/W)
#define UART_RS485_CLASH_INT_ENA        0x20000
// The interrupt enable bit for the UART_RS485_FRM_ERR_INT interrupt. (R/W)
#define UART_RS485_FRM_ERR_INT_ENA      0x10000
// The interrupt enable bit for the UART_RS485_PARITY_ERR_INT interrupt. (R/W)
#define UART_RS485_PARITY_ERR_INT_ENA   0x08000
// The interrupt enable bit for the UART_TX_DONE_INT interrupt. (R/W)
#define UART_TX_DONE_INT_ENA            0x04000
// The interrupt enable bit for the UART_TX_BRK_IDLE_DONE_INT interrupt. (R/W)
#define UART_TX_BRK_IDLE_DONE_INT_ENA   0x02000
// The interrupt enable bit for the UART_TX_BRK_DONE_INT interrupt. (R/W)
#define UART_TX_BRK_DONE_INT_ENA        0x01000
// The interrupt enable bit for the UART_GLITCH_DET_INT interrupt. (R/W)
#define UART_GLITCH_DET_INT_ENA         0x00800
// The interrupt enable bit for the UART_SW_XOFF_INT interrupt. (R/W)
#define UART_SW_XOFF_INT_ENA            0x00400
// The interrupt enable bit for the UART_SW_XON_INT interrupt. (R/W)
#define UART_SW_XON_INT_ENA             0x00200
// The interrupt enable bit for the UART_RXFIFO_TOUT_INT interrupt. (R/W)
#define UART_RXFIFO_TOUT_INT_ENA        0x00100
// The interrupt enable bit for the UART_BRK_DET_INT interrupt. (R/W)
#define UART_BRK_DET_INT_ENA            0x00080
// The interrupt enable bit for the UART_CTS_CHG_INT interrupt. (R/W)
#define UART_CTS_CHG_INT_ENA            0x00040
// The interrupt enable bit for the UART_DSR_CHG_INT interrupt. (R/W)
#define UART_DSR_CHG_INT_ENA            0x00020
// The interrupt enable bit for the UART_RXFIFO_OVF_INT interrupt. (R/W)
#define UART_RXFIFO_OVF_INT_ENA         0x00010
// The interrupt enable bit for the UART_FRM_ERR_INT interrupt. (R/W)
#define UART_FRM_ERR_INT_ENA            0x00008
// The interrupt enable bit for the UART_PARITY_ERR_INT interrupt. (R/W)
#define UART_PARITY_ERR_INT_ENA         0x00004
// The interrupt enable bit for the UART_TXFIFO_EMPTY_INT interrupt. (R/W)
#define UART_TXFIFO_EMPTY_INT_ENA       0x00002
// The interrupt enable bit for the UART_RXFIFO_FULL_INT interrupt. (R/W)
#define UART_RXFIFO_FULL_INT_ENA        0x00001

/* Interrupt clear bits */
#define UART0_INT_CLR_REG           MMIO32(UART0_BASE + 0x10)
#define UART2_INT_CLR_REG           MMIO32(UART1_BASE + 0x10)
#define UART1_INT_CLR_REG           MMIO32(UART2_BASE + 0x10)
// Set this bit to clear the UART_AT_CMD_CHAR_DET_INT interrupt. (WO)
#define UART_AT_CMD_CHAR_DET_INT_CLR    0x40000
// Set this bit to clear the UART_RS485_CLASH_INT interrupt. (WO)
#define UART_RS485_CLASH_INT_CLR        0x20000
// Set this bit to clear the UART_RS485_FRM_ERR_INT interrupt. (WO)
#define UART_RS485_FRM_ERR_INT_CLR      0x10000
// Set this bit to clear the UART_RS485_PARITY_ERR_INT interrupt. (WO)
#define UART_RS485_PARITY_ERR_INT_CLR   0x08000
// Set this bit to clear the UART_TX_DONE_INT interrupt. (WO)
#define UART_TX_DONE_INT_CLR            0x04000
// Set this bit to clear the UART_TX_BRK_IDLE_DONE_INT interrupt. (WO)
#define UART_TX_BRK_IDLE_DONE_INT_CLR   0x02000
// Set this bit to clear the UART_TX_BRK_DONE_INT interrupt. (WO)
#define UART_TX_BRK_DONE_INT_CLR        0x01000
// Set this bit to clear the UART_GLITCH_DET_INT interrupt. (WO)
#define UART_GLITCH_DET_INT_CLR         0x00800
// Set this bit to clear the UART_SW_XOFF_INT interrupt. (WO)
#define UART_SW_XOFF_INT_CLR            0x00400
// Set this bit to clear the UART_SW_XON_INT interrupt. (WO)
#define UART_SW_XON_INT_CLR             0x00200
// Set this bit to clear the UART_RXFIFO_TOUT_INT interrupt. (WO)
#define UART_RXFIFO_TOUT_INT_CLR        0x00100
// Set this bit to clear the UART_BRK_DET_INT interrupt. (WO)
#define UART_BRK_DET_INT_CLR            0x00080
// Set this bit to clear the UART_CTS_CHG_INT interrupt. (WO)
#define UART_CTS_CHG_INT_CLR            0x00040
// Set this bit to clear the UART_DSR_CHG_INT interrupt. (WO)
#define UART_DSR_CHG_INT_CLR            0x00020
// Set this bit to clear the UART_RXFIFO_OVF_INT interrupt. (WO)
#define UART_RXFIFO_OVF_INT_CLR         0x00010
// Set this bit to clear the UART_FRM_ERR_INT interrupt. (WO)
#define UART_FRM_ERR_INT_CLR            0x00008
// Set this bit to clear the UART_PARITY_ERR_INT interrupt. (WO)
#define UART_PARITY_ERR_INT_CLR         0x00004
// Set this bit to clear the UART_TXFIFO_EMPTY_INT interrupt. (WO)
#define UART_TXFIFO_EMPTY_INT_CLR       0x00002
// Set this bit to clear the UART_RXFIFO_FULL_INT interrupt. (WO)
#define UART_RXFIFO_FULL_INT_CLR        0x00001

/* UART and frame separation config */
#define UHCI0_CONF0_REG                     MMIO32(UDMA0_BASE + 0x00)
#define UHCI1_CONF0_REG                     MMIO32(UDMA1_BASE + 0x00)
// Reserved. Please initialize it to 0. (R/W)
#define UHCI_ENCODE_CRC_EN      0x200000
// Reserved. Please initialize it to 0. (R/W)
#define UHCI_LEN_EOF_EN         0x100000
// Reserved. Please initialize it to 0. (R/W)
#define UHCI_UART_IDLE_EOF_EN   0x080000
// Reserved. Please initialize it to 0. (R/W)
#define UHCI_CRC_REC_EN         0x040000
// Reserved. Please initialize it to 0. (R/W)
#define UHCI_HEAD_EN            0x020000
// Set this bit to use a special char and separate the data frame. (R/W)
#define UHCI_SEPER_EN           0x010000
// Set this bit to use UART2 and transmit or receive data. (R/W)
#define UHCI_UART2_CE           0x000800
// Set this bit to use UART1 and transmit or receive data. (R/W)
#define UHCI_UART1_CE           0x000400
// Set this bit to use UART and transmit or receive data. (R/W)
#define UHCI_UART0_CE           0x000200

/* UHCI config register */
#define UHCI0_CONF1_REG                     MMIO32(UDMA0_BASE + 0x2c)
#define UHCI1_CONF1_REG                     MMIO32(UDMA1_BASE + 0x2c)
// Reserved. Please initialize to 0. (R/W)
#define UHCI_TX_ACK_NUM_RE      0x20
// Reserved. Please initialize to 0. (R/W)
#define UHCI_TX_CHECK_SUM_RE    0x10
// Reserved. Please initialize to 0. (R/W)
#define UHCI_CHECK_SEQ_EN       0x02
// Reserved. Please initialize to 0. (R/W)
#define UHCI_CHECK_SUM_EN       0x01

/* Escape characters configuration */
#define UHCI0_ESCAPE_CONF_REG               MMIO32(UDMA0_BASE + 0x64)
#define UHCI1_ESCAPE_CONF_REG               MMIO32(UDMA1_BASE + 0x64)
// enable replacing flow control char 0x13, when DMA sends data. (R/W)
#define UHCI_RX_13_ESC_EN   0x80
// enable replacing flow control char 0x11, when DMA sends data. (R/W)
#define UHCI_RX_11_ESC_EN   0x40
// Set this bit to enable replacing 0xdb char, when DMA sends data. (R/W)
#define UHCI_RX_DB_ESC_EN   0x20
// Set this bit to enable replacing 0xc0 char, when DMA sends data. (R/W)
#define UHCI_RX_C0_ESC_EN   0x10
// enable decoding flow control char 0x13, when DMA receives data. (R/W)
#define UHCI_TX_13_ESC_EN   0x08
// enable decoding flow control char 0x11, when DMA receives data. (R/W)
#define UHCI_TX_11_ESC_EN   0x04
// Set this bit to enable decoding 0xdb char, when DMA receives data. (R/W)
#define UHCI_TX_DB_ESC_EN   0x02
// Set this bit to enable decoding 0xc0 char, when DMA receives data. (R/W)
#define UHCI_TX_C0_ESC_EN   0x01

/* Timeout configuration */
#define UHCI0_HUNG_CONF_REG                 MMIO32(UDMA0_BASE + 0x68)
#define UHCI1_HUNG_CONF_REG                 MMIO32(UDMA1_BASE + 0x68)
// This is the enable bit for DMA send-data timeout. (R/W)
#define UHCI_RXFIFO_TIMEOUT_SHIFT           0x800000
// The cnt is clr when its val is eq to or gr (17’d8000»reg_rxfifo_timeout_shift).
#define UHCI_RXFIFO_TIMEOUT_SHIFT_MSK       0x700000
#define UHCI_RXFIFO_TIMEOUT_SHIFT_SFT       20
#define UHCI_RXFIFO_TIMEOUT_SHIFT_SET(n) ((n<<UHCI_RXFIFO_TIMEOUT_SHIFT_SFT)&UHCI_RXFIFO_TIMEOUT_SHIFT_MSK)
// This register stores the timeout value.
#define UHCI_RXFIFO_TIMEOUT_MSK             0x0ff000
#define UHCI_RXFIFO_TIMEOUT_SFT             12
#define UHCI_RXFIFO_TIMEOUT_SET(n) ((n<<UHCI_RXFIFO_TIMEOUT_SFT)&UHCI_RXFIFO_TIMEOUT_MSK)
// The enable bit for Tx FIFO receive-data timeout (R/W)
#define UHCI_TXFIFO_TIMEOUT_ENA             0x000800
// The cnt is clr when its val is eq to or gr (17’d8000»reg_txfifo_timeout_shift).
#define UHCI_TXFIFO_TIMEOUT_SHIFT_MSK       0x000700
#define UHCI_TXFIFO_TIMEOUT_SHIFT_SFT       8
#define UHCI_TXFIFO_TIMEOUT_SHIFT_SET(n) ((n<<UHCI_TXFIFO_TIMEOUT_SHIFT_SFT)&UHCI_TXFIFO_TIMEOUT_SHIFT_MSK)
// This register stores the timeout value
#define UHCI_TXFIFO_TIMEOUT_MSK             0x0000ff
#define UHCI_TXFIFO_TIMEOUT_SET(n) (n&UHCI_TXFIFO_TIMEOUT_MSK)

/* Escape sequence configuration register n */
#define UHCI0_ESC_CONF0_REG                 MMIO32(UDMA0_BASE + 0xb0)
#define UHCI1_ESC_CONF0_REG                 MMIO32(UDMA1_BASE + 0xb0)
#define UHCI0_ESC_CONF1_REG                 MMIO32(UDMA0_BASE + 0xb4)
#define UHCI1_ESC_CONF1_REG                 MMIO32(UDMA1_BASE + 0xb4)
#define UHCI0_ESC_CONF2_REG                 MMIO32(UDMA0_BASE + 0xb8)
#define UHCI1_ESC_CONF2_REG                 MMIO32(UDMA1_BASE + 0xb8)
#define UHCI0_ESC_CONF3_REG                 MMIO32(UDMA0_BASE + 0xbc)
#define UHCI1_ESC_CONF3_REG                 MMIO32(UDMA1_BASE + 0xbc)
// second char used to replace the reg_esc_seq2 in data. (R/W)
#define UHCI_ESC_SEQ2_CHAR1_MSK     0xff0000
#define UHCI_ESC_SEQ2_CHAR1_SFT     16
#define UHCI_ESC_SEQ2_CHAR1_SET(n) ((n<<UHCI_ESC_SEQ2_CHAR1_SFT)&UHCI_ESC_SEQ2_CHAR1_MSK)
// stores the first char used to replace the reg_esc_seq2 in data. (R/W)
#define UHCI_ESC_SEQ2_CHAR0_MSK     0x00ff00
#define UHCI_ESC_SEQ2_CHAR0_SFT     8
#define UHCI_ESC_SEQ2_CHAR0_SET(n) ((n<<UHCI_ESC_SEQ2_CHAR0_SFT)&UHCI_ESC_SEQ2_CHAR0_MSK)
// stores the flow_control char to turn off the flow_control. (R/W)
#define UHCI_ESC_SEQ2_MSK           0x0000ff
#define UHCI_ESC_SEQ2_SET(n)        (n&UHCI_ESC_SEQ2_MSK)

/* Link descriptor address and control */
#define UHCI0_DMA_OUT_LINK_REG              MMIO32(UDMA0_BASE + 0x24)
#define UHCI1_DMA_OUT_LINK_REG              MMIO32(UDMA1_BASE + 0x24)
// 1: the outlink descriptor’s FSM is in idle state;
#define UHCI_OUTLINK_PARK           0x80000000
// Set this bit to restart the outlink descriptor from the last address. (R/W)
#define UHCI_OUTLINK_RESTART        0x40000000
// Set this bit to start a new outlink descriptor. (R/W)
#define UHCI_OUTLINK_START          0x20000000
// Set this bit to stop dealing with the outlink descriptor. (R/W)
#define UHCI_OUTLINK_STOP           0x10000000
// least significant 20 bits of the first outlink descriptor’s address. (R/W)
#define UHCI_OUTLINK_ADDR_MSK       0x000fffff
#define UHCI_OUTLINK_ADDR_SET(n)    (n&UHCI_OUTLINK_ADDR_MSK)

/* Link descriptor address and control  */
#define UHCI0_DMA_IN_LINK_REG               MMIO32(UDMA0_BASE + 0x28)
#define UHCI1_DMA_IN_LINK_REG               MMIO32(UDMA1_BASE + 0x28)
// 1: the inlink descriptor’s FSM is in idle state;
#define UHCI_INLINK_PARK           0x80000000
// Set this bit to mount new inlink descriptors. (R/W)
#define UHCI_INLINK_RESTART        0x40000000
// Set this bit to start dealing with the inlink descriptors. (R/W)
#define UHCI_INLINK_START          0x20000000
// Set this bit to stop dealing with the inlink descriptors. (R/W)
#define UHCI_INLINK_STOP           0x10000000
// 20 least significant bits of the first inlink descriptor’s address. (R/W)
#define UHCI_INLINK_ADDR_MSK       0x000fffff
#define UHCI_INLINK_ADDR_SET(n)    (n&UHCI_INLINK_ADDR_MSK)

/* FIFO data push register */
#define UHCI0_DMA_OUT_PUSH_REG              MMIO32(UDMA0_BASE + 0x18)
#define UHCI1_DMA_OUT_PUSH_REG              MMIO32(UDMA1_BASE + 0x18)
// Set this bit to push data into DMA FIFO. (R/W)
#define UHCI_OUTFIFO_POP             0x10000
// This is the data that need to be pushed into DMA FIFO. (R/W)
#define UHCI_OUTFIFO_RDATA_MSK       0x001ff
#define UHCI_OUTFIFO_RDATA_SET(n)    (n&UHCI_OUTFIFO_RDATA_MSK)

/* FIFO data pop register */
#define UHCI0_DMA_IN_POP_REG                MMIO32(UDMA0_BASE + 0x20)
#define UHCI1_DMA_IN_POP_REG                MMIO32(UDMA1_BASE + 0x20)
// Set this bit to pop data from DMA FIFO. (R/W)
#define UHCI_INFIFO_POP             0x10000
// This register stores the data popping from DMA FIFO. (RO)
#define UHCI_INFIFO_RDATA_MSK       0x001ff
#define UHCI_INFIFO_RDATA_SET(n)    (n&UHCI_INFIFO_RDATA_MSK)

/* DMA FIFO status */
#define UHCI0_DMA_OUT_STATUS_REG            MMIO32(UDMA0_BASE + 0x14)
#define UHCI1_DMA_OUT_STATUS_REG            MMIO32(UDMA1_BASE + 0x14)
// 1: DMA inlink descriptor’s FIFO is empty. (RO)
#define UHCI_OUT_EMPTY  0x2
// 1: DMA outlink descriptor’s FIFO is full. (RO)
#define UHCI_OUT_FULL   0x1

/* Out EOF link descriptor address on success */
#define UHCI0_DMA_OUT_EOF_DES_ADDR_REG      MMIO32(UDMA0_BASE + 0x38)
#define UHCI1_DMA_OUT_EOF_DES_ADDR_REG      MMIO32(UDMA1_BASE + 0x38)
// address of the outlink descriptor when the EOF bit in this descriptor is 1. (RO)

/* Out EOF link descriptor address on error */
#define UHCI0_DMA_OUT_EOF_BFR_DES_ADDR_REG  MMIO32(UDMA0_BASE + 0x44)
#define UHCI1_DMA_OUT_EOF_BFR_DES_ADDR_REG  MMIO32(UDMA1_BASE + 0x44)
// address of the outlink descriptor when there are some errors in this descriptor.

/* In EOF link descriptor address on error */
#define UHCI0_DMA_IN_SUC_EOF_DES_ADDR_REG   MMIO32(UDMA0_BASE + 0x3c)
#define UHCI1_DMA_IN_SUC_EOF_DES_ADDR_REG   MMIO32(UDMA1_BASE + 0x3c)
// address of the inlink descriptor when the EOF bit in this descriptor is 1. (RO)

/* In EOF link descriptor address on error */
#define UHCI0_DMA_IN_ERR_EOF_DES_ADDR_REG   MMIO32(UDMA0_BASE + 0x40)
#define UHCI1_DMA_IN_ERR_EOF_DES_ADDR_REG   MMIO32(UDMA1_BASE + 0x40)
// address of the inlink descriptor when there are some errors in this descriptor.

/* Current inlink descriptor, first word */
#define UHCI0_DMA_IN_DSCR_REG               MMIO32(UDMA0_BASE + 0x4c)
#define UHCI1_DMA_IN_DSCR_REG               MMIO32(UDMA1_BASE + 0x4c)
// The address of the current inlink descriptor x. (RO)

/* Current inlink descriptor, second word */
#define UHCI0_DMA_IN_DSCR_BF0_REG           MMIO32(UDMA0_BASE + 0x50)
#define UHCI1_DMA_IN_DSCR_BF0_REG           MMIO32(UDMA1_BASE + 0x50)
// The address of the last inlink descriptor x-1. (RO)

/* Current inlink descriptor, third word */
#define UHCI0_DMA_IN_DSCR_BF1_REG           MMIO32(UDMA0_BASE + 0x54)
#define UHCI1_DMA_IN_DSCR_BF1_REG           MMIO32(UDMA1_BASE + 0x54)
// The address of the second-to-last inlink descriptor x-2. (RO)

/* Current outlink descriptor, first word */
#define UHCI0_DMA_OUT_DSCR_REG              MMIO32(UDMA0_BASE + 0x58)
#define UHCI1_DMA_OUT_DSCR_REG              MMIO32(UDMA1_BASE + 0x58)
// The address of the current outlink descriptor y. (RO)

/* Current outlink descriptor, second word */
#define UHCI0_DMA_OUT_DSCR_BF0_REG          MMIO32(UDMA0_BASE + 0x5c)
#define UHCI1_DMA_OUT_DSCR_BF0_REG          MMIO32(UDMA1_BASE + 0x5c)
// The address of the last outlink descriptor y-1. (RO)

/* Current outlink descriptor, third word */
#define UHCI0_DMA_OUT_DSCR_BF1_REG          MMIO32(UDMA0_BASE + 0x60)
#define UHCI1_DMA_OUT_DSCR_BF1_REG          MMIO32(UDMA1_BASE + 0x60)
// The address of the second-to-last outlink descriptor y-2. (RO)

/* Raw interrupt status */
#define UHCI0_INT_RAW_REG                   MMIO32(UDMA0_BASE + 0x04)
#define UHCI1_INT_RAW_REG                   MMIO32(UDMA1_BASE + 0x04)
// The raw interrupt status bit for the UHCI_OUT_TOTAL_EOF_INT interrupt. (RO)
#define UHCI_OUT_TOTAL_EOF_INT_RAW      0x2000
// The raw interrupt status bit for the UHCI_OUTLINK_EOF_ERR_INT interrupt. (RO)
#define UHCI_OUTLINK_EOF_ERR_INT_RAW    0x1000
// The raw interrupt status bit for the UHCI_IN_DSCR_EMPTY_INT interrupt. (RO)
#define UHCI_IN_DSCR_EMPTY_INT_RAW      0x0800
// The raw interrupt status bit for the UHCI_OUT_DSCR_ERR_INT interrupt. (RO)
#define UHCI_OUT_DSCR_ERR_INT_RAW       0x0400
// The raw interrupt status bit for the UHCI_IN_DSCR_ERR_INT interrupt. (RO)
#define UHCI_IN_DSCR_ERR_INT_RAW        0x0200
// The raw interrupt status bit for the UHCI_OUT_EOF_INT interrupt. (RO)
#define UHCI_OUT_EOF_INT_RAW            0x0100
// The raw interrupt status bit for the UHCI_OUT_DONE_INT interrupt. (RO)
#define UHCI_OUT_DONE_INT_RAW           0x0080
// The raw interrupt status bit for the UHCI_IN_ERR_EOF_INT interrupt. (RO)
#define UHCI_IN_ERR_EOF_INT_RAW         0x0040
// The raw interrupt status bit for the UHCI_IN_SUC_EOF_INT interrupt. (RO)
#define UHCI_IN_SUC_EOF_INT_RAW         0x0020
// The raw interrupt status bit for the UHCI_IN_DONE_INT interrupt. (RO)
#define UHCI_IN_DONE_INT_RAW            0x0010
// The raw interrupt status bit for the UHCI_TX_HUNG_INT interrupt. (RO)
#define UHCI_TX_HUNG_INT_RAW            0x0008
// The raw interrupt status bit for the UHCI_RX_HUNG_INT interrupt. (RO)
#define UHCI_RX_HUNG_INT_RAW            0x0004
// The raw interrupt status bit for the UHCI_TX_START_INT interrupt. (RO)
#define UHCI_TX_START_INT_RAW           0x0002
// The raw interrupt status bit for the UHCI_RX_START_INT interrupt. (RO)
#define UHCI_RX_START_INT_RAW           0x0001

/* Masked interrupt status */
#define UHCI0_INT_ST_REG                    MMIO32(UDMA0_BASE + 0x08)
#define UHCI1_INT_ST_REG                    MMIO32(UDMA1_BASE + 0x08)
// The masked interrupt status bit for the UHCI_OUT_TOTAL_EOF_INT interrupt. (RO)
#define UHCI_OUT_TOTAL_EOF_INT_ST      0x2000
// The masked interrupt status bit for the UHCI_OUTLINK_EOF_ERR_INT interrupt. (RO)
#define UHCI_OUTLINK_EOF_ERR_INT_ST    0x1000
// The masked interrupt status bit for the UHCI_IN_DSCR_EMPTY_INT interrupt. (RO)
#define UHCI_IN_DSCR_EMPTY_INT_ST      0x0800
// The masked interrupt status bit for the UHCI_OUT_DSCR_ERR_INT interrupt. (RO)
#define UHCI_OUT_DSCR_ERR_INT_ST       0x0400
// The masked interrupt status bit for the UHCI_IN_DSCR_ERR_INT interrupt. (RO)
#define UHCI_IN_DSCR_ERR_INT_ST        0x0200
// The masked interrupt status bit for the UHCI_OUT_EOF_INT interrupt. (RO)
#define UHCI_OUT_EOF_INT_ST            0x0100
// The masked interrupt status bit for the UHCI_OUT_DONE_INT interrupt. (RO)
#define UHCI_OUT_DONE_INT_ST           0x0080
// The masked interrupt status bit for the UHCI_IN_ERR_EOF_INT interrupt. (RO)
#define UHCI_IN_ERR_EOF_INT_ST         0x0040
// The masked interrupt status bit for the UHCI_IN_SUC_EOF_INT interrupt. (RO)
#define UHCI_IN_SUC_EOF_INT_ST         0x0020
// The masked interrupt status bit for the UHCI_IN_DONE_INT interrupt. (RO)
#define UHCI_IN_DONE_INT_ST            0x0010
// The masked interrupt status bit for the UHCI_TX_HUNG_INT interrupt. (RO)
#define UHCI_TX_HUNG_INT_ST            0x0008
// The masked interrupt status bit for the UHCI_RX_HUNG_INT interrupt. (RO)
#define UHCI_RX_HUNG_INT_ST            0x0004
// The masked interrupt status bit for the UHCI_TX_START_INT interrupt. (RO)
#define UHCI_TX_START_INT_ST           0x0002
// The masked interrupt status bit for the UHCI_RX_START_INT interrupt. (RO)
#define UHCI_RX_START_INT_ST           0x0001

/* Interrupt enable bits */
#define UHCI0_INT_ENA_REG                   MMIO32(UDMA0_BASE + 0x0c)
#define UHCI1_INT_ENA_REG                   MMIO32(UDMA1_BASE + 0x0c)
// The interrupt enable bit for the UHCI_OUT_TOTAL_EOF_INT interrupt. (R/W)
#define UHCI_OUT_TOTAL_EOF_INT_ENA      0x2000
// The interrupt enable bit for the UHCI_OUTLINK_EOF_ERR_INT interrupt. (R/W)
#define UHCI_OUTLINK_EOF_ERR_INT_ENA    0x1000
// The interrupt enable bit for the UHCI_IN_DSCR_EMPTY_INT interrupt. (R/W)
#define UHCI_IN_DSCR_EMPTY_INT_ENA      0x0800
// The interrupt enable bit for the UHCI_OUT_DSCR_ERR_INT interrupt. (R/W)
#define UHCI_OUT_DSCR_ERR_INT_ENA       0x0400
// The interrupt enable bit for the UHCI_IN_DSCR_ERR_INT interrupt. (R/W)
#define UHCI_IN_DSCR_ERR_INT_ENA        0x0200
// The interrupt enable bit for the UHCI_OUT_EOF_INT interrupt. (R/W)
#define UHCI_OUT_EOF_INT_ENA            0x0100
// The interrupt enable bit for the UHCI_OUT_DONE_INT interrupt. (R/W)
#define UHCI_OUT_DONE_INT_ENA           0x0080
// The interrupt enable bit for the UHCI_IN_ERR_EOF_INT interrupt. (R/W)
#define UHCI_IN_ERR_EOF_INT_ENA         0x0040
// The interrupt enable bit for the UHCI_IN_SUC_EOF_INT interrupt. (R/W)
#define UHCI_IN_SUC_EOF_INT_ENA         0x0020
// The interrupt enable bit for the UHCI_IN_DONE_INT interrupt. (R/W)
#define UHCI_IN_DONE_INT_ENA            0x0010
// The interrupt enable bit for the UHCI_TX_HUNG_INT interrupt. (R/W)
#define UHCI_TX_HUNG_INT_ENA            0x0008
// The interrupt enable bit for the UHCI_RX_HUNG_INT interrupt. (R/W)
#define UHCI_RX_HUNG_INT_ENA            0x0004
// The interrupt enable bit for the UHCI_TX_START_INT interrupt. (R/W)
#define UHCI_TX_START_INT_ENA           0x0002
// The interrupt enable bit for the UHCI_RX_START_INT interrupt. (R/W)
#define UHCI_RX_START_INT_ENA           0x0001

/* Interrupt clear bits */
#define UHCI0_INT_CLR_REG                   MMIO32(UDMA0_BASE + 0x10)
#define UHCI1_INT_CLR_REG                   MMIO32(UDMA1_BASE + 0x10)
// Set this bit to clear the UHCI_OUT_TOTAL_EOF_INT interrupt. (WO)
#define UHCI_OUT_TOTAL_EOF_INT_CLR      0x2000
// Set this bit to clear the UHCI_OUTLINK_EOF_ERR_INT interrupt. (WO)
#define UHCI_OUTLINK_EOF_ERR_INT_CLR    0x1000
// Set this bit to clear the UHCI_IN_DSCR_EMPTY_INT interrupt. (WO)
#define UHCI_IN_DSCR_EMPTY_INT_CLR      0x0800
// Set this bit to clear the UHCI_OUT_DSCR_ERR_INT interrupt. (WO)
#define UHCI_OUT_DSCR_ERR_INT_CLR       0x0400
// Set this bit to clear the UHCI_IN_DSCR_ERR_INT interrupt. (WO)
#define UHCI_IN_DSCR_ERR_INT_CLR        0x0200
// Set this bit to clear the UHCI_OUT_EOF_INT interrupt. (WO)
#define UHCI_OUT_EOF_INT_CLR            0x0100
// Set this bit to clear the UHCI_OUT_DONE_INT interrupt. (WO)
#define UHCI_OUT_DONE_INT_CLR           0x0080
// Set this bit to clear the UHCI_IN_ERR_EOF_INT interrupt. (WO)
#define UHCI_IN_ERR_EOF_INT_CLR         0x0040
// Set this bit to clear the UHCI_IN_SUC_EOF_INT interrupt. (WO)
#define UHCI_IN_SUC_EOF_INT_CLR         0x0020
// Set this bit to clear the UHCI_IN_DONE_INT interrupt. (WO)
#define UHCI_IN_DONE_INT_CLR            0x0010
// Set this bit to clear the UHCI_TX_HUNG_INT interrupt. (WO)
#define UHCI_TX_HUNG_INT_CLR            0x0008
// Set this bit to clear the UHCI_RX_HUNG_INT interrupt. (WO)
#define UHCI_RX_HUNG_INT_CLR            0x0004
// Set this bit to clear the UHCI_TX_START_INT interrupt. (WO)
#define UHCI_TX_START_INT_CLR           0x0002
// Set this bit to clear the UHCI_RX_START_INT interrupt. (WO)
#define UHCI_RX_START_INT_CLR           0x0001

#endif
