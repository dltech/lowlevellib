#ifndef H_SDIO_REGS
#define H_SDIO_REGS
/*
 * ESP32 alternative library, SDIO Slave Controller registers.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* SLCCONF0_SLC configuration */
#define SLCCONF0_REG                MMIO32(SDIO_SLAVE3 + 0x000)
// Please initialize to 0. Do not modify it. (R/W)
#define SLCCONF0_SLC0_TOKEN_AUTO_CLR    0x4000
// Allows changing the owner bit of the tx buffer’s linked list when tx data. (R/W)
#define SLCCONF0_SLC0_RX_AUTO_WRBACK    0x0040
// Loop around when the slave buffer finishes sending packets.
#define SLCCONF0_SLC0_RX_LOOP_TEST      0x0020
// Loop around when the slave buffer finishes receiving packets.
#define SLCCONF0_SLC0_TX_LOOP_TEST      0x0010
// Set this bit to reset the transmitting FSM. (R/W)
#define SLCCONF0_SLC0_RX_RST            0x0002
// Set this bit to reset the receiving FSM. (R/W)
#define SLCCONF0_SLC0_TX_RST            0x0001

/* Raw interrupt status */
#define SLC0INT_RAW_REG             MMIO32(SDIO_SLAVE3 + 0x004)
// The raw interrupt bit for Slave sending descriptor error (RO)
#define SLC0INT_SLC0_RX_DSCR_ERR_INT_RAW    0x100000
// The raw interrupt bit for Slave receiving descriptor error. (RO)
#define SLC0INT_SLC0_TX_DSCR_ERR_INT_RAW    0x080000
// The interrupt mark bit for Slave sending operation finished. (RO)
#define SLC0INT_SLC0_RX_EOF_INT_RAW         0x020000
// The raw interrupt bit to mark single buffer as sent by Slave. (RO)
#define SLC0INT_SLC0_RX_DONE_INT_RAW        0x010000
// The raw interrupt bit to mark Slave receiving operation as finished. (RO)
#define SLC0INT_SLC0_TX_SUC_EOF_INT_RAW     0x008000
// The raw interrupt bit to mark a single buffer as finished during Slave rx op.
#define SLC0INT_SLC0_TX_DONE_INT_RAW        0x004000
// The raw interrupt bit to mark Slave receiving buffer overflow. (RO)
#define SLC0INT_SLC0_TX_OVF_INT_RAW         0x000800
// The raw interrupt bit for Slave sending buffer underflow. (RO)
#define SLC0INT_SLC0_RX_UDF_INT_RAW         0x000400
// The raw interrupt bit for registering Slave receiving initialization int. (RO)
#define SLC0INT_SLC0_TX_START_INT_RAW       0x000200
// The raw interrupt bit to mark Slave sending initialization interrupt. (RO)
#define SLC0INT_SLC0_RX_START_INT_RAW       0x000100
// The interrupt mark bit 7 for Host to interrupt Slave. (RO)
#define SLC0INT_SLC_FRHOST_BIT7_INT_RAW     0x000080
// The interrupt mark bit 6 for Host to interrupt Slave. (RO)
#define SLC0INT_SLC_FRHOST_BIT6_INT_RAW     0x000040
// The interrupt mark bit 5 for Host to interrupt Slave. (RO)
#define SLC0INT_SLC_FRHOST_BIT5_INT_RAW     0x000020
// The interrupt mark bit 4 for Host to interrupt Slave. (RO)
#define SLC0INT_SLC_FRHOST_BIT4_INT_RAW     0x000010
// The interrupt mark bit 3 for Host to interrupt Slave. (RO)
#define SLC0INT_SLC_FRHOST_BIT3_INT_RAW     0x000008
// The interrupt mark bit 2 for Host to interrupt Slave. (RO)
#define SLC0INT_SLC_FRHOST_BIT2_INT_RAW     0x000004
// The interrupt mark bit 1 for Host to interrupt Slave. (RO)
#define SLC0INT_SLC_FRHOST_BIT1_INT_RAW     0x000002
// The interrupt mark bit 0 for Host to interrupt Slave. (RO)
#define SLC0INT_SLC_FRHOST_BIT0_INT_RAW     0x000001

/* Interrupt status */
#define SLC0INT_ST_REG              MMIO32(SDIO_SLAVE3 + 0x008)
// The interrupt status bit for Slave sending descriptor error (RO)
#define SLC0INT_SLC0_RX_DSCR_ERR_INT_ST    0x100000
// The interrupt status bit for Slave receiving descriptor error. (RO)
#define SLC0INT_SLC0_TX_DSCR_ERR_INT_ST    0x080000
// The interrupt status bit for Slave sending operation finished. (RO)
#define SLC0INT_SLC0_RX_EOF_INT_ST         0x020000
// The interrupt status bit to mark single buffer as sent by Slave. (RO)
#define SLC0INT_SLC0_RX_DONE_INT_ST        0x010000
// The interrupt status bit to mark Slave receiving operation as finished. (RO)
#define SLC0INT_SLC0_TX_SUC_EOF_INT_ST     0x008000
// The interrupt status bit to mark a single buffer as finished during Slave rx op.
#define SLC0INT_SLC0_TX_DONE_INT_ST        0x004000
// The interrupt status bit to mark Slave receiving buffer overflow. (RO)
#define SLC0INT_SLC0_TX_OVF_INT_ST         0x000800
// The interrupt status bit for Slave sending buffer underflow. (RO)
#define SLC0INT_SLC0_RX_UDF_INT_ST         0x000400
// The interrupt status bit for registering Slave receiving initialization int. (RO)
#define SLC0INT_SLC0_TX_START_INT_ST       0x000200
// The interrupt status bit to mark Slave sending initialization interrupt. (RO)
#define SLC0INT_SLC0_RX_START_INT_ST       0x000100
// The interrupt status bit 7 for Host to interrupt Slave. (RO)
#define SLC0INT_SLC_FRHOST_BIT7_INT_ST     0x000080
// The interrupt status bit 6 for Host to interrupt Slave. (RO)
#define SLC0INT_SLC_FRHOST_BIT6_INT_ST     0x000040
// The interrupt status bitt 5 for Host to interrupt Slave. (RO)
#define SLC0INT_SLC_FRHOST_BIT5_INT_ST     0x000020
// The interrupt status bit 4 for Host to interrupt Slave. (RO)
#define SLC0INT_SLC_FRHOST_BIT4_INT_ST     0x000010
// The interrupt status bit 3 for Host to interrupt Slave. (RO)
#define SLC0INT_SLC_FRHOST_BIT3_INT_ST     0x000008
// The interrupt status bit 2 for Host to interrupt Slave. (RO)
#define SLC0INT_SLC_FRHOST_BIT2_INT_ST     0x000004
// The interrupt status bit 1 for Host to interrupt Slave. (RO)
#define SLC0INT_SLC_FRHOST_BIT1_INT_ST     0x000002
// The interrupt status bit 0 for Host to interrupt Slave. (RO)
#define SLC0INT_SLC_FRHOST_BIT0_INT_ST     0x000001

/* Interrupt enable */
#define SLC0INT_ENA_REG             MMIO32(SDIO_SLAVE3 + 0x00c)
// The interrupt enable bit for Slave sending descriptor error (RO)
#define SLC0INT_SLC0_RX_DSCR_ERR_INT_ENA    0x100000
// The interrupt enable bit for Slave receiving descriptor error. (RO)
#define SLC0INT_SLC0_TX_DSCR_ERR_INT_ENA    0x080000
// The interrupt enable bit for Slave sending operation finished. (RO)
#define SLC0INT_SLC0_RX_EOF_INT_ENA         0x020000
// The interrupt enable bit to mark single buffer as sent by Slave. (RO)
#define SLC0INT_SLC0_RX_DONE_INT_ENA        0x010000
// The interrupt enable bit to mark Slave receiving operation as finished. (RO)
#define SLC0INT_SLC0_TX_SUC_EOF_INT_ENA     0x008000
// The interrupt enable bit to mark a single buffer as finished during Slave rx op.
#define SLC0INT_SLC0_TX_DONE_INT_ENA        0x004000
// The interrupt enable bit to mark Slave receiving buffer overflow. (RO)
#define SLC0INT_SLC0_TX_OVF_INT_ENA         0x000800
// The interrupt enable bit for Slave sending buffer underflow. (RO)
#define SLC0INT_SLC0_RX_UDF_INT_ENA         0x000400
// The interrupt enable bit for registering Slave receiving initialization int. (RO)
#define SLC0INT_SLC0_TX_START_INT_ENA       0x000200
// The interrupt enable bit to mark Slave sending initialization interrupt. (RO)
#define SLC0INT_SLC0_RX_START_INT_ENA       0x000100
// The interrupt enable bit 7 for Host to interrupt Slave. (RO)
#define SLC0INT_SLC_FRHOST_BIT7_INT_ENA     0x000080
// The interrupt enable bit 6 for Host to interrupt Slave. (RO)
#define SLC0INT_SLC_FRHOST_BIT6_INT_ENA     0x000040
// The interrupt enable bit 5 for Host to interrupt Slave. (RO)
#define SLC0INT_SLC_FRHOST_BIT5_INT_ENA     0x000020
// The interrupt enable bit 4 for Host to interrupt Slave. (RO)
#define SLC0INT_SLC_FRHOST_BIT4_INT_ENA     0x000010
// The interrupt enable bit 3 for Host to interrupt Slave. (RO)
#define SLC0INT_SLC_FRHOST_BIT3_INT_ENA     0x000008
// The interrupt enable bit 2 for Host to interrupt Slave. (RO)
#define SLC0INT_SLC_FRHOST_BIT2_INT_ENA     0x000004
// The interrupt enable bit 1 for Host to interrupt Slave. (RO)
#define SLC0INT_SLC_FRHOST_BIT1_INT_ENA     0x000002
// The interrupt enable bit 0 for Host to interrupt Slave. (RO)
#define SLC0INT_SLC_FRHOST_BIT0_INT_ENA     0x000001

/* Interrupt clear */
#define SLC0INT_CLR_REG             MMIO32(SDIO_SLAVE3 + 0x010)
// Interrupt clear bit for Slave sending descriptor error (RO)
#define SLC0INT_SLC0_RX_DSCR_ERR_INT_CLR    0x100000
// Interrupt clear bit for Slave receiving descriptor error. (RO)
#define SLC0INT_SLC0_TX_DSCR_ERR_INT_CLR    0x080000
// Interrupt clear bit for Slave sending operation finished. (RO)
#define SLC0INT_SLC0_RX_EOF_INT_CLR         0x020000
// Interrupt clear bit to mark single buffer as sent by Slave. (RO)
#define SLC0INT_SLC0_RX_DONE_INT_CLR        0x010000
// Interrupt clear bit to mark Slave receiving operation as finished. (RO)
#define SLC0INT_SLC0_TX_SUC_EOF_INT_CLR     0x008000
// Interrupt clear bit to mark a single buffer as finished during Slave rx op.
#define SLC0INT_SLC0_TX_DONE_INT_CLR        0x004000
// Interrupt clear bit to mark Slave receiving buffer overflow. (RO)
#define SLC0INT_SLC0_TX_OVF_INT_CLR         0x000800
// Interrupt clear bit for Slave sending buffer underflow. (RO)
#define SLC0INT_SLC0_RX_UDF_INT_CLR         0x000400
// Interrupt clear bit for registering Slave receiving initialization int. (RO)
#define SLC0INT_SLC0_TX_START_INT_CLR       0x000200
// Interrupt clear bit to mark Slave sending initialization interrupt. (RO)
#define SLC0INT_SLC0_RX_START_INT_CLR       0x000100
// Interrupt clear bit 7 for Host to interrupt Slave. (RO)
#define SLC0INT_SLC_FRHOST_BIT7_INT_CLR     0x000080
// Interrupt clear bit 6 for Host to interrupt Slave. (RO)
#define SLC0INT_SLC_FRHOST_BIT6_INT_CLR     0x000040
// Interrupt clear bit 5 for Host to interrupt Slave. (RO)
#define SLC0INT_SLC_FRHOST_BIT5_INT_CLR     0x000020
// Interrupt clear bit 4 for Host to interrupt Slave. (RO)
#define SLC0INT_SLC_FRHOST_BIT4_INT_CLR     0x000010
// Interrupt clear bit 3 for Host to interrupt Slave. (RO)
#define SLC0INT_SLC_FRHOST_BIT3_INT_CLR     0x000008
// Interrupt clear bit 2 for Host to interrupt Slave. (RO)
#define SLC0INT_SLC_FRHOST_BIT2_INT_CLR     0x000004
// Interrupt clear bit 1 for Host to interrupt Slave. (RO)
#define SLC0INT_SLC_FRHOST_BIT1_INT_CLR     0x000002
// Interrupt clear bit 0 for Host to interrupt Slave. (RO)
#define SLC0INT_SLC_FRHOST_BIT0_INT_CLR     0x000001

/* Transmitting linked list configuration */
#define SLC0RX_LINK_REG             MMIO32(SDIO_SLAVE3 + 0x03c)
// restart and continue the linked list operation for sending packets. (R/W)
#define SLC0RX_SLC0_RXLINK_RESTART      0x40000000
// Set this bit to start the linked list operation for sending packets.
#define SLC0RX_SLC0_RXLINK_START        0x20000000
// Set this bit to stop the linked list operation. (R/W)
#define SLC0RX_SLC0_RXLINK_STOP         0x10000000
// The lowest 20 bits in the initial address of Slave’s sending linked list.
#define SLC0RX_SLC0_RXLINK_ADDR_MSK     0x000fffff
#define SLC0RX_SLC0_RXLINK_ADDR_SET(n)  (n&SLC0RX_SLC0_RXLINK_ADDR_MSK)

/* Receiving linked list configuration */
#define SLC0TX_LINK_REG             MMIO32(SDIO_SLAVE3 + 0x040)
// restart and continue the linked list operation for receiving packets. (R/W)
#define SLC0TX_SLC0_RXLINK_RESTART      0x40000000
// Set this bit to start the linked list operation for receiving packets.
#define SLC0TX_SLC0_RXLINK_START        0x20000000
// Set this bit to stop the linked list operation for receiving packets. (R/W)
#define SLC0TX_SLC0_RXLINK_STOP         0x10000000
// The lowest 20 bits in the initial address of Slave’s receiving linked list. (R/W)
#define SLC0TX_SLC0_RXLINK_ADDR_MSK     0x000fffff
#define SLC0TX_SLC0_RXLINK_ADDR_SET(n)  (n&SLC0TX_SLC0_RXLINK_ADDR_MSK)

/* Interrupt sector for Slave to interrupt Host */
#define SLCINTVEC_TOHOST_REG        MMIO32(SDIO_SLAVE3 + 0x04c)
// The interrupt vector for Slave to interrupt Host. (WO)
#define SLCINTVEC_SLC0_TOHOST_INTVEC_MSK    0xff

/* Number of receiving buffer */
#define SLC0TOKEN1_REG              MMIO32(SDIO_SLAVE3 + 0x054)
// The accumulated number of buffers for receiving packets. (RO)
#define SLC0TOKEN1_SLC0_TOKEN1_MSK          0xfff0000
#define SLC0TOKEN1_SLC0_TOKEN1_SFT          16
#define SLC0TOKEN1_SLC0_TOKEN1_GET ((SLC0TOKEN1_REG>>SLC0TOKEN1_SLC0_TOKEN1_SFT)&0xfff)
// Set this bit to add the value of SLC0TOKEN1_SLC0_TOKEN1_WDATA to that of.
#define SLC0TOKEN1_SLC0_TOKEN1_INC_MORE     0x4000
// The number of available receiving buffers. (WO)
#define SLC0TOKEN1_SLC0_TOKEN1_WDATA_MSK    0x0fff
#define SLC0TOKEN1_SLC0_TOKEN1_WDATA_SET(n) (n&SLC0TOKEN1_SLC0_TOKEN1_WDATA_MSK)

/* Control register */
#define SLCCONF1_REG                MMIO32(SDIO_SLAVE3 + 0x060)
// Please initialize to 0. Do not modify it. (R/W)
#define SLCCONF1_SLC0_RX_STITCH_EN  0x40
// Please initialize to 0. Do not modify it. (R/W)
#define SLCCONF1_SLC0_TX_STITCH_EN  0x20
// Please initialize to 0. Do not modify it. (R/W)
#define SLCCONF1_SLC0_LEN_AUTO_CLR  0x10

/* DMA transmission configuration */
#define SLC_RX_DSCR_CONF_REG        MMIO32(SDIO_SLAVE3 + 0x098)
// Please initialize to 1. Do not modify it. (R/W)
#define SLC_SLC0_TOKEN_NO_REPLACE   0x1

/* Length control of the transmitting packets */
#define SLC0_LEN_CONF_REG           MMIO32(SDIO_SLAVE3 + 0x0e4)
// Set this bit to add the value of SLC0_LEN to that of SLC0_LEN_WDATA. (WO)
#define SLC0_LEN_INC_MORE   0x400000
// The packet length sent. (WO)
#define SLC0_LEN_WDATA_MSK  0x0fffff

/* Length of the transmitting packets */
#define SLC0_LENGTH_REG             MMIO32(SDIO_SLAVE3 + 0x0e8)
// Indicates the packet length sent by the Slave. (RO)
#define SLC0_LEN    0xfffff

/* The accumulated number of Slave’s receiving buffers */
#define SLC0HOST_TOKEN_RDATA        MMIO32(SDIO_SLAVE2 + 0x044)
// The accumulated number of Slave’s receiving buffers. (RO)
#define HOSTREG_SLC0_TOKEN1_MSK 0xfff0000
#define HOSTREG_SLC0_TOKEN1_SFT 16
#define HOSTREG_SLC0_TOKEN1_GET ((SLC0HOST_TOKEN_RDATA>>HOSTREG_SLC0_TOKEN1_MSK)&0xfff)

/* Raw interrupt */
#define SLC0HOST_INT_RAW_REG        MMIO32(SDIO_SLAVE2 + 0x050)
// The raw interrupt status bit for the SLC0HOST_SLC0_RX_NEW_PACKET_INT int. (RO)
#define SLC0HOST_SLC0_RX_NEW_PACKET_INT_RAW 0x800000
// The raw interrupt status bit for the SLC0HOST_SLC0_TX_OVF_INT interrupt. (RO)
#define SLC0HOST_SLC0_TX_OVF_INT_RAW        0x020000
// The raw interrupt status bit for the SLC0HOST_SLC0_RX_UDF_INT interrupt. (RO)
#define SLC0HOST_SLC0_RX_UDF_INT_RAW        0x010000
// The raw interrupt status bit for the SLC0HOST_SLC0_TOHOST_BIT7_INT int. (RO)
#define SLC0HOST_SLC0_TOHOST_BIT7_INT_RAW   0x000080
// The raw interrupt status bit for the SLC0HOST_SLC0_TOHOST_BIT6_INT int. (RO)
#define SLC0HOST_SLC0_TOHOST_BIT6_INT_RAW   0x000040
// The raw interrupt status bit for the SLC0HOST_SLC0_TOHOST_BIT5_INT int. (RO)
#define SLC0HOST_SLC0_TOHOST_BIT5_INT_RAW   0x000020
// The raw interrupt status bit for the SLC0HOST_SLC0_TOHOST_BIT4_INT int. (RO)
#define SLC0HOST_SLC0_TOHOST_BIT4_INT_RAW   0x000010
// The raw interrupt status bit for the SLC0HOST_SLC0_TOHOST_BIT3_INT int. (RO)
#define SLC0HOST_SLC0_TOHOST_BIT3_INT_RAW   0x000008
// The raw interrupt status bit for the SLC0HOST_SLC0_TOHOST_BIT2_INT int. (RO)
#define SLC0HOST_SLC0_TOHOST_BIT2_INT_RAW   0x000004
// The raw interrupt status bit for the SLC0HOST_SLC0_TOHOST_BIT1_INT int. (RO)
#define SLC0HOST_SLC0_TOHOST_BIT1_INT_RAW   0x000002
// The raw interrupt status bit for the SLC0HOST_SLC0_TOHOST_BIT0_INT int. (RO)
#define SLC0HOST_SLC0_TOHOST_BIT0_INT_RAW   0x000001

/* Masked interrupt status */
#define SLC0HOST_INT_ST_REG         MMIO32(SDIO_SLAVE2 + 0x058)
// The masked interrupt status bit for the SLC0HOST_SLC0_RX_NEW_PACKET_INT int. (RO)
#define SLC0HOST_SLC0_RX_NEW_PACKET_INT_ST 0x800000
// The masked interrupt status bit for the SLC0HOST_SLC0_TX_OVF_INT interrupt. (RO)
#define SLC0HOST_SLC0_TX_OVF_INT_ST        0x020000
// The masked interrupt status bit for the SLC0HOST_SLC0_RX_UDF_INT interrupt. (RO)
#define SLC0HOST_SLC0_RX_UDF_INT_ST        0x010000
// The masked interrupt status bit for the SLC0HOST_SLC0_TOHOST_BIT7_INT int. (RO)
#define SLC0HOST_SLC0_TOHOST_BIT7_INT_ST   0x000080
// The masked interrupt status bit for the SLC0HOST_SLC0_TOHOST_BIT6_INT int. (RO)
#define SLC0HOST_SLC0_TOHOST_BIT6_INT_ST   0x000040
// The masked interrupt status bit for the SLC0HOST_SLC0_TOHOST_BIT5_INT int. (RO)
#define SLC0HOST_SLC0_TOHOST_BIT5_INT_ST   0x000020
// The masked interrupt status bit for the SLC0HOST_SLC0_TOHOST_BIT4_INT int. (RO)
#define SLC0HOST_SLC0_TOHOST_BIT4_INT_ST   0x000010
// The masked interrupt status bit for the SLC0HOST_SLC0_TOHOST_BIT3_INT int. (RO)
#define SLC0HOST_SLC0_TOHOST_BIT3_INT_ST   0x000008
// The masked interrupt status bit for the SLC0HOST_SLC0_TOHOST_BIT2_INT int. (RO)
#define SLC0HOST_SLC0_TOHOST_BIT2_INT_ST   0x000004
// The masked interrupt status bit for the SLC0HOST_SLC0_TOHOST_BIT1_INT int. (RO)
#define SLC0HOST_SLC0_TOHOST_BIT1_INT_ST   0x000002
// The masked interrupt status bit for the SLC0HOST_SLC0_TOHOST_BIT0_INT int. (RO)
#define SLC0HOST_SLC0_TOHOST_BIT0_INT_ST   0x000001

/* Length of the transmitting packets */
#define SLCHOST_PKT_LEN_REG         MMIO32(SDIO_SLAVE2 + 0x060)
// Its value is HOSTREG_SLC0_LEN[9:0] plus HOSTREG_SLC0_LEN[19:10]. (RO)
#define SLCHOST_HOSTREG_SLC0_LEN_CHECK_MSK  0xfff00000
#define SLCHOST_HOSTREG_SLC0_LEN_CHECK_SFT  20
#define SLCHOST_HOSTREG_SLC0_LEN_CHECK_GET  ((SLCHOST_PKT_LEN_REG>>SLCHOST_HOSTREG_SLC0_LEN_CHECK_SFT)&0xfff)
// The accumulated value of the data length sent by the Slave.
#define SLCHOST_HOSTREG_SLC0_LEN_MSK        0x000fffff
#define SLCHOST_HOSTREG_SLC0_LEN_GET (SLCHOST_PKT_LEN_REG&SLCHOST_HOSTREG_SLC0_LEN_MSK)

/* Host and Slave communication register0 */
#define SLCHOST_CONF_W0_REG         MMIO32(SDIO_SLAVE2 + 0x06c)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF3_MSK       0xff000000
#define SLCHOST_CONF3_SFT       24
#define SLCHOST_CONF3_SET(n)    ((n<<SLCHOST_CONF3_SFT)&SLCHOST_CONF3_MSK)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF2_MSK       0x00ff0000
#define SLCHOST_CONF2_SFT       24
#define SLCHOST_CONF2_SET(n)    ((n<<SLCHOST_CONF2_SFT)&SLCHOST_CONF2_MSK)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF1_MSK       0x0000ff00
#define SLCHOST_CONF1_SFT       24
#define SLCHOST_CONF1_SET(n)    ((n<<SLCHOST_CONF1_SFT)&SLCHOST_CONF1_MSK)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF0_MSK       0x000000ff
#define SLCHOST_CONF0_SET(n)    (n&SLCHOST_CONF0_MSK)

/* Host and Slave communication register0 */
#define SLCHOST_CONF_W1_REG         MMIO32(SDIO_SLAVE2 + 0x070)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF7_MSK       0xff000000
#define SLCHOST_CONF7_SFT       24
#define SLCHOST_CONF7_SET(n)    ((n<<SLCHOST_CONF7_SFT)&SLCHOST_CONF7_MSK)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF6_MSK       0x00ff0000
#define SLCHOST_CONF6_SFT       24
#define SLCHOST_CONF6_SET(n)    ((n<<SLCHOST_CONF6_SFT)&SLCHOST_CONF6_MSK)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF5_MSK       0x0000ff00
#define SLCHOST_CONF5_SFT       24
#define SLCHOST_CONF5_SET(n)    ((n<<SLCHOST_CONF5_SFT)&SLCHOST_CONF5_MSK)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF4_MSK       0x000000ff
#define SLCHOST_CONF4_SET(n)    (n&SLCHOST_CONF4_MSK)

/* Host and Slave communication register0 */
#define SLCHOST_CONF_W2_REG         MMIO32(SDIO_SLAVE2 + 0x074)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF11_MSK       0xff000000
#define SLCHOST_CONF11_SFT       24
#define SLCHOST_CONF11_SET(n)    ((n<<SLCHOST_CONF11_SFT)&SLCHOST_CONF11_MSK)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF10_MSK       0x00ff0000
#define SLCHOST_CONF10_SFT       24
#define SLCHOST_CONF10_SET(n)    ((n<<SLCHOST_CONF10_SFT)&SLCHOST_CONF10_MSK)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF9_MSK       0x0000ff00
#define SLCHOST_CONF9_SFT       24
#define SLCHOST_CONF9_SET(n)    ((n<<SLCHOST_CONF9_SFT)&SLCHOST_CONF9_MSK)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF8_MSK       0x000000ff
#define SLCHOST_CONF8_SET(n)    (n&SLCHOST_CONF8_MSK)

/* Host and Slave communication register0 */
#define SLCHOST_CONF_W3_REG         MMIO32(SDIO_SLAVE2 + 0x078)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF15_MSK       0xff000000
#define SLCHOST_CONF15_SFT       24
#define SLCHOST_CONF15_SET(n)    ((n<<SLCHOST_CONF15_SFT)&SLCHOST_CONF15_MSK)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF14_MSK       0x00ff0000
#define SLCHOST_CONF14_SFT       24
#define SLCHOST_CONF14_SET(n)    ((n<<SLCHOST_CONF14_SFT)&SLCHOST_CONF14_MSK)

/* Host and Slave communication register0 */
#define SLCHOST_CONF_W4_REG         MMIO32(SDIO_SLAVE2 + 0x07c)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF19_MSK       0xff000000
#define SLCHOST_CONF19_SFT       24
#define SLCHOST_CONF19_SET(n)    ((n<<SLCHOST_CONF19_SFT)&SLCHOST_CONF19_MSK)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF18_MSK       0x00ff0000
#define SLCHOST_CONF18_SFT       24
#define SLCHOST_CONF18_SET(n)    ((n<<SLCHOST_CONF18_SFT)&SLCHOST_CONF18_MSK)

/* Host and Slave communication register0 */
#define SLCHOST_CONF_W6_REG         MMIO32(SDIO_SLAVE2 + 0x088)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF27_MSK       0xff000000
#define SLCHOST_CONF27_SFT       24
#define SLCHOST_CONF27_SET(n)    ((n<<SLCHOST_CONF27_SFT)&SLCHOST_CONF27_MSK)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF26_MSK       0x00ff0000
#define SLCHOST_CONF26_SFT       24
#define SLCHOST_CONF26_SET(n)    ((n<<SLCHOST_CONF26_SFT)&SLCHOST_CONF26_MSK)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF25_MSK       0x0000ff00
#define SLCHOST_CONF25_SFT       24
#define SLCHOST_CONF25_SET(n)    ((n<<SLCHOST_CONF25_SFT)&SLCHOST_CONF25_MSK)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF24_MSK       0x000000ff
#define SLCHOST_CONF24_SET(n)    (n&SLCHOST_CONF24_MSK)

/* Interrupt vector for Host to interrupt Slave */
#define SLCHOST_CONF_W7_REG         MMIO32(SDIO_SLAVE2 + 0x08c)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF31_MSK       0xff000000
#define SLCHOST_CONF31_SFT       24
#define SLCHOST_CONF31_SET(n)    ((n<<SLCHOST_CONF31_SFT)&SLCHOST_CONF31_MSK)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF29_MSK       0x0000ff00
#define SLCHOST_CONF29_SFT       24
#define SLCHOST_CONF29_SET(n)    ((n<<SLCHOST_CONF29_SFT)&SLCHOST_CONF29_MSK)

/* Host and Slave communication register0 */
#define SLCHOST_CONF_W8_REG         MMIO32(SDIO_SLAVE2 + 0x09c)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF35_MSK       0xff000000
#define SLCHOST_CONF35_SFT       24
#define SLCHOST_CONF35_SET(n)    ((n<<SLCHOST_CONF35_SFT)&SLCHOST_CONF35_MSK)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF34_MSK       0x00ff0000
#define SLCHOST_CONF34_SFT       24
#define SLCHOST_CONF34_SET(n)    ((n<<SLCHOST_CONF34_SFT)&SLCHOST_CONF34_MSK)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF33_MSK       0x0000ff00
#define SLCHOST_CONF33_SFT       24
#define SLCHOST_CONF33_SET(n)    ((n<<SLCHOST_CONF33_SFT)&SLCHOST_CONF33_MSK)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF32_MSK       0x000000ff
#define SLCHOST_CONF32_SET(n)    (n&SLCHOST_CONF32_MSK)

/* Host and Slave communication register0 */
#define SLCHOST_CONF_W9_REG         MMIO32(SDIO_SLAVE2 + 0x0a0)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF39_MSK       0xff000000
#define SLCHOST_CONF39_SFT       24
#define SLCHOST_CONF39_SET(n)    ((n<<SLCHOST_CONF39_SFT)&SLCHOST_CONF39_MSK)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF38_MSK       0x00ff0000
#define SLCHOST_CONF38_SFT       24
#define SLCHOST_CONF38_SET(n)    ((n<<SLCHOST_CONF38_SFT)&SLCHOST_CONF38_MSK)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF37_MSK       0x0000ff00
#define SLCHOST_CONF37_SFT       24
#define SLCHOST_CONF37_SET(n)    ((n<<SLCHOST_CONF37_SFT)&SLCHOST_CONF37_MSK)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF36_MSK       0x000000ff
#define SLCHOST_CONF36_SET(n)    (n&SLCHOST_CONF36_MSK)

/* Host and Slave communication register0 */
#define SLCHOST_CONF_W10_REG        MMIO32(SDIO_SLAVE2 + 0x0a4)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF43_MSK       0xff000000
#define SLCHOST_CONF43_SFT       24
#define SLCHOST_CONF43_SET(n)    ((n<<SLCHOST_CONF43_SFT)&SLCHOST_CONF43_MSK)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF42_MSK       0x00ff0000
#define SLCHOST_CONF42_SFT       24
#define SLCHOST_CONF42_SET(n)    ((n<<SLCHOST_CONF42_SFT)&SLCHOST_CONF42_MSK)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF41_MSK       0x0000ff00
#define SLCHOST_CONF41_SFT       24
#define SLCHOST_CONF41_SET(n)    ((n<<SLCHOST_CONF41_SFT)&SLCHOST_CONF41_MSK)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF40_MSK       0x000000ff
#define SLCHOST_CONF40_SET(n)    (n&SLCHOST_CONF40_MSK)

/* Host and Slave communication register0 */
#define SLCHOST_CONF_W11_REG        MMIO32(SDIO_SLAVE2 + 0x0a8)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF47_MSK       0xff000000
#define SLCHOST_CONF47_SFT       24
#define SLCHOST_CONF47_SET(n)    ((n<<SLCHOST_CONF47_SFT)&SLCHOST_CONF47_MSK)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF46_MSK       0x00ff0000
#define SLCHOST_CONF46_SFT       24
#define SLCHOST_CONF46_SET(n)    ((n<<SLCHOST_CONF46_SFT)&SLCHOST_CONF46_MSK)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF45_MSK       0x0000ff00
#define SLCHOST_CONF45_SFT       24
#define SLCHOST_CONF45_SET(n)    ((n<<SLCHOST_CONF45_SFT)&SLCHOST_CONF45_MSK)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF44_MSK       0x000000ff
#define SLCHOST_CONF44_SET(n)    (n&SLCHOST_CONF44_MSK)

/* Host and Slave communication register0 */
#define SLCHOST_CONF_W12_REG        MMIO32(SDIO_SLAVE2 + 0x0ac)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF51_MSK       0xff000000
#define SLCHOST_CONF51_SFT       24
#define SLCHOST_CONF51_SET(n)    ((n<<SLCHOST_CONF51_SFT)&SLCHOST_CONF51_MSK)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF50_MSK       0x00ff0000
#define SLCHOST_CONF50_SFT       24
#define SLCHOST_CONF50_SET(n)    ((n<<SLCHOST_CONF50_SFT)&SLCHOST_CONF50_MSK)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF49_MSK       0x0000ff00
#define SLCHOST_CONF49_SFT       24
#define SLCHOST_CONF49_SET(n)    ((n<<SLCHOST_CONF49_SFT)&SLCHOST_CONF49_MSK)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF48_MSK       0x000000ff
#define SLCHOST_CONF48_SET(n)    (n&SLCHOST_CONF48_MSK)

/* Host and Slave communication register0 */
#define SLCHOST_CONF_W13_REG        MMIO32(SDIO_SLAVE2 + 0x0b0)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF55_MSK       0xff000000
#define SLCHOST_CONF55_SFT       24
#define SLCHOST_CONF55_SET(n)    ((n<<SLCHOST_CONF55_SFT)&SLCHOST_CONF55_MSK)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF54_MSK       0x00ff0000
#define SLCHOST_CONF54_SFT       24
#define SLCHOST_CONF54_SET(n)    ((n<<SLCHOST_CONF54_SFT)&SLCHOST_CONF54_MSK)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF53_MSK       0x0000ff00
#define SLCHOST_CONF53_SFT       24
#define SLCHOST_CONF53_SET(n)    ((n<<SLCHOST_CONF53_SFT)&SLCHOST_CONF53_MSK)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF52_MSK       0x000000ff
#define SLCHOST_CONF52_SET(n)    (n&SLCHOST_CONF52_MSK)

/* Host and Slave communication register0 */
#define SLCHOST_CONF_W14_REG        MMIO32(SDIO_SLAVE2 + 0x0b4)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF59_MSK       0xff000000
#define SLCHOST_CONF59_SFT       24
#define SLCHOST_CONF59_SET(n)    ((n<<SLCHOST_CONF59_SFT)&SLCHOST_CONF59_MSK)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF58_MSK       0x00ff0000
#define SLCHOST_CONF58_SFT       24
#define SLCHOST_CONF58_SET(n)    ((n<<SLCHOST_CONF58_SFT)&SLCHOST_CONF58_MSK)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF57_MSK       0x0000ff00
#define SLCHOST_CONF57_SFT       24
#define SLCHOST_CONF57_SET(n)    ((n<<SLCHOST_CONF57_SFT)&SLCHOST_CONF57_MSK)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF56_MSK       0x000000ff
#define SLCHOST_CONF56_SET(n)    (n&SLCHOST_CONF56_MSK)

/* Host and Slave communication register0 */
#define SLCHOST_CONF_W15_REG        MMIO32(SDIO_SLAVE2 + 0x0b8)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF63_MSK       0xff000000
#define SLCHOST_CONF63_SFT       24
#define SLCHOST_CONF63_SET(n)    ((n<<SLCHOST_CONF63_SFT)&SLCHOST_CONF63_MSK)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF62_MSK       0x00ff0000
#define SLCHOST_CONF62_SFT       24
#define SLCHOST_CONF62_SET(n)    ((n<<SLCHOST_CONF62_SFT)&SLCHOST_CONF62_MSK)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF61_MSK       0x0000ff00
#define SLCHOST_CONF61_SFT       24
#define SLCHOST_CONF61_SET(n)    ((n<<SLCHOST_CONF61_SFT)&SLCHOST_CONF61_MSK)
// The information interaction register between Host and Slave.
#define SLCHOST_CONF60_MSK       0x000000ff
#define SLCHOST_CONF60_SET(n)    (n&SLCHOST_CONF60_MSK)

/* Interrupt clear */
#define SLC0HOST_INT_CLR_REG        MMIO32(SDIO_SLAVE2 + 0x0d4)
// Set this bit to clear the SLC0HOST_SLC0_RX_NEW_PACKET_INT int. (RO)
#define SLC0HOST_SLC0_RX_NEW_PACKET_INT_CLR 0x800000
// Set this bit to clear the SLC0HOST_SLC0_TX_OVF_INT interrupt. (RO)
#define SLC0HOST_SLC0_TX_OVF_INT_CLR        0x020000
// Set this bit to clear the SLC0HOST_SLC0_RX_UDF_INT interrupt. (RO)
#define SLC0HOST_SLC0_RX_UDF_INT_CLR        0x010000
// Set this bit to clear the SLC0HOST_SLC0_TOHOST_BIT7_INT int. (RO)
#define SLC0HOST_SLC0_TOHOST_BIT7_INT_CLR   0x000080
// Set this bit to clear the SLC0HOST_SLC0_TOHOST_BIT6_INT int. (RO)
#define SLC0HOST_SLC0_TOHOST_BIT6_INT_CLR   0x000040
// Set this bit to clear the SLC0HOST_SLC0_TOHOST_BIT5_INT int. (RO)
#define SLC0HOST_SLC0_TOHOST_BIT5_INT_CLR   0x000020
// Set this bit to clear the SLC0HOST_SLC0_TOHOST_BIT4_INT int. (RO)
#define SLC0HOST_SLC0_TOHOST_BIT4_INT_CLR   0x000010
// Set this bit to clear the SLC0HOST_SLC0_TOHOST_BIT3_INT int. (RO)
#define SLC0HOST_SLC0_TOHOST_BIT3_INT_CLR   0x000008
// Set this bit to clear the SLC0HOST_SLC0_TOHOST_BIT2_INT int. (RO)
#define SLC0HOST_SLC0_TOHOST_BIT2_INT_CLR   0x000004
// Set this bit to clear the SLC0HOST_SLC0_TOHOST_BIT1_INT int. (RO)
#define SLC0HOST_SLC0_TOHOST_BIT1_INT_CLR   0x000002
// Set this bit to clear the SLC0HOST_SLC0_TOHOST_BIT0_INT int. (RO)
#define SLC0HOST_SLC0_TOHOST_BIT0_INT_CLR   0x000001

/* Interrupt enable */
#define SLC0HOST_FUNC1_INT_ENA_REG  MMIO32(SDIO_SLAVE2 + 0x0dc)
// The interrupt enable bit for the SLC0HOST_SLC0_RX_NEW_PACKET_INT int. (RO)
#define SLC0HOST_SLC0_RX_NEW_PACKET_INT_ENA 0x800000
// The interrupt enable bit for the SLC0HOST_SLC0_TX_OVF_INT interrupt. (RO)
#define SLC0HOST_SLC0_TX_OVF_INT_ENA        0x020000
// The interrupt enable bit for the SLC0HOST_SLC0_RX_UDF_INT interrupt. (RO)
#define SLC0HOST_SLC0_RX_UDF_INT_ENA        0x010000
// The interrupt enable bit for the SLC0HOST_SLC0_TOHOST_BIT7_INT int. (RO)
#define SLC0HOST_SLC0_TOHOST_BIT7_INT_ENA   0x000080
// The interrupt enable bit for the SLC0HOST_SLC0_TOHOST_BIT6_INT int. (RO)
#define SLC0HOST_SLC0_TOHOST_BIT6_INT_ENA   0x000040
// The interrupt enable bit for the SLC0HOST_SLC0_TOHOST_BIT5_INT int. (RO)
#define SLC0HOST_SLC0_TOHOST_BIT5_INT_ENA   0x000020
// The interrupt enable bit for the SLC0HOST_SLC0_TOHOST_BIT4_INT int. (RO)
#define SLC0HOST_SLC0_TOHOST_BIT4_INT_ENA   0x000010
// The interrupt enable bit for the SLC0HOST_SLC0_TOHOST_BIT3_INT int. (RO)
#define SLC0HOST_SLC0_TOHOST_BIT3_INT_ENA   0x000008
// The interrupt enable bit for the SLC0HOST_SLC0_TOHOST_BIT2_INT int. (RO)
#define SLC0HOST_SLC0_TOHOST_BIT2_INT_ENA   0x000004
// The interrupt enable bit for the SLC0HOST_SLC0_TOHOST_BIT1_INT int. (RO)
#define SLC0HOST_SLC0_TOHOST_BIT1_INT_ENA   0x000002
// The interrupt enable bit for the SLC0HOST_SLC0_TOHOST_BIT0_INT int. (RO)
#define SLC0HOST_SLC0_TOHOST_BIT0_INT_ENA   0x000001

/* Edge configuration */
#define SLCHOST_CONF_REG            MMIO32(SDIO_SLAVE2 + 0x1f0)
// Set this bit and HINF_HIGHSPEED_ENABLE
#define SLCHOST_HSPEED_CON_EN       0x8000000
// Set this bit to sample the corresponding signal at the rising clock edge.
#define SLCHOST_FRC_POS_SAMP_MSK    0x00f8000
#define SLCHOST_FRC_POS_SAMP_SFT    15
// Set this bit to sample the corresponding signal at the falling clock edge.
#define SLCHOST_FRC_NEG_SAMP_MSK    0x0007c00
#define SLCHOST_FRC_NEG_SAMP_SFT    10
// Set this bit to output the corresponding signal at the rising clock edge.
#define SLCHOST_FRC_SDIO20_MSK      0x00003e0
#define SLCHOST_FRC_SDIO20_SFT      5
// Set this bit to output the corresponding signal at the falling clock edge.
#define SLCHOST_FRC_SDIO11_MSK      0x000001f

/* SDIO specification configuration */
#define HINF_CFG_DATA1_REG          MMIO32(SDIO_SLAVE1 + 0x004)
// Please initialize to 1. Do not modify it. (R/W)
#define HINF_HIGHSPEED_ENABLE   0x2
// Please initialize to 1. Do not modify it. (R/W)
#define HINF_SDIO_IOREADY1      0x1

#endif
