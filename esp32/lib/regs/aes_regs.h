#ifndef H_AES_REGS
#define H_AES_REGS
/*
 * ESP32 alternative library, AES Accelerator registers.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Mode of operation of the AES Accelerator */
#define AES_MODE_REG    MMIO32(AES_ACCELERATOR + 0x08)
// Selects the AES accelerator mode of operation.
#define AES_MODE128ENC  0x0
#define AES_MODE192ENC  0x1
#define AES_MODE256ENC  0x2
#define AES_MODE128DEC  0x4
#define AES_MODE192DEC  0x5
#define AES_MODE256DEC  0x6

/* Endianness configuration register */
#define AES_ENDIAN_REG  MMIO32(AES_ACCELERATOR + 0x40)
// Endianness selection register.
#define AES_ENDIAN_MSK  0x3f
// #define AES_ENDIAN_AEX_TEXT_3_REG_31_24 0x00
// #define AES_ENDIAN_AEX_TEXT_3_REG_23_16
// #define AES_ENDIAN_AEX_TEXT_3_REG_15_8
// #define AES_ENDIAN_AEX_TEXT_3_REG_7_0
// #define AES_ENDIAN_AEX_TEXT_2_REG_31_24
// #define AES_ENDIAN_AEX_TEXT_2_REG_23_16
// #define AES_ENDIAN_AEX_TEXT_2_REG_15_8
// #define AES_ENDIAN_AEX_TEXT_2_REG_7_0
// #define AES_ENDIAN_AEX_TEXT_1_REG_31_24
// #define AES_ENDIAN_AEX_TEXT_1_REG_23_16
// #define AES_ENDIAN_AEX_TEXT_1_REG_15_8
// #define AES_ENDIAN_AEX_TEXT_1_REG_7_0
// #define AES_ENDIAN_AEX_TEXT_0_REG_31_24
// #define AES_ENDIAN_AEX_TEXT_0_REG_23_16
// #define AES_ENDIAN_AEX_TEXT_0_REG_15_8
// #define AES_ENDIAN_AEX_TEXT_0_REG_7_0

/* AES key material register n */
#define AES_KEY_0_REG   MMIO32(AES_ACCELERATOR + 0x10)
#define AES_KEY_1_REG   MMIO32(AES_ACCELERATOR + 0x14)
#define AES_KEY_2_REG   MMIO32(AES_ACCELERATOR + 0x18)
#define AES_KEY_3_REG   MMIO32(AES_ACCELERATOR + 0x1c)
#define AES_KEY_4_REG   MMIO32(AES_ACCELERATOR + 0x20)
#define AES_KEY_5_REG   MMIO32(AES_ACCELERATOR + 0x24)
#define AES_KEY_6_REG   MMIO32(AES_ACCELERATOR + 0x28)
#define AES_KEY_7_REG   MMIO32(AES_ACCELERATOR + 0x2c)

/* AES encrypted/decrypted data register n */
#define AES_TEXT_0_REG  MMIO32(AES_ACCELERATOR + 0x30)
#define AES_TEXT_1_REG  MMIO32(AES_ACCELERATOR + 0x34)
#define AES_TEXT_2_REG  MMIO32(AES_ACCELERATOR + 0x38)
#define AES_TEXT_3_REG  MMIO32(AES_ACCELERATOR + 0x3c)
// Plaintext and ciphertext register. (R/W)

/* AES operation start control register */
#define AES_START_REG   MMIO32(AES_ACCELERATOR + 0x00)
// Write 1 to start the AES operation. (WO)
#define AES_START   0x1

/* AES idle status register */
#define AES_IDLE_REG    MMIO32(AES_ACCELERATOR + 0x04)
// AES Idle register. Reads ’zero’ while the AES Accelerator is busy processing;
#define AES_IDLE    0x1

#endif
