#ifndef H_EFUSE_REGS
#define H_EFUSE_REGS
/*
 * ESP32 alternative library, eFuse Controller registers.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Returns data word 0 in eFuse BLOCK 0 */
#define EFUSE_BLK0_RDATA0_REG   MMIO32(EFUSE_CONTROLLER + 0x000)
// This bit returns the value of uart_download_dis. Valid only for ESP32 . (RO)
#define EFUSE_RD_UART_DOWNLOAD_DIS      0x8000000
// This field returns the value of flash_crypt_cnt. (RO)
#define EFUSE_RD_FLASH_CRYPT_CNT_MSK    0x7f00000
#define EFUSE_RD_FLASH_CRYPT_CNT_SFT    20
#define EFUSE_RD_FLASH_CRYPT_CNT_GET ((EFUSE_BLK0_RDATA0_REG>>EFUSE_RD_FLASH_CRYPT_CNT_SFT)&0x7f)
// This field returns the value of efuse_rd_disable. (RO)
#define EFUSE_RD_EFUSE_RD_DIS_MSK       0x00f0000
#define EFUSE_RD_EFUSE_RD_DIS_SFT       16
#define EFUSE_RD_EFUSE_RD_DIS_GET ((EFUSE_BLK0_RDATA0_REG>>EFUSE_RD_EFUSE_RD_DIS_SFT)&0xf)
// This field returns the value of efuse_wr_disable. (RO)
#define EFUSE_RD_EFUSE_WR_DIS_MSK       0x000ffff
#define EFUSE_RD_EFUSE_WR_DIS_GET (EFUSE_BLK0_RDATA0_REG&EFUSE_RD_EFUSE_WR_DIS_MSK)

#define EFUSE_BLK0_RDATA1_REG   MMIO32(EFUSE_CONTROLLER + 0x004)
// This field returns the value of the lower 32 bits of WIFI_MAC_Address. (RO)

#define EFUSE_BLK0_RDATA2_REG   MMIO32(EFUSE_CONTROLLER + 0x008)
// This field returns the value of the higher 24 bits of WIFI_MAC_Address. (RO)
#define EFUSE_RD_WIFI_MAC_CRC_HIGH  0xffffff

#define EFUSE_BLK0_RDATA3_REG   MMIO32(EFUSE_CONTROLLER + 0x00c)
// first three id bits of chip packaging version among the four identification bits.
#define EFUSE_RD_CHIP_VER_PKG_MSK       0xe00
#define EFUSE_RD_CHIP_VER_PKG_SFT       9
#define EFUSE_RD_CHIP_VER_PKG_GET ((EFUSE_BLK0_RDATA3_REG>>EFUSE_RD_CHIP_VER_PKG_SFT)&0x7)
// This field returns the value of SPI_pad_config_hd. (RO)
#define EFUSE_RD_SPI_PAD_CONFIG_HD_MSK  0x1f0
#define EFUSE_RD_SPI_PAD_CONFIG_HD_SFT  4
#define EFUSE_RD_SPI_PAD_CONFIG_HD_GET ((EFUSE_BLK0_RDATA3_REG>>EFUSE_RD_SPI_PAD_CONFIG_HD_SFT)&0x1f)
// Disables cache. (RO)
#define EFUSE_RD_CHIP_VER_DIS_CACHE     0x008
// fourth id bit of chip packaging version among the four identification bits. (RO)
#define EFUSE_RD_CHIP_VER_PKG           0x004
// Disables Bluetooth. (RO)
#define EFUSE_RD_CHIP_VER_DIS_BT        0x002
// Disables APP CPU. (RO)
#define EFUSE_RD_CHIP_VER_DIS_APP_CPU   0x001

#define EFUSE_BLK0_RDATA4_REG   MMIO32(EFUSE_CONTROLLER + 0x010)
// This field returns the value of sdio_force. (RO)
#define EFUSE_RD_SDIO_FORCE     0x10000
// This field returns the value of SDIO_TIEH. (RO)
#define EFUSE_RD_SDIO_TIEH      0x08000
// This field returns the value of XPD_SDIO_REG. (RO)
#define EFUSE_RD_XPD_SDIO       0x04000
// RC_FAST_CLK frequency. (RO)
#define ESFUSE_RD_CK8M_FREQ_MSK 0x000ff
#define ESFUSE_RD_CK8M_FREQ_GET (EFUSE_BLK0_RDATA4_REG&ESFUSE_RD_CK8M_FREQ_MSK)

#define EFUSE_BLK0_RDATA5_REG   MMIO32(EFUSE_CONTROLLER + 0x014)
// This field returns the value of flash_crypt_config. (RO)
#define EFUSE_RD_FLASH_CRYPT_CONFIG_MSK 0xf0000000
#define EFUSE_RD_FLASH_CRYPT_CONFIG_SFT 28
#define EFUSE_RD_FLASH_CRYPT_CONFIG_GET ((EFUSE_BLK0_RDATA5_REG>>EFUSE_RD_FLASH_CRYPT_CONFIG_SFT)&0xf)
// difference between the digital regulator voltage at level 6 and 1.2 V. (RO)
#define EFUSE_RD_DIG_VOL_L6_MSK         0x0f000000
#define EFUSE_RD_DIG_VOL_L6_SFT         24
#define EFUSE_RD_DIG_VOL_L6_GET ((EFUSE_BLK0_RDATA5_REG>>EFUSE_RD_DIG_VOL_L6_SFT)&0xf)
// voltage level for CPU to run at 240 MHz, or for flash/PSRAM to run at 80 MHz.
#define EFUSE_RD_VOL_LEVEL_HP_INV_7     0x00000000
#define EFUSE_RD_VOL_LEVEL_HP_INV_6     0x00400000
#define EFUSE_RD_VOL_LEVEL_HP_INV_5     0x00800000
#define EFUSE_RD_VOL_LEVEL_HP_INV_4     0x00c00000
// This field returns the value of SPI_pad_config_cs0. (RO)
#define EFUSE_RD_SPI_PAD_CONFIG_CS0_MSK 0x000f8000
#define EFUSE_RD_SPI_PAD_CONFIG_CS0_SFT 15
#define EFUSE_RD_SPI_PAD_CONFIG_CS0_GET ((EFUSE_BLK0_RDATA5_REG>>EFUSE_RD_SPI_PAD_CONFIG_CS0_SFT)&0x1f)
// This field returns the value of SPI_pad_config_d. (RO)
#define EFUSE_RD_SPI_PAD_CONFIG_D_MSK   0x00007c00
#define EFUSE_RD_SPI_PAD_CONFIG_D_SFT   10
#define EFUSE_RD_SPI_PAD_CONFIG_D_GET ((EFUSE_BLK0_RDATA5_REG>>EFUSE_RD_SPI_PAD_CONFIG_D_SFT)&0x1f)
// This field returns the value of SPI_pad_config_q. (RO)
#define EFUSE_RD_SPI_PAD_CONFIG_Q_MSK   0x000003e0
#define EFUSE_RD_SPI_PAD_CONFIG_Q_SFT   5
#define EFUSE_RD_SPI_PAD_CONFIG_Q_GET ((EFUSE_BLK0_RDATA5_REG>>EFUSE_RD_SPI_PAD_CONFIG_Q_SFT)&0x1f)
// This field returns the value of SPI_pad_config_clk. (RO)
#define EFUSE_RD_SPI_PAD_CONFIG_CLK_MSK 0x0000001f
#define EFUSE_RD_SPI_PAD_CONFIG_CLK_GET (EFUSE_BLK0_RDATA5_REG&EFUSE_RD_SPI_PAD_CONFIG_CLK_MSK)

#define EFUSE_BLK0_RDATA6_REG   MMIO32(EFUSE_CONTROLLER + 0x018)
// This field returns the value of key_status. (RO)
#define EFUSE_RD_KEY_STATUS             0x200
// This field returns the value of download_dis_cache. (RO)
#define EFUSE_RD_DISABLE_DL_CACHE       0x100
// This field returns the value of download_dis_decrypt. (RO)
#define EFUSE_RD_DISABLE_DL_DECRYPT     0x080
// This field returns the value of download_dis_encrypt. (RO)
#define EFUSE_RD_DISABLE_DL_ENCRYPT     0x040
// This field returns the value of JTAG_disable. (RO)
#define EFUSE_RD_DISABLE_JTAG           0x020
// This field returns the value of abstract_done_1. (RO)
#define EFUSE_RD_ABS_DONE_1             0x010
// This field returns the value of abstract_done_0. (RO)
#define EFUSE_RD_ABS_DONE_0             0x008
// This field returns the value of console_debug_disable. (RO)
#define EFUSE_RD_CONSOLE_DEBUG_DISABLE  0x004
// This field returns the value of coding_scheme. (RO)
#define EFUSE_RD_CODING_SCHEME_MSK      0x003
#define EFUSE_RD_CODING_SCHEME_GET  (EFUSE_BLK0_RDATA6_REG&EFUSE_RD_CODING_SCHEME_MSK)

// This field returns the value of word n in BLOCKn. (RO)
#define EFUSE_BLK1_RDATA0_REG   MMIO32(EFUSE_CONTROLLER + 0x038)
#define EFUSE_BLK1_RDATA1_REG   MMIO32(EFUSE_CONTROLLER + 0x03c)
#define EFUSE_BLK1_RDATA2_REG   MMIO32(EFUSE_CONTROLLER + 0x040)
#define EFUSE_BLK1_RDATA3_REG   MMIO32(EFUSE_CONTROLLER + 0x044)
#define EFUSE_BLK1_RDATA4_REG   MMIO32(EFUSE_CONTROLLER + 0x048)
#define EFUSE_BLK1_RDATA5_REG   MMIO32(EFUSE_CONTROLLER + 0x04c)
#define EFUSE_BLK1_RDATA6_REG   MMIO32(EFUSE_CONTROLLER + 0x050)
#define EFUSE_BLK1_RDATA7_REG   MMIO32(EFUSE_CONTROLLER + 0x054)
#define EFUSE_BLK2_RDATA0_REG   MMIO32(EFUSE_CONTROLLER + 0x058)
#define EFUSE_BLK2_RDATA1_REG   MMIO32(EFUSE_CONTROLLER + 0x05c)
#define EFUSE_BLK2_RDATA2_REG   MMIO32(EFUSE_CONTROLLER + 0x060)
#define EFUSE_BLK2_RDATA3_REG   MMIO32(EFUSE_CONTROLLER + 0x064)
#define EFUSE_BLK2_RDATA4_REG   MMIO32(EFUSE_CONTROLLER + 0x068)
#define EFUSE_BLK2_RDATA5_REG   MMIO32(EFUSE_CONTROLLER + 0x06c)
#define EFUSE_BLK2_RDATA6_REG   MMIO32(EFUSE_CONTROLLER + 0x070)
#define EFUSE_BLK2_RDATA7_REG   MMIO32(EFUSE_CONTROLLER + 0x074)
#define EFUSE_BLK3_RDATA0_REG   MMIO32(EFUSE_CONTROLLER + 0x078)
#define EFUSE_BLK3_RDATA1_REG   MMIO32(EFUSE_CONTROLLER + 0x07c)
#define EFUSE_BLK3_RDATA2_REG   MMIO32(EFUSE_CONTROLLER + 0x080)
#define EFUSE_BLK3_RDATA3_REG   MMIO32(EFUSE_CONTROLLER + 0x084)
#define EFUSE_BLK3_RDATA4_REG   MMIO32(EFUSE_CONTROLLER + 0x088)
#define EFUSE_BLK3_RDATA5_REG   MMIO32(EFUSE_CONTROLLER + 0x08c)
#define EFUSE_BLK3_RDATA6_REG   MMIO32(EFUSE_CONTROLLER + 0x090)
#define EFUSE_BLK3_RDATA7_REG   MMIO32(EFUSE_CONTROLLER + 0x094)

/* Writes data to word 0 in eFuse BLOCK 0 */
#define EFUSE_BLK0_WDATA0_REG   MMIO32(EFUSE_CONTROLLER + 0x01c)
// This bit programs the the value of uart_download_dis. Valid only for ESP32.
#define EFUSE_UART_DOWNLOAD_DIS      0x8000000
// This field programs the value of flash_crypt_cnt. (R/W)
#define EFUSE_FLASH_CRYPT_CNT_MSK    0x7f00000
#define EFUSE_FLASH_CRYPT_CNT_SFT    20
#define EFUSE_FLASH_CRYPT_CNT_SET(n) ((n<<EFUSE_RD_FLASH_CRYPT_CNT_SFT)&EFUSE_FLASH_CRYPT_CNT_MSK)
// This field programs thevalue of efuse_rd_disable. (R/W)
#define EFUSE_EFUSE_RD_DIS_MSK       0x00f0000
#define EFUSE_EFUSE_RD_DIS_SFT       16
#define EFUSE_EFUSE_RD_DIS_SET(n) ((n<<EFUSE_RD_EFUSE_RD_DIS_SFT)&EFUSE_EFUSE_RD_DIS_MSK)
// This field programs the the value of efuse_wr_disable. (R/W)
#define EFUSE_EFUSE_WR_DIS_MSK       0x000ffff
#define EFUSE_EFUSE_WR_DIS_SET(n) (n&EFUSE_RD_EFUSE_WR_DIS_MSK)

#define EFUSE_BLK0_WDATA1_REG   MMIO32(EFUSE_CONTROLLER + 0x020)
// This field programs the value of lower 32 bits of WIFI_MAC_Address. (R/W)

#define EFUSE_BLK0_WDATA2_REG   MMIO32(EFUSE_CONTROLLER + 0x024)
// This field programs the value of higher 24 bits of WIFI_MAC_Address. (R/W)
#define EFUSE_WIFI_MAC_CRC_HIGH_MSK 0xffffff

#define EFUSE_BLK0_WDATA3_REG   MMIO32(EFUSE_CONTROLLER + 0x028)
// first three id bits of chip packaging version among the four identification bits.
#define EFUSE_CHIP_VER_PKG_MSK       0xe00
#define EFUSE_CHIP_VER_PKG_SFT       9
#define EFUSE_CHIP_VER_PKG_SET(n) ((n<<EFUSE_RD_CHIP_VER_PKG_SFT)&EFUSE_CHIP_VER_PKG_MSK)
// This field returns the value of SPI_pad_config_hd. (R/W)
#define EFUSE_SPI_PAD_CONFIG_HD_MSK  0x1f0
#define EFUSE_SPI_PAD_CONFIG_HD_SFT  4
#define EFUSE_SPI_PAD_CONFIG_HD_SET(n) ((n<<EFUSE_RD_SPI_PAD_CONFIG_HD_SFT)&EFUSE_SPI_PAD_CONFIG_HD_MSK)
// Disables cache. (R/W)
#define EFUSE_CHIP_VER_DIS_CACHE     0x008
// fourth id bit of chip packaging version among the four identification bits. (R/W)
#define EFUSE_CHIP_VER_PKG           0x004
// Disables Bluetooth. (R/W)
#define EFUSE_CHIP_VER_DIS_BT        0x002
// Disables APP CPU. (R/W)
#define EFUSE_CHIP_VER_DIS_APP_CPU   0x001

#define EFUSE_BLK0_WDATA4_REG   MMIO32(EFUSE_CONTROLLER + 0x02c)
// This field programs the value of sdio_force. (R/W)
#define EFUSE_SDIO_FORCE        0x10000
// This field programs the value of SDIO_TIEH. (R/W)
#define EFUSE_SDIO_TIEH         0x08000
// This field programs the value of XPD_SDIO_REG. (R/W)
#define EFUSE_XPD_SDIO          0x04000
// RC_FAST_CLK frequency. (R/W)
#define ESFUSE_CK8M_FREQ_MSK    0x000ff
#define ESFUSE_CK8M_FREQ_SET(n) (n&ESFUSE_RD_CK8M_FREQ_MSK)

#define EFUSE_BLK0_WDATA5_REG   MMIO32(EFUSE_CONTROLLER + 0x030)
// This field programs the value of flash_crypt_config. (R/W)
#define EFUSE_FLASH_CRYPT_CONFIG_MSK 0xf0000000
#define EFUSE_FLASH_CRYPT_CONFIG_SFT 28
#define EFUSE_FLASH_CRYPT_CONFIG_SET(n) ((n<<EFUSE_RD_FLASH_CRYPT_CONFIG_SFT)&EFUSE_FLASH_CRYPT_CONFIG_MSK)
// difference between the digital regulator voltage at level 6 and 1.2 V. (R/W)
#define EFUSE_DIG_VOL_L6_MSK         0x0f000000
#define EFUSE_DIG_VOL_L6_SFT         24
#define EFUSE_DIG_VOL_L6_SET(n) ((n<<EFUSE_RD_DIG_VOL_L6_SFT)&EFUSE_DIG_VOL_L6_MSK)
// voltage level for CPU to run at 240 MHz, or for flash/PSRAM to run at 80 MHz.
#define EFUSE_VOL_LEVEL_HP_INV_7     0x00000000
#define EFUSE_VOL_LEVEL_HP_INV_6     0x00400000
#define EFUSE_VOL_LEVEL_HP_INV_5     0x00800000
#define EFUSE_VOL_LEVEL_HP_INV_4     0x00c00000
// This field stores the value of SPI_pad_config_cs0. (R/W)
#define EFUSE_SPI_PAD_CONFIG_CS0_MSK 0x000f8000
#define EFUSE_SPI_PAD_CONFIG_CS0_SFT 15
#define EFUSE_SPI_PAD_CONFIG_CS0_SET(n) ((n<<EFUSE_RD_SPI_PAD_CONFIG_CS0_SFT)&EFUSE_SPI_PAD_CONFIG_CS0_SFT)
// This field stores the value of SPI_pad_config_d. (R/W)
#define EFUSE_SPI_PAD_CONFIG_D_MSK   0x00007c00
#define EFUSE_SPI_PAD_CONFIG_D_SFT   10
#define EFUSE_SPI_PAD_CONFIG_D_SET(n) ((EFUSE_BLK0_RDATA5_REG>>EFUSE_RD_SPI_PAD_CONFIG_D_SFT)&EFUSE_SPI_PAD_CONFIG_D_SFT)
// This field stores the value of SPI_pad_config_q. (R/W)
#define EFUSE_SPI_PAD_CONFIG_Q_MSK   0x000003e0
#define EFUSE_SPI_PAD_CONFIG_Q_SFT   5
#define EFUSE_SPI_PAD_CONFIG_Q_SET(n) ((n<<EFUSE_RD_SPI_PAD_CONFIG_Q_SFT)&EFUSE_SPI_PAD_CONFIG_Q_SFT)
// This field stores the value of SPI_pad_config_clk. (R/W)
#define EFUSE_SPI_PAD_CONFIG_CLK_MSK 0x0000001f
#define EFUSE_SPI_PAD_CONFIG_CLK_SET(n) (n&EFUSE_RD_SPI_PAD_CONFIG_CLK_MSK)

#define EFUSE_BLK0_WDATA6_REG   MMIO32(EFUSE_CONTROLLER + 0x034)
// This field programs the value of key_status. (R/W)
#define EFUSE_KEY_STATUS             0x200
// This field programs the value of download_dis_cache. (R/W)
#define EFUSE_DISABLE_DL_CACHE       0x100
// This field programs the value of download_dis_decrypt. (R/W)
#define EFUSE_DISABLE_DL_DECRYPT     0x080
// This field programs the value of download_dis_encrypt. (R/W)
#define EFUSE_DISABLE_DL_ENCRYPT     0x040
// This field programs the value of JTAG_disable. (R/W)
#define EFUSE_DISABLE_JTAG           0x020
// This field programs the value of abstract_done_1. (R/W)
#define EFUSE_ABS_DONE_1             0x010
// This field programs the value of abstract_done_0. (R/W)
#define EFUSE_ABS_DONE_0             0x008
// This field programs the value of console_debug_disable. (R/W)
#define EFUSE_CONSOLE_DEBUG_DISABLE  0x004
// This field programs the value of coding_scheme. (R/W)
#define EFUSE_CODING_SCHEME_MSK      0x003
#define EFUSE_CODING_SCHEME_SET(n)  (n&EFUSE_RD_CODING_SCHEME_MSK)

// This field programs the value of word n in of BLOCKn. (R/W)
#define EFUSE_BLK1_WDATA0_REG   MMIO32(EFUSE_CONTROLLER + 0x098)
#define EFUSE_BLK1_WDATA1_REG   MMIO32(EFUSE_CONTROLLER + 0x09c)
#define EFUSE_BLK1_WDATA2_REG   MMIO32(EFUSE_CONTROLLER + 0x0a0)
#define EFUSE_BLK1_WDATA3_REG   MMIO32(EFUSE_CONTROLLER + 0x0a4)
#define EFUSE_BLK1_WDATA4_REG   MMIO32(EFUSE_CONTROLLER + 0x0a8)
#define EFUSE_BLK1_WDATA5_REG   MMIO32(EFUSE_CONTROLLER + 0x0ac)
#define EFUSE_BLK1_WDATA6_REG   MMIO32(EFUSE_CONTROLLER + 0x0b0)
#define EFUSE_BLK1_WDATA7_REG   MMIO32(EFUSE_CONTROLLER + 0x0b4)
#define EFUSE_BLK2_WDATA0_REG   MMIO32(EFUSE_CONTROLLER + 0x0b8)
#define EFUSE_BLK2_WDATA1_REG   MMIO32(EFUSE_CONTROLLER + 0x0bc)
#define EFUSE_BLK2_WDATA2_REG   MMIO32(EFUSE_CONTROLLER + 0x0c0)
#define EFUSE_BLK2_WDATA3_REG   MMIO32(EFUSE_CONTROLLER + 0x0c4)
#define EFUSE_BLK2_WDATA4_REG   MMIO32(EFUSE_CONTROLLER + 0x0c8)
#define EFUSE_BLK2_WDATA5_REG   MMIO32(EFUSE_CONTROLLER + 0x0cc)
#define EFUSE_BLK2_WDATA6_REG   MMIO32(EFUSE_CONTROLLER + 0x0d0)
#define EFUSE_BLK2_WDATA7_REG   MMIO32(EFUSE_CONTROLLER + 0x0d4)
#define EFUSE_BLK3_WDATA0_REG   MMIO32(EFUSE_CONTROLLER + 0x0d8)
#define EFUSE_BLK3_WDATA1_REG   MMIO32(EFUSE_CONTROLLER + 0x0dc)
#define EFUSE_BLK3_WDATA2_REG   MMIO32(EFUSE_CONTROLLER + 0x0e0)
#define EFUSE_BLK3_WDATA3_REG   MMIO32(EFUSE_CONTROLLER + 0x0e4)
#define EFUSE_BLK3_WDATA4_REG   MMIO32(EFUSE_CONTROLLER + 0x0e8)
#define EFUSE_BLK3_WDATA5_REG   MMIO32(EFUSE_CONTROLLER + 0x0ec)
#define EFUSE_BLK3_WDATA6_REG   MMIO32(EFUSE_CONTROLLER + 0x0f0)
#define EFUSE_BLK3_WDATA7_REG   MMIO32(EFUSE_CONTROLLER + 0x0f4)

/* Timing configuration register */
#define EFUSE_CLK_REG           MMIO32(EFUSE_CONTROLLER + 0x0f8)
// eFuse clock configuration field. (R/W)
#define EFUSE_CLK_SEL1_MSK      0xff00
#define EFUSE_CLK_SEL1_SFT      8
#define EFUSE_CLK_SEL1_SET(n)   ((n<<EFUSE_CLK_SEL1_SFT)&EFUSE_CLK_SEL1_MSK)
// eFuse clock configuration field. (R/W)
#define EFUSE_CLK_SEL0_MSK      0x00ff
#define EFUSE_CLK_SEL0_SET(n)   (n&EFUSE_CLK_SEL0_MSK)

/* Opcode register */
#define EFUSE_CONF_REG          MMIO32(EFUSE_CONTROLLER + 0x0fc)
// eFuse operation code register. (R/W)
#define EFUSE_OP_CODE_MSK   0xffff

/* Read/write command register */
#define EFUSE_CMD_REG           MMIO32(EFUSE_CONTROLLER + 0x104)
// Set this to 1 to start a program operation.
#define EFUSE_PGM_CMD   0x2
// Set this to 1 to start a read operation.
#define EFUSE_READ_CMD  0x1

/* Raw interrupt status */
#define EFUSE_INT_RAW_REG       MMIO32(EFUSE_CONTROLLER + 0x108)
// The raw interrupt status bit for the EFUSE_PGM_DONE_INT interrupt. (RO)
#define EFUSE_PGM_DONE_INT_RAW  0x2
// The raw interrupt status bit for the EFUSE_READ_DONE_INT interrupt. (RO)
#define EFUSE_READ_DONE_INT_RAW 0x1

/* Masked interrupt status */
#define EFUSE_INT_ST_REG        MMIO32(EFUSE_CONTROLLER + 0x10c)
// The masked interrupt status bit for the EFUSE_PGM_DONE_INT interrupt. (RO)
#define EFUSE_PGM_DONE_INT_ST  0x2
// The masked interrupt status bit for the EFUSE_READ_DONE_INT interrupt. (RO)
#define EFUSE_READ_DONE_INT_ST 0x1

/* Interrupt enable bits */
#define EFUSE_INT_ENA_REG       MMIO32(EFUSE_CONTROLLER + 0x110)
// The interrupt enable bit for the EFUSE_PGM_DONE_INT interrupt. (R/W)
#define EFUSE_PGM_DONE_INT_ENA  0x2
// The interrupt enable bit for the EFUSE_READ_DONE_INT interrupt. (R/W)
#define EFUSE_READ_DONE_INT_ENA 0x1

/* Interrupt clear bits */
#define EFUSE_INT_CLR_REG       MMIO32(EFUSE_CONTROLLER + 0x114)
// Set this bit to clear the EFUSE_PGM_DONE_INT interrupt. (WO)
#define EFUSE_PGM_DONE_INT_CLR  0x2
// Set this bit to clear the EFUSE_READ_DONE_INT interrupt. (WO)
#define EFUSE_READ_DONE_INT_CLR 0x1

/* Efuse timing configuration */
#define EFUSE_DAC_CONF_REG      MMIO32(EFUSE_CONTROLLER + 0x118)
// eFuse timing configuration register. (R/W)
#define EFUSE_DAC_CLK_DIV_MSK   0xff

/* Status of 3/4 coding scheme */
#define EFUSE_DEC_STATUS_REG    MMIO32(EFUSE_CONTROLLER + 0x11c)
// some errors were corrected while decoding the 3/4 encoding scheme. (RO)
#define EFUSE_DEC_WARNINGS_MSK  0xfff


#endif
