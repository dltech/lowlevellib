#ifndef H_TDA8425_REGS
#define H_TDA8425_REGS
/*
 * TDA8425 Hi-fi stereo audio processor; I2C-bus. Register definitions.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define TDA8425_ADDRESS 0x82

#define VOLUME_L    0x00    /* volume left */
#define VOLUME_R    0x01    /* volume right */
// volume in 2dB step from 6 to -80
#define VOLUME_MSK  0x3f
#define VOLUME_MAX  0x3f
#define VOLUME_MIN  0x1b
#define VOLUME6DB   0xff
#define VOLUME4DB   0xfe
#define VOLUME2DB   0xfd
#define VOLUME0DB   0xfc
#define VOLUMEM2DB  0xfb
#define VOLUMEM4DB  0xfa
#define VOLUMEM6DB  0xf9
#define VOLUMEM8DB  0xf8
#define VOLUMEM10DB 0xf7
#define VOLUMEM12DB 0xf6
#define VOLUMEM14DB 0xf5
#define VOLUMEM16DB 0xf4
#define VOLUMEM18DB 0xf3
#define VOLUMEM20DB 0xf2
#define VOLUMEM22DB 0xf1
#define VOLUMEM24DB 0xf0
#define VOLUMEM26DB 0xef
#define VOLUMEM28DB 0xee
#define VOLUMEM30DB 0xed
#define VOLUMEM32DB 0xec
#define VOLUMEM34DB 0xeb
#define VOLUMEM36DB 0xea
#define VOLUMEM38DB 0xe9
#define VOLUMEM40DB 0xe8
#define VOLUMEM42DB 0xe7
#define VOLUMEM44DB 0xe6
#define VOLUMEM46DB 0xe5
#define VOLUMEM48DB 0xe4
#define VOLUMEM50DB 0xe3
#define VOLUMEM52DB 0xe2
#define VOLUMEM54DB 0xe1
#define VOLUMEM56DB 0xe0
#define VOLUMEM58DB 0xdf
#define VOLUMEM60DB 0xde
#define VOLUMEM62DB 0xdd
#define VOLUMEM64DB 0xdc
#define VOLUMEM80DB 0xd0

#define BASS        0x02    /* bass */
// Bass control in 3 dB/step
#define BASS_MSK    0x0f
#define BASS_MAX    0x0f
#define BASS15DB    0xff
#define BASS12DB    0xfa
#define BASS9DB     0xf9
#define BASS6DB     0xf8
#define BASS3DB     0xf7
#define BASS0DB     0xf6
#define BASSM3DB    0xf5
#define BASSM6DB    0xf4
#define BASSM9DB    0xf3
#define BASSM12DB   0xf0

#define TREBLE      0x03    /* treble */
// Treble control in 3 dB/step
#define TREBLE_MSK  0x0f
#define TREBLE_MAX  0x0f
#define TREBLE12DB  0xff
#define TREBLE9DB   0xf9
#define TREBLE6DB   0xf8
#define TREBLE3DB   0xf7
#define TREBLE0DB   0xf6
#define TREBLEM3DB  0xf5
#define TREBLEM6DB  0xf4
#define TREBLEM9DB  0xf3
#define TREBLEM12DB 0xf0

#define SWITCH      0x08    /* switch functions */
// Mute
#define MU          0x20
// effects on (spatial stereo or pseudo mute)
#define EFL         0x10
// stereo on (stereo or mute)
#define STL         0x08
// Source selector
#define ML_STEREO   0x06
#define ML_L        0x02
#define ML_R        0x04
// input source in1 or in2
#define IS          0x01
// selector presets
#define SPATIAL_STEREO  0xde
#define SPATIAL_STEREO1 0xde
#define SPATIAL_STEREO2 0xdf
#define LINEAR_STEREO   0xce
#define LINEAR_STEREO1  0xce
#define LINEAR_STEREO2  0xcf
#define PSEUDO_STEREOL  0xd2
#define PSEUDO_STEREOR  0xd4
#define PSEUDO_STEREO1L 0xd2
#define PSEUDO_STEREO1R 0xd4
#define PSEUDO_STEREO2L 0xd3
#define PSEUDO_STEREO2R 0xd5
#define FORCED_MONO_L   0xc2
#define FORCED_MONO_R   0xc4
#define FORCED_MONO1L   0xc2
#define FORCED_MONO1R   0xc4
#define FORCED_MONO2L   0xc3
#define FORCED_MONO2R   0xc5

#endif
