#ifndef H_USB_REGS
#define H_USB_REGS
/*
 * RP2040 alternative library, a USB 2.0 controller registers
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "system_regs/memorymap.h"

/* Device address and endpoint control */
#define ADDR_ENDP               MMIO32(USBCTRL_REGS_BASE + 0x00)
// Device endpoint to send data to. Only valid for HOST mode.
#define ENDPOINT_MSK    0xf0000
#define ENDPOINT_SFT    16
#define ENDPOINT(n)     ((n&0xf)<<ENDPOINT_SFT)
// In device mode, the address that the device should respond to
#define ADDRESS_MSK     0x3f

/* Interrupt endpoint n. Only valid for HOST mode. */
#define ADDR_ENDP1              MMIO32(USBCTRL_REGS_BASE + 0x04)
#define ADDR_ENDP2              MMIO32(USBCTRL_REGS_BASE + 0x08)
#define ADDR_ENDP3              MMIO32(USBCTRL_REGS_BASE + 0x0c)
#define ADDR_ENDP4              MMIO32(USBCTRL_REGS_BASE + 0x10)
#define ADDR_ENDP5              MMIO32(USBCTRL_REGS_BASE + 0x14)
#define ADDR_ENDP6              MMIO32(USBCTRL_REGS_BASE + 0x18)
#define ADDR_ENDP7              MMIO32(USBCTRL_REGS_BASE + 0x1c)
#define ADDR_ENDP8              MMIO32(USBCTRL_REGS_BASE + 0x20)
#define ADDR_ENDP9              MMIO32(USBCTRL_REGS_BASE + 0x24)
#define ADDR_ENDP10             MMIO32(USBCTRL_REGS_BASE + 0x28)
#define ADDR_ENDP11             MMIO32(USBCTRL_REGS_BASE + 0x2c)
#define ADDR_ENDP12             MMIO32(USBCTRL_REGS_BASE + 0x30)
#define ADDR_ENDP13             MMIO32(USBCTRL_REGS_BASE + 0x34)
#define ADDR_ENDP14             MMIO32(USBCTRL_REGS_BASE + 0x38)
#define ADDR_ENDP15             MMIO32(USBCTRL_REGS_BASE + 0x3c)
// Interrupt EP requires preamble
#define INTEP_PREAMBLE  0x2000000
// Direction of the interrupt endpoint. In=0, Out=1
#define INTEP_DIR       0x1000000
// Endpoint number of the interrupt endpoint
// Device address

/* Main control register */
#define MAIN_CTRL               MMIO32(USBCTRL_REGS_BASE + 0x40)
// Reduced timings for simulation
#define SIM_TIMING      0x80000000
// Device mode = 0, Host mode = 1
#define HOST_NDEVICE    0x00000002
// Enable controller
#define CONTROLLER_EN   0x00000001

/* Set the SOF (Start of Frame) frame number in the host controller. */
#define SOF_WR                  MMIO32(USBCTRL_REGS_BASE + 0x44)
#define COUNT_MSK   0x7f

/* Read the last SOF (Start of Frame) frame number seen. */
#define SOF_RD                  MMIO32(USBCTRL_REGS_BASE + 0x48)

/* SIE control register */
#define SIE_CTRL                MMIO32(USBCTRL_REGS_BASE + 0x4c)
// Set bit in EP_STATUS_STALL_NAK when EP0 sends a STALL
#define EP0_INT_STALL   0x80000000
// Device: EP0 single buffered = 0, double buffered = 1
#define EP0_DOUBLE_BUF  0x40000000
// Set bit in BUFF_STATUS for every buffer completed on EP0
#define EP0_INT_1BUF    0x20000000
// Set bit in BUFF_STATUS for every 2 buffers completed on EP0
#define EP0_INT_2BUF    0x10000000
// Set bit in EP_STATUS_STALL_NAK when EP0 sends a NAK
#define EP0_INT_NAK     0x08000000
// Direct bus drive enable
#define DIRECT_EN       0x04000000
// Direct control of DP
#define DIRECT_DP       0x02000000
// Direct control of DM
#define DIRECT_DM       0x01000000
// Power down bus transceiver
#define TRANSCEIVER_PD  0x00040000
// Device: Pull-up strength (0=1K2, 1=2k3)
#define RPU_OPT         0x00020000
// Device: Enable pull up resistor
#define PULLUP_EN       0x00010000
// Host: Enable pull down resistors
#define PULLDOWN_EN     0x00008000
// Host: Reset bus
#define RESET_BUS       0x00002000
// Device: Remote wakeup. Device can initiate its own resume after suspend.
#define RESUME          0x00001000
// Host: Enable VBUS
#define VBUS_EN         0x00000800
// Host: Enable keep alive packet (for low speed bus)
#define KEEP_ALIVE_EN   0x00000400
// Host: Enable SOF generation (for full speed bus)
#define SOF_EN          0x00000200
// Host: Delay packet(s) until after SOF
#define SOF_SYNC        0x00000100
// Host: Preable enable for LS device on FS hub
#define PREAMBLE_EN     0x00000040
// Host: Stop transaction
#define STOP_TRANS      0x00000010
// Host: Receive transaction (IN to host)
#define RECEIVE_DATA    0x00000008
// Host: Send transaction (OUT from host)
#define SEND_DATA       0x00000004
// Host: Send Setup packet
#define SEND_SETUP      0x00000002
// Host: Start transaction
#define START_TRANS     0x00000001

/* SIE status register */
#define SIE_STATUS              MMIO32(USBCTRL_REGS_BASE + 0x50)
// Data Sequence Error.
#define DATA_SEQ_ERROR  0x80000000
// ACK received. Raised by both host and device.
#define ACK_REC         0x40000000
// Host: STALL received
#define STALL_REC       0x20000000
// Host: NAK received
#define NAK_REC         0x10000000
// RX timeout is raised by both the host and device if an ACK is not received
#define RX_TIMEOUT      0x08000000
// RX overflow is raised by the Serial RX engine if the incoming data is too fast.
#define RX_OVERFLOW     0x04000000
// Bit Stuff Error. Raised by the Serial RX engine.
#define BIT_STUFF_ERROR 0x02000000
// CRC Error. Raised by the Serial RX engine.
#define CRC_ERROR       0x01000000
// Device: bus reset received
#define BUS_RESET       0x00080000
// Transaction complete.
#define TRANS_COMPLETE  0x00040000
// Device: Setup packet received
#define SETUP_REC       0x00020000
// Device: connected
#define CONNECTED       0x00010000
// Host: Device has initiated a remote resume. Device: host has initiated a resume.
#define RESUME_ST       0x00000800
// VBUS over current detected
#define VBUS_OVER_CURR  0x00000400
// Host: device speed. Disconnected = 00, LS = 01, FS = 10
#define SPEED_DISCONN   0x00000000
#define SPEED_LS        0x00000100
#define SPEED_FS        0x00000200
// Bus in suspended state.
#define SUSPENDED       0x00000010
// USB bus line state
#define LINE_STATE_MSK  0x0000000c
#define LINE_STATE_SFT  2
#define LINE_STATE_FETCH ((SIE_STATUS>>LINE_STATE_SFT)&0x3)
// Device: VBUS Detected
#define VBUS_DETECTED   0x00000001

/* interrupt endpoint control register */
#define INT_EP_CTRL             MMIO32(USBCTRL_REGS_BASE + 0x54)
// Host: Enable interrupt endpoint 1 → 15
#define INT_EP_ACTIVE15 0x8000
#define INT_EP_ACTIVE14 0x4000
#define INT_EP_ACTIVE13 0x2000
#define INT_EP_ACTIVE12 0x1000
#define INT_EP_ACTIVE11 0x0800
#define INT_EP_ACTIVE10 0x0400
#define INT_EP_ACTIVE9  0x0200
#define INT_EP_ACTIVE8  0x0100
#define INT_EP_ACTIVE7  0x0080
#define INT_EP_ACTIVE6  0x0040
#define INT_EP_ACTIVE5  0x0020
#define INT_EP_ACTIVE4  0x0010
#define INT_EP_ACTIVE3  0x0008
#define INT_EP_ACTIVE2  0x0004
#define INT_EP_ACTIVE1  0x0002

/* Buffer status register. */
#define BUFF_STATUS             MMIO32(USBCTRL_REGS_BASE + 0x58)
// A bit set here indicates that a buffer has completed on the endpoint
#define EP15_OUT    0x80000000
#define EP15_IN     0x40000000
#define EP14_OUT    0x20000000
#define EP14_IN     0x10000000
#define EP13_OUT    0x08000000
#define EP13_IN     0x04000000
#define EP12_OUT    0x02000000
#define EP12_IN     0x01000000
#define EP11_OUT    0x00800000
#define EP11_IN     0x00400000
#define EP10_OUT    0x00200000
#define EP10_IN     0x00100000
#define EP9_OUT     0x00080000
#define EP9_IN      0x00040000
#define EP8_OUT     0x00020000
#define EP8_IN      0x00010000
#define EP7_OUT     0x00008000
#define EP7_IN      0x00004000
#define EP6_OUT     0x00002000
#define EP6_IN      0x00001000
#define EP5_OUT     0x00000800
#define EP5_IN      0x00000400
#define EP4_OUT     0x00000200
#define EP4_IN      0x00000100
#define EP3_OUT     0x00000080
#define EP3_IN      0x00000040
#define EP2_OUT     0x00000020
#define EP2_IN      0x00000010
#define EP1_OUT     0x00000008
#define EP1_IN      0x00000004
#define EP0_OUT     0x00000002
#define EP0_IN      0x00000001

/* Which of the double buffers should be handled. */
#define BUFF_CPU_SHOULD_HANDLE  MMIO32(USBCTRL_REGS_BASE + 0x5c)

/* Can be set to ignore the buffer control register for this endpoint */
#define EP_ABORT                MMIO32(USBCTRL_REGS_BASE + 0x60)

/* Set once an endpoint is idle */
#define EP_ABORT_DONE           MMIO32(USBCTRL_REGS_BASE + 0x64)

/* set to send a STALL on EP0. */
#define EP_STALL_ARM            MMIO32(USBCTRL_REGS_BASE + 0x68)
//  EP0_OUT, EP0_IN

/* Sets the wait time in ms before trying again if the device replies with a NAK. */
#define NAK_POLL                MMIO32(USBCTRL_REGS_BASE + 0x6c)
// NAK polling interval for a full speed device
#define DELAY_FS_MSK    0x3ff0000
#define DELAY_FS_SFT    16
#define DELAY_FS_SET(n) ((n&0x3ff)<<DELAY_FS_SFT)
// NAK polling interval for a low speed device
#define DELAY_LS_MSK    0x00003ff
#define DELAY_LS_SET(n) (n&0x3ff)

/* bits are set when the IRQ_ON_NAK or IRQ_ON_STALL bits are set. */
#define EP_STATUS_STALL_NAK     MMIO32(USBCTRL_REGS_BASE + 0x70)

/* Where to connect the USB controller. */
#define USB_MUXING              MMIO32(USBCTRL_REGS_BASE + 0x74)
#define SOFTCON         0x8
#define TO_DIGITAL_PAD  0x4
#define TO_EXTPHY       0x2
#define TO_PHY          0x1

/* Overrides for pow sigs in the ev that the VBUS sigs are not hooked up to GPIO. */
#define USB_PWR                 MMIO32(USBCTRL_REGS_BASE + 0x78)
#define OVERCURR_DETECT_EN      0x20
#define OVERCURR_DETECT         0x10
#define VBUS_DETECT_OVERRIDE_EN 0x08
#define VBUS_DETECT             0x04
#define VBUS_EN_OVERRIDE_EN     0x02
#define VBUS_EN_PWR             0x01

/* This register allows for direct control of the USB phy. */
#define USBPHY_DIRECT           MMIO32(USBCTRL_REGS_BASE + 0x7c)
// DM over voltage
#define DM_OVV          0x400000
// DP over voltage
#define DP_OVV          0x200000
// DM overcurrent
#define DM_OVCN         0x100000
// DP overcurrent
#define DP_OVCN         0x080000
// DPM pin state
#define RX_DM           0x040000
// DPP pin state
#define RX_DP           0x020000
// Differential RX
#define RX_DD           0x010000
// TX_DIFFMODE=1: Differential drive mode
#define TX_DIFFMODE     0x008000
// TX_FSSLEW=1: Full speed slew rate
#define TX_FSSLEW       0x004000
// TX power down override (if override enable is set).
#define TX_PD           0x002000
// RX power down override (if override enable is set).
#define RX_PD           0x001000
// Output data. TX_DM_OE=1 to enable drive. DPM=TX_DM
#define TX_DM           0x000800
// TX_DP_OE=1 to enable drive. DPP=TX_DP
#define TX_DP           0x000400
// OE for DPM only. 0 - DPM in Hi-Z state; 1 - DPM driving
#define TX_DM_OE        0x000200
// f TX_DIFFMODE=1, OE for DPP/DPM diff pair. 0 - DPP/DPM in Hi-Z state; 1 - DPP/DPM
#define TX_DP_OE        0x000100
// DM pull down enable
#define DM_PULLDN_EN    0x000040
// DM pull up enable
#define DM_PULLUP_EN    0x000020
// Enable the second DM pull up resistor.
#define DM_PULLUP_HISEL 0x000010
// DP pull down enable
#define DP_PULLDN_EN    0x000004
// DP pull up enable
#define DP_PULLUP_EN    0x000002
// Enable the second DP pull up resistor.
#define DP_PULLUP_HISEL 0x000001

/* Override enable for each control in usbphy_direct */
#define USBPHY_DIRECT_OVERRIDE  MMIO32(USBCTRL_REGS_BASE + 0x80)
#define TX_DIFFMODE_OVERRIDE_EN     0x8000
#define DM_PULLUP_OVERRIDE_EN       0x1000
#define TX_FSSLEW_OVERRIDE_EN       0x0800
#define TX_PD_OVERRIDE_EN           0x0400
#define RX_PD_OVERRIDE_EN           0x0200
#define TX_DM_OVERRIDE_EN           0x0100
#define TX_DP_OVERRIDE_EN           0x0080
#define TX_DM_OE_OVERRIDE_EN        0x0040
#define TX_DP_OE_OVERRIDE_EN        0x0020
#define DM_PULLDN_EN_OVERRIDE_EN    0x0010
#define DP_PULLDN_EN_OVERRIDE_EN    0x0008
#define DP_PULLUP_EN_OVERRIDE_EN    0x0004
#define DM_PULLUP_HISEL_OVERRIDE_EN 0x0002
#define DP_PULLUP_HISEL_OVERRIDE_EN 0x0001

/* Used to adjust trim values of USB phy pull down resistors. */
#define USBPHY_TRIM             MMIO32(USBCTRL_REGS_BASE + 0x84)
// DM pulldown resistor trim control
#define DM_PULLDN_TRIM_MSK      0x1f00
#define DM_PULLDN_TRIM_SFT      8
#define DM_PULLDN_TRIM_SET(n)   ((n&0x1f)<<DM_PULLDN_TRIM_SFT)
// DP pulldown resistor trim control
#define DP_PULLDN_TRIM_MSK      0x001f
#define DP_PULLDN_TRIM_SET(n)   (n&DP_PULLDN_TRIM_MSK)

/* Raw Interrupts */
#define USB_INTR                MMIO32(USBCTRL_REGS_BASE + 0x8c)
// Raised when any bit in EP_STATUS_STALL_NAK is set.
#define EP_STALL_NAK            0x80000
// Raised when any bit in ABORT_DONE is set.
#define ABORT_DONE              0x40000
// Set every time the device receives a SOF
#define DEV_SOF                 0x20000
// Device. Source: SIE_STATUS.SETUP_REC
#define SETUP_REQ               0x10000
// Set when the device receives a resume from the host.
#define DEV_RESUME_FROM_HOST    0x08000
// Set when the device suspend state changes.
#define DEV_SUSPEND             0x04000
// Set when the device connection state changes.
#define DEV_CONN_DIS            0x02000
// Source: SIE_STATUS.BUS_RESET
#define BUS_RESET_INT           0x01000
// Source: SIE_STATUS.VBUS_DETECTED
#define VBUS_DETECT_INT         0x00800
// Source: SIE_STATUS.STALL_REC
#define STALL                   0x00400
// Source: SIE_STATUS.CRC_ERROR
#define ERROR_CRC               0x00200
// Source: SIE_STATUS.BIT_STUFF_ERROR
#define ERROR_BIT_STUFF         0x00100
// Source: SIE_STATUS.RX_OVERFLOW
#define ERROR_RX_OVERFLOW       0x00080
// Source: SIE_STATUS.RX_TIMEOUT
#define ERROR_RX_TIMEOUT        0x00040
// Source: SIE_STATUS.DATA_SEQ_ERROR
#define ERROR_DATA_SEQ          0x00020
// Raised when any bit in BUFF_STATUS is set
#define BUFF_STATUS_INT         0x00010
// Raised every time SIE_STATUS.TRANS_COMPLETE is set.
#define TRANS_COMPLETE_INT      0x00008
// Host: raised every time the host sends a SOF
#define HOST_SOF                0x00004
// Host: raised when a device wakes up the host.
#define HOST_RESUME             0x00002
// Host: raised when a device is connected or disconnected
#define HOST_CONN_DIS           0x00001

/* Interrupt Enable */
#define USB_INTE                MMIO32(USBCTRL_REGS_BASE + 0x90)

/* Interrupt Force */
#define USB_INTF                MMIO32(USBCTRL_REGS_BASE + 0x94)

/* Interrupt status after masking & forcing */
#define USB_INTS                MMIO32(USBCTRL_REGS_BASE + 0x98)

#endif
