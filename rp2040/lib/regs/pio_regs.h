#ifndef H_PIO_REGS
#define H_PIO_REGS
/*
 * RP2040 alternative library, programmable input/output block (PIO) registers
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "system_regs/memorymap.h"

/* PIO control register */
#define CTRL_0              MMIO32(PIO0_BASE + 0x000)
#define CTRL_1              MMIO32(PIO1_BASE + 0x000)
// Restart a state machine’s clock divider from an initial phase of 0.
#define CLKDIV_RESTART0     0x100
#define CLKDIV_RESTART1     0x200
#define CLKDIV_RESTART2     0x400
#define CLKDIV_RESTART3     0x800
// Write 1 to instantly clear internal SM state
#define SM_RESTART0         0x010
#define SM_RESTART1         0x020
#define SM_RESTART2         0x040
#define SM_RESTART3         0x080
// Enable/disable each of the four state machines by writing 1/0
#define SM_ENABLE0          0x001
#define SM_ENABLE1          0x002
#define SM_ENABLE2          0x004
#define SM_ENABLE3          0x008

/* FIFO status register */
#define FSTAT_0             MMIO32(PIO0_BASE + 0x004)
#define FSTAT_1             MMIO32(PIO1_BASE + 0x004)
// State machine TX FIFO is empty
#define TXEMPTY_FETCH(reg)  ((reg>>24)&0xf)
// State machine TX FIFO is full
#define TXFULL_FETCH(reg)   ((reg>>16)&0xf)
// State machine RX FIFO is empty
#define RXEMPTY_FETCH(reg)  ((reg>>8)&0xf)
// State machine RX FIFO is full
#define RXFULL_FETCH(reg)   ((reg&0xf)

/* FIFO debug register */
#define FDEBUG_0            MMIO32(PIO0_BASE + 0x008)
#define FDEBUG_1            MMIO32(PIO1_BASE + 0x008)
// State machine has stalled on empty TX FIFO
#define TXSTALL0    0x1000000
#define TXSTALL1    0x2000000
#define TXSTALL2    0x4000000
#define TXSTALL3    0x8000000
// TX FIFO overflow (i.e. write-on-full by the system) has occurred.
#define TXOVER0     0x0010000
#define TXOVER1     0x0020000
#define TXOVER2     0x0040000
#define TXOVER3     0x0080000
// RX FIFO underflow (i.e. read-on-empty by the system) has occurred.
#define RXUNDER0    0x0000100
#define RXUNDER1    0x0000200
#define RXUNDER2    0x0000400
#define RXUNDER3    0x0000800
// State machine has stalled on full RX FIFO
#define RXSTALL0    0x0000001
#define RXSTALL1    0x0000002
#define RXSTALL2    0x0000004
#define RXSTALL3    0x0000008

/* FIFO levels */
#define FLEVEL_0            MMIO32(PIO0_BASE + 0x00c)
#define FLEVEL_1            MMIO32(PIO1_BASE + 0x00c)
#define RX3_FETCH(reg)  ((reg>>28)&0xf)
#define TX3_FETCH(reg)  ((reg>>24)&0xf)
#define RX2_FETCH(reg)  ((reg>>20)&0xf)
#define TX2_FETCH(reg)  ((reg>>16)&0xf)
#define RX1_FETCH(reg)  ((reg>>12)&0xf)
#define TX1_FETCH(reg)  ((reg>>8)&0xf)
#define RX0_FETCH(reg)  ((reg>>4)&0xf)
#define TX0_FETCH(reg)  (reg&0xf)

/* Direct write access to the TX FIFO for this state machine. */
#define TXF0_0              MMIO32(PIO0_BASE + 0x010)
#define TXF0_1              MMIO32(PIO1_BASE + 0x010)
#define TXF1_0              MMIO32(PIO0_BASE + 0x014)
#define TXF1_1              MMIO32(PIO1_BASE + 0x014)
#define TXF2_0              MMIO32(PIO0_BASE + 0x018)
#define TXF2_1              MMIO32(PIO1_BASE + 0x018)
#define TXF3_0              MMIO32(PIO0_BASE + 0x01c)
#define TXF3_1              MMIO32(PIO1_BASE + 0x01c)
// Each write pushes one word to the FIFO.

/* Direct read access to the RX FIFO for this state machine. */
#define RXF0_0              MMIO32(PIO0_BASE + 0x020)
#define RXF0_1              MMIO32(PIO1_BASE + 0x020)
#define RXF1_0              MMIO32(PIO0_BASE + 0x024)
#define RXF1_1              MMIO32(PIO1_BASE + 0x024)
#define RXF2_0              MMIO32(PIO0_BASE + 0x028)
#define RXF2_1              MMIO32(PIO1_BASE + 0x028)
#define RXF3_0              MMIO32(PIO0_BASE + 0x02c)
#define RXF3_1              MMIO32(PIO1_BASE + 0x02c)
// Each read pops one word from the FIFO.

/* State machine IRQ flags register. */
#define IRQ_0               MMIO32(PIO0_BASE + 0x030)
#define IRQ_1               MMIO32(PIO1_BASE + 0x030)
// There are 8 state machine IRQ flags, which can be set, cleared, and waited

/* Writing a 1 to each of these bits will forcibly assert the corresponding IRQ. */
#define IRQ_FORCE_0         MMIO32(PIO0_BASE + 0x034)
#define IRQ_FORCE_1         MMIO32(PIO1_BASE + 0x034)
// writing here affects PIO internal state.

/* Each bit in this register corresponds to one GPIO. */
#define INPUT_SYNC_BYPASS_0 MMIO32(PIO0_BASE + 0x038)
#define INPUT_SYNC_BYPASS_1 MMIO32(PIO1_BASE + 0x038)
// If in doubt, leave this register as all zeroes.

/* Read to sample the pad output values PIO is currently driving to the GPIOs. */
#define DBG_PADOUT_0        MMIO32(PIO0_BASE + 0x03c)
#define DBG_PADOUT_1        MMIO32(PIO1_BASE + 0x03c)

/* Read to sample the pad output en (dir) PIO is currently driving to the GPIOs. */
#define DBG_PADOE_0         MMIO32(PIO0_BASE + 0x040)
#define DBG_PADOE_1         MMIO32(PIO1_BASE + 0x040)

/* The PIO hardware has some free parameters that may vary between chip products. */
#define DBG_CFGINFO_0       MMIO32(PIO0_BASE + 0x044)
#define DBG_CFGINFO_1       MMIO32(PIO1_BASE + 0x044)
// The size of the instruction memory, measured in units of one instruction
#define IMEM_SIZE_FETCH(reg)    ((reg>>16)&0x3f)
// The number of state machines this PIO instance is equipped with.
#define SM_COUNT_FETCH(reg)     ((reg>>8)&0xf)
// The depth of the state machine TX/RX FIFOs, measured in words.
#define FIFO_DEPTH_FETCH(reg)   (reg&0x3f)

/* Write-only access to instruction memory location n */
#define INSTR_MEM0_0        MMIO32(PIO0_BASE + 0x048)
#define INSTR_MEM0_1        MMIO32(PIO1_BASE + 0x048)
#define INSTR_MEM1_0        MMIO32(PIO0_BASE + 0x04c)
#define INSTR_MEM1_1        MMIO32(PIO1_BASE + 0x04c)
#define INSTR_MEM2_0        MMIO32(PIO0_BASE + 0x050)
#define INSTR_MEM2_1        MMIO32(PIO1_BASE + 0x050)
#define INSTR_MEM3_0        MMIO32(PIO0_BASE + 0x054)
#define INSTR_MEM3_1        MMIO32(PIO1_BASE + 0x054)
#define INSTR_MEM4_0        MMIO32(PIO0_BASE + 0x058)
#define INSTR_MEM4_1        MMIO32(PIO1_BASE + 0x058)
#define INSTR_MEM5_0        MMIO32(PIO0_BASE + 0x05c)
#define INSTR_MEM5_1        MMIO32(PIO1_BASE + 0x05c)
#define INSTR_MEM6_0        MMIO32(PIO0_BASE + 0x060)
#define INSTR_MEM6_1        MMIO32(PIO1_BASE + 0x060)
#define INSTR_MEM7_0        MMIO32(PIO0_BASE + 0x064)
#define INSTR_MEM7_1        MMIO32(PIO1_BASE + 0x064)
#define INSTR_MEM8_0        MMIO32(PIO0_BASE + 0x068)
#define INSTR_MEM8_1        MMIO32(PIO1_BASE + 0x068)
#define INSTR_MEM9_0        MMIO32(PIO0_BASE + 0x06c)
#define INSTR_MEM9_1        MMIO32(PIO1_BASE + 0x06c)
#define INSTR_MEM10_0       MMIO32(PIO0_BASE + 0x070)
#define INSTR_MEM10_1       MMIO32(PIO1_BASE + 0x070)
#define INSTR_MEM11_0       MMIO32(PIO0_BASE + 0x074)
#define INSTR_MEM11_1       MMIO32(PIO1_BASE + 0x074)
#define INSTR_MEM12_0       MMIO32(PIO0_BASE + 0x078)
#define INSTR_MEM12_1       MMIO32(PIO1_BASE + 0x078)
#define INSTR_MEM13_0       MMIO32(PIO0_BASE + 0x07c)
#define INSTR_MEM13_1       MMIO32(PIO1_BASE + 0x07c)
#define INSTR_MEM14_0       MMIO32(PIO0_BASE + 0x080)
#define INSTR_MEM14_1       MMIO32(PIO1_BASE + 0x080)
#define INSTR_MEM15_0       MMIO32(PIO0_BASE + 0x084)
#define INSTR_MEM15_1       MMIO32(PIO1_BASE + 0x084)
#define INSTR_MEM16_0       MMIO32(PIO0_BASE + 0x088)
#define INSTR_MEM16_1       MMIO32(PIO1_BASE + 0x088)
#define INSTR_MEM17_0       MMIO32(PIO0_BASE + 0x08c)
#define INSTR_MEM17_1       MMIO32(PIO1_BASE + 0x08c)
#define INSTR_MEM18_0       MMIO32(PIO0_BASE + 0x090)
#define INSTR_MEM18_1       MMIO32(PIO1_BASE + 0x090)
#define INSTR_MEM19_0       MMIO32(PIO0_BASE + 0x094)
#define INSTR_MEM19_1       MMIO32(PIO1_BASE + 0x094)
#define INSTR_MEM20_0       MMIO32(PIO0_BASE + 0x098)
#define INSTR_MEM20_1       MMIO32(PIO1_BASE + 0x098)
#define INSTR_MEM21_0       MMIO32(PIO0_BASE + 0x09c)
#define INSTR_MEM21_1       MMIO32(PIO1_BASE + 0x09c)
#define INSTR_MEM22_0       MMIO32(PIO0_BASE + 0x0a0)
#define INSTR_MEM22_1       MMIO32(PIO1_BASE + 0x0a0)
#define INSTR_MEM23_0       MMIO32(PIO0_BASE + 0x0a4)
#define INSTR_MEM23_1       MMIO32(PIO1_BASE + 0x0a4)
#define INSTR_MEM24_0       MMIO32(PIO0_BASE + 0x0a8)
#define INSTR_MEM24_1       MMIO32(PIO1_BASE + 0x0a8)
#define INSTR_MEM25_0       MMIO32(PIO0_BASE + 0x0ac)
#define INSTR_MEM25_1       MMIO32(PIO1_BASE + 0x0ac)
#define INSTR_MEM26_0       MMIO32(PIO0_BASE + 0x0b0)
#define INSTR_MEM26_1       MMIO32(PIO1_BASE + 0x0b0)
#define INSTR_MEM27_0       MMIO32(PIO0_BASE + 0x0b4)
#define INSTR_MEM27_1       MMIO32(PIO1_BASE + 0x0b4)
#define INSTR_MEM28_0       MMIO32(PIO0_BASE + 0x0b8)
#define INSTR_MEM28_1       MMIO32(PIO1_BASE + 0x0b8)
#define INSTR_MEM29_0       MMIO32(PIO0_BASE + 0x0bc)
#define INSTR_MEM29_1       MMIO32(PIO1_BASE + 0x0bc)
#define INSTR_MEM30_0       MMIO32(PIO0_BASE + 0x0c0)
#define INSTR_MEM30_1       MMIO32(PIO1_BASE + 0x0c0)
#define INSTR_MEM31_0       MMIO32(PIO0_BASE + 0x0c4)
#define INSTR_MEM31_1       MMIO32(PIO1_BASE + 0x0c4)

/* Clock divisor register for state machine 0 */
#define SM0_CLKDIV_0        MMIO32(PIO0_BASE + 0x0c8)
#define SM0_CLKDIV_1        MMIO32(PIO1_BASE + 0x0c8)
#define SM1_CLKDIV_0        MMIO32(PIO0_BASE + 0x0e0)
#define SM1_CLKDIV_1        MMIO32(PIO1_BASE + 0x0e0)
#define SM2_CLKDIV_0        MMIO32(PIO0_BASE + 0x0f8)
#define SM2_CLKDIV_1        MMIO32(PIO1_BASE + 0x0f8)
#define SM3_CLKDIV_0        MMIO32(PIO0_BASE + 0x110)
#define SM3_CLKDIV_1        MMIO32(PIO1_BASE + 0x110)
// Effective frequency is sysclk/(int + frac/256).
#define PIO_INT_SFT     16
#define PIO_INT_MSK     0xffff0000
// Fractional part of clock divisor
#define PIO_FRAC_SFT    8
#define PIO_FRAC_MSK    0x0000ff00

/* Execution/behavioural settings for state machine 0 */
#define SM0_EXECCTRL_0      MMIO32(PIO0_BASE + 0x0cc)
#define SM0_EXECCTRL_1      MMIO32(PIO1_BASE + 0x0cc)
#define SM1_EXECCTRL_0      MMIO32(PIO0_BASE + 0x0e4)
#define SM1_EXECCTRL_1      MMIO32(PIO1_BASE + 0x0e4)
#define SM2_EXECCTRL_0      MMIO32(PIO0_BASE + 0x0fc)
#define SM2_EXECCTRL_1      MMIO32(PIO1_BASE + 0x0fc)
#define SM3_EXECCTRL_0      MMIO32(PIO0_BASE + 0x114)
#define SM3_EXECCTRL_1      MMIO32(PIO1_BASE + 0x114)
// If 1, an instruction written to SMx_INSTR is stalled
#define EXEC_STALLED        0x80000000
// If 1, the MSB of the Delay/Side-set instruction field is used as side-set enable
#define SIDE_EN             0x40000000
// If 1, side-set data is asserted to pin directions
#define SIDE_PINDIR         0x20000000
// The GPIO number to use as condition for JMP PIN.
#define JMP_PIN_MSK         0x1f000000
#define JMP_PIN_SFT         24
#define JMP_PIN_SET(n)      ((n&0x1f)<<JMP_PIN_SFT)
// Which data bit to use for inline OUT enable
#define OUT_EN_SEL_MSK      0x00f80000
#define OUT_EN_SEL_SFT      19
#define OUT_EN_SEL_SET(n)   ((n&0x1f)<<JMP_PIN_SFT)
// If 1, use a bit of OUT data as an auxiliary write enable
#define INLINE_OUT_EN       0x00040000
// Continuously assert the most recent OUT/SET to the pins
#define OUT_STICKY          0x00020000
// After reaching this address, execution is wrapped to wrap_bottom.
#define WRAP_TOP_MSK        0x0001f000
#define WRAP_TOP_SFT        12
#define WRAP_TOP_SET(n)     ((n&0x1f)<<WRAP_TOP_SFT)
// After reaching wrap_top, execution is wrapped to this address.
#define WRAP_BOTTOM_MSK     0x00000f80
#define WRAP_BOTTOM_SFT     7
#define WRAP_BOTTOM_SET(n)  ((n&0x1f)<<WRAP_BOTTOM_SFT)
// Comparison used for the MOV x, STATUS instruction.
#define STATUS_SEL          0x00000010
// Comparison level for the MOV x, STATUS instruction
#define STATUS_N_MSK        0x0000000f

/* Control behaviour of the input/output shift registers for state machine 0 */
#define SM0_SHIFTCTRL_0     MMIO32(PIO0_BASE + 0x0d0)
#define SM0_SHIFTCTRL_1     MMIO32(PIO1_BASE + 0x0d0)
#define SM1_SHIFTCTRL_0     MMIO32(PIO0_BASE + 0x0e8)
#define SM1_SHIFTCTRL_1     MMIO32(PIO1_BASE + 0x0e8)
#define SM2_SHIFTCTRL_0     MMIO32(PIO0_BASE + 0x100)
#define SM2_SHIFTCTRL_1     MMIO32(PIO1_BASE + 0x100)
#define SM3_SHIFTCTRL_0     MMIO32(PIO0_BASE + 0x118)
#define SM3_SHIFTCTRL_1     MMIO32(PIO1_BASE + 0x118)
// When 1, RX FIFO steals the TX FIFO’s storage , and becomes twice as deep.
#define FJOIN_RX            0x80000000
// When 1, TX FIFO steals the RX FIFO’s storage, and becomes twice as deep.
#define FJOIN_TX            0x40000000
// Number of bits shifted out of OSR before autopull
#define PULL_THRESH_SFT     25
#define PULL_THRESH_MSK     0x3e000000
#define PULL_THRESH_SET(n)  ((n&0x1f)<<25)
// Number of bits shifted into ISR before autopush
#define PUSH_THRESH_SFT     20
#define PUSH_THRESH_MSK     0x01f00000
#define PUSH_THRESH_SET(n)  ((n&0x1f)<<20)
// 1 = shift out of output shift register to right. 0 = to left.
#define OUT_SHIFTDIR        0x00080000
// 1 = shift input shift register to right (data enters from left).
#define IN_SHIFTDIR         0x00040000
// Pull automatically when the output shift register is emptied
#define AUTOPULL            0x00020000
// Push automatically when the input shift register is filled
#define AUTOPUSH            0x00010000

/* Current instruction address of state machine n */
#define SM0_ADDR_0          MMIO32(PIO0_BASE + 0x0d4)
#define SM0_ADDR_1          MMIO32(PIO1_BASE + 0x0d4)
#define SM1_ADDR_0          MMIO32(PIO0_BASE + 0x0ec)
#define SM1_ADDR_1          MMIO32(PIO1_BASE + 0x0ec)
#define SM2_ADDR_0          MMIO32(PIO0_BASE + 0x104)
#define SM2_ADDR_1          MMIO32(PIO1_BASE + 0x104)
#define SM3_ADDR_0          MMIO32(PIO0_BASE + 0x11c)
#define SM3_ADDR_1          MMIO32(PIO1_BASE + 0x11c)

/* the instruction currently addressed by state machine n’s program counter */
#define SM0_INSTR_0         MMIO32(PIO0_BASE + 0x0d8)
#define SM0_INSTR_1         MMIO32(PIO1_BASE + 0x0d8)
#define SM1_INSTR_0         MMIO32(PIO0_BASE + 0x0f0)
#define SM1_INSTR_1         MMIO32(PIO1_BASE + 0x0f0)
#define SM2_INSTR_0         MMIO32(PIO0_BASE + 0x108)
#define SM2_INSTR_1         MMIO32(PIO1_BASE + 0x108)
#define SM3_INSTR_0         MMIO32(PIO0_BASE + 0x120)
#define SM3_INSTR_1         MMIO32(PIO1_BASE + 0x120)

/* State machine pin control */
#define SM0_PINCTRL_0       MMIO32(PIO0_BASE + 0x0dc)
#define SM0_PINCTRL_1       MMIO32(PIO1_BASE + 0x0dc)
#define SM1_PINCTRL_0       MMIO32(PIO0_BASE + 0x0f4)
#define SM1_PINCTRL_1       MMIO32(PIO1_BASE + 0x0f4)
#define SM2_PINCTRL_0       MMIO32(PIO0_BASE + 0x10c)
#define SM2_PINCTRL_1       MMIO32(PIO1_BASE + 0x10c)
#define SM3_PINCTRL_0       MMIO32(PIO0_BASE + 0x124)
#define SM3_PINCTRL_1       MMIO32(PIO1_BASE + 0x124)
// The number of MSBs of the Delay/Side-set instruction field
#define SIDESET_COUNT_SFT       29
#define SIDESET_COUNT_MSK       0xe0000000
#define SIDESET_COUNT_SET(n)    ((n&0x7)<<SIDESET_COUNT_SFT)
// The number of pins asserted by a SET.
#define SET_COUNT_SFT           26
#define SET_COUNT_MSK           0x1c000000
#define SET_COUNT_SET(n)        ((n&0x7)<<SET_COUNT_SFT)
// The number of pins asserted by an OUT PINS, OUT PINDIRS or MOV PINS instr
#define OUT_COUNT_SFT           20
#define OUT_COUNT_MSK           0x03f00000
#define OUT_COUNT_SET(n)        ((n&0x3f)<<SET_COUNT_SFT)
// The pin which is map to the lsb of a st machine’s IN data bus.
#define IN_BASE_SFT             15
#define IN_BASE_MSK             0x000f8000
#define IN_BASE_SET(n)          ((n&0x3f)<<IN_BASE_SFT)
// The lowest-numbered pin that will be affected by a side-set operation.
#define SIDESET_BASE_SFT        10
#define SIDESET_BASE_MSK        0x00007c00
#define SIDESET_BASE_SET(n)     ((n&0x1f)<<SIDESET_BASE_SFT)
// The lowest-numbered pin that will be aff by a SET PINS or SET PINDIRS instruction.
#define SET_BASE_SFT            5
#define SET_BASE_MSK            0x000003e0
#define SET_BASE_SET(n)         ((n&0x1f)<<SET_BASE_SFT)
// The low-num pin that will be aff by an OUT PINS, OUT PINDIRS or MOV PINS instr.
#define OUT_BASE_MSK            0x0000001f
#define OUT_BASE_SET(N)         (n&0x1f)

/* Raw Interrupts */
#define INTR_0              MMIO32(PIO0_BASE + 0x128)
#define INTR_1              MMIO32(PIO1_BASE + 0x128)
#define SM3             0x800
#define SM2             0x400
#define SM1             0x200
#define SM0             0x100
#define SM3_TXNFULL     0x080
#define SM2_TXNFULL     0x040
#define SM1_TXNFULL     0x020
#define SM0_TXNFULL     0x010
#define SM3_RXNEMPTY    0x008
#define SM2_RXNEMPTY    0x004
#define SM1_RXNEMPTY    0x002
#define SM0_RXNEMPTY    0x001

/* Interrupt Enable for irq0 */
#define IRQ0_INTE_0         MMIO32(PIO0_BASE + 0x12c)
#define IRQ0_INTE_1         MMIO32(PIO1_BASE + 0x12c)
#define IRQ1_INTE_0         MMIO32(PIO0_BASE + 0x138)
#define IRQ1_INTE_1         MMIO32(PIO1_BASE + 0x138)

/* Interrupt Force for irq0 */
#define IRQ0_INTF_0         MMIO32(PIO0_BASE + 0x130)
#define IRQ0_INTF_1         MMIO32(PIO1_BASE + 0x130)
#define IRQ1_INTF_0         MMIO32(PIO0_BASE + 0x13c)
#define IRQ1_INTF_1         MMIO32(PIO1_BASE + 0x13c)

/* Interrupt status after masking & forcing for irq0 */
#define IRQ0_INTS_0         MMIO32(PIO0_BASE + 0x134)
#define IRQ0_INTS_1         MMIO32(PIO1_BASE + 0x134)
#define IRQ1_INTS_0         MMIO32(PIO0_BASE + 0x140)
#define IRQ1_INTS_1         MMIO32(PIO1_BASE + 0x140)

#endif
