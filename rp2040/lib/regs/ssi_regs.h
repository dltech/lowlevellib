#ifndef H_SSI_REGS
#define H_SSI_REGS
/*
 * RP2040 alternative library, Synchronous Serial Interface (SSI)
 * controller registers
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "system_regs/memorymap.h"

/* Control register 0 */
#define CTRLR0          MMIO32(XIP_SSI_BASE + 0x00)
// Slave select toggle enable
#define SSTE            0x800000
// SPI frame format
#define SPI_FRF_ST      0x000000
#define SPI_FRF_DUAL    0x200000
#define SPI_FRF_QUAD    0x600000
// Data frame size in 32b transfer mode Value of n → n+1 clocks per frame.
#define DFS_32_MSK      0x1f0000
#define DFS_32_SFT      16
#define DFS_32_SET(n)   ((n<<DFS_32_SFT)&DFS_32_MSK)
// Control frame size Value of n → n+1 clocks per frame.
#define CFS_MSK         0x00f000
#define CFS_SFT         12
#define CFS_SET(n)      ((n<<CFS_SFT)&CFS_MSK)
// Shift register loop (test mode)
#define SRL             0x000800
// Slave output enable
#define SLV_OE          0x000400
// Transfer mode
#define TMOD_BOTH       0x000000
#define TMOD_TRANSMIT   0x000100
#define TMOD_RECEIVE    0x000200
#define TMOD_EEPROM     0x000300
// Serial clock polarity
#define SCPOL           0x000080
// Serial clock phase
#define SCPH            0x000040
// Frame format
#define FRF0            0x000000
#define FRF1            0x000010
#define FRF2            0x000020
#define FRF3            0x000030
// Data frame size
#define DFS_MSK         0x00000f
#define DFS_SET(n)      (n&DFS_MSK)

/* Master Control register 1 */
#define CTRLR1          MMIO32(XIP_SSI_BASE + 0x04)
// Number of data frames

/* SSI Enable */
#define SSIENR          MMIO32(XIP_SSI_BASE + 0x08)

/* Microwire Control */
#define MWCR            MMIO32(XIP_SSI_BASE + 0x0c)
// Microwire handshaking
#define MHS     0x3
// Microwire control
#define MDD     0x2
// Microwire transfer mode
#define MWMOD   0x1

/* Slave enable */
#define SER             MMIO32(XIP_SSI_BASE + 0x10)
// 1 → slave selected

/* Baud rate */
#define BAUDR           MMIO32(XIP_SSI_BASE + 0x14)
// SSI clock divider

/* TX FIFO threshold level */
#define TXFTLR          MMIO32(XIP_SSI_BASE + 0x18)
// Transmit FIFO threshold

/* RX FIFO threshold level */
#define RXFTLR          MMIO32(XIP_SSI_BASE + 0x1c)
// Receive FIFO threshold

/* TX FIFO level */
#define TXFLR           MMIO32(XIP_SSI_BASE + 0x20)
// Transmit FIFO level

/* RX FIFO level */
#define RXFLR           MMIO32(XIP_SSI_BASE + 0x24)
// Receive FIFO level

/* Status register */
#define SR              MMIO32(XIP_SSI_BASE + 0x28)
// Data collision error
#define DCOL    0x40
// Transmission error
#define TXE     0x20
// Receive FIFO full
#define RFF     0x10
// Receive FIFO not empty
#define RFNE    0x08
// Transmit FIFO empty
#define TFE     0x04
// Transmit FIFO not full
#define TFNF    0x02
// SSI busy flag
#define BUSY    0x01

/* Interrupt mask */
#define IMR             MMIO32(XIP_SSI_BASE + 0x2c)
// Multi-master contention interrupt mask
#define MSTIM   0x20
// Receive FIFO full interrupt mask
#define RXFIM   0x10
// Receive FIFO overflow interrupt mask
#define RXOIM   0x08
// Receive FIFO underflow interrupt mask
#define RXUIM   0x04
// Transmit FIFO overflow interrupt mask
#define TXOIM   0x02
// Transmit FIFO empty interrupt mask
#define TXEIM   0x01

/* Interrupt status */
#define ISR             MMIO32(XIP_SSI_BASE + 0x30)
// Multi-master contention interrupt status
#define MSTIS   0x20
// Receive FIFO full interrupt status
#define RXFIS   0x10
// Receive FIFO overflow interrupt status
#define RXOIS   0x08
// Receive FIFO underflow interrupt status
#define RXUIS   0x04
// Transmit FIFO overflow interrupt status
#define TXOIS   0x02
// Transmit FIFO empty interrupt status
#define TXEIS   0x01

/* Raw interrupt status */
#define RISR            MMIO32(XIP_SSI_BASE + 0x34)
// Multi-master contention raw interrupt status
#define MSTIR   0x20
// Receive FIFO full raw interrupt status
#define RXFIR   0x10
// Receive FIFO overflow raw interrupt status
#define RXOIR   0x08
// Receive FIFO underflow raw interrupt status
#define RXUIR   0x04
// Transmit FIFO overflow raw interrupt status
#define TXOIR   0x02
// Transmit FIFO empty raw interrupt status
#define TXEIR   0x01

/* TX FIFO overflow interrupt clear */
#define TXOICR          MMIO32(XIP_SSI_BASE + 0x38)
// Clear-on-read transmit FIFO overflow interrupt

/* RX FIFO overflow interrupt clear */
#define RXOICR          MMIO32(XIP_SSI_BASE + 0x3c)
// Clear-on-read receive FIFO overflow interrupt

/* RX FIFO underflow interrupt clear */
#define RXUICR          MMIO32(XIP_SSI_BASE + 0x40)
// Clear-on-read receive FIFO underflow interrupt

/* Multi-master interrupt clear */
#define MSTICR          MMIO32(XIP_SSI_BASE + 0x44)
// Clear-on-read multi-master contention interrupt

/* Interrupt clear */
#define ICR             MMIO32(XIP_SSI_BASE + 0x48)
// Clear-on-read all active interrupts

/* DMA control */
#define DMACR           MMIO32(XIP_SSI_BASE + 0x4c)
// Transmit DMA enable
#define TDMAE   0x2
// Receive DMA enable
#define RDMAE   0x1

/* DMA TX data level */
#define DMATDLR         MMIO32(XIP_SSI_BASE + 0x50)
// Transmit data watermark level

/* DMA RX data level */
#define DMARDLR         MMIO32(XIP_SSI_BASE + 0x54)
// Receive data watermark level (DMARDLR+1)

/* Identification register */
#define IDR             MMIO32(XIP_SSI_BASE + 0x58)
// Peripheral dentification code

/* Version ID */
#define SSI_VERSION_ID  MMIO32(XIP_SSI_BASE + 0x5c)
// SNPS component version (format X.YY)

/* Data Register 0 (of 36) */
#define DR0             MMIO32(XIP_SSI_BASE + 0x60)
// First data register of 36
#define SSI_DR  0xffffffff

/* RX sample delay */
#define RX_SAMPLE_DLY   MMIO32(XIP_SSI_BASE + 0xf0)
// RXD sample delay (in SCLK cycles)
#define RSD_MSK 0xff

/* SPI control */
#define SPI_CTRLR0      MMIO32(XIP_SSI_BASE + 0xf4)
// SPI Command to send in XIP mode
#define XIP_CMD_MSK                 0xff000000
#define XIP_CMD_SFT                 24
#define XIP_CMD_SET(n)              ((n<<XIP_CMD_SFT)&XIP_CMD_MSK)
// Read data strobe enable
#define SPI_RXDS_EN                 0x00040000
// Instruction DDR transfer enable
#define INST_DDR_EN                 0x00020000
// SPI DDR transfer enable
#define SPI_DDR_EN                  0x00010000
// Wait cycles between control frame transmit and data reception (in SCLK cycles)
#define WAIT_CYCLES_MSK             0x0000f800
#define WAIT_CYCLES_SFT             11
#define WAIT_CYCLES_SET(n)          ((n<<WAIT_CYCLES_SFT)&WAIT_CYCLES_MSK)
// Instruction length (0/4/8/16b)
#define INST_L0BIT                  0x00000000
#define INST_L4BIT                  0x00000100
#define INST_L8BIT                  0x00000200
#define INST_L16BIT                 0x00000300
// Address length (0b-60b in 4b increments)
#define ADDR_L_MSK                  0x0000003c
#define ADDR_L_SFT                  2
#define ADDR_L_SET(n)               ((n<<ADDR_L_SFT)&ADDR_L_MSK)
// Address and instruction transfer format
#define TRANS_TYPE_CMD_ADDR_SPI     0x00000000
#define TRANS_TYPE_CMD_SPI_ADDR_FRF 0x00000001
#define TRANS_TYPE_CMD_ADDR_FRF     0x00000002


/* TX drive edge */
#define TXD_DRIVE_EDGE  MMIO32(XIP_SSI_BASE + 0xf8)
// TXD drive edge
#define TDE_MSK 0xff

#endif
