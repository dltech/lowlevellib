#ifndef H_RTC_REGS
#define H_RTC_REGS
/*
 * RP2040 alternative library, The Real-time Clock (RTC) registers
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "system_regs/memorymap.h"

/* Divider minus 1 for the 1 second counter. */
#define CLKDIV_M1   MMIO32(RTC_BASE + 0x00)
// Divider minus 1 for the 1 second counter.

/* RTC setup register 0 */
#define SETUP_0     MMIO32(RTC_BASE + 0x04)
// Year
#define YEAR_MSK        0xfff000
#define YEAR_SFT        12
#define YEAR_SET(n)     ((n<<YEAR_SFT)&YEAR_MSK)
// Month (1..12)
#define MONTH_MSK       0x00f
#define MONTH_SFT       8
#define MONTH_SET(n)    ((n<<MONTH_SFT)&MONTH_MSK)
// Day of the month (1..31)
#define DAY_MSK         0xf
#define DAY_SET(n)      (n&DAY_MSK)

/* RTC setup register 1 */
#define SETUP_1     MMIO32(RTC_BASE + 0x08)
// Day of the week: 1-Monday...0-Sunday ISO 8601 mod 7
#define DOTW_MSK    0x7000000
#define DOTW_SFT    24
#define DOTW_SET(n) ((n<<DOTW_SFT)&DOTW_MSK)
// Hours
#define HOUR_MSK    0x01f0000
#define HOUR_SFT    16
#define HOUR_SET(n) ((n<<MONTH_SFT)&MONTH_MSK)
// Minutes
#define MIN_MSK     0x0003f00
#define MIN_SFT     8
#define MIN_SET(n)  ((n<<MIN_SFT)&MIN_MSK)
// Seconds
#define SEC_MSK     0x000001f
#define SEC_SET(n)  (n&SEC_MSK)

/* RTC Control and status */
#define CTRL        MMIO32(RTC_BASE + 0x0c)
// If set, leapyear is forced off.
#define FORCE_NOTLEAPYEAR   0x100
// Load RTC
#define LOAD                0x010
// RTC enabled (running)
#define RTC_ACTIVE          0x002
// Enable RTC
#define RTC_ENABLE          0x001

/* Interrupt setup register 0 */
#define IRQ_SETUP_0 MMIO32(RTC_BASE + 0x10)
#define MATCH_ACTIVE    0x20000000
// Global match enable. Don’t change any other value while this one is enabled
#define MATCH_ENA       0x10000000
// Enable year matching
#define YEAR_ENA        0x04000000
// Enable month matching
#define MONTH_ENA       0x02000000
// Enable day matching
#define DAY_ENA         0x01000000
// Year
// Month (1..12)
// Day of the month (1..31)

/* Interrupt setup register 1 */
#define IRQ_SETUP_1 MMIO32(RTC_BASE + 0x14)
// Enable day of the week matching
#define DOTW_ENA    0x80000000
// Enable hour matching
#define HOUR_ENA    0x40000000
// Enable minute matching
#define MIN_ENA     0x20000000
// Enable second matching
#define SEC_ENA     0x10000000
// Day of the week
// Hours
// Minutes
// Seconds

/* RTC register 1. */
#define RTC_1       MMIO32(RTC_BASE + 0x18)
// Year
#define YEAR    ((RTC_1>>YEAR_SFT)&0xfff)
// Month (1..12)
#define MONTH   ((RTC_1>>MONTH_SFT)&0xf)
// Day of the month (1..31)
#define DAY     (RTC_1&0xf)

/* RTC register 0 Read this before RTC 1! */
#define RTC_0       MMIO32(RTC_BASE + 0x1c)
// Day of the week: 1-Monday...0-Sunday ISO 8601 mod 7
#define DOTW    ((RTC_0>>DOTW_SFT)&0x7)
// Hours
#define HOUR    ((RTC_0>>MONTH_SFT)&0x1f)
// Minutes
#define MIN     ((RTC_0>>MIN_SFT)&0x3f)
// Seconds
#define SEC     (RTC_0&SEC_MSK)

/* Raw Interrupts */
#define RTC_INTR    MMIO32(RTC_BASE + 0x20)
#define RTC 0x1

/* Interrupt Enable */
#define RTC_INTE    MMIO32(RTC_BASE + 0x24)

/* Interrupt Force */
#define RTC_INTF    MMIO32(RTC_BASE + 0x28)

/* Interrupt status after masking & forcing */
#define RTC_INTS    MMIO32(RTC_BASE + 0x2c)

#endif
