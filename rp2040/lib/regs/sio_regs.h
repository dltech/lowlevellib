#ifndef H_SIO_REGS
#define H_SIO_REGS
/*
 * RP2040 alternative library, Single-cycle IO block control registers
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Processor core identifier */
#define CPUID               MMIO32(SIO_BASE + 0x000)
// Value is 0 when read from processor core 0, and 1 when read from proc core1.

/* Input value for GPIO pins */
#define GPIO_IN             MMIO32(SIO_BASE + 0x004)
//Input value for GPIO0...29
#define GPIO0   0x00000001
#define GPIO1   0x00000002
#define GPIO2   0x00000004
#define GPIO3   0x00000008
#define GPIO4   0x00000010
#define GPIO5   0x00000020
#define GPIO6   0x00000040
#define GPIO7   0x00000080
#define GPIO8   0x00000100
#define GPIO9   0x00000200
#define GPIO10  0x00000400
#define GPIO11  0x00000800
#define GPIO12  0x00001000
#define GPIO13  0x00002000
#define GPIO14  0x00004000
#define GPIO15  0x00008000
#define GPIO16  0x00010000
#define GPIO17  0x00020000
#define GPIO18  0x00040000
#define GPIO19  0x00080000
#define GPIO20  0x00100000
#define GPIO21  0x00200000
#define GPIO22  0x00400000
#define GPIO23  0x00800000
#define GPIO24  0x01000000
#define GPIO25  0x02000000
#define GPIO26  0x04000000
#define GPIO27  0x08000000
#define GPIO28  0x10000000
#define GPIO29  0x20000000

/* Input value for QSPI pins */
#define GPIO_HI_IN          MMIO32(SIO_BASE + 0x008)
// Input value on QSPI IO in order 0..5: SCLK, SSn, SD0, SD1, SD2, SD3
#define QSPI_IN_SCLK    0x000000001
#define QSPI_IN_SSN     0x000000002
#define QSPI_IN_SD0     0x000000004
#define QSPI_IN_SD1     0x000000008
#define QSPI_IN_SD2     0x000000010
#define QSPI_IN_SD3     0x000000020

/* GPIO output value */
#define GPIO_OUT            MMIO32(SIO_BASE + 0x010)
// Set output level (1/0 → high/low) for GPIO0...29.

/* GPIO output value set */
#define GPIO_OUT_SET        MMIO32(SIO_BASE + 0x014)
// Perform an atomic bit-set on GPIO_OUT, i.e. GPIO_OUT |= wdata

/* GPIO output value clear */
#define GPIO_OUT_CLR        MMIO32(SIO_BASE + 0x018)
// Perform an atomic bit-clear on GPIO_OUT, i.e. GPIO_OUT &= ~wdata

/* GPIO output value XOR */
#define GPIO_OUT_XOR        MMIO32(SIO_BASE + 0x01c)
// Perform an atomic bitwise XOR on GPIO_OUT, i.e. GPIO_OUT ^= wdata

/* GPIO output enable */
#define GPIO_OE             MMIO32(SIO_BASE + 0x020)
// Set output enable (1/0 → output/input) for GPIO0...29.

/* GPIO output enable set */
#define GPIO_OE_SET         MMIO32(SIO_BASE + 0x024)
// Perform an atomic bit-set on GPIO_OE, i.e. GPIO_OE |= wdata

/* GPIO output enable clear */
#define GPIO_OE_CLR         MMIO32(SIO_BASE + 0x028)
// Perform an atomic bit-clear on GPIO_OE, i.e. GPIO_OE &= ~wdata

/* GPIO output enable XOR */
#define GPIO_OE_XOR         MMIO32(SIO_BASE + 0x02c)
// Perform an atomic bitwise XOR on GPIO_OE, i.e. GPIO_OE ^= wdata

/* QSPI output value */
#define GPIO_HI_OUT         MMIO32(SIO_BASE + 0x030)
// Set output level (1/0 → high/low) for QSPI IO0...5.

/* QSPI output value set */
#define GPIO_HI_OUT_SET     MMIO32(SIO_BASE + 0x034)
// Perform an atomic bit-set on GPIO_HI_OUT, i.e. GPIO_HI_OUT |= wdata

/* QSPI output value clear */
#define GPIO_HI_OUT_CLR     MMIO32(SIO_BASE + 0x038)
// Perform an atomic bit-clear on GPIO_HI_OUT, i.e. GPIO_HI_OUT &= ~wdata

/* QSPI output value XOR */
#define GPIO_HI_OUT_XOR     MMIO32(SIO_BASE + 0x03c)
// Perform an atomic bitwise XOR on GPIO_HI_OUT, i.e. GPIO_HI_OUT ^= wdata

/* QSPI output enable */
#define GPIO_HI_OE          MMIO32(SIO_BASE + 0x040)
// Set output enable (1/0 → output/input) for QSPI IO0...5.

/* QSPI output enable set */
#define GPIO_HI_OE_SET      MMIO32(SIO_BASE + 0x044)
// Perform an atomic bit-set on GPIO_HI_OE, i.e. GPIO_HI_OE |= wdata

/* QSPI output enable clear */
#define GPIO_HI_OE_CLR      MMIO32(SIO_BASE + 0x048)
// Perform an atomic bit-clear on GPIO_HI_OE, i.e. GPIO_HI_OE &= ~wdata

/* QSPI output enable XOR */
#define GPIO_HI_OE_XOR      MMIO32(SIO_BASE + 0x04c)
// Perform an atomic bitwise XOR on GPIO_HI_OE, i.e. GPIO_HI_OE ^= wdata

/* Status register for inter-core FIFOs (mailboxes). */
#define FIFO_ST             MMIO32(SIO_BASE + 0x050)
// Sticky flag indicating the RX FIFO was read when empty.
#define ROE 0x00000008
// Sticky flag indicating the TX FIFO was written when full.
#define WOF 0x00000004
// Value is 1 if this core’s TX FIFO is not full
#define RDY 0x00000002
// Value is 1 if this core’s RX FIFO is not empty
#define VLD 0x00000001

/* Write access to this core’s TX FIFO */
#define FIFO_WR             MMIO32(SIO_BASE + 0x054)

/* Read access to this core’s RX FIFO */
#define FIFO_RD             MMIO32(SIO_BASE + 0x058)

/* Spinlock state */
#define SPINLOCK_ST         MMIO32(SIO_BASE + 0x05c)
// A bitmap containing the state of all 32 spinlocks (1=locked).

/* Divider unsigned dividend */
#define DIV_UDIVIDEND       MMIO32(SIO_BASE + 0x060)
// Write to the DIVIDEND operand of the divider, i.e. the p in p / q.

/* Divider unsigned divisor */
#define DIV_UDIVISOR        MMIO32(SIO_BASE + 0x064)
// Write to the DIVISOR operand of the divider, i.e. the q in p / q.

/* Divider signed dividend */
#define DIV_SDIVIDEND       MMIO32(SIO_BASE + 0x068)
// The same as UDIVIDEND, but starts a signed calculation, rather than unsigned.

/* Divider signed divisor */
#define DIV_SDIVISOR        MMIO32(SIO_BASE + 0x06c)
// The same as UDIVISOR, but starts a signed calculation, rather than unsigned.

/* Divider result quotient */
#define DIV_QUOTIENT        MMIO32(SIO_BASE + 0x070)
// The result of DIVIDEND / DIVISOR (division). Contents undefined while CSR_READY is low.

/* Divider result remainder */
#define DIV_REMAINDER       MMIO32(SIO_BASE + 0x074)
// The result of DIVIDEND % DIVISOR (modulo). Contents undefined while CSR_READY is low.

/* Control and status register for divider. */
#define DIV_CSR             MMIO32(SIO_BASE + 0x078)
// Changes to 1 when any register is written, and back to 0RO0x0 RO0x1 when QUOTIENT is read.
#define DIRTY   0x000000002
// Reads as 0 when a calculation is in progress, 1 otherwise.
#define READY   0x000000001

/* Read/write access to accumulator 0 */
#define INTERP0_ACCUM0      MMIO32(SIO_BASE + 0x080)
/* Read/write access to accumulator 1 */
#define INTERP0_ACCUM1      MMIO32(SIO_BASE + 0x084)

/* Read/write access to BASE0 register. */
#define  INTERP0_BASE0      MMIO32(SIO_BASE + 0x088)
/* Read/write access to BASE1 register. */
#define  INTERP0_BASE1      MMIO32(SIO_BASE + 0x08c)
/* Read/write access to BASE2 register. */
#define  INTERP0_BASE2      MMIO32(SIO_BASE + 0x090)

/* Read LANE0 result, and simul write lane results to both accu (POP). */
#define INTERP0_POP_LANE0   MMIO32(SIO_BASE + 0x094)
/* Read LANE1 result, and simul write lane results to both accu (POP). */
#define INTERP0_POP_LANE1   MMIO32(SIO_BASE + 0x098)
/* Read FULL result, and simul write lane results to both accu (POP). */
#define INTERP0_POP_FULL    MMIO32(SIO_BASE + 0x09c)

/* Read LANE0 result, without altering any internal state (PEEK). */
#define INTERP0_PEEK_LANE0  MMIO32(SIO_BASE + 0x0a0)
/* Read LANE1 result, without altering any internal state (PEEK). */
#define INTERP0_PEEK_LANE1  MMIO32(SIO_BASE + 0x0a4)
/* Read FULL result, without altering any internal state (PEEK). */
#define INTERP0_PEEK_FULL   MMIO32(SIO_BASE + 0x0a8)

/* Control register for lane 0 */
#define INTERP0_CTRL_LANE0  MMIO32(SIO_BASE + 0x0ac)
// Set if either OVERF0 or OVERF1 is set.
#define OVERF           0x02000000
// Indicates if any masked-off MSBs in ACCUM1 are set.
#define OVERF1          0x01000000
// Indicates if any masked-off MSBs in ACCUM0 are set.
#define OVERF0          0x00800000
// Enable blend mode
#define BLEND           0x00200000
// ORed intobits 29:28 of the lane result presented to the processor on the bus.
#define FORCE_MSB00     0x00000000
#define FORCE_MSB01     0x00080000
#define FORCE_MSB10     0x00100000
#define FORCE_MSB11     0x00180000
// If 1, mask + shift is bypassed for LANE0 result
#define ADD_RAW         0x00040000
// If 1, feed the opposite lane’s result into this lane’s accumulator on POP.
#define CROSS_RESULT    0x00020000
// If 1, feed the opposite lane’s accumulator into this lane’s shift + mask hardware.
#define CROSS_INPUT     0x00010000
// If SIGNED is set, the shifted and masked accumulator value is sign-extended to 32 bits
#define SIGNED          0x00008000
// The most-significant bit allowed to pass by the mask (inclusive)
#define MASK_MSB_MSK    0x00007c00
#define MASK_MSB_SFT    10
// The least-significant bit allowed to pass by the mask (inclusive)
#define MASK_LSB_MSK    0x000003e0
#define MASK_LSB_SFT    5
// Logical right-shift applied to accumulator before masking
#define SHIFT_MSK       0x0000001f
/* Control register for lane 1 */
#define INTERP0_CTRL_LANE1  MMIO32(SIO_BASE + 0x0b0)

/* Values written here are atomically added to ACCUM0 */
#define INTERP0_ACCUM0_ADD  MMIO32(SIO_BASE + 0x0b4)
/* Values written here are atomically added to ACCUM1 */
#define INTERP0_ACCUM1_ADD  MMIO32(SIO_BASE + 0x0b8)

/* On write, the lower 16 bits go to BASE0, upper bits to BASE1 simul. */
#define INTERP0_BASE_1AND0  MMIO32(SIO_BASE + 0x0bc)

/* Read/write access to accumulator 0 */
#define INTERP1_ACCUM0      MMIO32(SIO_BASE + 0x0c0)
/* Read/write access to accumulator 1 */
#define INTERP1_ACCUM1      MMIO32(SIO_BASE + 0x0c4)

/* Read/write access to BASE0 register. */
#define INTERP1_BASE0       MMIO32(SIO_BASE + 0x0c8)
/* Read/write access to BASE1 register. */
#define INTERP1_BASE1       MMIO32(SIO_BASE + 0x0cc)
/* Read/write access to BASE2 register. */
#define INTERP1_BASE2       MMIO32(SIO_BASE + 0x0d0)

/* Read LANE0 result, and simul write lane results to both accu (POP). */
#define INTERP1_POP_LANE0   MMIO32(SIO_BASE + 0x0d4)
/* Read LANE1 result, and simul write lane results to both accu (POP). */
#define INTERP1_POP_LANE1   MMIO32(SIO_BASE + 0x0d8)
/* Read FULL result, and simul write lane results to both accu (POP). */
#define INTERP1_POP_FULL    MMIO32(SIO_BASE + 0x0dc)

/* Read LANE0 result, without altering any internal state (PEEK). */
#define INTERP1_PEEK_LANE0  MMIO32(SIO_BASE + 0x0e0)
/* Read LANE1 result, without altering any internal state (PEEK). */
#define INTERP1_PEEK_LANE1  MMIO32(SIO_BASE + 0x0e4)
/* Read FULL result, without altering any internal state (PEEK). */
#define INTERP1_PEEK_FULL   MMIO32(SIO_BASE + 0x0e8)

/* Control register for lane 0 */
#define INTERP1_CTRL_LANE0  MMIO32(SIO_BASE + 0x0ec)
// enable CLAMP mode
#define CLAMP           0x00400000
/* Control register for lane 1 */
#define INTERP1_CTRL_LANE1  MMIO32(SIO_BASE + 0x0f0)

/* Values written here are atomically added to ACCUM0 */
#define INTERP1_ACCUM0_ADD  MMIO32(SIO_BASE + 0x0f4)
/* Values written here are atomically added to ACCUM1 */
#define INTERP1_ACCUM1_ADD  MMIO32(SIO_BASE + 0x0f8)

/* On write, the lower 16 bits go to BASE0, upper bits to BASE1 simul. */
#define INTERP1_BASE_1AND0  MMIO32(SIO_BASE + 0x0fc)
// On write, the lower 16 bits go to BASE0, upper bits to BASE1 simultaneously.
// Each half is sign-extended to 32 bits if that lane’s SIGNED flag is set.

/* Spinlock register 0 - 31 */
#define SPINLOCK0           MMIO32(SIO_BASE + 0x100)
#define SPINLOCK1           MMIO32(SIO_BASE + 0x104)
#define SPINLOCK2           MMIO32(SIO_BASE + 0x108)
#define SPINLOCK3           MMIO32(SIO_BASE + 0x10c)
#define SPINLOCK4           MMIO32(SIO_BASE + 0x110)
#define SPINLOCK5           MMIO32(SIO_BASE + 0x114)
#define SPINLOCK6           MMIO32(SIO_BASE + 0x118)
#define SPINLOCK7           MMIO32(SIO_BASE + 0x11c)
#define SPINLOCK8           MMIO32(SIO_BASE + 0x120)
#define SPINLOCK9           MMIO32(SIO_BASE + 0x124)
#define SPINLOCK10          MMIO32(SIO_BASE + 0x128)
#define SPINLOCK11          MMIO32(SIO_BASE + 0x12c)
#define SPINLOCK12          MMIO32(SIO_BASE + 0x130)
#define SPINLOCK13          MMIO32(SIO_BASE + 0x134)
#define SPINLOCK14          MMIO32(SIO_BASE + 0x138)
#define SPINLOCK15          MMIO32(SIO_BASE + 0x13c)
#define SPINLOCK16          MMIO32(SIO_BASE + 0x140)
#define SPINLOCK17          MMIO32(SIO_BASE + 0x144)
#define SPINLOCK18          MMIO32(SIO_BASE + 0x148)
#define SPINLOCK19          MMIO32(SIO_BASE + 0x14c)
#define SPINLOCK20          MMIO32(SIO_BASE + 0x150)
#define SPINLOCK21          MMIO32(SIO_BASE + 0x154)
#define SPINLOCK22          MMIO32(SIO_BASE + 0x158)
#define SPINLOCK23          MMIO32(SIO_BASE + 0x15c)
#define SPINLOCK24          MMIO32(SIO_BASE + 0x160)
#define SPINLOCK25          MMIO32(SIO_BASE + 0x164)
#define SPINLOCK26          MMIO32(SIO_BASE + 0x168)
#define SPINLOCK27          MMIO32(SIO_BASE + 0x16c)
#define SPINLOCK28          MMIO32(SIO_BASE + 0x170)
#define SPINLOCK29          MMIO32(SIO_BASE + 0x174)
#define SPINLOCK30          MMIO32(SIO_BASE + 0x178)
#define SPINLOCK31          MMIO32(SIO_BASE + 0x17c)

#endif
