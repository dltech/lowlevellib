#ifndef H_I2C_REGS
#define H_I2C_REGS
/*
 * RP2040 alternative library, I2C registers
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "system_regs/memorymap.h"

/* I2C Control Register */
#define IC0_CON                 MMIO32(I2C0_BASE + 0x00)
#define IC1_CON                 MMIO32(I2C1_BASE + 0x00)
// Master issues the STOP_DET int irrespective of whether master is active or not
#define STOP_DET_IF_MASTER_ACTIVE   0x400
// This bit controls whether DW_apb_i2c should hold the bus
#define RX_FIFO_FULL_HLD_CTRL       0x200
// This bit controls the generation of the TX_EMPTY int
#define TX_EMPTY_CTRL               0x100
// 1’b1: issues the STOP_DET interrupt only when it is addressed.
#define STOP_DET_IFADDRESSED        0x080
// This bit controls whether I2C has its slave disabled
#define IC_SLAVE_DISABLE            0x040
// Determines whether RESTART conditions may be sent when acting as a master
#define IC_RESTART_EN               0x020
// Controls whether the DW_apb_i2c starts its transfers in 7-or 10-bit addr mode
#define IC_10BITADDR_MASTER         0x010
// ctrl whether the DW_apb_i2c responds to 7- or 10-bit addr. - 0: 7-bit addr.
#define IC_10BITADDR_SLAVE          0x008
// These bits control at which speed the DW_apb_i2c operates
#define SPEED_STANDARD              0x002
#define SPEED_FAST                  0x004
#define SPEED_HIGH                  0x006
// This bit controls whether the DW_apb_i2c master is enabled.
#define MASTER_MODE                 0x001

/* I2C Target Address Register */
#define IC0_TAR                 MMIO32(I2C0_BASE + 0x04)
#define IC1_TAR                 MMIO32(I2C1_BASE + 0x04)
// This bit indicates whether software performs a Device-ID
#define SPECIAL     0x800
// If bit 11 (SPECIAL) is set to 1 and bit 13(Device-ID) is set to 0
#define GC_OR_START 0x400
// This is the target address for any master transaction.
#define IC_TAR_MSK  0x3ff

/* I2C Slave Address Register */
#define IC0_SAR                 MMIO32(I2C0_BASE + 0x08)
#define IC1_SAR                 MMIO32(I2C1_BASE + 0x08)
// The IC_SAR holds the slave address when the I2C is operating as a slave

/* I2C Rx/Tx Data Buffer and Command Register */
#define IC0_DATA_CMD            MMIO32(I2C0_BASE + 0x10)
#define IC1_DATA_CMD            MMIO32(I2C1_BASE + 0x10)
// first data byte received after the address phase for receive transfer
#define FIRST_DATA_BYTE 0x800
// This bit controls whether a RESTART is issued before the byte is sent or received.
#define RESTART         0x400
// This bit controls whether a STOP is issued after the byte is sent or received.
#define STOP            0x200
// This bit controls whether a read or a write is performed.
#define CMD             0x100
// This register contains the data to be transmitted or received on the I2C bus.
#define DAT_MSK         0x0ff

/* Standard Speed I2C Clock SCL High Count Register */
#define IC0_SS_SCL_HCNT         MMIO32(I2C0_BASE + 0x14)
#define IC1_SS_SCL_HCNT         MMIO32(I2C1_BASE + 0x14)
// register sets the SCL clock high-period count for standard speed.

/* Standard Speed I2C Clock SCL Low Count Register */
#define IC0_SS_SCL_LCNT         MMIO32(I2C0_BASE + 0x18)
#define IC1_SS_SCL_LCNT         MMIO32(I2C1_BASE + 0x18)
// sets the SCL clock low period count for standard speed.

/* Fast Mode or Fast Mode Plus I2C Clock SCL High Count Register */
#define IC0_FS_SCL_HCNT         MMIO32(I2C0_BASE + 0x1c)
#define IC1_FS_SCL_HCNT         MMIO32(I2C1_BASE + 0x1c)
// sets the SCL clock high-period count for fast mode or fast mode plus.

/* Fast Mode or Fast Mode Plus I2C Clock SCL Low Count Register */
#define IC0_FS_SCL_LCNT         MMIO32(I2C0_BASE + 0x20)
#define IC1_FS_SCL_LCNT         MMIO32(I2C1_BASE + 0x20)
// sets the SCL clock low period count for fast speed.

/* I2C Interrupt Status Register */
#define IC0_INTR_STAT           MMIO32(I2C0_BASE + 0x2c)
#define IC1_INTR_STAT           MMIO32(I2C1_BASE + 0x2c)
// See IC_RAW_INTR_STAT for a detailed description of R_RESTART_DET bit.
#define R_RESTART_DET   0x1000
// See IC_RAW_INTR_STAT for a detailed description of R_GEN_CALL bit.
#define R_GEN_CALL      0x0800
// 0x1 → R_START_DET interrupt is active
#define R_START_DET     0x0400
// 0x1 → R_STOP_DET interrupt is active
#define R_STOP_DET      0x0200
// 0x1 → R_ACTIVITY interrupt is active
#define R_ACTIVITY      0x0100
// 0x1 → R_RX_DONE interrupt is active
#define R_RX_DONE       0x0080
// 0x1 → R_TX_ABRT interrupt is active
#define R_TX_ABRT       0x0040
// 0x1 → R_RD_REQ interrupt is active
#define R_RD_REQ        0x0020
// 0x1 → R_TX_EMPTY interrupt is active
#define R_TX_EMPTY      0x0010
// 0x1 → R_TX_OVER interrupt is active
#define R_TX_OVER       0x0008
// 0x1 → R_RX_FULL interrupt is active
#define R_RX_FULL       0x0004
// 0x1 → R_RX_OVER interrupt is active
#define R_RX_OVER       0x0002
// 0x1 → RX_UNDER interrupt is active
#define R_RX_UNDER      0x0001

/* I2C Interrupt Mask Register */
#define IC0_INTR_MASK           MMIO32(I2C0_BASE + 0x30)
#define IC1_INTR_MASK           MMIO32(I2C1_BASE + 0x30)
// 0x1 → RESTART_DET interrupt is unmasked
#define M_RESTART_DET   0x1000
// 0x1 → GEN_CALL interrupt is unmasked
#define M_GEN_CALL      0x0800
// 0x1 → START_DET interrupt is unmasked
#define M_START_DET     0x0400
// 0x1 → STOP_DET interrupt is unmasked
#define M_STOP_DET      0x0200
// 0x1 → ACTIVITY interrupt is unmasked
#define M_ACTIVITY      0x0100
// 0x1 → RX_DONE interrupt is unmasked
#define M_RX_DONE       0x0080
// 0x1 → TX_ABORT interrupt is unmasked
#define M_TX_ABRT       0x0040
// 0x1 → RD_REQ interrupt is unmasked
#define M_RD_REQ        0x0020
// 0x1 → TX_EMPTY interrupt is unmasked
#define M_TX_EMPTY      0x0010
// 0x1 → TX_OVER interrupt is unmasked
#define M_TX_OVER       0x0008
// 0x1 → RX_FULL interrupt is unmasked
#define M_RX_FULL       0x0004
// 0x1 → RX_OVER interrupt is unmasked
#define M_RX_OVER       0x0002
// 0x1 → RX_UNDER interrupt is unmasked
#define M_RX_UNDER      0x0001

/* I2C Raw Interrupt Status Register */
#define IC0_RAW_INTR_STAT       MMIO32(I2C0_BASE + 0x34)
#define IC1_RAW_INTR_STAT       MMIO32(I2C1_BASE + 0x34)
// 0x1 → RESTART_DET interrupt is active
#define RESTART_DET 0x1000
// 0x1 → GEN_CALL interrupt is active
#define GEN_CALL    0x0800
// 0x1 → START_DET interrupt is active
#define START_DET   0x0400
// 0x1 → STOP_DET interrupt is active
#define STOP_DET    0x0200
// 0x1 → RAW_INTR_ACTIVITY interrupt is active
#define ACTIVITY    0x0100
// 0x1 → RX_DONE interrupt is active
#define RX_DONE     0x0080
// 0x1 → TX_ABRT interrupt is active
#define TX_ABRT     0x0040
// 0x1 → RD_REQ interrupt is active
#define RD_REQ      0x0020
// 0x1 → TX_EMPTY interrupt is active
#define TX_EMPTY    0x0010
// 0x1 → TX_OVER interrupt is active
#define TX_OVER     0x0008
// 0x1 → RX_FULL interrupt is active
#define RX_FULL     0x0004
// 0x1 → RX_OVER interrupt is active
#define RX_OVER     0x0002
// 0x1 → RX_UNDER interrupt is active
#define RX_UNDER    0x0001

/* I2C Receive FIFO Threshold Register */
#define IC0_RX_TL               MMIO32(I2C0_BASE + 0x38)
#define IC1_RX_TL               MMIO32(I2C1_BASE + 0x38)
// Receive FIFO Threshold Level.

/* I2C Transmit FIFO Threshold Register */
#define IC0_TX_TL               MMIO32(I2C0_BASE + 0x3c)
#define IC1_TX_TL               MMIO32(I2C1_BASE + 0x3c)
// Transmit FIFO Threshold Level.

/* Clear Combined and Individual Interrupt Register */
#define IC0_CLR_INTR            MMIO32(I2C0_BASE + 0x40)
#define IC1_CLR_INTR            MMIO32(I2C1_BASE + 0x40)
// Read this register to clear the combined interrupt
#define CLR_INTR    0x1

/* Clear RX_UNDER Interrupt Register */
#define IC0_CLR_RX_UNDER        MMIO32(I2C0_BASE + 0x44)
#define IC1_CLR_RX_UNDER        MMIO32(I2C1_BASE + 0x44)
// Read this register to clear the RX_UNDER interrupt (bit 0)
#define CLR_RX_UNDER    0x1

/* Clear RX_OVER Interrupt Register */
#define IC0_CLR_RX_OVER         MMIO32(I2C0_BASE + 0x48)
#define IC1_CLR_RX_OVER         MMIO32(I2C1_BASE + 0x48)
// Read this register to clr the RX_OVER int (bit 1) of the IC_RAW_INTR_STAT reg.
#define CLR_RX_OVER 0x1

/* Clear TX_OVER Interrupt Register */
#define IC0_CLR_TX_OVER         MMIO32(I2C0_BASE + 0x4c)
#define IC1_CLR_TX_OVER         MMIO32(I2C1_BASE + 0x4c)
// Read this register to clear the TX_OVER int (bit 3) of the IC_RAW_INTR_STAT reg.
#define CLR_TX_OVER 0x1

/* Clear RD_REQ Interrupt Register */
#define IC0_CLR_RD_REQ          MMIO32(I2C0_BASE + 0x50)
#define IC1_CLR_RD_REQ          MMIO32(I2C1_BASE + 0x50)
// Read this reg to clr the RD_REQ int (bit 5) of the IC_RAW_INTR_STAT reg.
#define CLR_RD_REQ  0x1

/* Clear TX_ABRT Interrupt Register */
#define IC0_CLR_TX_ABRT         MMIO32(I2C0_BASE + 0x54)
#define IC1_CLR_TX_ABRT         MMIO32(I2C1_BASE + 0x54)
// Read this register to clr the TX_ABRT int (bit 6) of the IC_RAW_INTR_STAT reg
#define CLR_TX_ABRT 0x1

/* Clear RX_DONE Interrupt Register */
#define IC0_CLR_RX_DONE         MMIO32(I2C0_BASE + 0x58)
#define IC1_CLR_RX_DONE         MMIO32(I2C1_BASE + 0x58)
// Read this register to clr the RX_DONE int (bit 7) of RO the IC_RAW_INTR_STAT reg.
#define CLR_RX_DONE 0x1

/* Clear ACTIVITY Interrupt Register */
#define IC0_CLR_ACTIVITY        MMIO32(I2C0_BASE + 0x5c)
#define IC1_CLR_ACTIVITY        MMIO32(I2C1_BASE + 0x5c)
// clears the ACTIVITY interrupt if the I2C is not active anymore.
#define CLR_ACTIVITY    0x1

/* Clear STOP_DET Interrupt Register */
#define IC0_CLR_STOP_DET        MMIO32(I2C0_BASE + 0x60)
#define IC1_CLR_STOP_DET        MMIO32(I2C1_BASE + 0x60)
// Read this reg to clr the STOP_DET interrupt (bit 9) of the IC_RAW_INTR_STAT reg.
#define CLR_STOP_DET    0x1

/* Clear START_DET Interrupt Register */
#define IC0_CLR_START_DET       MMIO32(I2C0_BASE + 0x64)
#define IC1_CLR_START_DET       MMIO32(I2C1_BASE + 0x64)
// Read this reg to clr the START_DET int (bit 10) of the IC_RAW_INTR_STAT reg.
#define CLR_START_DET   0x1

/* Clear GEN_CALL Interrupt Register */
#define IC0_CLR_GEN_CALL        MMIO32(I2C0_BASE + 0x68)
#define IC1_CLR_GEN_CALL        MMIO32(I2C1_BASE + 0x68)
// Read this reg to clr the GEN_CALL int (bit 11) of IC_RAW_INTR_STAT reg.
#define CLR_GEN_CALL    0x1

/* I2C ENABLE Register */
#define IC0_ENABLE              MMIO32(I2C0_BASE + 0x6c)
#define IC1_ENABLE              MMIO32(I2C1_BASE + 0x6c)
// Blocks the transmission of data on I2C bus even if Tx FIFO has data to transmit
#define TX_CMD_BLOCK    0x4
// When set, the controller initiates the transfer abort. - 0:
#define ABORT_IC        0x2
// Controls whether the DW_apb_i2c is enabled
#define ENABLE_IC       0x1

/* I2C STATUS Register */
#define IC0_STATUS              MMIO32(I2C0_BASE + 0x70)
#define IC1_STATUS              MMIO32(I2C1_BASE + 0x70)
// Slave FSM Activity Status.
#define SLV_ACTIVITY    0x40
// Master FSM Activity Status.
#define MST_ACTIVITY    0x20
// Receive FIFO Completely Full.
#define RFF             0x10
// Receive FIFO Not Empty.
#define RFNE            0x08
// Transmit FIFO Completely Empty.
#define TFE             0x04
// Transmit FIFO Not Full.
#define TFNF            0x02
// I2C Activity Status. Reset value: 0x0
#define ACTIVITY_ST     0x01

/* I2C Transmit FIFO Level Register */
#define IC0_TXFLR               MMIO32(I2C0_BASE + 0x74)
#define IC1_TXFLR               MMIO32(I2C1_BASE + 0x74)
// Transmit FIFO Level.

/* I2C Receive FIFO Level Register */
#define IC0_RXFLR               MMIO32(I2C0_BASE + 0x78)
#define IC1_RXFLR               MMIO32(I2C1_BASE + 0x78)
// Receive FIFO Level.

/* I2C SDA Hold Time Length Register */
#define IC0_SDA_HOLD            MMIO32(I2C0_BASE + 0x7c)
#define IC1_SDA_HOLD            MMIO32(I2C1_BASE + 0x7c)
// Sets the required SDA hold time in units of ic_clk period,
#define IC_SDA_RX_HOLD_SFT  16
#define IC_SDA_RX_HOLD_MSK  0xff0000
// Sets the required SDA hold time in units of ic_clk period,
#define IC_SDA_TX_HOLD_MSK  0x00ffff

/* I2C Transmit Abort Source Register */
#define IC0_TX_ABRT_SOURCE      MMIO32(I2C0_BASE + 0x80)
#define IC1_TX_ABRT_SOURCE      MMIO32(I2C1_BASE + 0x80)
// Role of DW_apb_i2c: Master-Transmitter or Slave-Transmitter
#define TX_FLUSH_CNT_SFT        23
#define TX_FLUSH_CNT0_FETCH     ((IC0_TX_ABRT_SOURCE>>TX_FLUSH_CNT_SFT)&0xff)
#define TX_FLUSH_CNT1_FETCH     ((IC1_TX_ABRT_SOURCE>>TX_FLUSH_CNT_SFT)&0xff)
#define TX_FLUSH_CNT_MSK        0xff000000
// 0x1 → Transfer abort detected by master
#define ABRT_USER_ABRT          0x00010000
// 0x1 → Slave trying to transmit to remote master in read mode
#define ABRT_SLVRD_INTX         0x00008000
// 0x1 → Slave lost arbitration to remote master
#define ABRT_SLV_ARBLOST        0x00004000
// 0x1 → Slave flushes existing data in TX-FIFO upon getting read command
#define ABRT_SLVFLUSH_TXFIFO    0x00002000
// 0x1 → Master or Slave-Transmitter lost arbitration
#define ARB_LOST                0x00001000
// 0x1 → User initiating master operation when MASTER disabled
#define ABRT_MASTER_DIS         0x00000800
// 0x1 → Master trying to read in 10Bit addressing mode when RESTART disabled
#define ABRT_10B_RD_NORSTRT     0x00000400
// 0x1 → User trying to send START byte when RESTART disabled
#define ABRT_SBYTE_NORSTRT      0x00000200
// 0x1 → User trying to switch Master to HS mode when RESTART disabled
#define ABRT_HS_NORSTRT         0x00000100
// 0x1 → ACK detected for START byte
#define ABRT_SBYTE_ACKDET       0x00000080
// 0x1 → HS Master code ACKed in HS Mode
#define ABRT_HS_ACKDET          0x00000040
// 0x1 → GCALL is followed by read from bus
#define ABRT_GCALL_READ         0x00000020
// 0x1 → GCALL not ACKed by any slave
#define ABRT_GCALL_NOACK        0x00000010
// 0x1 → Transmitted data not ACKed by addressed slave
#define ABRT_TXDATA_NOACK       0x00000008
// 0x1 → Byte 2 of 10Bit Address not ACKed by any slave
#define ABRT_10ADDR2_NOACK      0x00000004
// 0x1 → Byte 1 of 10Bit Address not ACKed by any slave
#define ABRT_10ADDR1_NOACK      0x00000002
// 0x1 → This abort is generated because of NOACK for 7-bit address
#define ABRT_7B_ADDR_NOACK      0x00000001

/* Generate Slave Data NACK Register */
#define IC0_SLV_DATA_NACK_ONLY  MMIO32(I2C0_BASE + 0x84)
#define IC1_SLV_DATA_NACK_ONLY  MMIO32(I2C1_BASE + 0x84)
// 0x1 → Slave receiver generates NACK upon data
#define NACK    0x1

/* DMA Control Register */
#define IC0_DMA_CR              MMIO32(I2C0_BASE + 0x88)
#define IC1_DMA_CR              MMIO32(I2C1_BASE + 0x88)
// Transmit DMA Enable.
#define TDMAE   0x2
// Receive DMA Enable.
#define RDMAE   0x1

/* DMA Transmit Data Level Register */
#define IC0_DMA_TDLR            MMIO32(I2C0_BASE + 0x8c)
#define IC1_DMA_TDLR            MMIO32(I2C1_BASE + 0x8c)
// controls the level at which a DMA request is made by the transmit logic.

/* DMA Transmit Data Level Register */
#define IC0_DMA_RDLR            MMIO32(I2C0_BASE + 0x90)
#define IC1_DMA_RDLR            MMIO32(I2C1_BASE + 0x90)
// This bit field ctrls the lvl at which a DMA req is made by the receive logic.

/* I2C SDA Setup Register */
#define IC0_SDA_SETUP           MMIO32(I2C0_BASE + 0x94)
#define IC1_SDA_SETUP           MMIO32(I2C1_BASE + 0x94)
// It is recommended that if the required delay is 1000ns

/* I2C ACK General Call Register */
#define IC0_ACK_GENERAL_CALL    MMIO32(I2C0_BASE + 0x98)
#define IC1_ACK_GENERAL_CALL    MMIO32(I2C1_BASE + 0x98)
// 0x1 → Generate ACK for a General Call
#define ACK_GEN_CALL    0x1

/* I2C Enable Status Register */
#define IC0_ENABLE_STATUS       MMIO32(I2C0_BASE + 0x9c)
#define IC1_ENABLE_STATUS       MMIO32(I2C1_BASE + 0x9c)
// 0x1 → Slave RX Data is lost
#define SLV_RX_DATA_LOST        0x4
// 0x1 → Slave is disabled when it is active
#define SLV_DISABLED_WHILE_BUSY 0x2
// 0x1 → I2C enabled
#define IC_EN                   0x1

/* I2C SS, FS or FM+ spike suppression limit */
#define IC0_FS_SPKLEN           MMIO32(I2C0_BASE + 0xa0)
#define IC1_FS_SPKLEN           MMIO32(I2C1_BASE + 0xa0)
// This register sets the duration, measured in ic_clk cycles

/* Clear RESTART_DET Interrupt Register */
#define IC0_CLR_RESTART_DET     MMIO32(I2C0_BASE + 0xa8)
#define IC1_CLR_RESTART_DET     MMIO32(I2C1_BASE + 0xa8)
// Read this reg to clr the RESTART_DET interrupt (bit12) of IC_RAW_INTR_STAT reg.
#define CLR_RESTART_DET 0x1

/* Component Parameter Register 1 */
#define IC0_COMP_PARAM_1        MMIO32(I2C0_BASE + 0xf4)
#define IC1_COMP_PARAM_1        MMIO32(I2C1_BASE + 0xf4)
// TX Buffer Depth = 16
#define TX_BUFFER_DEPTH_MSK 0xff0000
#define TX_BUFFER_DEPTH_SFT 16
// RX Buffer Depth = 16
#define RX_BUFFER_DEPTH_MSK 0x00ff00
#define RX_BUFFER_DEPTH_SFT 8
// Encoded parameters not visible
#define ADD_ENCODED_PARAMS  0x000080
// DMA handshaking signals are enabled
#define HAS_DMA             0x000040
// COMBINED Interrupt outputs
#define INTR_IO             0x000020
// Programmable count values for each mode.
#define HC_COUNT_VALUES     0x000010
// MAX SPEED MODE = FAST MODE
#define MAX_SPEED_MODE_MSK  0x00000c
#define MAX_SPEED_MODE_SFT  2
// APB data bus width is 32 bits
#define APB_DATA_WIDTH_MSK  0x000003

/* I2C Component Version Register */
#define IC0_COMP_VERSION        MMIO32(I2C0_BASE + 0xf8)
#define IC1_COMP_VERSION        MMIO32(I2C1_BASE + 0xf8)
// 0x3230312a

/* I2C Component Type Register */
#define IC0_COMP_TYPE           MMIO32(I2C0_BASE + 0xfc)
#define IC1_COMP_TYPE           MMIO32(I2C1_BASE + 0xfc)
// Designware Component Type number = 0x44_57_01_40.

#endif
