#ifndef H_SPI_REGS
#define H_SPI_REGS
/*
 * RP2040 alternative library, SPI controller registers
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "system_regs/memorymap.h"

/* Control register 0, SSPCR0 on page 3-4 */
#define SSP0CR0         MMIO32(SPI0_BASE + 0x000)
#define SSP1CR0         MMIO32(SPI1_BASE + 0x000)
// Serial clock rate.
#define SCR_MSK         0xff00
#define SCR_SFT         8
#define SCR0_SET(n)     ((n&0xff)<<SCR_SFT)
#define SCR1_SET(n)     ((n&0xff)<<SCR_SFT)
// SSPCLKOUT phase, applicable to Motorola SPI frame
#define SPH             0x0080
// SSPCLKOUT polarity, applicable to Motorola SPI frame
#define SPO             0x0040
// Frame format:
#define FRF_MOTO        0x0000
#define FRF_TI          0x0010
#define FRF_NATIONAL    0x0020
// Data Size Select:
#define DSS4BIT         0x0003
#define DSS5BIT         0x0004
#define DSS6BIT         0x0005
#define DSS7BIT         0x0006
#define DSS8BIT         0x0007
#define DSS9BIT         0x0008
#define DSS10BIT        0x0009
#define DSS11BIT        0x000a
#define DSS12BIT        0x000b
#define DSS13BIT        0x000c
#define DSS14BIT        0x000d
#define DSS15BIT        0x000e
#define DSS16BIT        0x000f

/* Control register 1, SSPCR1 on page 3-5 */
#define SSP0CR1         MMIO32(SPI0_BASE + 0x004)
#define SSP1CR1         MMIO32(SPI1_BASE + 0x004)
// Slave-mode output disable.
#define SOD 0x8
// Master or slave mode select.
#define MS  0x4
// Synchronous serial port enable:
#define SSE 0x2
// Loop back mode:
#define LBM 0x1

/* Data register, SSPDR on page 3-6 */
#define SSP0DR          MMIO32(SPI0_BASE + 0x008)
#define SSP1DR          MMIO32(SPI1_BASE + 0x008)
// Transmit/Receive FIFO: Read Receive FIFO.

/* Status register, SSPSR on page 3-7 */
#define SSP0SR          MMIO32(SPI0_BASE + 0x00c)
#define SSP1SR          MMIO32(SPI1_BASE + 0x00c)
// PrimeCell SSP busy flag
#define BSY 0x10
// Receive FIFO full
#define RFF 0x08
// Receive FIFO not empty
#define RNE 0x04
// Transmit FIFO not full
#define TNF 0x02
// Transmit FIFO empty
#define TFE 0x01

/* Clock prescale register, SSPCPSR on page 3-8 */
#define SSP0CPSR        MMIO32(SPI0_BASE + 0x010)
#define SSP1CPSR        MMIO32(SPI1_BASE + 0x010)
// Clock prescale divisor.

/* Interrupt mask set or clear register, SSPIMSC on page 3-9 */
#define SSP0IMSC        MMIO32(SPI0_BASE + 0x014)
#define SSP1IMSC        MMIO32(SPI1_BASE + 0x014)
// Transmit FIFO interrupt mask
#define TXIM    0x8
// Receive FIFO interrupt mask
#define RXIM    0x4
// Receive timeout interrupt mask
#define RTIM    0x2
// Receive overrun interrupt mask
#define RORIM   0x1

/* Raw interrupt status register, SSPRIS on page 3-10 */
#define SSP0RIS         MMIO32(SPI0_BASE + 0x018)
#define SSP1RIS         MMIO32(SPI1_BASE + 0x018)
// prior to masking, of the SSPTXINTR interrupt
#define TXRIS   0x8
// prior to masking, of the SSPRXINTR interrupt
#define RXRIS   0x4
// prior to masking, of the SSPRTINTR interrupt
#define RTRIS   0x2
// prior to masking, of the SSPRORINTR interrupt
#define RORRIS  0x1

/* Masked interrupt status register, SSPMIS on page 3-11 */
#define SSP0MIS         MMIO32(SPI0_BASE + 0x01c)
#define SSP1MIS         MMIO32(SPI1_BASE + 0x01c)
// interrupt state, after masking, of the SSPTXINTR interrupt
#define TXMIS   0x8
// interrupt state, after masking, of the SSPRXINTR interrupt
#define RXMIS   0x4
// interrupt state, after masking, of the SSPRTINTR interrupt
#define RTMIS   0x2
// interrupt state, after masking, of the SSPRORINTR interrupt
#define RORMIS  0x1

/* Interrupt clear register, SSPICR on page 3-11 */
#define SSP0ICR         MMIO32(SPI0_BASE + 0x020)
#define SSP1ICR         MMIO32(SPI1_BASE + 0x020)
// Clears the SSPRTINTR interrupt
#define RTIC    0x2
// Clears the SSPRTINTR interrupt
#define RORIC   0x1

/* DMA control register, SSPDMACR on page 3-12 */
#define SSP0DMACR       MMIO32(SPI0_BASE + 0x024)
#define SSP1DMACR       MMIO32(SPI1_BASE + 0x024)
// Transmit DMA Enable.
#define TXDMAE  0x2
// Receive DMA Enable.
#define RXDMAE  0x1

/* Peripheral identification registers, SSPPeriphID0-3 on page 3-13 */
#define SSP0PERIPHID0   MMIO32(SPI0_BASE + 0xfe0)
#define SSP1PERIPHID0   MMIO32(SPI1_BASE + 0xfe0)
// PARTNUMBER0 These bits read back as 0x22
#define SSP0PERIPHID1   MMIO32(SPI0_BASE + 0xfe4)
#define SSP1PERIPHID1   MMIO32(SPI1_BASE + 0xfe4)
// DESIGNER0 These bits read back as 0x1
// PARTNUMBER1 These bits read back as 0x0
#define SSP0PERIPHID2   MMIO32(SPI0_BASE + 0xfe8)
#define SSP1PERIPHID2   MMIO32(SPI1_BASE + 0xfe8)
// REVISION These bits return the peripheral revision
// DESIGNER1 These bits read back as 0x4
#define SSP0PERIPHID3   MMIO32(SPI0_BASE + 0xfec)
#define SSP1PERIPHID3   MMIO32(SPI1_BASE + 0xfec)
// CONFIGURATION These bits read back as 0x00

/* PrimeCell identification registers, SSPPCellID0-3 on page 3-16 */
#define SSP0PCELLID0    MMIO32(SPI0_BASE + 0xff0)
#define SSP1PCELLID0    MMIO32(SPI1_BASE + 0xff0)
// SSPPCELLID0 These bits read back as 0x0D
#define SSP0PCELLID1    MMIO32(SPI0_BASE + 0xff4)
#define SSP1PCELLID1    MMIO32(SPI1_BASE + 0xff4)
// SSPPCELLID1 These bits read back as 0xF0
#define SSP0PCELLID2    MMIO32(SPI0_BASE + 0xff8)
#define SSP1PCELLID2    MMIO32(SPI1_BASE + 0xff8)
// SSPPCELLID2 These bits read back as 0x05
#define SSP0PCELLID3    MMIO32(SPI0_BASE + 0xffc)
#define SSP1PCELLID3    MMIO32(SPI1_BASE + 0xffc)
// SSPPCELLID3 These bits read back as 0xB1

#endif
