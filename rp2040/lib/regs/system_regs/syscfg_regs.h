#ifndef H_SYSCFG_REGS
#define H_SYSCFG_REGS
/*
 * RP2040 alternative library, system config block registers
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Processor core 0 NMI source mask */
#define PROC0_NMI_MASK          MMIO32(SYSCFG_BASE + 0x00)
/* Processor core 1 NMI source mask */
#define PROC1_NMI_MASK          MMIO32(SYSCFG_BASE + 0x04)
// Set a bit high to enable NMI from that IRQ

/* Configuration for processors */
#define PROC_CONFIG             MMIO32(SYSCFG_BASE + 0x08)
// Configure proc1 DAP instance ID.
#define PROC1_DAP_INST_ID_MSK   0xf0000000
#define PROC1_DAP_INST_ID_SFT   28
#define PROC1_DAP_INST_ID_FETCH ((PROC_CONFIG>>PROC1_DAP_INST_ID_SFT)&0xf)
// Configure proc0 DAP instance ID.
#define PROC0_DAP_INST_ID_MSK   0x0f000000
#define PROC0_DAP_INST_ID_SFT   24
#define PROC0_DAP_INST_ID_FETCH ((PROC_CONFIG>>PROC0_DAP_INST_ID_SFT)&0xf)
// Indication that proc1 has halted
#define PROC1_HALTED        0x00000002
// Indication that proc0 has halted
#define PROC0_HALTED        0x00000001

/* bypass the input synchronizer between that GPIORW0x00000000
and the GPIO input register in the SIO. */
#define PROC_IN_SYNC_BYPASS     MMIO32(SYSCFG_BASE + 0x0c)
#define PROC_IN_SYNC_BYPASS_HI  MMIO32(SYSCFG_BASE + 0x10)

/* Directly control the SWD debug port of either processor */
#define DBGFORCE                MMIO32(SYSCFG_BASE + 0x14)
// Attach processor 1 debug port to syscfg controls
#define PROC1_ATTACH    0x80
// Directly drive processor 1 SWCLK
#define PROC1_SWCLK     0x40
// Directly drive processor 1 SWDIO input
#define PROC1_SWDI      0x20
// Observe the value of processor 1 SWDIO output.
#define PROC1_SWDO      0x10
// Attach processor 0 debug port to syscfg controls
#define PROC0_ATTACH    0x08
// Directly drive processor 0 SWCLK, if PROC0_ATTACH is set
#define PROC0_SWCLK     0x04
// Directly drive processor 0 SWDIO input, if PROC0_ATTACH is set
#define PROC0_SWDI      0x02
// Observe the value of processor 0 SWDIO output.
#define PROC0_SWDO      0x01

/* Control power downs to memories. Set high to power down memories. */
#define MEMPOWERDOWN            MMIO32(SYSCFG_BASE + 0x18)
#define ROM_PDWN_MEM    0x80
#define USB_PDWN_MEM    0x40
#define SRAM5_PDWN_MEM  0x20
#define SRAM4_PDWN_MEM  0x10
#define SRAM3_PDWN_MEM  0x08
#define SRAM2_PDWN_MEM  0x04
#define SRAM1_PDWN_MEM  0x02
#define SRAM0_PDWN_MEM  0x01

#endif
