#ifndef H_POWERON_REGS
#define H_POWERON_REGS
/*
 * RP2040 alternative library, Power-On State Machine registers
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Force block out of reset (i.e. power it on) */
#define FRCE_ON     MMIO32(PSM_BASE + 0x0)
// Force block out of reset (i.e. power it on)
#define PROC1_ON                0x10000
#define PROC0_ON                0x08000
#define SIO_ON                  0x04000
#define VREG_AND_CHIP_RESET_ON  0x02000
#define XIP_ON                  0x01000
#define SRAM5_ON                0x00800
#define SRAM4_ON                0x00400
#define SRAM3_ON                0x00200
#define SRAM2_ON                0x00100
#define SRAM1_ON                0x00080
#define SRAM0_ON                0x00040
#define ROM_ON                  0x00020
#define BUSFABRIC_ON            0x00010
#define RESETS_ON               0x00008
#define CLOCKS_ON               0x00004
#define XOSC_ON                 0x00002
#define ROSC_ON                 0x00001

/* Force into reset (i.e. power it off) */
#define FRCE_OFF    MMIO32(PSM_BASE + 0x4)

/* Set to 1 if this peripheral should be reset when the watchdog fires. */
#define WDSEL       MMIO32(PSM_BASE + 0x8)

/* Indicates the peripheral’s registers are ready to access. */
#define DONE        MMIO32(PSM_BASE + 0xc)

#endif
