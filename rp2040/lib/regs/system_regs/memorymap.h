#ifndef H_MEMORYMAP
#define H_MEMORYMAP
/*
 * RP2040 alternative library, memorymap
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// XIP:
#define ROM_BASE                    0x00000000
#define XIP_BASE                    0x10000000
#define XIP_NOALLOC_BASE            0x11000000
#define XIP_NOCACHE_BASE            0x12000000
#define XIP_NOCACHE_NOALLOC_BASE    0x13000000
#define XIP_CTRL_BASE               0x14000000
#define XIP_SRAM_BASE               0x15000000
#define XIP_SRAM_END                0x15004000
#define XIP_SSI_BASE                0x18000000
// SRAM. SRAM0-3 striped:
#define SRAM_BASE                   0x20000000
#define SRAM_STRIPED_BASE           0x20000000
#define SRAM_STRIPED_END            0x20040000
//SRAM 4-5 are always non-striped:
#define SRAM4_BASE                  0x20040000
#define SRAM5_BASE                  0x20041000
#define SRAM_END                    0x20042000
// Non-striped aliases of SRAM0-3:
#define SRAM0_BASE                  0x21000000
#define SRAM1_BASE                  0x21010000
#define SRAM2_BASE                  0x21020000
#define SRAM3_BASE                  0x21030000
// APB Peripherals:
#define SYSINFO_BASE                0x40000000
#define SYSCFG_BASE                 0x40004000
#define CLOCKS_BASE                 0x40008000
#define RESETS_BASE                 0x4000c000
#define PSM_BASE                    0x40010000
#define IO_BANK0_BASE               0x40014000
#define IO_QSPI_BASE                0x40018000
#define PADS_BANK0_BASE             0x4001c000
#define PADS_QSPI_BASE              0x40020000
#define XOSC_BASE                   0x40024000
#define PLL_SYS_BASE                0x40028000
#define PLL_USB_BASE                0x4002c000
#define BUSCTRL_BASE                0x40030000
#define UART0_BASE                  0x40034000
#define UART1_BASE                  0x40038000
#define SPI0_BASE                   0x4003c000
#define SPI1_BASE                   0x40040000
#define I2C0_BASE                   0x40044000
#define I2C1_BASE                   0x40048000
#define ADC_BASE                    0x4004c000
#define PWM_BASE                    0x40050000
#define TIMER_BASE                  0x40054000
#define WATCHDOG_BASE               0x40058000
#define RTC_BASE                    0x4005c000
#define ROSC_BASE                   0x40060000
#define VREG_AND_CHIP_RESET_BASE    0x00064000
#define TBMAN_BASE                  0x0006c000
// AHB-Lite peripherals:
#define DMA_BASE                    0x50000000
// USB has a DPRAM at its base followed by registers:
#define USBCTRL_BASE                0x50100000
#define USBCTRL_DPRAM_BASE          0x50100000
#define USBCTRL_REGS_BASE           0x50110000
// Remaining AHB-Lite peripherals:
#define PIO0_BASE                   0x50200000
#define PIO1_BASE                   0x50300000
#define XIP_AUX_BASE                0x50400000
// IOPORT Peripherals:
#define SIO_BASE                    0xd0000000
// Cortex-M0+ Internal Peripherals:
#define PPB_BASE                    0xe0000000

// RAM banks
#define BANK0                       0x20000000
#define BANK1                       0x20000004
#define BANK2                       0x20000008
#define BANK3                       0x2000000c
#define BANK0_1                     0x20000010
#define BANK1_1                     0x20000014
#define BANK2_1                     0x20000018
#define BANK3_1                     0x2000001c
#define BANK0_2                     0x20000020
#define BANK1_2                     0x20000024
#define BANK2_2                     0x20000028
#define BANK3_2                     0x2000002c

// access to register with specified address
#define  MMIO32(addr)		(*(volatile uint32_t *)(addr))

// interrupts
#define TIMER_IRQ_0     0
#define TIMER_IRQ_1     1
#define TIMER_IRQ_2     2
#define TIMER_IRQ_3     3
#define PWM_IRQ_WRAP    4
#define USBCTRL_IRQ     5
#define XIP_IRQ         6
#define PIO0_IRQ_0      7
#define PIO0_IRQ_1      8
#define PIO1_IRQ_0      9
#define PIO1_IRQ_1      10
#define DMA_IRQ_0       11
#define DMA_IRQ_1       12
#define IO_IRQ_BANK0    13
#define IO_IRQ_QSPI     14
#define SIO_IRQ_PROC0   15
#define SIO_IRQ_PROC1   16
#define CLOCKS_IRQ      17
#define SPI0_IRQ        18
#define SPI1_IRQ        19
#define UART0_IRQ       20
#define UART1_IRQ       21
#define ADC_IRQ_FIFO    22
#define I2C0_IRQ        23
#define I2C1_IRQ        24
#define RTC_IRQ         25


#endif
