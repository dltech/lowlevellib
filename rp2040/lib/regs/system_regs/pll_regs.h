#ifndef H_PLL_REGS
#define H_PLL_REGS
/*
 * RP2040 alternative library, PLL registers
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Control and Status */
#define CS          MMIO32(PLL_USB_BASE + 0x0)
// PLL is locked
#define LOCK        0x80000000
// Passes the reference clock to the output instead of the divided VCO.
#define BYPASS      0x00000100
// Divides the PLL input reference clock.
#define REFDIV_MSK  0x3f

/* Controls the PLL power modes. */
#define PWR         MMIO32(PLL_USB_BASE + 0x4)
// PLL VCO powerdown
#define VCOPD       0x20
// PLL post divider powerdown
#define POSTDIVPD   0x08
// PLL DSM powerdown
#define DSMPD       0x04
// PLL powerdown
#define PD          0x01

/* Feedback divisor */
#define FBDIV_INT   MMIO32(PLL_USB_BASE + 0x8)
// see ctrl reg description for constraints

/* Controls the PLL post dividers for the primary output */
#define PRIM        MMIO32(PLL_USB_BASE + 0xc)
// divide by 1-7
#define POSTDIV1_1  0x10000
#define POSTDIV1_2  0x20000
#define POSTDIV1_3  0x30000
#define POSTDIV1_4  0x40000
#define POSTDIV1_5  0x50000
#define POSTDIV1_6  0x60000
#define POSTDIV1_7  0x70000
// divide by 1-7
#define POSTDIV2_1  0x01000
#define POSTDIV2_2  0x02000
#define POSTDIV2_3  0x03000
#define POSTDIV2_4  0x04000
#define POSTDIV2_5  0x05000
#define POSTDIV2_6  0x06000
#define POSTDIV2_7  0x07000

#endif
