#ifndef H_XOSC_REGS
#define H_XOSC_REGS
/*
 * RP2040 alternative library, Crystal Oscillator registers
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Crystal Oscillator Control */
#define CTRL    MMIO32(XOSC_BASE + 0x00)
// DISABLE and the chip runs from the ROSC.
#define ENABLE_XOSC         0xd1e000
#define DISABLE_XOSC        0xfab000
// Frequency range. This resets to 0xAA0 and cannot be changed.
#define FREQ_RANGE_1_15MHZ  0x000aa0

/* Crystal Oscillator Status */
#define STATUS  MMIO32(XOSC_BASE + 0x04)
// Oscillator is running and stable
#define STABLE                  0x80000000
// An invalid value has been wr to CTRL_ENABLE or CTRL_FREQ_RANGE or DORMANT
#define BADWRITE                0x01000000
// Oscillator is enabled but not necessarily running and stable, resets to 0
#define ENABLED                 0x00001000
// The current frequency range setting, always reads 0
#define FREQ_RANGE_ST_1_15MHZ   0x00000000

/* Crystal Oscillator pause control */
#define DORMANT MMIO32(XOSC_BASE + 0x08)
// This is used to save power by pausing the XOSC

/* Controls the startup delay */
#define STARTUP MMIO32(XOSC_BASE + 0x0c)
// Multiplies the startup_delay by 4.
#define X4              0x00100000
// in multiples of 256*xtal_period.
#define DELAY(cycle)    (cycle/256)&0x3fff

/* A down counter running at the XOSC frequency which counts to zero and stops. */
#define COUNT   MMIO32(XOSC_BASE + 0x1c)

#endif
