#ifndef H_XIP_REGS
#define H_XIP_REGS
/*
 * RP2040 alternative library, execute-in-place (XIP) hardware registers
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Cache control */
#define CTRL        MMIO32(XIP_CTRL_BASE + 0x00)
// When 1, the cache memories are powered down.
#define POWER_DOWN      0x8
// When 1, writes to any alias other than 0x0 (caching allocating)
#define ERR_BADWRITE    0x2
// When 1, enable the cache.
#define XIP_EN          0x1

/* Cache Flush control */
#define FLUSH       MMIO32(XIP_CTRL_BASE + 0x04)
// Write 1 to flush the cache.

/* Cache Status */
#define STAT        MMIO32(XIP_CTRL_BASE + 0x08)
// When 1, indicates the XIP streaming FIFO is completely full.
#define FIFO_FULL   0x4
// When 1, indicates the XIP streaming FIFO is completely empty.
#define FIFO_EMPTY  0x2
// Reads as 0 while a cache flush is in progress, and 1 otherwise.
#define FLUSH_READY 0x1


/* Cache Hit counter */
#define CTR_HIT     MMIO32(XIP_CTRL_BASE + 0x0c)
// A 32 bit saturating counter that increments upon each cache hit,

/* Cache Access counter */
#define CTR_ACC     MMIO32(XIP_CTRL_BASE + 0x10)
// A 32 bit saturating counter that increments upon each XIP access,

/* FIFO stream address */
#define STREAM_ADDR MMIO32(XIP_CTRL_BASE + 0x14)
// The address of the next word to be streamed from flash to the streaming FIFO.
#define STREAM_ADDR_SFT 2

/* FIFO stream control */
#define STREAM_CTR  MMIO32(XIP_CTRL_BASE + 0x18)
// Write a nonzero value to start a streaming read.

/* FIFO stream data */
#define STREAM_FIFO MMIO32(XIP_CTRL_BASE + 0x1c)
// Streamed data is buffered here, for retrieval by the system DMA.

#endif
