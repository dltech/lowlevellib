#ifndef H_ROSC_REGS
#define H_ROSC_REGS
/*
 * RP2040 alternative library, Ring Oscillator registers
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Ring Oscillator control */
#define CTRL        MMIO32(ROSC_BASE + 0x00)
// before setting this field to DISABLE otherwise the chip will lock up
#define DISABLE_RSOSC       0xd1e000
#define ENABLE_RSOSC        0xfab000
// Controls the number of delay stages in the ROSC ring
#define FREQ_RANGE_LOW      0x000fa4
#define FREQ_RANGE_MEDIUM   0x000fa5
#define FREQ_RANGE_HIGH     0x000fa7
#define FREQ_RANGE_TOOHIGH  0x000fa6

/* Ring Oscillator frequency control A */
#define FREQA       MMIO32(ROSC_BASE + 0x04)
// set to 0xaa
#define PASSWD_OK   0x96960000
#define PASSWD_MSK  0xffff0000
#define PASSWD_SFT  16
// Stage 3 drive strength
#define DS3_DEFAULT 0x00000000
#define DS3_DOUBLE  0x00001000
#define DS3_TRIPLE  0x00002000
#define DS3_QUAD    0x00003000
// Stage 2 drive strength
#define DS2_DEFAULT 0x00000000
#define DS2_DOUBLE  0x00000100
#define DS2_TRIPLE  0x00000200
#define DS2_QUAD    0x00000300
// Stage 1 drive strength
#define DS1_DEFAULT 0x00000000
#define DS1_DOUBLE  0x00000010
#define DS1_TRIPLE  0x00000020
#define DS1_QUAD    0x00000030
// Stage 0 drive strength
#define DS0_DEFAULT 0x00000000
#define DS0_DOUBLE  0x00000001
#define DS0_TRIPLE  0x00000002
#define DS0_QUAD    0x00000003

/* Ring Oscillator frequency control B */
#define FREQB       MMIO32(ROSC_BASE + 0x08)
// Stage 7 drive strength
#define DS7_DEFAULT 0x00000000
#define DS7_DOUBLE  0x00001000
#define DS7_TRIPLE  0x00002000
#define DS7_QUAD    0x00003000
// Stage 6 drive strength
#define DS6_DEFAULT 0x00000000
#define DS6_DOUBLE  0x00000100
#define DS6_TRIPLE  0x00000200
#define DS6_QUAD    0x00000300
// Stage 5 drive strength
#define DS5_DEFAULT 0x00000000
#define DS5_DOUBLE  0x00000010
#define DS5_TRIPLE  0x00000020
#define DS5_QUAD    0x00000030
// Stage 4 drive strength
#define DS4_DEFAULT 0x00000000
#define DS4_DOUBLE  0x00000001
#define DS4_TRIPLE  0x00000002
#define DS4_QUAD    0x00000003

/* Ring Oscillator pause control */
#define DORMANT     MMIO32(ROSC_BASE + 0x0c)
// This is used to save power by pausing the ROSC
#define DORMANT_VAL 0x636f6d61
#define WAKE        0x77616b65

/* Controls the output divider */
#define DIV         MMIO32(ROSC_BASE + 0x10)
//set to 0xaa0 + div where div = 1-31 divides by div

/* Controls the phase shifted output */
#define PHASE       MMIO32(ROSC_BASE + 0x14)
// set to 0xaa
#define PASSWD_PH_MSK  0xff0
#define PASSWD_PH_SFT  4
// enable the phase-shifted output
#define ENABLE_PH   0x008
// invert the phase-shifted output
#define FLIP        0x004
// phase shift the phase-shifted output by SHIFT input clocks
#define SHIFT_MSK   0x003

/* Ring Oscillator Status */
#define STATUS      MMIO32(ROSC_BASE + 0x18)
// Oscillator is running and stable
#define STABLE          0x80000000
// An invalid value has been written to ...
#define BADWRITE        0x01000000
// post-divider is running
#define DIV_RUNNING     0x00010000
// Oscillator is enabled but not necessarily running and stable
#define ENABLED_ROSC    0x00001000

/* Returns a 1 bit random value */
#define RANDOMBIT   MMIO32(ROSC_BASE + 0x1c)

/* A down counter running at the ROSC frequency which counts to zero and stops. */
#define COUNT       MMIO32(ROSC_BASE + 0x20)

#endif
