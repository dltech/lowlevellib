#ifndef H_CLOCKS_REGS
#define H_CLOCKS_REGS
/*
 * RP2040 alternative library, clocks registers
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Clock control, can be changed on-the-fly (except for auxsrc) */
#define CLK_GPOUT0_CTRL         MMIO32(CLOCKS_BASE + 0x00)
#define CLK_GPOUT1_CTRL         MMIO32(CLOCKS_BASE + 0x0c)
#define CLK_GPOUT2_CTRL         MMIO32(CLOCKS_BASE + 0x18)
#define CLK_GPOUT3_CTRL         MMIO32(CLOCKS_BASE + 0x24)
// An edge on this signal shifts the ph of the out by 1 cycle of the input clock
#define NUDGE                   0x100000
// This delays the enable signal by up to 3 cycles of the input clock
#define PHASE_SFT               16
#define PHASE_MSK               0x030000
#define PHASE0                  0x000000
#define PHASE1                  0x010000
#define PHASE2                  0x020000
#define PHASE3                  0x030000
// Enables duty cycle correction for odd divisors
#define DC50                    0x001000
// Starts and stops the clock generator cleanly
#define ENABLE_CLOCK            0x000800
// Asynchronously kills the clock generator
#define KILL                    0x000400
// Selects the auxiliary clock source, will glitch when switching
#define AUXSRC_CLKSRC_PLL_SYS   0x000000
#define AUXSRC_CLKSRC_GPIN0     0x000020
#define AUXSRC_CLKSRC_GPIN1     0x000040
#define AUXSRC_CLKSRC_PLL_USB   0x000060
#define AUXSRC_ROSC_CLKSRC      0x000080
#define AUXSRC_XOSC_CLKSRC      0x0000a0
#define AUXSRC_CLK_SYS          0x0000c0
#define AUXSRC_CLK_USB          0x0000e0
#define AUXSRC_CLK_ADC          0x000100
#define AUXSRC_CLK_RTC          0x000120
#define AUXSRC_CLK_REF          0x000140

/* Clock divisor, can be changed on-the-fly */
#define CLK_GPOUT0_DIV          MMIO32(CLOCKS_BASE + 0x04)
#define CLK_GPOUT1_DIV          MMIO32(CLOCKS_BASE + 0x10)
#define CLK_GPOUT2_DIV          MMIO32(CLOCKS_BASE + 0x1c)
#define CLK_GPOUT3_DIV          MMIO32(CLOCKS_BASE + 0x28)
#define CLK_SYS_DIV             MMIO32(CLOCKS_BASE + 0x40)
#define CLK_RTC_DIV             MMIO32(CLOCKS_BASE + 0x70)
// Integer component of the divisor, 0 → divide by 2^16
#define INT_SFT     8
#define INT_MSK     0xffffff00
#define INT(n)      (n<<INT_SFT)&INT_MSK
// Fractional component of the divisor
#define FRAC_MSK    0xff

/* Indicates which SRC is currently selected by the glitchless mux (one-hot). */
#define CLK_GPOUT0_SELECTED     MMIO32(CLOCKS_BASE + 0x08)
#define CLK_GPOUT1_SELECTED     MMIO32(CLOCKS_BASE + 0x14)
#define CLK_GPOUT2_SELECTED     MMIO32(CLOCKS_BASE + 0x20)
#define CLK_GPOUT3_SELECTED     MMIO32(CLOCKS_BASE + 0x2c)
#define CLK_PERI_SELECTED       MMIO32(CLOCKS_BASE + 0x50)
#define CLK_USB_SELECTED        MMIO32(CLOCKS_BASE + 0x5c)
#define CLK_ADC_SELECTED        MMIO32(CLOCKS_BASE + 0x68)
#define CLK_RTC_SELECTED        MMIO32(CLOCKS_BASE + 0x74)
// o this register is hardwired to 0x1

/* Clock control, can be changed on-the-fly (except for auxsrc) */
#define CLK_REF_CTRL            MMIO32(CLOCKS_BASE + 0x30)
// Selects the auxiliary clock source, will glitch when switching
#define AUXSRC_REF_CLKSRC_PLL_USB   0x00
#define AUXSRC_REF_CLKSRC_GPIN0     0x20
#define AUXSRC_REF_CLKSRC_GPIN1     0x40
// Selects the clock source glitchlessly, can be changed on-the-fly
#define SRC_ROSC_CLKSRC_PH          0x00
#define SRC_CLKSRC_CLK_REF_AUX      0x01
#define SRC_XOSC_CLKSRC             0x02

/* Clock divisor, can be changed on-the-fly */
#define CLK_REF_DIV             MMIO32(CLOCKS_BASE + 0x34)
#define CLK_USB_DIV             MMIO32(CLOCKS_BASE + 0x58)
#define CLK_ADC_DIV             MMIO32(CLOCKS_BASE + 0x64)
// Integer component of the divisor, 0 → divide by 2^16
#define CLK_REF_DIV_INT_SFT 8
#define CLK_REF_DIV_INT_MSK 0x300
#define CLK_REF_DIV_INT0    0x000
#define CLK_REF_DIV_INT1    0x100
#define CLK_REF_DIV_INT2    0x200
#define CLK_REF_DIV_INT3    0x300

/* Indicates which SRC is currently selected by the glitchless mux (one-hot). */
#define CLK_REF_SELECTED        MMIO32(CLOCKS_BASE + 0x38)
#define CLK_SYS_SELECTED        MMIO32(CLOCKS_BASE + 0x44)
// one decoded bit for each of the clock sources enumerated in the CTRL SRC field.

/* Clock control, can be changed on-the-fly (except for auxsrc) */
#define CLK_SYS_CTRL            MMIO32(CLOCKS_BASE + 0x3c)
// Selects the auxiliary clock source, will glitch when switching
#define AUXSRC_SYS_CLKSRC_PLL_SYS   0x00
#define AUXSRC_SYS_CLKSRC_PLL_USB   0x20
#define AUXSRC_SYS_ROSC_CLKSRC      0x40
#define AUXSRC_SYS_XOSC_CLKSRC      0x60
#define AUXSRC_SYS_CLKSRC_GPIN0     0x80
#define AUXSRC_SYS_CLKSRC_GPIN1     0xa0
// Selects the clock source glitchlessly, can be changed on-the-fly
#define SRC_CLK_REF                 0x00
#define SRC_CLKSRC_CLK_SYS_AUX      0x01

/* Clock control, can be changed on-the-fly (except for auxsrc) */
#define CLK_PERI_CTRL           MMIO32(CLOCKS_BASE + 0x48)
// Selects the auxiliary clock source, will glitch when switching
#define AUXSRC_PERI_CLK_SYS         0x000
#define AUXSRC_PERI_CLKSRC_PLL_SYS  0x020
#define AUXSRC_PERI_CLKSRC_PLL_USB  0x040
#define AUXSRC_PERI_ROSC_CLKSRC_PH  0x060
#define AUXSRC_PERI_XOSC_CLKSRC     0x080
#define AUXSRC_PERI_CLKSRC_GPIN0    0x0a0
#define AUXSRC_PERI_CLKSRC_GPIN1    0x0c0

/* Clock control, can be changed on-the-fly (except for auxsrc) */
#define CLK_USB_CTRL            MMIO32(CLOCKS_BASE + 0x54)
// Selects the auxiliary clock source, will glitch when switching
#define AUXSRC_USB_CLKSRC_PLL_USB   0x000
#define AUXSRC_USB_CLKSRC_PLL_SYS   0x020
#define AUXSRC_USB_ROSC_CLKSRC_PH   0x040
#define AUXSRC_USB_XOSC_CLKSRC      0x060
#define AUXSRC_USB_CLKSRC_GPIN0     0x080
#define AUXSRC_USB_CLKSRC_GPIN1     0x0a0

/* Clock control, can be changed on-the-fly (except for auxsrc) */
#define CLK_ADC_CTRL            MMIO32(CLOCKS_BASE + 0x60)
// Selects the auxiliary clock source, will glitch when switching
#define AUXSRC_ADC_CLKSRC_PLL_USB   0x000
#define AUXSRC_ADC_CLKSRC_PLL_SYS   0x020
#define AUXSRC_ADC_ROSC_CLKSRC_PH   0x040
#define AUXSRC_ADC_XOSC_CLKSRC      0x060
#define AUXSRC_ADC_CLKSRC_GPIN0     0x080
#define AUXSRC_ADC_CLKSRC_GPIN1     0x0a0

/* Clock control, can be changed on-the-fly (except for auxsrc) */
#define CLK_RTC_CTRL            MMIO32(CLOCKS_BASE + 0x6c)
// Selects the auxiliary clock source, will glitch when switching
#define AUXSRC_RTC_CLKSRC_PLL_USB   0x000
#define AUXSRC_RTC_CLKSRC_PLL_SYS   0x020
#define AUXSRC_RTC_ROSC_CLKSRC_PH   0x040
#define AUXSRC_RTC_XOSC_CLKSRC      0x060
#define AUXSRC_RTC_CLKSRC_GPIN0     0x080
#define AUXSRC_RTC_CLKSRC_GPIN1     0x0a0

#define CLK_SYS_RESUS_CTRL      MMIO32(CLOCKS_BASE + 0x78)
// For clearing the resus after the fault that triggered it has been corrected
#define CLEAR           0x10000
// Force a resus, for test purposes only
#define FRCE            0x01000
// Enable resus
#define ENABLE_RESUS    0x00100
// This is expressed as a number of clk_ref cycles
#define TIMEOUT_MSK     0x000ff

#define CLK_SYS_RESUS_STATUS    MMIO32(CLOCKS_BASE + 0x7c)
// Clock has been resuscitated, correct the error then send ctrl_clear=1
#define RESUSSED    0x1

/* Reference clock frequency in kHz */
#define FC0_REF_KHZ             MMIO32(CLOCKS_BASE + 0x80)

/* Minimum pass frequency in kHz. */
#define FC0_MIN_KHZ             MMIO32(CLOCKS_BASE + 0x84)
// This is optional. Set to 0 if you are not using the pass/fail flags

/* Maximum pass frequency in kHz. */
#define FC0_MAX_KHZ             MMIO32(CLOCKS_BASE + 0x88)
// This is optional. Set to 0x1ffffff if you are not using the pass/fail flags

/* Delays the start of frequency counting to allow the mux to settle */
#define FC0_DELAY               MMIO32(CLOCKS_BASE + 0x8c)
// Delay is measured in multiples of the reference clock period

/* The test interval is 0.98us * 2**interval, but let’s call it 1us * 2**interval */
#define FC0_INTERVAL            MMIO32(CLOCKS_BASE + 0x90)
// The default gives a test interval of 250us

/* Writing to this register initiates the frequency count */
#define FC0_SRC                 MMIO32(CLOCKS_BASE + 0x94)
#define FC0_SRC_NULL                    0x0
#define FC0_SRC_PLL_SYS_CLKSRC_PRIMARY  0x1
#define FC0_SRC_PLL_USB_CLKSRC_PRIMARY  0x2
#define FC0_SRC_ROSC_CLKSRC             0x3
#define FC0_SRC_ROSC_CLKSRC_PH          0x4
#define FC0_SRC_XOSC_CLKSRC             0x5
#define FC0_SRC_CLKSRC_GPIN0            0x6
#define FC0_SRC_CLKSRC_GPIN1            0x7
#define FC0_SRC_CLK_REF                 0x8
#define FC0_SRC_CLK_SYS                 0x9
#define FC0_SRC_CLK_PERI                0xa
#define FC0_SRC_CLK_USB                 0xb
#define FC0_SRC_CLK_ADC                 0xc
#define FC0_SRC_CLK_RTC                 0xd

/* Frequency counter status */
#define FC0_STATUS              MMIO32(CLOCKS_BASE + 0x98)
// Test clock stopped during test
#define DIED    0x10000000
// Test clock faster than expected, only valid when status_done=1
#define FAST    0x01000000
// Test clock slower than expected, only valid when status_done=1
#define SLOW    0x00100000
// Test failed
#define FAIL    0x00010000
// Waiting for test clock to start
#define WAITING 0x00001000
// Test running
#define RUNNING 0x00000100
// Test complete
#define DONE    0x00000010
// Test passed
#define PASS    0x00000001

/* Result of frequency measurement, only valid when status_done=1 */
#define FC0_RESULT              MMIO32(CLOCKS_BASE + 0x9c)
#define KHZ_FETCH   ((FC0_RESULT>>5)&0x1ffffff)
#define FRAC_FETCH  (FC0_RESULT&0x1f)

/* enable clock in wake mode */
#define WAKE_EN0                MMIO32(CLOCKS_BASE + 0xa0)
#define CLK_SYS_SRAM3               0x80000000
#define CLK_SYS_SRAM2               0x40000000
#define CLK_SYS_SRAM1               0x20000000
#define CLK_SYS_SRAM0               0x10000000
#define CLK_SYS_SPI1                0x08000000
#define CLK_PERI_SPI1               0x04000000
#define CLK_SYS_SPI0                0x02000000
#define CLK_PERI_SPI0               0x01000000
#define CLK_SYS_SIO                 0x00800000
#define CLK_SYS_RTC                 0x00400000
#define CLK_RTC_RTC                 0x00200000
#define CLK_SYS_ROSC                0x00100000
#define CLK_SYS_ROM                 0x00080000
#define CLK_SYS_RESETS              0x00040000
#define CLK_SYS_PWM                 0x00020000
#define CLK_SYS_PSM                 0x00010000
#define CLK_SYS_PLL_USB             0x00008000
#define CLK_SYS_PLL_SYS             0x00004000
#define CLK_SYS_PIO1                0x00002000
#define CLK_SYS_PIO0                0x00001000
#define CLK_SYS_PADS                0x00000800
#define CLK_SYS_VREG_AND_CHIP_RESET 0x00000400
#define CLK_SYS_JTAG                0x00000200
#define CLK_SYS_IO                  0x00000100
#define CLK_SYS_I2C1                0x00000080
#define CLK_SYS_I2C0                0x00000040
#define CLK_SYS_DMA                 0x00000020
#define CLK_SYS_BUSFABRIC           0x00000010
#define CLK_SYS_BUSCTRL             0x00000008
#define CLK_SYS_ADC                 0x00000004
#define CLK_ADC_ADC                 0x00000002
#define CLK_SYS_CLOCKS              0x00000001

/* enable clock in wake mode */
#define WAKE_EN1                MMIO32(CLOCKS_BASE + 0xa4)
#define CLK_SYS_XOSC        0x4000
#define CLK_SYS_XIP         0x2000
#define CLK_SYS_WATCHDOG    0x1000
#define CLK_USB_USBCTRL     0x0800
#define CLK_SYS_USBCTRL     0x0400
#define CLK_SYS_UART1       0x0200
#define CLK_PERI_UART1      0x0100
#define CLK_SYS_UART0       0x0080
#define CLK_PERI_UART0      0x0040
#define CLK_SYS_TIMER       0x0020
#define CLK_SYS_TBMAN       0x0010
#define CLK_SYS_SYSINFO     0x0008
#define CLK_SYS_SYSCFG      0x0004
#define CLK_SYS_SRAM5       0x0002
#define CLK_SYS_SRAM4       0x0001

/* enable clock in sleep mode */
#define SLEEP_EN0               MMIO32(CLOCKS_BASE + 0xa8)
#define SLEEP_EN1               MMIO32(CLOCKS_BASE + 0xac)

/* indicates the state of the clock enable */
#define ENABLED0                MMIO32(CLOCKS_BASE + 0xb0)
#define ENABLED1                MMIO32(CLOCKS_BASE + 0xb4)

/* Raw Interrupts */
#define INTR                    MMIO32(CLOCKS_BASE + 0xb8)
#define CLK_SYS_RESUS   0x1
/* Interrupt Enable */
#define INTE                    MMIO32(CLOCKS_BASE + 0xbc)
/* Interrupt Force */
#define INTF                    MMIO32(CLOCKS_BASE + 0xc0)
/* Interrupt status after masking & forcing */
#define INTS                    MMIO32(CLOCKS_BASE + 0xc4)

#endif
