#ifndef H_TBMAN_REGS
#define H_TBMAN_REGS
/*
 * RP2040 alternative library, testbench manager registers
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Indicates the type of platform in use */
#define PLATFORM    MMIO32(TBMAN_BASE + 0x0)
// Indicates the platform is an FPGA
#define FPGA         0x2
// Indicates the platform is an ASIC
#define ASIC         0x1

#endif
