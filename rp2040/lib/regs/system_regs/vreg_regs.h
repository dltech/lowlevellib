#ifndef H_VREG_REGS
#define H_VREG_REGS
/*
 * RP2040 alternative library, voltage regulator registers
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Voltage regulator control and status */
#define VREG        MMIO32(VREG_AND_CHIP_RESET_BASE + 0x0)
// regulation status, 1=in regulation
#define ROK         0x00001000
// output voltage select
#define VSEL0P8V    0x00000050
#define VSEL0P85V   0x00000060
#define VSEL0P9V    0x00000070
#define VSEL0P95V   0x00000080
#define VSEL1V      0x00000090
#define VSEL1P05V   0x000000a0
#define VSEL1P1V    0x000000b0
#define VSEL1P15V   0x000000c0
#define VSEL1P2V    0x000000d0
#define VSEL1P25V   0x000000e0
#define VSEL1P3V    0x000000f0
// high impedance mode select
#define HIZ         0x00000002
// enable
#define VREG_EN     0x00000001

/* brown-out detection control */
#define BOD         MMIO32(VREG_AND_CHIP_RESET_BASE + 0x4)
// threshold select
#define VSEL0P473V  0x00000000
#define VSEL0P516V  0x00000010
#define VSEL0P559V  0x00000020
#define VSEL0P602V  0x00000030
#define VSEL0P346V  0x00000040
#define VSEL0P688V  0x00000050
#define VSEL0P731V  0x00000060
#define VSEL0P774V  0x00000070
#define VSEL0P817V  0x00000080
#define VSEL0P860V  0x00000090
#define VSEL0P903V  0x000000a0
#define VSEL0P946V  0x000000b0
#define VSEL0P989V  0x000000c0
#define VSEL1P032V  0x000000d0
#define VSEL1P075V  0x000000e0
#define VSEL1P118V  0x000000f0
// enable
#define BOD_EN      0x00000001

/* Chip reset control and status */
#define CHIP_RESET  MMIO32(VREG_AND_CHIP_RESET_BASE + 0x8)
// This is set by psm_restart from the debugger.
#define PSM_RESTART_FLAG    0x01000000
// Last reset was from the debug port
#define HAD_PSM_RESTART     0x00100000
// Last reset was from the RUN pin
#define HAD_RUN             0x00010000
// Last reset was from the power-on reset or brown-out detection blocks
#define HAD_POR             0x00000100

#endif
