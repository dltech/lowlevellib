#ifndef H_CORTEX_REGS
#define H_CORTEX_REGS
/*
 * RP2040 alternative library, Cortex-M0+ core control registers
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* SysTick Control and Status Register */
#define SYST_CSR    MMIO32(PPB_BASE + 0xe010)
// Returns 1 if timer counted to 0 since last time this wasRO0x0 read.
#define COUNTFLAG   0x00010000
// SysTick clock source.
#define CLKSOURCE   0x00000004
// Enables SysTick exception request:
#define TICKINT     0x00000002
// Enable SysTick counter:
#define SYST_ENABLE 0x00000001

/* SysTick Reload Value Register */
#define SYST_RVR    MMIO32(PPB_BASE + 0xe014)
// Value to load into the SysTick Current Value Register when the counter reaches 0.

/* SysTick Current Value Register */
#define SYST_CVR    MMIO32(PPB_BASE + 0xe018)
// Reads return the current value of the SysTick counter.

/* SysTick Calibration Value Register */
#define SYST_CALIB  MMIO32(PPB_BASE + 0xe01c)
// If reads as 1, the Reference clock is not provided
#define NOREF       0x80000000
// If reads as 1, the calibration value for 10ms is inexact
#define SKEW        0x40000000
// An optional Reload value to be used for 10ms (100Hz) timing
#define TENMS_MSK   0x00ffffff

/* Interrupt Set-Enable Register */
#define NVIC_ISER   MMIO32(PPB_BASE + 0xe100)
// Interrupt set-enable bits.

/* Interrupt Clear-Enable Register */
#define NVIC_ICER   MMIO32(PPB_BASE + 0xe180)
// Interrupt clear-enable bits.

/* Interrupt Set-Pending Register */
#define NVIC_ISPR   MMIO32(PPB_BASE + 0xe200)
// Interrupt set-pending bits.

/* Interrupt Clear-Pending Register */
#define NVIC_ICPR   MMIO32(PPB_BASE + 0xe280)
// Interrupt clear-pending bits.

/* Interrupt Priority Register 0 */
#define NVIC_IPR0   MMIO32(PPB_BASE + 0xe400)
// Priority of interrupt 3
#define IP_3_0  0x00000000
#define IP_3_1  0x40000000
#define IP_3_2  0x80000000
#define IP_3_3  0xc0000000
#define IP_3(n) ((n&0x3)<<30)
// Priority of interrupt 2
#define IP_2_0  0x00000000
#define IP_2_1  0x00400000
#define IP_2_2  0x00800000
#define IP_2_3  0x00c00000
#define IP_2(n) ((n&0x3)<<22)
// Priority of interrupt 1
#define IP_1_0  0x00000000
#define IP_1_1  0x00004000
#define IP_1_2  0x00008000
#define IP_1_3  0x0000c000
#define IP_1(n) ((n&0x3)<<14)
// Priority of interrupt 0
#define IP_0_0  0x00000000
#define IP_0_1  0x00000040
#define IP_0_2  0x00000080
#define IP_0_3  0x000000c0
#define IP_0(n) ((n&0x3)<<6)

/* Interrupt Priority Register 1 */
#define NVIC_IPR1   MMIO32(PPB_BASE + 0xe404)
// Priority of interrupt 7
#define IP_7_0  0x00000000
#define IP_7_1  0x40000000
#define IP_7_2  0x80000000
#define IP_7_3  0xc0000000
#define IP_7(n) ((n&0x3)<<30)
// Priority of interrupt 6
#define IP_6_0  0x00000000
#define IP_6_1  0x00400000
#define IP_6_2  0x00800000
#define IP_6_3  0x00c00000
#define IP_6(n) ((n&0x3)<<22)
// Priority of interrupt 5
#define IP_5_0  0x00000000
#define IP_5_1  0x00004000
#define IP_5_2  0x00008000
#define IP_5_3  0x0000c000
#define IP_5(n) ((n&0x3)<<14)
// Priority of interrupt 4
#define IP_4_0  0x00000000
#define IP_4_1  0x00000040
#define IP_4_2  0x00000080
#define IP_4_3  0x000000c0
#define IP_4(n) ((n&0x3)<<6)

/* Interrupt Priority Register 2 */
#define NVIC_IPR2   MMIO32(PPB_BASE + 0xe408)
// Priority of interrupt 11
#define IP_11_0  0x00000000
#define IP_11_1  0x40000000
#define IP_11_2  0x80000000
#define IP_11_3  0xc0000000
#define IP_11(n) ((n&0x3)<<30)
// Priority of interrupt 10
#define IP_10_0  0x00000000
#define IP_10_1  0x00400000
#define IP_10_2  0x00800000
#define IP_10_3  0x00c00000
#define IP_10(n) ((n&0x3)<<22)
// Priority of interrupt 9
#define IP_9_0   0x00000000
#define IP_9_1   0x00004000
#define IP_9_2   0x00008000
#define IP_9_3   0x0000c000
#define IP_9(n)  ((n&0x3)<<14)
// Priority of interrupt 8
#define IP_8_0   0x00000000
#define IP_8_1   0x00000040
#define IP_8_2   0x00000080
#define IP_8_3   0x000000c0
#define IP_8(n)  ((n&0x3)<<6)

/* Interrupt Priority Register 3 */
#define NVIC_IPR3   MMIO32(PPB_BASE + 0xe40c)
// Priority of interrupt 15
#define IP_15_0  0x00000000
#define IP_15_1  0x40000000
#define IP_15_2  0x80000000
#define IP_15_3  0xc0000000
#define IP_15(n) ((n&0x3)<<30)
// Priority of interrupt 14
#define IP_14_0  0x00000000
#define IP_14_1  0x00400000
#define IP_14_2  0x00800000
#define IP_14_3  0x00c00000
#define IP_14(n) ((n&0x3)<<22)
// Priority of interrupt 13
#define IP_13_0  0x00000000
#define IP_13_1  0x00004000
#define IP_13_2  0x00008000
#define IP_13_3  0x0000c000
#define IP_13(n) ((n&0x3)<<14)
// Priority of interrupt 12
#define IP_12_0  0x00000000
#define IP_12_1  0x00000040
#define IP_12_2  0x00000080
#define IP_12_3  0x000000c0
#define IP_12(n) ((n&0x3)<<6)

/* Interrupt Priority Register 4 */
#define NVIC_IPR4   MMIO32(PPB_BASE + 0xe410)
// Priority of interrupt 19
#define IP_19_0  0x00000000
#define IP_19_1  0x40000000
#define IP_19_2  0x80000000
#define IP_19_3  0xc0000000
#define IP_19(n) ((n&0x3)<<30)
// Priority of interrupt 18
#define IP_18_0  0x00000000
#define IP_18_1  0x00400000
#define IP_18_2  0x00800000
#define IP_18_3  0x00c00000
#define IP_18(n) ((n&0x3)<<22)
// Priority of interrupt 17
#define IP_17_0  0x00000000
#define IP_17_1  0x00004000
#define IP_17_2  0x00008000
#define IP_17_3  0x0000c000
#define IP_17(n) ((n&0x3)<<14)
// Priority of interrupt 16
#define IP_16_0  0x00000000
#define IP_16_1  0x00000040
#define IP_16_2  0x00000080
#define IP_16_3  0x000000c0
#define IP_16(n) ((n&0x3)<<6)

/* Interrupt Priority Register 5 */
#define NVIC_IPR5   MMIO32(PPB_BASE + 0xe414)
// Priority of interrupt 23
#define IP_23_0  0x00000000
#define IP_23_1  0x40000000
#define IP_23_2  0x80000000
#define IP_23_3  0xc0000000
#define IP_23(n) ((n&0x3)<<30)
// Priority of interrupt 22
#define IP_22_0  0x00000000
#define IP_22_1  0x00400000
#define IP_22_2  0x00800000
#define IP_22_3  0x00c00000
#define IP_22(n) ((n&0x3)<<22)
// Priority of interrupt 21
#define IP_21_0  0x00000000
#define IP_21_1  0x00004000
#define IP_21_2  0x00008000
#define IP_21_3  0x0000c000
#define IP_21(n) ((n&0x3)<<14)
// Priority of interrupt 20
#define IP_20_0  0x00000000
#define IP_20_1  0x00000040
#define IP_20_2  0x00000080
#define IP_20_3  0x000000c0
#define IP_20(n) ((n&0x3)<<6)

/* Interrupt Priority Register 6 */
#define NVIC_IPR6   MMIO32(PPB_BASE + 0xe418)
// Priority of interrupt 27
#define IP_27_0  0x00000000
#define IP_27_1  0x40000000
#define IP_27_2  0x80000000
#define IP_27_3  0xc0000000
#define IP_27(n) ((n&0x3)<<30)
// Priority of interrupt 26
#define IP_26_0  0x00000000
#define IP_26_1  0x00400000
#define IP_26_2  0x00800000
#define IP_26_3  0x00c00000
#define IP_26(n) ((n&0x3)<<22)
// Priority of interrupt 25
#define IP_25_0  0x00000000
#define IP_25_1  0x00004000
#define IP_25_2  0x00008000
#define IP_25_3  0x0000c000
#define IP_25(n) ((n&0x3)<<14)
// Priority of interrupt 24
#define IP_24_0  0x00000000
#define IP_24_1  0x00000040
#define IP_24_2  0x00000080
#define IP_24_3  0x000000c0
#define IP_24(n) ((n&0x3)<<6)

/* Interrupt Priority Register 7 */
#define NVIC_IPR7   MMIO32(PPB_BASE + 0xe41c)
// Priority of interrupt 31
#define IP_31_0  0x00000000
#define IP_31_1  0x40000000
#define IP_31_2  0x80000000
#define IP_31_3  0xc0000000
#define IP_31(n) ((n&0x3)<<30)
// Priority of interrupt 30
#define IP_30_0  0x00000000
#define IP_30_1  0x00400000
#define IP_30_2  0x00800000
#define IP_30_3  0x00c00000
#define IP_30(n) ((n&0x3)<<22)
// Priority of interrupt 29
#define IP_29_0  0x00000000
#define IP_29_1  0x00004000
#define IP_29_2  0x00008000
#define IP_29_3  0x0000c000
#define IP_29(n) ((n&0x3)<<14)
// Priority of interrupt 28
#define IP_28_0  0x00000000
#define IP_28_1  0x00000040
#define IP_28_2  0x00000080
#define IP_28_3  0x000000c0
#define IP_28(n) ((n&0x3)<<6)

/* CPUID Base Register */
#define CPUID       MMIO32(PPB_BASE + 0xed00)
// Implementor code: 0x41 = ARM
#define IMPLEMENTER_FETCH(reg)  ((reg>>24)&0xff)
// Major revision number n in the rnpm revision status:
#define VARIANT_FETCH(reg)      ((reg>>20)&0x0f)
// Number of processor within family: 0xC60 = Cortex-M0+
#define PARTNO_FETCH(reg)       ((reg>>4)&0xfff)
// Minor revision number m in the rnpm revision status:
#define REVISION_FETCH(reg)     (reg&0xf)

/* Interrupt Control and State Register */
#define ICSR        MMIO32(PPB_BASE + 0xed04)
// Setting this bit will activate an NMI.
#define NMIPENDSET  0x80000000
// PendSV set-pending bit.
#define PENDSVSET   0x10000000
// PendSV clear-pending bit.
#define PENDSVCLR   0x08000000
// SysTick exception set-pending bit.
#define PENDSTSET   0x04000000
// SysTick exception clear-pending bit.
#define PENDSTCLR   0x02000000
// indicates that a pending interrupt is to be taken in the next running cycle.
#define ISRPREEMPT  0x00800000
// External interrupt pending flag
#define ISRPENDING  0x00400000
// Indicates the exception number for the highest priority
#define VECTPENDING_FETCH(reg)   ((reg>>12)&0x1ff)
// Active exception number field.
#define VECTACTIVE_FETCH(reg)    (reg&0x1ff)

/* Vector Table Offset Register */
#define VTOR        MMIO32(PPB_BASE + 0xed08)
// Bits [31:8] of the indicate the vector table offset address.
#define TBLOFF_SFT  8

/* Application Interrupt and Reset Control Register */
#define AIRCR       MMIO32(PPB_BASE + 0xed0c)
// Register key:
#define VECTKEY_SFT     16
#define VECTKEY_MSK     0xffff0000
#define VECTKEY_FETCH(reg)  ((reg>>VECTKEY_SFT)&0xffff)
// Data endianness implemented:
#define ENDIANESS       0x00008000
// SYSRESETREQ signal to the outer system to be asserted to request a reset.
#define SYSRESETREQ     0x00000004
// Clears all active state information for fixed and configurable exceptions.
#define VECTCLRACTIVE   0x00000002

/* System Control Register */
#define SCR         MMIO32(PPB_BASE + 0xed10)
// Send Event on Pending bit:
#define SEVONPEND   0x00000010
// Controls whether the processor uses sleep or deep sleep as its low power mode:
#define SLEEPDEEP   0x00000004
// Indicates sleep-on-exit when returning from Handler mode to Thread mode:
#define SLEEPONEXIT 0x00000002

/* Configuration and Control Register */
#define CCR         MMIO32(PPB_BASE + 0xed14)
// indicates 8-byte stack alignment on exception entry
#define STKALIGN    0x00010000
// indicates that all unaligned accesses generate a HardFault.
#define UNALIGN_TRP 0x00000008

/* System Handler Priority Register 2 */
#define SHPR2       MMIO32(PPB_BASE + 0xed1c)
// Priority of system handler 11, SVCall
#define PRI_11_0  0x00000000
#define PRI_11_1  0x40000000
#define PRI_11_2  0x80000000
#define PRI_11_3  0xc0000000
#define PRI_11(n) ((n&0x3)<<30)

/* System Handler Priority Register 3 */
#define SHPR3       MMIO32(PPB_BASE + 0xed20)
// Priority of system handler 15, SysTick
#define PRI_15_0  0x00000000
#define PRI_15_1  0x40000000
#define PRI_15_2  0x80000000
#define PRI_15_3  0xc0000000
#define PRI_15(n) ((n&0x3)<<30)
// Priority of system handler 14, PendSV
#define PRI_14_0  0x00000000
#define PRI_14_1  0x00400000
#define PRI_14_2  0x00800000
#define PRI_14_3  0x00c00000
#define PRI_14(n) ((n&0x3)<<22)

/* System Handler Control and State Register */
#define SHCSR       MMIO32(PPB_BASE + 0xed24)
// Reads as 1 if SVCall is Pending. Write 1 to set pending SVCall,
#define SVCALLPENDED    0x00008000

/* MPU Type Register */
#define MPU_TYPE    MMIO32(PPB_BASE + 0xed90)
// Reads as zero as ARMv6-M only supports a unified MPU
#define IREGION_FETCH(reg)  ((reg>>23)&0xff)
// Number of regions supported by the MPU.
#define DREGION_FETCH(reg)  ((reg>>8)&0xff)
// Indicates support for separate instruction and data address maps.
#define SEPARATE            0x00000001

/* MPU Control Register */
#define MPU_CTRL    MMIO32(PPB_BASE + 0xed94)
// Ctrls whether the def memory map is en as a backgr rgn for privileged accesses.
#define PRIVDEFENA  0x00000004
// Controls the use of the MPU for HardFaults and NMIs.
#define HFNMIENA    0x00000002
// Enables the MPU.
#define MPU_ENABLE  0x00000001

/* MPU Region Number Register */
#define MPU_RNR     MMIO32(PPB_BASE + 0xed98)
// Indicates the MPU region referenced by the MPU_RBAR and MPU_RASR registers.
#define REGION_MSK          0xf
#define REGION_FETCH(reg)   (reg&0xf)

/* MPU Region Base Address Register */
#define MPU_RBAR    MMIO32(PPB_BASE + 0xed9c)
// Base address of the region.
#define ADDR_SFT          8
#define ADDR_MSK          0xffffff00
#define ADDR_FETCH(reg)   ((reg>>ADDR_SFT)&0xf)
// write must update the base address of the region identified by the REGION field
#define VALID
// spec the number of the region whose base addr to upd prov VALID is set wr as 1

/* MPU Region Attribute and Size Register */
#define MPU_RASR    MMIO32(PPB_BASE + 0xeda0)
// The MPU Region Attribute field. Use to define the region attribute control.
#define ATTRS_SFT           16
#define ATTRS_MSK           0xffff0000
#define ATTRS_FETCH(reg)    ((reg>>ATTRS_SFT)&0xffff)
// Subregion Disable
#define SRD_SFT             8
#define SRD_MSK             0x0000ff00
#define SRD_FETCH(reg)      ((reg>>SRD_SFT)&0xff)
// Indicates the region size.
#define SIZE_SFT            1
#define SIZE_MSK            0x0000003e
#define SIZE_FETCH(reg)     ((reg>>SIZE_SFT)&0x1f)
// Enables the region.
#define REG_ENABLE          0x00000001

#endif
