#ifndef H_RESET_REGS
#define H_RESET_REGS
/*
 * RP2040 alternative library, Subsystem Reset registers
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Reset control. */
#define RESET       MMIO32(RESETS_BASE + 0x0)
#define USBCTRL_RES     0x1000000
#define UART1_RES       0x0800000
#define UART0_RES       0x0400000
#define TIMER_RES       0x0200000
#define TBMAN_RES       0x0100000
#define SYSINFO_RES     0x0080000
#define SYSCFG_RES      0x0040000
#define SPI1_RES        0x0020000
#define SPI0_RES        0x0010000
#define RTC_RES         0x0008000
#define PWM_RES         0x0004000
#define PLL_USB_RES     0x0002000
#define PLL_SYS_RES     0x0001000
#define PIO1_RES        0x0000800
#define PIO0_RES        0x0000400
#define PADS_QSPI_RES   0x0000200
#define PADS_BANK0_RES  0x0000100
#define JTAG_RES        0x0000080
#define IO_QSPI_RES     0x0000040
#define IO_BANK0_RES    0x0000020
#define I2C1_RES        0x0000010
#define I2C0_RES        0x0000008
#define DMA_RES         0x0000004
#define BUSCTRL_RES     0x0000002
#define ADC_RES         0x0000001

/* Watchdog select. */
#define WDSEL       MMIO32(RESETS_BASE + 0x4)

/* Reset done. */
#define RESET_DONE  MMIO32(RESETS_BASE + 0x8)

#endif
