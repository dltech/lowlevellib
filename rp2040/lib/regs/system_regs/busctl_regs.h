#ifndef H_BUSCTRL
#define H_BUSCTRL
/*
 * RP2040 alternative library, bus control registers
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Set the priority of each master for bus arbitration. */
#define BUS_PRIORITY        MMIO32(BUSCTRL_BASE + 0x00)
// 0 - low priority, 1 - high priority
#define DMA_W   0x00001000
// 0 - low priority, 1 - high priority
#define DMA_R   0x00000100
// 0 - low priority, 1 - high priority
#define PROC1   0x00000010
// 0 - low priority, 1 - high priority
#define PROC0   0x00000001

/* Bus priority acknowledge */
#define BUS_PRIORITY_ACK    MMIO32(BUSCTRL_BASE + 0x04)
// Goes to 1 once all arbiters have registered the new global priority levels.
#define RO      0x00000001

/* Bus fabric performance counter 0 */
#define PERFCTR0            MMIO32(BUSCTRL_BASE + 0x08)
// Busfabric saturating performance counter 0
#define PERFCTR_MSK 0x00ffffff

/* Bus fabric performance event select for PERFCTR0 */
#define PERFSEL0            MMIO32(BUSCTRL_BASE + 0x0c)
// Select an event for PERFCTR0.
#define apb_contested       0x00
#define apb                 0x01
#define fastperi_contested  0x02
#define fastperi            0x03
#define sram5_contested     0x04
#define sram5               0x05
#define sram4_contested     0x06
#define sram4               0x07
#define sram3_contested     0x08
#define sram3               0x09
#define sram2_contested     0x0a
#define sram2               0x0b
#define sram1_contested     0x0c
#define sram1               0x0d
#define sram0_contested     0x0e
#define sram0               0x0f
#define xip_main_contested  0x10
#define xip_main            0x11
#define rom_contested       0x12
#define rom                 0x13

/* Bus fabric performance counter 1 */
#define PERFCTR1            MMIO32(BUSCTRL_BASE + 0x10)
//Busfabric saturating performance counter 1
/* Bus fabric performance event select for PERFCTR1 */
#define PERFSEL1            MMIO32(BUSCTRL_BASE + 0x14)
// Select an event for PERFCTR1.

/* Bus fabric performance counter 2 */
#define PERFCTR2            MMIO32(BUSCTRL_BASE + 0x18)
/* Bus fabric performance event select for PERFCTR2 */
#define PERFSEL2            MMIO32(BUSCTRL_BASE + 0x1c)

/* Bus fabric performance counter 3 */
#define PERFCTR3            MMIO32(BUSCTRL_BASE + 0x20)
/* Bus fabric performance event select for PERFCTR3 */
#define PERFSEL3            MMIO32(BUSCTRL_BASE + 0x24)


#endif
