#ifndef H_WDT_REGS
#define H_WDT_REGS
/*
 * RP2040 alternative library, watchdog registers
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "system_regs/memorymap.h"

/* Watchdog control */
#define CTRL        MMIO32(WATCHDOG_BASE + 0x00)
// Trigger a watchdog reset
#define TRIGGER     0x80000000
// When not enabled the watchdog timer is paused
#define WDT_ENABLE  0x40000000
// Pause the watchdog timer when processor 1 is in debug mode
#define PAUSE_DBG1  0x04000000
// Pause the watchdog timer when processor 0 is in debug mode
#define PAUSE_DBG0  0x02000000
// Pause the watchdog timer when JTAG is accessing the bus fabric
#define PAUSE_JTAG  0x01000000
// Indicates the number of ticks / 2 before a watchdog reset will be triggered
#define TIME_MSK    0x00ffffff
#define TIME_SET(n) (n&TIME_MSK)

/* Load the watchdog timer. */
#define LOAD        MMIO32(WATCHDOG_BASE + 0x04)
// The maximum setting is 0xffffff which corresponds to 0xffffff / 2 ticks
#define LOAD_MSK    0xffffff

/* Logs the reason for the last reset. */
#define REASON      MMIO32(WATCHDOG_BASE + 0x08)
#define FORCE   0x2
#define TIMER   0x1

/* Scratch register */
#define SCRATCH0    MMIO32(WATCHDOG_BASE + 0x0c)
#define SCRATCH1    MMIO32(WATCHDOG_BASE + 0x10)
#define SCRATCH2    MMIO32(WATCHDOG_BASE + 0x14)
#define SCRATCH3    MMIO32(WATCHDOG_BASE + 0x18)
#define SCRATCH4    MMIO32(WATCHDOG_BASE + 0x1c)
#define SCRATCH5    MMIO32(WATCHDOG_BASE + 0x20)
#define SCRATCH6    MMIO32(WATCHDOG_BASE + 0x24)
#define SCRATCH7    MMIO32(WATCHDOG_BASE + 0x28)
// Information persists through soft reset of the chip.

/* Controls the tick generator */
#define TICK        MMIO32(WATCHDOG_BASE + 0x2c)
// remaining number clk_tick cycles before the next tick is generated.
#define COUNT_MSK   0xff800
#define COUNT_SFT   11
#define COUNT_FETCH ((TICK>>COUNT_SFT)&0x1ff)
// Is the tick generator running?
#define RUNNING     0x00400
// start / stop tick generation
#define TICK_ENABLE 0x00200
// Total number of clk_tick cycles before the next tick.
#define CYCLES_MSK  0x0001f
#define CYCLES_FETCH (TICK&CYCLES_MSK)

#endif
