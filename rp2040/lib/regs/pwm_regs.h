#ifndef H_PWM_REGS
#define H_PWM_REGS
/*
 * RP2040 alternative library, Pulse width modulation (PWM) registers
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "system_regs/memorymap.h"

/* Control and status register */
#define CH0_CSR     MMIO32(PWM_BASE + 0x00)
#define CH1_CSR     MMIO32(PWM_BASE + 0x14)
#define CH2_CSR     MMIO32(PWM_BASE + 0x28)
#define CH3_CSR     MMIO32(PWM_BASE + 0x3c)
#define CH4_CSR     MMIO32(PWM_BASE + 0x50)
#define CH5_CSR     MMIO32(PWM_BASE + 0x64)
#define CH6_CSR     MMIO32(PWM_BASE + 0x78)
#define CH7_CSR     MMIO32(PWM_BASE + 0x8c)
// Advance the phase of the counter by 1 count, while it is running.
#define PH_ADV              0x80
// Retard the phase of the counter by 1 count, while it is running.
#define PH_RET              0x40
//
#define DIVMODE_FRACDIV     0x00
#define DIVMODE_PWMB        0x10
#define DIVMODE_PWMB_RISE   0x20
#define DIVMODE_PWMB_FALL   0x30
// Invert output B
#define B_INV               0x08
// Invert output A
#define A_INV               0x04
// 1: Enable phase-correct modulation. 0: Trailing-edge
#define PH_CORRECT          0x02
// Enable the PWM channel.
#define PWM_CH_EN           0x01

/* INT and FRAC form a fixed-point fractional number. */
#define CH0_DIV     MMIO32(PWM_BASE + 0x04)
#define CH1_DIV     MMIO32(PWM_BASE + 0x18)
#define CH2_DIV     MMIO32(PWM_BASE + 0x2c)
#define CH3_DIV     MMIO32(PWM_BASE + 0x40)
#define CH4_DIV     MMIO32(PWM_BASE + 0x54)
#define CH5_DIV     MMIO32(PWM_BASE + 0x68)
#define CH6_DIV     MMIO32(PWM_BASE + 0x7c)
#define CH7_DIV     MMIO32(PWM_BASE + 0x90)
#define PWM_INT_MSK     0xff0
#define PWM_INT_SFT     4
#define PWM_INT_SET(n)  ((n<<PWM_INT_SFT)&PWM_INT_MSK)
#define PWM_FRAC_MSK    0x00f
#define PWM_FRAC_SET(n) (n&PWM_FRAC_MSK)

/* Direct access to the PWM counter */
#define CH0_CTR     MMIO32(PWM_BASE + 0x08)
#define CH1_CTR     MMIO32(PWM_BASE + 0x1c)
#define CH2_CTR     MMIO32(PWM_BASE + 0x30)
#define CH3_CTR     MMIO32(PWM_BASE + 0x44)
#define CH4_CTR     MMIO32(PWM_BASE + 0x58)
#define CH5_CTR     MMIO32(PWM_BASE + 0x6c)
#define CH6_CTR     MMIO32(PWM_BASE + 0x80)
#define CH7_CTR     MMIO32(PWM_BASE + 0x94)
// Direct access to the PWM counter

/* Counter compare values */
#define CH0_CC      MMIO32(PWM_BASE + 0x0c)
#define CH1_CC      MMIO32(PWM_BASE + 0x20)
#define CH2_CC      MMIO32(PWM_BASE + 0x34)
#define CH3_CC      MMIO32(PWM_BASE + 0x48)
#define CH4_CC      MMIO32(PWM_BASE + 0x5c)
#define CH5_CC      MMIO32(PWM_BASE + 0x70)
#define CH6_CC      MMIO32(PWM_BASE + 0x84)
#define CH7_CC      MMIO32(PWM_BASE + 0x98)
#define B_MSK     0xffff0000
#define B_SFT     4
#define B_SET(n)  ((n<<B_SFT)&B_MSK)
#define A_MSK    0x0000fffff
#define A_SET(n) (n&A_MSK)

/* Counter compare values */
#define CH0_TOP     MMIO32(PWM_BASE + 0x10)
#define CH1_TOP     MMIO32(PWM_BASE + 0x24)
#define CH2_TOP     MMIO32(PWM_BASE + 0x38)
#define CH3_TOP     MMIO32(PWM_BASE + 0x4c)
#define CH4_TOP     MMIO32(PWM_BASE + 0x60)
#define CH5_TOP     MMIO32(PWM_BASE + 0x74)
#define CH6_TOP     MMIO32(PWM_BASE + 0x88)
#define CH7_TOP     MMIO32(PWM_BASE + 0x9c)
// [15:0]

/* This register aliases the CSR_EN bits for all channels. */
#define PWM_EN      MMIO32(PWM_BASE + 0xa0)
// Writing to this register allows multiple channels to be en or dis simultaneously
#define CH7 0x80
#define CH6 0x40
#define CH5 0x20
#define CH4 0x10
#define CH3 0x08
#define CH2 0x04
#define CH1 0x02
#define CH0 0x01

/* Raw Interrupts */
#define PWM_INTR    MMIO32(PWM_BASE + 0xa4)

/* Interrupt Enable */
#define PWM_INTE    MMIO32(PWM_BASE + 0xa8)

/* Interrupt Force */
#define PWM_INTF    MMIO32(PWM_BASE + 0xac)

/* Interrupt status after masking & forcing */
#define PWM_INTS    MMIO32(PWM_BASE + 0xb0)

#endif
