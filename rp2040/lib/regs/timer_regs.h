#ifndef H_TIMER_REGS
#define H_TIMER_REGS
/*
 * RP2040 alternative library, system timer peripheral registers
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "system_regs/memorymap.h"

/* Time write */
#define TIMEHW      MMIO32(TIMER_BASE + 0x00)
// Write to bits 63:32 of time always write timelw before timehw
#define TIMELW      MMIO32(TIMER_BASE + 0x04)
// Write to bits 31:0 of time writes do not get copi to time until timehw is written

/* Time read */
#define TIMEHR      MMIO32(TIMER_BASE + 0x08)
// Read from bits 63:32 of time always read timelr before timehr
#define TIMELR      MMIO32(TIMER_BASE + 0x0c)
// Read from bits 31:0 of time

/* Arm alarm n */
#define ALARM0      MMIO32(TIMER_BASE + 0x10)
#define ALARM1      MMIO32(TIMER_BASE + 0x14)
#define ALARM2      MMIO32(TIMER_BASE + 0x18)
#define ALARM3      MMIO32(TIMER_BASE + 0x1c)
// Arm alarm n, and configure the time it will fire.

/* Alarm status */
#define ARMED       MMIO32(TIMER_BASE + 0x20)
// Indicates the armed/disarmed status of each alarm.
#define ARMED3  0x8
#define ARMED2  0x4
#define ARMED1  0x2
#define ARMED0  0x1

/* Raw time */
#define TIMERAWH    MMIO32(TIMER_BASE + 0x24)
// Raw read from bits 63:32 of time (no side effects)
#define TIMERAWL    MMIO32(TIMER_BASE + 0x28)
// Raw read from bits 31:0 of time (no side effects)

/* Set bits high to enable pause when the corresponding debug ports are active */
#define DBGPAUSE    MMIO32(TIMER_BASE + 0x2c)
// Pause when processor 1 is in debug mode
#define DBG1    0x4
// Pause when processor 0 is in debug mode
#define DBG0    0x2

/* PAUSE Register */
#define PAUSE       MMIO32(TIMER_BASE + 0x30)
// Set high to pause the timer
#define PAUSE_EN    0x1

/* Raw Interrupts */
#define TIM_INTR    MMIO32(TIMER_BASE + 0x34)
#define ALARM_3 0x8
#define ALARM_2 0x4
#define ALARM_1 0x2
#define ALARM_0 0x1

/* Interrupt Enable */
#define TIM_INTE    MMIO32(TIMER_BASE + 0x38)

/* Interrupt Force */
#define TIM_INTF    MMIO32(TIMER_BASE + 0x3c)

/* Interrupt status after masking & forcing */
#define TIM_INTS    MMIO32(TIMER_BASE + 0x40)

#endif
