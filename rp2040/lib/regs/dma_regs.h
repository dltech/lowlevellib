#ifndef H_DMA_REGS
#define H_DMA_REGS
/*
 * RP2040 alternative library, Direct Memory Access (DMA) controller
 * control registers
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* DMA Channel n Read Address pointer */
#define CH0_READ_ADDR               MMIO32(DMA_BASE + 0x000)
#define CH1_READ_ADDR               MMIO32(DMA_BASE + 0x040)
#define CH2_READ_ADDR               MMIO32(DMA_BASE + 0x080)
#define CH3_READ_ADDR               MMIO32(DMA_BASE + 0x0c0)
#define CH4_READ_ADDR               MMIO32(DMA_BASE + 0x100)
#define CH5_READ_ADDR               MMIO32(DMA_BASE + 0x140)
#define CH6_READ_ADDR               MMIO32(DMA_BASE + 0x180)
#define CH7_READ_ADDR               MMIO32(DMA_BASE + 0x1c0)
#define CH8_READ_ADDR               MMIO32(DMA_BASE + 0x200)
#define CH9_READ_ADDR               MMIO32(DMA_BASE + 0x240)
#define CH10_READ_ADDR              MMIO32(DMA_BASE + 0x280)
#define CH11_READ_ADDR              MMIO32(DMA_BASE + 0x2c0)
// The current value is the next address to be read by this channel.
/* Aliases for channel n READ_ADDR register */
#define CH0_AL1_READ_ADDR           MMIO32(DMA_BASE + 0x014)
#define CH0_AL2_READ_ADDR           MMIO32(DMA_BASE + 0x028)
#define CH1_AL1_READ_ADDR           MMIO32(DMA_BASE + 0x054)
#define CH1_AL2_READ_ADDR           MMIO32(DMA_BASE + 0x068)
#define CH2_AL1_READ_ADDR           MMIO32(DMA_BASE + 0x094)
#define CH2_AL2_READ_ADDR           MMIO32(DMA_BASE + 0x0a8)
#define CH3_AL1_READ_ADDR           MMIO32(DMA_BASE + 0x0d4)
#define CH3_AL2_READ_ADDR           MMIO32(DMA_BASE + 0x0e8)
#define CH4_AL1_READ_ADDR           MMIO32(DMA_BASE + 0x114)
#define CH4_AL2_READ_ADDR           MMIO32(DMA_BASE + 0x128)
#define CH5_AL1_READ_ADDR           MMIO32(DMA_BASE + 0x154)
#define CH5_AL2_READ_ADDR           MMIO32(DMA_BASE + 0x168)
#define CH6_AL1_READ_ADDR           MMIO32(DMA_BASE + 0x194)
#define CH6_AL2_READ_ADDR           MMIO32(DMA_BASE + 0x1a8)
#define CH7_AL1_READ_ADDR           MMIO32(DMA_BASE + 0x1d4)
#define CH7_AL2_READ_ADDR           MMIO32(DMA_BASE + 0x1e8)
#define CH8_AL1_READ_ADDR           MMIO32(DMA_BASE + 0x214)
#define CH8_AL2_READ_ADDR           MMIO32(DMA_BASE + 0x228)
#define CH9_AL1_READ_ADDR           MMIO32(DMA_BASE + 0x254)
#define CH9_AL2_READ_ADDR           MMIO32(DMA_BASE + 0x268)
#define CH10_AL1_READ_ADDR          MMIO32(DMA_BASE + 0x294)
#define CH10_AL2_READ_ADDR          MMIO32(DMA_BASE + 0x2a8)
#define CH11_AL1_READ_ADDR          MMIO32(DMA_BASE + 0x2d4)
#define CH11_AL2_READ_ADDR          MMIO32(DMA_BASE + 0x2e8)
// Writing a nonzero value will reload the channel counter and start the channel.
#define CH0_AL3_READ_ADDR_TRIG      MMIO32(DMA_BASE + 0x03c)
#define CH1_AL3_READ_ADDR_TRIG      MMIO32(DMA_BASE + 0x07c)
#define CH2_AL3_READ_ADDR_TRIG      MMIO32(DMA_BASE + 0x0bc)
#define CH3_AL3_READ_ADDR_TRIG      MMIO32(DMA_BASE + 0x0fc)
#define CH4_AL3_READ_ADDR_TRIG      MMIO32(DMA_BASE + 0x13c)
#define CH5_AL3_READ_ADDR_TRIG      MMIO32(DMA_BASE + 0x17c)
#define CH6_AL3_READ_ADDR_TRIG      MMIO32(DMA_BASE + 0x1bc)
#define CH7_AL3_READ_ADDR_TRIG      MMIO32(DMA_BASE + 0x1fc)
#define CH8_AL3_READ_ADDR_TRIG      MMIO32(DMA_BASE + 0x23c)
#define CH9_AL3_READ_ADDR_TRIG      MMIO32(DMA_BASE + 0x27c)
#define CH10_AL3_READ_ADDR_TRIG     MMIO32(DMA_BASE + 0x2bc)
#define CH11_AL3_READ_ADDR_TRIG     MMIO32(DMA_BASE + 0x2fc)

/* DMA Channel n Write Address pointer */
#define CH0_WRITE_ADDR              MMIO32(DMA_BASE + 0x004)
#define CH1_WRITE_ADDR              MMIO32(DMA_BASE + 0x044)
#define CH2_WRITE_ADDR              MMIO32(DMA_BASE + 0x084)
#define CH3_WRITE_ADDR              MMIO32(DMA_BASE + 0x0c4)
#define CH4_WRITE_ADDR              MMIO32(DMA_BASE + 0x104)
#define CH5_WRITE_ADDR              MMIO32(DMA_BASE + 0x144)
#define CH6_WRITE_ADDR              MMIO32(DMA_BASE + 0x184)
#define CH7_WRITE_ADDR              MMIO32(DMA_BASE + 0x1c4)
#define CH8_WRITE_ADDR              MMIO32(DMA_BASE + 0x204)
#define CH9_WRITE_ADDR              MMIO32(DMA_BASE + 0x244)
#define CH10_WRITE_ADDR             MMIO32(DMA_BASE + 0x284)
#define CH11_WRITE_ADDR             MMIO32(DMA_BASE + 0x2c4)
// The current value is the next address to be written by this channel.
/* Aliases for channel n WRITE_ADDR register */
#define CH0_AL1_WRITE_ADDR          MMIO32(DMA_BASE + 0x018)
#define CH0_AL3_WRITE_ADDR          MMIO32(DMA_BASE + 0x034)
#define CH1_AL1_WRITE_ADDR          MMIO32(DMA_BASE + 0x058)
#define CH1_AL3_WRITE_ADDR          MMIO32(DMA_BASE + 0x074)
#define CH2_AL1_WRITE_ADDR          MMIO32(DMA_BASE + 0x098)
#define CH2_AL3_WRITE_ADDR          MMIO32(DMA_BASE + 0x0b4)
#define CH3_AL1_WRITE_ADDR          MMIO32(DMA_BASE + 0x0d8)
#define CH3_AL3_WRITE_ADDR          MMIO32(DMA_BASE + 0x0f4)
#define CH4_AL1_WRITE_ADDR          MMIO32(DMA_BASE + 0x118)
#define CH4_AL3_WRITE_ADDR          MMIO32(DMA_BASE + 0x134)
#define CH5_AL1_WRITE_ADDR          MMIO32(DMA_BASE + 0x158)
#define CH5_AL3_WRITE_ADDR          MMIO32(DMA_BASE + 0x174)
#define CH6_AL1_WRITE_ADDR          MMIO32(DMA_BASE + 0x198)
#define CH6_AL3_WRITE_ADDR          MMIO32(DMA_BASE + 0x1b4)
#define CH7_AL1_WRITE_ADDR          MMIO32(DMA_BASE + 0x1d8)
#define CH7_AL3_WRITE_ADDR          MMIO32(DMA_BASE + 0x1f4)
#define CH8_AL1_WRITE_ADDR          MMIO32(DMA_BASE + 0x218)
#define CH8_AL3_WRITE_ADDR          MMIO32(DMA_BASE + 0x234)
#define CH9_AL1_WRITE_ADDR          MMIO32(DMA_BASE + 0x258)
#define CH9_AL3_WRITE_ADDR          MMIO32(DMA_BASE + 0x274)
#define CH10_AL1_WRITE_ADDR         MMIO32(DMA_BASE + 0x298)
#define CH10_AL3_WRITE_ADDR         MMIO32(DMA_BASE + 0x2b4)
#define CH11_AL1_WRITE_ADDR         MMIO32(DMA_BASE + 0x2d8)
#define CH11_AL3_WRITE_ADDR         MMIO32(DMA_BASE + 0x2f4)
// Writing a nonzero value will reload the channel counter and start the channel.
#define CH0_AL2_WRITE_ADDR_TRIG     MMIO32(DMA_BASE + 0x02c)
#define CH1_AL2_WRITE_ADDR_TRIG     MMIO32(DMA_BASE + 0x06c)
#define CH2_AL2_WRITE_ADDR_TRIG     MMIO32(DMA_BASE + 0x0ac)
#define CH3_AL2_WRITE_ADDR_TRIG     MMIO32(DMA_BASE + 0x0ec)
#define CH4_AL2_WRITE_ADDR_TRIG     MMIO32(DMA_BASE + 0x12c)
#define CH5_AL2_WRITE_ADDR_TRIG     MMIO32(DMA_BASE + 0x16c)
#define CH6_AL2_WRITE_ADDR_TRIG     MMIO32(DMA_BASE + 0x1ac)
#define CH7_AL2_WRITE_ADDR_TRIG     MMIO32(DMA_BASE + 0x1ec)
#define CH8_AL2_WRITE_ADDR_TRIG     MMIO32(DMA_BASE + 0x22c)
#define CH9_AL2_WRITE_ADDR_TRIG     MMIO32(DMA_BASE + 0x26c)
#define CH10_AL2_WRITE_ADDR_TRIG    MMIO32(DMA_BASE + 0x2ac)
#define CH11_AL2_WRITE_ADDR_TRIG    MMIO32(DMA_BASE + 0x2ec)

/* DMA Channel n Transfer Count */
#define CH0_TRANS_COUNT             MMIO32(DMA_BASE + 0x008)
#define CH1_TRANS_COUNT             MMIO32(DMA_BASE + 0x048)
#define CH2_TRANS_COUNT             MMIO32(DMA_BASE + 0x088)
#define CH3_TRANS_COUNT             MMIO32(DMA_BASE + 0x0c8)
#define CH4_TRANS_COUNT             MMIO32(DMA_BASE + 0x108)
#define CH5_TRANS_COUNT             MMIO32(DMA_BASE + 0x148)
#define CH6_TRANS_COUNT             MMIO32(DMA_BASE + 0x188)
#define CH7_TRANS_COUNT             MMIO32(DMA_BASE + 0x1c8)
#define CH8_TRANS_COUNT             MMIO32(DMA_BASE + 0x208)
#define CH9_TRANS_COUNT             MMIO32(DMA_BASE + 0x248)
#define CH10_TRANS_COUNT            MMIO32(DMA_BASE + 0x288)
#define CH11_TRANS_COUNT            MMIO32(DMA_BASE + 0x2c8)
// Program the number of bus transfers a channel will perform before halting.
/* Aliases for channel n TRANS_COUNT register */
#define CH0_AL2_TRANS_COUNT         MMIO32(DMA_BASE + 0x024)
#define CH0_AL3_TRANS_COUNT         MMIO32(DMA_BASE + 0x038)
#define CH1_AL2_TRANS_COUNT         MMIO32(DMA_BASE + 0x064)
#define CH1_AL3_TRANS_COUNT         MMIO32(DMA_BASE + 0x078)
#define CH2_AL2_TRANS_COUNT         MMIO32(DMA_BASE + 0x0a4)
#define CH2_AL3_TRANS_COUNT         MMIO32(DMA_BASE + 0x0b8)
#define CH3_AL2_TRANS_COUNT         MMIO32(DMA_BASE + 0x0e4)
#define CH3_AL3_TRANS_COUNT         MMIO32(DMA_BASE + 0x0f8)
#define CH4_AL2_TRANS_COUNT         MMIO32(DMA_BASE + 0x124)
#define CH4_AL3_TRANS_COUNT         MMIO32(DMA_BASE + 0x138)
#define CH5_AL2_TRANS_COUNT         MMIO32(DMA_BASE + 0x164)
#define CH5_AL3_TRANS_COUNT         MMIO32(DMA_BASE + 0x178)
#define CH6_AL2_TRANS_COUNT         MMIO32(DMA_BASE + 0x1a4)
#define CH6_AL3_TRANS_COUNT         MMIO32(DMA_BASE + 0x1b8)
#define CH7_AL2_TRANS_COUNT         MMIO32(DMA_BASE + 0x1e4)
#define CH7_AL3_TRANS_COUNT         MMIO32(DMA_BASE + 0x1f8)
#define CH8_AL2_TRANS_COUNT         MMIO32(DMA_BASE + 0x224)
#define CH8_AL3_TRANS_COUNT         MMIO32(DMA_BASE + 0x238)
#define CH9_AL2_TRANS_COUNT         MMIO32(DMA_BASE + 0x264)
#define CH9_AL3_TRANS_COUNT         MMIO32(DMA_BASE + 0x278)
#define CH10_AL2_TRANS_COUNT        MMIO32(DMA_BASE + 0x2a4)
#define CH10_AL3_TRANS_COUNT        MMIO32(DMA_BASE + 0x2b8)
#define CH11_AL2_TRANS_COUNT        MMIO32(DMA_BASE + 0x2e4)
#define CH11_AL3_TRANS_COUNT        MMIO32(DMA_BASE + 0x2f8)
// Writing a nonzero value will reload the channel counter and start the channel.
#define CH0_AL1_TRANS_COUNT_TRIG    MMIO32(DMA_BASE + 0x01c)
#define CH1_AL1_TRANS_COUNT_TRIG    MMIO32(DMA_BASE + 0x05c)
#define CH2_AL1_TRANS_COUNT_TRIG    MMIO32(DMA_BASE + 0x09c)
#define CH3_AL1_TRANS_COUNT_TRIG    MMIO32(DMA_BASE + 0x0dc)
#define CH4_AL1_TRANS_COUNT_TRIG    MMIO32(DMA_BASE + 0x11c)
#define CH5_AL1_TRANS_COUNT_TRIG    MMIO32(DMA_BASE + 0x15c)
#define CH6_AL1_TRANS_COUNT_TRIG    MMIO32(DMA_BASE + 0x19c)
#define CH7_AL1_TRANS_COUNT_TRIG    MMIO32(DMA_BASE + 0x1dc)
#define CH8_AL1_TRANS_COUNT_TRIG    MMIO32(DMA_BASE + 0x21c)
#define CH9_AL1_TRANS_COUNT_TRIG    MMIO32(DMA_BASE + 0x25c)
#define CH10_AL1_TRANS_COUNT_TRIG   MMIO32(DMA_BASE + 0x29c)
#define CH11_AL1_TRANS_COUNT_TRIG   MMIO32(DMA_BASE + 0x2dc)

/* DMA Channel n Control and Status */
#define CH0_CTRL_TRIG               MMIO32(DMA_BASE + 0x00c)
#define CH1_CTRL_TRIG               MMIO32(DMA_BASE + 0x04c)
#define CH2_CTRL_TRIG               MMIO32(DMA_BASE + 0x08c)
#define CH3_CTRL_TRIG               MMIO32(DMA_BASE + 0x0cc)
#define CH4_CTRL_TRIG               MMIO32(DMA_BASE + 0x10c)
#define CH5_CTRL_TRIG               MMIO32(DMA_BASE + 0x14c)
#define CH6_CTRL_TRIG               MMIO32(DMA_BASE + 0x18c)
#define CH7_CTRL_TRIG               MMIO32(DMA_BASE + 0x1cc)
#define CH8_CTRL_TRIG               MMIO32(DMA_BASE + 0x20c)
#define CH9_CTRL_TRIG               MMIO32(DMA_BASE + 0x24c)
#define CH10_CTRL_TRIG              MMIO32(DMA_BASE + 0x28c)
#define CH11_CTRL_TRIG              MMIO32(DMA_BASE + 0x2cc)
// Logical OR of the READ_ERROR and WRITE_ERROR flags.
#define AHB_ERROR       0x80000000
// If 1, the channel received a read bus error. Write one to clear.
#define READ_ERROR      0x40000000
// If 1, the channel received a write bus error. Write one to clear.
#define WRITE_ERROR     0x20000000
// This flag goes high when the channel starts a new transfer sequence
#define BUSY            0x01000000
// If 1, this channel’s data transfers are visible to the sniff hardware
#define SNIFF_EN        0x00800000
// Apply byte-swap transformation to DMA data.
#define BSWAP           0x00400000
// In QUIET mode, the channel don't gen IRQs at end of evr trans block.
#define IRQ_QUIET       0x00200000
// Select a Transfer Request signal.
#define TREQ_SEL_DREQ0  0x00000000
#define TREQ_SEL_DREQ1  0x00008000
#define TREQ_SEL_DREQ2  0x00010000
#define TREQ_SEL_DREQ3  0x00018000
#define TREQ_SEL_DREQ4  0x00020000
#define TREQ_SEL_DREQ5  0x00028000
#define TREQ_SEL_DREQ6  0x00030000
#define TREQ_SEL_DREQ7  0x00038000
#define TREQ_SEL_DREQ8  0x00040000
#define TREQ_SEL_DREQ9  0x00048000
#define TREQ_SEL_DREQ10 0x00050000
#define TREQ_SEL_DREQ11 0x00058000
#define TREQ_SEL_DREQ12 0x00060000
#define TREQ_SEL_DREQ13 0x00068000
#define TREQ_SEL_DREQ14 0x00070000
#define TREQ_SEL_DREQ15 0x00078000
#define TREQ_SEL_DREQ16 0x00080000
#define TREQ_SEL_DREQ17 0x00088000
#define TREQ_SEL_DREQ18 0x00090000
#define TREQ_SEL_DREQ19 0x00098000
#define TREQ_SEL_DREQ20 0x000a0000
#define TREQ_SEL_DREQ21 0x000a8000
#define TREQ_SEL_DREQ22 0x000b0000
#define TREQ_SEL_DREQ23 0x000b8000
#define TREQ_SEL_DREQ24 0x000c0000
#define TREQ_SEL_DREQ25 0x000c8000
#define TREQ_SEL_DREQ26 0x000d0000
#define TREQ_SEL_DREQ27 0x000d8000
#define TREQ_SEL_DREQ28 0x000e0000
#define TREQ_SEL_DREQ29 0x000e8000
#define TREQ_SEL_DREQ30 0x000f0000
#define TREQ_SEL_DREQ31 0x000f8000
#define TREQ_SEL_DREQ32 0x00100000
#define TREQ_SEL_DREQ33 0x00108000
#define TREQ_SEL_DREQ34 0x00110000
#define TREQ_SEL_DREQ35 0x00118000
#define TREQ_SEL_DREQ36 0x00120000
#define TREQ_SEL_DREQ37 0x00128000
#define TREQ_SEL_DREQ38 0x00130000
#define TREQ_SEL_DREQ39 0x00138000
#define TREQ_SEL_DREQ40 0x00140000
#define TREQ_SEL_DREQ41 0x00148000
#define TREQ_SEL_DREQ42 0x00150000
#define TREQ_SEL_DREQ43 0x00158000
#define TREQ_SEL_DREQ44 0x00160000
#define TREQ_SEL_DREQ45 0x00168000
#define TREQ_SEL_DREQ46 0x00170000
#define TREQ_SEL_DREQ47 0x00178000
#define TREQ_SEL_DREQ48 0x00180000
#define TREQ_SEL_DREQ49 0x00188000
#define TREQ_SEL_DREQ50 0x00190000
#define TREQ_SEL_DREQ51 0x00198000
#define TREQ_SEL_DREQ52 0x001a0000
#define TREQ_SEL_DREQ53 0x001a8000
#define TREQ_SEL_DREQ54 0x001b0000
#define TREQ_SEL_DREQ55 0x001b8000
#define TREQ_SEL_DREQ56 0x001c0000
#define TREQ_SEL_DREQ57 0x001c8000
#define TREQ_SEL_DREQ58 0x001d0000
#define TREQ_SEL_DREQ(n) ((n&0x3f)<<15)
#define TREQ_SEL_TIM0   0x001d8000
#define TREQ_SEL_TIM1   0x001e0000
#define TREQ_SEL_TIM2   0x001e8000
#define TREQ_SEL_TIM3   0x001f0000
#define TREQ_SEL_PREMAMENT  0x001f8000
#define TREQ_SEL_MSK    0x001f8000
// When this channel completes, it will trigger the channel indicated by CHAIN_TO
#define CHAIN_TO0       0x00000000
#define CHAIN_TO1       0x00000800
#define CHAIN_TO2       0x00001000
#define CHAIN_TO3       0x00001800
#define CHAIN_TO4       0x00001000
#define CHAIN_TO5       0x00002800
#define CHAIN_TO6       0x00003000
#define CHAIN_TO7       0x00003800
#define CHAIN_TO8       0x00004000
#define CHAIN_TO9       0x00004800
#define CHAIN_TO10      0x00005000
#define CHAIN_TO11      0x00005800
#define CHAIN_TO(n)     ((n&0xf)<<11)
#define CHAIN_TO_MSK    0x00007800
// Select whether RING_SIZE applies to read or write addresses.
#define RING_SEL        0x00000400
// Size of address wrap region.
#define RING_NONE       0x00000000
#define RING_SIZE1      0x00000040
#define RING_SIZE2      0x00000080
#define RING_SIZE3      0x000000c0
#define RING_SIZE4      0x00000100
#define RING_SIZE5      0x00000140
#define RING_SIZE6      0x00000180
#define RING_SIZE7      0x000001c0
#define RING_SIZE8      0x00000200
#define RING_SIZE9      0x00000240
#define RING_SIZE10     0x00000280
#define RING_SIZE11     0x000002c0
#define RING_SIZE12     0x00000300
#define RING_SIZE13     0x00000340
#define RING_SIZE14     0x00000380
#define RING_SIZE15     0x000003c0
#define RING_SIZE_MSK   0x000003c0
#define RING_SIZE_SFT   6
#define RING_SIZE(n)    ((n&0xf)<<6)
// If 1, the write address increments with each transfer.
#define INCR_WRITE      0x00000020
// If 1, the read address increments with each transfer.
#define INCR_READ       0x00000010
// Set the size of each bus transfer
#define DATA_SIZE_BYTE      0x00000000
#define DATA_SIZE_HALFWORD  0x00000004
#define DATA_SIZE_WORD      0x00000002
// gives a channel preferential treatment in issue scheduling:
#define HIGH_PRIORITY   0x00000002
// DMA Channel Enable.
#define DMA_EN          0x00000001
/* Aliases for channel n CTRL register */
#define CH0_AL1_CTRL                MMIO32(DMA_BASE + 0x010)
#define CH0_AL2_CTRL                MMIO32(DMA_BASE + 0x020)
#define CH0_AL3_CTRL                MMIO32(DMA_BASE + 0x030)
#define CH1_AL1_CTRL                MMIO32(DMA_BASE + 0x050)
#define CH1_AL2_CTRL                MMIO32(DMA_BASE + 0x060)
#define CH1_AL3_CTRL                MMIO32(DMA_BASE + 0x070)
#define CH2_AL1_CTRL                MMIO32(DMA_BASE + 0x090)
#define CH2_AL2_CTRL                MMIO32(DMA_BASE + 0x0a0)
#define CH2_AL3_CTRL                MMIO32(DMA_BASE + 0x0b0)
#define CH3_AL1_CTRL                MMIO32(DMA_BASE + 0x0d0)
#define CH3_AL2_CTRL                MMIO32(DMA_BASE + 0x0e0)
#define CH3_AL3_CTRL                MMIO32(DMA_BASE + 0x0f0)
#define CH4_AL1_CTRL                MMIO32(DMA_BASE + 0x110)
#define CH4_AL2_CTRL                MMIO32(DMA_BASE + 0x120)
#define CH4_AL3_CTRL                MMIO32(DMA_BASE + 0x130)
#define CH5_AL1_CTRL                MMIO32(DMA_BASE + 0x150)
#define CH5_AL2_CTRL                MMIO32(DMA_BASE + 0x160)
#define CH5_AL3_CTRL                MMIO32(DMA_BASE + 0x170)
#define CH6_AL1_CTRL                MMIO32(DMA_BASE + 0x190)
#define CH6_AL2_CTRL                MMIO32(DMA_BASE + 0x1a0)
#define CH6_AL3_CTRL                MMIO32(DMA_BASE + 0x1b0)
#define CH7_AL1_CTRL                MMIO32(DMA_BASE + 0x1d0)
#define CH7_AL2_CTRL                MMIO32(DMA_BASE + 0x1e0)
#define CH7_AL3_CTRL                MMIO32(DMA_BASE + 0x1f0)
#define CH8_AL1_CTRL                MMIO32(DMA_BASE + 0x210)
#define CH8_AL2_CTRL                MMIO32(DMA_BASE + 0x220)
#define CH8_AL3_CTRL                MMIO32(DMA_BASE + 0x230)
#define CH9_AL1_CTRL                MMIO32(DMA_BASE + 0x250)
#define CH9_AL2_CTRL                MMIO32(DMA_BASE + 0x260)
#define CH9_AL3_CTRL                MMIO32(DMA_BASE + 0x270)
#define CH10_AL1_CTRL               MMIO32(DMA_BASE + 0x390)
#define CH10_AL2_CTRL               MMIO32(DMA_BASE + 0x2a0)
#define CH10_AL3_CTRL               MMIO32(DMA_BASE + 0x2b0)
#define CH11_AL1_CTRL               MMIO32(DMA_BASE + 0x2d0)
#define CH11_AL2_CTRL               MMIO32(DMA_BASE + 0x2e0)
#define CH11_AL3_CTRL               MMIO32(DMA_BASE + 0x2f0)

/* Interrupt Status (raw) */
#define INTR                        MMIO32(DMA_BASE + 0x400)
// Bit n corresponds to channel n Ignores any masking or forcing.
#define INTR_CH0    0x0001
#define INTR_CH1    0x0002
#define INTR_CH2    0x0004
#define INTR_CH3    0x0008
#define INTR_CH4    0x0010
#define INTR_CH5    0x0020
#define INTR_CH6    0x0040
#define INTR_CH7    0x0080
#define INTR_CH8    0x0100
#define INTR_CH9    0x0200
#define INTR_CH10   0x0400
#define INTR_CH11   0x0800
#define INTR_CH12   0x1000
#define INTR_CH13   0x2000
#define INTR_CH14   0x4000
#define INTR_CH15   0x8000

/* Interrupt Enables for IRQ 0,1 */
#define INTE0                       MMIO32(DMA_BASE + 0x404)
#define INTE1                       MMIO32(DMA_BASE + 0x414)
// Set bit n to pass interrupts from channel n to DMA IRQ 0,1.

/* Force Interrupts */
#define INTF0                       MMIO32(DMA_BASE + 0x408)
#define INTF1                       MMIO32(DMA_BASE + 0x418)
// Write 1s to force the corresponding bits in INTE0/1.

/* Interrupt Status for IRQ 0/1 */
#define INTS0                       MMIO32(DMA_BASE + 0x40c)
#define INTS1                       MMIO32(DMA_BASE + 0x41c)
// active channel interrupt requests which are currently causing IRQ 0/1.

/* Pacing (X/Y) Fractional Timer */
#define TIMER0                      MMIO32(DMA_BASE + 0x420)
#define TIMER1                      MMIO32(DMA_BASE + 0x424)
#define TIMER2                      MMIO32(DMA_BASE + 0x428)
#define TIMER3                      MMIO32(DMA_BASE + 0x42c)
// Specifies the X value for the (X/Y) fractional timer.
#define X(n)        ((n&0xffff)<<16)
#define FETCH_X(n)  ((n>>16)&0xffff)
// Specifies the Y value for the (X/Y) fractional timer.
#define Y(n)        (n&0xffff)
#define FETCH_Y(n)  (n&0xffff)

/* Trigger one or more channels simultaneously */
#define MULTI_CHAN_TRIGGER          MMIO32(DMA_BASE + 0x430)
// channel will start if it is currently enabled and not already busy.

/* Sniffer Control */
#define SNIFF_CTRL                  MMIO32(DMA_BASE + 0x434)
// If set, the result appears inverted (bitwise complement) when read.
#define OUT_INV             0x00000800
// If set, the result appears bit-reversed when read.
#define OUT_REV             0x00000400
// Locally perform a byte reverse on the sniffed data, before feeding into crc
#define SNIFF_BSWAP         0x00000200
// Calculate a CRC
#define CALC_CRC32          0x00000000
#define CALC_CRC32REVERSE   0x00000020
#define CALC_CRC16          0x00000040
#define CALC_CRC16REVERSE   0x00000060
#define CALC_XOR            0x000001c0
#define CALC_CRC_SIMPLE     0x000001e0
// DMA channel for Sniffer to observe
#define DMACH0              0x00000000
#define DMACH1              0x00000002
#define DMACH2              0x00000004
#define DMACH3              0x00000006
#define DMACH4              0x00000008
#define DMACH5              0x0000000a
#define DMACH6              0x0000000c
#define DMACH7              0x0000000e
#define DMACH8              0x00000010
#define DMACH9              0x00000012
#define DMACH10             0x00000014
#define DMACH11             0x00000016
#define DMACH12             0x00000018
#define DMACH13             0x0000001a
#define DMACH14             0x0000001c
#define DMACH15             0x0000001e
#define DMACH(n)            ((n&0xf)<<1)
#define DMACH_MSK           0x0000001e
// Enable sniffer
#define DMA_SNIFF_EN    0x00000001

/* Data accumulator for sniff hardware */
#define SNIFF_DATA                  MMIO32(DMA_BASE + 0x438)
// Write an initial seed value here before starting a DMA transfer on the channel

/* Debug RAF, WAF, TDF levels */
#define FIFO_LEVELS                 MMIO32(DMA_BASE + 0x440)
// Current Read-Address-FIFO fill level
#define RAF_LVL ((FIFO_LEVELS>>16)&0xff)
// Current Write-Address-FIFO fill level
#define WAF_LVL ((FIFO_LEVELS>>8)&0xff)
// Current Transfer-Data-FIFO fill level
#define TDF_LVL (FIFO_LEVELS&0xff)

/* Abort an in-progress transfer sequence on one or more channels */
#define CHAN_ABORT                  MMIO32(DMA_BASE + 0x444)
// aborts whatever transfer sequence is in progress on that channel.

/* The number of channels this DMA instance is equipped with. */
#define N_CHANNELS                  MMIO32(DMA_BASE + 0x448)
// The number of channels this DMA instance is equipped with

/* get channel DREQ counter */
#define CH0_DBG_CTDREQ              MMIO32(DMA_BASE + 0x800)
#define CH1_DBG_CTDREQ              MMIO32(DMA_BASE + 0x840)
#define CH2_DBG_CTDREQ              MMIO32(DMA_BASE + 0x880)
#define CH3_DBG_CTDREQ              MMIO32(DMA_BASE + 0x8c0)
#define CH4_DBG_CTDREQ              MMIO32(DMA_BASE + 0x900)
#define CH5_DBG_CTDREQ              MMIO32(DMA_BASE + 0x940)
#define CH6_DBG_CTDREQ              MMIO32(DMA_BASE + 0x980)
#define CH7_DBG_CTDREQ              MMIO32(DMA_BASE + 0x9c0)
#define CH8_DBG_CTDREQ              MMIO32(DMA_BASE + 0xa00)
#define CH9_DBG_CTDREQ              MMIO32(DMA_BASE + 0xa40)
#define CH10_DBG_CTDREQ             MMIO32(DMA_BASE + 0xa80)
#define CH11_DBG_CTDREQ             MMIO32(DMA_BASE + 0xac0)

/*  Read to get channel TRANS_COUNT reload value*/
#define CH0_DBG_TCR                 MMIO32(DMA_BASE + 0x804)
#define CH1_DBG_TCR                 MMIO32(DMA_BASE + 0x844)
#define CH2_DBG_TCR                 MMIO32(DMA_BASE + 0x884)
#define CH3_DBG_TCR                 MMIO32(DMA_BASE + 0x8c4)
#define CH4_DBG_TCR                 MMIO32(DMA_BASE + 0x904)
#define CH5_DBG_TCR                 MMIO32(DMA_BASE + 0x944)
#define CH6_DBG_TCR                 MMIO32(DMA_BASE + 0x984)
#define CH7_DBG_TCR                 MMIO32(DMA_BASE + 0x9c4)
#define CH8_DBG_TCR                 MMIO32(DMA_BASE + 0xa04)
#define CH9_DBG_TCR                 MMIO32(DMA_BASE + 0xa44)
#define CH10_DBG_TCR                MMIO32(DMA_BASE + 0xa84)
#define CH11_DBG_TCR                MMIO32(DMA_BASE + 0xac4)
// length of the next transfer

#endif
