#ifndef H_UART_REGS
#define H_UART_REGS
/*
 * RP2040 alternative library, a UART peripheral registers
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "system_regs/memorymap.h"

/* Data Register, UARTDR */
#define UARTDR_0        MMIO32(UART0_BASE + 0x000)
#define UARTDR_1        MMIO32(UART1_BASE + 0x000)
// Overrun error.
#define OE              0x800
// Break error.
#define BE              0x400
// Parity error.
#define PE              0x200
// FE
#define FE              0x100
// Receive (read) data character. Transmit (write) data character.
#define DATA_MSK        0xff
#define DATA0_SET(d)    (UARTDR_0|(d&DATA_MSK))
#define DATA0_FETCH     (UARTDR_0&DATA_MSK)
#define DATA1_SET(d)    (UARTDR_1|(d&DATA_MSK))
#define DATA1_FETCH     (UARTDR_1&DATA_MSK)

/* Receive Status Register/Error Clear Register, UARTRSR/UARTECR */
#define UARTRSR_0       MMIO32(UART0_BASE + 0x004)
#define UARTRSR_1       MMIO32(UART1_BASE + 0x004)
// Overrun error.
#define ROE             0x8
// Break error.
#define RBE             0x4
// Parity error.
#define RPE             0x2
// FE
#define RFE             0x1

/* Flag Register, UARTFR */
#define UARTFR_0        MMIO32(UART0_BASE + 0x018)
#define UARTFR_1        MMIO32(UART1_BASE + 0x018)
// Ring indicator.
#define RI      0x100
// Transmit FIFO empty.
#define TXFE    0x080
// Receive FIFO full.
#define RXFF    0x040
// Transmit FIFO full.
#define TXFF    0x020
// Receive FIFO empty.
#define RXFE    0x010
// UART busy.
#define BUSY    0x008
// Data carrier detect.
#define DCD     0x004
// Data set ready.
#define DSR     0x002
// Clear to send.
#define CTS     0x001

/* IrDA Low-Power Counter Register, UARTILPR */
#define UARTILPR_0      MMIO32(UART0_BASE + 0x020)
#define UARTILPR_1      MMIO32(UART1_BASE + 0x020)
// 8-bit low-power divisor value. These bits are cleared to 0 at reset.

/* Integer Baud Rate Register, UARTIBRD */
#define UARTIBRD_0      MMIO32(UART0_BASE + 0x024)
#define UARTIBRD_1      MMIO32(UART1_BASE + 0x024)
// The integer baud rate divisor. These bits are cleared to 0 on reset.

/* Fractional Baud Rate Register, UARTFBRD */
#define UARTFBRD_0      MMIO32(UART0_BASE + 0x028)
#define UARTFBRD_1      MMIO32(UART1_BASE + 0x028)
// The fractional baud rate divisor. These bits are cleared to 0 on reset.

/* Line Control Register, UARTLCR_H */
#define UARTLCR_H_0     MMIO32(UART0_BASE + 0x02c)
#define UARTLCR_H_1     MMIO32(UART1_BASE + 0x02c)
// Stick parity select.
#define SPS     0x80
// Word length.
#define WLEN8   0x60
#define WLEN7   0x40
#define WLEN6   0x20
#define WLEN5   0x00
// Enable FIFOs: 0 = FIFOs are disabled (character mode)
#define FEN     0x10
// Two stop bits select
#define STP2    0x08
// Even parity select.
#define EPS     0x04
// Parity enable: 0 = parity is disabled and no parity bit added
#define PEN     0x02
// Send break.
#define BRK     0x01

/* Control Register, UARTCR */
#define UARTCR_0        MMIO32(UART0_BASE + 0x030)
#define UARTCR_1        MMIO32(UART1_BASE + 0x030)
// CTS hardware flow control enable.
#define CTSEN   0x8000
// RTS hardware flow control enable.
#define RTSEN   0x4000
// This bit is the complement of the UART Out2 modem status output.
#define OUT2    0x2000
// This bit is the complement of the UART Out1 modem status output.
#define OUT1    0x1000
// Request to send.
#define RTS     0x0800
// Data transmit ready.
#define DTR     0x0400
// Receive enable.
#define RXE     0x0200
// Transmit enable.
#define TXE     0x0100
// Loopback enable.
#define LBE     0x0080
// SIR low-power IrDA mode.
#define SIRLP   0x0004
// SIR enable: 0 = IrDA SIR ENDEC is disabled.
#define SIREN   0x0002
// UART enable: 0 = UART is disabled.
#define UARTEN  0x0001

/* Interrupt FIFO Level Select Register, UARTIFLS */
#define UARTIFLS_0      MMIO32(UART0_BASE + 0x034)
#define UARTIFLS_1      MMIO32(UART1_BASE + 0x034)
// Receive interrupt FIFO level select.
#define RXIFLSEL1_8 0x00
#define RXIFLSEL1_4 0x08
#define RXIFLSEL1_2 0x10
#define RXIFLSEL3_4 0x18
#define RXIFLSEL7_8 0x20
// Transmit interrupt FIFO level select.
#define TXIFLSEL1_8 0x00
#define TXIFLSEL1_4 0x01
#define TXIFLSEL1_2 0x02
#define TXIFLSEL3_4 0x03
#define TXIFLSEL7_8 0x04

/* Interrupt Mask Set/Clear Register, UARTIMSC */
#define UARTIMSC_0      MMIO32(UART0_BASE + 0x038)
#define UARTIMSC_1      MMIO32(UART1_BASE + 0x038)
// Overrun error interrupt mask.
#define OEIM    0x400
// Break error interrupt mask.
#define BEIM    0x200
// Parity error interrupt mask.
#define PEIM    0x100
// Framing error interrupt mask.
#define FEIM    0x080
// Receive timeout interrupt mask.
#define RTIM    0x040
// Transmit interrupt mask.
#define TXIM    0x020
// Receive interrupt mask.
#define RXIM    0x010
// nUARTDSR modem interrupt mask.
#define DSRMIM  0x008
// nUARTDCD modem interrupt mask.
#define DCDMIM  0x004
// nUARTCTS modem interrupt mask.
#define CTSMIM  0x002
// nUARTRI modem interrupt mask.
#define RIMIM   0x001

/* Raw Interrupt Status Register, UARTRIS */
#define UARTRIS_0       MMIO32(UART0_BASE + 0x03c)
#define UARTRIS_1       MMIO32(UART1_BASE + 0x03c)
// Overrun error interrupt status.
#define OERIS   0x400
// Break error interrupt status.
#define BERIS   0x200
// Parity error interrupt status.
#define PERIS   0x100
// Framing error interrupt status.
#define FERIS   0x080
// Receive timeout interrupt status.
#define RTRIS   0x040
// Transmit interrupt status.
#define TXRIS   0x020
// Receive interrupt status.
#define RXRIS   0x010
// nUARTDSR modem interrupt status.
#define DSRMRIS 0x008
// nUARTDCD modem interrupt status.
#define DCDMRIS 0x004
// nUARTCTS modem interrupt status.
#define CTSMRIS 0x002
// nUARTRI modem interrupt status.
#define RIMRIS  0x001

/* Masked Interrupt Status Register, UARTMIS */
#define UARTMIS_0       MMIO32(UART0_BASE + 0x040)
#define UARTMIS_1       MMIO32(UART1_BASE + 0x040)
// Overrun error masked interrupt status.
#define OEMIS   0x400
// Break error masked interrupt status.
#define BEMIS   0x200
// Parity error masked interrupt status.
#define PEMIS   0x100
// Framing error masked interrupt status.
#define FEMIS   0x080
// Receive timeout masked interrupt status.
#define RTMIS   0x040
// Transmit masked interrupt status.
#define TXMIS   0x020
// Receive masked interrupt status.
#define RXMIS   0x010
// nUARTDSR modem masked interrupt status.
#define DSRMMIS 0x008
// nUARTDCD modem masked interrupt status.
#define DCDMMIS 0x004
// nUARTCTS modem masked interrupt status.
#define CTSMMIS 0x002
// nUARTRI modem masked interrupt status.
#define RIMMIS  0x001

/* Interrupt Clear Register, UARTICR */
#define UARTICR_0       MMIO32(UART0_BASE + 0x044)
#define UARTICR_1       MMIO32(UART1_BASE + 0x044)
// Overrun error interrupt clear.
#define OEIC    0x400
// Break error interrupt clear.
#define BEIC    0x200
// Parity error interrupt clear.
#define PEIC    0x100
// Framing error interrupt clear.
#define FEIC    0x080
// Receive timeout interrupt clear.
#define RTIC    0x040
// Transmit interrupt clear.
#define TXIC    0x020
// Receive interrupt clear.
#define RXIC    0x010
// nUARTDSR modem interrupt clear.
#define DSRMIC  0x008
// nUARTDCD modem interrupt clear.
#define DCDMIC  0x004
// nUARTCTS modem interrupt clear.
#define CTSMIC  0x002
// nUARTRI modem interrupt clear.
#define RIMIC   0x001

/* DMA Control Register, UARTDMACR */
#define UARTDMACR_0     MMIO32(UART0_BASE + 0x048)
#define UARTDMACR_1     MMIO32(UART1_BASE + 0x048)
// DMA on error.
#define DMAONERR    0x4
// Transmit DMA enable.
#define TXDMAE      0x2
// Receive DMA enable.
#define RXDMAE      0x1

/* UARTPeriphID0 Register */
#define UARTPERIPHID0_0 MMIO32(UART0_BASE + 0xfe0)
#define UARTPERIPHID0_1 MMIO32(UART1_BASE + 0xfe0)
// These bits read back as 0x11

/* UARTPeriphID1 Register */
#define UARTPERIPHID1_0 MMIO32(UART0_BASE + 0xfe4)
#define UARTPERIPHID1_1 MMIO32(UART1_BASE + 0xfe4)
// These bits read back as 0x1
#define DESIGNER0_0FETCH    ((UARTPERIPHID1_0>>7)&0xf)
#define DESIGNER0_1FETCH    ((UARTPERIPHID1_1>>7)&0xf)
// These bits read back as 0x0
#define PARTNUMBER1_0FETCH  (UARTPERIPHID1_0&0xf)
#define PARTNUMBER1_1FETCH  (UARTPERIPHID1_1&0xf)

/* UARTPeriphID2 Register */
#define UARTPERIPHID2_0 MMIO32(UART0_BASE + 0xfe8)
#define UARTPERIPHID2_1 MMIO32(UART1_BASE + 0xfe8)
// This field depends on the revision of the UART
#define REVISION_0FETCH     ((UARTPERIPHID2_0>>7)&0xf)
#define REVISION_1FETCH     ((UARTPERIPHID2_1>>7)&0xf)
// These bits read back as 0x4
#define DESIGNER1_0FETCH    (UARTPERIPHID2_0&0xf)
#define DESIGNER1_1FETCH    (UARTPERIPHID2_1&0xf)

/* UARTPeriphID3 Register */
#define UARTPERIPHID3_0 MMIO32(UART0_BASE + 0xfec)
#define UARTPERIPHID3_1 MMIO32(UART1_BASE + 0xfec)
// These bits read back as 0x00
#define

/* UARTPCellID0 Register */
#define UARTPCELLID0_0  MMIO32(UART0_BASE + 0xff0)
#define UARTPCELLID0_1  MMIO32(UART1_BASE + 0xff0)
// These bits read back as 0x0D

/* UARTPCellID1 Register */
#define UARTPCELLID1_0  MMIO32(UART0_BASE + 0xff4)
#define UARTPCELLID1_1  MMIO32(UART1_BASE + 0xff4)
// These bits read back as 0xF0

/* UARTPCellID2 Register */
#define UARTPCELLID2_0  MMIO32(UART0_BASE + 0xff8)
#define UARTPCELLID2_1  MMIO32(UART1_BASE + 0xff8)
// These bits read back as 0x05

/* UARTPCellID3 Register */
#define UARTPCELLID3_0  MMIO32(UART0_BASE + 0xffc)
#define UARTPCELLID3_1  MMIO32(UART1_BASE + 0xffc)
// These bits read back as 0xB1

#endif
