#ifndef H_ADC_REGS
#define H_ADC_REGS
/*
 * RP2040 alternative library, analogue-digital converter (ADC) registers
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "system_regs/memorymap.h"

/* ADC Control and Status */
#define CS          MMIO32(ADC_BASE + 0x00)
// Round-robin sampling. 1 bit per channel.
#define RROBIN4         0x100000
#define RROBIN3         0x080000
#define RROBIN2         0x040000
#define RROBIN1         0x020000
#define RROBIN0         0x010000
#define RROBIN_MSK      0x1f0000
// Select analog mux input. Updated automatically in round-robin mode.
#define AINSEL_MSK      0x007000
#define AINSEL_SFT      12
#define AINSEL_SET(n)   ((n<<AINSEL_SFT)&AINSEL_MSK)
// Some past ADC conversion encountered an error.
#define ERR_STICKY      0x000400
// ADC conversion encountered an error, result is undefined or noisy.
#define ERR             0x000200
// 1 if the ADC is ready to start a new conversion.
#define READY           0x000100
// Continuously perform conversions whilst this bit is 1.
#define START_MANY      0x000008
// Start a single conversion. Self-clearing.
#define START_ONCE      0x000004
// Power on temperature sensor. 1 - enabled. 0 - disabled.
#define TS_EN           0x000002
// Power on ADC and enable its clock.
#define ADC_EN          0x000001

/* Result of most recent ADC conversion */
#define RESULT      MMIO32(ADC_BASE + 0x04)
// Result of most recent ADC conversion
#define RESULT_MSK  0xfff

/* FIFO control and status */
#define FCS         MMIO32(ADC_BASE + 0x08)
// DREQ/IRQ asserted when level >= threshold
#define THRESH_MSK      0xf000000
#define THRESH_SFT      24
#define THRESH_SET(n)   ((n<<THRESH_SFT)&THRESH_MSK)
// The number of conversion results currently waiting in the FIFO
#define LEVEL_MSK       0x00f0000
#define LEVEL_SFT       16
#define LEVEL_SET(n)    ((n<<LEVEL_SFT)&LEVEL_MSK)
// 1 if the FIFO has been overflowed. Write 1 to clear.
#define OVER            0x0000800
// 1 if the FIFO has been underflowed. Write 1 to clear.
#define UNDER           0x0000400
#define FULL            0x0000200
#define EMPTY           0x0000100
// If 1: assert DMA requests when FIFO contains data
#define DREQ_EN         0x0000008
// If 1: conversion error bit appears in the FIFO alongside the result
#define ADC_FIFO_ERR    0x0000004
// If 1: FIFO results are right-shifted to be one byte in size.
#define SHIFT           0x0000002
// If 1: write result to the FIFO after each conversion.
#define ADC_FIFO_EN     0x0000001

/* Conversion result FIFO */
#define FIFO        MMIO32(ADC_BASE + 0x0c)
// 1 if this particular sample experienced a conversion error.
#define FIFO_ERR    0x8000
// value
#define VAL_MSK     0x0fff

/* Clock divider. */
#define DIV         MMIO32(ADC_BASE + 0x10)
// Integer part of clock divisor.
#define ADC_INT_MSK     0xffff00
#define ADC_INT_SFT     8
// Fractional part of clock divisor. First-order delta-sigma.
#define ADC_FRAC_MSK    0x0000ff

/* Raw Interrupts */
#define ADC_INTR    MMIO32(ADC_BASE + 0x14)
// Triggered when the sample FIFO reaches a certain level.
#define ADC_FIFO_INT    0x1

/* Interrupt Enable */
#define ADC_INTE    MMIO32(ADC_BASE + 0x18)
// Triggered when the sample FIFO reaches a certain level.

/* Interrupt Force */
#define ADC_INTF    MMIO32(ADC_BASE + 0x1c)
// Triggered when the sample FIFO reaches a certain level.

/* Interrupt status after masking & forcing */
#define ADC_INTS    MMIO32(ADC_BASE + 0x20)
// Triggered when the sample FIFO reaches a certain level.

#endif
