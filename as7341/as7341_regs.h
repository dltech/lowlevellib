#ifndef H_AS7341_REGS
#define H_AS7341_REGS
/*
 * AS7341 11-Channel Multi-Spectral Digital Sensor. Register definitions.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define AS7341_ADDR 0x39

#define ASTATUS         0x60    /* ASTATUS Register */
#define ASTATUS_ALIAS   0x94
// Saturation Status.
#define ASAT_STATUS         0x80
// Gain Status.
#define AGAIN_STATUS_MSK    0x0f

#define CH0_DATA_L      0x95    /* CH0 ADC data – low byte */
#define CH0_DATA_H      0x96    /* CH0 ADC data – high byte */
#define CH0_DATA_L_ALIAS    0x61
#define CH0_DATA_H_ALIAS    0x62

#define CH1_DATA_L      0x97    /* CH1 ADC data – low byte */
#define CH1_DATA_H      0x98    /* CH1 ADC data – high byte */
#define CH1_DATA_L_ALIAS    0x66
#define CH1_DATA_H_ALIAS    0x67

#define CH2_DATA_L      0x99    /* CH2 ADC data – low byte */
#define CH2_DATA_H      0x9a    /* CH2 ADC data – high byte */
#define CH2_DATA_L_ALIAS    0x68
#define CH2_DATA_H_ALIAS    0x69

#define CH3_DATA_L      0x9b    /* CH3 ADC data – low byte */
#define CH3_DATA_H      0x9c    /* CH3 ADC data – high byte */
#define CH3_DATA_L_ALIAS    0x6a
#define CH3_DATA_H_ALIAS    0x6b

#define CH4_DATA_L      0x9d    /* CH4 ADC data – low byte */
#define CH4_DATA_H      0x9e    /* CH4 ADC data – high byte */
#define CH4_DATA_L_ALIAS    0x6c
#define CH4_DATA_H_ALIAS    0x6d

#define CH5_DATA_L      0x9f    /* CH5 ADC data – low byte */
#define CH5_DATA_H      0xa0    /* CH5 ADC data – high byte */
#define CH5_DATA_L_ALIAS    0x6e
#define CH5_DATA_H_ALIAS    0x6f

#define CONFIG          0x70    /* CONFIG Register */
// 0: External LED not controlled by AS7341  1: Register LED controls LED connected to pin LDR
#define LED_SEL         0x08
// 1: Sync signal applied on output pin INT
#define INT_SEL         0x04
// Ambient light sensing mode:
#define INT_MODE_MSK    0x03
#define INT_MODE_SPM    0x00
#define INT_MODE_SYNS   0x01
#define INT_MODE_SYND   0x03

#define STAT            0x71    /* STAT Register */
// 1: Device waits for sync pulse on GPIO to start integration (SYNS / SYND INT_mode)
#define WAIT_SYNC   0x2
// 1: Spectral measurement status is ready
#define READY       0x1

#define STATUS          0x93    /* STATUS Register */
// Spectral and Flicker Detect saturation.
#define ASAT    0x80
// Spectral Channel Interrupt.
#define AINT    0x08
// FIFO Buffer Interrupt.
#define FINT    0x04
// Calibration Interrupt.
#define C_INT   0x02
// System Interrupt.
#define SINT    0x01

#define STATUS2         0xa3    /* STATUS 2 Register */
// Spectral Valid. Indicates that the spectral measurement has been completed
#define AVALID          0x40
// Digital saturation. Indicates that the maximum counter value has been reached.
#define ASAT_DIGITAL    0x10
// Analog saturation.
#define ASAT_ANALOG     0x08
// Flicker detect analog saturation.
#define FDSAT_ANALOG    0x02
// Flicker detect digital saturation.
#define FDSAT_DIGITAL   0x01

#define STATUS3         0xa4    /* STATUS 3 Register */
// Spectral interrupt high.
#define INT_SP_H    0x20
// Spectral interrupt low.
#define INT_SP_L    0x10

#define STATUS5         0xa6    /* STATUS 5 Register */
// Flicker Detect interrupt.
#define SINT_FD     0x8
// SMUX operation interrupt.
#define SINT_SMUX   0x4

#define STATUS6         0xa7    /* STATUS 6 Register */
// FIFO Buffer Overflow.
#define FIFO_OV     0x80
// Over Temperature Detected.
#define OVTEMP      0x20
// Flicker Detect Trigger Error.
#define FD_TRIG     0x10
// Spectral Trigger Error.
#define SP_TRIG     0x04
// Sleep after Interrupt Active.
#define SAI_ACTIVE  0x02
// Initialization Busy.
#define INT_BUSY    0x01

#define FD_STATUS       0xdb    /* FD_STATUS Register */
// Flicker Detection Measurement Valid.
#define FD_MEASUREMENT_VALID    0x20
// Flicker Saturation Detected.
#define FD_SATURATION_DETECTED  0x10
// Flicker Detection 120Hz Flicker Valid.
#define FD_120HZ_FLICKER_VALID  0x08
// Flicker Detection 100Hz Flicker Valid.
#define FD_100HZ_FLICKER_VALID  0x04
// Flicker Detected at 120Hz.
#define FD_120HZ_FLICKER        0x02
// Flicker Detected at 100Hz.
#define FD_100HZ_FLICKER        0x01

#define GPIO            0x73    /* GPIO Register */
// 1: Photo diode connected to pin GPIO
#define PD_GPIO 0x2
// 1: Photo diode connected to pin INT
#define PD_INT  0x1

#define GPIO2           0xbe    /* GPIO 2 Register */
// GPIO Invert. If set, the GPIO output is inverted.
#define GPIO_INV    0x8
// GPIO Input Enable. If set, the GPIO pin accepts a non-floating input.
#define GPIO_IN_EN  0x4
// GPIO Output. If set, the output state of the GPIO is active directly.
#define GPIO_OUT    0x2
// GPIO Input. Indicates the status of the GPIO input if GPIO_IN_EN is set.
#define GPIO_IN     0x1

#define LED             0x74    /* LED Register */
// 1: External LED connected to pin LDR on
#define LED_ACT 0x80
// LED driving strength.
#define LED_DRIVE_MSK   0x7f
#define LED_DRIVE(ma)   (((ma/2)+4)&LED_DRIVE_MSK)
#define LED_DRIVE4MA    0x00
#define LED_DRIVE6MA    0x01
#define LED_DRIVE8MA    0x02
#define LED_DRIVE10MA   0x03
#define LED_DRIVE12MA   0x04
#define LED_DRIVE14MA   0x05
#define LED_DRIVE16MA   0x06
#define LED_DRIVE18MA   0x07
#define LED_DRIVE20MA   0x08
#define LED_DRIVE22MA   0x09
#define LED_DRIVE24MA   0x0a
#define LED_DRIVE26MA   0x0b
#define LED_DRIVE28MA   0x0c
#define LED_DRIVE30MA   0x0d
#define LED_DRIVE32MA   0x0e
#define LED_DRIVE34MA   0x0f
#define LED_DRIVE36MA   0x10
#define LED_DRIVE38MA   0x11
#define LED_DRIVE40MA   0x12
#define LED_DRIVE42MA   0x13
#define LED_DRIVE44MA   0x14
#define LED_DRIVE46MA   0x15
#define LED_DRIVE48MA   0x16
#define LED_DRIVE50MA   0x17
#define LED_DRIVE52MA   0x18
#define LED_DRIVE54MA   0x19
#define LED_DRIVE56MA   0x1a
#define LED_DRIVE58MA   0x1b
#define LED_DRIVE60MA   0x1c
#define LED_DRIVE62MA   0x1d
#define LED_DRIVE64MA   0x1e
#define LED_DRIVE66MA   0x1f
#define LED_DRIVE68MA   0x20
#define LED_DRIVE70MA   0x21
#define LED_DRIVE72MA   0x22
#define LED_DRIVE74MA   0x23
#define LED_DRIVE76MA   0x24
#define LED_DRIVE78MA   0x25
#define LED_DRIVE80MA   0x26
#define LED_DRIVE82MA   0x27
#define LED_DRIVE84MA   0x28
#define LED_DRIVE86MA   0x29
#define LED_DRIVE88MA   0x2a
#define LED_DRIVE90MA   0x2b
#define LED_DRIVE92MA   0x2c
#define LED_DRIVE94MA   0x2d
#define LED_DRIVE96MA   0x2e
#define LED_DRIVE98MA   0x2f
#define LED_DRIVE100MA  0x30
#define LED_DRIVE102MA  0x31
#define LED_DRIVE104MA  0x32
#define LED_DRIVE106MA  0x33
#define LED_DRIVE108MA  0x34
#define LED_DRIVE110MA  0x35
#define LED_DRIVE112MA  0x36
#define LED_DRIVE114MA  0x37
#define LED_DRIVE116MA  0x38
#define LED_DRIVE118MA  0x39
#define LED_DRIVE120MA  0x3a
#define LED_DRIVE122MA  0x3b
#define LED_DRIVE124MA  0x3c
#define LED_DRIVE126MA  0x3d
#define LED_DRIVE128MA  0x3e
#define LED_DRIVE130MA  0x3f
#define LED_DRIVE132MA  0x40
#define LED_DRIVE134MA  0x41
#define LED_DRIVE136MA  0x42
#define LED_DRIVE138MA  0x43
#define LED_DRIVE140MA  0x44
#define LED_DRIVE142MA  0x45
#define LED_DRIVE144MA  0x46
#define LED_DRIVE146MA  0x47
#define LED_DRIVE148MA  0x48
#define LED_DRIVE150MA  0x49
#define LED_DRIVE152MA  0x4a
#define LED_DRIVE154MA  0x4b
#define LED_DRIVE156MA  0x4c
#define LED_DRIVE158MA  0x4d
#define LED_DRIVE160MA  0x4e
#define LED_DRIVE162MA  0x4f
#define LED_DRIVE164MA  0x50
#define LED_DRIVE166MA  0x51
#define LED_DRIVE168MA  0x52
#define LED_DRIVE170MA  0x53
#define LED_DRIVE172MA  0x54
#define LED_DRIVE174MA  0x55
#define LED_DRIVE176MA  0x56
#define LED_DRIVE178MA  0x57
#define LED_DRIVE180MA  0x58
#define LED_DRIVE182MA  0x59
#define LED_DRIVE184MA  0x5a
#define LED_DRIVE186MA  0x5b
#define LED_DRIVE188MA  0x5c
#define LED_DRIVE190MA  0x5d
#define LED_DRIVE192MA  0x5e
#define LED_DRIVE194MA  0x5f
#define LED_DRIVE196MA  0x60
#define LED_DRIVE198MA  0x61
#define LED_DRIVE200MA  0x62
#define LED_DRIVE202MA  0x63
#define LED_DRIVE204MA  0x64
#define LED_DRIVE206MA  0x65
#define LED_DRIVE208MA  0x66
#define LED_DRIVE210MA  0x67
#define LED_DRIVE212MA  0x68
#define LED_DRIVE214MA  0x69
#define LED_DRIVE216MA  0x6a
#define LED_DRIVE218MA  0x6b
#define LED_DRIVE220MA  0x6c
#define LED_DRIVE222MA  0x6d
#define LED_DRIVE224MA  0x6e
#define LED_DRIVE226MA  0x6f
#define LED_DRIVE228MA  0x70
#define LED_DRIVE230MA  0x71
#define LED_DRIVE232MA  0x72
#define LED_DRIVE234MA  0x73
#define LED_DRIVE236MA  0x74
#define LED_DRIVE238MA  0x75
#define LED_DRIVE240MA  0x76
#define LED_DRIVE242MA  0x77
#define LED_DRIVE244MA  0x78
#define LED_DRIVE246MA  0x79
#define LED_DRIVE248MA  0x7a
#define LED_DRIVE250MA  0x7b
#define LED_DRIVE252MA  0x7c
#define LED_DRIVE254MA  0x7d
#define LED_DRIVE256MA  0x7e
#define LED_DRIVE258MA  0x7f

#define ENABLE          0x80    /* ENABLE Register */
// Flicker Detection Enable.
#define FDEN    0x40
// 1: Starts SMUX command
#define SMUXEN  0x10
// 1: Wait time between two consecutive spectral measurements enabled
#define WEN     0x08
// Spectral Measurement Enable.
#define SP_EN   0x02
// Power ON.
#define PON     0x01

#define ATIME           0x81    /* Integration time.*/
// Sets the number of integration steps from 1 to 256.

#define ASTEP_L         0xca    /* Integration time step size. */
#define ASTEP_H         0xcb    /* Integration time step size. */
// Sets the integration time per step in increments of 2.78μs.
#define ASTEP(us)   ((us/2.78)-1)

#define WTIME           0x83    /* Spectral Measurement Wait time. */
// 8-bit value to specify the delay between two consecutive spectral measurements.
#define WTIME_MS(ms)    ((ms/2.78)-1)

#define ITIME_L         0x63    /* Integration time in integration mode SYND */
#define ITIME_M         0x64    /* Integration time in integration mode SYND */
#define ITIME_H         0x65    /* Integration time in integration mode SYND */
#define ITIME(us)   ((us/2.78)-1)

#define EDGE            0x72    /* EDGE Register */
// Number of falling SYNC-edges between start and stop of integration in mode SYND

#define FD_TIME1        0xd8    /* flicker detection integration time */
// LSB of flicker detection integration time

#define FD_TIME2        0xda    /* flicker detection integration time */
// Flicker Detection gain setting (ADC5)
#define FD_GAIN0P5  0x00
#define FD_GAIN1    0x08
#define FD_GAIN2    0x10
#define FD_GAIN4    0x18
#define FD_GAIN8    0x20
#define FD_GAIN16   0x28
#define FD_GAIN32   0x30
#define FD_GAIN64   0x38
#define FD_GAIN128  0x40
#define FD_GAIN256  0x48
#define FD_GAIN512  0x50
// MSB of flicker detection integration time
#define FD_TIME_MSK 0x07

#define SP_TH_L_LSB     0x84    /* Spectral low threshold */
#define SP_TH_L_MSB     0x85    /* Spectral low threshold */

#define SP_TH_H_LSB     0x86    /* Spectral high threshold LSB */
#define SP_TH_H_MSB     0x87    /* Spectral high threshold LSB */

#define AUXID           0x90    /* Auxiliary identification */
#define AUXID_MSK   0xf

#define REVID           0x91    /* Revision number identification */
#define REV_ID_MSK  0x7

#define ID              0x92    /* Part number Identification */
#define ID_MSK      0xfc
#define ID_SFT      2
#define ID_GET(r)   ((r>>ID_SFT)&0x3f)

#define CFG0            0xa9    /* CFG0 Register */
// Low Power Idle.
#define LOW_POWER   0x20
// Register Bank Access 1: Register access to register 0x60 to 0x74
#define REG_BANK    0x10
// Trigger Long. Increases the WTIME setting by a factor of 16.
#define WLONG       0x04

#define CFG1            0xaa    /* CFG1 Register */
// Spectral engines gain setting. Sets the spectral sensitivity.
#define AGAIN_MSK   0x1f
#define AGAIN0P5    0x00
#define AGAIN1      0x01
#define AGAIN2      0x02
#define AGAIN4      0x03
#define AGAIN8      0x04
#define AGAIN16     0x05
#define AGAIN32     0x06
#define AGAIN64     0x07
#define AGAIN128    0x08
#define AGAIN256    0x09
#define AGAIN512    0x0a

#define CFG3            0xac    /* CFG3 Register */
// Sleep after interrupt.
#define SAI 0x10

#define CFG6            0xaf    /* CFG6 Register */
// SMUX command. Selects the SMUX command to execute when setting SMUXEN gets set.
#define SMUX_CMD_ROM_INIT       0x00
#define SMUX_CMD_READ_CONFIG    0x08
#define SMUX_CMD_WRITE_CONFIG   0x10

#define CFG8            0xb1    /* CFG8 Register */
// FIFO Threshold.
#define FIFO_TH1    0x00
#define FIFO_TH4    0x40
#define FIFO_TH8    0x80
#define FIFO_TH16   0xc0
// Flicker Detect AGC Enable.
#define FD_AGC      0x08
// Spectral AGC enable.
#define SP_AGC      0x04

#define CFG9            0xb2    /*  */
// System Interrupt Flicker Detection.
#define SIEN_FD     0x40
// System Interrupt SMUX Operation.
#define SIEN_SMUX   0x10

#define CFG10           0xb3    /* CFG10 Register */
// AGC High Hysteresis.
#define AGC_H_50PERCENT 0x00
#define AGC_H_62PERCENT 0x40
#define AGC_H_75PERCENT 0x80
#define AGC_H_87PERCENT 0xc0
// AGC Low Hysteresis.
#define AGC_L_12PERCENT 0x00
#define AGC_L_25PERCENT 0x10
#define AGC_L_37PERCENT 0x20
#define AGC_L_50PERCENT 0x30
// Flicker Detect Persistence.
#define FD_PERS_MSK     0x07

#define CFG12           0xb5    /* CFG12 Register */
// Spectral Threshold Channel.
#define SP_TH_CH0   0x0
#define SP_TH_CH1   0x1
#define SP_TH_CH2   0x2
#define SP_TH_CH3   0x3
#define SP_TH_CH4   0x4

#define PERS            0xbd    /* Spectral Interrupt Persistence. */
// Defines a filter for the number of consecutive occurrences
#define APERS_EVERY 0x00
#define APERS1      0x01
#define APERS2      0x02
#define APERS3      0x03
#define APERS5      0x04
#define APERS10     0x05
#define APERS15     0x06
#define APERS20     0x07
#define APERS25     0x08
#define APERS30     0x09
#define APERS35     0x0a
#define APERS40     0x0b
#define APERS45     0x0c
#define APERS50     0x0d
#define APERS55     0x0e
#define APERS60     0x0f

#define AGC_GAIN_MAX    0xcf    /* AGC_GAIN_MAX Register */
// Flicker Detection AGC Gain Max.
#define AGC_FD_GAIN_MAX_MSK     0xf0
#define AGC_FD_GAIN_MAX_SFT     4
#define AGC_FD_GAIN_MAX_SET(n)  ((n<<AGC_FD_GAIN_MAX_SFT)&AGC_FD_GAIN_MAX_MSK)
// AGC Gain Max.
#define AGC_AGAIN_MAX_MSK       0x0f
#define AGC_AGAIN_MAX_SET(n)    (n&AGC_AGAIN_MAX_MSK)

#define AZ_CONFIG       0xd6    /* AUTOZERO FREQUENCY. */
// Sets the frequency at which the device performs auto zero of the spectral engines.
#define AZ_NTH_ITERATION_NEVER  0x00

#define INTENAB         0xf9    /* INTENAB Register */
// Spectral and Flicker Detect Saturation Interrupt Enable.
#define ASIEN   0x80
// Spectral Interrupt Enable.
#define SP_IEN  0x08
// FIFO Buffer Interrupt Enable.
#define F_IEN   0x04
// System Interrupt Enable.
#define SIEN    0x01

#define CONTROL         0xfa    /* CONTROL Register */
// Spectral Engine Manual Autozero.
#define SP_MAN_AZ       0x4
// FIFO Buffer Clear. Clears all FIFO data, FINT, FIFO_OV, and FIFO_LVL.
#define FIFO_CLR        0x2
// Clear Sleep-After-Interrupt Active.
#define CLEAR_SAI_ACT   0x1

#define FIFO_MAP        0xfc    /* FIFO_MAP Register */
// FIFO write CH5 Data.
#define FIFO_WRITE_CH5_DATA 0x40
// FIFO write CH4 Data.
#define FIFO_WRITE_CH4_DATA 0x20
// FIFO write CH3 Data.
#define FIFO_WRITE_CH3_DATA 0x10
// FIFO write CH2 Data.
#define FIFO_WRITE_CH2_DATA 0x08
// FIFO write CH1 Data.
#define FIFO_WRITE_CH1_DATA 0x04
// FIFO write CH0 Data.
#define FIFO_WRITE_CH0_DATA 0x02
// FIFO write Status.
#define FIFO_WRITE_ASTATUS  0x01

#define FD_CFG0         0xd7    /* FIFO_CFG0 Register */
// FIFO write Flicker Detection
#define FIFO_WRITE_FD   0x80

#define FIFO_LVL        0xfd    /* FIFO Buffer Level. */
// Indicates the number of entries (each are 2 bytes) available

#define FDATA_L         0xfe    /* FIFO Buffer Data */
#define FDATA_H         0xff    /* FIFO Buffer Data */

#endif
