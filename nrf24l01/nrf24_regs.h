#ifndef H_NRF24_REGS
#define H_NRF24_REGS
/*
 * Cheapest radiomodule NRF24L01 library, register definitions
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*** commands ***/
#define W_REGISTER          0x20    // Write command and status registers.
#define R_RX_PAYLOAD        0x61    // Read RX-payload: 1 – 32 bytes.
#define W_TX_PAYLOAD        0xa0    // Write TX-payload: 1 – 32 bytes.
#define FLUSH_TX            0xe1    // Flush TX FIFO, used in TX mode
#define FLUSH_RX            0xe2    // Flush RX FIFO, used in RX mode
#define REUSE_TX_PL         0xe3    // Reuse last transmitted payload.
#define R_RX_PL_WID         0x60    // Read RX payload width for the top
#define W_ACK_PAYLOAD       0xa8    // Write Payload to be transmitted together with ACK packet
#define W_TX_PAYLOAD_NOACK  0xb0    // Disables AUTOACK on this specific packet.
#define NOP                 0xff    // No Operation.
// pipe numbers
#define PIPE0   0x00
#define PIPE1   0x01
#define PIPE2   0x02
#define PIPE3   0x03
#define PIPE4   0x04
#define PIPE5   0x05

/*** registers ***/
#define CONFIG      0x00 /* Configuration Register */
// Mask interrupt caused by RX_DR
#define MASK_RX_DR  0x40
// Mask interrupt caused by TX_DS
#define MASK_TX_DS  0x20
// Mask interrupt caused by MAX_RT
#define MASK_MAX_RT 0x10
// Enable CRC.
#define EN_CRC      0x08
// CRC encoding scheme
#define CRCO        0x04
// 1: POWER UP, 0:POWER DOWN
#define PWR_UP      0x02
// RX/TX control
#define PRIM_RX     0x01

#define EN_AA       0x01 /* Enable ‘Auto Acknowledgment’ Function Disable */
// Enable auto acknowledgement data pipe 0 - 5
#define ENAA_P5 0x20
#define ENAA_P4 0x10
#define ENAA_P3 0x08
#define ENAA_P2 0x04
#define ENAA_P1 0x02
#define ENAA_P0 0x01

#define EN_RXADDR   0x02 /* Enabled RX Addresses */
// Enable data pipe 0 - 5.
#define ERX_P5  0x20
#define ERX_P4  0x10
#define ERX_P3  0x08
#define ERX_P2  0x04
#define ERX_P1  0x02
#define ERX_P0  0x01

#define SETUP_AW    0x03 /* Setup of Address Widths */
// RX/TX Address field width
#define AW3BYTES    0x01
#define AW4BYTES    0x02
#define AW5BYTES    0x03

#define SETUP_RETR  0x04 /* Setup of Automatic Retransmission */
// Auto Retransmit Delay
#define ARD250US    0x00
#define ARD500US    0x10
#define ARD750US    0x20
#define ARD1000US   0x30
#define ARD1250US   0x40
#define ARD1500US   0x50
#define ARD1750US   0x60
#define ARD2000US   0x70
#define ARD2250US   0x80
#define ARD2500US   0x90
#define ARD2750US   0xa0
#define ARD3000US   0xb0
#define ARD3250US   0xc0
#define ARD3500US   0xd0
#define ARD3750US   0xe0
#define ARD4000US   0xf0
// Auto Retransmit Count
#define ARC_DIS     0x00
#define ARC1RETX    0x01
#define ARC2RETX    0x02
#define ARC3RETX    0x03
#define ARC4RETX    0x04
#define ARC5RETX    0x05
#define ARC6RETX    0x06
#define ARC7RETX    0x07
#define ARC8RETX    0x08
#define ARC9RETX    0x09
#define ARC10RETX   0x0a
#define ARC11RETX   0x0b
#define ARC12RETX   0x0c
#define ARC13RETX   0x0d
#define ARC14RETX   0x0e
#define ARC15RETX   0x0f

#define RF_CH       0x05 /* RF Channel */
// Sets the frequency channel nRF24L01+ operates on
#define RF_CH_MSK   0x7f

#define RF_SETUP    0x06 /* RF Setup Register */
// Enables continuous carrier transmit when high.
#define CONT_WAVE       0x80
// Force PLL lock signal. Only used in test
#define PLL_LOCK        0x10
// Select between speed data rates.
#define RF_DR1M    0x00
#define RF_DR2M    0x20
#define RF_DR250K  0x08
#define RF_DR_LOW  0x20
#define RF_DR_HIGH 0x08
// Set RF output power in TX mode
#define RF_PWR_M18DBM   0x00
#define RF_PWR_M12DBM   0x02
#define RF_PWR_M6DBM    0x04
#define RF_PWR_0DBM     0x06

#define STATUS      0x07 /* Status Register */
// Data Ready RX FIFO interrupt.
#define RX_DR       0x40
// Data Sent TX FIFO interrupt.
#define TX_DS       0x20
// Maximum number of TX retransmits interrupt
#define MAX_RT      0x10
// Data pipe number for the payload available for reading from RX_FIFO
#define RX_P_NO0    0x00
#define RX_P_NO1    0x02
#define RX_P_NO2    0x04
#define RX_P_NO3    0x06
#define RX_P_NO4    0x08
#define RX_P_NO5    0x09
#define RX_P_EMPTY  0x0e
#define RX_P_NO_MSK 0x0e
// TX FIFO full flag.
#define TX_FULL 0x01

#define OBSERVE_TX  0x08 /* Transmit observe register */
// Count lost packets.
#define PLOS_CNT_MSK    0xf0
#define PLOS_CNT_SFT    4
// Count retransmitted packets.
#define ARC_CNT_MSK     0x0f

#define RPD         0x09 /* Received Power Detector. */
// Received Power Detector.
#define RPD_BIT 0x01

#define RX_ADDR_P0  0x0a /* Receive address data pipe 0. 5 Bytes maximum length. */
#define RX_ADDR_P1  0x0b /* Receive address data pipe 0. 5 Bytes maximum length. */
#define RX_ADDR_P2  0x0c /* Receive address data pipe 0. 5 Bytes maximum length. */
#define RX_ADDR_P3  0x0d /* Receive address data pipe 0. 5 Bytes maximum length. */
#define RX_ADDR_P4  0x0e /* Receive address data pipe 0. 5 Bytes maximum length. */
#define RX_ADDR_P5  0x0f /* Receive address data pipe 0. 5 Bytes maximum length. */
#define TX_ADDR     0x10 /* Transmit address. */

#define RX_PW_P0    0x11 /* Number of bytes in RX payload in data pipe 0 (1 to 32 bytes). */
// Number of bytes in RX payload in data pipe 0 (1 to 32 bytes).
#define RX_PW_MSK   0x3f
#define RX_PW_P1    0x12 /* Number of bytes in RX payload in data pipe 0 (1 to 32 bytes). */
#define RX_PW_P2    0x13 /* Number of bytes in RX payload in data pipe 0 (1 to 32 bytes). */
#define RX_PW_P3    0x14 /* Number of bytes in RX payload in data pipe 0 (1 to 32 bytes). */
#define RX_PW_P4    0x15 /* Number of bytes in RX payload in data pipe 0 (1 to 32 bytes). */
#define RX_PW_P5    0x16 /* Number of bytes in RX payload in data pipe 0 (1 to 32 bytes). */

#define FIFO_STATUS 0x17 /* FIFO Status Register */
// Used for a PTX device
#define TX_REUSE    0x40
// TX FIFO full flag.
#define FIFO_STATUS_TX_FULL 0x20
// TX FIFO empty flag.
#define TX_EMPTY    0x10
// RX FIFO full flag.
#define RX_FULL     0x02
// RX FIFO empty flag.
#define RX_EMPTY    0x01

#define DYNPD       0x1c /* Enable dynamic payload length */
// Enable dynamic payload length data pipe 5 - 0.
#define DPL_P5  0x20
#define DPL_P4  0x10
#define DPL_P3  0x08
#define DPL_P2  0x04
#define DPL_P1  0x02
#define DPL_P0  0x01

#define FEATURE     0x1d /* Feature Register */
// Enables Dynamic Payload Length
#define EN_DPL      0x04
// Enables Payload with ACK
#define EN_ACK_PAY  0x02
// Enables the W_TX_PAYLOAD_NOACK command
#define EN_DYN_ACK  0x01

#endif
