#ifndef H_ST7735
#define H_ST7735
/*
 * ST7735 128x160 graphic display driver support library.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// display size
#define LINES   160
#define COLUMNS 128

/* No Operation */
#define NOP         0x00

/* Software reset */
#define SWRESET     0x01

/* Read Display ID */
#define RDDID       0x04
#define RDDID_SIZE  4
// The 1st parameter is dummy data
// The 2nd parameter (ID17 to ID10): LCD module’s manufacturer ID.
// The 3rd parameter (ID26 to ID20): LCD module/driver version ID
#define LCD_VERSION_ID_MSK  0x7f
// The 4th parameter (ID37 to UD30): LCD module/driver ID.

/* Read Display Status */
#define RDDST       0x09
#define RDDST_SIZE  5
// Booster Voltage Status
#define BSTON       0x80
// Row Address Order (MY)
#define MY          0x40
// Column Address Order (MX)
#define MX          0x20
// Row/Column Exchange (MV)
#define MV          0x10
// Scan Address Order (ML)
#define ML          0x08
// RGB/ BGR Order (RGB)
#define RGB         0x04
// Horizontal Order
#define MH          0x02
// Interface Color Pixel Format Definition
#define IFPF12BIT   0x30
#define IFPF16BIT   0x50
#define IFPF18BIT   0x60
// Idle Mode On/Off
#define IDMON       0x08
// Partial Mode On/Off
#define PTLON       0x04
// Sleep In/Out
#define SLPOUT      0x02
// Display Normal Mode On/Off
#define NORON       0x01
// Inversion Status
#define INVON       0x20
// Display On/Off
#define DISON       0x04
// Tearing effect line on/off
#define TEON        0x02
// Gamma Curve Selection
#define GCSEL0      0x40
#define GCSEL1      0x80
#define GCSEL2      0x01
// Tearing effect line mode
#define TELOM       0x20

/* Read Display Power */
#define RDDPM       0x0a
#define RDDPM_SIZE  2
// Booster Voltage Status
#define BSTON   0x80
// Idle Mode On/Off
#define IDMON   0x40
// Partial Mode On/Off
#define PTLON   0x20
// Sleep In/Out
#define SLPON   0x10
// Display Normal ModemOn/Off
#define NORON   0x08
// Display On/Off
#define DISON   0x04

/* Read Display */
#define RDDMADCTL   0x0b
#define RDDMADCTL_SIZE  2
// Column Address Order
#define MX  0x80
// Row Address Order
#define MY  0x40
// Row/Column Order (MV)
#define MV  0x20
// Vertical Refresh Order
#define ML  0x10
// RGB/BGR Order
#define RGB 0x08
// Horizontal Refresh Order
#define MH  0x04

/* Read Display Pixel */
#define RDDCOLMOD   0x0c
#define RDDCOLMOD_SIZE  2
// MCU Interface Color Format
#define IFPF12BIT   0x03
#define IFPF16BIT   0x05
#define IFPF18BIT   0x06

/* Read Display Image */
#define RDDIM       0x0d
#define RDDIM_SIZE  2
// Inversion On/Off
#define INVON   0x20
// Gamma Curve Selection
#define GCS0    0x00
#define GCS1    0x01
#define GCS2    0x02
#define GCS3    0x03

/* Read Display Signal */
#define RDDSM       0x0e
#define RDDSM_SIZE  2
// Tearing Effect Line On/Off
#define TEON    0x80
// Tearing effect line mode
#define TELOM   0x40

/* Sleep in & booster off */
#define SLPIN       0x10
#define SLPIN_SIZE  0

/* Sleep out & booster on */
#define SLPOUT      0x11
#define SLPOUT_SIZE 0

/* Partial mode on */
#define PTLON       0x12
#define PTLON_SIZE  0

/* Partial off (Normal) */
#define NORON       0x13
#define NORON_SIZE  0

/* Display inversion off */
#define INVOFF      0x20
#define INVOFF_SIZE 0

/* Display inversion on */
#define INVON       0x21
#define INVON_SIZE  0

/* Gamma curve select */
#define GAMSET      0x26
#define GAMSET_SIZE 1
// select the desired Gamma curve
#define GC0 0x01
#define GC1 0x02
#define GC2 0x04
#define GC3 0x08

/* Display off */
#define DISPOFF     0x28
#define DISPOFF_SIZE    0

/* Display on */
#define DISPON      0x29
#define DISPON_SIZE 0

/* Column address set */
#define CASET       0x2a
#define CASET_SIZE  4

/* Row address set */
#define RASET       0x2b
#define RASET_SIZE  4

/* Memory write */
#define RAMWR       0x2c
//#define RAMWR_SIZE(n)

/* Memory read */
#define RAMRD       0x2e
//#define RAMRD_SIZE(n)

/* Partial start/end address set */
#define PTLAR       0x30
#define PTLAR_SIZE  4

/* Tearing effect line off */
#define TEOFF       0x34
#define TEOFF_SIZE  0

/* Tearing effect mode set & on */
#define TEON        0x35
#define TEON_SIZE   1
// ON the Tearing Effect output signal from the TE signal line
#define TELOM   0x01

/* Memory data access control */
#define MADCTL      0x36
#define MADCTL_SIZE 1
// Row Address Order
#define MY  0x80
// Column Address Order
#define MX  0x40
// Row/Column Exchange
#define MV  0x20
// Vertical Refresh Order
#define ML  0x10
// RGB-BGR ORDER
#define RGB 0x08
// Horizontal Refresh Order
#define MH  0x04

/* Idle mode off */
#define IDMOFF      0x38
#define IDMOFF_SIZE 0

/* Idle mode on */
#define IDMON       0x39
#define IDMON_SIZE  0

/* Interface pixel format */
#define COLMOD      0x3a
#define COLMOD_SIZE 1
// MCU Interface Color Format
#define IFPF12BIT   0x03
#define IFPF16BIT   0x05
#define IFPF18BIT   0x06

/* Read ID1 */
#define RDID1       0xda
#define RDID1_SIZE  2

/* Read ID2 */
#define RDID2       0xdb
#define RDID2_SIZE  2

/* Read ID3 */
#define RDID3       0xdc
#define RDID3_SIZE  2


/*** Panel Function Command List and Description ***/

/* In normal mode (Full colors) */
#define FRMCTR1     0xb1
#define FRMCTR1_SIZE    3
// Set the frame frequency of the full colors normal mode.
#define FOSC    333000
#define RTNA        0
#define FPA         0
#define BPA         0
#define RTNA_MSK    0x0f
#define FPA_MSK     0x3f
#define BPA_MSK     0x3f
#define RATE        FOSC/((RTNA+20)*(LINES+FPA+BPA))

/* In Idle mode (8-colors) */
#define FRMCTR2     0xb2
#define FRMCTR2_SIZE    3
// Set the frame frequency of the Idle mode.
#define RTNB        0
#define FPB         0
#define BPB         0
#define RTNB_MSK    0x0f
#define FPB_MSK     0x3f
#define BPB_MSK     0x3f
#define RATE_IDLE   FOSC/((RTNB+20)*(LINES+FPB+BPB))

/* In partial mode + Full colors */
#define FRMCTR3     0xb3
#define FRMCTR3_SIZE    6
// in line inversion mode
#define RTNC        0
#define FPC         0
#define BPC         0
#define RTNC_MSK    0x0f
#define FPC_MSK     0x3f
#define BPC_MSK     0x3f
#define RATE_LINE   FOSC/((RTNC+20)*(LINES+FPC+BPC))
// in frame inversion mode
#define RTND        0
#define FPD         0
#define BPD         0
#define RTND_MSK    0x0f
#define FPD_MSK     0x3f
#define BPD_MSK     0x3f
#define RATE_FRAME  FOSC/((RTND+20)*(LINES+FPD+BPD))

/* Display inversion control */
#define INVCTR      0xb4
#define INVCTR_SIZE 1
// Inversion setting in full Colors normal mode
#define NLA 0x04
// Inversion setting in Idle mode
#define NLB 0x02
// Inversion setting in full Colors partial mode
#define NLC 0x01

/* Display function setting */
#define DISSET5     0xb6
#define DISSET5_SIZE    2
// Set the amount of non-overlap of the gate output
#define NO_1CYCLE   0x00
#define NO_2CYCLE   0x10
#define NO_4CYCLE   0x20
#define NO_6CYCLE   0x30
// Set delay amount from gate signal rising edge of the source output.
#define SDT_0CYCLE  0x00
#define SDT_1CYCLE  0x04
#define SDT_2CYCLE  0x08
#define SDT_3CYCLE  0x0c
// Set the Equalizing period
#define EQ_NO       0x00
#define EQ_3CYCLE   0x01
#define EQ_5CYCLE   0x02
#define EQ_7CYCLE   0x03
// Determine gate output in a non-display area in the partial mode
#define PTG_NORMAL  0x00
#define PTG_VGL     0x04
// Determine Source /VCOM output in a non-display area in the partial mode
#define PT0         0x00
#define PT1         0x01
#define PT2         0x02
#define PT3         0x03

/* Power control setting */
#define PWCTR1      0xc0
#define PWCTR1_SIZE 2
// Set the GVDD voltage
#define VRH5V       0x00
#define VRH4P75V    0x01
#define VRH4P7V     0x02
#define VRH4P65V    0x03
#define VRH4P6V     0x04
#define VRH4P55V    0x05
#define VRH4P5V     0x06
#define VRH4P45V    0x07
#define VRH4P4V     0x08
#define VRH4P35V    0x09
#define VRH4P3V     0x0a
#define VRH4P25V    0x0b
#define VRH4P2V     0x0c
#define VRH4P15V    0x0d
#define VRH4P1V     0x0e
#define VRH4P05V    0x0f
#define VRH4V       0x10
#define VRH3P95V    0x11
#define VRH3P9V     0x12
#define VRH3P85V    0x13
#define VRH3P8V     0x14
#define VRH3P75V    0x15
#define VRH3P7V     0x16
#define VRH3P65V    0x17
#define VRH3P6V     0x18
#define VRH3P55V    0x19
#define VRH3P5V     0x1a
#define VRH3P45V    0x1b
#define VRH3P4V     0x1c
#define VRH3P35V    0x1d
#define VRH3P25V    0x1e
#define VRH3V       0x1f
//
#define IB_SEL2P5UA 0x40
#define IB_SEL2UA   0x50
#define IB_SEL1P5UA 0x60
#define IB_SEL1UA   0x70

/* Power control setting */
#define PWCTR2      0xc1
#define PWCTR2_SIZE 1
// Set the VGH and VGL supply power level
#define BT_VGH_4X   0x00
#define BT_VGH_5X   0x02
#define BT_VGH_6X   0x05
#define BT_VGL_M3X  0x00
#define BT_VGL_M4X  0x01
#define BT_VGL_M5X  0x04

/* In normal mode (Full colors) */
#define PWCTR3      0xc2
#define PWCTR3_SIZE 2
// Amount of Current in Operational Amplifier
#define AP_STOP         0x00
#define AP_SMALL        0x01
#define AP_MEDIUM_LOW   0x02
#define AP_MEDIUM       0x03
#define AP_MEDIUM_HIGH  0x04
#define AP_LARGE        0x05
// Booster circuit Step-up cycle in Normal mode/ full colors.
#define DC_BCLK1_DIV1   0x00
#define DC_BCLK1_DIV2   0x03
#define DC_BCLK1_DIV4   0x05
#define DC_BCLK2_DIV1   0x00
#define DC_BCLK2_DIV2   0x01
#define DC_BCLK2_DIV4   0x02
#define DC_BCLK2_DIV8   0x06
#define DC_BCLK2_DIV16  0x07

/* In Idle mode (8-colors) */
#define PWCTR4      0xc3
#define PWCTR4_SIZE 2

/* In partial mode + Full colors */
#define PWCTR5      0xc4
#define PWCTR5_SIZE 2

/* VCOM control 1 */
#define VMCTR1      0xc5
#define VMCTR1_SIZE 2
// Set VCOMH Voltage
#define VMH2P5V     0x00
#define VMH2P525V   0x01
#define VMH2P55V    0x02
#define VMH2P575V   0x03
#define VMH2P6V     0x04
#define VMH2P625V   0x05
#define VMH2P65V    0x06
#define VMH2P675V   0x07
#define VMH2P7V     0x08
#define VMH2P725V   0x09
#define VMH2P75V    0x0a
#define VMH2P775V   0x0b
#define VMH2P8V     0x0c
#define VMH2P825V   0x0d
#define VMH2P85V    0x0e
#define VMH2P875V   0x0f
#define VMH2P9V     0x10
#define VMH2P925V   0x11
#define VMH2P95V    0x12
#define VMH2P975V   0x13
#define VMH3V       0x14
#define VMH3P025V   0x15
#define VMH3P05V    0x16
#define VMH3P075V   0x17
#define VMH3P1V     0x18
#define VMH3P125V   0x19
#define VMH3P15V    0x1a
#define VMH3P175V   0x1b
#define VMH3P2V     0x1c
#define VMH3P225V   0x1d
#define VMH3P25V    0x1e
#define VMH3P275V   0x1f
#define VMH3P3V     0x20
#define VMH3P325V   0x21
#define VMH3P350V   0x22
#define VMH3P375V   0x23
#define VMH3P4V     0x24
#define VMH3P425V   0x25
#define VMH3P45V    0x26
#define VMH3P475V   0x27
#define VMH3P5V     0x28
#define VMH3P525V   0x29
#define VMH3P55V    0x2a
#define VMH3P575V   0x2b
#define VMH3P6V     0x2c
#define VMH3P625V   0x2d
#define VMH3P65V    0x2e
#define VMH3P675V   0x2f
#define VMH3P7V     0x30
#define VMH3P725V   0x31
#define VMH3P75V    0x32
#define VMH3P775V   0x33
#define VMH3P8V     0x34
#define VMH3P825V   0x35
#define VMH3P85V    0x36
#define VMH3P875V   0x37
#define VMH3P9V     0x38
#define VMH3P925V   0x39
#define VMH3P95V    0x3a
#define VMH3P975V   0x3b
#define VMH4V       0x3c
#define VMH4P025V   0x3d
#define VMH4P05V    0x3e
#define VMH4P075V   0x3f
#define VMH4P1V     0x40
#define VMH4P125V   0x41
#define VMH4P15V    0x42
#define VMH4P175V   0x43
#define VMH4P2V     0x44
#define VMH4P225V   0x45
#define VMH4P25V    0x46
#define VMH4P275V   0x47
#define VMH4P3V     0x48
#define VMH4P325V   0x49
#define VMH4P35V    0x4a
#define VMH4P375V   0x4b
#define VMH4P4V     0x4c
#define VMH4P425V   0x4d
#define VMH4P45V    0x4e
#define VMH4P475V   0x4f
#define VMH4P5V     0x50
#define VMH4P525V   0x51
#define VMH4P55V    0x52
#define VMH4P575V   0x53
#define VMH4P6V     0x54
#define VMH4P625V   0x55
#define VMH4P65V    0x56
#define VMH4P675V   0x57
#define VMH4P7V     0x58
#define VMH4P725V   0x59
#define VMH4P75V    0x5a
#define VMH4P775V   0x5b
#define VMH4P8V     0x5c
#define VMH4P825V   0x5d
#define VMH4P85V    0x5e
#define VMH4P875V   0x5f
#define VMH4P9V     0x60
#define VMH4P925V   0x61
#define VMH4P95V    0x62
#define VMH4P975V   0x63
#define VMH5V       0x64
// -Set VCOML Voltage
#define VML_M2P4    0x04
#define VML_M2P375  0x05
#define VML_M2P35   0x06
#define VML_M2P325  0x07
#define VML_M2P3    0x08
#define VML_M2P275  0x09
#define VML_M2P25   0x0a
#define VML_M2P225  0x0b
#define VML_M2P2    0x0c
#define VML_M2P175  0x0d
#define VML_M2P15   0x0e
#define VML_M2P125  0x0f
#define VML_M2P1    0x10
#define VML_M2P075  0x11
#define VML_M2P05   0x12
#define VML_M2P025  0x13
#define VML_M2      0x14
#define VML_M1P975  0x15
#define VML_M1P95   0x16
#define VML_M1P925  0x17
#define VML_M1P9    0x18
#define VML_M1P875  0x19
#define VML_M1P85   0x1a
#define VML_M1P825  0x1b
#define VML_M1P8    0x1c
#define VML_M1P775  0x1d
#define VML_M1P75   0x1e
#define VML_M1P725  0x1f
#define VML_M1P7    0x20
#define VML_M1P675  0x21
#define VML_M1P65   0x22
#define VML_M1P625  0x23
#define VML_M1P6    0x24
#define VML_M1P575  0x25
#define VML_M1P55   0x26
#define VML_M1P525  0x27
#define VML_M1P5    0x28
#define VML_M1P475  0x29
#define VML_M1P45   0x2a
#define VML_M1P425  0x2b
#define VML_M1P4    0x2c
#define VML_M1P375  0x2d
#define VML_M1P35   0x2e
#define VML_M1P325  0x2f
#define VML_M1P3    0x30
#define VML_M1P275  0x31
#define VML_M1P25   0x32
#define VML_M1P225  0x33
#define VML_M1P2    0x34
#define VML_M1P175  0x35
#define VML_M1P15   0x36
#define VML_M1P125  0x37
#define VML_M1P1    0x38
#define VML_M1P075  0x39
#define VML_M1P05   0x3a
#define VML_M1P025  0x3b
#define VML_M1      0x3c
#define VML_M0P975  0x3d
#define VML_M0P95   0x3e
#define VML_M0P925  0x3f
#define VML_M0P9    0x40
#define VML_M0P875  0x41
#define VML_M0P85   0x42
#define VML_M0P825  0x43
#define VML_M0P8    0x44
#define VML_M0P775  0x45
#define VML_M0P75   0x46
#define VML_M0P725  0x47
#define VML_M0P7    0x48
#define VML_M0P675  0x49
#define VML_M0P65   0x4a
#define VML_M0P625  0x4b
#define VML_M0P6    0x4c
#define VML_M0P575  0x4d
#define VML_M0P55   0x4e
#define VML_M0P525  0x4f
#define VML_M0P5    0x50
#define VML_M0P475  0x51
#define VML_M0P45   0x52
#define VML_M0P425  0x53
#define VML_M0P4    0x54
#define VML_M0P375  0x55
#define VML_M0P35   0x56
#define VML_M0P325  0x57
#define VML_M0P3    0x58
#define VML_M0P275  0x59
#define VML_M0P25   0x5a
#define VML_M0P225  0x5b
#define VML_M0P2    0x5c
#define VML_M0P175  0x5d
#define VML_M0P15   0x5e
#define VML_M0P125  0x5f
#define VML_M0P1    0x60
#define VML_M0P075  0x61
#define VML_M0P05   0x62
#define VML_M0P025  0x63
#define VML_0       0x64

/* Set VCOM offset control */
#define VMOFCTR     0xc7
#define VMOFCTR_SIZE    1
// Set VCOM Voltage level for reduce the flicker issue
#define VMF0V       0x00
#define VMF0P025V   0x01
#define VMF0P050V   0x02
#define VMF0P075V   0x03
#define VMF0P1V     0x04
#define VMF0P125V   0x05
#define VMF0P15V    0x06
#define VMF0P175V   0x07
#define VMF0P2V     0x08
#define VMF0P225V   0x09
#define VMF0P25V    0x0a
#define VMF0P275V   0x0b
#define VMF0P3V     0x0c
#define VMF0P325V   0x0d
#define VMF0P35V    0x0e
#define VMF0P375V   0x0f
#define VMF0P4V     0x10
#define VMF0P425V   0x11
#define VMF0P45V    0x12
#define VMF0P475V   0x13
#define VMF0P5V     0x14
#define VMF0P525V   0x15
#define VMF0P55V    0x16
#define VMF0P575V   0x17
#define VMF0P6V     0x18
#define VMF0P625V   0x19
#define VMF0P65V    0x1a
#define VMF0P675V   0x1b
#define VMF0P7V     0x1c
#define VMF0P725V   0x1d
#define VMF0P75V    0x1e
#define VMF0P775V   0x1f

/* Set LCM version code */
#define WRID2       0xd1
#define WRID2_SIZE  1
// LCD Module version ID.
#define ID_MSK  0x7f

/* Customer Project code */
#define WRID3       0xd2
#define WRID3_SIZE  1
// product project ID.
#define

/* In partial mode + Idle */
#define PWCTR6      0xfc
#define PWCTR6_SIZE 2
// amount of current in Operational amplifier in Partial mode + Idle mode
#define PWCTR6_1    0x11
#define PWCTR6_2    0x15

/* EEPROM control status */
#define NVCTR1      0xd9
#define NVCTR1_SIZE 1
// EEPROM control status
#define VMF_EN  0x20
#define ID2_EN  0x10

/* EEPROM Read Command */
#define NVCTR2      0xde
#define NVCTR2_SIZE 1

/* EEPROM Write Command */
#define NVCTR3      0xdf
#define NVCTR3_SIZE 3
// Select Command
#define EE_IBC7         0xc7
#define EE_IBD1         0xd1
#define EE_IBD2         0xd2
// Select to Program/Erase
#define EE_CMD_PROGRAM  0x3a
#define EE_CMD_ERASE    0xc5

/* Gamma adjustment (+ polarity) */
#define GAMCTRP1    0xe0
#define GAMCTRP1_SIZE   16
// Variable resistor VRHP
// Variable resistor VRLP
// The voltage of V3 grayscale is selected by the 64 to 1 selector
// The voltage of V6 grayscale is selected by the 64 to 1 selector
// The voltage of V11 grayscale is selected by the 64 to 1 selector
// The voltage of V19 grayscale is selected by the 64 to 1 selector
// The voltage of V27 grayscale is selected by the 64 to 1 selector
// The voltage of V36 grayscale is selected by the 64 to 1 selector
// The voltage of V44 grayscale is selected by the 64 to 1 selector
// The voltage of V52 grayscale is selected by the 64 to 1 selector
// The voltage of V57 grayscale is selected by the 64 to 1 selector
// The voltage of V60 grayscale is selected by the 64 to 1 selector
// The voltage of V0 grayscale is selected by the 64 to 1 selector
// The voltage of V1 grayscale is selected by the 64 to 1 selector
// The voltage of V62 grayscale is selected by the 64 to 1 selector
// The voltage of V63 grayscale is selected by the 64 to 1 selector
#define GAMMA_MSK   0x3f

/* Gamma adjustment (- polarity) */
#define GAMCTRN1    0xe1
#define GAMCTRN1_SIZE   16
// Variable resistor VRHN
// Variable resistor VRLN
// The voltage of V3 grayscale is selected by the 64 to 1 selector
// The voltage of V6 grayscale is selected by the 64 to 1 selector
// The voltage of V11 grayscale is selected by the 64 to 1 selector
// The voltage of V19 grayscale is selected by the 64 to 1 selector
// The voltage of V27 grayscale is selected by the 64 to 1 selector
// The voltage of V36 grayscale is selected by the 64 to 1 selector
// The voltage of V44 grayscale is selected by the 64 to 1 selector
// The voltage of V52 grayscale is selected by the 64 to 1 selector
// The voltage of V57 grayscale is selected by the 64 to 1 selector
// The voltage of V60 grayscale is selected by the 64 to 1 selector
// The voltage of V0 grayscale is selected by the 64 to 1 selector
// The voltage of V1 grayscale is selected by the 64 to 1 selector
// The voltage of V62 grayscale is selected by the 64 to 1 selector
// The voltage of V63 grayscale is selected by the 64 to 1 selector

/* Extension Command Control */
#define EXTCTRL     0xf0
#define EXTCTRL_SIZE    1
// When EXTC PIN =”L”, this command will enable extension command.

/* Vcom 4 Level control */
#define VCOM4L      0xff
#define VCOM4L_SIZE 3
// delay time
#define TC1_0CLOCK  0x00
#define TC1_1CLOCK  0x01
#define TC1_2CLOCK  0x02
#define TC1_3CLOCK  0x03
#define TC1_4CLOCK  0x04
#define TC1_5CLOCK  0x05
#define TC1_6CLOCK  0x06
#define TC1_7CLOCK  0x07
#define TC1_8CLOCK  0x08
#define TC1_9CLOCK  0x09
#define TC1_10CLOCK 0x0a
#define TC1_11CLOCK 0x0b
#define TC1_12CLOCK 0x0c
#define TC1_13CLOCK 0x0d
#define TC1_14CLOCK 0x0e
#define TC1_15CLOCK 0x0f
// delay time
#define TC2_0CLOCK  0x00
#define TC2_1CLOCK  0x10
#define TC2_2CLOCK  0x20
#define TC2_3CLOCK  0x30
#define TC2_4CLOCK  0x40
#define TC2_5CLOCK  0x50
#define TC2_6CLOCK  0x60
#define TC2_7CLOCK  0x70
#define TC2_8CLOCK  0x80
#define TC2_9CLOCK  0x90
#define TC2_10CLOCK 0xa0
#define TC2_11CLOCK 0xb0
#define TC2_12CLOCK 0xc0
#define TC2_13CLOCK 0xd0
#define TC2_14CLOCK 0xe0
#define TC2_15CLOCK 0xf0
// delay time
#define TC3_0CLOCK  0x00
#define TC3_1CLOCK  0x01
#define TC3_2CLOCK  0x02
#define TC3_3CLOCK  0x03
#define TC3_4CLOCK  0x04
#define TC3_5CLOCK  0x05
#define TC3_6CLOCK  0x06
#define TC3_7CLOCK  0x07
#define TC3_8CLOCK  0x08
#define TC3_9CLOCK  0x09
#define TC3_10CLOCK 0x0a
#define TC3_11CLOCK 0x0b
#define TC3_12CLOCK 0x0c
#define TC3_13CLOCK 0x0d
#define TC3_14CLOCK 0x0e
#define TC3_15CLOCK 0x0f

#endif
