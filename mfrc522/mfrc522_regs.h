#ifndef H_MFRC522_REGS
#define H_MFRC522_REGS
/*
 * MFRC522, a highly integrated reader/writer for contactless communication
 * at 13.56 MHz. Register definitions.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*** Page 0: Command and Status ***/
#define COMMAND_REG         0x1 /* Starts and stops command execution */
// Set to logic 1, the analog part of the receiver is switched off.
#define RCV_OFF         0x20
// Set to logic 1, Soft Power-down mode is entered.
#define POWER_DOWN      0x10
// Activates a command according to the Command Code.
#define COMMAND_MSK     0x0f
#define COMMAND_SET(n)  (n&COMMAND_MSK)

#define COML_EN_REG         0x2 /* Controls bits to enable and disable the passing of Interrupt Requests */
// signal on pin IRQ is inverted with respect to bit IRq in the register Status1Reg.
#define IRQ_INV         0x80
// Allows the transmitter interrupt request (indicated by bit TxIRq) to be propagated to pin IRQ.
#define TX_IEN          0x40
// Allows the receiver interrupt request (indicated by bit RxIRq) to be propagated to pin IRQ.
#define RX_IEN          0x20
// Allows the idle interrupt request (indicated by bit IdleIRq) to be propagated to pin IRQ.
#define IDLE_IEN        0x10
// Allows the high alert interrupt request (indicated by bit HiAlertIRq) to be propagated to pin IRQ.
#define HI_ALERT_IEN    0x08
// Allows the low alert interrupt request (indicated by bit LoAlertIRq) to be propagated to pin IRQ.
#define LO_ALERT_IEN    0x04
// Allows the error interrupt request (indicated by bit ErrIRq) to be propagated to pin IRQ.
#define ERR_IEN         0x02
// Allows the timer interrupt request (indicated by bit TimerIRq) to be propagated to pin IRQ.
#define TIMER_IEN       0x01

#define DIVL_EN_REG         0x3 /* Controls bits to enable and disable the passing of Interrupt Requests */
// Set to logic 1, the pin IRQ works as standard CMOS output pad.
#define IRQ_PUSH_PULL   0x80
// Allows the MFIN active interrupt request to be propagated to pin IRQ.
#define MFIN_ACT_IEN    0x10
// Allows the CRC interrupt request (indicated by bit CRCIRq) to be propagated to pin IRQ.
#define CRC_IEN         0x04

#define COM_IRQ_REG         0x4 /* Contains Interrupt Request bits */
// Set1 defines that the marked bits in the register CommIRqReg are set.
#define SET1            0x80
// Set to logic 1 immediately after the last bit of the transmitted data was sent out.
#define TX_IRQ          0x40
// Set to logic 1 when the receiver detects the end of a valid data stream.
#define RX_IRQ          0x20
// Set to logic 1, when a command terminates by itself
#define IDLE_IRQ        0x10
// Set to logic 1, when bit HiAlert in register Status1Reg is set.
#define HI_ALERT_IRQ    0x08
// Set to logic 1, when bit LoAlert in register Status1Reg is set.
#define LO_ALERT_IRQ    0x04
// Set to logic 1 if any error bit in the ErrorReg Register is set.
#define ERR_IRQ         0x02
// Set to logic 1 when the timer decrements the TimerValue Register to zero.
#define TIMER_IRQ       0x01

#define DIV_IRQ_REG         0x5 /* Contains Interrupt Request bits */
// Set to logic 1, Set2 defines that the marked bits in the register DivIRqReg are set.
#define SET2            0x80
// Set to logic 1, when MFIN is active.
#define MFIN_ACT_IRQ    0x10
// Set to logic 1, when the CRC command is active and all data are processed.
#define CRC_IRQ         0x04

#define ERROR_REG           0x6 /* Error bits showing the error status of the last command executed */
// Set to logic 1, when data is written into the FIFO by the host
#define WR_ERR          0x80
// Set to logic 1, if the internal temperature sensor detects overheating.
#define TEMP_ERR        0x40
// Set to logic 1, if the host or a MFRC522’s internal state machine
#define BUFFER_OVFL     0x10
// Set to logic 1, if a bit-collision is detected.
#define COLL_ERR        0x08
// Set to logic 1, if bit RxCRCEn in register RxModeReg is set and the CRC calculation fails.
#define CRC_ERR         0x04
// Set to logic 1, if the parity check has failed. It is cleared automatically at receiver start-up phase.
#define PARITY_ERR      0x02
// Set to logic 1, if one out of the following cases occur:
#define PROTOCOL_ERR    0x01

#define STATUS1REG          0x7 /* Contains status bits for communication */
// Set to logic 1, if the CRC result is zero.
#define CRC_OK      0x40
// Set to logic 1, when the CRC calculation has finished.
#define CRC_READY   0x20
// This bit shows, if any interrupt source requests attention
#define IRQ         0x10
// set to logic 1, if the MFRC522’s timer unit is running
#define TRUNNING    0x08
// Set to logic 1, when the number of bytes stored in the FIFO buffer fulfils the following eq:
#define HI_ALERT    0x02
// Set to logic 1, when the number of bytes stored in the FIFO buffer fulfils the following eq:
#define LO_ALERT    0x01

#define STATUS2REG          0x8 /* Contains status bits of the receiver and transmitter */
// Set to logic 1, this bit clears the temperature error, if t below the alarm limit of 125 °C.
#define TEMP_SENS_CLEAR             0x80
// I2C input filter settings.
#define I2C_FORCE_HS                0x40
// This bit indicates that the MIFARE® Crypto1 unit is switched on
#define MF_CRYPTO1ON                0x08
// ModemState shows the state of the transmitter and receiver state machines.
#define MODEM_STATE_MSK             0x07
#define MODEM_STATE_IDLE            0x00
#define MODEM_STATE_WAIT_START_SEND 0x01
#define MODEM_STATE_TX_WAIT         0x02
#define MODEM_STATE_TX              0x03
#define MODEM_STATE_RX_WAIT         0x04
#define MODEM_STATE_WAIT_DATA       0x05
#define MODEM_STATE_RX              0x06

#define FIFO_DATA_REG       0x9 /* In- and output of 64 byte FIFO buffer */
// Data input and output port for the internal 64 byte FIFO buffer.

#define FIFO_LEVEL_REG      0xa /* Indicates the number of bytes stored in the FIFO */
// Set to logic 1, this bit clears the internal FIFO-buffer’s read- and write-pointer
#define FLUSH_BUFFER        0x80
// Indicates the number of bytes stored in the FIFO buffer.
#define FIFO_LEVEL_MSK      0x7f
#define FIFO_LEVEL_GET(n)   (n&FIFO_LEVEL_MSK)

#define WATER_LEVEL_REG     0xb /* Defines the level for FIFO under- and overflow warning */
// This register defines a warning level to indicate a FIFO-buffer over- or underflow:
#define WATER_LEVEL_MSK 0x3f

#define CONTROL_REG         0xc /* Contains miscellaneous Control Registers */
// Set to logic 1, the timer stops immediately.
#define TSTOP_NOW           0x80
// Set to logic 1 starts the timer immediately.
#define TSTART_NOW          0x40
// Shows the number of valid bits in the last received byte. If 0, the whole byte is valid.
#define RX_LAST_BITS_MSK    0x07
#define RX_LAST_BITS_GET(n) (n&RX_LAST_BITS_MSK)

#define BIT_FRAMING_REG     0xd /* Adjustments for bit oriented frames */
// Set to logic 1, the transmission of data starts.
#define START_SEND          0x80
// Used for reception of bit oriented frames:
#define RX_ALIGN_MSK        0x70
#define RX_ALIGN0           0x00
#define RX_ALIGN1           0x10
#define RX_ALIGN2           0x20
#define RX_ALIGN3           0x30
#define RX_ALIGN4           0x40
#define RX_ALIGN5           0x50
#define RX_ALIGN6           0x60
#define RX_ALIGN7           0x70
// Used for transmission of bit oriented frames:
#define TX_LAST_BITS_MSK    0x07
#define TX_LAST_BITS_SET(n) (n&TX_LAST_BITS_MSK)

#define COLL_REG            0xe /* Bit position of the first bit collision detected on the RF-interface */
// If this bit is set to logic 0, all receiving bits will be cleared after a collision.
#define VALUES_AFTER_COLL   0x80
// 1, if no collision is detected or the position of the collision is out of the range of bits CollPos.
#define COLL_POS_NOT_VALID  0x20
// These bits show the bit position of the first detected collision in a received frame.
#define COLL_POS_MSK        0x1f
#define COLL_POS32          0x00
#define COLL_POS1           0x01
#define COLL_POS2           0x02
#define COLL_POS3           0x03
#define COLL_POS4           0x04
#define COLL_POS5           0x05
#define COLL_POS6           0x06
#define COLL_POS7           0x07
#define COLL_POS8           0x08
#define COLL_POS9           0x09
#define COLL_POS10          0x0a
#define COLL_POS11          0x0b
#define COLL_POS12          0x0c
#define COLL_POS13          0x0d
#define COLL_POS14          0x0e
#define COLL_POS15          0x0f
#define COLL_POS16          0x10
#define COLL_POS17          0x11
#define COLL_POS18          0x12
#define COLL_POS19          0x13
#define COLL_POS20          0x14
#define COLL_POS21          0x15
#define COLL_POS22          0x16
#define COLL_POS23          0x17
#define COLL_POS24          0x18
#define COLL_POS25          0x19
#define COLL_POS26          0x1a
#define COLL_POS27          0x1b
#define COLL_POS28          0x1c
#define COLL_POS29          0x1d
#define COLL_POS30          0x1e
#define COLL_POS31          0x1f



/*** Page 1: Command ***/

#define MODE_REG            0x1 /* Defines general modes for transmitting and receiving */
// Set to logic 1, the CRC co-processor calculates the CRC with MSB first
#define MSB_FIRST       0x80
// Set to logic 1 the transmitter can only be started, if an RF field is generated.
#define TX_WAIT_RF      0x20
// PolMFin defines the polarity of the MFIN pin. Set to logic 1, the polarity of MFIN pin is active high.
#define POLM_FIN        0x08
// Defines the preset value for the CRC co-processor for the command CalCRC.
#define CRC_PRESET_MSK  0x03
#define CRC_PRESET0000  0x00
#define CRC_PRESET6363  0x01
#define CRC_PRESETA671  0x02
#define CRC_PRESETFFFF  0x03

#define TX_MODE_REG         0x2 /* Defines the transmission data rate and framing */
// Set to logic 1, this bit enables the CRC generation during data transmission.
#define TX_CRC_EN           0x80
// Defines the bit rate while data transmission.
#define TX_SPEED_MSK        0x70
#define TX_SPEED106KBITS    0x00
#define TX_SPEED212KBITS    0x10
#define TX_SPEED424KBITS    0x20
#define TX_SPEED848KBITS    0x30
// Set to logic 1, the modulation for transmitting data is inverted.
#define INV_MOD             0x08

#define RX_MODE_REG         0x3 /* Defines the receive data rate and framing */
// Set to logic 1, this bit enables the CRC calculation during reception.
#define RX_CRC_EN           0x80
// Defines the bit rate while data receiving.
#define RX_SPEED_MSK        0x70
#define RX_SPEED106KBITS    0x00
#define RX_SPEED212KBITS    0x10
#define RX_SPEED424KBITS    0x20
#define RX_SPEED848KBITS    0x30
// If set to logic 1, a not valid received data stream
#define RX_NO_ERR           0x08
// Set to logic 0, the receiver is deactivated after receiving a data frame.
#define RX_MULTIPLE         0x04

#define TX_CONTROL_REG      0x4 /* Controls the logical behavior of the antenna driver pins TX1 and TX2 */
// Set to logic 1, the output signal at pin TX2 will be inverted, if driver TX2 is enabled.
#define INV_TX2_RF_ON   0x80
// Set to logic 1, the output signal at pin TX1 will be inverted, if driver TX1 is enabled.
#define INV_TX1_RF_ON   0x40
// Set to logic 1, the output signal at pin TX2 will be inverted, if driver TX2 is disabled.
#define INV_TX2_RF_OFF  0x20
// Set to logic 1, the output signal at pin TX1 will be inverted, if driver TX1 is disabled.
#define INV_TX1_RF_OFF  0x10
// output signal on pin TX2 will deliver continuously the un-modulated 13.56 MHz energy carrier.
#define TX2CW           0x08
// out signal on pin TX2 will deliver the 13.56 MHz energy carrier modulated by the transmission data.
#define TX2RFEN         0x02
// out signal on pin TX1 will deliver the 13.56 MHz energy carrier modulated by the transmission data.
#define TX1RFEN         0x01

#define TX_ASK_REG          0x5 /* Controls the setting of the TX modulation */
// Set to logic 1, Force100ASK forces a 100% ASK modulation
#define FORCE100ASK 0x40

#define TX_SEL_REG          0x6 /* Selects the internal sources for the antenna driver */
// Selects the input of driver Tx1 and Tx2.
#define DRIVER_SEL_MSK                      0x30
#define DRIVER_SEL_TRISTATE                 0x00
#define DRIVER_SEL_MODULATION_MILLER_PULSE  0x10
#define DRIVER_SEL_MODULATION_MIFIN         0x20
#define DRIVER_SEL_HIGH                     0x30
// Selects the input for the MFOUT Pin.
#define MF_OUT_SEL_MSK                      0x0f
#define MF_OUT_SEL_TRISTATE                 0x00
#define MF_OUT_SEL_LOW                      0x01
#define MF_OUT_SEL_HIGH                     0x02
#define MF_OUT_SEL_TESTBUS                  0x03
#define MF_OUT_SEL_MODULATION               0x04
#define MF_OUT_SEL_SERIAL_TX                0x05
#define MF_OUT_SEL_SERIAL_RX                0x07

#define RX_SEL_REG          0x7 /* Selects internal receiver settings */
// Selects the input of the contactless UART
#define UART_SEL_MSK                0xc0
#define UART_SEL_LOW                0x00
#define UART_SEL_MFIN_MANCHESTER    0x40
#define UART_SEL_ANALOG             0x80
#define UART_SEL_MFIN_NRZ           0xc0
// After data transmission, the activation of the receiver is delayed for RxWait bit-clocks.
#define RX_WAIT_MSK                 0x3f
#define RX_WAIT_SET(n)              (n&RX_WAIT_MSK)

#define RX_THRESHOLD_REG    0x8 /* Selects thresholds for the bit decoder */
// Defines the minimum signal strength at the decoder input that shall be accepted.
#define MIN_LEVEL_MSK       0xf0
#define MIN_LEVEL_SFT       4
#define MIN_LEVEL_SET(n)    ((n<<MIN_LEVEL_SFT)&MIN_LEVEL_MSK)
// Defines the minimum signal strength at the decoder input that has to be reached by the weaker half-bit
#define COLL_LEVEL_MSK      0x07
#define COLL_LEVEL_SET(n)   (n&COLL_LEVEL_MSK)

#define DEMOD_REG           0x9 /* Defines demodulator settings */
// Defines the use of I and Q channel during reception
#define ADD_IQ_MSK                      0xc0
#define ADD_IQ_SEL_STRONG               0x00
#define ADD_IQ_SEL_STRONG_FREEZE_SEL    0x40
// If set to logic 1 and the bits AddIQ are set to X0b, the reception is fixed to I channel.
#define FIX_IQ                          0x20
// Changes the time constant of the internal PLL during data reception.
#define TAU_RCV_MSK                     0x0c
#define TAU_RCV_SFT                     2
#define TAU_RCV_SET(n)                  ((n<<TAU_RCV_SFT)&TAU_RCV_MSK)
// Changes the time constant of the internal PLL during burst.
#define TAU_SYNC_MSK                    0x03
#define TAU_SYNC_SET(n)                 (n&TAU_SYNC_MSK)

#define MF_TX_REG           0xc /* Controls some MIFARE® communication transmit parameters */
// These bits define the additional response time.
#define TX_WAIT_MSK 0x3

#define MF_RX_REG           0xd /* Controls some MIFARE® communication receive parameters */
// generation of the Parity bit for transmission and the Parity-Check for receiving is switched off.
#define PARITY_DISABLE  0x10

#define SERIAL_SPEED_REG    0xf /* Selects the speed of the serial UART interface */
// Factor BR_T0 to adjust the transfer speed
#define BR_T0_MSK       0xe0
#define BR_T0_SFT       5
#define BR_T0_SET(n)    ((n<<BR_T0_SFT)&BR_T0_MSK)
// Factor BR_T1 to adjust the transfer speed
#define BR_T1_MSK       0x1f
#define BR_T1_SET(n)    (n&BR_T1_MSK)
#define BR_T_7P2K       0xfa
#define BR_T_9P6K       0xeb
#define BR_T_14P4K      0xda
#define BR_T_19P2K      0xcb
#define BR_T_38P4K      0xab
#define BR_T_57P6K      0x9a
#define BR_T_115P2K     0x7a
#define BR_T_128K       0x74
#define BR_T_230P4K     0x5a
#define BR_T_460P8K     0x3a
#define BR_T_921P6K     0x1c
#define BR_T_1228P8K    0x15


/*** Page 2: CFG ***/

#define CRC_RESULT_MSB_REG  0x1 /* Shows the actual MSB and LSB values of the CRC calculation */
#define CRC_RESULT_LSB_REG  0x2 /* Shows the actual MSB and LSB values of the CRC calculation */
// This register shows the actual value of the most significant byte of the CRCResultReg register.

#define MOD_WIDTH_REG       0x4 /* Controls the setting of the ModWidth */
// define the width of the Miller modulation as multiples of the carrier f (ModWidth +1/fc).

#define RF_CFG_REG          0x6 /* Configures the receiver gain */
// This register defines the receivers signal voltage gain factor:
#define RX_GAIN_MSK 0x70
#define RX_GAIN18DB 0x00
#define RX_GAIN23DB 0x10
#define RX_GAIN33DB 0x40
#define RX_GAIN38DB 0x50
#define RX_GAIN43DB 0x60
#define RX_GAIN48DB 0x70

#define GS_N_REG            0x7 /* the conductance of the antenna driver pins TX1 and TX2 for modulation */
// The value of this register defines the conductance of the output N-driver during times of no modulation.
#define CW_GS_N_MSK      0xf0
#define CW_GS_N_SFT      4
#define CW_GS_N_SET(n)   ((n<<CW_GSN_SFT)&CW_GSN_MSK)
// The value of this register defines the conductance of the output N-driver for the time of modulation.
#define MOD_GS_N_MSK     0x0f
#define MOD_GS_N_SET(n)  (n&MOD_GSN_MSK)

#define CW_GS_P_REG         0x8 /* the conductance of the antenna driver pins TX1 and TX2 for modulation */
// The value of this register defines the conductance of the output P-driver.
#define CW_GS_P_MSK 0x3f

#define MOD_GS_P_REG        0x9 /* the conductance of the antenna driver pins TX1 and TX2 for modulation */
// The value of this register defines the conductance of the output P-driver for the time of modulation.
#define MOD_GS_P_MSK    0x3f

#define TMODE_REG           0xa /* Defines settings for the internal timer */
// timer starts automatically at the end of the transmission in all communication modes at all speeds.
#define TAUTO                   0x80
// The internal timer is running in gated mode.
#define TGATED_MSK              0x60
#define TGATED_NON_GATE         0x00
#define TGATED_GATED_MFIN       0x20
#define TGATED_GATED_AUX1       0x40
#define TGATED_GATED_A3         0x60
// Set to logic 1, the timer automatically restart its count-down from TReloadValue
#define TAUTO_RESTART           0x10
// Defines higher 4 bits for TPrescaler.
#define TPRESCALER_HI_MSK       0x0f
#define TPRESCALER_HI_SET(n)    (n&TPRESCALER_HI_MSK)

#define TPRESCALER_REG      0xb /* Defines settings for the internal timer */
// The following formula is used to calculate fTimer: fTimer = 6.78 MHz/TPreScaler.
#define TPRESCALER(khz) (khz/6780)

#define TRELOAD_REG_HI      0xc /* Describes the 16 bit timer reload value */
#define TRELOAD_REG_LO      0xd /* Describes the 16 bit timer reload value */
// With a start ev the tim loads the TReloadVal. Changing this reg affects the tim only at the next start ev.

#define TCOUNTER_VAL_REG_HI 0xe /* Shows the 16 bit actual timer value */
#define TCOUNTER_VAL_REG_LO 0xf /* Shows the 16 bit actual timer value */
// Current value of the timer


/*** Page 3: TestRegister ***/

#define TEST_SEL1_REG       0x1 /* General test signal configuration */
// Select the TestBus bit from the testbus to be propagated to MFOUT.
#define TST_BUS_BIT_SEL_MSK 0x7

#define TEST_SEL2_REG       0x2 /* General test signal configuration and PRBS control */
// If set to logic 1, the testbus is mapped to the parallel port by the following order:
#define TST_BUS_FLIP        0x80
// Starts and enables the PRBS9 sequence according ITU-TO150.
#define PRBS9               0x40
// Starts and enables the PRBS15 sequence according ITU-TO150.
#define PRBS15              0x20
// Selects the testbus.
#define TEST_BUS_SEL_MSK    0x1f

#define TEST_PIN_EN_REG     0x3 /* Enables pin output driver on D1-D7 */
// Set to logic 0, the lines MX and DTRQ for the serial UART are disabled.
#define RS232_LINE_EN   0x80
// Enables the pin output driver on D1 to D7.
#define TEST_PIN_EN1    0x02
#define TEST_PIN_EN2    0x04
#define TEST_PIN_EN3    0x08
#define TEST_PIN_EN4    0x10
#define TEST_PIN_EN5    0x20
#define TEST_PIN_EN6    0x40

#define TEST_PIN_VALUE_REG  0x4 /* Defines the values for D1 - D7 when it is used as I/O bus */
// bit enables the I/O functionality for the test port if one of the serial interfaces is used.
#define USE_IO          0x80
// Defines the value of the test port, when it is used as I/O.
#define TEST_PIN_VALUE1 0x02
#define TEST_PIN_VALUE2 0x04
#define TEST_PIN_VALUE3 0x08
#define TEST_PIN_VALUE4 0x10
#define TEST_PIN_VALUE5 0x20
#define TEST_PIN_VALUE6 0x40

#define TEST_BUS_REG        0x5 /* Shows the status of the internal testbus */
// The test bus is selected by the register TestSel2Reg. See Section 19 “Testsignals”.

#define AUTO_TEST_REG       0x6 /* Controls the digital selftest */
// If set to logic 1, the internal signal processing in the receiver chain is performed non-linear.
#define AMP_RCV         0x40
// Enables the digital self test.
#define SELF_TEST_EN    0x09
#define SELF_TEST_DEF   0x00

#define VERSION_REG         0x7 /* Shows the version */
// Remark: The current version for MFRC522 is 90h or 91h.

#define ANALOG_TEST_REG     0x8 /* Controls the pins AUX1 and AUX2 */
// Controls the AUX pin.
#define ANALOG_SET_AUX1_TRISTATE        0x00
#define ANALOG_SET_AUX1_TEST_DAC1       0x10
#define ANALOG_SET_AUX1_CORR1           0x20
#define ANALOG_SET_AUX1_MIN_LEVEL       0x40
#define ANALOG_SET_AUX1_ADC_I           0x50
#define ANALOG_SET_AUX1_ADC_Q           0x60
#define ANALOG_SET_AUX1_HIGH            0xa0
#define ANALOG_SET_AUX1_LOW             0xb0
#define ANALOG_SET_AUX1_TX_ACTIVE       0xc0
#define ANALOG_SET_AUX1_RX_ACTIVE       0xd0
#define ANALOG_SET_AUX1_SUBCARRIER_DET  0xe0
#define ANALOG_SET_AUX1_TST_BUS_BIT_SEL 0xf0
#define ANALOG_SET_AUX2_TRISTATE        0x00
#define ANALOG_SET_AUX2_TEST_DAC2       0x01
#define ANALOG_SET_AUX2_CORR1           0x02
#define ANALOG_SET_AUX2_MIN_LEVEL       0x04
#define ANALOG_SET_AUX2_ADC_I           0x05
#define ANALOG_SET_AUX2_ADC_Q           0x06
#define ANALOG_SET_AUX2_HIGH            0x0a
#define ANALOG_SET_AUX2_LOW             0x0b
#define ANALOG_SET_AUX2_TX_ACTIVE       0x0c
#define ANALOG_SET_AUX2_RX_ACTIVE       0x0d
#define ANALOG_SET_AUX2_SUBCARRIER_DET  0x0e
#define ANALOG_SET_AUX2_TST_BUS_BIT_SEL 0x0f

#define TEST_DAC1_REG       0x9 /* Defines the test value for the TestDAC1 */
// Defines the test value for TestDAC1.
#define TEST_DAC1_MSK   0x3f

#define TEST_DAC2_REG       0xa /* Defines the test value for the TestDAC2 */
// Defines the testvalue for TestDAC2.
#define TEST_DAC2_MSK   0x3f

#define TEST_ADC_REG        0xb /* Shows the actual value of ADC I and Q */
// Shows the actual value of ADC I channel.
#define ADC_I_MSK       0xf0
#define ADC_I_SFT       4
#define ADC_I_GET(n)    ((n>>ADC_I_SFT)&0x0f)
// Shows the actual value of ADC Q channel.
#define ADC_Q_MSK       0x0f
#define ADC_Q_GET(n)    (n&ADC_Q_MSK)

#endif
