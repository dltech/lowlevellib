#ifndef H_POWER_REG
#define H_POWER_REG
/*
 * Lasertag on Nordic NRF52
 * Register macro for Low Power Comparator
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/*************Tasks************************/
/* Enable constant latency mode */
#define TASKS_CONSTLAT        MMIO32(POWER + 0x078)
// Trigger task
#define TASKS_CONSTLAT_EN   0x1

/* Enable low power mode (variable latency) */
#define TASKS_LOWPWR          MMIO32(POWER + 0x07c)
// Trigger task
#define TASKS_LOWPWR_EN 0x1

/************Events************************/
/* Power failure warning */
#define EVENTS_POFWARN         MMIO32(POWER + 0x108)
// Event generated
#define EVENTS_POFWARN_EN   0x1

/* CPU entered WFI/WFE sleep */
#define EVENTS_SLEEPENTER MMIO32(POWER + 0x114)
// Event generated
#define EVENTS_SLEEPENTER_EN    0x1

/* CPU exited WFI/WFE sleep */
#define EVENTS_SLEEPEXIT MMIO32(POWER + 0x118)
// Event generated
#define EVENTS_SLEEPEXIT_EN  0x1

/* Voltage supply detected on VBUS */
#define EVENTS_USBDETECTED MMIO32(POWER + 0x11c)
// Event generated
#define EVENTS_USBDETECTED_EN   0x1

/* Voltage supply removed from VBUS */
#define EVENTS_USBREMOVED MMIO32(POWER + 0x120)
// Event generated
#define EVENTS_USBREMOVED_EN    0x1

/* USB 3.3 V supply ready */
#define EVENTS_USBPWRRDY MMIO32(POWER + 0x124)
// Event generated
#define EVENTS_USBPWRRDY_EN 0x1

/************Registers*********************/
/* Enable interrupt */
#define INTENSETPWR     MMIO32(POWER + 0x304)
// Write '1' to Enable interrupt on POFWARN event.
#define POFWARN     0x004
// Write '1' to disable interrupt for event SLEEPENTER
#define SLEEPENTER  0x020
// Write '1' to disable interrupt for event SLEEPEXIT
#define SLEEPEXIT   0x040
// Write '1' to disable interrupt for event USBDETECTED
#define USBDETECTED 0x080
// Write '1' to disable interrupt for event USBREMOVED
#define USBREMOVED  0x100
// Write '1' to disable interrupt for event USBPWRRDY
#define USBPWRRDY   0x200


/* Disable interrupt */
#define INTENCLRPWR     MMIO32(POWER + 0x308)
// Write '1' to disable interrupt for event POFWARN
// Write '1' to disable interrupt for event POFWARN
// Write '1' to disable interrupt for event SLEEPEXIT
// Write '1' to disable interrupt for event USBDETECTED
// Write '1' to disable interrupt for event USBREMOVED
// Write '1' to disable interrupt for event USBPWRRDY

/* Reset reason */
#define RESETREASPWR    MMIO32(POWER + 0x400)
// Reset from pin-reset detected
#define RESETPINP   0x000001
// Reset from watchdog detected
#define DOGP        0x000002
// Reset from AIRCR.SYSRESETREQ detected
#define SREQP       0x000004
// Reset from CPU lock-up detected
#define LOCKUPP     0x000008
// Res due to wkup from sys OFF mode when wkup is trig from DETECT sig from GPIO
#define OFFP        0x010000
//Res to wkup from sys OFF mod when wkup is trig from ANADETECT sig from LPCOMP
#define LPCOMPP     0x020000
//Res to wkup from sys OFF mod when wkup is trig from enter into dbg iface mode
#define DIFP        0x040000
//Reset due to wake up from System OFF mode by NFC field detect
#define NFCP        0x080000
//Reset due to wake up from System OFF mode by VBUS rising into valid range
#define VBUSP       0x100000

/* RAM status register */
#define RAMSTATUSPWR    MMIO32(POWER + 0x428)
// RAM block 0 is on or off/powering up
#define RAMBLOCK0   0x1
// RAM block 1 is on or off/powering up
#define RAMBLOCK1   0x2
// RAM block 2 is on or off/powering up
#define RAMBLOCK2   0x4
// RAM block 3 is on or off/powering up
#define RAMBLOCK3   0x8

/* USB supply status */
#define USBREGSTATUS MMIO32(POWER + 0x438)
// VBUS input detection status
#define VBUSDETECT  0x1
// USB supply output settling time elapsed
#define OUTPUTRDY   0x2

/* System OFF register */
#define SYSTEMOFFPWR    MMIO32(POWER + 0x500)
// Enable system OFF mode
#define SYSTEMOFF   0x1

/* Power failure comparator configuration */
#define POFCONPWR       MMIO32(POWER + 0x510)
// Enable or disable power failure comparator
#define POF                 0x1
// Power failure comparator threshold setting
#define THRESHOLD_V17       0x008
#define THRESHOLD_V18       0x00a
#define THRESHOLD_V19       0x00c
#define THRESHOLD_V20       0x00e
#define THRESHOLD_V21       0x010
#define THRESHOLD_V22       0x012
#define THRESHOLD_V23       0x014
#define THRESHOLD_V24       0x016
#define THRESHOLD_V25       0x018
#define THRESHOLD_V26       0x01a
#define THRESHOLD_V27       0x01c
#define THRESHOLD_V28       0x01e
// Power-fail comparator threshold setting for high voltage mode
#define THRESHOLDVDDH_V27   0x000
#define THRESHOLDVDDH_V28   0x100
#define THRESHOLDVDDH_V29   0x200
#define THRESHOLDVDDH_V30   0x300
#define THRESHOLDVDDH_V31   0x400
#define THRESHOLDVDDH_V32   0x500
#define THRESHOLDVDDH_V33   0x600
#define THRESHOLDVDDH_V34   0x700
#define THRESHOLDVDDH_V35   0x800
#define THRESHOLDVDDH_V36   0x900
#define THRESHOLDVDDH_V37   0xa00
#define THRESHOLDVDDH_V38   0xb00
#define THRESHOLDVDDH_V39   0xc00
#define THRESHOLDVDDH_V40   0xd00
#define THRESHOLDVDDH_V41   0xe00
#define THRESHOLDVDDH_V42   0xf00

/* General purpose retention register */
#define GPREGRETPWR     MMIO32(POWER + 0x51c)
// General purpose retention register. GPREGRET[7:0]

/* General purpose retention register */
#define GPREGRET2PWR     MMIO32(POWER + 0x520)
// General purpose retention register. GPREGRET[7:0]

/* DC/DC enable register */
#define DCDCENPWR       MMIO32(POWER + 0x578)
// Enable or disable DC/DC converter
#define DCDCEN  0x1

/* Main supply status */
#define MAINREGSTATUS MMIO32(POWER + 0x640)
// High voltage mode. Voltage supplied on VDDH.
#define MAINREGSTATUS_HIGH  0x1

/* RAMx power control register */
#define RAM0POWER MMIO32(POWER + 0x900)
#define RAM1POWER MMIO32(POWER + 0x910)
#define RAM2POWER MMIO32(POWER + 0x920)
#define RAM3POWER MMIO32(POWER + 0x930)
#define RAM4POWER MMIO32(POWER + 0x940)
#define RAM5POWER MMIO32(POWER + 0x950)
#define RAM6POWER MMIO32(POWER + 0x960)
#define RAM7POWER MMIO32(POWER + 0x970)
#define RAM8POWER MMIO32(POWER + 0x980)
// Keep RAM section Si on or off in System ON mode.
#define S0POWER         0x00000001
#define S1POWER         0x00000002
#define S2POWER         0x00000004
#define S3POWER         0x00000008
#define S4POWER         0x00000010
#define S5POWER         0x00000020
#define S6POWER         0x00000040
#define S7POWER         0x00000080
#define S8POWER         0x00000100
#define S9POWER         0x00000200
#define S10POWER        0x00000400
#define S11POWER        0x00000800
#define S12POWER        0x00001000
#define S13POWER        0x00002000
#define S14POWER        0x00004000
#define S15POWER        0x00008000
// Keep retention on RAM section Si when RAM section is off
#define S0RETENTION     0x00010000
#define S1RETENTION     0x00020000
#define S2RETENTION     0x00040000
#define S3RETENTION     0x00080000
#define S4RETENTION     0x00100000
#define S5RETENTION     0x00200000
#define S6RETENTION     0x00400000
#define S7RETENTION     0x00800000
#define S8RETENTION     0x01000000
#define S9RETENTION     0x02000000
#define S10RETENTION    0x04000000
#define S11RETENTION    0x08000000
#define S12RETENTION    0x10000000
#define S13RETENTION    0x20000000
#define S14RETENTION    0x40000000
#define S15RETENTION    0x80000000

/* RAMx power control set register */
#define RAM0POWERSET MMIO32(POWER + 0x904)
#define RAM1POWERSET MMIO32(POWER + 0x914)
#define RAM2POWERSET MMIO32(POWER + 0x924)
#define RAM3POWERSET MMIO32(POWER + 0x934)
#define RAM4POWERSET MMIO32(POWER + 0x944)
#define RAM5POWERSET MMIO32(POWER + 0x954)
#define RAM6POWERSET MMIO32(POWER + 0x964)
#define RAM7POWERSET MMIO32(POWER + 0x974)
#define RAM8POWERSET MMIO32(POWER + 0x984)
// Keep RAM section Si of RAMn on or off in System ON mode
// Keep retention on RAM section Si when RAM section is switched off

/* RAMx power control clear register */
#define RAM0POWERCLR MMIO32(POWER + 0x908)
#define RAM1POWERCLR MMIO32(POWER + 0x918)
#define RAM2POWERCLR MMIO32(POWER + 0x928)
#define RAM3POWERCLR MMIO32(POWER + 0x938)
#define RAM4POWERCLR MMIO32(POWER + 0x948)
#define RAM5POWERCLR MMIO32(POWER + 0x958)
#define RAM6POWERCLR MMIO32(POWER + 0x968)
#define RAM7POWERCLR MMIO32(POWER + 0x978)
#define RAM8POWERCLR MMIO32(POWER + 0x988)
// Keep RAM section Si of RAMn on or off in System ON mode
// Keep retention on RAM section Si when RAM section is switched off

#endif
