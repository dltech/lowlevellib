#ifndef H_SI47XX_AM_REGS
#define H_SI47XX_AM_REGS
/*
 * Si4730 broadcast AM/FM receiver registers.
 * Commands and Properties for the AM/SW/LW Receiver
 * (Si4730/31/32/34/35/36/37/40/41/42/43/44/45)
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*** AM/SW/LW Receiver Commands ***/

/* Tunes to a given AM frequency. */
#define AM_TUNE_FREQ    0x40
// ARG1
// FAST Tuning.
#define FAST    0x1
// ARG2 Tune Frequency High Byte. FREQH[7:0]
// ARG3 Tune Frequency Low Byte. FREQL[7:0]
// ARG4 Antenna Tuning Capacitor High Byte. ANTCAPH[15:8]
// ARG5 Antenna Tuning Capacitor Low Byte. ANTCAPL[7:0]
#define AM_TUNE_FREQ_REQ_SIZE   6
#define AM_TUNE_FREQ_RESP_SIZE  1

/* Begins searching for a valid frequency. */
#define AM_SEEK_START   0x41
// ARG1
// Seek Up/Down. Determines the direction of the search, either UP = 1, or DOWN = 0.
#define SEEKUP  0x8
// Wrap/Halt. Determines whether the seek should Wrap = 1, or Halt = 0
#define WRAP    0x4
// ARG2 Reserved
// ARG3 Reserved
// ARG4 ANTCAPH[15:8]   Antenna Tuning Capacitor High Byte.
// ARG5 ANTCAPL[7:0]    Antenna Tuning Capacitor Low Byte.
#define AM_SEEK_START_REQ_SIZE   6
#define AM_SEEK_START_RESP_SIZE  1

/* Queries the status of the already issued AM_TUNE_FREQ or AM_SEEK_START cmmd. */
#define AM_TUNE_STATUS  0x42
// Cancel seek. If set, aborts a seek currently in progress.
#define CANCEL  0x2
// Seek/Tune Interrupt Clear.
#define INTACK  0x1

// RESP1
// Band Limit. Reports if a seek hit the band limit
#define BLTF    0x80
// AFC Rail Indicator. Set if the AFC rails.
#define AFCRL   0x02
// Valid Channel. Set if the ch is currently valid.
#define VALID   0x01
// RESP2 READFREQH[7:0]     Read Frequency High Byte.
// RESP3 READFREQL[7:0]     Read Frequency Low Byte.
// RESP4 RSSI[7:0]          Received Signal Strength Indicator.
// RESP5 SNR[7:0]           SNR.
// RESP6 READANTCAPH[15:8]  Read Antenna Tuning Capacitor High Byte.
// RESP7 READANTCAPL[7:0]   Read Antenna Tuning Capacitor Low Byte.
#define AM_TUNE_STATUS_REQ_SIZE     2
#define AM_TUNE_STATUS_RESP_SIZE    8

/* Queries the status of the Received Signal Quality (RSQ) for the current ch. */
#define AM_RSQ_STATUS   0x43
// Interrupt Acknowledge. 0 = Interrupt status preserved.
#define INTACK  0x1

// RESP1
// SNR Detect High. 1 = Received SNR has exceeded above SNR high threshold.
#define SNRHINT     0x8
// SNR Detect Low. 1 = Received SNR has exceeded below SNR low threshold.
#define SNRLINT     0x4
// RSSI Detect High. 1 = RSSI has exceeded above RSSI high threshold.
#define RSSIHINT    0x2
// RSSI Detect Low. 1 = RSSI has exceeded below RSSI low threshold.
#define RSSILINT    0x1
// RESP2
// Soft Mute Indicator. Indicates soft mute is engaged.
#define SMUTE       0x8
// AFC Rail Indicator. Set if the AFC rails.
#define AFCRL_ST    0x2
// Valid Channel. ch is currently valid and would have been found during a seek.
#define VALID_ST    0x1
// RESP3 Reserved
// RESP4 RSSI[7:0]  Received Signal Strength Indicator.
// RESP5 SNR[7:0]   SNR.
#define AM_RSQ_STATUS_REQ_SIZE  2
#define AM_RSQ_STATUS_RESP_SIZE 6

/* Queries the current AGC settings. */
#define AM_AGC_STATUS   0x47
// RESP1
// AM AGC Disable
#define AMAGCDIS    0x1
// RESP2 AMAGCNDX[7:0] AM AGC Index
#define AM_AGC_STATUS_REQ_SIZE  1
#define AM_AGC_STATUS_RESP_SIZE 3

/* Overrides AGC settings by disabling and forcing it to a fixed value. */
#define AM_AGC_OVERRIDE 0x48
// ARG1
// AM AGC Disable
#define AMAGCDIS    0x1
// ARG2 AM AGC Index AMAGCNDX[7:0]
#define AM_AGC_OVERRIDE_REQ_SIZE  3
#define AM_AGC_OVERRIDE_RESP_SIZE 1

/*** AM/SW/LW Receiver Properties ***/

/* Sets deemphasis time constant. Can be set to 50 μs. */
#define AM_DEEMPHASIS                           0x3100
// AM De-Emphasis. 1 = 50 μs. 0 = Disabled.
#define DEEMPH  0x1

/* Selects the bandwidth of the channel filter for AM reception. */
#define AM_CHANNEL_FILTER                       0x3102
// Enables the AM Power Line Noise Rejection Filter
#define AMPLFLT 0x100
// AM Channel Filter.
#define AMCHFILT6KHZ
#define AMCHFILT4KHZ
#define AMCHFILT3KHZ
#define AMCHFILT2KHZ
#define AMCHFILT1KHZ
#define AMCHFILT1P8KHZ
#define AMCHFILT2P5KHZ

/* Sets the maximum gain for automatic volume control. */
#define AM_AUTOMATIC_VOLUME_CONTROL_MAX_GAIN    0x3103
// Automatic Volume Control Max Gain.
#define AVC_MAXGAIN_MSK 0x7fff

/* Sets the SW AFC pull-in range. */
#define AM_MODE_AFC_SW_PULL_IN_RANGE            0x3104
// The SW pull-in range expressed relative to the tuned frequency.

/* Sets the SW AFC lock-in. */
#define AM_MODE_AFC_SW_LOCK_IN_RANGE            0x3105
// The SW lock-in range expressed relative to the tuned frequency.

/* Configures interrupt related to Received Signal Quality metrics. */
#define AM_RSQ_INTERRUPTS                       0x3200
// Interrupt Source Enable: SNR High.
#define SNRHIEN     0x8
// Interrupt Source Enable: SNR Low.
#define SNRLIEN     0x4
// Interrupt Source Enable: RSSI High.
#define RSSIHIEN    0x2
// Interrupt Source Enable: RSSI Low.
#define RSSILIEN    0x1

/* Sets high threshold for SNR interrupt. */
#define AM_RSQ_SNR_HIGH_THRESHOLD               0x3201
// Specified in units of dB in 1 dB steps (0–127). Default is 0 dB.
#define SNRH_MSK    0x7f

/* Sets low threshold for SNR interrupt. */
#define AM_RSQ_SNR_LOW_THRESHOLD                0x3202
// Specified in units of dB in 1 dB steps (0–127). Default is 0 dB.
#define SNRL_MSK    0x7f

/* Sets high threshold for RSSI interrupt. */
#define AM_RSQ_RSSI_HIGH_THRESHOLD              0x3203
// Specified in units of dBμV in 1 dB steps (0–127). Default is 0 dBμV.
#define RSSIH_MSK   0x7f

/* Sets low threshold for RSSI interrupt. */
#define AM_RSQ_RSSI_LOW_THRESHOLD               0x3204
// Specified in units of dBμV in 1 dB steps (0–127). Default is 0 dBμV.
#define RSSIL_MSK   0x7f

/* Sets the attack and decay rates when entering or leaving soft mute. */
#define AM_SOFT_MUTE_RATE                       0x3300
// Determines how quickly the AM goes into soft mute when soft mute is enabled.

/* Sets the AM soft mute slope. Default value is a slope of 1. */
#define AM_SOFT_MUTE_SLOPE                      0x3301
// Set soft mute att slope in dB attenuation per dB SNR below the soft mute SNR th.
#define SMSLOPE_MSK 0xf

/* Sets maximum attenuation during soft mute (dB). */
#define AM_SOFT_MUTE_MAX_ATTENUATION            0x3302
// Specified in units of dB. Default maximum attenuation is 8 dB.
#define SMATTN_MSK  0x3f

/* Sets SNR threshold to engage soft mute. */
#define AM_SOFT_MUTE_SNR_THRESHOLD              0x3303
// The SNR th for a tuned freq below which soft mute is engaged provided the val.
#define SMTHR_MSK   0x3f

/* Sets softmute release rate. */
#define AM_SOFT_MUTE_RELEASE_RATE               0x3304
// Smaller values provide slower release and larger values provide faster release.
#define RELEASE_MSK 0x7fff

/* Sets software attack rate. */
#define AM_SOFT_MUTE_ATTACK_RATE                0x3305
// Smaller values provide slower attack and larger values provide faster attack
#define ATTACK_MSK  0x7fff

/* Sets the bottom of the AM band for seek. Default is 520. */
#define AM_SEEK_BAND_BOTTOM                     0x3400
// Sets the lower boundary for the AM band in kHz.

/* Sets the top of the AM band for seek. Default is 1710. */
#define AM_SEEK_BAND_TOP                        0x3401
// Specify the higher boundary of the AM band when performing a seek.

/* Selects frequency spacing for AM seek. Default is 10 kHz spacing. */
#define AM_SEEK_FREQ_SPACING                    0x3402
// Sets the frequency spacing when performing a seek in the AM band.
#define AMSKSPACE_MSK   0xf


/* Sets the SNR threshold for a valid AM Seek/Tune. */
#define AM_SEEK_SNR_THRESHOLD                   0x3403
// SNR Threshold which determines if a valid channel has been found during Seek/Tune.
#define AMSKSNR_MSK 0x3f


/* Sets the RSSI threshold for a valid AM Seek/Tune. */
#define AM_SEEK_RSSI_THRESHOLD                  0x3404
// RSSI Threshold which determines if a valid channel has been found during Seek/Tune.
#define AMSKRSSI_MSK    0x3f


/* Sets the num of ms the high peak det must be exceeded before decreasing gain. */
#define AM_AGC_ATTACK_RATE                      0x3702
// Large values provide slower attack, and smaller values provide faster attack.
#define ATTACK_AGC_MSK  0xff


/* num of ms the low peak detector must not be exceeded before inc the gain. */
#define AM_AGC_RELEASE_RATE                     0x3703
// Larger values provide slower release, and smaller values provide faster release.
#define RELEASE_AGC_MSK 0x7f

/* Adjusts AM AGC for frontend (external) attenuator and LNA. */
#define AM_FRONTEND_AGC_CONTROL                 0x3705
// impacts sensitivity and U/D performance.
#define MIN_GAIN_INDEX_MSK      0xff00
#define MIN_GAIN_INDEX_SFT      8
#define MIN_GAIN_INDEX_SET(n)   ((n<<MIN_GAIN_INDEX_SFT)&MIN_GAIN_INDEX_MSK)
// insures the AGC gain indexes
#define ATTN_BACKUP_MSK         0x00ff
#define ATTN_BACKUP_SET(n)      (n&ATTN_BACKUP_MSK)

/* Sets the threshold for detecting impulses in dB above the noise floor. */
#define AM_NB_DETECT_THRESHOLD                  0x3900
// The default is 12 dB.

/* Interval in ms that original samples are replaced by interpolated clean sampl. */
#define AM_NB_INTERVAL                          0x3901
// The default is 55 μs.

/* Noise blanking rate in 100 Hz units. Default value is 64. */
#define AM_NB_RATE                              0x3902
// The default is 64 (6400 Hz).

/* Sets the bandwidth of the noise floor estimator. Default value is 300. */
#define AM_NB_IIR_FILTER                        0x3903
// The default is 300 (465 Hz).

/* Delay in us before applying impulse blanking to the original samples. */
#define AM_NB_DELAY                             0x3904
// The default is 172 μs.

#endif
