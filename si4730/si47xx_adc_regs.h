#ifndef H_SI47XX_ADC_REGS
#define H_SI47XX_ADC_REGS
/*
 * Si4730 broadcast AM/FM receiver registers.
 * Commands and Properties for the Stereo Audio ADC Mode
 * (Si4704/05/30/31)
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*** Stereo Audio ADC Mode Commands ***/

/* Starts sampling rate conversion. */
#define AUX_ASRC_START  0x61
#define AUX_ASRC_START_REQ_SIZE    1
#define AUX_ASRC_START_RESP_SIZE   1

/* Reports audio signal quality metrics. */
#define AUX_ASQ_STATUS  0x65
// Interrupt Acknowledge. 1 = Clears ASQINT
#define INTACK  0x1

// RESP1
// Audio Signal Overload Interrupt. 1 = Audio Input Signal overload has been detected.
#define OVERLOADINT 0x1
// RESP2
// Audio Signal Overload. 1 = Audio Input Signal overload is present.
#define OVERLOAD    0x1
// RESP3 LEVEL[7:0] Audio Input Signal Level.
#define AUX_ASQ_STATUS_REQ_SIZE    2
#define AUX_ASQ_STATUS_RESP_SIZE   4

/*** Stereo Audio ADC Mode Properties ***/

/* Configure ASQ Interrupt source. */
#define AUX_ASQ_INTERRUPT_SOURCE    0x6600
// Interrupt Source Enable: Overload
#define OVERLOADINT 0x1

#endif
