#ifndef H_SI47XX_WB_REGS
#define H_SI47XX_WB_REGS
/*
 * Si4730 broadcast AM/FM receiver registers.
 * Commands and Properties for the WB Receiver (Si4707/36/37/38/39/42/43)
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Status Response */
// Clear to Send. 1 = Clear to send next command.
#define CTS     0x80
// Error.
#define ERR     0x40
// Received Signal Quality Interrupt.
#define RSQINT  0x08
// SAME Interrupt (Si4707 Only). 1 = SAME interrupt has been triggered.
#define SAMEINT 0x04
// Audio Signal Quality Interrupt.
#define ASQINT  0x02
// Seek/Tune Complete Interrupt. 1 = Tune complete interrupt has been triggered.
#define STCINT  0x01

/*** WB Receiver Commands ***/

/* Power up device and mode selection. */
#define POWER_UP        0x01
// ARG1
// CTS Interrupt Enable.
#define CTSIEN_UP   0x80
// GPO2 Output Enable.
#define GPO2OEN_UP  0x40
// Patch Enable. 1 Cp NVM to RAM, but do not boot.
#define PATCH       0x20
// Crystal Oscillator Enable. 1 = Use crystal oscillator
#define XOSCEN      0x10
// Function.
#define FUNC_WB     0x03
#define FUNC_LIB_ID 0x0f
// ARG2
// Application Setting.
#define OPMODE_ANALOG_OUT           0x05
#define OPMODE_DIGITAL_LR_OUT       0x0b
#define OPMODE_DIGITAL_OUT          0xb0
#define OPMODE_ANALOG_DIGITAL_OUT   0xb5

// Response (FUNC = 15, Query Library ID)
// RESP1 PN[7:0]        Final 2 digits of part number (HEX).
// RESP2 FWMAJOR[7:0]   Firmware Major Revision (ASCII).
// RESP3 FWMINOR[7:0]   Firmware Minor Revision (ASCII).
// RESP4 RESERVED[7:0]  Reserved, various values.
// RESP5 RESERVED[7:0]  Reserved, various values.
// RESP6 CHIPREV[7:0]   Chip Revision (ASCII).
// RESP7 LIBRARYID[7:0] Library Revision (HEX).
#define POWER_UP_REQ_SIZE   3
#define POWER_UP_RESP1_SIZE 1
#define POWER_UP_RESP2_SIZE 8

/* Returns revision information on the device. */
#define GET_REV         0x10
// RESP1 PN[7:0]        Final 2 digits of Part Number (HEX).
// RESP2 FWMAJOR[7:0]   Firmware Major Revision (ASCII).
// RESP3 FWMINOR[7:0]   Firmware Minor Revision (ASCII).
// RESP4 PATCHH[7:0]    Patch ID High Byte (HEX).
// RESP5 PATCHL[7:0]    Patch ID Low Byte (HEX).
// RESP6 CMPMAJOR[7:0]  Component Major Revision (ASCII).
// RESP7 CMPMINOR[7:0]  Component Minor Revision (ASCII).
// RESP8 CHIPREV[7:0]   Chip Revision (ASCII).
#define GET_REV_REQ_SIZE    1
#define GET_REV_RESP_SIZE   9

/* Power down device. */
#define POWER_DOWN      0x11
#define POWER_DOWN_REQ_SIZE    1
#define POWER_DOWN_RESP_SIZE   1

/* Sets the value of a property. */
#define SET_PROPERTY    0x12
// ARG1 Always write to 0.
// ARG2 Property High Byte. PROPH[7:0]
// ARG3 Property Low Byte. PROPL[7:0]
// ARG4 Property Value High Byte. PROPVH[7:0]
// ARG5 Property Value Low Byte. PROPVL[7:0]
#define SET_PROPERTY_REQ_SIZE    6
#define SET_PROPERTY_RESP_SIZE   1

/* Retrieves a property’s value. */
#define GET_PROPERTY    0x13
// ARG1 Always write to 0.
// ARG2 Property High Byte. PROPGH[7:0]
// ARG3 Property Low Byte. PROPGL[7:0]

// RESP1 Reserved
// RESP2 PROPVH[7:0] Property Value High Byte.
// RESP3 PROPVL[7:0] Property Value Low Byte.
#define GET_PROPERTY_REQ_SIZE    4
#define GET_PROPERTY_RESP_SIZE   4

/* Reads interrupt status bits. */
#define GET_INT_STATUS  0x14
#define GET_INT_STATUS_REQ_SIZE    1
#define GET_INT_STATUS_RESP_SIZE   1

/* Reserved command used for patch file downloads. */
#define PATCH_ARGS      0x15

/* Reserved command used for patch file downloads. */
#define PATCH_DATA      0x16

/* Selects the WB tuning frequency. */
#define WB_TUNE_FREQ    0x50
// ARG1 Always write to 0.
// ARG2 FREQH[7:0] Tune Frequency High Byte.
// ARG3 FREQL[7:0] Tune Frequency Low Byte.
#define WB_TUNE_FREQ_REQ_SIZE   4
#define WB_TUNE_FREQ_RESP_SIZE  1

/* Queries the status of previous WB_TUNE_FREQ or WB_SEEK_START command. */
#define WB_TUNE_STATUS  0x52
// Seek/Tune Interrupt Clear.
#define INTACK  0x1

// RESP1
// AFC Rail Indicator. This bit will be set if the AFC rails.
#define AFCRL   0x2
// Valid Channel. Confirms if the tuned channel is currently valid.
#define VALID   0x1
// RESP2 READFREQH[7:0] Read Frequency High Byte.
// RESP3 READFREQL[7:0] Read Frequency Low Byte.
// RESP4 RSSI[7:0]      Received Signal Strength Indicator.
// RESP5 SNR[7:0]       SNR.
#define WB_TUNE_STATUS_REQ_SIZE     2
#define WB_TUNE_STATUS_RESP_SIZE    6

/* Queries the status of the Received Signal Quality (RSQ) of the current channel */
#define WB_RSQ_STATUS   0x53
// Interrupt Acknowledge 0 = Interrupt status preserved.
#define INTACK  0x1

// RESP1
// SNR Detect High. 1 = Received SNR has exceeded above SNR high threshold.
#define SNRHINT     0x8
// SNR Detect Low. 1 = Received SNR has exceeded below SNR low threshold.
#define SNRLINT     0x4
// RSSI Detect High. 1 = RSSI has exceeded above RSSI high threshold.
#define RSSIHINT    0x2
// RSSI Detect Low. 1 = RSSI has exceeded below RSSI low threshold.
#define RSSILINT    0x1
// RESP2
// AFC Rail Indicator. This bit will be set if the AFC rails.
#define AFCRL_RSQ   0x2
// Valid Channel. Confirms if the channel is currently valid.
#define VALID_RSQ   0x1
// RESP3 Reserved
// RESP4 RSSI[7:0]      Received Signal Strength Indicator.
// RESP5 SNR[7:0]       SNR.
// RESP6 Reserved
// RESP7 FREQOFF[7:0]   Frequency Offset.
#define WB_RSQ_STATUS_REQ_SIZE     2
#define WB_RSQ_STATUS_RESP_SIZE    8

/* Retrieves Specific Area Message Encoding (SAME) */
#define WB_SAME_STATUS  0x54
// ARG1
// Clear Buffer 1 = Clears the contents of the SAME Message Buffer.
#define CLRBUF      0x2
// Interrupt Acknowledge 1 = Clears SAMEINT.
#define INTACK_SAME 0x1
// ARG2 Byte in the message buffer to start reading from. READADDR[7:0]

// RESP1
// End Of Message Detected 1 = End of message is detected.
#define EOMDET_SAME  0x8
// Start Of Message Detected 1 = start of message is detected.
#define SOMDET_SAME  0x4
// Preamble Detected 1 = Preamble is detected.
#define PREDET_SAME  0x2
// Header Buffer Ready 1 = Header buffer is ready.
#define HDRRDY_SAME  0x1
// RESP2 STATE[7:0]     State Machine Status
// RESP3 MSGLEN[7:0]    SAME Message Length
// RESP4
// Confidence Metric for DATAn represented as a number between 0 (low) and 3 (high).
#define CONF7_GET(reg)  ((reg>>6)&0x3)
#define CONF6_GET(reg)  ((reg>>4)&0x3)
#define CONF5_GET(reg)  ((reg>>2)&0x3)
#define CONF4_GET(reg)  (reg&0x3)
// RESP5
#define CONF3_GET(reg)  ((reg>>6)&0x3)
#define CONF2_GET(reg)  ((reg>>4)&0x3)
#define CONF1_GET(reg)  ((reg>>2)&0x3)
#define CONF0_GET(reg)  (reg&0x3)
// RESP6  DATA0[7:0]    Byte of message read at address, READADDR + 0
// RESP7  DATA1[7:0]    Byte of message read at address, READADDR + 1
// RESP8  DATA2[7:0]    Byte of message read at address, READADDR + 2
// RESP9  DATA3[7:0]    Byte of message read at address, READADDR + 3
// RESP10 DATA4[7:0]    Byte of message read at address, READADDR + 4
// RESP11 DATA5[7:0]    Byte of message read at address, READADDR + 5
// RESP12 DATA6[7:0]    Byte of message read at address, READADDR + 6
// RESP13 DATA7[7:0]    Byte of message read at address, READADDR + 7
#define WB_SAME_STATUS_REQ_SIZE     2
#define WB_SAME_STATUS_RESP_SIZE    14

/* Queries the status of the 1050 kHz alert tone in Weather Band. */
#define WB_ASQ_STATUS   0x55
// Interrupt Acknowledge
#define INTACK_ASQ  0x1

// RESP1
// 0 = 1050 Hz alert tone has not been detected to be absent
#define ALERTOFF_INT    0x2
// 0 = 1050 Hz alert tone has not been detected to be present
#define ALERTON_INT     0x1
// RESP2
// 1 = 1050 Hz alert tone is currently present.
#define ALERT  0x1
#define WB_ASQ_STATUS_REQ_SIZE  2
#define WB_ASQ_STATUS_RESP_SIZE 3

/* Queries the current AGC settings */
#define WB_AGC_STATUS   0x57
// RESP1
// This bit indicates whether the RF AGC is disabled or not 1 = RF AGC is disabled.
#define READ_RFAGCDIS   0x1
#define WB_AGC_STATUS_REQ_SIZE  2
#define WB_AGC_STATUS_RESP_SIZE 2

/* Override AGC setting by disabling and forcing it to a fixed value */
#define WB_AGC_OVERRIDE 0x58
// This bit selects whether the RF AGC is disabled or not 1 = RF AGC is disabled.
#define RFAGCDIS    0x1
#define WB_AGC_OVERRIDE_REQ_SIZE  2
#define WB_AGC_OVERRIDE_RESP_SIZE 1

/* Configures GPO1, 2, and 3 as output or Hi-Z */
#define GPIO_CTL        0x80
// GPO3 Output Enable.
#define GPO3OEN 0x8
// GPO2 Output Enable.
#define GPO2OEN 0x4
// GPO1 Output Enable.
#define GPO1OEN 0x2
#define GPIO_CTL_REQ_SIZE   2
#define GPIO_CTL_RESP_SIZE  1

/* Sets GPO1, 2, and 3 output level (low or high) */
#define GPIO_SET        0x81
// GPO3 Output Level.
#define GPO3LEVEL 0x8
// GPO2 Output Level.
#define GPO2LEVEL 0x4
// GPO1 Output Level.
#define GPO1LEVEL 0x2
#define GPIO_SET_REQ_SIZE   2
#define GPIO_SET_RESP_SIZE  1

/*** WB Receiver Properties ***/

/* Enables interrupt sources. */
#define GPO_IEN                     0x0001
// RSQ Interrupt Repeat. 1 = Interrupt generated even if RSQINT is already set.
#define RSQREP  0x800
// SAME Interrupt Repeat (Si4707 Only).
#define SAMEREP 0x400
// ASQ Interrupt Repeat. 1 = Interrupt generated even if ASQINT is already set.
#define ASQREP  0x200
// STC Interrupt Repeat. 1 = Interrupt generated even if STCINT is already set.
#define STCREP  0x100
// CTS Int En. After PowerUp, this bit reflects the CTSIEN bit in ARG1 of PowerUp Cmd.
#define CTSIEN  0x080
// ERR Interrupt Enable. 1 = Interrupt generated when ERR is set.
#define ERRIEN  0x040
// RSQ Interrupt Enable. 1 = Interrupt generated when RSQINT is set.
#define RSQIEN  0x008
// SAME Interrupt Enable (Si4707 Only).
#define SAMEIEN 0x004
// ASQ Interrupt Enable. 1 = Interrupt generated when ASQINT is set.
#define ASQIEN  0x002
// Seek/Tune Complete Interrupt Enable. 1 = Interrupt generated when STCINT is set.
#define STCIEN  0x001

/* Configure digital audio outputs. */
#define DIGITAL_OUTPUT_FORMAT       0x0102
// Digital Output DCLK Edge. 1 = use DCLK falling edge
#define OFALL                       0x80
// Digital Output Mode.
#define OMODE_I2S                   0x00
#define OMODE_LEFT_JUSTIFIED        0x30
#define OMODE_MSB_AT_SECOND_DCLK    0x40
#define OMODE_MSB_AT_FIRST_DCLK     0x60
// Digital Output Mono Mode. 0 = Use mono/stereo blend (per blend thresholds)
#define OMONO                       0x04
// Digital Output Audio Sample Precision.
#define OSIZE16BITS                 0x00
#define OSIZE20BITS                 0x01
#define OSIZE24BITS                 0x02
#define OSIZE8BITS                  0x03

/* Configure digital audio output sample rate. */
#define DIGITAL_OUTPUT_SAMPLE_RATE  0x0104
// 32–48 ksps. 0 to disable digital audio output.

/* Sets frequency of reference clock in Hz. The range is 31130 to  34406 Hz */
#define REFCLK_FREQ                 0x0201
// The allowed REFCLK frequency range is between 31130 and 34406 Hz

/* Sets the prescaler value for RCLK input. */
#define REFCLK_PRESCALE             0x0202
// 1 = DCLK pin is clock source.
#define RCLKSEL     0x1000
// Prescaler for Reference Clock. Integer number used to divide clock frequency down to REFCLK frequency.
#define REFCLKP_MSK     0x0fff
#define REFCLKP_SET(n)  (n&REFCLKP_MSK)

/* Sets the maximum freq error allowed before setting the AFC_RAIL indicator. */
#define WB_MAX_TUNE_ERROR           0x5108
// Specified in units of kHz. Default is 10 kHz.

/* Configures interrupt related to Received Signal Quality metrics. */
#define WB_RSQ_INT_SOURCE           0x5200
// Interrupt Source Enable: Audio SNR High.
#define SNRHIEN     0x8
// Interrupt Source Enable: Audio SNR Low.
#define SNRLIEN     0x4
// Interrupt Source Enable: RSSI High.
#define RSSIHIEN    0x2
// Interrupt Source Enable: RSSI Low.
#define RSSILIEN    0x1

/* Sets high threshold for SNR interrupt. */
#define WB_RSQ_SNR_HI_THRESHOLD     0x5201
// Specified in units of dB in 1 dB steps (0...127). Default is 127dB.

/* Sets low threshold for SNR interrupt. */
#define WB_RSQ_SNR_LO_THRESHOLD     0x5202
// Specified in units of dB in 1 dB steps (0...127). Default is 0dB.

/* Sets high threshold for RSSI interrupt. */
#define WB_RSQ_RSSI_HI_THRESHOLD    0x5203
// Specified in units of dB in 1 dB steps (0...127). Default is 127dB.

/* Sets low threshold for RSSI interrupt. */
#define WB_RSQ_RSSI_LO_THRESHOLD    0x5204
// Specified in units of dB in 1 dB steps (0...127). Default is 0dB.

/* Sets SNR threshold to indicate a valid channel */
#define WB_VALID_SNR_THRESHOLD      0x5403
// Specified in units of dB in 1 dB steps (0...127). Default is 3 dB.

/* Sets RSSI threshold to indicate a valid channel */
#define WB_VALID_RSSI_THRESHOLD     0x5404
// Specified in units of dB in 1 dB steps (0...127). Default is 20 dB.

/* Configures SAME interrupt sources. */
#define WB_SAME_INTERRUPT_SOURCE    0x5500
// Enable EOMDET as the source of SAME Interrupt.
#define EOMDET  0x8
// Enable SOMDET as the source of SAME Interrupt.
#define SOMDET  0x4
// Enable PREDET as the source of SAME Interrupt.
#define PREDET  0x2
// Enable HDRRDY as the source of SAME Interrupt.
#define HDRRDY  0x1

/* Configures interrupt related to the 1050 kHz alert tone */
#define WB_ASQ_INT_SOURCE           0x5600
// Enable 1050kHz alert tone disappeared as the source of interrupt.
#define ALERTOFF_IEN    0x2
// Enable 1050kHz alert tone appeared as the source of interrupt.
#define ALERTON_IEN     0x1

/* Sets the output volume. */
#define RX_VOLUME                   0x4000
// Sets the output volume level, 63 max, 0 min. Default is 63.
#define VOL_MSK 0x3f

/* Mutes the audio output. L and R audio outputs may not be muted independently. */
#define RX_HARD_MUTE                0x4001
// Mutes L Audio Output.
#define LMUTE   0x2
// Mutes R Audio Output.
#define RMUTE   0x1

#endif
