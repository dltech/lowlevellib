#ifndef H_SI47XX_FM_REGS
#define H_SI47XX_FM_REGS
/*
 * Si4730 broadcast AM/FM receiver registers.
 * Commands and Properties for the FM/RDS Receiver
 * (Si4710/11/12/13/20/21)
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*** FM/RDS Receiver Command Summary ***/

/* Status Response */
// Clear to Send. 1 = Clear to send next command.
#define CTS     0x80
// Error. 0 = No error
#define ERR     0x40
// Received Signal Quality Interrupt.
#define RSQINT  0x08
// Radio Data System (RDS) Interrupt (Si4705/21/31/32/35/37/39/85 Only).
#define RDSINT  0x04
// Seek/Tune Complete Interrupt. 1 = Tune complete has been triggered.
#define STCINT  0x01

/*** FM/RDS Receiver Command Summary ***/

/* Power up device and mode selection. */
#define POWER_UP        0x01
// ARG1
// CTS Interrupt Enable.
#define CTSIEN_UP   0x80
// GPO2 Output Enable.
#define GPO2OEN_UP  0x40
// Patch Enable. 1 Cp NVM to RAM, but do not boot.
#define PATCH       0x20
// Crystal Oscillator Enable. 1 = Use crystal oscillator
#define XOSCEN      0x10
// Function.
#define FUNC_FM     0x00
#define FUNC_LIB_ID 0x0f
// ARG2
// Application Setting.
#define OPMODE_RDS_ONLY             0x00
#define OPMODE_ANALOG_OUT           0x05
#define OPMODE_DIGITAL_LR_OUT       0x0b
#define OPMODE_DIGITAL_OUT          0xb0
#define OPMODE_ANALOG_DIGITAL_OUT   0xb5

// Response (FUNC = 15, Query Library ID)
// RESP1 PN[7:0]        Final 2 digits of part number (HEX).
// RESP2 FWMAJOR[7:0]   Firmware Major Revision (ASCII).
// RESP3 FWMINOR[7:0]   Firmware Minor Revision (ASCII).
// RESP4 RESERVED[7:0]  Reserved, various values.
// RESP5 RESERVED[7:0]  Reserved, various values.
// RESP6 CHIPREV[7:0]   Chip Revision (ASCII).
// RESP7 LIBRARYID[7:0] Library Revision (HEX).
#define POWER_UP_REQ_SIZE   3
#define POWER_UP_RESP1_SIZE 1
#define POWER_UP_RESP2_SIZE 8

/* Returns revision information on the device. */
#define GET_REV         0x10
// RESP1 PN[7:0]        Final 2 digits of Part Number (HEX).
// RESP2 FWMAJOR[7:0]   Firmware Major Revision (ASCII).
// RESP3 FWMINOR[7:0]   Firmware Minor Revision (ASCII).
// RESP4 PATCHH[7:0]    Patch ID High Byte (HEX).
// RESP5 PATCHL[7:0]    Patch ID Low Byte (HEX).
// RESP6 CMPMAJOR[7:0]  Component Major Revision (ASCII).
// RESP7 CMPMINOR[7:0]  Component Minor Revision (ASCII).
// RESP8 CHIPREV[7:0]   Chip Revision (ASCII).
// RESP9-14 Reserved
// RESP15 CID[7:0]      CID (Si4705/06 only).
#define GET_REV_REQ_SIZE    1
#define GET_REV_RESP_SIZE   16

/* Power down device. */
#define POWER_DOWN      0x11
#define POWER_DOWN_REQ_SIZE    1
#define POWER_DOWN_RESP_SIZE   1

/* Sets the value of a property. */
#define SET_PROPERTY    0x12
// ARG1 Always write to 0.
// ARG2 Property High Byte. PROPH[7:0]
// ARG3 Property Low Byte. PROPL[7:0]
// ARG4 Property Value High Byte. PROPDH[7:0]
// ARG5 Property Value Low Byte. PROPDL[7:0]
#define SET_PROPERTY_REQ_SIZE    6
#define SET_PROPERTY_RESP_SIZE   1

/* Retrieves a property’s value. */
#define GET_PROPERTY    0x13
// ARG1 Always write to 0.
// ARG2 Property High Byte. PROPH[7:0]
// ARG3 Property Low Byte. PROPL[7:0]

// RESP1 Reserved
// RESP2 PROPDH[7:0] Property Value High Byte.
// RESP3 PROPDL[7:0] Property Value Low Byte.
#define GET_PROPERTY_REQ_SIZE    1
#define GET_PROPERTY_RESP_SIZE   1

/* Reads interrupt status bits. */
#define GET_INT_STATUS  0x14
#define GET_INT_STATUS_REQ_SIZE    1
#define GET_INT_STATUS_RESP_SIZE   1

/* Reserved command used for patch file downloads. */
#define PATCH_ARGS      0x15

/* Reserved command used for patch file downloads. */
#define PATCH_DAT       0x16

/* Selects the FM tuning frequency. */
#define FM_TUNE_FREQ    0x20
// ARG1
// Freeze Metrics During Alternate Frequency Jump.
#define FREEZE  0x2
// FAST Tuning. If set, executes fast and invalidated tune.
#define FAST    0x1
// ARG2 Tune Frequency High Byte. FREQH[7:0]
// ARG3 Tune Frequency Low Byte.  FREQL[7:0]
// ARG4 Antenna Tuning Capacitor  ANTCAP[7:0]
#define FM_TUNE_FREQ_REQ_SIZE   5
#define FM_TUNE_FREQ_RESP_SIZE  1

/* Begins searching for a valid frequency. */
#define FM_SEEK_START   0x21
// Seek Up/Down. Determines the direction of the search, either UP = 1, or DOWN = 0.
#define SEEKUP  0x8
// Wrap/Halt. Determines whether the seek should Wrap = 1
#define WRAP    0x4
#define FM_SEEK_START_REQ_SIZE   2
#define FM_SEEK_START_RESP_SIZE  1

/* Queries the status of previous FM_TUNE_FREQ or FM_SEEK_START command. */
#define FM_TUNE_STATUS  0x22
// Cancel seek. If set, aborts a seek currently in progress.
#define CANCEL  0x2
// Seek/Tune Interrupt Clear. If set, clears the seek/tune complete int status.
#define INTACK  0x1

// RESP1
// Band Limit. Reports if a seek hit the band limit
#define BLTF    0x80
// AFC Rail Indicator. Set if the AFC rails.
#define AFCRL   0x02
// Valid Channel.
#define VALID   0x01
// RESP2 READFREQH[7:0]     Read Frequency High Byte.
// RESP3 READFREQL[7:0]     Read Frequency Low Byte.
// RESP4 RSSI[7:0]          Received Signal Strength Indicator.
// RESP5 SNR[7:0]           SNR.
// RESP6 MULT[7:0]          Multipath.
// RESP7 READANTCAP[7:0]    Read Antenna Tuning Capacitor (Si4704/05/06/2x only).
#define FM_TUNE_STATUS_REQ_SIZE   2
#define FM_TUNE_STATUS_RESP_SIZE  8

/* Queries the status of the Received Signal Quality (RSQ) of the current ch. */
#define FM_RSQ_STATUS   0x23
// Interrupt Acknowledge. 0 = Interrupt status preserved. 1 = Clears
#define INTACK_RSQ  0x1

// RESP1
// Blend Detect Interrupt. 1 = Blend goes above or below the Blend threshold sets.
#define BLENDINT    0x80
// Multipath Detect High
#define MULTHINT    0x20
// Multipath Detect Low
#define MULTLINT    0x10
// SNR Detect High. 1 = Received SNR has exceeded above SNR high threshold.
#define SNRHINT     0x08
// SNR Detect Low. 1 = Received SNR has fallen below SNR low threshold.
#define SNRLINT     0x04
// RSSI Detect High. 1 = RSSI has exceeded above RSSI high threshold.
#define RSSIHINT    0x02
// RSSI Detect Low. 1 = RSSI has fallen below RSSI low threshold.
#define RSSILINT    0x01
// RESP2
// Soft Mute Indicator. Indicates soft mute is engaged.
#define SMUTE       0x3
// AFC Rail Indicator. Set if the AFC rails.
#define AFCRL_RSQ   0x2
// Valid Channel. Set if the channel is currently valid and would have been found
#define VALID_RSQ   0x1
// RESP3
// Pilot Indicator. Indicates stereo pilot presence.
#define PILOT_RSQ   0x80
// Stereo Blend Indicator. Indicates amount of stereo blend in%
#define STBLEND_MSK 0x7f
// RESP4 RSSI[7:0]      Contains the current receive signal strength (0–127 dBμV).
// RESP5 SNR[7:0]       Contains the current SNR metric (0–127 dB).
// RESP6 MULT[7:0]      Multipath Contains the current multipath metric.
// RESP7 FREQOFF[7:0]   Frequency Offset. Signed frequency offset (kHz).
#define FM_RSQ_STATUS_REQ_SIZE   2
#define FM_RSQ_STATUS_RESP_SIZE  8

/* Returns RDS information for current channel and reads an entry from RDS FIFO. */
#define FM_RDS_STATUS   0x24
// Status Only.
#define STATUSONLY  0x4
// Empty FIFO 1 = Clear RDS Receive FIFO.
#define MTFIFO      0x2
// Interrupt Acknowledge 1 = Clears RDSINT.
#define INTACK      0x1

// RESP1
// RDS New Block B. 1 = Valid Block B data has been received.
#define RDSNEWBLOCKB_RDS    0x20
// RDS New Block A. 1 = Valid Block A data has been received.
#define RDSNEWBLOCKA_RDS    0x10
// RDS Sync Found. 1 = Found RDS synchronization.
#define RDSSYNCFOUND_RDS    0x04
// RDS Sync Lost. 1 = Lost RDS synchronization.
#define RDSSYNCLOST_RDS     0x02
// RDS Received. 1 = FIFO filled to minimum number of groups set by RDSFIFOCNT.
#define RDSRECV_RDS         0x01
// RESP2
// Group Lost. 1 = One or more RDS groups discarded due to FIFO overrun.
#define GRPLOST 0x4
// RDS Sync. 1 = RDS currently synchronized.
#define RDSSYNC 0x1
// RESP3 RDSFIFOUSED[7:0]   RDS FIFO Used.
// RESP4 BLOCKA[15:8]       RDS Block A.
// RESP5 BLOCKA[7:0]
// RESP6 BLOCKB[15:8]       RDS Block B.
// RESP7 BLOCKB[7:0]
// RESP8 BLOCKC[15:8]       RDS Block C.
// RESP9 BLOCKC[7:0]
// RESP10 BLOCKD[15:8]      RDS Block D.
// RESP11 BLOCKD[7:0]
// RESP12
// RDS Block A Corrected Errors.
#define BLEA_NOERR          0x00
#define BLEA_1_2BITS        0x40
#define BLEA_3_5BITS        0x80
#define BLEA_UNCORRECTABLE  0xc0
// RDS Block B Corrected Errors.
#define BLEB_NOERR          0x00
#define BLEB_1_2BITS        0x10
#define BLEB_3_5BITS        0x20
#define BLEB_UNCORRECTABLE  0x30
// RDS Block C Corrected Errors.
#define BLEC_NOERR          0x00
#define BLEC_1_2BITS        0x04
#define BLEC_3_5BITS        0x08
#define BLEC_UNCORRECTABLE  0x0c
// RDS Block D Corrected Errors.
#define BLED_NOERR          0x00
#define BLED_1_2BITS        0x01
#define BLED_3_5BITS        0x02
#define BLED_UNCORRECTABLE  0x03
#define FM_RDS_STATUS_REQ_SIZE   2
#define FM_RDS_STATUS_RESP_SIZE  13

/* Queries the current AGC settings */
#define FM_AGC_STATUS   0x27
// RESP1
// This bit indicates whether the RF AGC is disabled or not
#define READ_RFAGCDIS   0x1
// RESP2
// These bits returns the value of the LNA GAIN index
#define READ_LNA_GAIN_INDEX_MSK 0x1f
#define FM_AGC_STATUS_REQ_SIZE   1
#define FM_AGC_STATUS_RESP_SIZE  3

/* Override AGC setting by disabling and forcing it to a fixed value */
#define FM_AGC_OVERRIDE 0x28
// ARG1
// This bit selects whether the RF AGC is disabled or not
#define RFAGCDIS    0x1
// ARG2
// These bits set the value of the LNA GAIN index
#define LNA_GAIN_INDEX_MSK  0x1f
#define FM_AGC_OVERRIDE_REQ_SIZE   3
#define FM_AGC_OVERRIDE_RESP_SIZE  1

/* Configures GPO1, 2, and 3 as output or Hi-Z. */
#define GPIO_CTL        0x80
// GPO3 Output Enable.
#define GPO3OEN 0x8
// GPO2 Output Enable.
#define GPO2OEN 0x4
// GPO1 Output Enable.
#define GPO1OEN 0x2
#define GPIO_CTL_REQ_SIZE   2
#define GPIO_CTL_RESP_SIZE  1

/* Sets GPO1, 2, and 3 output level (low or high). */
#define GPIO_SET        0x81
// GPO3 Output Level.
#define GPO3LEVEL 0x8
// GPO2 Output Level.
#define GPO2LEVEL 0x4
// GPO1 Output Level.
#define GPO1LEVEL 0x2
#define GPIO_SET_REQ_SIZE   2
#define GPIO_SET_RESP_SIZE  1

/*** FM/RDS Receiver Properties ***/

/* Enables interrupt sources. */
#define GPO_IEN                                 0x0001
// RSQ Interrupt Repeat. 1 = Interrupt generated even if RSQINT is already set.
#define RSQREP  0x800
// RDS Interrupt Repeat (Si4705/21/31/35/37/39/41/43/45/85-C40 and Si4732 Only).
#define RDSREP  0x400
// STC Interrupt Repeat. 1 = Interrupt generated even if STCINT is already set.
#define STCREP  0x100
// CTS Interrupt Enable. After PowerUp, this bit reflects the CTSIEN bit in ARG1 of PowerUp Command.
#define CTSIEN  0x080
// ERR Interrupt Enable. 1 = Interrupt generated when ERR is set.
#define ERRIEN  0x040
// RSQ Interrupt Enable. 1 = Interrupt generated when RSQINT is set.
#define RSQIEN  0x008
// RDS Interrupt Enable (Si4705/21/31/35/37/39/41/43/45/85-C40 and Si4732 Only).
#define RDSIEN  0x004
// Seek/Tune Complete Interrupt Enable. 1 = Interrupt generated when STCINT is set.
#define STCIEN  0x001

/* Configure digital audio outputs. */
#define DIGITAL_OUTPUT_FORMAT                   0x0102
// Digital Output DCLK Edge. 1 = use DCLK falling edge
#define OFALL                       0x80
// Digital Output Mode.
#define OMODE_I2S                   0x00
#define OMODE_LEFT_JUSTIFIED        0x30
#define OMODE_MSB_AT_FIRST_DCLK     0x40
#define OMODE_MSB_AT_SECOND_DCLK    0x60
// Digital Output Mono Mode. 0 = Use mono/stereo blend (per blend thresholds)
#define OMONO                       0x04
// Digital Output Audio Sample Precision.
#define OSIZE16BITS                 0x00
#define OSIZE20BITS                 0x01
#define OSIZE24BITS                 0x02
#define OSIZE8BITS                  0x03

/* Configure digital audio output sample rate. */
#define DIGITAL_OUTPUT_SAMPLE_RATE              0x0104
// 32–48 ksps. 0 to disable digital audio output.

/* Sets frequency of reference clock in Hz. */
#define REFCLK_FREQ                             0x0201
// The allowed REFCLK frequency range is between 31130 and 34406 Hz

/* Sets the prescaler value for RCLK input. */
#define REFCLK_PRESCALE                         0x0202
// 1 = DCLK pin is clock source.
#define RCLKSEL     0x1000
// Prescaler for Reference Clock. Integer number used to divide clock frequency down to REFCLK frequency.
#define REFCLKP_MSK     0x0fff
#define REFCLKP_SET(n)  (n&REFCLKP_MSK)

/* Sets deemphasis time constant. Default is 75 μs. */
#define FM_DEEMPHASIS                           0x1100
// FM De-Emphasis.
#define DEEMPH75US  0x2
#define DEEMPH50US  0x1

/* Selects bandwidth of channel filter applied at the demodulation stage. */
#define FM_CHANNEL_FILTER                       0x1102
//
#define FM_CHANNEL_FILTER_AUTO  0x0
#define FM_CHANNEL_FILTER110KHZ 0x1
#define FM_CHANNEL_FILTER84KHZ  0x2
#define FM_CHANNEL_FILTER60KHZ  0x3
#define FM_CHANNEL_FILTER40KHZ  0x4

/* Selects bandwidth of channel filter applied at the demodulation stage. */
#define FM_BLEND_STEREO_THRESHOLD               0x1105
// RSSI threshold below which the audio output goes into a blend mode.
#define STTHRESH_MSK    0x7f

/* Sets RSSI threshold for mono blend (Full mono below th, blend above th). */
#define FM_BLEND_MONO_THRESHOLD                 0x1106
// RSSI threshold below which the audio output goes into full mono mode.
#define MONOTHRESH_MSK  0x7f

/* Selects the antenna type and the pin to which it is connected. */
#define FM_ANTENNA_INPUT                        0x1107
// 1 = Use TXO/LPI pin for embedded (short) antenna
#define FMTXO   0x1

/* Sets the maximum freq error allowed before set the AFC rail (AFCRL) indicator. */
#define FM_MAX_TUNE_ERROR                       0x1108
// Maximum tuning error allowed before setting the AFC Rail Indicator ON.
#define FMMAXTUNEERR_MSK    0xff

/* Configures interrupt related to Received Signal Quality metrics. */
#define FM_RSQ_INT_SOURCE                       0x1200
// Interrupt Source Enable: Blend.
#define BLENDIEN    0x80
// Interrupt Source Enable: Multipath High
#define MULTHIEN    0x20
// Interrupt Source Enable: Multipath Low
#define MULTLIEN    0x10
// Interrupt Source Enable: SNR High.
#define SNRHIEN     0x08
// Interrupt Source Enable: SNR Low.
#define SNRLIEN     0x04
// Interrupt Source Enable: RSSI High.
#define RSSIHIEN    0x02
// Interrupt Source Enable: RSSI Low.
#define RSSILIEN    0x01

/* Sets high threshold for SNR interrupt. */
#define FM_RSQ_SNR_HI_THRESHOLD                 0x1201
// FM RSQ SNR High Threshold. Threshold which triggers the RSQ interrupt if the SNR is above this th.
#define SNRH_FSK    0x7f

/* Sets low threshold for SNR interrupt. */
#define FM_RSQ_SNR_LO_THRESHOLD                 0x1202
// FM RSQ SNR Low Threshold. Threshold which triggers the RSQ interrupt if the SNR is below this th.
#define SNRL_SFT    0x7f

/* Sets high threshold for RSSI interrupt. */
#define FM_RSQ_RSSI_HI_THRESHOLD                0x1203
// FM RSQ RSSI High Threshold. Threshold which triggers the RSQ interrupt if the RSSI is above this th.
#define RSSIH_SFT   0x7f

/* Sets low threshold for RSSI interrupt. */
#define FM_RSQ_RSSI_LO_THRESHOLD                0x1204
// FM RSQ RSSI Low Threshold. Threshold which triggers the RSQ interrupt if the RSSI is below this th.
#define RSSIL_SFT   0x7f

/* Sets high threshold for multipath interrupt. */
#define FM_RSQ_MULTIPATH_HI_THRESHOLD           0x1205
// FM RSQ Multipath High Threshold. Threshold which triggers the RSQ int if the Multipath is above th.
#define MULTH_SFT   0x7f

/* Sets low threshold for multipath interrupt. */
#define FM_RSQ_MULTIPATH_LO_THRESHOLD           0x1206
// FM RSQ Multipath Low Threshold. Threshold which triggers the RSQ int if the Multipath is below  th.
#define MULTL_SFT   0x7f

/* Sets the blend threshold for blend interrupt when boundary is crossed. */
#define FM_RSQ_BLEND_THRESHOLD                  0x1207
// Pilot Indicator.
#define PILOT           0x80
// FM RSQ Blend Threshold. This is a boundary cross threshold.
#define BLEND_MSK       0x7f
#define BLEND_SET(n)    (n&BLEND_MSK)

/* Sets the attack and decay rates when entering and leaving soft mute. */
#define FM_SOFT_MUTE_RATE                       0x1300
// ater values increase rates, and lower values decrease rates.
#define SMRATE_MSK  0xff

/* att slope during soft mute in dB att per dB SNR below the soft mute SNR th. */
#define FM_SOFT_MUTE_SLOPE                      0x1301
//
#define SMSLOPE_MSK 0xff

/* Sets maximum attenuation during soft mute (dB). */
#define FM_SOFT_MUTE_MAX_ATTENUATION            0x1302
// Specified in units of dB in 1 dB steps (0–31).
#define SMATTN_MSK  0x1f

/* Sets SNR threshold to engage soft mute. */
#define FM_SOFT_MUTE_SNR_THRESHOLD              0x1303
// FM Soft Mute SNR Threshold. Specified in units of dB in 1 dB steps (0–15).
#define SMTHR_MSK   0xf

/* Sets soft mute release rate. */
#define FM_SOFT_MUTE_RELEASE_RATE               0x1304
// Smaller values provide slower release and larger values provide faster release.
#define RELEASE_MSK 0x7fff

/* Sets soft mute attack rate. */
#define FM_SOFT_MUTE_ATTACK_RATE                0x1305
// Smaller values provide slower attack and larger values provide faster attack.
#define ATTACK_MSK  0x7fff

/* Sets the bottom of the FM band for seek. */
#define FM_SEEK_BAND_BOTTOM                     0x1400
// Specified in units of 10 kHz.

/* Sets the top of the FM band for seek. */
#define FM_SEEK_BAND_TOP                        0x1401
// Specified in units of 10 kHz.

/* Selects frequency spacing for FM seek. */
#define FM_SEEK_FREQ_SPACING                    0x1402
// Specified in units of 10 kHz.
#define SKSPACE_MSK 0x1f

/* Sets the SNR threshold for a valid FM Seek/Tune. */
#define FM_SEEK_TUNE_SNR_THRESHOLD              0x1403
// Specified in units of dB in 1 dB steps (0–127).
#define SKSNR_MSK   0x7f

/* Sets the RSSI threshold for a valid FM Seek/Tune. */
#define FM_SEEK_TUNE_RSSI_TRESHOLD              0x1404
// Specified in units of dBμV in 1 dBμV steps (0–127).
#define SKRSSI_MSK  0x7f

/* Configures RDS interrupt behavior. */
#define FM_RDS_INT_SOURCE                       0x1500
// RDS New Block B Found (Si4706, Si474x, and Si4705/31/35/85-D50 and later, and Si4732 only)
#define RDSNEWBLOCKB    0x20
// RDS New Block A Found (Si4706,Si474x and Si4705/31/35/85-D50 and later, and Si4732 only)
#define RDSNEWBLOCKA    0x10
// RDS Sync Found. If set, generate RDSINT when RDS gains synchronization.
#define RDSSYNCFOUND    0x04
// RDS Sync Lost. If set, generate RDSINT when RDS loses synchronization.
#define RDSSYNCLOST     0x02
// RDS Received. If set, generate RDSINT when RDS FIFO has at least FM_RDS_INT_FIFO_COUNT entries.
#define RDSRECV         0x01

/* minimum number of RDS groups stored in the receive FIFO required before RDSRECV*/
#define FM_RDS_INT_FIFO_COUNT                   0x1501
// Minimum number of RDS groups stored in the RDS FIFO before RDSRECV is set.
#define RDSFIFOCNT_MSk  0xff

/* Configures RDS setting. */
#define FM_RDS_CONFIG                           0x1502
// Block Error Threshold BLOCKA.
#define BLETHA_0BITS            0x0000
#define BLETHA_1_2BITS          0x4000
#define BLETHA_3_5BITS          0x8000
#define BLETHA_UNCORRECTABLE    0xc000
// Block Error Threshold BLOCKB.
#define BLETHB_0BITS            0x0000
#define BLETHB_1_2BITS          0x1000
#define BLETHB_3_5BITS          0x2000
#define BLETHB_UNCORRECTABLE    0x3000
// Block Error Threshold BLOCKC.
#define BLETHC_0BITS            0x0000
#define BLETHC_1_2BITS          0x0400
#define BLETHC_3_5BITS          0x0800
#define BLETHC_UNCORRECTABLE    0x0c00
// Block Error Threshold BLOCKD.
#define BLETHD_0BITS            0x0000
#define BLETHD_1_2BITS          0x0100
#define BLETHD_3_5BITS          0x0200
#define BLETHD_UNCORRECTABLE    0x0300
// RDS Processing Enable.
#define RDSEN                   0x0001

/* Sets the confidence level threshold for each RDS block. */
#define FM_RDS_CONFIDENCE                       0x1503
// Selects decoder error rate threshold for Block B.
#define CONFIDENCEB_MSK     0xf00
#define CONFIDENCEB_SFT     8
#define CONFIDENCEB_SET(n)  ((n<<CONFIDENCEB_SFT)&CONFIDENCEB_MSK)
// Selects decoder error rate threshold for Block C.
#define CONFIDENCEC_MSK     0x0f0
#define CONFIDENCEC_SFT     4
#define CONFIDENCEC_SET(n)  ((n<<CONFIDENCEC_SFT)&CONFIDENCEC_MSK)
// Selects decoder error rate threshold for Block D.
#define CONFIDENCED_MSK     0x00f
#define CONFIDENCED_SET(n)  (n&CONFIDENCED_MSK)

/* Sets the AGC attack rate. */
#define FM_AGC_ATTACK_RATE                      0x1700
// AGC Attack Rate (dB/s) = 6000 / ATTACK[7:0]
#define ATTACK_AGC_MSK  0xff

/* Sets the AGC release rate. */
#define FM_AGC_RELEASE_RATE                     0x1701
// AGC Release Rate (dB/s) = 6000 / RELEASE[7:0]
#define RELEASE_AGC_MSK 0xff

/* Sets RSSI threshold for stereo blend. */
#define FM_BLEND_RSSI_STEREO_THRESHOLD          0x1800
// Sets RSSI threshold for stereo blend (Full stereo above threshold, blend below threshold).
#define STRTHRESH   0x7f

/* Sets RSSI threshold for mono blend */
#define FM_BLEND_RSSI_MONO_THRESHOLD            0x1801
// Sets RSSI threshold for mono blend (Full mono below threshold, blend above threshold).
#define MONOTHRESH_BLEND_MSK    0x7f

/* Sets the stereo to mono attack rate for RSSI based blend. */
#define FM_BLEND_RSSI_ATTACK_RATE               0x1802
// Smaller values provide slower attack and larger values provide faster attack.

/* Sets the mono to stereo release rate for RSSI based blend. */
#define FM_BLEND_RSSI_RELEASE_RATE              0x1803
// Smaller values provide slower release and larger values provide faster release.

/* Sets SNR threshold for stereo blend */
#define FM_BLEND_SNR_STEREO_THRESHOLD           0x1804
// To force stereo, set this to 0. To force mono, set this to 127.
#define STRTHRESH_MSK   0x7f

/* Sets SNR threshold for mono blend */
#define FM_BLEND_SNR_MONO_THRESHOLD             0x1805
// To force stereo, set to 0. To force mono, set to 127.
#define MONOTHRESH_MSK  0x7f

/* Sets the stereo to mono attack rate for SNR based blend. */
#define FM_BLEND_SNR_ATTACK_RATE                0x1806
// Smaller values provide slower attack and larger values provide faster attack.

/* Sets the mono to stereo release rate for SNR based blend. */
#define FM_BLEND_SNR_RELEASE_RATE               0x1807
// Smaller values provide slower release and larger values provide faster release.

/* Sets multipath threshold for stereo blend */
#define FM_BLEND_MULTIPATH_STEREO_THRESHOLD     0x1808
// To force stereo, set to 100. To force mono, set to 0.
#define STRTHRESH_MSK   0x7f

/* Sets Multipath threshold for mono blend */
#define FM_BLEND_MULTIPATH_MONO_THRESHOLD       0x1809
// To force stereo, set to 100. To force mono, set to 0.
#define MONOTHRESH_MSK  0x7f

/* Sets the stereo to mono attack rate for Multipath based blend. */
#define FM_BLEND_MULTIPATH_ATTACK_RATE          0x180a
// Smaller values provide slower attack and larger values provide faster attack.

/* Sets the mono to stereo release rate for Multipath based blend. */
#define FM_BLEND_MULTIPATH_RELEASE_RATE         0x180b
// Smaller values provide slower release and larger values provide faster release.

/* Sets the maximum amount of stereo separation */
#define FM_BLEND_MAX_STEREO_SEPARATION          0x180c
// Maximum Stereo Separation.
#define MAX_SEP_DISABLED    0x0
#define MAX_SEP_12DB        0x1
#define MAX_SEP_15DB        0x2
#define MAX_SEP_18DB        0x3
#define MAX_SEP_21DB        0x4
#define MAX_SEP_24DB        0x5
#define MAX_SEP_27DB        0x6
#define MAX_SEP_30DB        0x7

/* Sets the threshold for detecting impulses in dB above the noise floor. */
#define FM_NB_DETECT_THRESHOLD                  0x1900
// The CTS bit (and optional interrupt) is set when it is safe to send the next command.

/* Interval in ms that original samples are replaced by interpol clean samples. */
#define FM_NB_INTERVAL                          0x1901
// The CTS bit (and optional interrupt) is set when it is safe to send the next command.

/* Noise blanking rate in 100 Hz units. Default value is 64. */
#define FM_NB_RATE                              0x1902
// The CTS bit (and optional interrupt) is set when it is safe to send the next command.

/* Sets the bandwidth of the noise floor estimator Default value is 300. */
#define FM_NB_IIR_FILTER                        0x1903
// The CTS bit (and optional interrupt) is set when it is safe to send the next command.

/* Delay in micro-seconds before applying impulse blanking to the orig samples. */
#define FM_NB_DELAY                             0x1904
// The CTS bit (and optional interrupt) is set when it is safe to send the next command.

/* Sets the SNR level at which hi-cut begins to band limit. */
#define FM_HICUT_SNR_HIGH_THRESHOLD             0x1a00
// The CTS bit (and optional interrupt) is set when it is safe to send the next command.
#define SNR_HIGH_MSK    0x7f

/* Sets the SNR level at which hi-cut reaches maximum band limiting. */
#define FM_HICUT_SNR_LOW_THRESHOLD              0x1a01
// The default is 15 dB.
#define SNR_LOW_MSK 0x7f

/* Sets the rate at which hi-cut lowers the cut-off frequency. */
#define FM_HICUT_ATTACK_RATE                    0x1a02
// The default is 20000 (approximately 3 ms).

/* Sets the rate at which hi-cut increases the cut-off frequency. */
#define FM_HICUT_RELEASE_RATE                   0x1a03
// The default is 20 (approximately 3.3 s).

/* Sets the MULTIPATH level at which hi-cut begins to band limit. */
#define FM_HICUT_MULTIPATH_TRIGGER_THRESHOLD    0x1a04
// The default is 20%.
#define MULT_TRIGGER_MSK    0x7f

/* Sets the MULTIPATH level at which hi-cut reaches maximum band limiting. */
#define FM_HICUT_MULTIPATH_END_THRESHOLD        0x1a05
// The default is 60%.
#define MULT_END_MSK    0x7f

/* Sets the maximum band limit freq for hi-cut and also sets the max audio freq. */
#define FM_HICUT_CUTOFF_FREQUENCY               0x1a06
// Maximum Audio Frequency.
#define MAXIMUM_AUDIO_FREQUENCY_MAX_BW  0x00
#define MAXIMUM_AUDIO_FREQUENCY2KHZ     0x10
#define MAXIMUM_AUDIO_FREQUENCY3KHZ     0x20
#define MAXIMUM_AUDIO_FREQUENCY4KHZ     0x30
#define MAXIMUM_AUDIO_FREQUENCY5KHZ     0x40
#define MAXIMUM_AUDIO_FREQUENCY6KHZ     0x50
#define MAXIMUM_AUDIO_FREQUENCY8KHZ     0x60
#define MAXIMUM_AUDIO_FREQUENCY11KHZ    0x70
// Frequency.
#define FREQUENCY_DISABLED  0x00
#define FREQUENCY2KHZ       0x01
#define FREQUENCY3KHZ       0x02
#define FREQUENCY4KHZ       0x03
#define FREQUENCY5KHZ       0x04
#define FREQUENCY6KHZ       0x05
#define FREQUENCY8KHZ       0x06
#define FREQUENCY11KHZ      0x07

/* Sets the output volume. */
#define RX_VOLUME                               0x4000
// Sets the output volume level, 63 max, 0 min. Default is 63.
#define VOL_MSK 0x3f

/* Mutes the audio output. L and R audio outputs may be muted independently. */
#define RX_HARD_MUTE                            0x4001
// Mutes L Audio Output.
#define LMUTE   0x2
// Mutes R Audio Output.
#define RMUTE   0x1

#endif
