#ifndef H_SI47XX_AM_REGS
#define H_SI47XX_AM_REGS
/*
 * Si4730 broadcast AM/FM receiver registers.
 * Commands and Properties for the AM/SW/LW Receiver
 * (Si4730/31/32/34/35/36/37/40/41/42/43/44/45)
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/* Status Response */
// Clear to Send. 1 = Clear to send next command.
#define CTS     0x80
// Error.
#define ERR     0x40
// Received Signal Quality Interrupt.
#define RSQINT  0x08
// Seek/Tune Complete Interrupt. 1 = Tune complete has been triggered.
#define STCINT  0x01

/*** AM/SW/LW Receiver Commands ***/

/* Power up device and mode selection. */
#define POWER_UP        0x01
// ARG1
// CTS Interrupt Enable.
#define CTSIEN_UP   0x80
// GPO2 Output Enable.
#define GPO2OEN_UP  0x40
// Patch Enable. 1 = Copy NVM to RAM, but do not boot. After CTS has been set
#define PATCH       0x20
// Crystal Oscillator Enable. 1 = Use crystal oscillator
#define XOSCEN      0x10
// Function.
#define FUNC_AM     0x01
#define FUNC_ID     0x0f
// ARG2
// Application Setting
#define OPMODE_ANALOG_OUT           0x05
#define OPMODE_DIGITAL_LR_OUT       0x0b
#define OPMODE_DIGITAL_OUT          0xb0
#define OPMODE_ANALOG_DIGITAL_OUT   0xb5

// RESP1 PN[7:0]        Final 2 digits of Part Number (HEX).
// RESP2 FWMAJOR[7:0]   Firmware Major Revision (ASCII).
// RESP3 FWMINOR[7:0]   Firmware Minor Revision (ASCII).
// RESP4 RESERVED[7:0]  Reserved, various values.
// RESP5 RESERVED[7:0]  Reserved, various values.
// RESP6 CHIPREV[7:0]   Component Major Revision (ASCII).
// RESP7 LIBRARYID[7:0] Component Minor Revision (ASCII).
#define POWER_UP_REQ_SIZE   3
#define POWER_UP_RESP_SIZE  8

/* Returns revision information on the device. */
#define GET_REV         0x10
// RESP1 PN[7:0]        Final 2 digits of Part Number (HEX).
// RESP2 FWMAJOR[7:0]   Firmware Major Revision (ASCII).
// RESP3 FWMINOR[7:0]   Firmware Minor Revision (ASCII).
// RESP4 PATCHH[7:0]    Patch ID High Byte (HEX).
// RESP5 PATCHL[7:0]    Patch ID Low Byte (HEX).
// RESP6 CMPMAJOR[7:0]  Component Major Revision (ASCII).
// RESP7 CMPMINOR[7:0]  Component Minor Revision (ASCII).
// RESP8 CHIPREV[7:0]   Chip Revision (ASCII).
#define GET_REV_REQ_SIZE    1
#define GET_REV_RESP_SIZE   9

/* Power down device. */
#define POWER_DOWN      0x11
#define POWER_DOWN_REQ_SIZE     1
#define POWER_DOWN_RESP_SIZE    1

/* Sets the value of a property. */
#define SET_PROPERTY    0x12
// ARG1 Reserved
// ARG2 Property High Byte. PROPH[7:0]
// ARG3 Property Low Byte. PROPL[7:0]
// ARG4 Property Value High Byte. PROPDH[7:0]
// ARG5 Property Value Low Byte. PROPDL[7:0]
#define SET_PROPERTY_REQ_SIZE   6
#define SET_PROPERTY_RESP_SIZE  1

/* Retrieves a property’s value. */
#define GET_PROPERTY    0x13
// ARG1 Reserved
// ARG2 Property High Byte. PROPH[7:0]
// ARG3 Property Low Byte. PROPL[7:0]

// RESP1 Reserved
// RESP2 PROPDH[7:0] Property Value High Byte.
// RESP3 PROPDL[7:0] Property Value High Byte.
#define GET_PROPERTY_REQ_SIZE   4
#define GET_PROPERTY_RESP_SIZE  4

/* Read interrupt status bits. */
#define GET_INT_STATUS  0x14
#define GET_INT_STATUS_REQ_SIZE     1
#define GET_INT_STATUS_RESP_SIZE    1

/* Reserved command used for patch file downloads. */
#define PATCH_ARGS      0x15

/* Reserved command used for patch file downloads. */
#define PATCH_DATA      0x16

/* Tunes to a given AM frequency. */
#define AM_TUNE_FREQ    0x40
// ARG1
// FAST Tuning.
#define FAST    0x1
// ARG2 Tune Frequency High Byte. FREQH[7:0]
// ARG3 Tune Frequency Low Byte. FREQL[7:0]
// ARG4 Antenna Tuning Capacitor High Byte. ANTCAPH[15:8]
// ARG5 Antenna Tuning Capacitor Low Byte. ANTCAPL[7:0]
#define AM_TUNE_FREQ_REQ_SIZE   6
#define AM_TUNE_FREQ_RESP_SIZE  1

/* Begins searching for a valid frequency. */
#define AM_SEEK_START   0x41
// ARG1
// Seek Up/Down. Determines the direction of the search, either UP = 1, or DOWN = 0.
#define SEEKUP  0x8
// Wrap/Halt. Determines whether the seek should Wrap = 1, or Halt = 0
#define WRAP    0x4
// ARG2 Reserved
// ARG3 Reserved
// ARG4 ANTCAPH[15:8]   Antenna Tuning Capacitor High Byte.
// ARG5 ANTCAPL[7:0]    Antenna Tuning Capacitor Low Byte.
#define AM_SEEK_START_REQ_SIZE   6
#define AM_SEEK_START_RESP_SIZE  1

/* Queries the status of the already issued AM_TUNE_FREQ or AM_SEEK_START cmmd. */
#define AM_TUNE_STATUS  0x42
// Cancel seek. If set, aborts a seek currently in progress.
#define CANCEL  0x2
// Seek/Tune Interrupt Clear.
#define INTACK  0x1

// RESP1
// Band Limit. Reports if a seek hit the band limit
#define BLTF    0x80
// AFC Rail Indicator. Set if the AFC rails.
#define AFCRL   0x02
// Valid Channel. Set if the ch is currently valid.
#define VALID   0x01
// RESP2 READFREQH[7:0]     Read Frequency High Byte.
// RESP3 READFREQL[7:0]     Read Frequency Low Byte.
// RESP4 RSSI[7:0]          Received Signal Strength Indicator.
// RESP5 SNR[7:0]           SNR.
// RESP6 READANTCAPH[15:8]  Read Antenna Tuning Capacitor High Byte.
// RESP7 READANTCAPL[7:0]   Read Antenna Tuning Capacitor Low Byte.
#define AM_TUNE_STATUS_REQ_SIZE     2
#define AM_TUNE_STATUS_RESP_SIZE    8

/* Queries the status of the Received Signal Quality (RSQ) for the current ch. */
#define AM_RSQ_STATUS   0x43
// Interrupt Acknowledge. 0 = Interrupt status preserved.
#define INTACK  0x1

// RESP1
// SNR Detect High. 1 = Received SNR has exceeded above SNR high threshold.
#define SNRHINT     0x8
// SNR Detect Low. 1 = Received SNR has exceeded below SNR low threshold.
#define SNRLINT     0x4
// RSSI Detect High. 1 = RSSI has exceeded above RSSI high threshold.
#define RSSIHINT    0x2
// RSSI Detect Low. 1 = RSSI has exceeded below RSSI low threshold.
#define RSSILINT    0x1
// RESP2
// Soft Mute Indicator. Indicates soft mute is engaged.
#define SMUTE       0x8
// AFC Rail Indicator. Set if the AFC rails.
#define AFCRL_ST    0x2
// Valid Channel. ch is currently valid and would have been found during a seek.
#define VALID_ST    0x1
// RESP3 Reserved
// RESP4 RSSI[7:0]  Received Signal Strength Indicator.
// RESP5 SNR[7:0]   SNR.
#define AM_RSQ_STATUS_REQ_SIZE  2
#define AM_RSQ_STATUS_RESP_SIZE 6

/* Queries the current AGC settings. */
#define AM_AGC_STATUS   0x47
// RESP1
// AM AGC Disable
#define AMAGCDIS    0x1
// RESP2 AMAGCNDX[7:0] AM AGC Index
#define AM_AGC_STATUS_REQ_SIZE  1
#define AM_AGC_STATUS_RESP_SIZE 3

/* Overrides AGC settings by disabling and forcing it to a fixed value. */
#define AM_AGC_OVERRIDE 0x48
// ARG1
// AM AGC Disable
#define AMAGCDIS    0x1
// ARG2 AM AGC Index AMAGCNDX[7:0]
#define AM_AGC_OVERRIDE_REQ_SIZE  3
#define AM_AGC_OVERRIDE_RESP_SIZE 1

/* Configures GPO1, 2, and 3 as output or Hi-Z. */
#define GPIO_CTL        0x80
// GPO3 Output Enable.
#define GPO3OEN 0x8
// GPO2 Output Enable.
#define GPO2OEN 0x4
// GPO1 Output Enable.
#define GPO1OEN 0x2
#define GPIO_CTL_REQ_SIZE   2
#define GPIO_CTL_RESP_SIZE  1

/* Sets GPO1, 2, and 3 output level (low or high). */
#define GPIO_SET        0x81
// GPO3 Output Level.
#define GPO3LEVEL 0x8
// GPO2 Output Level.
#define GPO2LEVEL 0x4
// GPO1 Output Level.
#define GPO1LEVEL 0x2
#define GPIO_SET_REQ_SIZE   2
#define GPIO_SET_RESP_SIZE  1

/*** AM/SW/LW Receiver Properties ***/

/* Enables interrupt sources. */
#define GPO_IEN                                 0x0001
// RSQ Interrupt Repeat. 1 = Interrupt generated even if RSQINT is already set.
#define RSQREP  0x800
// STC Interrupt Repeat. 1 = Interrupt generated even if STCINT is already set.
#define STCREP  0x100
// CTS Interrupt Enable. After PowerUp, this bit refl the CTSIEN bit in ARG1 of cmd.
#define CTSIEN  0x080
// ERR Interrupt Enable. 1 = Interrupt generated when ERR is set.
#define ERRIEN  0x040
// RSQ Interrupt Enable. 1 = Interrupt generated when RSQINT is set.
#define RSQIEN  0x008
// Seek/Tune Complete Interrupt Enable. 1 = Interrupt generated when STCINT is set.
#define STCIEN  0x001

/* Configure digital audio outputs */
#define DIGITAL_OUTPUT_FORMAT                   0x0102
// Digital Output DCLK Edge. 1 = use DCLK falling edge
#define OFALL                       0x80
// Digital Output Mode.
#define OMODE_I2S                   0x00
#define OMODE_LEFT_JUSTIFIED        0x30
#define OMODE_MSB_AT_FIRST_DCLK     0x40
#define OMODE_MSB_AT_SECOND_DCLK    0x60
// Digital Output Mono Mode. 0 = Use mono/stereo blend (per blend thresholds)
#define OMONO                       0x04
// Digital Output Audio Sample Precision.
#define OSIZE16BITS                 0x00
#define OSIZE20BITS                 0x01
#define OSIZE24BITS                 0x02
#define OSIZE8BITS                  0x03

/* Configure digital audio output sample rate */
#define DIGITAL_OUTPUT_SAMPLE_RATE              0x0104
// 32–48 ksps. 0 to disable digital audio output.

/* Sets frequency of reference clock in Hz. */
#define REFCLK_FREQ                             0x0201
// The allowed REFCLK frequency range is between 31130 and 34406 Hz

/* Sets the prescaler value for RCLK input. */
#define REFCLK_PRESCALE                         0x0202
// 1 = DCLK pin is clock source.
#define RCLKSEL     0x1000
// Prescaler for Reference Clock. Integer num used to div clock freq down to REFCLK freq.
#define REFCLKP_MSK     0x0fff
#define REFCLKP_SET(n)  (n&REFCLKP_MSK)

/* Sets deemphasis time constant. Can be set to 50 μs. */
#define AM_DEEMPHASIS                           0x3100
// AM De-Emphasis. 1 = 50 μs. 0 = Disabled.
#define DEEMPH  0x1

/* Selects the bandwidth of the channel filter for AM reception. */
#define AM_CHANNEL_FILTER                       0x3102
// Enables the AM Power Line Noise Rejection Filter
#define AMPLFLT 0x100
// AM Channel Filter.
#define AMCHFILT6KHZ
#define AMCHFILT4KHZ
#define AMCHFILT3KHZ
#define AMCHFILT2KHZ
#define AMCHFILT1KHZ
#define AMCHFILT1P8KHZ
#define AMCHFILT2P5KHZ

/* Sets the maximum gain for automatic volume control. */
#define AM_AUTOMATIC_VOLUME_CONTROL_MAX_GAIN    0x3103
// Automatic Volume Control Max Gain.
#define AVC_MAXGAIN_MSK 0x7fff

/* Sets the SW AFC pull-in range. */
#define AM_MODE_AFC_SW_PULL_IN_RANGE            0x3104
// The SW pull-in range expressed relative to the tuned frequency.

/* Sets the SW AFC lock-in. */
#define AM_MODE_AFC_SW_LOCK_IN_RANGE            0x3105
// The SW lock-in range expressed relative to the tuned frequency.

/* Configures interrupt related to Received Signal Quality metrics. */
#define AM_RSQ_INTERRUPTS                       0x3200
// Interrupt Source Enable: SNR High.
#define SNRHIEN     0x8
// Interrupt Source Enable: SNR Low.
#define SNRLIEN     0x4
// Interrupt Source Enable: RSSI High.
#define RSSIHIEN    0x2
// Interrupt Source Enable: RSSI Low.
#define RSSILIEN    0x1

/* Sets high threshold for SNR interrupt. */
#define AM_RSQ_SNR_HIGH_THRESHOLD               0x3201
// Specified in units of dB in 1 dB steps (0–127). Default is 0 dB.
#define SNRH_MSK    0x7f

/* Sets low threshold for SNR interrupt. */
#define AM_RSQ_SNR_LOW_THRESHOLD                0x3202
// Specified in units of dB in 1 dB steps (0–127). Default is 0 dB.
#define SNRL_MSK    0x7f

/* Sets high threshold for RSSI interrupt. */
#define AM_RSQ_RSSI_HIGH_THRESHOLD              0x3203
// Specified in units of dBμV in 1 dB steps (0–127). Default is 0 dBμV.
#define RSSIH_MSK   0x7f

/* Sets low threshold for RSSI interrupt. */
#define AM_RSQ_RSSI_LOW_THRESHOLD               0x3204
// Specified in units of dBμV in 1 dB steps (0–127). Default is 0 dBμV.
#define RSSIL_MSK   0x7f

/* Sets the attack and decay rates when entering or leaving soft mute. */
#define AM_SOFT_MUTE_RATE                       0x3300
// Determines how quickly the AM goes into soft mute when soft mute is enabled.

/* Sets the AM soft mute slope. Default value is a slope of 1. */
#define AM_SOFT_MUTE_SLOPE                      0x3301
// Set soft mute att slope in dB attenuation per dB SNR below the soft mute SNR th.
#define SMSLOPE_MSK 0xf

/* Sets maximum attenuation during soft mute (dB). */
#define AM_SOFT_MUTE_MAX_ATTENUATION            0x3302
// Specified in units of dB. Default maximum attenuation is 8 dB.
#define SMATTN_MSK  0x3f

/* Sets SNR threshold to engage soft mute. */
#define AM_SOFT_MUTE_SNR_THRESHOLD              0x3303
// The SNR th for a tuned freq below which soft mute is engaged provided the val.
#define SMTHR_MSK   0x3f

/* Sets softmute release rate. */
#define AM_SOFT_MUTE_RELEASE_RATE               0x3304
// Smaller values provide slower release and larger values provide faster release.
#define RELEASE_MSK 0x7fff

/* Sets software attack rate. */
#define AM_SOFT_MUTE_ATTACK_RATE                0x3305
// Smaller values provide slower attack and larger values provide faster attack
#define ATTACK_MSK  0x7fff

/* Sets the bottom of the AM band for seek. Default is 520. */
#define AM_SEEK_BAND_BOTTOM                     0x3400
// Sets the lower boundary for the AM band in kHz.

/* Sets the top of the AM band for seek. Default is 1710. */
#define AM_SEEK_BAND_TOP                        0x3401
// Specify the higher boundary of the AM band when performing a seek.

/* Selects frequency spacing for AM seek. Default is 10 kHz spacing. */
#define AM_SEEK_FREQ_SPACING                    0x3402
// Sets the frequency spacing when performing a seek in the AM band.
#define AMSKSPACE_MSK   0xf


/* Sets the SNR threshold for a valid AM Seek/Tune. */
#define AM_SEEK_SNR_THRESHOLD                   0x3403
// SNR Threshold which determines if a valid channel has been found during Seek/Tune.
#define AMSKSNR_MSK 0x3f


/* Sets the RSSI threshold for a valid AM Seek/Tune. */
#define AM_SEEK_RSSI_THRESHOLD                  0x3404
// RSSI Threshold which determines if a valid channel has been found during Seek/Tune.
#define AMSKRSSI_MSK    0x3f


/* Sets the num of ms the high peak det must be exceeded before decreasing gain. */
#define AM_AGC_ATTACK_RATE                      0x3702
// Large values provide slower attack, and smaller values provide faster attack.
#define ATTACK_AGC_MSK  0xff


/* num of ms the low peak detector must not be exceeded before inc the gain. */
#define AM_AGC_RELEASE_RATE                     0x3703
// Larger values provide slower release, and smaller values provide faster release.
#define RELEASE_AGC_MSK 0x7f

/* Adjusts AM AGC for frontend (external) attenuator and LNA. */
#define AM_FRONTEND_AGC_CONTROL                 0x3705
// impacts sensitivity and U/D performance.
#define MIN_GAIN_INDEX_MSK      0xff00
#define MIN_GAIN_INDEX_SFT      8
#define MIN_GAIN_INDEX_SET(n)   ((n<<MIN_GAIN_INDEX_SFT)&MIN_GAIN_INDEX_MSK)
// insures the AGC gain indexes
#define ATTN_BACKUP_MSK         0x00ff
#define ATTN_BACKUP_SET(n)      (n&ATTN_BACKUP_MSK)

/* Sets the threshold for detecting impulses in dB above the noise floor. */
#define AM_NB_DETECT_THRESHOLD                  0x3900
// The default is 12 dB.

/* Interval in ms that original samples are replaced by interpolated clean sampl. */
#define AM_NB_INTERVAL                          0x3901
// The default is 55 μs.

/* Noise blanking rate in 100 Hz units. Default value is 64. */
#define AM_NB_RATE                              0x3902
// The default is 64 (6400 Hz).

/* Sets the bandwidth of the noise floor estimator. Default value is 300. */
#define AM_NB_IIR_FILTER                        0x3903
// The default is 300 (465 Hz).

/* Delay in us before applying impulse blanking to the original samples. */
#define AM_NB_DELAY                             0x3904
// The default is 172 μs.

/* Sets the output volume. */
#define RX_VOLUME                               0x4000
// Sets the output volume level, 63 max, 0 min. Default is 63.
#define VOL_MSK 0x3f

/* Mutes the L and R audio outputs. */
#define RX_HARD_MUTE                            0x4001
// Mutes L Audio Output.
#define LMUTE   0x2
// Mutes R Audio Output.
#define RMUTE   0x1

#endif
