#ifndef H_SI4730_REGS
#define H_SI4730_REGS
/*
 * Si4730 broadcast AM/FM receiver registers.
 * Commands and Properties for the FM/RDS Transmitter
 * (Si4710/11/12/13/20/21)
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*** FM/RDS Transmitter Command Summary ***/

/* Status Response */
// Clear to Send.
#define CTS     0x80
// Error.
#define ERR     0x40
// RDS Interrupt.
#define RDSINT  0x04
// Signal Quality Interrupt.
#define ASQINT  0x02
// Seek/Tune Complete Interrupt.
#define STCINT  0x01

/* Power up device and mode selection. */
#define POWER_UP                    0x01
// ARG1
// CTS Interrupt Enable.
#define CTSIEN_UP           0x80
// GPO2 Output Enable.
#define GPO2OEN_UP          0x40
// Patch Enable.
#define PATCH               0x20
// Crystal Oscillator Enable.
#define XOSCEN              0x10
// Function.
#define FUNC_TRANSMIT       0x02
#define FUNC_QUERY_LIB_ID   0x0f
// ARG2
// Application Setting
#define OPMODE_LIN_RIN      0x50
#define OPMODE_DIN_DFS_DCLK 0x0f

// Response (to FUNC = 2, TX)
// RESP1 Status Response

// Response (to FUNC = 15, Query Library ID)
// RESP1 PN[7:0]          Final 2 digits of part number.
// RESP2 FWMAJOR[7:0]     Firmware Major Revision.
// RESP3 FWMINOR[7:0]     Firmware Minor Revision.
// RESP4 RESERVED[7:0]    Reserved, various values.
// RESP5 RESERVED[7:0]    Reserved, various values.
// RESP6 CHIPREV[7:0]     Chip Revision.
// RESP7 LIBRARYID[7:0]   Library Revision.
#define POWER_UP_REQ_SIZE   3
#define POWER_UP_RESP1_SIZE 2
#define POWER_UP_RESP2_SIZE 8

/* Returns revision information on the device. */
#define GET_REV                     0x10
// Response
// RESP1 PN[7:0]          Final 2 digits of part number.
// RESP2 FWMAJOR[7:0]     Firmware Major Revision.
// RESP3 FWMINOR[7:0]     Firmware Minor Revision.
// RESP4 PATCHH[7:0]      Patch ID High Byte.
// RESP5 PATCHL[7:0]      Patch ID Low Byte.
// RESP6 CMPMAJOR[7:0]    Component Major Revision.
// RESP7 CMPMINOR[7:0]    Component Minor Revision.
// RESP8 CHIPREV[7:0]     Chip Revision.
#define GET_REV_REQ_SIZE  1
#define GET_REV_RESP_SIZE 9

/* Power down device. */
#define POWER_DOWN                  0x11
#define POWER_DOWN_REQ_SIZE  1
#define POWER_DOWN_RESP_SIZE 1

/* Sets the value of a property. */
#define SET_PROPERTY                0x12
// ARG1 Always write to 0.
// ARG2 Property High Byte. PROPH[7:0]
// ARG3 Property Low Byte. PROPL[7:0]
// ARG4 Property Value High Byte. PROPDH[7:0]
// ARG5 Property Value Low Byte. PROPDL[7:0]
#define SET_PROPERTY_REQ_SIZE  6
#define SET_PROPERTY_RESP_SIZE 1

/* Retrieves a property’s value. */
#define GET_PROPERTY                0x13
// ARG1 Always write to 0.
// ARG2 Property Get High Byte. PROPH[7:0]
// ARG3 Property Get Low Byte. PROPL[7:0]

// RESP1 Response     Reserved, various values.
// RESP2 PROPDH[7:0]  Property Value High Byte.
// RESP3 PROPDL[7:0]  Property Value High Byte.
#define GET_PROPERTY_REQ_SIZE  4
#define GET_PROPERTY_RESP_SIZE 4

/* Read interrupt status bits. */
#define GET_INT_STATUS              0x14
#define GET_INT_STATUS_REQ_SIZE  1
#define GET_INT_STATUS_RESP_SIZE 1

/* Reserved command used for patch file downloads. */
#define PATCH_ARGS                  0x15
#define PATCH_ARGS_REQ_SIZE  1
#define PATCH_ARGS_RESP_SIZE 1

/* Reserved command used for patch file downloads. */
#define PATCH_DATA                  0x16
#define PATCH_DATA_REQ_SIZE  1
#define PATCH_DATA_RESP_SIZE 1

/* Tunes to given transmit frequency. */
#define TX_TUNE_FREQ                0x30
// ARG1 Always write to 0.
// ARG2 Tune Frequency High Byte. FREQH[7:0]
// ARG3 Tune Frequency Low Byte. FREQL[7:0]
#define TX_TUNE_FREQ_REQ_SIZE  4
#define TX_TUNE_FREQ_RESP_SIZE 1

/* Sets the output power level and tunes the antenna cap. */
#define TX_TUNE_POWER               0x31
// ARG1 Always write to 0.
// ARG2 Always write to 0.
// ARG3 Tune Power Byte. RFdBμV[7:0]
// ARG4 Antenna Tuning Capacitor. ANTCAP[7:0]
#define TX_TUNE_POWER_REQ_SIZE  5
#define TX_TUNE_POWER_RESP_SIZE 1

/* Measure the received noise level at the specified frequency. */
#define TX_TUNE_MEASURE             0x32
// ARG1  Always write to 0.
// ARG2 Tune Frequency High Byte. FREQH[7:0]
// ARG3 Tune Frequency Low Byte. FREQL[7:0]
// ARG4 Antenna Tuning Capacitor. ANTCAP[7:0]
#define TX_TUNE_MEASURE_REQ_SIZE  5
#define TX_TUNE_MEASURE_RESP_SIZE 1

/* Queries the st of a prev sent TX Tune Freq, TX Tune Power, or TX Tune Meas cmd.*/
#define TX_TUNE_STATUS              0x33
// Seek/Tune Interrupt Clear.
#define INTACK  0x1

// Response
// RESP1 Reserved Returns various data.
// RESP2 READFREQH[7:0]   Read Frequency High Byte.
// RESP3 READFREQL[7:0]   Read Frequency Low Byte.
// RESP4 Reserved Returns various data.
// RESP5 READRFdBμV[7:0]  Read Power.
// RESP6 READANTCAP[7:0]  Read Antenna Tuning Capacitor.
// RESP7 RNL[7:0]         Read Received Noise Level (Si4712/13 Only).
#define TX_TUNE_STATUS_REQ_SIZE  2
#define TX_TUNE_STATUS_RESP_SIZE 8

/* Queries the TX status and input audio signal metrics. */
#define TX_ASQ_STATUS               0x34
// Interrupt Acknowledge.
#define INTACK  0x1

// Response
// RESP1
// Overmodulation Detection.
#define OVERMOD 0x4
// Input Audio Level Threshold Detect High.
#define IALH    0x2
// Input Audio Level Threshold Detect Low.
#define IALL    0x1
// RESP2 Reserved Returns various values.
// RESP3 Reserved Returns various values.
// RESP4 INLEVEL[7:0] Input Audio Level.
#define TX_ASQ_STATUS_REQ_SIZE  2
#define TX_ASQ_STATUS_RESP_SIZE 5

/* Queries the status of the RDS Group Buffer and loads new data into buffer. */
#define TX_RDS_BUFF                 0x35
// ARG1
// Operate on FIFO.
#define FIFO        0x80
// Load RDS Group Buffer.
#define LDBUFF      0x04
// Empty RDS Group Buffer.
#define MTBUFF      0x02
// Clear RDS Group buffer interrupt.
#define INTACK_CLR  0x01
// ARG2 RDS Block B High Byte. RDSBH[7:0]
// ARG3 RDS Block B Low Byte. RDSBL[7:0]
// ARG4 RDS Block C High Byte. RDSCH[7:0]
// ARG5 RDS Block C Low Byte. RDSCL[7:0]
// ARG6 RDS Block D High Byte. RDSDH[7:0]
// ARG7 RDS Block D Low Byte. RDSDL[7:0]

// Response
// RESP1
// Interrupt source: RDS PS Group has been transmitted.
#define RDSPSXMIT   0x10
// Interrupt source: RDS Group has been transmitted from the FIFO buffer.
#define CBUFXMIT    0x08
// Interrupt source: RDS Group has been transmitted from the circular buffer.
#define FIFOXMIT    0x04
// Interrupt source: RDS Group Circular Buffer has wrapped.
#define CBUFWRAP    0x02
// Interrupt source: RDS Group FIFO Buffer is empty.
#define FIFOMT      0x01
// RESP2 CBAVAIL[7:0]     Returns the number of available Circular Buffer blocks.
// RESP3 CBUSED[7:0]      Returns the number of used Circular Buffer blocks.
// RESP4 FIFOAVAIL[7:0]   Returns the number of available FIFO blocks.
// RESP5 FIFOUSED[7:0]    Returns the number of used FIFO blocks.
#define TX_RDS_BUFF_REQ_SIZE  8
#define TX_RDS_BUFF_RESP_SIZE 6

/* Set up default PS strings. */
#define TX_RDS_PS                   0x36
// ARG1
// Selects which PS data to load (0–23)
#define PSID_FIRST4CH_PS0   0x00
#define PSID_LAST4CH_PS0    0x01
#define PSID_FIRST4CH_PS1   0x02
#define PSID_LAST4CH_PS1    0x03
#define PSID_FIRST4CH_PS2   0x04
#define PSID_LAST4CH_PS2    0x05
#define PSID_FIRST4CH_PS3   0x06
#define PSID_LAST4CH_PS3    0x07
#define PSID_FIRST4CH_PS4   0x08
#define PSID_LAST4CH_PS4    0x09
#define PSID_FIRST4CH_PS5   0x0a
#define PSID_LAST4CH_PS5    0x0b
#define PSID_FIRST4CH_PS6   0x0c
#define PSID_LAST4CH_PS6    0x0d
#define PSID_FIRST4CH_PS7   0x0e
#define PSID_LAST4CH_PS7    0x0f
#define PSID_FIRST4CH_PS8   0x10
#define PSID_LAST4CH_PS8    0x11
#define PSID_FIRST4CH_PS9   0x12
#define PSID_LAST4CH_PS9    0x13
#define PSID_FIRST4CH_PS10  0x14
#define PSID_LAST4CH_PS10   0x15
#define PSID_FIRST4CH_PS11  0x16
#define PSID_LAST4CH_PS11   0x17
// ARG2 RDS PSID CHAR0. PSCHAR0[7:0]
// ARG3 RDS PSID CHAR1. PSCHAR1[7:0]
// ARG4 RDS PSID CHAR2. PSCHAR2[7:0]
// ARG5 RDS PSID CHAR3. PSCHAR3[7:0]
#define TX_RDS_PS_REQ_SIZE  6
#define TX_RDS_PS_RESP_SIZE 1

/* Configures GPO1, 2, and 3 as output or Hi-Z. */
#define GPIO_CTL                    0x80
// GPO3 Output Enable.
#define GPO3OEN 0x8
// GPO2 Output Enable.
#define GPO2OEN 0x4
// GPO1 Output Enable.
#define GPO1OEN 0x2
#define GPIO_CTL_REQ_SIZE  2
#define GPIO_CTL_RESP_SIZE 1

/* Sets GPO1, 2, and 3 output level (low or high). */
#define GPIO_SET                    0x81
// GPO3 Output Level.
#define GPO3LEVEL   0x8
// GPO2 Output Level.
#define GPO2LEVEL   0x4
// GPO1 Output Level.
#define GPO1LEVEL   0x2
#define GPIO_SET_REQ_SIZE  2
#define GPIO_SET_RESP_SIZE 1

/*** FM Transmitter Property Summary ***/

/* Enables interrupt sources. */
#define GPO_IEN                     0x0001
// RDS Interrupt Repeat. (Si4711/13/21 Only)
#define RDSREP  0x400
// ASQ Interrupt Repeat.
#define ASQREP  0x200
// STC Interrupt Repeat.
#define STCREP  0x100
// CTS Interrupt Enable.
#define CTSIEN  0x080
// ERR Interrupt Enable.
#define ERRIEN  0x040
// RDS Interrupt Enable (Si4711/13/21 Only).
#define RDSIEN  0x004
// Audio Signal Quality Interrupt Enable.
#define ASQIEN  0x002
// Seek/Tune Complete Interrupt Enable.
#define STCIEN  0x001

/* Configures the digital input format. */
#define DIGITAL_INPUT_FORMAT        0x0101
// DCLK Falling Edge.
#define IFALL                   0x80
// Digital Mode.
#define IMODE_DEFAULT           0x00
#define IMODE_I2S               0x08
#define IMODE_LEFT_JUSTIFIED    0x38
#define IMODE_MAB_AT1DCLK       0x68
#define IMODE_MAB_AT2DCLK       0x48
// Mono Audio Mode.
#define IMONO                   0x04
// Digital Audio Sample Precision.
#define ISIZE6BIT               0x00
#define ISIZE20BIT              0x01
#define ISIZE24BIT              0x02
#define ISIZE8BIT               0x03

/* Configures the digital input sample rate in 1 Hz steps. */
#define DIGITAL_INPUT_SAMPLE_RATE   0x0103
// Digital Input Sample Rate.

/* Sets frequency of the reference clock in Hz. The range is 31130 to 34406 Hz */
#define REFCLK_FREQ                 0x0201
// Frequency of Reference Clock in Hz.

/* Sets the prescaler value for the reference clock. */
#define REFCLK_PRESCALE             0x0202
// 0 = RCLK pin is clock source. 1 = DCLK pin is clock source.
#define RCLKSEL     0x1000
// Prescaler for Reference Clock.
#define REFCLKP_MSK 0x0fff

/* Enable transmit multiplex signal components. */
#define TX_COMPONENT_ENABLE         0x2100
// RDS Enable (Si4711/13/21 Only). 1 = Enables RDS to be transmitted.
#define RDS     0x4
// Left Minus Right. 1 = Enables Left minus Right (Stereo) to be transmitted.
#define LMR     0x2
// Pilot Tone. 1 = Enables the Pilot tone to be transmitted (default).
#define PILOT   0x1

/* Configures audio frequency deviation level. Units are in 10 Hz increments. */
#define TX_AUDIO_DEVIATION          0x2101
// Transmit Audio Frequency Deviation.

/* Configures pilot tone freq deviation level. Units are in 10 Hz increments. */
#define TX_PILOT_DEVIATION          0x2102
// Pilot tone frequency deviation is programmable from 0 Hz to 90 kHz in 10 Hz.

/* Configures the RDS/RBDS frequency deviation level. */
#define TX_RDS_DEVIATION            0x2103
// RDS frequency deviation is programmable from 0 Hz to 90 kHz in 10 Hz units.

/* max ana line in lvl to the LIN/RIN pins to reach the max dev lvl */
#define TX_LINE_INPUT_LEVEL         0x2104
// Line Attenuation.
#define LIATTEN190MV    0x0000
#define LIATTEN301MV    0x1000
#define LIATTEN416MV    0x2000
#define LIATTEN636MV    0x3000
// Maximum line amplitude level on the LIN/RIN pins in mVPK.
#define LILEVEL_MSK     0x03ff

/* Sets line input mute. L and R inputs may be independently muted. */
#define TX_LINE_INPUT_MUTE          0x2105
// Mutes L Line Input.
#define LIMUTE  0x2
// Mutes R Line Input.
#define RIMUTE  0x1

/* Configures pre-emphasis time constant. Default is 0 (75 μs). */
#define TX_PREEMPHASIS              0x2106
// FM Pre-Emphasis.
#define FMPE75US    0x0
#define FMPE50US    0x1

/* Configures the frequency of the stereo pilot. Default is 19000 Hz. */
#define TX_PILOT_FREQUENCY          0x2107
// Sets the frequency of the stereo pilot in 1 Hz steps.

/* Enables audio dynamic range control and limiter. */
#define TX_ACOMP_ENABLE             0x2200
// Audio Limiter.
#define LIMITEN 0x2
// Transmit Audio Dynamic Range Control Enable.
#define ACEN    0x1

/* Sets the threshold level for audio dynamic range control. */
#define TX_ACOMP_THRESHOLD          0x2201
// Transmit Audio Dynamic Range Control Threshold.

/* Sets the attack time for audio dynamic range control. */
#define TX_ACOMP_ATTACK_TIME        0x2202
// Transmit Audio Dynamic Range Control Attack Time.
#define ATTACK0P5MS 0x0
#define ATTACK1MS   0x1
#define ATTACK1P5MS 0x2
#define ATTACK2MS   0x3
#define ATTACK2P5MS 0x4
#define ATTACK3MS   0x5
#define ATTACK3P5MS 0x6
#define ATTACK4MS   0x7
#define ATTACK4P5MS 0x8
#define ATTACK5MS   0x9

/* Sets the release time for audio dynamic range control. */
#define TX_ACOMP_RELEASE_TIME       0x2203
// Transmit Audio Dynamic Range Control Release Time.
#define RELEASE100MS    0x0
#define RELEASE200MS    0x1
#define RELEASE350MS    0x2
#define RELEASE525MS    0x3
#define RELEASE1000MS   0x4

/* Sets the gain for audio dynamic range control. */
#define TX_ACOMP_GAIN               0x2204
// Range is from 0 to 20 dB in 1 dB steps.
#define GAIN_MSK    0x3f

/* Sets the limiter release time. Default is 102 (5.01 ms) */
#define TX_LIMITER_RELEASE_TIME     0x2205
// Sets the limiter release time.
#define LMITERTC102MS   5
#define LMITERTC85MS    6
#define LMITERTC73MS    7
#define LMITERTC63MS    8
#define LMITERTC51MS    10
#define LMITERTC39MS    13
#define LMITERTC30MS    17
#define LMITERTC20MS    25
#define LMITERTC10MS    51
#define LMITERTC8MS     57
#define LMITERTC7P9MS   64
#define LMITERTC7MS     73
#define LMITERTC6MS     85
#define LMITERTC5MS     102
#define LMITERTC4MS     127
#define LMITERTC3MS     170
#define LMITERTC2MS     255
#define LMITERTC1MS     510
#define LMITERTC0P5MS   1000
#define LMITERTC0P2MS   2000

/* Configures measurements related to signal quality metrics. Default is none */
#define TX_ASQ_INTERRUPT_SOURCE     0x2300
// Overmodulation Detection Enable.
#define OVERMODIEN  0x4
// Input Audio Level Detection High Threshold Enable.
#define IALHIEN     0x2
// Input Audio Level Detection Low Threshold Enable.
#define IALLIEN     0x1

/* Configures low audio input level detection threshold. */
#define TX_ASQ_LEVEL_LOW            0x2301
// Input Audio Level Low Threshold. Specified in units of dBFS in 1 dB steps (–70 0)
#define IALLTH_MSK  0xff

/* duration which the in audio lvl must be below the low threshold */
#define TX_ASQ_DURATION_LOW         0x2302
// Specified in 1mS increments (0–65535 ms). Default is 0.

/* Configures high audio input level detection threshold. */
#define TX_ASQ_LEVEL_HIGH           0x2303
// Specified in units of dBFS in 1 dB steps (–70 .. 0).
#define IALHTH_MSK  0xff

/* duration which the in audio lvl must be above the high threshold. */
#define TX_ASQ_DURATION_HIGH        0x2304
// Specified in 1 ms increments (0 – 65535 ms). Default is 0. IALHDUR[15:0]

/* Configure RDS interrupt sources. Default is none selected. */
#define TX_RDS_INTERRUPT_SOURCE     0x2c00
// 1 = Interrupt when a RDS PS Group has been transmitted.
#define RDSPSXMIT   0x10
// 1 = Interrupt when a RDS Group has been transmitted from the Circular Buffer.
#define RDSCBUFXMIT 0x08
// 1 = Interrupt when a RDS Group has been transmitted from the FIFO Buffer.
#define RDSFIFOXMIT 0x04
// 1 = Interrupt when the RDS Group Circular Buffer has wrapped.
#define RDSCBUFWRAP 0x02
// 1 = Interrupt when the RDS Group FIFO Buffer is empty.
#define RDSFIFOMT   0x01

/* Sets transmit RDS program identifier. */
#define TX_RDS_PI                   0x2c01
// RDS program identifier data.

/* Configures mix of RDS PS Group with RDS Group Buffer. */
#define TX_RDS_PS_MIX               0x2c02
// Transmit RDS Mix.
#define RDSPSMIX__GROUP_BUFFER_EMPTY    0x0
#define RDSPSMIX12P5PERCENT_TIME        0x1
#define RDSPSMIX25PERCENT_TIME          0x2
#define RDSPSMIX50PERCENT_TIME          0x3
#define RDSPSMIX75PERCENT_TIME          0x4
#define RDSPSMIX87P5PERCENT_TIME        0x5
#define RDSPSMIX100PERCENT_TIME         0x6

/* Miscellaneous bits to transmit along with RDS_PS Groups. */
#define TX_RDS_PS_MISC              0x2c03
// 1 = Indicates that the PTY code is dynamically switched.
#define RDSD3           0x8000
// Compressed code.
#define RDSD2           0x4000
// Artificial Head code.
#define RDSD1           0x2000
// Mono/Stereo code.
#define RDSD0           0x1000
// Use the PTY and TP set here in all block B data.
#define FORCEB          0x0800
// Traffic Program Code (default = 0).
#define RDSTP           0x0400
// Program Type Code (default = 0).
#define RDSPTY_MSK      0x03e0
#define RDSPTY_SFT      5
#define RDSPTY_SET(n)   ((n<<RDSPTY_SFT)&RDSPTY_MSK)
// Traffic Announcement Code (default = 0).
#define RDSTA           0x0010
// Music/Speech Switch Code.
#define RDSMS           0x0008

/* Number of times to repeat transmission of a PS message before next PS mssg. */
#define TX_RDS_PS_REPEAT_COUNT      0x2c04
// Transmit RDS PS Repeat Count.
#define RDSPSRC_MSK 0xff

/* Number of PS messages in use. */
#define TX_RDS_PS_MESSAGE_COUNT     0x2c05
// Number of PS messages to cycle through. Default is 1.
#define RDSPSMC_MSK 0xf

/* RDS Program Service Alternate Frequency. */
#define TX_RDS_PS_AF                0x2c06
// Transmit RDS Program Service Alternate Frequency.
#define

/* Number of blocks reserved for the FIFO. */
#define TX_RDS_FIFO_SIZE            0x2c07
// Transmit RDS FIFO Size.
#define RDSFIFOSZ_MSK   0xff

#endif
