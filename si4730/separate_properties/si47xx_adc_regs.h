#ifndef H_SI47XX_ADC_REGS
#define H_SI47XX_ADC_REGS
/*
 * Si4730 broadcast AM/FM receiver registers.
 * Commands and Properties for the Stereo Audio ADC Mode
 * (Si4704/05/30/31)
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/* Status Response */
// Clear to Send. 1 = Clear to send next command.
#define CTS     0x80
// Error.
#define ERR     0x40
// Audio Signal Quality Interrupt. 1 = Audio signal quality int has been triggered.
#define ASQINT  0x02

/*** Stereo Audio ADC Mode Commands ***/

/* Power-up device and mode selection. */
#define POWER_UP        0x01
// ARG1
// CTS Interrupt Enable.
#define CTSIEN_UP   0x80
// GPO2 Output Enable.
#define GPO2OEN_UP  0x40
// Patch Enable. 1 Cp NVM to RAM, but do not boot.
#define PATCH       0x20
// Crystal Oscillator Enable. 1 = Use crystal oscillator
#define XOSCEN      0x10
// Function.
#define FUNC_AUX_IN 0x04
// ARG2
// Application Setting.
#define OPMODE_DIGITAL_OUT          0x5b

// Response (FUNC = 15, Query Library ID)
// RESP1 PN[7:0]        Final 2 digits of part number (HEX).
// RESP2 FWMAJOR[7:0]   Firmware Major Revision (ASCII).
// RESP3 FWMINOR[7:0]   Firmware Minor Revision (ASCII).
// RESP4 RESERVED[7:0]  Reserved, various values.
// RESP5 RESERVED[7:0]  Reserved, various values.
// RESP6 CHIPREV[7:0]   Chip Revision (ASCII).
// RESP7 LIBRARYID[7:0] Library Revision (HEX).
#define POWER_UP_REQ_SIZE   3
#define POWER_UP_RESP1_SIZE 1
#define POWER_UP_RESP2_SIZE 8

/* Returns the revision information on the device. */
#define GET_REV         0x10
// RESP1 PN[7:0]        Final 2 digits of Part Number (HEX).
// RESP2 FWMAJOR[7:0]   Firmware Major Revision (ASCII).
// RESP3 FWMINOR[7:0]   Firmware Minor Revision (ASCII).
// RESP4 PATCHH[7:0]    Patch ID High Byte (HEX).
// RESP5 PATCHL[7:0]    Patch ID Low Byte (HEX).
// RESP6 CMPMAJOR[7:0]  Component Major Revision (ASCII).
// RESP7 CMPMINOR[7:0]  Component Minor Revision (ASCII).
// RESP8 CHIPREV[7:0]   Chip Revision (ASCII).
// RESP9-14 Reserved
// RESP15 CID[7:0]      CID (Si4705 only).
#define GET_REV_REQ_SIZE    1
#define GET_REV_RESP_SIZE   16

/* Power down device. */
#define POWER_DOWN      0x11
#define POWER_DOWN_REQ_SIZE    1
#define POWER_DOWN_RESP_SIZE   1

/* Sets the value of a property. */
#define SET_PROPERTY    0x12
// ARG1 Always write to 0.
// ARG2 Property High Byte. PROPH[7:0]
// ARG3 Property Low Byte. PROPL[7:0]
// ARG4 Property Value High Byte. PROPDH[7:0]
// ARG5 Property Value Low Byte. PROPDL[7:0]
#define SET_PROPERTY_REQ_SIZE    6
#define SET_PROPERTY_RESP_SIZE   1

/* Retrieves a property's value. */
#define GET_PROPERTY    0x13
// ARG1 Always write to 0.
// ARG2 Property High Byte. PROPH[7:0]
// ARG3 Property Low Byte. PROPL[7:0]

// RESP1 Reserved
// RESP2 PROPDH[7:0] Property Value High Byte.
// RESP3 PROPDL[7:0] Property Value Low Byte.
#define GET_PROPERTY_REQ_SIZE    1
#define GET_PROPERTY_RESP_SIZE   1

/* Read interrupt status bits. */
#define GET_INT_STATUS  0x14
#define GET_INT_STATUS_REQ_SIZE    1
#define GET_INT_STATUS_RESP_SIZE   1

/* Reserved command used for patch file down-loads. */
#define PATCH_ARGS      0x15

/* Reserved command used for patch file down-loads. */
#define PATCH_DATA      0x16

/* Starts sampling rate conversion. */
#define AUX_ASRC_START  0x61
#define AUX_ASRC_START_REQ_SIZE    1
#define AUX_ASRC_START_RESP_SIZE   1

/* Reports audio signal quality metrics. */
#define AUX_ASQ_STATUS  0x65
// Interrupt Acknowledge. 1 = Clears ASQINT
#define INTACK  0x1

// RESP1
// Audio Signal Overload Interrupt. 1 = Audio Input Signal overload has been detected.
#define OVERLOADINT 0x1
// RESP2
// Audio Signal Overload. 1 = Audio Input Signal overload is present.
#define OVERLOAD    0x1
// RESP3 LEVEL[7:0] Audio Input Signal Level.
#define AUX_ASQ_STATUS_REQ_SIZE    2
#define AUX_ASQ_STATUS_RESP_SIZE   4


/* Configures GPO1, 2, and 3 as output or Hi-Z */
#define GPIO_CNTL       0x80
// GPO3 Output Enable.
#define GPO3OEN 0x8
// GPO2 Output Enable.
#define GPO2OEN 0x4
// GPO1 Output Enable.
#define GPO1OEN 0x2
#define GPIO_CTL_REQ_SIZE   2
#define GPIO_CTL_RESP_SIZE  1

/* Sets GPO1, 2, and 3 output level (low or high). */
#define GPIO_SET        0x81
// GPO3 Output Level.
#define GPO3LEVEL 0x8
// GPO2 Output Level.
#define GPO2LEVEL 0x4
// GPO1 Output Level.
#define GPO1LEVEL 0x2
#define GPIO_SET_REQ_SIZE   2
#define GPIO_SET_RESP_SIZE  1

/*** Stereo Audio ADC Mode Properties ***/

/* Enables interrupt sources. */
#define GPO_IEN                     0x0001
// ASQ Interrupt Repeat. 1 = Interrupt generated even if ASQINT is already set.
#define ASQREP  0x200
// CTS Interrupt Enable. After PowerUp, bit refl the CTSIEN bit in ARG1 of PowerUp Cmd.
#define CTSIEN  0x080
// ERR Interrupt Enable. 1 = Interrupt generated when ERR is set.
#define ERRIEN  0x040
// ASQ Interrupt Enable. 1 = Interrupt generated when ASQINT is set.
#define ASQIEN  0x002

/* Configure digital audio outputs. */
#define DIGITAL_OUTPUT_FORMAT       0x0102
// Digital Output DCLK Edge. 1 = use DCLK falling edge
#define OFALL                       0x80
// Digital Output Mode.
#define OMODE_I2S                   0x00
#define OMODE_LEFT_JUSTIFIED        0x30
#define OMODE_MSB_AT_FIRST_DCLK     0x60
#define OMODE_MSB_AT_SECOND_DCLK    0x40
// Digital Output Mono Mode. 0 = Use mono/stereo blend (per blend thresholds)
#define OMONO                       0x04
// Digital Output Audio Sample Precision.
#define OSIZE16BITS                 0x00
#define OSIZE20BITS                 0x01
#define OSIZE24BITS                 0x02
#define OSIZE8BITS                  0x03

/* Configure digital audio output sample rate. */
#define DIGITAL_OUTPUT_SAMPLE_RATE  0x0104
// 32–48 ksps. 0 to disable digital audio output.

/* Sets the frequency of the reference clock in Hz. The from 31130 to 34406 Hz. */
#define REFCLK_FREQ                 0x0201
// The allowed REFCLK frequency range is between 31130 and 34406 Hz

/* Sets prescaler value for the reference clock. */
#define REFCLK_PRESCALE             0x0202
// 1 = DCLK pin is clock source.
#define RCLKSEL     0x1000
// Prescaler for Reference Clock. Int num used to divide clock freq down to REFCLK freq.
#define REFCLKP_MSK     0x0fff
#define REFCLKP_SET(n)  (n&REFCLKP_MSK)

/* Configure ASQ Interrupt source. */
#define AUX_ASQ_INTERRUPT_SOURCE    0x6600
// Interrupt Source Enable: Overload
#define OVERLOADINT 0x1

#endif
