#ifndef H_SI47XX_WB_REGS
#define H_SI47XX_WB_REGS
/*
 * Si4730 broadcast AM/FM receiver registers.
 * Commands and Properties for the WB Receiver (Si4707/36/37/38/39/42/43)
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*** WB Receiver Commands ***/

/* Selects the WB tuning frequency. */
#define WB_TUNE_FREQ    0x50
// ARG1 Always write to 0.
// ARG2 FREQH[7:0] Tune Frequency High Byte.
// ARG3 FREQL[7:0] Tune Frequency Low Byte.
#define WB_TUNE_FREQ_REQ_SIZE   4
#define WB_TUNE_FREQ_RESP_SIZE  1

/* Queries the status of previous WB_TUNE_FREQ or WB_SEEK_START command. */
#define WB_TUNE_STATUS  0x52
// Seek/Tune Interrupt Clear.
#define INTACK  0x1

// RESP1
// AFC Rail Indicator. This bit will be set if the AFC rails.
#define AFCRL   0x2
// Valid Channel. Confirms if the tuned channel is currently valid.
#define VALID   0x1
// RESP2 READFREQH[7:0] Read Frequency High Byte.
// RESP3 READFREQL[7:0] Read Frequency Low Byte.
// RESP4 RSSI[7:0]      Received Signal Strength Indicator.
// RESP5 SNR[7:0]       SNR.
#define WB_TUNE_STATUS_REQ_SIZE     2
#define WB_TUNE_STATUS_RESP_SIZE    6

/* Queries the status of the Received Signal Quality (RSQ) of the current channel */
#define WB_RSQ_STATUS   0x53
// Interrupt Acknowledge 0 = Interrupt status preserved.
#define INTACK  0x1

// RESP1
// SNR Detect High. 1 = Received SNR has exceeded above SNR high threshold.
#define SNRHINT     0x8
// SNR Detect Low. 1 = Received SNR has exceeded below SNR low threshold.
#define SNRLINT     0x4
// RSSI Detect High. 1 = RSSI has exceeded above RSSI high threshold.
#define RSSIHINT    0x2
// RSSI Detect Low. 1 = RSSI has exceeded below RSSI low threshold.
#define RSSILINT    0x1
// RESP2
// AFC Rail Indicator. This bit will be set if the AFC rails.
#define AFCRL_RSQ   0x2
// Valid Channel. Confirms if the channel is currently valid.
#define VALID_RSQ   0x1
// RESP3 Reserved
// RESP4 RSSI[7:0]      Received Signal Strength Indicator.
// RESP5 SNR[7:0]       SNR.
// RESP6 Reserved
// RESP7 FREQOFF[7:0]   Frequency Offset.
#define WB_RSQ_STATUS_REQ_SIZE     2
#define WB_RSQ_STATUS_RESP_SIZE    8

/* Retrieves Specific Area Message Encoding (SAME) */
#define WB_SAME_STATUS  0x54
// ARG1
// Clear Buffer 1 = Clears the contents of the SAME Message Buffer.
#define CLRBUF      0x2
// Interrupt Acknowledge 1 = Clears SAMEINT.
#define INTACK_SAME 0x1
// ARG2 Byte in the message buffer to start reading from. READADDR[7:0]

// RESP1
// End Of Message Detected 1 = End of message is detected.
#define EOMDET_SAME  0x8
// Start Of Message Detected 1 = start of message is detected.
#define SOMDET_SAME  0x4
// Preamble Detected 1 = Preamble is detected.
#define PREDET_SAME  0x2
// Header Buffer Ready 1 = Header buffer is ready.
#define HDRRDY_SAME  0x1
// RESP2 STATE[7:0]     State Machine Status
// RESP3 MSGLEN[7:0]    SAME Message Length
// RESP4
// Confidence Metric for DATAn represented as a number between 0 (low) and 3 (high).
#define CONF7_GET(reg)  ((reg>>6)&0x3)
#define CONF6_GET(reg)  ((reg>>4)&0x3)
#define CONF5_GET(reg)  ((reg>>2)&0x3)
#define CONF4_GET(reg)  (reg&0x3)
// RESP5
#define CONF3_GET(reg)  ((reg>>6)&0x3)
#define CONF2_GET(reg)  ((reg>>4)&0x3)
#define CONF1_GET(reg)  ((reg>>2)&0x3)
#define CONF0_GET(reg)  (reg&0x3)
// RESP6  DATA0[7:0]    Byte of message read at address, READADDR + 0
// RESP7  DATA1[7:0]    Byte of message read at address, READADDR + 1
// RESP8  DATA2[7:0]    Byte of message read at address, READADDR + 2
// RESP9  DATA3[7:0]    Byte of message read at address, READADDR + 3
// RESP10 DATA4[7:0]    Byte of message read at address, READADDR + 4
// RESP11 DATA5[7:0]    Byte of message read at address, READADDR + 5
// RESP12 DATA6[7:0]    Byte of message read at address, READADDR + 6
// RESP13 DATA7[7:0]    Byte of message read at address, READADDR + 7
#define WB_SAME_STATUS_REQ_SIZE     2
#define WB_SAME_STATUS_RESP_SIZE    14

/* Queries the status of the 1050 kHz alert tone in Weather Band. */
#define WB_ASQ_STATUS   0x55
// Interrupt Acknowledge
#define INTACK_ASQ  0x1

// RESP1
// 0 = 1050 Hz alert tone has not been detected to be absent
#define ALERTOFF_INT    0x2
// 0 = 1050 Hz alert tone has not been detected to be present
#define ALERTON_INT     0x1
// RESP2
// 1 = 1050 Hz alert tone is currently present.
#define ALERT  0x1
#define WB_ASQ_STATUS_REQ_SIZE  2
#define WB_ASQ_STATUS_RESP_SIZE 3

/* Queries the current AGC settings */
#define WB_AGC_STATUS   0x57
// RESP1
// This bit indicates whether the RF AGC is disabled or not 1 = RF AGC is disabled.
#define READ_RFAGCDIS   0x1
#define WB_AGC_STATUS_REQ_SIZE  2
#define WB_AGC_STATUS_RESP_SIZE 2

/* Override AGC setting by disabling and forcing it to a fixed value */
#define WB_AGC_OVERRIDE 0x58
// This bit selects whether the RF AGC is disabled or not 1 = RF AGC is disabled.
#define RFAGCDIS    0x1
#define WB_AGC_OVERRIDE_REQ_SIZE  2
#define WB_AGC_OVERRIDE_RESP_SIZE 1

/*** WB Receiver Properties ***/

/* Sets the maximum freq error allowed before setting the AFC_RAIL indicator. */
#define WB_MAX_TUNE_ERROR           0x5108
// Specified in units of kHz. Default is 10 kHz.

/* Configures interrupt related to Received Signal Quality metrics. */
#define WB_RSQ_INT_SOURCE           0x5200
// Interrupt Source Enable: Audio SNR High.
#define SNRHIEN     0x8
// Interrupt Source Enable: Audio SNR Low.
#define SNRLIEN     0x4
// Interrupt Source Enable: RSSI High.
#define RSSIHIEN    0x2
// Interrupt Source Enable: RSSI Low.
#define RSSILIEN    0x1

/* Sets high threshold for SNR interrupt. */
#define WB_RSQ_SNR_HI_THRESHOLD     0x5201
// Specified in units of dB in 1 dB steps (0...127). Default is 127dB.

/* Sets low threshold for SNR interrupt. */
#define WB_RSQ_SNR_LO_THRESHOLD     0x5202
// Specified in units of dB in 1 dB steps (0...127). Default is 0dB.

/* Sets high threshold for RSSI interrupt. */
#define WB_RSQ_RSSI_HI_THRESHOLD    0x5203
// Specified in units of dB in 1 dB steps (0...127). Default is 127dB.

/* Sets low threshold for RSSI interrupt. */
#define WB_RSQ_RSSI_LO_THRESHOLD    0x5204
// Specified in units of dB in 1 dB steps (0...127). Default is 0dB.

/* Sets SNR threshold to indicate a valid channel */
#define WB_VALID_SNR_THRESHOLD      0x5403
// Specified in units of dB in 1 dB steps (0...127). Default is 3 dB.

/* Sets RSSI threshold to indicate a valid channel */
#define WB_VALID_RSSI_THRESHOLD     0x5404
// Specified in units of dB in 1 dB steps (0...127). Default is 20 dB.

/* Configures SAME interrupt sources. */
#define WB_SAME_INTERRUPT_SOURCE    0x5500
// Enable EOMDET as the source of SAME Interrupt.
#define EOMDET  0x8
// Enable SOMDET as the source of SAME Interrupt.
#define SOMDET  0x4
// Enable PREDET as the source of SAME Interrupt.
#define PREDET  0x2
// Enable HDRRDY as the source of SAME Interrupt.
#define HDRRDY  0x1

/* Configures interrupt related to the 1050 kHz alert tone */
#define WB_ASQ_INT_SOURCE           0x5600
// Enable 1050kHz alert tone disappeared as the source of interrupt.
#define ALERTOFF_IEN    0x2
// Enable 1050kHz alert tone appeared as the source of interrupt.
#define ALERTON_IEN     0x1

#endif
