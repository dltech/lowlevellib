#ifndef H_BME280_REGS
#define H_BME280_REGS
/*
 * BME280 Combined humidity and pressure sensor. Register definitions.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BME280_ADDR 0xe6

#define ID          0xd0    /* chip identification number */
// the chip identification number chip_id[7:0]
#define BME280ID    0x60

#define RESET       0xe0    /* the soft reset word */
// If the val 0xB6 is written to the reg, the device is reset using the complete power-on-reset procedure.
#define RESET_VAL   0xb6

#define HUM_MSB     0xfd    /* raw temperature measurement output data */
#define HUM_LSB     0xfe    /* raw temperature measurement output data */

#define TEMP_MSB    0xfa    /* raw temperature measurement output data */
#define TEMP_LSB    0xfb    /* raw temperature measurement output data */
#define TEMP_XLSB   0xfc    /* raw temperature measurement output data */

#define PRESS_MSB   0xf7    /* raw pressure measurement output data */
#define PRESS_LSB   0xf8    /* raw pressure measurement output data */
#define PRESS_XLSB  0xf9    /* raw pressure measurement output data */
// Contains the XLSB part
#define XLSB_MSK    0xf0
#define XLSB_SFT    4
#define XLSB_GET(n) ((n>>XLSB_SFT)&0xf)

#define CONFIG      0xf5    /* sets the rate, filter and interface options of the device. */
// Controls inactive duration tstandby in normal mode.
#define T_SB_0P5MS  0x00
#define T_SB_62P5MS 0x20
#define T_SB_125MS  0x40
#define T_SB_250MS  0x60
#define T_SB_500MS  0x80
#define T_SB_1000MS 0xa0
#define T_SB_10MS   0xc0
#define T_SB_20MS   0xe0
// Controls the time constant of the IIR filter.
#define FILTER_OFF  0x00
#define FILTER2     0x02
#define FILTER4     0x04
#define FILTER8     0x06
#define FILTER16    0x08
// Enables 3-wire SPI interface when set to ‘1’.
#define SPI3W_EN    0x01

#define CTRL_MEAS   0xf4    /* pressure and temperature data acquisition options of the device */
// Controls oversampling of temperature data.
#define OSRS_T_SKIP 0x00
#define OSRS_T_X1   0x20
#define OSRS_T_X2   0x40
#define OSRS_T_X4   0x60
#define OSRS_T_X8   0x80
#define OSRS_T_X16  0xa0
// Controls oversampling of pressure data.
#define OSRS_P_SKIP 0x00
#define OSRS_P_X1   0x04
#define OSRS_P_X2   0x08
#define OSRS_P_X4   0x0c
#define OSRS_P_X8   0x80
#define OSRS_P_X16  0x84
// Controls the sensor mode of the device.
#define MODE_SLEEP  0x00
#define MODE_FORCED 0x01
#define MODE_NORMAL 0x03

#define STATUS      0xf3    /* two bits which indicate the status of the device. */
// Auto set to ‘1’ whenever a conversion is run and back to ‘0’ when the results have been transferred to regs.
#define MEASURING   0x4
// Auto set to ‘1’ when the NVM data are being copied to image regs and back to ‘0’ when the copy is done.
#define IM_UPDATE   0x1

#define CTRL_HUM    0xf2    /* humidity data acquisition options of the device */
// Controls oversampling of humidity data.
#define OSRS_H_SKIP 0x0
#define OSRS_H_X1   0x1
#define OSRS_H_X2   0x2
#define OSRS_H_X4   0x3
#define OSRS_H_X8   0x4
#define OSRS_H_X16  0x8

#define CALIB00     0x88    /* calibration data */
#define CALIB01     0x89    /* calibration data */
#define CALIB02     0x8a    /* calibration data */
#define CALIB03     0x8b    /* calibration data */
#define CALIB04     0x8c    /* calibration data */
#define CALIB05     0x8d    /* calibration data */
#define CALIB06     0x8e    /* calibration data */
#define CALIB07     0x8f    /* calibration data */
#define CALIB08     0x90    /* calibration data */
#define CALIB09     0x91    /* calibration data */
#define CALIB10     0x92    /* calibration data */
#define CALIB11     0x93    /* calibration data */
#define CALIB12     0x94    /* calibration data */
#define CALIB13     0x95    /* calibration data */
#define CALIB14     0x96    /* calibration data */
#define CALIB15     0x97    /* calibration data */
#define CALIB16     0x98    /* calibration data */
#define CALIB17     0x99    /* calibration data */
#define CALIB18     0x9a    /* calibration data */
#define CALIB19     0x9b    /* calibration data */
#define CALIB20     0x9c    /* calibration data */
#define CALIB21     0x9d    /* calibration data */
#define CALIB22     0x9e    /* calibration data */
#define CALIB23     0x9f    /* calibration data */
#define CALIB24     0xa0    /* calibration data */
#define CALIB25     0xa1    /* calibration data */
#define CALIB26     0xe1    /* calibration data */
#define CALIB27     0xe2    /* calibration data */
#define CALIB28     0xe3    /* calibration data */
#define CALIB29     0xe4    /* calibration data */
#define CALIB30     0xe5    /* calibration data */
#define CALIB31     0xe6    /* calibration data */
#define CALIB32     0xe7    /* calibration data */
#define CALIB33     0xe8    /* calibration data */
#define CALIB34     0xe9    /* calibration data */
#define CALIB35     0xea    /* calibration data */
#define CALIB36     0xeb    /* calibration data */
#define CALIB37     0xec    /* calibration data */
#define CALIB38     0xed    /* calibration data */
#define CALIB39     0xee    /* calibration data */
#define CALIB40     0xef    /* calibration data */
#define CALIB41     0xf0    /* calibration data */

#endif
