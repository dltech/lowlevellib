#ifndef H_HT1621_REGS
#define H_HT1621_REGS
/*
 * RAM Mapping 324 LCD Controller for I/O MCU. Register definitions.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* ID */'
// Read data from the RAM
#define READ    0x6
// Write data to the RAM
#define WRITE   0x5
// command
#define CDM     0x4

/* commands */
// Turn off both system oscillator and LCD bias generator
#define SYS_DIS     0x00
// Turn on system oscillator
#define SYS_EN      0x01
// Turn off LCD bias generator
#define LCD_OFF     0x02
// Turn on LCD bias generator
#define LCD_ON      0x03
// Disable time base output
#define TIMER_DIS   0x04
// Disable WDT time-out flag output
#define WDT_DIS     0x05
// Enable time base output
#define TIMER_EN    0x06
// Enable WDT time-out flag output
#define WDT_EN      0x07
// Turn off tone outputs
#define TONE_OFF    0x08
// Turn on tone outputs
#define TONE_ON     0x09
// Clear the contents of time base generator
#define CLR_TIMER   0x0c
// Clear the contents of WDT stage
#define CLR_WDT     0x0e
// System clock source, crystal oscillator, on-chip RC oscillator, external
#define XTAL_32K    0x14
#define RC_256K     0x18
#define EXT_256K    0x1c
// LCD 1/2 bias option
#define BIAS12_2C   0x20
#define BIAS12_3C   0x24
#define BIAS12_4C   0x28
// LCD 1/3 bias option
#define BIAS13_2C   0x21
#define BIAS13_3C   0x25
#define BIAS13_4C   0x29
// Tone frequency, 4kHz, 2kHz
#define TONE_4K     0x40
#define TONE_2K     0x60
// Disable IRQ output
#define IRQ_DIS     0x80
// Enable IRQ output
#define IRQ_EN      0x88
// Time base/WDT clock output:
#define F1          0xa0
#define F2          0xa1
#define F4          0xa2
#define F8          0xa3
#define F16         0xa4
#define F32         0xa5
#define F64         0xa6
#define F128        0xa7
// Test mode, user don′t use.
#define TEST        0xe0
// Normal mode
#define NORMAL      0xe3

#endif
