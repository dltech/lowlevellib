#ifndef H_TDA8546_REGS
#define H_TDA8546_REGS
/*
 * MAX2769 Universal GPS Receiver. Register definitions.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define CONF1   0x0 /* Configures RX and IF sections, bias settings for individual blocks. */
// Chip enable. Set 1 to enable the device and 0 to disable the entire device except the serial bus.
#define CHIPEN          0x8000000
// Idle enable. Set 1 to put the chip in the idle mode and 0 for operating mode.
#define IDLE            0x4000000
// LNA1 current programming.
#define ILNA1_MSK       0x3c00000
#define ILNA1_SFT       22
#define ILNA1_SET(n)    ((n<<ILNA1_SFT)&ILNA1_MSK)
// LNA2 current programming.
#define ILNA2_MSK       0x0300000
#define ILNA2_SFT       20
#define ILNA2_SET(n)    ((n<<ILNA2_SFT)&ILNA2_MSK)
// LO buffer current programming.
#define ILO_MSK         0x00c0000
#define ILO_SFT         18
#define ILO_SET(n)      ((n<<ILO_SFT)&ILO_MSK)
// Mixer current programming.
#define IMIX_MSK        0x0030000
#define IMIX_SFT        16
#define IMIX_SET(n)     ((n<<IMIX_SFT)&IMIX_MSK)
// Mixer pole selection. Set 1 to program the passive filter pole at mixer output at 36MHz, 0 13MHz.
#define MIXPOLE         0x0008000
// LNA mode selection
#define LNAMODE_BIAS    0x0000000
#define LNAMODE_LNA2    0x0002000
#define LNAMODE_LNA1    0x0004000
#define LNAMODE_OFF     0x0006000
// Mixer enable. Set 1 to enable the mixer and 0 to shut down the mixer.
#define MIXEN           0x0001000
// Antenna bias enable. Set 1 to enable the antenna bias and 0 to shut down the antenna bias.
#define ANTEN           0x0000800
// IF center frequency programming. Default for fCENTER = 4MHz, BW = 2.5MHz.
#define FCEN_MSK        0x00007e0
#define FCEN_SFT        5
#define FCEN_SET(n)     ((n<<FCEN_SFT)&FCEN_MSK)
// IF filter center bandwidth selection.
#define FBW2P5MHZ       0x0000000
#define FBW4P2MHZ       0x0000008
#define FBW8MHZ         0x0000010
#define FBW18MHZ        0x0000018
// Filter order selection. Set 0 to select the 5th-order Butterworth filter. 1 3rd
#define F3OR5           0x0000004
// Polyphase filter selection. Set 1 to select complex bandpass filter mode.
#define FCENX           0x0000002
// IF filter gain setting. Set 0 to reduce the filter gain by 6dB.
#define FGAIN           0x0000001

#define CONF2   0x1 /* Configures AGC and output sections. */
// I and Q channels enable. Set 1 to enable both I and Q channels and 0 to enable I channel only.
#define IQEN                    0x8000000
// AGC gain reference value expressed by the number of MSB counts (magnitude bit density).
#define GAINREF_MSK             0x7ff0000
#define GAINREF_SFT             15
#define GAINREF_SET(n)          ((n<<GAINREF_SFT)&GAINREF_MSK)
// AGC mode control.
#define AGCMODE_INDEPENDENT_IQ  0x0000000
#define AGCMODE_LOCKED_IQ       0x0000800
#define AGCMODE_BY_GAINN        0x0001000
#define AGCMODE_DISALLOW        0x0001800
// Output data format.
#define FORMAT_UINT             0x0000000
#define FORMAT_SIGN_MAGN        0x0000200
#define FORMAT_2S_COMPL         0x0000400
// Number of bits in the ADC.
#define BITS1BIT                0x0000000
#define BITS1P5BIT              0x0000040
#define BITS2BIT                0x0000080
#define BITS2P5BIT              0x00000c0
#define BITS3BIT                0x0000100
// Output driver configuration.
#define DRVCFG_CMOS_LOGIC       0x0000000
#define DRVCFG_LIM_DIFF_LOGIC   0x0000010
#define DRVCFG_ANALOG_OUT       0x0000020
// LO buffer enable. Set 1 to enable LO buffer or 0 to disable the buffer.
#define LOEN                    0x0000004
// Identifies a version of the IC.
#define DIEID_MSK               0x0000003
#define DIEID_GET(n)            (n&DIEID_MSK)

#define CONF3   0x2 /* Configures support and test functions for IF filter and AGC. */
// PGA gain value programming from the serial interface in steps of dB per LSB.
#define GAININ_MSK                          0xfc00000
#define GAININ_SFT                          22
#define GAININ_SET(n)    ((n<<GAININ_SFT)&GAININ_MSK)
// Low value of the ADC full-scale enable. Set 1 to enable or 0 to disable.
#define FSLOWEN                             0x0200000
// Set 1 to enable the output driver to drive high loads.
#define HILOADEN                            0x0100000
// ADC enable. Set 1 to enable ADC or 0 to disable.
#define ADCEN                               0x0080000
// Output driver enable. Set 1 to enable the driver or 0 to disable.
#define DRVEN                               0x0040000
// Filter DC offset cancellation circuitry enable. Set 1 to enable the circuitry or 0 to
#define FOFSTEN                             0x0020000
// IF filter enable. Set 1 to enable the filter or 0 to disable.
#define FILTEN                              0x0010000
// Highpass coupling enable. Set 1 to enable the highpass coupling between the filter and PGA
#define FHIPEN                              0x0008000
// I-channel PGA enable. Set 1 to enable PGA in the I channel or 0 to disable.
#define PGAIEN                              0x0002000
// Q-channel PGA enable. Set 1 to enable PGA in the Q channel or 0 to disable.
#define PGAQEN                              0x0001000
// DSP interface for serial streaming of data enable.
#define STRMEN                              0x0000800
// The positive edge of this command enables data streaming to the output.
#define STRMSTART                           0x0000400
// The positive edge of this command disables data streaming to the output.
#define STRMSTOP                            0x0000200
// Sets the length of the data counter from 128 (000) to 16,394 (111) bits per frame.
#define STRMCOUNT_MSK                       0x00001c0
#define STRMCOUNT_SFT                       6
#define STRMCOUNT_SET(n)                    ((n<<STRMCOUNT_SFT)&STRMCOUNT_MSK)
// Number of bits streamed.
#define STRMBITS_I_MSB                      0x0000000
#define STRMBITS_I_MSG_I_LSB                0x0000010
#define STRMBITS_I_MSB_Q_MSB                0x0000020
#define STRMBITS_I_MSB_I_LSB_Q_MSB_Q_LSB    0x0000030
// The signal enables the insertion of the frame number at the beginning of each frame.
#define STAMPEN                             0x0000008
// output of the time sync pulses at all times when streaming is enabled by the STRMEN command.
#define TIMESYNCEN                          0x0000004
// This control signal enables the sync pulses at the DATASYNC output.
#define DATSYNCEN                           0x0000002
// This command resets all the counters irrespective of the timing within the stream cycle.
#define STRMRST                             0x0000001

#define PLLCONF 0x3 /* PLL, VCO, and CLK settings. */
// VCO enable. Set 1 to enable the VCO or 0 to disable VCO.
#define VCOEN                       0x8000000
// VCO current-mode selection. Set 1 to program the VCO in the low-current mode
#define IVCO                        0x4000000
// Clock buffer enable. Set 1 to enable the clock buffer or 0 to disable the clock buffer.
#define REFOUTEN                    0x1000000
// Clock output divider ratio.
#define REFDIV_X2                   0x0000000
#define REFDIV_DIV4                 0x0200000
#define REFDIV_DIV2                 0x0400000
#define REFDIV1                     0x0600000
// Current programming for XTAL oscillator/buffer.
#define IXTAL_OSC_NORMAL_CURR       0x0000000
#define IXTAL_BUFFER_NORMAL_CURR    0x0080000
#define IXTAL_OSC_MEDIUM_CURR       0x0100000
#define IXTAL_OSC_HIGH_CURR         0x0180000
// Digital XTAL load cap programming.
#define XTALCAP_MSK                 0x007c000
#define XTALCAP_SFT                 14
#define XTALCAP_SET(n)              ((n<<XTALCAP_SFT)&XTALCAP_MSK)
// LD pin output selection. Set D13:D10 = 0000: PLL lock-detect signal.
#define LDMUX_MSK                   0x0003c00
#define LDMUX_SFT                   10
#define LDMUX_SET(n)                ((n<<LDMUX_SFT)&LDMUX_MSK)
// Charge-pump current selection. Set 1 for 1mA and 0 for 0.5mA.
#define ICP                         0x0000200
// Set 0 for normal operation or 1 to disable the PLL phase frequency detector.
#define PFDEN                       0x0000100
// Charge-pump test.
#define CPTEST_NORMAL               0x0000000
#define CPTEST_PUMP_UP              0x0000020
#define CPTEST_PUMP_DOWN            0x0000010
#define CPTEST_HIGH_IMPEDANCE       0x0000040
#define CPTEST_UP_DOWN              0x0000070
// PLL mode control. Set 1 to enable the integer-N PLL
#define INT_PLL                     0x0000008
// PLL power-save mode. Set 1 to enable the power-save mode or 0 to disable.
#define PWRSAV                      0x0000004

#define DIV     0x4 /* PLL main and reference division ratios, other controls. */
// PLL integer division ratio.
#define NDIV_MSK    0xfffe000
#define NDIV_SFT    13
#define NDIV_SET(n) ((n<<NDIV_SFT)&NDIV_MSK)
// PLL reference division ratio.
#define RDIV_MSK    0x0001ff8
#define RDIV_SFT    3
#define RDIV_SET(n) ((n<<RDIV_SFT)&RDIV_MSK)

#define FDIV    0x5 /* PLL fractional division ratio, other controls. */
// PLL fractional divider ratio.
#define FDIV_MSK    0xffffff0
#define FDIV_SFT    8
#define FDIV_SET(n) ((n<<FDIV_SFT)&FDIV_MSK)

#define STRM    0x6 /* DSP interface number of frames to stream. */
// This word defines the frame number at which to start streaming.
#define FRAMECOUNT_MSK  0xfffffff

#define CLK     0x7 /* Fractional clock-divider values. */
// Sets the value for the L counter.
#define L_CNT_MSK       0xfff0000
#define L_CNT_SFT       16
#define L_CNT_SET(n)    ((n<<L_CNT_SFT)&L_CNT_MSK)
// Sets the value for the M counter.
#define M_CNT_MSK       0x000fff0
#define M_CNT_SFT       4
#define M_CNT_SET(n)    ((n<<M_CNT_SFT)&M_CNT_MSK)
// Fractional clock divider. Set 1 to select the ADC clock to come from the fractional clk div
#define FCLKIN          0x0000008
// ADC clock selection. Set 0 to select the ADC and fractional divider clocks.
#define ADCCLK          0x0000004
// Serializer clock selection. Set 0 to select the serializer clock output.
#define SERCLK          0x0000002
// DSP interface mode selection.
#define MODE            0x0000001

#define TEST1   0x8 /* Reserved for test mode. */
#define TEST1_DEF   0x1E0F401

#define TEST2   0x9 /* Reserved for test mode. */
#define TEST2_DEF   0x14C0402

#endif
