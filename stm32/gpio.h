#ifndef H_GPIO
#define H_GPIO
/*
 * Part of lowlevellib - a heap of microcontroller periferial code.
 * Here is STM32 GPIO API functions.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "regs/f1/gpio_regs.h"
#include <inttypes.h>

// port configuration
void gpioToDefault(uint32_t port, uint32_t pin);
// types of port
void gpioSetAnalogue(uint32_t port, uint32_t pin);
void gpioSetInput(uint32_t port, uint32_t pin);
void gpioSetPushPull(uint32_t port, uint32_t pin);
void gpioSetOpenDrain(uint32_t port, uint32_t pin);
// peripherial or manual (NOTE: choose the type of port at first)
void gpioSetAlternativeF(uint32_t port, uint32_t pin);
// 20k resistors
void gpioSetPullUp(uint32_t port, uint32_t pin);
void gpioSetPullDown(uint32_t port, uint32_t pin);
// port clocking speed in MHz
void gpioSetOutput2M(uint32_t port, uint32_t pin);
void gpioSetOutput10M(uint32_t port, uint32_t pin);
void gpioSetOutput50M(uint32_t port, uint32_t pin);

// port usage
void gpioSet(uint32_t port, uint32_t pin);
void gpioReset(uint32_t port, uint32_t pin);
int gpioIsActive(uint32_t port, uint32_t pin);
uint32_t getPort(uint32_t port);

// boosted version
// port configuration
void gpionToDefault(uint32_t port, int pin);
// types of port
void gpionSetAnalogue(uint32_t port, int pin);
void gpionSetInput(uint32_t port, int pin);
void gpionSetPushPull(uint32_t port, int pin);
void gpionSetOpenDrain(uint32_t port, int pin);
// peripherial or manual (NOTE: choose the type of port at first)
void gpionSetAlternativeF(uint32_t port, int pin);
// 20k resistors
void gpionSetPullUp(uint32_t port, int pin);
void gpionSetPullDown(uint32_t port, int pin);
// port clocking speed in MHz
void gpionSetOutput2M(uint32_t port, int pin);
void gpionSetOutput10M(uint32_t port, int pin);
void gpionSetOutput50M(uint32_t port, int pin);
// port usage
void gpionSet(uint32_t port, int pin);
void gpionReset(uint32_t port, int pin);
int gpionIsActive(uint32_t port, int pin);

#endif
