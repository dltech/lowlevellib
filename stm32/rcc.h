#ifndef H_RCC
#define H_RCC
/*
 * Part of Belkin STM32 HAL, clocking enable standard functions.
 *
 * Copyright 2024 Mikhail Belkin <dltech174DEFINES@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "regs/type_detect.h"
#ifdef XXX_DENSITY
#include "regs/f1/rcc_regs.h"
#endif
#ifdef CONNECTIVITY
#include "regs/f1/rcc_regs_con.h"
#endif
#include "inttypes.h"

// maximal frequency system clock
void sysClk(void);
// slowest system clock
void suspSysClk(void);
// simplified clock enable interface
void enablePeriphClock(uint16_t periph);
// simplified clock disable interface
void resetPeriphClock(uint16_t periph);

#ifdef XXX_DENSITY
enum{
    SDIO,
    FSMC,
    CRC,
    FLITF,
    SRAM,
    DMA2,
    DMA1,
    TIM14,
    TIM13,
    TIM12,
    TIM11,
    TIM10,
    TIM9,
    TIM8,
    TIM7,
    TIM6,
    TIM5,
    TIM4,
    TIM3,
    TIM2,
    TIM1,
    ADC3,
    ADC2,
    ADC1,
    UART5,
    UART4,
    USART3,
    USART2,
    USART1,
    SPI3,
    SPI2,
    SPI1,
    IOPG,
    IOPF,
    IOPE,
    IOPD,
    IOPC,
    IOPB,
    IOPA,
    AFIO,
    DAC,
    PWR,
    BKP,
    CAN,
    USB,
    I2C2,
    I2C1,
    WWDG
};
#endif

#ifdef CONNECTIVITY
#pragma once
enum{
    ETHMACRX,
    ETHMACTX,
    ETHMAC,
    OTGFS,
    CRC,
    FLITF,
    SRAM,
    DMA2,
    DMA1,
    UART5,
    UART4,
    USART3,
    USART2,
    USART1,
    SPI3,
    SPI2,
    SPI1,
    TIM7,
    TIM6,
    TIM5,
    TIM4,
    TIM3,
    TIM2,
    TIM1,
    ADC2,
    ADC1,
    IOPE,
    IOPD,
    IOPC,
    IOPB,
    IOPA,
    AFIO,
    DAC,
    PWR,
    BKP,
    CAN2,
    CAN1,
    I2C2,
    I2C1,
    WWDG
};
#endif

#endif
