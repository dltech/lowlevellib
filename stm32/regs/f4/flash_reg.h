#ifndef H_FLASH_REG
#define H_FLASH_REG
/*
 * Part of Belkin STM32 HAL, FLASH register definitions of STM32F4xx MCUs.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Flash access control register */
#define FLASH_ACR       MMIO32(FLASH_BASE + 0x00)
// Data cache reset
#define DCRST           0x1000
// Instruction cache reset
#define ICRST           0x0800
// Data cache enable
#define DCEN            0x0400
// Instruction cache enable
#define ICEN            0x0200
// Prefetch enable
#define PRFTEN          0x0100
// Latency
#define LATENCY_ZERO    0x0000
#define LATENCY_ONE     0x0001
#define LATENCY_TWO     0x0002
#define LATENCY_THREE   0x0003
#define LATENCY_FOUR    0x0004
#define LATENCY_FIVE    0x0005
#define LATENCY_SIX     0x0006
#define LATENCY_SEVEN   0x0007

/* Flash key register */
#define FLASH_KEYR      MMIO32(FLASH_BASE + 0x04)
// FPEC key
#define KEY1    0x45670123
#define KEY2    0xcdef89ab

/* Flash option key register */
#define FLASH_OPT_KEYR  MMIO32(FLASH_BASE + 0x08)
// Option byte key
#define OPTKEY1 0x08192a3b
#define OPTKEY2 0x4c5d6e7f

/* Flash status register */
#define FLASH_SR        MMIO32(FLASH_BASE + 0x0c)
// Busy
#define BSY     0x10000
// Programming sequence error
#define PGSERR  0x00080
// Programming parallelism error
#define PGPERR  0x00040
// Programming alignment error
#define PGAERR  0x00020
// Write protection error
#define WRPERR  0x00010
// Operation error
#define OPERR   0x00002
// End of operation
#define EOP     0x00001

/* Flash control register */
#define FLASH_CR        MMIO32(FLASH_BASE + 0x10)
// Lock
#define LOCK        0x80000000
// Error interrupt enable
#define ERRIE       0x02000000
// End of operation interrupt enable
#define EOPIE       0x01000000
// Start
#define STRT        0x00010000
// Program size
#define PSIZE_X8    0x00000000
#define PSIZE_X16   0x00000100
#define PSIZE_X32   0x00000200
#define PSIZE_X64   0x00000300
// Sector number
#define SNB0        0x00000000
#define SNB1        0x00000008
#define SNB2        0x00000010
#define SNB3        0x00000018
#define SNB4        0x00000020
#define SNB5        0x00000028
#define SNB6        0x00000030
#define SNB7        0x00000038
#define SNB8        0x00000040
#define SNB9        0x00000048
#define SNB10       0x00000050
#define SNB11       0x00000058
// Mass Erase
#define MER         0x00000004
// Sector Erase
#define SER         0x00000002
// Programming
#define PG          0x00000001

/* Flash option control register */
#define FLASH_OPTCR     MMIO32(FLASH_BASE + 0x14)
// Not write protect
#define NWRP0       0x0010000
#define NWRP1       0x0020000
#define NWRP2       0x0040000
#define NWRP3       0x0080000
#define NWRP4       0x0100000
#define NWRP5       0x0200000
#define NWRP6       0x0400000
#define NWRP7       0x0800000
#define NWRP8       0x1000000
#define NWRP9       0x2000000
#define NWRP10      0x4000000
#define NWRP11      0x8000000
// Read protect
#define RDP_LEVEL0  0x000aa00
#define RDP_LEVEL2  0x000cc00
#define RDP_MSK     0x000ff00
#define RDP_SFT     8
// User option bytes
#define NRST_STDBY  0x0000080
#define NRST_STOP   0x0000040
#define WDG_SW      0x0000020
// BOR reset Level
#define BOR_LEV3    0x0000000
#define BOR_LEV2    0x0000004
#define BOR_LEV1    0x0000008
#define BOR_OFF     0x000000c
// Option start
#define OPTSTRT     0x0000002
// Option lock
#define OPTLOCK     0x0000001

#endif
