#ifndef H_PWR_REG
#define H_PWR_REG
/*
 * Part of Belkin STM32 HAL, Power control registers of STMF4xx MCU.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Power control register */
#define PWR_CR  MMIO32(PWR_BASE + 0x0)
// Regulator voltage scaling output selection
#define VOS     0x4000
// Flash power-down in Stop mode
#define FPDS    0x0200
// Disable backup domain write protection.
#define DBP     0x100
// PVD level selection.
#define PLS2V0  0x000
#define PLS2V1  0x020
#define PLS2V3  0x040
#define PLS2V5  0x060
#define PLS2V6  0x080
#define PLS2V7  0x0a0
#define PLS2V8  0x0c0
#define PLS2V9  0x0e0
// Power voltage detector enable.
#define PVDE    0x010
// Clear standby flag.
#define CSBF    0x008
// Clear wakeup flag.
#define CWUF    0x004
// Power down deepsleep.
#define PDDS    0x002
// Low-power deepsleep.
#define LPDS    0x001

/* Power control/status register */
#define PWR_CSR MMIO32(PWR_BASE + 0x4)
// Regulator voltage scaling output selection ready bit
#define VOSRDY          0x04000
// Backup regulator enable
#define BRE             0x00200
// Enable WKUP pin 1: WKUP pin is used for wakeup from Standby mode
#define EWUP            0x00100
// Backup regulator ready
#define BRR             0x00008
// PVD output 1: VDD/VDDA is lower than the PVD threshold
#define PVDO            0x00004
// Standby flag 1: Device has been in Standby mode
#define SBF             0x00002
// Wakeup flag 1: A wakeup event was received from the WKUP
#define WUF             0x00001

#endif
