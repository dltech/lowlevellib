#ifndef H_GPIO_REG
#define H_GPIO_REG
/*
 * Part of Belkin STM32 HAL, gpio registers of STMF4xx MCU.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* GPIO port mode register */
#define GPIOA_MODER         MMIO32(GPIOA_BASE + 0x00)
#define GPIOB_MODER         MMIO32(GPIOB_BASE + 0x00)
#define GPIOC_MODER         MMIO32(GPIOC_BASE + 0x00)
#define GPIOD_MODER         MMIO32(GPIOD_BASE + 0x00)
#define GPIOE_MODER         MMIO32(GPIOE_BASE + 0x00)
#define GPIOF_MODER         MMIO32(GPIOF_BASE + 0x00)
#define GPIOG_MODER         MMIO32(GPIOG_BASE + 0x00)
#define GPIOH_MODER         MMIO32(GPIOH_BASE + 0x00)
#define GPIOI_MODER         MMIO32(GPIOI_BASE + 0x00)
#define GPIOx_MODER(x)      MMIO32(x + 0x00)
// Port x configuration bits (y = 0..15)
#define MODERx_IN(x)    0x00000000
#define MODERx_GPIO(x)  (1<<(x*2))
#define MODERx_AF(x)    (2<<(x*2))
#define MODERx_ANA(x)   (3<<(x*2))
#define MODER0_IN       0x00000000
#define MODER0_GPIO     0x00000001
#define MODER0_AF       0x00000002
#define MODER0_ANA      0x00000003
#define MODER1_IN       0x00000000
#define MODER1_GPIO     0x00000004
#define MODER1_AF       0x00000008
#define MODER1_ANA      0x0000000c
#define MODER2_IN       0x00000000
#define MODER2_GPIO     0x00000010
#define MODER2_AF       0x00000020
#define MODER2_ANA      0x00000030
#define MODER3_IN       0x00000000
#define MODER3_GPIO     0x00000040
#define MODER3_AF       0x00000080
#define MODER3_ANA      0x000000c0
#define MODER4_IN       0x00000000
#define MODER4_GPIO     0x00000100
#define MODER4_AF       0x00000200
#define MODER4_ANA      0x00000300
#define MODER5_IN       0x00000000
#define MODER5_GPIO     0x00000400
#define MODER5_AF       0x00000800
#define MODER5_ANA      0x00000c00
#define MODER6_IN       0x00000000
#define MODER6_GPIO     0x00001000
#define MODER6_AF       0x00002000
#define MODER6_ANA      0x00003000
#define MODER7_IN       0x00000000
#define MODER7_GPIO     0x00004000
#define MODER7_AF       0x00008000
#define MODER7_ANA      0x0000c000
#define MODER8_IN       0x00000000
#define MODER8_GPIO     0x00010000
#define MODER8_AF       0x00020000
#define MODER8_ANA      0x00030000
#define MODER9_IN       0x00000000
#define MODER9_GPIO     0x00040000
#define MODER9_AF       0x00080000
#define MODER9_ANA      0x000c0000
#define MODER10_IN      0x00000000
#define MODER10_GPIO    0x00100000
#define MODER10_AF      0x00200000
#define MODER10_ANA     0x00300000
#define MODER11_IN      0x00000000
#define MODER11_GPIO    0x00400000
#define MODER11_AF      0x00800000
#define MODER11_ANA     0x00c00000
#define MODER12_IN      0x00000000
#define MODER12_GPIO    0x01000000
#define MODER12_AF      0x02000000
#define MODER12_ANA     0x03000000
#define MODER13_IN      0x00000000
#define MODER13_GPIO    0x04000000
#define MODER13_AF      0x08000000
#define MODER13_ANA     0x0c000000
#define MODER14_IN      0x00000000
#define MODER14_GPIO    0x10000000
#define MODER14_AF      0x20000000
#define MODER14_ANA     0x30000000
#define MODER15_IN      0x00000000
#define MODER15_GPIO    0x40000000
#define MODER15_AF      0x80000000
#define MODER15_ANA     0xc0000000

/* GPIO port output type register */
#define GPIOA_OTYPER        MMIO32(GPIOA_BASE + 0x04)
#define GPIOB_OTYPER        MMIO32(GPIOB_BASE + 0x04)
#define GPIOC_OTYPER        MMIO32(GPIOC_BASE + 0x04)
#define GPIOD_OTYPER        MMIO32(GPIOD_BASE + 0x04)
#define GPIOE_OTYPER        MMIO32(GPIOE_BASE + 0x04)
#define GPIOF_OTYPER        MMIO32(GPIOF_BASE + 0x04)
#define GPIOG_OTYPER        MMIO32(GPIOG_BASE + 0x04)
#define GPIOH_OTYPER        MMIO32(GPIOH_BASE + 0x04)
#define GPIOI_OTYPER        MMIO32(GPIOI_BASE + 0x04)
#define GPIOx_OTYPER(x)     MMIO32(x + 0x04)
// Port x configuration bits (y = 0..15)
#define OTx_PP(x)   0x0000
#define OTx_OD(x)   (1<<x)
#define OT0_PP      0x0000
#define OT0_OD      0x0001
#define OT1_PP      0x0000
#define OT1_OD      0x0002
#define OT2_PP      0x0000
#define OT2_OD      0x0004
#define OT3_PP      0x0000
#define OT3_OD      0x0008
#define OT4_PP      0x0000
#define OT4_OD      0x0010
#define OT5_PP      0x0000
#define OT5_OD      0x0020
#define OT6_PP      0x0000
#define OT6_OD      0x0040
#define OT7_PP      0x0000
#define OT7_OD      0x0080
#define OT8_PP      0x0000
#define OT8_OD      0x0100
#define OT9_PP      0x0000
#define OT9_OD      0x0200
#define OT10_PP     0x0000
#define OT10_OD     0x0400
#define OT11_PP     0x0000
#define OT11_OD     0x0800
#define OT12_PP     0x0000
#define OT12_OD     0x1000
#define OT13_PP     0x0000
#define OT13_OD     0x2000
#define OT14_PP     0x0000
#define OT14_OD     0x4000
#define OT15_PP     0x0000
#define OT15_OD     0x8000

/* GPIO port output speed register */
#define GPIOA_OSPEEDR       MMIO32(GPIOA_BASE + 0x08)
#define GPIOB_OSPEEDR       MMIO32(GPIOB_BASE + 0x08)
#define GPIOC_OSPEEDR       MMIO32(GPIOC_BASE + 0x08)
#define GPIOD_OSPEEDR       MMIO32(GPIOD_BASE + 0x08)
#define GPIOE_OSPEEDR       MMIO32(GPIOE_BASE + 0x08)
#define GPIOF_OSPEEDR       MMIO32(GPIOF_BASE + 0x08)
#define GPIOG_OSPEEDR       MMIO32(GPIOG_BASE + 0x08)
#define GPIOH_OSPEEDR       MMIO32(GPIOH_BASE + 0x08)
#define GPIOI_OSPEEDR       MMIO32(GPIOI_BASE + 0x08)
#define GPIOx_OSPEEDR(x)    MMIO32(x + 0x08)
// Port x configuration bits (y = 0..15)
#define OSPEEDRx_LOW(x)         0x00000000
#define OSPEEDRx_MEDIUM(x)      (1<<(x*2))
#define OSPEEDRx_HIGH(x)        (2<<(x*2))
#define OSPEEDRx_VERY_HIGH(x)   (3<<(x*2))
#define OSPEEDR0_LOW            0x00000000
#define OSPEEDR0_MEDIUM         0x00000001
#define OSPEEDR0_HIGH           0x00000002
#define OSPEEDR0_VERY_HIGH      0x00000003
#define OSPEEDR1_LOW            0x00000000
#define OSPEEDR1_MEDIUM         0x00000004
#define OSPEEDR1_HIGH           0x00000008
#define OSPEEDR1_VERY_HIGH      0x0000000c
#define OSPEEDR2_LOW            0x00000000
#define OSPEEDR2_MEDIUM         0x00000010
#define OSPEEDR2_HIGH           0x00000020
#define OSPEEDR2_VERY_HIGH      0x00000030
#define OSPEEDR3_LOW            0x00000000
#define OSPEEDR3_MEDIUM         0x00000040
#define OSPEEDR3_HIGH           0x00000080
#define OSPEEDR3_VERY_HIGH      0x000000c0
#define OSPEEDR4_LOW            0x00000000
#define OSPEEDR4_MEDIUM         0x00000100
#define OSPEEDR4_HIGH           0x00000200
#define OSPEEDR4_VERY_HIGH      0x00000300
#define OSPEEDR5_LOW            0x00000000
#define OSPEEDR5_MEDIUM         0x00000400
#define OSPEEDR5_HIGH           0x00000800
#define OSPEEDR5_VERY_HIGH      0x00000c00
#define OSPEEDR6_LOW            0x00000000
#define OSPEEDR6_MEDIUM         0x00001000
#define OSPEEDR6_HIGH           0x00002000
#define OSPEEDR6_VERY_HIGH      0x00003000
#define OSPEEDR7_LOW            0x00000000
#define OSPEEDR7_MEDIUM         0x00004000
#define OSPEEDR7_HIGH           0x00008000
#define OSPEEDR7_VERY_HIGH      0x0000c000
#define OSPEEDR8_LOW            0x00000000
#define OSPEEDR8_MEDIUM         0x00010000
#define OSPEEDR8_HIGH           0x00020000
#define OSPEEDR8_VERY_HIGH      0x00030000
#define OSPEEDR9_LOW            0x00000000
#define OSPEEDR9_MEDIUM         0x00040000
#define OSPEEDR9_HIGH           0x00080000
#define OSPEEDR9_VERY_HIGH      0x000c0000
#define OSPEEDR10_LOW           0x00000000
#define OSPEEDR10_MEDIUM        0x00100000
#define OSPEEDR10_HIGH          0x00200000
#define OSPEEDR10_VERY_HIGH     0x00300000
#define OSPEEDR11_LOW           0x00000000
#define OSPEEDR11_MEDIUM        0x00400000
#define OSPEEDR11_HIGH          0x00800000
#define OSPEEDR11_VERY_HIGH     0x00c00000
#define OSPEEDR12_LOW           0x00000000
#define OSPEEDR12_MEDIUM        0x01000000
#define OSPEEDR12_HIGH          0x02000000
#define OSPEEDR12_VERY_HIGH     0x03000000
#define OSPEEDR13_LOW           0x00000000
#define OSPEEDR13_MEDIUM        0x04000000
#define OSPEEDR13_HIGH          0x08000000
#define OSPEEDR13_VERY_HIGH     0x0c000000
#define OSPEEDR14_LOW           0x00000000
#define OSPEEDR14_MEDIUM        0x10000000
#define OSPEEDR14_HIGH          0x20000000
#define OSPEEDR14_VERY_HIGH     0x30000000
#define OSPEEDR15_LOW           0x00000000
#define OSPEEDR15_MEDIUM        0x40000000
#define OSPEEDR15_HIGH          0x80000000
#define OSPEEDR15_VERY_HIGH     0xc0000000

/* GPIO port pull-up/pull-down register */
#define GPIOA_PUPDR         MMIO32(GPIOA_BASE + 0x0c)
#define GPIOB_PUPDR         MMIO32(GPIOB_BASE + 0x0c)
#define GPIOC_PUPDR         MMIO32(GPIOC_BASE + 0x0c)
#define GPIOD_PUPDR         MMIO32(GPIOD_BASE + 0x0c)
#define GPIOE_PUPDR         MMIO32(GPIOE_BASE + 0x0c)
#define GPIOF_PUPDR         MMIO32(GPIOF_BASE + 0x0c)
#define GPIOG_PUPDR         MMIO32(GPIOG_BASE + 0x0c)
#define GPIOH_PUPDR         MMIO32(GPIOH_BASE + 0x0c)
#define GPIOI_PUPDR         MMIO32(GPIOI_BASE + 0x0c)
#define GPIOx_PUPDR(x)      MMIO32(x + 0x0c)
// Port x configuration bits (y = 0..15)
#define PUPDRx_NO(x)    0x00000000
#define PUPDRx_UP(x)    (1<<(x*2))
#define PUPDRx_DOWN(x)  (2<<(x*2))
#define PUPDR0_NO       0x00000000
#define PUPDR0_UP       0x00000001
#define PUPDR0_DOWN     0x00000002
#define PUPDR1_NO       0x00000000
#define PUPDR1_UP       0x00000004
#define PUPDR1_DOWN     0x00000008
#define PUPDR2_NO       0x00000000
#define PUPDR2_UP       0x00000010
#define PUPDR2_DOWN     0x00000020
#define PUPDR3_NO       0x00000000
#define PUPDR3_UP       0x00000040
#define PUPDR3_DOWN     0x00000080
#define PUPDR4_NO       0x00000000
#define PUPDR4_UP       0x00000100
#define PUPDR4_DOWN     0x00000200
#define PUPDR5_NO       0x00000000
#define PUPDR5_UP       0x00000400
#define PUPDR5_DOWN     0x00000800
#define PUPDR6_NO       0x00000000
#define PUPDR6_UP       0x00001000
#define PUPDR6_DOWN     0x00002000
#define PUPDR7_NO       0x00000000
#define PUPDR7_UP       0x00004000
#define PUPDR7_DOWN     0x00008000
#define PUPDR8_NO       0x00000000
#define PUPDR8_UP       0x00010000
#define PUPDR8_DOWN     0x00020000
#define PUPDR9_NO       0x00000000
#define PUPDR9_UP       0x00040000
#define PUPDR9_DOWN     0x00080000
#define PUPDR10_NO      0x00000000
#define PUPDR10_UP      0x00100000
#define PUPDR10_DOWN    0x00200000
#define PUPDR11_NO      0x00000000
#define PUPDR11_UP      0x00400000
#define PUPDR11_DOWN    0x00800000
#define PUPDR12_NO      0x00000000
#define PUPDR12_UP      0x01000000
#define PUPDR12_DOWN    0x02000000
#define PUPDR13_NO      0x00000000
#define PUPDR13_UP      0x04000000
#define PUPDR13_DOWN    0x08000000
#define PUPDR14_NO      0x00000000
#define PUPDR14_UP      0x10000000
#define PUPDR14_DOWN    0x20000000
#define PUPDR15_NO      0x00000000
#define PUPDR15_UP      0x40000000
#define PUPDR15_DOWN    0x80000000

/* Port input data register */
#define GPIOA_IDR           MMIO32(GPIOA_BASE + 0x10)
#define GPIOB_IDR           MMIO32(GPIOB_BASE + 0x10)
#define GPIOC_IDR           MMIO32(GPIOC_BASE + 0x10)
#define GPIOD_IDR           MMIO32(GPIOD_BASE + 0x10)
#define GPIOE_IDR           MMIO32(GPIOE_BASE + 0x10)
#define GPIOF_IDR           MMIO32(GPIOF_BASE + 0x10)
#define GPIOG_IDR           MMIO32(GPIOG_BASE + 0x10)
#define GPIOH_IDR           MMIO32(GPIOH_BASE + 0x10)
#define GPIOI_IDR           MMIO32(GPIOI_BASE + 0x10)
#define GPIOx_IDR(x)        MMIO32(x + 0x08)
// pin bit definitions, they are the same for IDR, ODR, BSRR, BRR, LCKR regs
#define GPIO0   0x0001
#define GPIO1   0x0002
#define GPIO2   0x0004
#define GPIO3   0x0008
#define GPIO4   0x0010
#define GPIO5   0x0020
#define GPIO6   0x0040
#define GPIO7   0x0080
#define GPIO8   0x0100
#define GPIO9   0x0200
#define GPIO10  0x0400
#define GPIO11  0x0800
#define GPIO12  0x1000
#define GPIO13  0x2000
#define GPIO14  0x4000
#define GPIO15  0x8000
#define GPIO(n) (0x1 << n)
// IDR Port input data
#define IDR(n)  (1 << n)

/* Port output data register */
#define GPIOA_ODR           MMIO32(GPIOA_BASE + 0x14)
#define GPIOB_ODR           MMIO32(GPIOB_BASE + 0x14)
#define GPIOC_ODR           MMIO32(GPIOC_BASE + 0x14)
#define GPIOD_ODR           MMIO32(GPIOD_BASE + 0x14)
#define GPIOE_ODR           MMIO32(GPIOE_BASE + 0x14)
#define GPIOF_ODR           MMIO32(GPIOF_BASE + 0x14)
#define GPIOG_ODR           MMIO32(GPIOG_BASE + 0x14)
#define GPIOH_ODR           MMIO32(GPIOH_BASE + 0x14)
#define GPIOI_ODR           MMIO32(GPIOI_BASE + 0x14)
#define GPIOx_ODR(x)        MMIO32(x + 0x0c)
// ODR Port output data
#define ODR(n)  (1 << n)

/* Port bit set/reset register */
#define GPIOA_BSRR          MMIO32(GPIOA_BASE + 0x18)
#define GPIOB_BSRR          MMIO32(GPIOB_BASE + 0x18)
#define GPIOC_BSRR          MMIO32(GPIOC_BASE + 0x18)
#define GPIOD_BSRR          MMIO32(GPIOD_BASE + 0x18)
#define GPIOE_BSRR          MMIO32(GPIOE_BASE + 0x18)
#define GPIOF_BSRR          MMIO32(GPIOF_BASE + 0x18)
#define GPIOG_BSRR          MMIO32(GPIOG_BASE + 0x18)
#define GPIOH_BSRR          MMIO32(GPIOH_BASE + 0x18)
#define GPIOI_BSRR          MMIO32(GPIOI_BASE + 0x18)
#define GPIOx_BSRR(x)       MMIO32(x + 0x10)
// Port reset bit
#define BR(n)   ((uint32_t)(1 << (n+16)))
#define BR0     0x00010000
#define BR1     0x00020000
#define BR2     0x00040000
#define BR3     0x00080000
#define BR4     0x00100000
#define BR5     0x00200000
#define BR6     0x00400000
#define BR7     0x00800000
#define BR8     0x01000000
#define BR9     0x02000000
#define BR10    0x04000000
#define BR11    0x08000000
#define BR12    0x10000000
#define BR13    0x20000000
#define BR14    0x40000000
#define BR15    0x80000000
// Port set bit
#define BS(n)   ((uint32_t)(1 << n))

/* Port configuration lock register */
#define GPIOA_LCKR          MMIO32(GPIOA_BASE + 0x1c)
#define GPIOB_LCKR          MMIO32(GPIOB_BASE + 0x1c)
#define GPIOC_LCKR          MMIO32(GPIOC_BASE + 0x1c)
#define GPIOD_LCKR          MMIO32(GPIOD_BASE + 0x1c)
#define GPIOE_LCKR          MMIO32(GPIOE_BASE + 0x1c)
#define GPIOF_LCKR          MMIO32(GPIOE_BASE + 0x1c)
#define GPIOG_LCKR          MMIO32(GPIOE_BASE + 0x1c)
#define GPIOH_LCKR          MMIO32(GPIOE_BASE + 0x1c)
#define GPIOI_LCKR          MMIO32(GPIOE_BASE + 0x1c)
#define GPIOx_LCKR(x)       MMIO32(x + 0x1c)
// Lock key
#define LCKK            0x00010000
// Port lock bit
#define LCK_GPIO(n)     ((uint32_t)(1 << n))

/* Port configuration lock register */
#define GPIOA_AFRL          MMIO32(GPIOA_BASE + 0x20)
#define GPIOB_AFRL          MMIO32(GPIOB_BASE + 0x20)
#define GPIOC_AFRL          MMIO32(GPIOC_BASE + 0x20)
#define GPIOD_AFRL          MMIO32(GPIOD_BASE + 0x20)
#define GPIOE_AFRL          MMIO32(GPIOE_BASE + 0x20)
#define GPIOF_AFRL          MMIO32(GPIOE_BASE + 0x20)
#define GPIOG_AFRL          MMIO32(GPIOE_BASE + 0x20)
#define GPIOH_AFRL          MMIO32(GPIOE_BASE + 0x20)
#define GPIOI_AFRL          MMIO32(GPIOE_BASE + 0x20)
#define GPIOx_AFRL(x)       MMIO32(x + 0x20)
// Alternate function selection for port x bit y (y = 0..7)
#define AFRLx_AF0(x)    0x00000000
#define AFRLx_AF1(x)    (0x1<<(x*4))
#define AFRLx_AF2(x)    (0x2<<(x*4))
#define AFRLx_AF3(x)    (0x3<<(x*4))
#define AFRLx_AF4(x)    (0x4<<(x*4))
#define AFRLx_AF5(x)    (0x5<<(x*4))
#define AFRLx_AF6(x)    (0x6<<(x*4))
#define AFRLx_AF7(x)    (0x7<<(x*4))
#define AFRLx_AF8(x)    (0x8<<(x*4))
#define AFRLx_AF9(x)    (0x9<<(x*4))
#define AFRLx_AF10(x)   (0xa<<(x*4))
#define AFRLx_AF11(x)   (0xb<<(x*4))
#define AFRLx_AF12(x)   (0xc<<(x*4))
#define AFRLx_AF13(x)   (0xd<<(x*4))
#define AFRLx_AF14(x)   (0xe<<(x*4))
#define AFRLx_AF15(x)   (0xf<<(x*4))
#define AFRL0_AF0       0x00000000
#define AFRL0_AF1       0x00000001
#define AFRL0_AF2       0x00000002
#define AFRL0_AF3       0x00000003
#define AFRL0_AF4       0x00000004
#define AFRL0_AF5       0x00000005
#define AFRL0_AF6       0x00000006
#define AFRL0_AF7       0x00000007
#define AFRL0_AF8       0x00000008
#define AFRL0_AF9       0x00000009
#define AFRL0_AF10      0x0000000a
#define AFRL0_AF11      0x0000000b
#define AFRL0_AF12      0x0000000c
#define AFRL0_AF13      0x0000000d
#define AFRL0_AF14      0x0000000e
#define AFRL0_AF15      0x0000000f
#define AFRL1_AF0       0x00000000
#define AFRL1_AF1       0x00000010
#define AFRL1_AF2       0x00000020
#define AFRL1_AF3       0x00000030
#define AFRL1_AF4       0x00000040
#define AFRL1_AF5       0x00000050
#define AFRL1_AF6       0x00000060
#define AFRL1_AF7       0x00000070
#define AFRL1_AF8       0x00000080
#define AFRL1_AF9       0x00000090
#define AFRL1_AF10      0x000000a0
#define AFRL1_AF11      0x000000b0
#define AFRL1_AF12      0x000000c0
#define AFRL1_AF13      0x000000d0
#define AFRL1_AF14      0x000000e0
#define AFRL1_AF15      0x000000f0
#define AFRL2_AF0       0x00000000
#define AFRL2_AF1       0x00000100
#define AFRL2_AF2       0x00000200
#define AFRL2_AF3       0x00000300
#define AFRL2_AF4       0x00000400
#define AFRL2_AF5       0x00000500
#define AFRL2_AF6       0x00000600
#define AFRL2_AF7       0x00000700
#define AFRL2_AF8       0x00000800
#define AFRL2_AF9       0x00000900
#define AFRL2_AF10      0x00000a00
#define AFRL2_AF11      0x00000b00
#define AFRL2_AF12      0x00000c00
#define AFRL2_AF13      0x00000d00
#define AFRL2_AF14      0x00000e00
#define AFRL2_AF15      0x00000f00
#define AFRL3_AF0       0x00000000
#define AFRL3_AF1       0x00001000
#define AFRL3_AF2       0x00002000
#define AFRL3_AF3       0x00003000
#define AFRL3_AF4       0x00004000
#define AFRL3_AF5       0x00005000
#define AFRL3_AF6       0x00006000
#define AFRL3_AF7       0x00007000
#define AFRL3_AF8       0x00008000
#define AFRL3_AF9       0x00009000
#define AFRL3_AF10      0x0000a000
#define AFRL3_AF11      0x0000b000
#define AFRL3_AF12      0x0000c000
#define AFRL3_AF13      0x0000d000
#define AFRL3_AF14      0x0000e000
#define AFRL3_AF15      0x0000f000
#define AFRL4_AF0       0x00000000
#define AFRL4_AF1       0x00010000
#define AFRL4_AF2       0x00020000
#define AFRL4_AF3       0x00030000
#define AFRL4_AF4       0x00040000
#define AFRL4_AF5       0x00050000
#define AFRL4_AF6       0x00060000
#define AFRL4_AF7       0x00070000
#define AFRL4_AF8       0x00080000
#define AFRL4_AF9       0x00090000
#define AFRL4_AF10      0x000a0000
#define AFRL4_AF11      0x000b0000
#define AFRL4_AF12      0x000c0000
#define AFRL4_AF13      0x000d0000
#define AFRL4_AF14      0x000e0000
#define AFRL4_AF15      0x000f0000
#define AFRL5_AF0       0x00000000
#define AFRL5_AF1       0x00100000
#define AFRL5_AF2       0x00200000
#define AFRL5_AF3       0x00300000
#define AFRL5_AF4       0x00400000
#define AFRL5_AF5       0x00500000
#define AFRL5_AF6       0x00600000
#define AFRL5_AF7       0x00700000
#define AFRL5_AF8       0x00800000
#define AFRL5_AF9       0x00900000
#define AFRL5_AF10      0x00a00000
#define AFRL5_AF11      0x00b00000
#define AFRL5_AF12      0x00c00000
#define AFRL5_AF13      0x00d00000
#define AFRL5_AF14      0x00e00000
#define AFRL5_AF15      0x00f00000
#define AFRL6_AF0       0x00000000
#define AFRL6_AF1       0x01000000
#define AFRL6_AF2       0x02000000
#define AFRL6_AF3       0x03000000
#define AFRL6_AF4       0x04000000
#define AFRL6_AF5       0x05000000
#define AFRL6_AF6       0x06000000
#define AFRL6_AF7       0x07000000
#define AFRL6_AF8       0x08000000
#define AFRL6_AF9       0x09000000
#define AFRL6_AF10      0x0a000000
#define AFRL6_AF11      0x0b000000
#define AFRL6_AF12      0x0c000000
#define AFRL6_AF13      0x0d000000
#define AFRL6_AF14      0x0e000000
#define AFRL6_AF15      0x0f000000
#define AFRL7_AF0       0x00000000
#define AFRL7_AF1       0x10000000
#define AFRL7_AF2       0x20000000
#define AFRL7_AF3       0x30000000
#define AFRL7_AF4       0x40000000
#define AFRL7_AF5       0x50000000
#define AFRL7_AF6       0x60000000
#define AFRL7_AF7       0x70000000
#define AFRL7_AF8       0x80000000
#define AFRL7_AF9       0x90000000
#define AFRL7_AF10      0xa0000000
#define AFRL7_AF11      0xb0000000
#define AFRL7_AF12      0xc0000000
#define AFRL7_AF13      0xd0000000
#define AFRL7_AF14      0xe0000000
#define AFRL7_AF15      0xf0000000

/* Port configuration lock register */
#define GPIOA_AFRH          MMIO32(GPIOA_BASE + 0x24)
#define GPIOB_AFRH          MMIO32(GPIOB_BASE + 0x24)
#define GPIOC_AFRH          MMIO32(GPIOC_BASE + 0x24)
#define GPIOD_AFRH          MMIO32(GPIOD_BASE + 0x24)
#define GPIOE_AFRH          MMIO32(GPIOE_BASE + 0x24)
#define GPIOF_AFRH          MMIO32(GPIOE_BASE + 0x24)
#define GPIOG_AFRH          MMIO32(GPIOE_BASE + 0x24)
#define GPIOH_AFRH          MMIO32(GPIOE_BASE + 0x24)
#define GPIOI_AFRH          MMIO32(GPIOE_BASE + 0x24)
#define GPIOx_AFRH(x)       MMIO32(x + 0x24)
// Alternate function selection for port x bit y (y = 8..15)
#define AFRHx_AF0(x)    0x00000000
#define AFRHx_AF1(x)    (0x1<<((x-8)*4))
#define AFRHx_AF2(x)    (0x2<<((x-8)*4))
#define AFRHx_AF3(x)    (0x3<<((x-8)*4))
#define AFRHx_AF4(x)    (0x4<<((x-8)*4))
#define AFRHx_AF5(x)    (0x5<<((x-8)*4))
#define AFRHx_AF6(x)    (0x6<<((x-8)*4))
#define AFRHx_AF7(x)    (0x7<<((x-8)*4))
#define AFRHx_AF8(x)    (0x8<<((x-8)*4))
#define AFRHx_AF9(x)    (0x9<<((x-8)*4))
#define AFRHx_AF10(x)   (0xa<<((x-8)*4))
#define AFRHx_AF11(x)   (0xb<<((x-8)*4))
#define AFRHx_AF12(x)   (0xc<<((x-8)*4))
#define AFRHx_AF13(x)   (0xd<<((x-8)*4))
#define AFRHx_AF14(x)   (0xe<<((x-8)*4))
#define AFRHx_AF15(x)   (0xf<<((x-8)*4))
#define AFRH8_AF0       0x00000000
#define AFRH8_AF1       0x00000001
#define AFRH8_AF2       0x00000002
#define AFRH8_AF3       0x00000003
#define AFRH8_AF4       0x00000004
#define AFRH8_AF5       0x00000005
#define AFRH8_AF6       0x00000006
#define AFRH8_AF7       0x00000007
#define AFRH8_AF8       0x00000008
#define AFRH8_AF9       0x00000009
#define AFRH8_AF10      0x0000000a
#define AFRH8_AF11      0x0000000b
#define AFRH8_AF12      0x0000000c
#define AFRH8_AF13      0x0000000d
#define AFRH8_AF14      0x0000000e
#define AFRH8_AF15      0x0000000f
#define AFRH9_AF0       0x00000000
#define AFRH9_AF1       0x00000010
#define AFRH9_AF2       0x00000020
#define AFRH9_AF3       0x00000030
#define AFRH9_AF4       0x00000040
#define AFRH9_AF5       0x00000050
#define AFRH9_AF6       0x00000060
#define AFRH9_AF7       0x00000070
#define AFRH9_AF8       0x00000080
#define AFRH9_AF9       0x00000090
#define AFRH9_AF10      0x000000a0
#define AFRH9_AF11      0x000000b0
#define AFRH9_AF12      0x000000c0
#define AFRH9_AF13      0x000000d0
#define AFRH9_AF14      0x000000e0
#define AFRH9_AF15      0x000000f0
#define AFRH10_AF0      0x00000000
#define AFRH10_AF1      0x00000100
#define AFRH10_AF2      0x00000200
#define AFRH10_AF3      0x00000300
#define AFRH10_AF4      0x00000400
#define AFRH10_AF5      0x00000500
#define AFRH10_AF6      0x00000600
#define AFRH10_AF7      0x00000700
#define AFRH10_AF8      0x00000800
#define AFRH10_AF9      0x00000900
#define AFRH10_AF10     0x00000a00
#define AFRH10_AF11     0x00000b00
#define AFRH10_AF12     0x00000c00
#define AFRH10_AF13     0x00000d00
#define AFRH10_AF14     0x00000e00
#define AFRH10_AF15     0x00000f00
#define AFRH11_AF0      0x00000000
#define AFRH11_AF1      0x00001000
#define AFRH11_AF2      0x00002000
#define AFRH11_AF3      0x00003000
#define AFRH11_AF4      0x00004000
#define AFRH11_AF5      0x00005000
#define AFRH11_AF6      0x00006000
#define AFRH11_AF7      0x00007000
#define AFRH11_AF8      0x00008000
#define AFRH11_AF9      0x00009000
#define AFRH11_AF10     0x0000a000
#define AFRH11_AF11     0x0000b000
#define AFRH11_AF12     0x0000c000
#define AFRH11_AF13     0x0000d000
#define AFRH11_AF14     0x0000e000
#define AFRH11_AF15     0x0000f000
#define AFRH12_AF0      0x00000000
#define AFRH12_AF1      0x00010000
#define AFRH12_AF2      0x00020000
#define AFRH12_AF3      0x00030000
#define AFRH12_AF4      0x00040000
#define AFRH12_AF5      0x00050000
#define AFRH12_AF6      0x00060000
#define AFRH12_AF7      0x00070000
#define AFRH12_AF8      0x00080000
#define AFRH12_AF9      0x00090000
#define AFRH12_AF10     0x000a0000
#define AFRH12_AF11     0x000b0000
#define AFRH12_AF12     0x000c0000
#define AFRH12_AF13     0x000d0000
#define AFRH12_AF14     0x000e0000
#define AFRH12_AF15     0x000f0000
#define AFRH13_AF0      0x00000000
#define AFRH13_AF1      0x00100000
#define AFRH13_AF2      0x00200000
#define AFRH13_AF3      0x00300000
#define AFRH13_AF4      0x00400000
#define AFRH13_AF5      0x00500000
#define AFRH13_AF6      0x00600000
#define AFRH13_AF7      0x00700000
#define AFRH13_AF8      0x00800000
#define AFRH13_AF9      0x00900000
#define AFRH13_AF10     0x00a00000
#define AFRH13_AF11     0x00b00000
#define AFRH13_AF12     0x00c00000
#define AFRH13_AF13     0x00d00000
#define AFRH13_AF14     0x00e00000
#define AFRH13_AF15     0x00f00000
#define AFRH14_AF0      0x00000000
#define AFRH14_AF1      0x01000000
#define AFRH14_AF2      0x02000000
#define AFRH14_AF3      0x03000000
#define AFRH14_AF4      0x04000000
#define AFRH14_AF5      0x05000000
#define AFRH14_AF6      0x06000000
#define AFRH14_AF7      0x07000000
#define AFRH14_AF8      0x08000000
#define AFRH14_AF9      0x09000000
#define AFRH14_AF10     0x0a000000
#define AFRH14_AF11     0x0b000000
#define AFRH14_AF12     0x0c000000
#define AFRH14_AF13     0x0d000000
#define AFRH14_AF14     0x0e000000
#define AFRH14_AF15     0x0f000000
#define AFRH15_AF0      0x00000000
#define AFRH15_AF1      0x10000000
#define AFRH15_AF2      0x20000000
#define AFRH15_AF3      0x30000000
#define AFRH15_AF4      0x40000000
#define AFRH15_AF5      0x50000000
#define AFRH15_AF6      0x60000000
#define AFRH15_AF7      0x70000000
#define AFRH15_AF8      0x80000000
#define AFRH15_AF9      0x90000000
#define AFRH15_AF10     0xa0000000
#define AFRH15_AF11     0xb0000000
#define AFRH15_AF12     0xc0000000
#define AFRH15_AF13     0xd0000000
#define AFRH15_AF14     0xe0000000
#define AFRH15_AF15     0xf0000000

// some macro for gpio api
#define GPIOA   GPIOA_BASE
#define GPIOB   GPIOB_BASE
#define GPIOC   GPIOC_BASE
#define GPIOD   GPIOD_BASE
#define GPIOE   GPIOE_BASE
#define GPIOF   GPIOE_BASE
#define GPIOG   GPIOE_BASE
#define GPIOH   GPIOE_BASE
#define GPIOI   GPIOE_BASE

#endif
