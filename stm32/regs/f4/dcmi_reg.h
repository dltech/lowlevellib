#ifndef H_DCMI_REG
#define H_DCMI_REG
/*
 * Part of Belkin STM32 HAL, Digital camera interface (DCMI)
 * register definitions of STM32F4xx MCUs.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* DCMI control register 1 */
#define DCMI_CR     MMIO32(DCMI_BASE + 0x00)
// DCMI enable
#define DCMI_ENABLE 0x4000
// Extended data mode
#define EDM_8BIT    0x0000
#define EDM_10BIT   0x0400
#define EDM_12BIT   0x0800
#define EDM_14BIT   0x0c00
// Frame capture rate control
#define FCRC_ALL    0x0000
#define FCRC_50P    0x0100
#define FCRC_75P    0x0200
// Vertical synchronization polarity
#define VSPOL       0x0080
// Horizontal synchronization polarity
#define HSPOL       0x0040
// Pixel clock polarity
#define PCKPOL      0x0020
// Embedded synchronization select
#define ESS         0x0010
// JPEG format
#define JPEG        0x0008
// Crop feature
#define CROP        0x0004
// Capture mode
#define CM          0x0002
// Capture enable
#define CAPTURE     0x0001

/* DCMI status register */
#define DCMI_SR     MMIO32(DCMI_BASE + 0x04)
// FIFO not empty
#define FNE     0x4
// This bit gives the state of the VSYNC pin with the correct progr polarity.
#define VSYNC   0x2
// This bit gives the state of the HSYNC pin with the correct progr polarity.
#define HSYNC   0x1

/* DCMI raw interrupt status register */
#define DCMI_RIS    MMIO32(DCMI_BASE + 0x08)
// Line raw interrupt status
#define LINE_RIS    0x10
// VSYNC raw interrupt status
#define VSYNC_RIS   0x08
// Synchronization error raw interrupt status
#define ERR_RIS     0x04
// Overrun raw interrupt status
#define OVR_RIS     0x02
// Capture complete raw interrupt status
#define FRAME_RIS   0x01

/* DCMI interrupt enable register */
#define DCMI_IER    MMIO32(DCMI_BASE + 0x0c)
// Line interrupt enable
#define LINE_IE     0x10
// VSYNC interrupt enable
#define VSYNC_IE    0x08
// Synchronization error interrupt enable
#define ERR_IE      0x04
// Overrun interrupt enable
#define OVR_IE      0x02
// Capture complete interrupt enable
#define FRAME_IE    0x01

/* DCMI masked interrupt status register */
#define DCMI_MIS    MMIO32(DCMI_BASE + 0x10)
// Line masked interrupt status
#define LINE_MIS    0x10
// VSYNC masked interrupt status
#define VSYNC_MIS   0x08
// Synchronization error masked interrupt status
#define ERR_MIS     0x04
// Overrun masked interrupt status
#define OVR_MIS     0x02
// Capture complete masked interrupt status
#define FRAME_MIS   0x01

/* DCMI interrupt clear register */
#define DCMI_ICR    MMIO32(DCMI_BASE + 0x14)
// line interrupt status clear
#define LINE_ISC    0x10
// Vertical synch interrupt status clear
#define VSYNC_ISC   0x08
// Synchronization error interrupt status clear
#define ERR_ISC     0x04
// Overrun interrupt status clear
#define OVR_ISC     0x02
// Capture complete interrupt status clear
#define FRAME_ISC   0x01

/* DCMI embedded synchronization code register */
#define DCMI_ESCR   MMIO32(DCMI_BASE + 0x18)
// Frame end delimiter code
#define FEC_MSK     0xff000000
#define FEC_SFT     24
#define FEC_GET     ((DCMI_ESCR>>FEC_SFT)&0xff)
#define FEC_SET(x)  ((x<<FEC_SFT)&FEC_MSK)
// Line end delimiter code
#define LEC_MSK     0x00ff0000
#define LEC_SFT     16
#define LEC_GET     ((DCMI_ESCR>>LEC_SFT)&0xff)
#define LEC_SET(x)  ((x<<LEC_SFT)&LEC_MSK)
// Line start delimiter code
#define LSC_MSK     0x0000ff00
#define LSC_SFT     8
#define LSC_GET     ((DCMI_ESCR>>LSC_SFT)&0xff)
#define LSC_SET(x)  ((x<<LSC_SFT)&LSC_MSK)
// Frame start delimiter code
#define FSC_MSK     0x000000ff
#define FSC_GET     (DCMI_ESCR&FSC_MSK)
#define FSC_SET(x)  (x&FSC_MSK)

/* DCMI embedded synchronization unmask register */
#define DCMI_ESUR   MMIO32(DCMI_BASE + 0x1c)
// Frame end delimiter code
#define FEU_MSK     0xff000000
#define FEU_SFT     24
#define FEU_GET     ((DCMI_ESUR>>FEU_SFT)&0xff)
#define FEU_SET(x)  ((x<<FEU_SFT)&FEU_MSK)
// Line end delimiter code
#define LEU_MSK     0x00ff0000
#define LEU_SFT     16
#define LEU_GET     ((DCMI_ESUR>>LEU_SFT)&0xff)
#define LEU_SET(x)  ((x<<LEU_SFT)&LEU_MSK)
// Line start delimiter code
#define LSU_MSK     0x0000ff00
#define LSU_SFT     8
#define LSU_GET     ((DCMI_ESUR>>LSU_SFT)&0xff)
#define LSU_SET(x)  ((x<<LSU_SFT)&LSU_MSK)
// Frame start delimiter code
#define FSU_MSK     0x000000ff
#define FSU_GET     (DCMI_ESUR&FSU_MSK)
#define FSU_SET(x)  (x&FSU_MSK)

/* DCMI crop window start */
#define DCMI_CWSTRT MMIO32(DCMI_BASE + 0x20)
// Vertical start line count
#define VST_MSK         0x3fff0000
#define VST_SFT         16
#define VST_GET         ((DCMI_CWSTRT>>VST_SFT)&0x3fff)
#define VST_SET(x)      ((x<<VST_SFT)&VST_MSK)
// Frame start delimiter code
#define HOFFCNT_MSK     0x00003fff
#define HOFFCNT_GET     (DCMI_CWSTRT&HOFFCNT_MSK)
#define HOFFCNT_SET(x)  (x&HOFFCNT_MSK)

/* DCMI crop window size */
#define DCMI_CWSIZE MMIO32(DCMI_BASE + 0x24)
// Vertical line count
#define VLINE_MSK       0x3fff0000
#define VLINE_SFT       16
#define VLINE_GET       ((DCMI_CWSIZE>>VLINE_SFT)&0x3fff)
#define VLINE_SET(x)    ((x<<VLINE_SFT)&VLINE_MSK)
// Capture count
#define CAPCNT_MSK      0x00003fff
#define CAPCNT_GET      (DCMI_CWSIZE&CAPCNT_MSK)
#define CAPCNT_SET(x)   (x&CAPCNT_MSK)

/* DCMI data register */
#define DCMI_DR     MMIO32(DCMI_BASE + 0x28)
// Frame end delimiter code
#define DATA_BYTE3_MSK      0xff000000
#define DATA_BYTE3_SFT      24
#define DATA_BYTE3_GET      ((DCMI_DR>>DATA_BYTE3_SFT)&0xff)
#define DATA_BYTE3_SET(x)   ((x<<DATA_BYTE3_SFT)&DATA_BYTE3_MSK)
// Line end delimiter code
#define DATA_BYTE2_MSK      0x00ff0000
#define DATA_BYTE2_SFT      16
#define DATA_BYTE2_GET      ((DCMI_DR>>DATA_BYTE2_SFT)&0xff)
#define DATA_BYTE2_SET(x)   ((x<<DATA_BYTE2_SFT)&DATA_BYTE2_MSK)
// Line start delimiter code
#define DATA_BYTE1_MSK      0x0000ff00
#define DATA_BYTE1_SFT      8
#define DATA_BYTE1_GET      ((DCMI_DR>>DATA_BYTE1_SFT)&0xff)
#define DATA_BYTE1_SET(x)   ((x<<DATA_BYTE1_SFT)&DATA_BYTE1_MSK)
// Frame start delimiter code
#define DATA_BYTE0_MSK      0x000000ff
#define DATA_BYTE0_GET      (DCMI_DR&DATA_BYTE0_MSK)
#define DATA_BYTE0_SET(x)   (x&DATA_BYTE0_MSK)

#endif
