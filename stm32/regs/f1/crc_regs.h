#ifndef H_CRC_REGS
#define H_CRC_REGS
/*
 * Part of Belkin STM32 HAL, CRC calculation unit registers
 * of STMF103 MCU.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "memorymap.h"

/* Data register */
#define CRC_DR  MMIO32(CRC_BASE + 0x00)
// Data register bits DR[0:31]

/* Independent data register */
#define CRC_IDR MMIO32(CRC_BASE + 0x04)
// General-purpose 8-bit data register bits IDR[7:0]

/* Control register */
#define CRC_CR  MMIO32(CRC_BASE + 0x08)
// Resets the CRC calc unit and sets the data reg to 0xFFFF FFFF.
#define RESET_BIT 0x1

#endif
