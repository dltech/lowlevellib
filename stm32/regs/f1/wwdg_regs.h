#ifndef H_WWDG_REGS
#define H_WWDG_REGS
/*
 * Part of Belkin STM32 HAL, Window watchdog (WWDG) registers of
 * STMF103 MCU.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "memorymap.h"

/* Control register */
#define WWDG_CR     MMIO32(WWDG_BASE + 0x00)
// Activation bit 1: Watchdog enabled
#define WDGA    0x80
// 7-bit counter decremented every (4096 x 2WDGTB[1:0]) PCLK1
#define T_MSK   0x7f

/* Configuration register */
#define WWDG_CFR    MMIO32(WWDG_BASE + 0x04)
// Early wakeup interrupt, at value 0x40.
#define EWI     0x200
// Timer base. The time base of the prescaler.
#define WDGTB1  0x000
#define WDGTB2  0x080
#define WDGTB4  0x100
#define WDGTB8  0x180
// 7-bit window value, value to be compared to the downcounter.
#define W_MSK   0x07f

/* Status register */
#define WWDG_SR     MMIO32(WWDG_BASE + 0x08)
// Early wakeup interrupt flag
#define EWIF    0x1

#endif
