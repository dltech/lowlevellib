#ifndef H_NMEA_HEADERS
#define H_NMEA_HEADERS
/*
 * NMEA parser fixed-point C library. Constants and pack format.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define HEADER_SIZE 6
static uint8_t dtm_header[HEADER_SIZE] = {'$','G','P','D','T','M'};
static uint8_t gbs_header[HEADER_SIZE] = {'$','G','P','D','T','M'};
static uint8_t gga_header[HEADER_SIZE] = {'$','G','P','G','G','A'};
static uint8_t gll_header[HEADER_SIZE] = {'$','G','P','G','L','L'};
static uint8_t grs_header[HEADER_SIZE] = {'$','G','P','G','R','S'};
static uint8_t gsa_header[HEADER_SIZE] = {'$','G','P','G','S','A'};
static uint8_t gsv_header[HEADER_SIZE] = {'$','G','P','G','S','V'};
static uint8_t mss_header[HEADER_SIZE] = {'$','G','P','M','S','S'};
static uint8_t rmc_header[HEADER_SIZE] = {'$','G','P','R','M','C'};
static uint8_t vtg_header[HEADER_SIZE] = {'$','G','P','V','T','G'};
static uint8_t zda_header[HEADER_SIZE] = {'$','G','P','Z','D','A'};
#define CRC_ZIZE    2

#define START_CHARACTER '$'
#define DATA_DIVIDER    ','
#define END_CHARACTER   '*'

#endif
