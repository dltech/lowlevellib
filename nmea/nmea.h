#ifndef H_NMEA
#define H_NMEA
/*
 * NMEA parser fixed-point C library.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GGA pack constants
#define FIX_INVALID         '0'
#define FIX_VALID           '1'
#define FIX_DIFFERENTIAL    '2'
// GLL pack constants
#define MODE_AUTONOMOUSS    'A'
#define MODE_DGPS           'D'
#define MODE_DR             'E'
// GSA pack constants
#define MODE1_MANUAL        'M'
#define MODE1_AUTOMATIC     'A'
#define MODE2_FIX_NOT_AVAIL 1
#define MODE2_2D            2
#define MODE2_3D            3
// GRS pack constants
#define MODE_IN_USE             0
#define MODE_RECALCULATE_AFTER  1
// THS pack constants
#define MODE_AUTONOMOUS 'A'
#define MODE_ESTIMATED  'E'
#define MODE_MANUAL     'M'
#define MODE_SIMULATOR  'S'
#define MODE_NOT_VALID  'V'
// TXT pack constants
#define ERROR   0
#define WARNING 1
#define NOTICE  2
#define USER    7

// headers list
enum {
    NO_HEADER = 0,
    END_CHAR,
    CRC_OBTAINED,
    CRC_OK,
    CRC_FAILED,
    START_CHAR,
    DTM_HEAD,
    GBS_HEAD,
    GGA_HEAD,
    GLL_HEAD,
    GRS_HEAD,
    GSA_HEAD,
    GSV_HEAD,
    MSS_HEAD,
    RMC_HEAD,
    VTG_HEAD,
    ZDA_HEAD
};

typedef struct {
    uint8_t hour;
    uint8_t minutes;
    uint8_t seconds;
    uint8_t milliseconds;
    uint16_t errlat;
    uint16_t errlon;
    uint16_t erralt;
    int16_t bias;
    uint16_t stdev;
} gps_datat;

/* GBS - GNSS Satellite Fault Detection */
typedef struct {
    uint32_t hour;
    uint32_t minutes;
    uint32_t seconds;
    uint32_t milliseconds;
    uint32_t errlat;
    uint32_t errlon;
    uint32_t erralt;
    uint32_t svid;
    uint32_t bias;
    uint32_t stddev;
} gbs_packt;

/* GGA - Global positioning system fix data */
typedef struct {
    uint32_t hour;
    uint32_t minutes;
    uint32_t seconds;
    uint32_t milliseconds;
    uint32_t latitude;
    uint32_t latitude_d;
    char ns;
    uint32_t longitude;
    uint32_t longitude_d;
    char ew;
    uint32_t fix;
    uint32_t sattelites;
    uint32_t hdop;
    uint32_t altitude;
    int32_t geoid_separation;
    uint32_t station_id;
} gga_packt;

/* GLL - Latitude and longitude, with time of position fix and status */
typedef struct {
    uint32_t latitude;
    uint32_t latitude_d;
    char ns;
    uint32_t longitude;
    uint32_t longitude_d;
    char ew;
    uint32_t utc_hour;
    uint32_t utc_minutes;
    uint32_t utc_seconds;
    uint32_t utc_milliseconds;
    char status;
    char mode;
} gll_packt;

/* GRS - GNSS Range Residuals */
typedef struct {
    uint32_t hour;
    uint32_t minutes;
    uint32_t seconds;
    uint32_t milliseconds;
    uint32_t mode;
    int32_t residual[12];
} grs_packt;

/* GSA - GNSS DOP and Active Satellites */
typedef struct {
    char mode1;
    uint32_t mode2;
    uint32_t channel[12];
    uint32_t pdop;
    uint32_t hdop;
    uint32_t vdop;
} gsa_packt;

/* GST - GNSS Pseudo Range Error Statistics */
typedef struct {
    uint32_t hour;
    uint32_t minutes;
    uint32_t seconds;
    uint32_t milliseconds;
    uint32_t range_rms;
    uint32_t std_lat;
    uint32_t std_long;
    uint32_t std_alt;
} gst_packt;

typedef struct {
    uint32_t id;
    uint32_t elevation;
    uint32_t azimuth;
    uint32_t snr;
} gsv2_packt;

/* GSV - GNSS Satellites in View */
typedef struct {
    uint32_t messages;
    uint32_t sattelites;
    gsv2_packt sattelite[12];
} gsv_packt;

typedef struct {
    uint32_t signal_strenth;
    uint32_t snr;
    uint32_t beacon_freq;
    uint32_t beacon_bitrate;
} mss_packt;

/* RMC - Recommended Minimum data */
typedef struct {
    uint32_t utc_hour;
    uint32_t utc_minutes;
    uint32_t utc_seconds;
    uint32_t utc_milliseconds;
    char status;
    uint32_t latitude;
    uint32_t latitude_d;
    char ns;
    uint32_t longitude;
    uint32_t longitude_d;
    char ew;
    uint32_t speed_og;
    uint32_t cource_og;
    uint32_t date;
    uint32_t mouth;
    uint32_t year;
    char mode;
} rmc_packt;

/* THS - True Heading and Status */
typedef struct {
    uint32_t heading;
    char mode;
} ths_packt;

/* TXT - Text Transmission */
typedef struct {
    uint32_t amount;
    uint32_t number;
    uint32_t text_id;
    char text[20];
} txt_packt;

/* VTG - Course over ground and Ground speed */
typedef struct {
    uint32_t cource;
    uint32_t speed_knots;
    uint32_t speed_kmh;
    char mode;
} vtg_packt;

/* ZDA - Time and Date */
typedef struct {
    uint32_t utc_hour;
    uint32_t utc_minutes;
    uint32_t utc_seconds;
    uint32_t utc_milliseconds;
    uint32_t date;
    uint32_t mouth;
    uint32_t year;
} zda_packt;

// user settings of the parser
#define MAX_VALUE_SIZE      10
#define MAX_PACKAGE_SIZE    100

#endif
