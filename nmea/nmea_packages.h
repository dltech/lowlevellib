#ifndef H_NMEA_PACKAGES
#define H_NMEA_PACKAGES
/*
 * NMEA parser C library. Packages format for parser.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Global Positioning System Fixed Data */
#define HEAD_SIZE   6

struct{
    uint8_t header[HEAD_SIZE];
    uint8_t div0;
    uint8_t hh[2];
    uint8_t mm[2];
    uint8_t ss[2];
    uint8_t div1;
    uint8_t sss[2];
    uint8_t div2;
    uint8_t lattitude_d[4];
    uint8_t div3;
    uint8_t lattitude_m[5];
    uint8_t div4;
    uint8_t ns;
    uint8_t div5;
    uint8_t longitude_d[5];
    uint8_t div6;
    uint8_t longitude_m[5];
    uint8_t div7;
    uint8_t ew;
    uint8_t div8;
    uint8_t fix;
    uint8_t div9;
} gga_stream1;

struct {
    uint8_t station_id;
    uint8_t div0;
    uint8_t crc[2];
} gga_stream2;

struct{
    uint8_t header[HEAD_SIZE];
    uint8_t div0;
    uint8_t lattitude_d[4];
    uint8_t div1;
    uint8_t lattitude_m[5];
    uint8_t div2;
    uint8_t ns;
    uint8_t div3;
    uint8_t longitude_d[5];
    uint8_t div4;
    uint8_t longitude_m[5];
    uint8_t div5;
    uint8_t ew;
    uint8_t div6;
    uint8_t utc_hh[2];
    uint8_t utc_mm[2];
    uint8_t utc_ss[2];
    uint8_t div7;
    uint8_t utc_sss[2];
    uint8_t div8;
    uint8_t status;
    uint8_t div9;
    uint8_t pos_mode;
    uint8_t crc[2];
} gll_stream;

struct{
    uint8_t header[HEAD_SIZE];
    uint8_t div0;
    uint8_t mode1;
    uint8_t div1;
    uint8_t mode2;
    uint8_t div2;
} gsa_stream1;

struct{
    uint8_t pdop[3];
    uint8_t div0;
    uint8_t hdop[3];
    uint8_t div1;
    uint8_t vdop[3];
    uint8_t div2;
} gsa_stream2;

struct{
    uint8_t header[HEAD_SIZE];
    uint8_t div0;
    uint8_t messages;
    uint8_t div1;
    uint8_t msg_number;
    uint8_t div2;
} gsv_stream1;

struct {
    uint8_t id[2];
    uint8_t div0;
    uint8_t elevation[2];
    uint8_t div1;
    uint8_t azimuth[3];
    uint8_t div2;
    uint8_t snr[2];
    uint8_t div3;
} gsv_stream2;

struct{
    uint8_t header[HEAD_SIZE];
    uint8_t div0;
    uint8_t utc_hh[2];
    uint8_t utc_mm[2];
    uint8_t utc_ss[2];
    uint8_t div1;
    uint8_t utc_sss[2];
    uint8_t div2;
    uint8_t lattitude_d[4];
    uint8_t div3;
    uint8_t lattitude_m[5];
    uint8_t div4;
    uint8_t ns;
    uint8_t div5;
    uint8_t longitude_d[5];
    uint8_t div6;
    uint8_t longitude_m[5];
    uint8_t div7;
    uint8_t ew;
    uint8_t div8;
} rmc_stream;

struct{
    uint8_t header[HEAD_SIZE];
    uint8_t div0;
    uint8_t hh[2];
    uint8_t mm[2];
    uint8_t ss[2];
    uint8_t div1;
    uint8_t sss[2];
    uint8_t div2;
    uint8_t day[2];
    uint8_t div3;
    uint8_t mounth[2];
    uint8_t div4;
    uint8_t year[4];
    uint8_t div5;
    uint8_t local_hour[2];
    uint8_t div6;
    uint8_t local_minute[2];
    uint8_t div7;
    uint8_t crc[2];
} zda_stream;

#endif
