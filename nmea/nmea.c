/*
 * NMEA parser fixed-point C library.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <string.h>
#include "nmea_headers.h"

static uint32_t paramCnt = 0;
static uint32_t param[MAX_VALUE_SIZE];

static gbs_packt gbs, gbs_temp;

uint32_t isHeader(uint8_t data)
{
    static uint8_t head[HEADER_SIZE];
    static uint32_t i;
    static int32_t header = NO_HEADER;
    // catch the frst symbol of the package
    if(data == START_CHARACTER) {
        i=0;
        head[i] = data;
        header = START_CHAR;
        paramCnt = 0;
        return header;
    }
    // if catched, fill the remainder of the header
    if((i < HEADER_SIZE) && (header == START_CHAR)) {
        head[i++] = data;
    }
    // check, if header is filled
    if((i < HEADER_SIZE) && (header == START_CHAR)) {
        return header;
    }
    // determine the kind of obtained header
    header = NO_HEADER;
    if(memcmp(head+3, dtm_header+3, HEADER_SIZE) == 0) {
        header = DTM_HEAD;
    } else if(memcmp(head+3, gbs_header+3, HEADER_SIZE) == 0) {
        header = GBS_HEAD;
    } else if(memcmp(head+3, gga_header+3, HEADER_SIZE) == 0) {
        header = GGA_HEAD;
    } else if(memcmp(head+3, gll_header+3, HEADER_SIZE) == 0) {
        header = GLL_HEAD;
    } else if(memcmp(head+3, grs_header+3, HEADER_SIZE) == 0) {
        header = GRS_HEAD;
    } else if(memcmp(head+3, gsa_header+3, HEADER_SIZE) == 0) {
        header = GSA_HEAD;
    } else if(memcmp(head+3, gsv_header+3, HEADER_SIZE) == 0) {
        header = GSV_HEAD;
    } else if(memcmp(head+3, mss_header+3, HEADER_SIZE) == 0) {
        header = MSS_HEAD;
    } else if(memcmp(head+3, rmc_header+3, HEADER_SIZE) == 0) {
        header = RMC_HEAD;
    } else if(memcmp(head+3, vtg_header+3, HEADER_SIZE) == 0) {
        header = VTG_HEAD;
    } else if(memcmp(head+3, zda_header+3, HEADER_SIZE) == 0) {
        header = ZDA_HEAD;
    }
    return header;
}

uint32_t parseParam(uint8_t data, uint8_t *param)
{
    static int i = 0;
    static int32_t isFirstDivider = 0;
    // init the function variables
    if((i >= MAX_VALUE_SIZE) || (paramCnt == 0)) {
        isFirstDivider = 0;
        i=0;
    }
    // start to catch data from data divider
    if( (data == DATA_DIVIDER) && (isFirstDivider != 1) ) {
        isFirstDivider = 1;
        return 0;
    }
    // if value catched, return size
    if( (data == DATA_DIVIDER) && (isFirstDivider == 1) ) {
        int32_t tmp = i;
        i=0;
        ++paramCnt;
        return tmp;
    }
    // fill the parameter's array
    if((i<MAX_VALUE_SIZE) && (isFirstDivider == 1)) {
        param[i++] = data;
    }
    return 0;
}

uint8_t ctoh(uint8_t hex)
{
    switch(hex) {
        case '0':
            return 0x0;
        case '1':
            return 0x1;
        case '2':
            return 0x2;
        case '3':
            return 0x3;
        case '4':
            return 0x4;
        case '5':
            return 0x5;
        case '6':
            return 0x6;
        case '7':
            return 0x7;
        case '8':
            return 0x8;
        case '9':
            return 0x9;
        case 'A':
            return 0xa;
        case 'B':
            return 0xb;
        case 'C':
            return 0xc;
        case 'D':
            return 0xd;
        case 'E':
            return 0xe;
        case 'F':
            return 0xf;
    }
}

uint32_t parseCrc(uint8_t data, uint8_t *crc)
{
    static uint8_t crc;
    static uint32_t crcCnt = 0;
    if(data == END_CHARACTER) {
        crc = 0;
        crcCnt = 0;
        return END_CHAR;
    }
    if(crcCnt == 0) {
        crc = ctoh(data);
    }
    if(crcCnt == 1) {
        crc |= ctoh(data)<<4;
    }
    ++crcCnt;
    return CRC_OBTAINED;
}

uint32_t checkCrc(uint8_t *pack, uint8_t size, uint8_t crc) {
    uint8_t calcCrc = 0;
    for( uint32_t i=0 ; i<size ; ++i ) {
        calcCrc ^= pack[i];
    }
    if(calcCrc == crc) {
        return CRC_OK;
    }
    return CRC_FAILED;
}

void finalize(uint32_t packageType)
{
    switch(packageType)
    {
        case GBS_HEAD:
            gbs = gbs_temp;

    }
}

void nmeaParser(uint8_t data)
{
    static uint32_t packageType = NO_HEADER;
    static uint32_t obtainedPackageType = NO_HEADER;
    static uint32_t packCnt = 0;
    static uint8_t package[MAX_PACKAGE_SIZE];
    static uint8_t crc;
    // start character starts the packet parsing
    if(data == START_CHARACTER) {
        packageType = isHeader(data);
        packCnt = 0;
        return;
    }
    // let's find the header first
    if(packageType == START_CHAR) {
        packageType = isHeader(data);
    }
    // always ready to finalize due to collecting data
    // read the crc
    if((data == END_CHARACTER) || (packageType == END_CHAR)) {
        obtainedPackageType = packageType;
        packageType = parseCrc(data, &crc);
    }
    // check the crc
    if(packageType == CRC_OBTAINED) {
        packageType = checkCrc(package, packCnt, crc);
    }
    // copy the packet if data is valid
    if(packageType == CRC_OK) {
        finalize(obtainedPackageType);
        return;
    }
    // save the packet for crc check
    if( (packageType >= START_CHAR) && (packCnt < MAX_PACKAGE_SIZE) ) {
        package[packCnt++] = data;
    }
    // collecting data
    switch(packageType)
    {
        case GBS_HEAD:
            parseGbs(data);
            return;
        case

    }
}

void parseGbs(uint8_t data)
{
    static uint8_t param[MAX_VALUE_SIZE];
    switch(paramCnt) {
        case 0:
            parseParam(data, param);
        case 1:
            hour = atoi(param);
            parseParam(data, param);
        case 2:
            minutes = atoi(param);
            parseParam(data, param);
        case 3:
            seconds = atoi(param);
            parseParam(data, param);
        case 4:
            milliseconds = atoi(param);
            parseParam(data, param);
        case 5:
            latitude = atoi(param);
            parseParam(data, param);
        case 4:
            latitude_d = atoi(param);
            parseParam(data, param);
        case 4:
            milliseconds = atoi(param);
            parseParam(data, param);
        case 4:
            milliseconds = atoi(param);
            parseParam(data, param);
        case 4:
            milliseconds = atoi(param);
            parseParam(data, param);
        case 4:
            milliseconds = atoi(param);
            parseParam(data, param);
        case 4:
            milliseconds = atoi(param);
            parseParam(data, param);
        case 4:
            milliseconds = atoi(param);
            parseParam(data, param);
        case 4:
            milliseconds = atoi(param);
            parseParam(data, param);
        case 4:
            milliseconds = atoi(param);
            parseParam(data, param);
        case 4:
            milliseconds = atoi(param);
            parseParam(data, param);
    }

}
