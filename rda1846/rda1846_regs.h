#ifndef H_RDA1846_REGS
#define H_RDA1846_REGS
/*
 * Kitaenwood walkie-talkie IC RDA1846 library, register definitions
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// write register addr greater then 80h
#define WRITE80_1_1   0x7f
#define WRITE80_1_2   0x00
#define WRITE80_1_3   0x01
#define WRITE80_2_1   0x80
#define WRITE80_3_1   0x7f
#define WRITE80_3_2   0x00
#define WRITE80_3_3   0x00

/*** registers **/
#define REG04H  0x04
// clk_mode
#define CLK_MODE12_14MHZ    0x0001
#define CLK_MODE24_28MHZ    0x0000
#define REG0AH  0x0a
// See TX Pa_bias output voltage
#define PABIAS_VOLTAGE1P01  0x00
#define PABIAS_VOLTAGE1P05  0x01
#define PABIAS_VOLTAGE1P09  0x02
#define PABIAS_VOLTAGE1P18  0x04
#define PABIAS_VOLTAGE1P34  0x08
#define PABIAS_VOLTAGE1P68  0x10
#define PABIAS_VOLTAGE2P45  0x20
#define PABIAS_VOLTAGE3P13  0x3f
#define REG0FH  0x0f
// setting RF band
#define BAND_SELECT_400_520MHZ 0x00
#define BAND_SELECT_200_260MHZ 0x10
#define BAND_SELECT_134_174MHZ 0x11
#define REG1FH  0x1f
// gpio7
#define GPIO7HIZ    0x0000
#define GPIO7VOX    0x4000
#define GPIO7LOW    0x8000
#define GPIO7HI     0xc000
// gpio6
#define GPIO6HIZ    0x0000
#define GPIO6VOX    0x1000
#define GPIO6LOW    0x2000
#define GPIO6HI     0x3000
// gpio5
#define GPIO5HIZ    0x0000
#define GPIO5VOX    0x0400
#define GPIO5LOW    0x0800
#define GPIO5HI     0x0c00
// gpio4
#define GPIO4HIZ    0x0000
#define GPIO4VOX    0x0100
#define GPIO4LOW    0x0200
#define GPIO4HI     0x0300
// gpio3
#define GPIO3HIZ    0x0000
#define GPIO3VOX    0x0040
#define GPIO3LOW    0x0080
#define GPIO3HI     0x00c0
// gpio2
#define GPIO2HIZ    0x0000
#define GPIO2VOX    0x0010
#define GPIO2LOW    0x0020
#define GPIO2HI     0x0030
// gpio1
#define GPIO1HIZ    0x0000
#define GPIO1VOX    0x0004
#define GPIO1LOW    0x0008
#define GPIO1HI     0x000c
// gpio0
#define GPIO0HIZ    0x0000
#define GPIO0VOX    0x0001
#define GPIO0LOW    0x0002
#define GPIO0HI     0x0003
#define REG29H  0x29
// Freq high value (unit 1khz/8)
#define FREQ_REG_HI(f) (((f/125)<<16)&0xffff)
#define REG2AH  0x2a
#define FREQ_REG_LOW(f) ((f/125)&0xffff)
#define REG2BH  0x2b
// Crystal clk freq (unit khz)
#define XTAL_FREQ12_14(khz) (khz&0xffff)
#define XTAL_FREQ24_28(khz) ((khz/2)&0xffff)
#define REG2CH  0x2c
// Adc clk freq (unit khz)
#define ADCLK_FREQ12_14(khz) (khz&0xffff)
#define ADCLK_FREQ24_28(khz) ((khz/2)&0xffff)
#define REG2DH  0x2d
// interrupts
#define INT_GRP_CSS_XMP_INT_EN          0x0200
#define INT_GRP_RXON_RF_INT_EN          0x0100
#define INT_GRP_TXON_RF_INT_EN          0x0080
#define INT_GRP_DTMF_IDLE_INT_EN        0x0040
#define INT_GRP_CTSS_PH_SFT_DET_INT_EN  0x0020
#define INT_GRP_IDLE_TOUT_INT_EN        0x0010
#define INT_GRP_RXON_RF_TOUT_INT_EN     0x0008
#define INT_GRP_SQ_INT_EN               0x0004
#define INT_GRP_TXON_RF_TOUT_INT_EN     0x0002
#define INT_GRP_VOX_INT_EN              0x0001
#define REG30H  0x30
// channel_mode
#define CHANNEL_MODE25KHZ           0x3000
#define CHANNEL_MODE12P5KHZ         0x0000
// tail elim enable
#define TAIL_ELIM_EN                0x0800
// st_mode
#define ST_MODE_AUTO                0x0200
#define ST_MODE_RXON_AUTO_TXON_MAN  0x0100
#define ST_MODE_MANUAL              0x0000
// mute when rxno
#define MUTE                        0x0080
// tx_on
#define TX_ON                       0x0040
// rx_on
#define RX_ON                       0x0020
// on, then chip auto vox
#define VOX_ON                      0x0010
// on, then chip auto sq
#define SQ_ON                       0x0008
// The same as pdn pin
#define PDN_REG                     0x0004
// calibration enable
#define CHIP_CAL_EN                 0x0002
// reset, then all the registers are reset to default value
#define SOFT_RESET                  0x0001
#define REG35H  0x35
// interval_v_reg
#define TONE1_FREQ(hz)  (((uint16_t)(hz/244))&0xffff)
#define REG36H  0x36
// interval_c_reg
#define TONE2_FREQ(hz)  (((uint16_t)(hz/244))&0xffff)
#define REG3C   0x3c
// TX voice channel
#define VOICE_SEL_TX_MIC        0x0000
#define VOICE_SEL_TX_SINE       0x4000
#define VOICE_SEL_TX_GPIO       0x8000
#define VOICE_SEL_TX_NO_SIGNAL  0xc000
#define REG41H  0x41
// Vox open threshold
#define TH_H_VOX(f) ((uint16_t)(f/4.4))
#define REG42H  0x42
#define TH_L_VOX(f) ((uint16_t)(f/4.4))
#define REG43H  0x43
// Ctcss/cdcss + voice dev setting
#define XMITTER_DEV_MSK 0x1fc0
#define XMITTER_DEV_SFT 6
// Ctcss/cdcss dev setting
#define C_DEV_MSK       0x003f
#define REG44H  0x44
// volume1
#define VOLUME1(db) ((db<<4)&0xf0)
// volume2
#define VOLUME2(db) (db&0xf0)
#define REG45H  0x45
// Select ctcss phase shift when use tail eliminating function when TX
#define SHIFT_SELECT120DEG      0x0000
#define SHIFT_SELECT180DEG      0x4000
#define SHIFT_SELECT240DEG      0x8000
// If 1, cdcss code will be detected.
#define POS_DET_EN              0x0800
// If 1, sq detection will add ctcss/cdcss detect result.
#define CSS_DET_EN              0x0400
// If 1,cdcss inverse code will be detected at the same time.
#define NEG_DET_EN              0x0080
// 24/23 bit cdcss code sel for both txon and rxon
#define CDCSS_SEL               0x0010
// ctcss_cmp/cdcss_cmp out via gpio
#define CTCSS_SEL               0x0008
// Ctcss/cdcss mode select
#define C_MODE_DISABLE          0x0000
#define C_MODE_INNER_CTCSS_EN   0x0001
#define C_MODE_INNER_CDCSS_EN   0x0002
#define C_MODE_OUTER_CTCSS_EN   0x0005
#define C_MODE_OUTER_CDCSS_EN   0x0006
#define REG48H  0x48
// Sq open threshold
#define TH_H_SQ(g)  (((uint16_t)(g/1.25))&0x3ff)
#define REG49H  0x49
// Sq shut threshold
#define TH_L_SQ(g)  (((uint16_t)(g/1.25))&0x3ff)
#define REG4AH  0x4a
// Ctcss/cdcss frequency setting
#define CTCSS_FREQ(hz)  (((uint16_t)(hz/15))&0xffff)
#define REG4BH  0x4b
// Cdcss send/receive bit
#define CDSS_CODE_HI(code)  ((code>>16)&0x00ff)
#define REG4CH  0x4c
// Cdcss send/receive bit
#define CDSS_CODE_LO(code)  (code&0xffff)
#define REG54H  0x54
// If 1, the output gpio6 is sq & css_cmp; Else, the outputp gpio is sq only.
#define SQ_OUT_SEL  0x0080
#define REG58H  0x58
// 1=pre/de-emph bypass
#define PRE_DEEMPH  0x0008
#define REG5CH  0x5c
// Dtmf idle
#define DTMF_IDLE   0x1000
// If 1, rxon is enable
#define RXON_RF     0x0400
// If 1, txon isenable
#define TXON_RF     0x0200
// Ctcss phase shift detected
#define INVERT_DET  0x0080
// Ctcss/cdcss compared
#define CSS_CMP     0x0004
// Sq final signal out from dsp
#define SQ          0x0002
// Vox out from dsp
#define VOX         0x0001
#define REG5FH  0x5f
// Received signal strength indication, unit 1/8dB
#define RSSI(reg)   ((int32_t)(((reg&0x3ff)*0.125))-135)
#define REG60H  0x60
// Voice signal strength indication, unit mV
#define VSSI_MSK    0x7fff
#define REG63H  0x63
// Dtmf mode
#define DTMF_SINGLE_TONE2   0x0300
#define DTMF_DUAL_TONE1_2   0x0100
// Time interval for dual tone transmission
#define DTMF_TIME1(ms)  ((((uint8_t)(ms/5))&0xf)<<4)
// Time interval for dtmf idle state
#define DTMF_TIME2(ms)  (((uint8_t)(ms/5))&0xf)
#define REG66H  0x66
// dtmf_c0
#define DTMF_CO_12P8    0x61
#define DTMF_CO_13      0x61
// dtmf_c1
#define DTMF_C1_12P8    0x5b
#define DTMF_C1_13      0x5e
#define REG67H  0x67
// dtmf_c0
#define DTMF_C2_12P8    0x53
#define DTMF_C2_13      0x57
// dtmf_c1
#define DTMF_C3_12P8    0x4b
#define DTMF_C3_13      0x4b
#define REG68H  0x68
// dtmf_c0
#define DTMF_C4_12P8    0x2c
#define DTMF_C4_13      0x31
// dtmf_c1
#define DTMF_C5_12P8    0x1e
#define DTMF_C5_13      0x1e
#define REG69H  0x69
// dtmf_c0
#define DTMF_C6_12P8    0x0a
#define DTMF_C6_13      0x0f
// dtmf_c1
#define DTMF_C7_12P8    0xf6
#define DTMF_C7_13      0xfb
#define REG6CH  0x6c
// tone1 detect index
#define TONE1_DTMF_INDEX_MSK    0x0700
#define TONE1_DTMF_INDEX_SFT    8
// tone2 detect index
#define TONE2_DTMF_INDEX_MSK    0x00e0
#define TONE2_DTMF_INDEX_SFT    5
// Dtmf code out
#define DTMF_CODE_F0_F4 0x01
#define DTMF_CODE_F0_F5 0x02
#define DTMF_CODE_F0_F6 0x03
#define DTMF_CODE_F0_F7 0x0a
#define DTMF_CODE_F1_F4 0x04
#define DTMF_CODE_F1_F5 0x05
#define DTMF_CODE_F1_F6 0x06
#define DTMF_CODE_F1_F7 0x0b
#define DTMF_CODE_F2_F4 0x07
#define DTMF_CODE_F2_F5 0x08
#define DTMF_CODE_F2_F6 0x09
#define DTMF_CODE_F2_F7 0x0c
#define DTMF_CODE_F3_F4 0x0e
#define DTMF_CODE_F3_F5 0x00
#define DTMF_CODE_F3_F6 0x0f
#define DTMF_CODE_F3_F7 0x0d

#endif
