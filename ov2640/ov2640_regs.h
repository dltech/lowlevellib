#ifndef H_BME280_REGS
#define H_BME280_REGS
/*
 * OV2640 Color CMOS UXGA (2.0 MegaPixel) CAMERACHIP. Register definitions.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define R_BYPASS    0x05 /* Bypass DSP */
// Bypass DSP select. 1: Bypass DSP, sensor out directly
#define BYP_DSP_SEL 0x1

#define QS          0x44 /* Quantization Scale Factor */

#define CTRLL       0x50 /*  */
// LP_DP
#define LP_DP   0x80
// Round
#define ROUND   0x40
// V_DIVIDER
#define
// H_DIVIDER
#define

#define HSIZE       0x51 /* H_SIZE[7:0] (real/4) */
#define VSIZE       0x52 /* V_SIZE[7:0] (real/4) */

#define XOFFL       0x53 /* OFFSET_X[7:0] */
#define YOFFL       0x54 /* OFFSET_Y[7:0] */

#define VHYX        0x55 /*  */
// V_SIZE[8]
#define VSIZE8B 0x80
// OFFSET_Y[10:8]
#define
// H_SIZE[8]
#define HSIZE8B 0x08
// OFFSET_X[10:8]
#define

#define DPRP        0x56 /*  */
// DP_SELY
#define
// DP_SELX
#define

#define TEST        0x57 /* Bit[7]: H_SIZE[9] */
// H_SIZE[9]
#define HSIZE9  0x80

#define ZMOW        0x5a /* OUTW[7:0] (real/4) */
#define ZMOH        0x5b /* OUTH[7:0] (real/4) */

#define ZMHH        0x5c /*  */
// ZMSPD (zoom speed)
#define
// OUTH[8]
#define OUTH8   0x04
// OUTW[9:8]
#define

#define BPADDR      0x7c /* SDE Indirect Register Access: Address */
#define BPADDR_MSK  0xf

#define BPDATA      0x7d /* SDE Indirect Register Access: Data */

#define CTRL2       0x86 /* Module Enable */
#define DCW     0x20
#define SDE     0x10
#define UV_ADJ  0x08
#define UV_AVG  0x04
#define CMX     0x01

#define CTRL3       0x87 /* Module Enable */
#define BPC 0x80
#define WPC 0x40

#define HSIZE8      0xc0 /* Image Horizontal Size HSIZE[10:3] */
#define VSIZE8      0xc1 /* Image Vertical Size VSIZE[10:3] */
#define SIZEL       0x8c /* {HSIZE[11], HSIZE[2:0], VSIZE[2:0]} */
// HSIZE[11]
#define HSIZE11         0x40
// HSIZE[2:0]
#define HSIZE2_0_MSK    0x38
#define HSIZE2_0_SFK    3
#define HSIZE2_0_SET(n) ((n<<HSIZE2_0_SFK)&HSIZE2_0_MSK)
// VSIZE[2:0]
#define VSIZE2_0_MSK    0x07
#define VSIZE2_0_SET(n) (n&VSIZE2_0_MSK)

#define CTRL0       0xc2 /* Module Enable */
#define AEC_EN      0x80
#define AEC_SEL     0x40
#define STAT_SEL    0x20
#define VFIRST      0x10
#define YUV422      0x08
#define YUV_EN      0x04
#define RGB_EN      0x02
#define RAW_EN      0x01

#define CTRL1       0xc3 /* Module Enable */
#define CIP         0x80
#define DMY         0x40
#define RAW_GMA     0x20
#define DG          0x10
#define AWB         0x08
#define AWB_GAIN    0x04
#define LENC        0x02
#define PRE         0x01

#define R_DVP_SP    0xd3 /*  */
// Auto mode
#define AUTO_MODE   0x80
// DVP PCLK = sysclk (48)/[6:0] (YUV0); = sysclk (48)/(2*[6:0]) (RAW)
#define

#define IMAGE_MODE  0xda /* Image Output Format Select */
// Y8 enable for DVP
#define Y8DVP           0x40
// JPEG output enable
#define JPEG_OUT        0x10
// DVP output format
#define DVPOUT_YUV422   0x00
#define DVPOUT_RAW10    0x04
#define DVPOUT_RGB565   0x08
// HREF timing select in DVP JPEG output mode, 1: HREF = VSYNC
#define DVPJPEG_TIMING  0x02
// Byte swap enable for DVP, 1: Low byte first UYVY (C2[4] =0), VYUY (C2[4] =1)
#define DVPSWAP         0x01

#define RESET       0xe0 /* Reset */
#define MICROCONTROLLER 0x40
#define SCCB            0x20
#define JPEG            0x10
#define DVP             0x04
#define IPU             0x02
#define CIF             0x01

#define MS_SP       0xf0 /* SCCB Master Speed */

#define SS_ID       0xf7 /* SCCB Slave ID */

#define SS_CTRL     0xf8 /* SCCB Slave Control */
#define ADDR_AUTOINC    0x20
#define SCCBEN          0x08
#define SCCB_M_DELAY    0x04
#define SCCB_M_ACC      0x02
#define SENSOR_ACC      0x01

#define MC_BIST     0xf9 /* SCCB Slave Control */
#define MCURESET    0x80
#define BOOTROMSEL  0x40
#define ERR1_12K    0x20
#define ERR0_12K    0x10
#define ERR1_512K   0x08
#define ERR0_512K   0x04
#define BIST_READ   0x02
#define BIST_LAUNCH 0x01

#define MC_AL       0xfa /* Program Memory Pointer Address Low Byte */
#define MC_AH       0xfb /* Program Memory Pointer Address High Byte */

#define MC_D        0xfc /* Program Memory Pointer Access Address */
// Boundary of register address to separate DSP and sensor register

#define P_CMD       0xfd /* SCCB Protocol Command Register */

#define P_STATUS    0xfe /* SCCB Protocol Status Register */

#define RA_DLMT     0xff /* Register Bank Select */
#define DSP_SENSOR_ADDR 0x1

#define GAIN        0x00 /* AGC Gain Control LSBs */
// Gain = (Bit[7]+1) x (Bit[6]+1) x (Bit[5]+1) x (Bit[4]+1) x (1+Bit[3:0]/16)

#define COM1        0x03 /* Common Control 1 */
// Dummy frame control
#define DUMMY1  0x40
#define DUMMY3  0x80
#define DUMMY7  0xc0
// Vertical window end line control 2 LSBs (8 MSBs in VEND[7:0] (0x1A))
#define
// Vertical window start line control 2 LSBs (8 MSBs in VSTRT[7:0] (0x19))
#define

#define REG04       0x04 /* Register 04 */
// Horizontal mirror
#define HMIRROR     0x80
// Vertical flip
#define VFLIP       0x40
// VREF bit[0]
#define VREF0       0x10
// HREF bit[0]
#define HREF0       0x08
// AEC[1:0] (AEC[15:10] is in register REG45[5:0] (0x45), AEC[9:2] is in register AEC[7:0] (0x10))
#define AEC1_0_MSK  0x03

#define REG08       0x08 /* Frame Exposure One-pin Control Pre-charge Row Number */

#define COM2        0x09 /* Common Control 2 */
// Standby mode enable, 1: Standby mode
#define STBY        0x10
// Pin PWDN/RESETB used as SLVS/SLHS
#define PWDNASSLVS  0x04
// Output drive select
#define OUTDRV1XCAP 0x00
#define OUTDRV3XCAP 0x01
#define OUTDRV2XCAP 0x02
#define OUTDRV4XCAP 0x03

#define PIDH        0x0a /* Product ID Number MSB (Read only) */
#define PIDL        0x0b /* Product ID Number LSB (Read only) */

#define COM3        0x0c /* Common Control 3 */
// Set banding manually, 0: 60 Hz, 1: 50 Hz
#define BANDING50HZ 0x4
// Auto set banding
#define AUTOSETBAND 0x2
// Snapshot option, 1: Output single frame only
#define SINGLEFRAME 0x1

#define COM4        0x0d /* Common Control 4 */
// Clock output power-down pin status, 1: Data output pin hold at last state before power-down
#define PDWNPIN_CLKOUT  0x4

#define AEC         0x10 /* Automatic Exposure Control 8 bits for AEC[9:2] */
// TEX = tLINE x AEC[15:0]

#define CLKRC       0x11 /* Clock Rate Control */
// Internal frequency doublers ON/OFF selection, 1: ON
#define FREQDOUBLE  0x80
// Clock divider, CLK = XVCLK/(decimal value of CLKRC[5:0] + 1)
#define

#define COM7        0x12 /* Common Control 7 */
// 1: Initiates system reset.
#define SRST        0x80
// Resolution selection
#define RESOL_UXGA  0x00
#define RESOL_CIF   0x10
#define RESOL_SVGA  0x40
// Zoom mode
#define ZOOMMODE    0x04
// Color bar test pattern
#define TESTPTRN    0x02

#define COM8        0x13 /* Common Control 8 */
// Banding filter selection, 1: ON, set minimum exposure time to 1/120s
#define BANDFILTER      0x20
// AGC auto/manual control selection, 1: Auto
#define AGCAUTO         0x04
// Exposure control, 1: Auto
#define EXPOSUREAUTO    0x01

#define COM9        0x14 /* Common Control 9 */
// AGC gain ceiling, GH[2:0]
#define AGC_GAIN2X      0x00
#define AGC_GAIN4X      0x20
#define AGC_GAIN8X      0x40
#define AGC_GAIN16X     0x60
#define AGC_GAIN32X     0x80
#define AGC_GAIN64X     0xa0
#define AGC_GAIN128X    0xc0

#define COM10       0x15 /* Common Control 10 (if Bypass DSP is selected) */
// CHSYNC pin output swap, 0: CHSYNC, 1: HREF
#define CHSYNCSWAP_HREF 0x80
// HREF pin output swap, 0: HREF, 1: CHSYNC
#define HREFSWAP_CHSYNC 0x40
// PCLK output selection, 0: PCLK always output, 1: PCLK output qualified by HREF
#define PCLKHREF        0x20
// PCLK edge selection, 1: Data is updated at the rising edge of PCLK
#define PCLKPOL         0x10
// HREF output polarity, 1: Output negative HREF, HREF negative for data valid
#define HREFPOL         0x08
// VSYNC polarity, 0: Positive, 1: Negative
#define VSYNCPOL        0x02
// HSYNC polarity, 0: Positive, 1: Negative
#define HSYNCPOL        0x01

#define HREFST      0x17 /* Horizontal Window Start MSB 8 bits (3 LSBs in REG32[2:0] (0x32)) */
// Bit[10:0]: Selects the start of the horizontal window, each LSB represents two pixels
#define HREFEND     0x18 /* Horizontal Window End MSB 8 bits (3 LSBs in REG32[5:3] (0x32)) */
// Bit[10:0]: Selects the end of the horizontal window, each LSB represents two pixels

#define VSTRT       0x19 /* Vertical Window Line Start MSB 8 bits (2 LSBs in COM1[1:0] (0x03)) */
// Bit[9:0]: Selects the start of the vertical window, each LSB represents two scan lines.
#define VEND        0x1a /* Vertical Window Line End MSB 8 bits (2 LSBs in COM1[3:2] (0x03)) */
// Bit[9:0]: Selects the end of the vertical window, each LSB represents two scan lines.
#define

#define MIDH        0x1c /* Manufacturer ID Byte – High */
#define MIDH_DEF    0x7f
#define MIDL        0x1d /* Manufacturer ID Byte – Low */
#define MIDL_DEF    0xa2

#define AEW         0x24 /* Luminance Signal High Range for AEC/AGC Operation */
// AEC/AGC values will decrease in auto mode when average luminance is greater than AEW[7:0]
#define AEB         0x25 /* Luminance Signal Low Range for AEC/AGC Operation */
// AEC/AGC values will increase in auto mode when average luminance is less than AEB[7:0]

#define VV          0x26 /* Fast Mode Large Step Range Threshold - effective only in AEC/AGC fast mode */
// Bit[7:4]: High threshold
#define

#define REG2A       0x2a /* Register 2A */
// Line interval adjust value 4 MSBs (LSBs in FRARL[7:0] (0x2B))
#define
// HSYNC timing end point adjustment MSB 2 bits (LSBs in register HEDY[7:0] (0x31))
#define
// HSYNC timing start point adjustment MSB 2 bits (LSBs in register HSDY[7:0] (0x30))
#define

#define FRARL       0x2b /* Line Interval Adjustment Value LSB 8 bits (MSBs in REG2A[7:4] */
// The frame rate will be adjusted by changing the line interval.

#define ADDVSL      0x2d /* VSYNC Pulse Width LSB 8 bits */
// Line periods added to VSYNC width. Default VSYNC output width

#define ADDVSH      0x2e /* VSYNC Pulse Width MSB 8 bits */
// Line periods added to VSYNC width. Default VSYNC output width

#define YAVG        0x2f /* Luminance Average (this register will auto update) */
// B/Gb/Gr/R channel average = (BAVG[7:0] + (2 x GbAVG[7:0]) + RAVG[7:0]) x 0.25

#define HSDY        0x30 /* HSYNC Position and Width, Start Point LSB 8 bits */
// This register and REG2A[1:0] (0x2A) define HSYNC start position,

#define HEDY        0x31 /* HSYNC Position and Width, End Point LSB 8 bits */

#define REG32       0x32 /* Common Control 32 */
// Pixel clock divide option
#define PCLK2   0x80
#define PCLK4   0xc0
// Horizontal window end position 3 LSBs (8 MSBs in register HREFEND[7:0] (0x18))
#define
// Horizontal window start position 3 LSBs (8 MSBs in register HREFST[7:0] (0x17))
#define

#define ARCOM2      0x34 /* Zoom window horizontal start point */
// Zoom window horizontal start point
#define ZOOM_WINDOW_HOR_PT  0x4

#define REG45       0x45 /* Register 45 */
// AGC[9:8], AGC highest gain control
#define
// AEC[15:10], AEC MSBs
#define

#define FLL         0x46 /* Frame Length Adjustment LSBs */
// Each bit will add 1 horizontal line timing in frame
#define FLH         0x47 /* Frame Length Adjustment MSBs */
// Each bit will add 256 horizontal lines timing in frame

#define ZOOMS       0x49 /* Zoom Mode Vertical Window Start Point 8 MSBs */

#define COM19       0x48 /* Common Control 19 */

#define COM22       0x4a /* Bit[7:0]: Flash light control */

#define COM25       0x4b /* Common Control 25 - reserved for banding */
// 50Hz Banding AEC 2 MSBs
#define
// 60HZ Banding AEC 2 MSBs
#define

#define BD50        0x4f /* 50Hz Banding AEC 8 LSBs */

#define BD60        0x50 /* 60Hz Banding AEC 8 LSBs */

#define REG5D       0x5d /* AVGsel[7:0], 16-zone average weight option */
#define REG5E       0x5e /* AVGsel[15:8], 16-zone average weight option */
#define REG5F       0x5f /* AVGsel[23:16], 16-zone average weight option */
#define REG60       0x60 /* AVGsel[31:24], 16-zone average weight option */

#define HISTO_LOW   0x61 /* Histogram Algorithm Low Level */
#define HISTO_HIGH  0x62 /* Histogram Algorithm High Level */

#endif
