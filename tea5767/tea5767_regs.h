#ifndef H_TEA5767_REGS
#define H_TEA5767_REGS
/*
 * TEA5767 Low-power FM stereo radio for handheld applications.
 * Register definitions.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define TEA5767_ADDR    0xc0

/** write mode **/
/* Format of 1st data byte */
// if MUTE = 1 then L and R audio are muted;
#define MUTE        0x80
// Search mode: if SM = 1 then in search mode;
#define SM          0x40
// PLL[13:8] setting of synthesizer programmable counter for search or preset
#define PLL1_MSK    0x3f
#define PLL1_SFT    8

/* Format of 2nd data byte */
// PLL[7:0] setting of synthesizer programmable counter for search or preset
#define PLL_2MSK    0xff

/* Format of 3rd data byte */
// Search Up/Down: if SUD = 1 then search up; otherwise down
#define SUD         0x80
// Search stop level setting
#define SSL_LOW     0x20
#define SSL_MID     0x40
#define SSL_HIGH    0x60
// High/Low Side Injection: if HLSI = 1 then high side LO injection;
#define HLSI        0x10
// Mono to Stereo: if MS = 1 then forced mono; otherwise stereo
#define MS          0x08
// Mute Right: if MR = 1 then the right audio channel is muted and mono;
#define MR          0x04
// Mute Left: if ML = 1 then the left audio channel is muted and mono;
#define ML          0x02
// Software programmable port 1: if SWP1 = 1 then port 1 is HIGH;
#define SWP1        0x01

/* Format of 4th data byte */
// Software programmable port 2: if SWP2 = 1 then port 2 is HIGH;
#define SWP2    0x80
// Standby: if STBY = 1 then in Standby mode;
#define STBY    0x40
//Band Limits: if BL = 1 then Japanese FM band; if BL = 0 then US/Europe
#define BL      0x20
// Clock frequency
#define XTAL    0x10
// Soft Mute: if SMUTE = 1 then soft mute is ON;
#define SMUTE   0x08
// High Cut Control: if HCC = 1 then high cut control is ON;
#define HCC     0x04
// Stereo Noise Cancelling: if SNC = 1 then stereo noise cancelling is ON;
#define SNC     0x02
// Search Indicator if SI = 1 then pin SWPORT1 is output for the ready flag
#define SI      0x01

/* Format of 5th data byte */
// enable 6.5 MHz reference frequency for the PLL
#define PLLREF  0x80
// if DTC = 1 then the de-emphasis time constant is 75 μs, 50 μs otherwise
#define DTC     0x40


/** read mode **/
/* Format of 1st data byte */
// Ready Flag: if RF = 1 then a st has been found or the band lim reached
#define RF  0x
// Band Limit Flag: if BLF = 1 then the band limit has been reached;
#define BLF 0x
// PLL[13:8] setting of synthesizer programmable counter after search or preset

/* Format of 2nd data byte */
// PLL[7:0] setting of synthesizer programmable counter after search or preset

/* Format of 3rd data byte */
// Stereo indication: if STEREO = 1 then stereo reception; mono otherwise
#define STEREO      0x80
// IF[6:0] IF counter result
#define IF_MSK      0x7f
#define IF_GET(reg) (reg&IF_MSK)

/* Format of 4th data byte */
// level ADC output
#define LEV_MSK         0xf0
#define LEV_SFT         4
#define LEV_GET(reg)    ((reg>>LEV_SFT)&0xf)
// Chip Identification: these bits have to be set to logic 0
#define CI_MSK          0x0e
#define CI_SFT          1
#define CI_GET(reg)     ((reg>>CI_SFT)&0x7)

/* Format of 5th data byte */
// reserved for future extensions; these bits are internally set to logic 0

#endif
