#ifndef H_TDA8546_REGS
#define H_TDA8546_REGS
/*
 * TDA8546 I2C-bus controlled 4x45 W best efficiency amplifier.
 * Register definitions.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define TDA8546_ADDRESS 0xd8

#define IB1 0 /* Instruction byte IB1 */
// enable or disable clip detection below VP = 10 V
#define CLIP_DET        0x80
// channel 3 clip information on pin DIAG or pin STB
#define CH3STB          0x40
// channel 1 clip information on pin DIAG or pin STB
#define CH1STB          0x20
// channel 4 clip information on pin DIAG or pin STB
#define CH4STB          0x10
// channel 2 clip information on pin DIAG or pin STB
#define CH2STB          0x08
// enable or disable AC load detection
#define LOAD_DET        0x04
// enable or disable start-up diagnostics
#define STARTUP_DIAD_EN 0x02
// enable or disable amplifier start
#define START_AMP       0x01

#define IB2 1 /* Instruction byte IB2 */
// clip detection level
#define CLIP_DET2P          0x00
#define CLIP_DET5P          0x40
#define CLIP_DET10P         0x80
#define CLIP_DET_DISABLE    0xc0
// temperature information on pin DIAG
#define TEMP_DIAG           0x20
// load fault information (shorts) on pin DIAG
#define SHORTS_DIAG         0x10
// soft mute channel 1 and channel 3 (mute delay 15 ms)
#define MUTE13              0x04
// soft mute channel 2 and channel 4 (mute delay 15 ms)
#define MUTE24              0x02
// fast mute all amplifier channels
#define MUTE                0x01

#define IB3 2 /* Instruction byte IB3 */
// amplifier channel 1 and channel 3 gain select
#define GAIN13      0x40
// amplifier channel 2 and channel 4 gain select
#define GAIN24      0x20
// temperature pre-warning level
#define TWARN135    0x10
// enable or disable channel 3
#define CH3EN       0x08
// enable or disable channel 1
#define CH1EN       0x04
// enable or disable channel 4
#define CH4EN       0x02
// enable or disable channel 2
#define CH2EN       0x01

#define IB4 3 /* Instruction byte IB4 */
// use of SVR capacitor
#define SVRCAP          0x80
// soft or fast mute select during shut-down via pin STB
#define SLOW_SDN_MUTE   0x40
// 16 V overvoltage warning on pin DIAG
#define OVERVOLT_DIAG   0x20
// AC or DC load information on bits DBx[D5:D4]
#define ACDC_INFO       0x10
// line driver mode or low gain mode selection
#define LOW_GAIN        0x04
// AC load detection measurement current selection
#define AC_LOAD_HIGH    0x02
// low VP mute undervoltage level setting
#define LOW_MUTE_OVER   0x01

#define IB5 4 /* Instruction byte IB5 */
// best efficiency mode
#define BEST_EFFIENCY       0x80
// best efficiency mode channels
#define BEST_EFFIENCY2      0x40
// best efficiency switch level load impedance setting
#define BEST_EFFIENCY4OHM   0x10

#define DB1 0 /* Data byte DB1 */
// temperature pre-warning
#define OVERTEMP_PREWARN    0x80
// speaker fault channel 2
#define CH2SPK_FAULT        0x40
// channel 2 DC-load or AC-load detection
#define CH2LOAD_NORMAL      0x00
#define CH2LOAD_LINE        0x10
#define CH2LOAD_OPEN        0x20
#define CH2LOAD_NO          0x30
// channel 2 shorted load
#define CH2SHORT_LOAD       0x08
// channel 2 output offset
#define CH2OUT_OFFS         0x04
// channel 2 short to VP
#define CH2SHORT_VP         0x02
// channel 2 short to ground
#define CH2SHORT_GND        0x01

#define DB2 1 /* Data byte DB2 */
// POR and amplifier status
#define POR             0x80
// speaker fault channel 4
#define CH4SPK_FAULT    0x40
// channel 4 DC-load or AC-load detection
#define CH4LOAD_NORMAL  0x00
#define CH4LOAD_LINE    0x10
#define CH4LOAD_OPEN    0x20
#define CH4LOAD_NO      0x30
// channel 4 shorted load
#define CH4SHORT_LOAD   0x08
// channel 4 output offset
#define CH4OUT_OFFS     0x04
// channel 4 short to VP
#define CH4SHORT_VP     0x02
// channel 4 short to ground
#define CH4SHORT_GND    0x01

#define DB3 2 /* Data byte DB3 */
// maximum temperature protection
#define OVERTEMP_PROT   0x80
// speaker fault channel 1
#define CH1SPK_FAULT    0x40
// channel 1 DC-load or AC-load detection
#define CH1LOAD_NORMAL  0x00
#define CH1LOAD_LINE    0x10
#define CH1LOAD_OPEN    0x20
#define CH1LOAD_NO      0x30
// channel 1 shorted load
#define CH1SHORT_LOAD   0x08
// channel 1 output offset
#define CH1OUT_OFFS     0x04
// channel 1 short to VP
#define CH1SHORT_VP     0x02
// channel 1 short to ground
#define CH1SHORT_GND    0x01

#define DB4 3 /* Data byte DB4 */
// power supply 16 V overvoltage warning
#define OVERVOLTAGE16   0x80
// speaker fault channel 3
#define CH3SPK_FAULT    0x40
// channel 3 DC-load or AC-load detection
#define CH3LOAD_NORMAL  0x00
#define CH3LOAD_LINE    0x10
#define CH3LOAD_OPEN    0x20
#define CH3LOAD_NO      0x30
// channel 3 shorted load
#define CH3SHORT_LOAD   0x08
// channel 3 output offset
#define CH3OUT_OFFS     0x04
// channel 3 short to VP
#define CH3SHORT_VP     0x02
// channel 3 short to ground
#define CH3SHORT_GND    0x01

#define DB5 4 /* Data byte DB5 */
// power supply undervoltage
#define UNDERVOLTAGE            0x80
// power supply overvoltage
#define OVERVOLTAGE             0x40
// system status with start-up diagnostics or amplifier start-up
#define SYS_BYSY                0x20
// VP below/above 7.5 V
#define BELOV7V                 0x10
// VP below/above 10 V
#define BELOW10V                0x08
// undervoltage protection
#define UNDERVOLT_PROTECT_EV    0x04
// best efficiency protection
#define BEST_EFF_PROTECT_EV     0x02
// amplifier and output stage status
#define AMP_ON                  0x01

#endif
