#ifndef H_EEPROM
#define H_EEPROM
/*
 * STM8 basic support library. STM8 eeprom access functions.
 *
 * Copyright 2022 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdint.h>
#include "regs/flash_reg.h"

int8_t unlockData(void);
int8_t writeByte(uint8_t addr, uint8_t data);
uint8_t readByte(uint8_t addr);

#endif
