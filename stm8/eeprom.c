/*
 * STM8 basic support library. STM8 eeprom access functions.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "eeprom.h"
#include <stdint.h>

int8_t unlockData()
{
    FLASH_DUKR = FIRST_DATA_KEY;
    FLASH_DUKR = SECOND_DATA_KEY;
    uint8_t tOut = 0xff;
    while( ((FLASH_IAPSR & DUL) == 0) && ((--tOut) > 0) );
    if( tOut == 0 ) {
        return -1;
    }
    return 0;
}

int8_t writeByte(uint8_t addr, uint8_t data)
{
    MMIO8(addr + EEPROM_START) = data;
    if(FLASH_IAPSR & EOP) {
        return 0;
    }
    return -1;
}

uint8_t readByte(uint8_t addr)
{
    return MMIO8(addr + EEPROM_START);
}
