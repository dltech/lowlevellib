#include "mainmenu.h"

mainMenu::mainMenu(rda5807 *rda, QWidget *parent)
    : QWidget{parent}
{
    this->rda = rda;

    mute = new QCheckBox("беззвучн 1", this);
    softMute = new QCheckBox("беззвучн 2", this);
    mono = new QCheckBox("моно", this);
    bass = new QCheckBox("бас", this);
    vol = new QSlider(Qt::Horizontal, this);
    vol->setMaximum(0);
    vol->setMaximum(rda->volMax);

    band = new QComboBox(this);
    for(int i=0 ; i<rda->bandName->size() ; ++i) {
        band->insertItem(i, rda->bandName->at(i));
    }
    band->setCurrentIndex(rda->BAND_76_108);
    direct = new QCheckBox("частота ручн.", this);
    spacing = new QComboBox(this);
    for(int i=0 ; i<rda->spacingName->size() ; ++i) {
        spacing->insertItem(i, rda->spacingName->at(i));
    }
    spacing->setCurrentIndex(0);
    freq = new QDoubleSpinBox(this);
    freq->setDecimals(3);
    freq->setMinimum(rda->bandMin[band->currentIndex()]);
    if(direct->isChecked()) {
        freq->setMaximum(rda->bandMin[band->currentIndex()] + ((float)rda->freqMax)/1000.);
    } else {
        freq->setMaximum(rda->bandMin[band->currentIndex()] + (((float)(rda->spacings[spacing->currentIndex()] * rda->channelMax))/1000.));
        qDebug() << (((float)(rda->spacings[spacing->currentIndex()] * rda->channelMax))/1000.);
        freq->setSingleStep(((float)(rda->spacings[spacing->currentIndex()])) / 1000.);
    }
    freqOut = new QLCDNumber(6, this);
    freqOut->setSmallDecimalPoint(true);

    seek = new QPushButton("поиск", this);
    endless = new QCheckBox("бесконечно", this);
    reverse = new QCheckBox("обратно", this);
    tune = new QPushButton("поиск 2", this);
    seekCompl = new led("нашел", this);
    seekFail = new led("не нашел", this);

    power = new QCheckBox("вкл.", this);
    power->setChecked(true);
    rssi = new QProgressBar(this);
    rssi->setMinimum(0);
    rssi->setMaximum(rda->rssiMax);
    stereo = new led("стерео", this);
    rdsTrueStation = new led("RDS ловит", this);

    rdsReady = new led("RDS готов", this);
    rdsSynchronized = new led("RDS синхр", this);
    rds = new QLabel("0x0000 0x0000 0x0000 0x0000", this);
    rdsErrors = new QLabel("ошибок 0/24", this);

    lay = new QBoxLayout(QBoxLayout::LeftToRight, this);
    statementGroup = new QGroupBox("состояние", this);
    statementLay = new QBoxLayout(QBoxLayout::TopToBottom, statementGroup);
    statementLay->addWidget(power);
    statementLay->addWidget(rssi);
    statementLay->addWidget(stereo);
    statementLay->addWidget(rdsTrueStation);
    statementLay->addStretch();
    statementGroup->setLayout(statementLay);
    lay->addWidget(statementGroup, 1);

    soundGroup = new QGroupBox("громкость", this);
    soundLay = new QBoxLayout(QBoxLayout::TopToBottom, soundGroup);
    soundLay->addWidget(mute);
    soundLay->addWidget(softMute);
    soundLay->addWidget(mono);
    soundLay->addWidget(bass);
    soundLay->addWidget(vol);
    soundLay->addStretch();
    soundGroup->setLayout(soundLay);
    lay->addWidget(soundGroup, 1);

    bandGroup = new QGroupBox("диапазон", this);
    bandLay = new QBoxLayout(QBoxLayout::TopToBottom, bandGroup);
    bandLay->addWidget(band);
    bandLay->addWidget(direct);
    bandLay->addWidget(spacing);
    bandLay->addWidget(freq);
    bandLay->addWidget(freqOut);
    bandLay->addStretch();
    bandGroup->setLayout(bandLay);
    lay->addWidget(bandGroup, 1);

    seekGroup = new QGroupBox("автопоиск", this);
    seekLay = new QBoxLayout(QBoxLayout::TopToBottom, seekGroup);
    seekLay->addWidget(seek);
    seekLay->addWidget(endless);
    seekLay->addWidget(reverse);
    seekLay->addWidget(tune);
    seekLay->addWidget(seekCompl);
    seekLay->addWidget(seekFail);
    seekLay->addStretch();
    seekGroup->setLayout(seekLay);
    lay->addWidget(seekGroup, 1);

    rdsGroup = new QGroupBox("RDS", this);
    rdsLay = new QBoxLayout(QBoxLayout::TopToBottom, rdsGroup);
    rdsLay->addWidget(rdsReady);
    rdsLay->addWidget(rdsSynchronized);
    rdsLay->addWidget(rds);
    rdsLay->addWidget(rdsErrors);
    rdsLay->addStretch();
    rdsGroup->setLayout(rdsLay);
    lay->addWidget(rdsGroup, 1);
    setLayout(lay);

    timer = new QTimer(this);
    timer->setInterval(500);
    connect(timer, SIGNAL(timeout()), this, SLOT(readRda()));
    connect(mute, SIGNAL(stateChanged(int)), this, SLOT(muteHandl()));
    connect(softMute, SIGNAL(stateChanged(int)), this, SLOT(softMuteHandl()));
    connect(mono, SIGNAL(stateChanged(int)), this, SLOT(monoHandl()));
    connect(bass, SIGNAL(stateChanged(int)), this, SLOT(bassHandl()));
    connect(vol, SIGNAL(valueChanged(int)), this, SLOT(volHandl()));
    connect(band, SIGNAL(currentIndexChanged(int)), this, SLOT(bandHandl()));
    connect(direct, SIGNAL(stateChanged(int)), this, SLOT(directHandl()));
    connect(spacing, SIGNAL(currentIndexChanged(int)), this, SLOT(spacingHandl()));
    connect(freq, SIGNAL(valueChanged(double)), this, SLOT(freqHandl()));
    connect(seek, SIGNAL(released()), this, SLOT(seekHandl()));
    connect(endless, SIGNAL(stateChanged(int)), this, SLOT(endlessHandl()));
    connect(reverse, SIGNAL(stateChanged(int)), this, SLOT(reverseHandl()));
    connect(tune, SIGNAL(released()), this, SLOT(tuneHandl()));
    connect(power, SIGNAL(stateChanged(int)), this, SLOT(powerHandl()));
//    timer->start();
}

void mainMenu::muteHandl(void) {
    rda->mute(mute->isChecked());
}

void mainMenu::softMuteHandl(void) {
    rda->mute2(softMute->isChecked());
}

void mainMenu::monoHandl(void) {
    rda->mono(mono->isChecked());
}

void mainMenu::bassHandl(void) {
    rda->bass(bass->isChecked());
}

void mainMenu::volHandl(void) {
    rda->mono(mono->isChecked());
}

void mainMenu::bandHandl(void) {
    rda->setBand((rda5807::band)band->currentIndex());
    freq->blockSignals(true);
    freq->setMinimum(rda->bandMin[band->currentIndex()]);
    freq->setValue(rda->bandMin[band->currentIndex()]);
    if(direct->isChecked()) {
        freq->setMaximum(rda->bandMin[band->currentIndex()] + ((float)rda->freqMax)/1000.);
    } else {
        freq->setMaximum(rda->bandMin[band->currentIndex()] + (((float)(rda->spacings[spacing->currentIndex()] * rda->channelMax))/1000.));
    }
    freq->blockSignals(false);
}

void mainMenu::directHandl(void) {
    rda->freqDirect(direct->isChecked());
    freq->blockSignals(true);
    if(direct->isChecked()) {
        freq->setMaximum(rda->bandMin[band->currentIndex()] + ((float)rda->freqMax)/1000.);
        freq->setSingleStep(0.001);
    } else {
        freq->setValue(rda->bandMin[band->currentIndex()]);
        freq->setMaximum(rda->bandMin[band->currentIndex()] + (((float)(rda->spacings[spacing->currentIndex()] * rda->channelMax))/1000.));
        freq->setSingleStep(((float)(rda->spacings[spacing->currentIndex()])) / 1000.);
    }
    freq->blockSignals(false);
}

void mainMenu::spacingHandl(void) {
    rda->channelSpacing(rda->spacings[spacing->currentIndex()]);
    freq->blockSignals(true);
    if(!direct->isChecked()) {
        freq->setValue(rda->bandMin[band->currentIndex()]);
        freq->setMaximum(rda->bandMin[band->currentIndex()] + (((float)(rda->spacings[spacing->currentIndex()] * rda->channelMax))/1000.));
        freq->setSingleStep(((float)(rda->spacings[spacing->currentIndex()])) / 1000.);
    }
    freq->blockSignals(false);
}

void mainMenu::freqHandl(void) {
    if( direct->isChecked() ) {
        rda->freqDirect(freq->value()*1000);
    } else {
        rda->channelKhz(freq->value()*1000);
    }
}

void mainMenu::seekHandl(void) {
    rda->seek(true);
}

void mainMenu::endlessHandl(void) {
    rda->endlessSeek(endless->isChecked());
}

void mainMenu::reverseHandl(void) {
    rda->seekDir(reverse->isChecked());
}

void mainMenu::tuneHandl(void) {
    rda->tune(true);
}

void mainMenu::powerHandl(void) {
    rda->powerOn(power->isChecked());
}

void mainMenu::readRda(void) {
    rssi->setValue(rda->getRssi());
    stereo->active(rda->isStereo());
    rdsTrueStation->active(rda->isRdsTrueStation());
    freqOut->display(rda->getChannel() * rda->spacings[spacing->currentIndex()]);
    seekCompl->active(rda->isSeekComplete());
    seekFail->active(rda->isSeekFail());
    rdsReady->active(rda->isRdsReady());
    rdsSynchronized->active(rda->isRdsSynchronized());
    uint16_t rdsBuff[4];
    rda->getRds(rdsBuff);
    rds->clear();
    rds->setText(QString::asprintf("%04x %04x %04x %04x",rdsBuff[0],rdsBuff[1],rdsBuff[2],rdsBuff[3]));
    rdsErrors->clear();
    rds->setText(QString::asprintf("ошибок 0/%02d",rda->getRdsErrors()));
}
