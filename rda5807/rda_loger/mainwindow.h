#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "mainmenu.h"
#include "setupmenu.h"
#include "rda5807.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
private:
    QTabWidget *setupTabs;
    mainMenu *mainW;
    setupMenu *setup;
    rda5807 *rda;
};
#endif // MAINWINDOW_H
