#ifndef SETUPMENU_H
#define SETUPMENU_H

#include <QWidget>
#include <QBoxLayout>
#include <QCheckBox>
#include <QPushButton>
#include <QComboBox>
#include <QGroupBox>
#include <QSpinBox>
#include "rda5807.h"

class setupMenu : public QWidget
{
    Q_OBJECT
public:
    explicit setupMenu(rda5807 *rda, QWidget *parent = nullptr);

public slots:
    void hizHandl(void);
    void resetHandl(void);
    void afcHandl(void);
    void deEmphasis50Handl(void);
    void newDemodulatorHandl(void);
    void filterHandl(void);
    void softBlendThresholdHandl(void);
    void seekThresholdHandl(void);
    void oldSeekModeHandl(void);
    void oldSeekThresholdHandl(void);
    void rdsEnHandl(void);
    void rdsFifoModeHandl(void);
    void rdsFifoClearHandl(void);
    void rbdsHandl(void);
    void lnaCurrentHandl(void);
    void externalClockingHandl(void);
    void clkFreqHandl(void);
    void quartzHandl(void);
    void calibrateHandl(void);

signals:

private:
    QBoxLayout *lay;

    QGroupBox *mainGroup;
    QBoxLayout *mainLay;
    QCheckBox *hiz;
    QPushButton *reset;

    QGroupBox *fineGroup;
    QBoxLayout *fineLay;
    QCheckBox *afc;
    QCheckBox *deEmphasis50;
    QCheckBox *newDemodulator;
    QCheckBox *filter;
    QSpinBox *softBlendThresholdDb;

    QGroupBox *seekGroup;
    QBoxLayout *seekLay;
    QSpinBox *seekThreshold;
    QCheckBox *oldSeekMode;
    QSpinBox *oldSeekThreshold;

    QGroupBox *rdsGroup;
    QBoxLayout *rdsLay;
    QCheckBox *rdsEn;
    QCheckBox *rdsFifoMode;
    QPushButton *rdsFifoClear;
    QCheckBox *rbds;
    QComboBox *lnaCurrent;

    QGroupBox *clockGroup;
    QBoxLayout *clockLay;
    QCheckBox *externalClocking;
    QComboBox *clkFreq;
    QCheckBox *quartz;
    QCheckBox *calibrate;

    rda5807 *rda;
};

#endif // SETUPMENU_H
