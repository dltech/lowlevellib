#ifndef LED_H
#define LED_H

#include <QWidget>
#include <QPainter>

class led : public QWidget
{
    Q_OBJECT
public:
    explicit led(const QString &text, QWidget *parent = nullptr);
    void on();
    void off();
    void active(bool active = true);
    bool isActive(void);

protected:
    void paintEvent (QPaintEvent *event);
//    virtual void resizeEvent(QResizeEvent *event);

private:
    QPainter *bulb;
    bool voltage = false;
    QString *name;
    int nameWidth;
    static constexpr int ledSize = 20;

    void render(void);
};

#endif // LED_H
