#include "setupmenu.h"

setupMenu::setupMenu(rda5807 *rda, QWidget *parent)
    : QWidget{parent}
{
    this->rda = rda;

    hiz = new QCheckBox("выходы hiz", this);
    reset = new QPushButton("сброс", this);
    lnaCurrent = new QComboBox(this);
    for(int i=0 ; i<rda->lnaCurrentName->size() ; ++i) {
        lnaCurrent->insertItem(i, rda->lnaCurrentName->at(i));
    }
    lnaCurrent->setCurrentIndex(3);

    afc = new QCheckBox("ФАПЧ", this);
    deEmphasis50 = new QCheckBox("de emphasis 50/75 мкс", this);
    deEmphasis50->setChecked(true);
    newDemodulator = new QCheckBox("новый/старый демодулятор", this);
    newDemodulator->setChecked(true);
    filter = new QCheckBox("softblend фильтр", this);
    filter->setChecked(true);
    softBlendThresholdDb = new QSpinBox(this);
    softBlendThresholdDb->setMinimum(0);
    softBlendThresholdDb->setMaximum(rda->softBlendThresholdMax * 2);
    softBlendThresholdDb->setSingleStep(2);
    softBlendThresholdDb->setValue(32);
    softBlendThresholdDb->setSuffix(" дБ");

    seekThreshold = new QSpinBox(this);
    seekThreshold->setMinimum(0);
    seekThreshold->setMaximum(rda->seekThresholdMax);
    seekThreshold->setValue(4);
    oldSeekMode = new QCheckBox("старый поиск", this);
    oldSeekThreshold = new QSpinBox(this);
    oldSeekThreshold->setMinimum(0);
    oldSeekThreshold->setMaximum(rda->oldSeekThresholdMax);
    oldSeekThreshold->setValue(0);

    rdsEn = new QCheckBox("вкл RDS", this);
    rdsEn->setChecked(true);
    rdsFifoMode = new QCheckBox("режим FIFO", this);
    rdsFifoClear = new QPushButton("сброс FIFO", this);
    rbds = new QCheckBox("RBDS", this);

    externalClocking = new QCheckBox("внешнее", this);
    externalClocking->setChecked(true);
    clkFreq = new QComboBox(this);
    for(int i=0 ; i<rda->clkFreqName->size() ; ++i) {
        clkFreq->insertItem(i, rda->clkFreqName->at(i));
    }
    clkFreq->setCurrentIndex(4);
    quartz = new QCheckBox("кварц", this);
    calibrate = new QCheckBox("автокалибровка", this);
    calibrate->setChecked(true);

    lay = new QBoxLayout(QBoxLayout::LeftToRight, this);
    mainGroup = new QGroupBox("основные настр", this);
    mainLay = new QBoxLayout(QBoxLayout::TopToBottom, mainGroup);
    mainLay->addWidget(hiz);
    mainLay->addWidget(reset);
    mainLay->addWidget(lnaCurrent);
    mainLay->addStretch();
    mainGroup->setLayout(mainLay);
    lay->addWidget(mainGroup, 1);

    fineGroup = new QGroupBox("тонкие настр", this);
    fineLay = new QBoxLayout(QBoxLayout::TopToBottom, fineGroup);
    fineLay->addWidget(afc);
    fineLay->addWidget(deEmphasis50);
    fineLay->addWidget(newDemodulator);
    fineLay->addWidget(filter);
    fineLay->addWidget(softBlendThresholdDb);
    fineLay->addStretch();
    fineGroup->setLayout(fineLay);
    lay->addWidget(fineGroup, 1);

    seekGroup = new QGroupBox("автопоиск", this);
    seekLay = new QBoxLayout(QBoxLayout::TopToBottom, seekGroup);
    seekLay->addWidget(seekThreshold);
    seekLay->addWidget(oldSeekMode);
    seekLay->addWidget(oldSeekThreshold);
    seekLay->addStretch();
    seekGroup->setLayout(seekLay);
    lay->addWidget(seekGroup, 1);

    rdsGroup = new QGroupBox("RDS", this);
    rdsLay = new QBoxLayout(QBoxLayout::TopToBottom, rdsGroup);
    rdsLay->addWidget(rdsEn);
    rdsLay->addWidget(rdsFifoMode);
    rdsLay->addWidget(rdsFifoClear);
    rdsLay->addWidget(rbds);
    rdsLay->addStretch();
    rdsGroup->setLayout(rdsLay);
    lay->addWidget(rdsGroup, 1);

    clockGroup = new QGroupBox("тактирование", this);
    clockLay = new QBoxLayout(QBoxLayout::TopToBottom, clockGroup);
    clockLay->addWidget(externalClocking);
    clockLay->addWidget(clkFreq);
    clockLay->addWidget(quartz);
    clockLay->addWidget(calibrate);
    clockLay->addStretch();
    clockGroup->setLayout(clockLay);
    lay->addWidget(clockGroup, 1);
    setLayout(lay);

    connect(hiz, SIGNAL(stateChanged(int)), this, SLOT(hizHandl()));
    connect(reset, SIGNAL(released()), this, SLOT(resetHandl()));
    connect(afc, SIGNAL(stateChanged(int)), this, SLOT(afcHandl()));
    connect(deEmphasis50, SIGNAL(stateChanged(int)), this, SLOT(deEmphasis50Handl()));
    connect(newDemodulator, SIGNAL(stateChanged(int)), this, SLOT(newDemodulatorHandl()));
    connect(filter, SIGNAL(stateChanged(int)), this, SLOT(filterHandl()));
    connect(softBlendThresholdDb, SIGNAL(valueChanged(int)), this, SLOT(softBlendThresholdHandl()));
    connect(seekThreshold, SIGNAL(valueChanged(int)), this, SLOT(seekThresholdHandl()));
    connect(oldSeekMode, SIGNAL(stateChanged(int)), this, SLOT(oldSeekModeHandl()));
    connect(oldSeekThreshold, SIGNAL(valueChanged(int)), this, SLOT(oldSeekThresholdHandl()));
    connect(rdsEn, SIGNAL(stateChanged(int)), this, SLOT(rdsEnHandl()));
    connect(rdsFifoMode, SIGNAL(stateChanged(int)), this, SLOT(rdsFifoModeHandl()));
    connect(rdsFifoClear, SIGNAL(released()), this, SLOT(rdsFifoClearHandl()));
    connect(rbds, SIGNAL(stateChanged(int)), this, SLOT(rbdsHandl()));
    connect(lnaCurrent, SIGNAL(currentIndexChanged(int)), this, SLOT(lnaCurrentHandl()));
    connect(externalClocking, SIGNAL(stateChanged(int)), this, SLOT(externalClockingHandl()));
    connect(clkFreq, SIGNAL(currentIndexChanged(int)), this, SLOT(clkFreqHandl()));
    connect(quartz, SIGNAL(stateChanged(int)), this, SLOT(quartzHandl()));
    connect(calibrate, SIGNAL(stateChanged(int)), this, SLOT(calibrateHandl()));
}

void setupMenu::hizHandl(void) {
    rda->outputHiz(hiz->isChecked());
}

void setupMenu::resetHandl(void) {
    rda->reset(true);
}

void setupMenu::afcHandl(void) {
    rda->afc(afc->isChecked());
}

void setupMenu::deEmphasis50Handl(void) {
    rda->deEmphasis50(deEmphasis50->isChecked());
}

void setupMenu::newDemodulatorHandl(void) {
    rda->newDemodulator(newDemodulator->isChecked());
}

void setupMenu::filterHandl(void) {
    rda->filter(filter->isChecked());
}

void setupMenu::softBlendThresholdHandl(void) {
    rda->softBlendThresholdDb(softBlendThresholdDb->value()/2);
}

void setupMenu::seekThresholdHandl(void) {
    rda->seekThreshold(seekThreshold->value());
}

void setupMenu::oldSeekModeHandl(void) {
    rda->oldSeekMode(oldSeekMode->isChecked());
}

void setupMenu::oldSeekThresholdHandl(void) {
    rda->oldSeekThreshold(oldSeekThreshold->value());
}

void setupMenu::rdsEnHandl(void) {
    rda->rdsEn(rdsEn->isChecked());
}

void setupMenu::rdsFifoModeHandl(void) {
    rda->rdsFifoMode(rdsFifoMode->isChecked());
}

void setupMenu::rdsFifoClearHandl(void) {
    rda->rdsFifoClear(true);
}

void setupMenu::rbdsHandl(void) {
    rda->rbds(rbds->isChecked());
}

void setupMenu::lnaCurrentHandl(void) {
    rda->lnaCurrentUa(rda->lnaCurrents[lnaCurrent->currentIndex()]);
}

void setupMenu::externalClockingHandl(void) {
    if(externalClocking->isChecked()) {
        quartz->setChecked(false);
        calibrate->setChecked(true);
    } else {
        quartz->setChecked(true);
        calibrate->setChecked(false);
    }
}

void setupMenu::clkFreqHandl(void) {
    rda->clkFreq(rda->clkFreqs[clkFreq->currentIndex()]);
}

void setupMenu::quartzHandl(void) {
    rda->quartz(quartz->isChecked());
}

void setupMenu::calibrateHandl(void) {
    rda->calibrate(calibrate->isChecked());
}
