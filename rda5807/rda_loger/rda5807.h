#ifndef RDA5807_H
#define RDA5807_H

#include <QObject>
#include <QSerialPort>

class rda5807 : public QObject
{
    Q_OBJECT
public:
    explicit rda5807(QObject *parent = nullptr);
    ~rda5807(void);

    // setters
    // on/off options
    void powerOn(bool onOff);
    void reset(bool onOff);
    void outputHiz(bool onOff);
    // sound settings
    void mute(bool onOff);
    void mute2(bool onOff);
    void mono(bool onOff);
    void bass(bool onOff);
    void volume(uint8_t vol);
    static constexpr int volMax = 15;
    // receiver options
    void afc(bool onOff);
    void deEmphasis50(bool onOff);
    void newDemodulator(bool onOff);
    // softblend filter
    void filter(bool onOff);
    void softBlendThresholdDb(uint8_t db);
    static constexpr int softBlendThresholdMax = 31;
    // seek
    void seek(bool onOff);
    void tune(bool onOff);
    // seek options
    void endlessSeek(bool onOff);
    void seekDir(bool onOff);
    void seekThreshold(uint8_t snr);
    static constexpr int seekThresholdMax = 15;
    // old seek mode
    void oldSeekMode(bool onOff);
    void oldSeekThreshold(uint8_t snr);
    static constexpr int oldSeekThresholdMax = 63;
    // clocking config
    void externalClocking(void);
    void clkFreq(int kHz);
    static constexpr int clkFreqs[7] = {32, 12000, 13000, 19200, 24000, 27000, 38400};
    QVector<QString> *clkFreqName;
    void quartz(bool onOff);
    void calibrate(bool onOff);
    void clockInvert(bool onOff);
    // radio frequency
    // fm frequency band choosing
    enum band {
        BAND_50_65,
        BAND_65_76,
        BAND_76_91,
        BAND_76_108,
        BAND_87_108
    };
    void setBand(band n);
    QVector<QString> *bandName;
    static constexpr float bandMin[5] = {50., 65., 76., 76., 87.};
    void channelSpacing(int kHz);
    static constexpr int spacings[4] = {25, 50, 100, 200};
    QVector<QString> *spacingName;
    // channel choosing
    void channelN(uint16_t n);
    static constexpr int channelMax = 1023;
    void channelKhz(uint32_t khz);
    // direct access to the frequency
    void freqDirectMode(bool onOff);
    void freqDirect(uint32_t khz);
    static constexpr int freqMax = 65535;
    // input LNA
    void lnaCurrentUa(uint16_t current);
    static constexpr int lnaCurrents[4] = {1800, 2100, 2500, 3000};
    QVector<QString> *lnaCurrentName;
    // rds
    void rdsEn(bool onOff);
    void rdsFifoMode(bool onOff);
    void rdsFifoClear(bool onOff);
    void rbds(bool onOff);

    //getters
    // search progress
    bool isSeekComplete(void);
    bool isSeekFail(void);
    // rds statement
    bool isRdsReady(void);
    bool isRdsSynchronized(void);
    // fm station condition
    bool isStereo(void);
    bool isRdsTrueStation(void);
    // current shannel after seek
    uint16_t getChannel(void);
    uint8_t getRssi(void);
    static constexpr int rssiMax = 127;
    int getRds(uint16_t *rds);
    uint32_t getRdsErrors(void);
    static constexpr int rdsErtrorsMax = 24;

    // interface commands
    int updateConfig(void);
    int read(void);

    QString *message;

signals:


private:
    enum cmdSet {
        updateConfigCmd,
        readCmd,
        powerOnCmd,
        resetCmd,
        outputHizCmd,
        muteCmd,
        mute2Cmd,
        monoCmd,
        bassCmd,
        volumeCmd,
        afcCmd,
        deEmphasis50Cmd,
        newDemodulatorCmd,
        filterCmd,
        softBlendThresholdDbCmd,
        seekCmd,
        tuneCmd,
        endlessSeekCmd,
        seekDirCmd,
        seekThresholdCmd,
        oldSeekModeCmd,
        oldSeekThresholdCmd,
        isSeekCompleteCmd,
        isSeekFailCmd,
        externalClockingCmd,
        clkFreqCmd,
        quartzCmd,
        calibrateCmd,
        clockInvertCmd,
        band50_65Cmd,
        band65_76Cmd,
        band76_91Cmd,
        band87_108Cmd,
        band76_108Cmd,
        channelSpacingCmd,
        channelNCmd,
        channelKhzCmd,
        freqDirectModeCmd,
        freqDirectCmd,
        getChannelCmd,
        lnaCurrentUaCmd,
        getRdsCmd,
        rdsEnCmd,
        rdsFifoModeCmd,
        rdsFifoClearCmd,
        rbdsCmd,
        isRdsReadyCmd,
        isRdsSynchronizedCmd,
        getRdsErrorsCmd,
        isStereoCmd,
        isRdsTrueStationCmd,
        getRssiCmd
    };
    void request(cmdSet cmd, uint32_t par = 0);
    bool boolResponse(void);
    uint32_t intResponse(void);
    int arrayResponse(uint8_t *data);
    // data and port
    static constexpr int requestSize = 8;
    uint8_t reqData[requestSize];
    static constexpr int rxSize = 8;
    uint8_t rxBuffer[rxSize];
    QSerialPort *port;
    static constexpr int typicalDelay = 500;
};

#endif // RDA5807_H
