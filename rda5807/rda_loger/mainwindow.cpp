#include "mainwindow.h"
#include <QTabBar>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    this->setWindowTitle("rda test console");
    rda = new rda5807(this);
    setupTabs = new QTabWidget(this);
    mainW = new mainMenu(rda, this);
    setup = new setupMenu(rda, this);
    setupTabs->addTab(mainW, "радио");
    setupTabs->addTab(setup, "настройки");
    setCentralWidget(setupTabs);
    resize(640, 240);
}

MainWindow::~MainWindow()
{
}

