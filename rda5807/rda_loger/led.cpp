#include <QApplication>
#include "led.h"

led::led(const QString &text, QWidget *parent)
    : QWidget{parent}
{
    name = new QString(text);
    QFontMetrics fm(QApplication::font());
    nameWidth = fm.horizontalAdvance(*name);
    setFixedHeight(ledSize+5);
    setFixedWidth(nameWidth + ledSize + 10);
//    bulb.setRenderHint(QPainter::Antialiasing);
}

void led::paintEvent (QPaintEvent *event)
{
    Q_UNUSED(event);
    render();
}

void led::render()
{
    QPainter bulb(this);
    bulb.translate(((width()-nameWidth-ledSize)/2), (height()-ledSize)/2);
    if(voltage) {
        bulb.setBrush(QBrush(Qt::green, Qt::SolidPattern));
    } else {
        bulb.setBrush(QBrush(Qt::gray, Qt::SolidPattern));
    }
    bulb.drawEllipse(0, 0, ledSize, ledSize);
    bulb.drawText(ledSize+5, 15, QString(*name));
}

void led::on()
{
    voltage = true;
//    render();
}

void led::off()
{
    voltage = false;
//    render();
}

void led::active(bool active)
{
    voltage = active;
//    render();
}

bool led::isActive(void)
{
    return voltage;
}
