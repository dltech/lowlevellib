#ifndef MAINMENU_H
#define MAINMENU_H

#include <QWidget>
#include <QBoxLayout>
#include <QPushButton>
#include <QSlider>
#include <QCheckBox>
#include <QComboBox>
#include <QSpinBox>
#include <QLCDNumber>
#include <QProgressBar>
#include <QLabel>
#include <QGroupBox>
#include <QTimer>
#include "led.h"
#include "rda5807.h"

class mainMenu : public QWidget
{
    Q_OBJECT
public:
    explicit mainMenu(rda5807 *rda, QWidget *parent = nullptr);

public slots:
    void muteHandl(void);
    void softMuteHandl(void);
    void monoHandl(void);
    void bassHandl(void);
    void volHandl(void);
    void bandHandl(void);
    void directHandl(void);
    void spacingHandl(void);
    void freqHandl(void);
    void seekHandl(void);
    void endlessHandl(void);
    void reverseHandl(void);
    void tuneHandl(void);
    void powerHandl(void);
    void readRda(void);

signals:

private:
    rda5807 *rda;

    QBoxLayout *lay;

    QGroupBox *soundGroup;
    QBoxLayout *soundLay;
    QCheckBox *mute;
    QCheckBox *softMute;
    QCheckBox *mono;
    QCheckBox *bass;
    QSlider *vol;

    QGroupBox *bandGroup;
    QBoxLayout *bandLay;
    QComboBox *band;
    QCheckBox *direct;
    QComboBox *spacing;
    QDoubleSpinBox *freq;
    QLCDNumber *freqOut;

    QGroupBox *seekGroup;
    QBoxLayout *seekLay;
    QPushButton *seek;
    QCheckBox *endless;
    QCheckBox *reverse;
    QPushButton *tune;
    led *seekCompl;
    led *seekFail;

    QGroupBox *statementGroup;
    QBoxLayout *statementLay;
    QCheckBox *power;
    QProgressBar *rssi;
    led *stereo;
    led *rdsTrueStation;

    QGroupBox *rdsGroup;
    QBoxLayout *rdsLay;
    led *rdsReady;
    led *rdsSynchronized;
    QLabel *rds;
    QLabel *rdsErrors;

    QTimer *timer;
};

#endif // MAINMENU_H
