#include "rda5807.h"
#include <QSerialPortInfo>

rda5807::rda5807(QObject *parent)
    : QObject{parent}
{
    bandName = new QVector<QString>;
    bandName->append(QString("50-65 МГц"));
    bandName->append(QString("65-76 МГц"));
    bandName->append(QString("76-91 МГц"));
    bandName->append(QString("76-108 МГц"));
    bandName->append(QString("87-108 МГц"));

    clkFreqName = new QVector<QString>;
    clkFreqName->append(QString("32768 Гц"));
    clkFreqName->append(QString("12 МГц"));
    clkFreqName->append(QString("13 МГц"));
    clkFreqName->append(QString("19.2 МГц"));
    clkFreqName->append(QString("24 МГц"));
    clkFreqName->append(QString("27 МГц"));
    clkFreqName->append(QString("38.4 МГц"));

    spacingName = new QVector<QString>;
    spacingName->append(QString("25 кГц"));
    spacingName->append(QString("50 кГц"));
    spacingName->append(QString("100 кГц"));
    spacingName->append(QString("200 кГц"));

    lnaCurrentName = new QVector<QString>;
    lnaCurrentName->append(QString("1.8 мА"));
    lnaCurrentName->append(QString("2.1 мА"));
    lnaCurrentName->append(QString("2.5 мА"));
    lnaCurrentName->append(QString("3 мА"));

    port = new QSerialPort(0);
    message = new QString();
    QList<QSerialPortInfo> list;
    list = QSerialPortInfo::availablePorts();
    for( int i=0 ; i<list.length() ; ++i) {
        if( list[i].portName().contains("ACM") ) {
            port->setPortName(list[i].portName());
            message->clear();
            message->append("connected ");
            message->append(list[i].portName());
        }
    }
    port->setBaudRate(9600,QSerialPort::Input);
    if( !port->open(QIODevice::ReadWrite) ) {
        message->clear();
        message->append("port error");
    }
}

rda5807::~rda5807()
{
    delete message;
    port->close();
    delete port;
}

void rda5807::powerOn(bool onOff)
{
    if(onOff) {
        request(powerOnCmd, 1);
    } else {
        request(powerOnCmd, 0);
    }
}

void rda5807::reset(bool onOff)
{
    if(onOff) {
        request(resetCmd, 1);
    } else {
        request(resetCmd, 0);
    }
}

void rda5807::outputHiz(bool onOff)
{
    if(onOff) {
        request(outputHizCmd, 1);
    } else {
        request(outputHizCmd, 0);
    }
}

void rda5807::mute(bool onOff)
{
    if(onOff) {
        request(muteCmd, 1);
    } else {
        request(muteCmd, 0);
    }
}

void rda5807::mute2(bool onOff)
{
    if(onOff) {
        request(mute2Cmd, 1);
    } else {
        request(mute2Cmd, 0);
    }
}

void rda5807::mono(bool onOff)
{
    if(onOff) {
        request(monoCmd, 1);
    } else {
        request(monoCmd, 0);
    }
}

void rda5807::bass(bool onOff)
{
    if(onOff) {
        request(bassCmd, 1);
    } else {
        request(bassCmd, 0);
    }
}
void rda5807::volume(uint8_t vol)
{
    request(volumeCmd, (uint32_t)vol);
}

void rda5807::afc(bool onOff)
{
    request(afcCmd, (uint32_t)onOff);
}

void rda5807::deEmphasis50(bool onOff)
{
    request(deEmphasis50Cmd, (uint32_t)onOff);
}

void rda5807::newDemodulator(bool onOff)
{
    request(newDemodulatorCmd, (uint32_t)onOff);
}

void rda5807::filter(bool onOff)
{
    request(filterCmd, (uint32_t)onOff);
}

void rda5807::softBlendThresholdDb(uint8_t db)
{
    request(softBlendThresholdDbCmd, (uint32_t)db);
}

void rda5807::seek(bool onOff)
{
    request(seekCmd, (uint32_t)onOff);
}

void rda5807::tune(bool onOff)
{
    request(tuneCmd, (uint32_t)onOff);
}

void rda5807::endlessSeek(bool onOff)
{
    request(endlessSeekCmd, (uint32_t)onOff);
}

void rda5807::seekDir(bool onOff)
{
    request(seekDirCmd, (uint32_t)onOff);
}

void rda5807::seekThreshold(uint8_t snr)
{
    request(seekThresholdCmd, (uint32_t)snr);
}

void rda5807::oldSeekMode(bool onOff)
{
    request(oldSeekModeCmd, (uint32_t)onOff);
}

void rda5807::oldSeekThreshold(uint8_t snr)
{
    request(oldSeekThresholdCmd, (uint32_t)snr);
}

void rda5807::externalClocking()
{
    request(externalClockingCmd);
}

void rda5807::clkFreq(int kHz)
{
    request(clkFreqCmd, (uint32_t)kHz);
}

void rda5807::quartz(bool onOff)
{
    request(quartzCmd, (uint32_t)onOff);
}

void rda5807::calibrate(bool onOff)
{
    request(calibrateCmd, (uint32_t)onOff);
}

void rda5807::clockInvert(bool onOff)
{
    request(clockInvertCmd, (uint32_t)onOff);
}

void rda5807::setBand(band n)
{
    switch(n)
    {
        case BAND_50_65:
            request(band50_65Cmd);
            break;
        case BAND_65_76:
            request(band65_76Cmd);
            break;
        case BAND_76_91:
            request(band76_91Cmd);
            break;
        case BAND_76_108:
            request(band76_108Cmd);
            break;
        case BAND_87_108:
            request(band87_108Cmd);
            break;
    }
}

void rda5807::channelSpacing(int kHz)
{
    request(deEmphasis50Cmd, (uint32_t)kHz);
}

void rda5807::channelN(uint16_t n)
{
    request(channelNCmd, (uint32_t)n);
}

void rda5807::channelKhz(uint32_t khz)
{
    request(channelKhzCmd, khz);
}

void rda5807::freqDirectMode(bool onOff)
{
    request(freqDirectModeCmd, (uint32_t)onOff);
}

void rda5807::freqDirect(uint32_t khz)
{
    request(freqDirectCmd, khz);
}

void rda5807::lnaCurrentUa(uint16_t current)
{
    request(lnaCurrentUaCmd, (uint32_t)current);
}

void rda5807::rdsEn(bool onOff)
{
    request(rdsEnCmd, (uint32_t)onOff);
}

void rda5807::rdsFifoMode(bool onOff)
{
    request(rdsFifoModeCmd, (uint32_t)onOff);
}

void rda5807::rdsFifoClear(bool onOff)
{
    request(rdsFifoClearCmd, (uint32_t)onOff);
}

void rda5807::rbds(bool onOff)
{
    request(deEmphasis50Cmd, (uint32_t)onOff);
}

bool rda5807::isSeekComplete()
{
    request(isSeekCompleteCmd);
    return boolResponse();
}

bool rda5807::isSeekFail()
{
    request(isSeekFailCmd);
    return boolResponse();
}

bool rda5807::isRdsReady()
{
    request(isRdsReadyCmd);
    return boolResponse();
}

bool rda5807::isRdsSynchronized()
{
    request(isRdsSynchronizedCmd);
    return boolResponse();
}

bool rda5807::isStereo()
{
    request(isStereoCmd);
    return boolResponse();
}

bool rda5807::isRdsTrueStation()
{
    request(isRdsTrueStationCmd);
    return boolResponse();
}

uint16_t rda5807::getChannel()
{
    request(getChannelCmd);
    return (uint16_t)intResponse();
}

uint8_t rda5807::getRssi()
{
    request(getRssiCmd);
    return (uint8_t)intResponse();
}

int rda5807::getRds(uint16_t *rds)
{
    request(getRdsCmd);
    return arrayResponse((uint8_t*)rds);
}

uint32_t rda5807::getRdsErrors()
{
    request(getRdsErrorsCmd);
    return intResponse();
}

int rda5807::updateConfig()
{
    request(updateConfigCmd);
    if( !boolResponse() ) {
        message->clear();
        message->append("connection error");
        return -1;
    }
    return 0;
}

int rda5807::read()
{
    request(readCmd);
    if( !boolResponse() ) {
        message->clear();
        message->append("connection error");
        return -1;
    }
    return 0;
}

void rda5807::request(cmdSet cmd, uint32_t par)
{
    int ptr = 0;
    reqData[ptr++] = 'r';
    reqData[ptr++] = 'd';
    reqData[ptr++] = 'a';
    reqData[ptr++] = (uint8_t)cmd;
    for(int i=0 ; i<4 ; ++i) {
        reqData[ptr+i] = (uint8_t)(par >> (3-i)*8);
    }
    port->write((const char*)reqData, requestSize);
}

bool rda5807::boolResponse()
{
    if( !port->waitForReadyRead(typicalDelay) ) {
        return false;
    }
    int obtained = port->read((char*)rxBuffer, rxSize);
    port->readAll();
    if(obtained < rxSize) {
        return false;
    }

    if(rxBuffer[0] == 1) {
        return true;
    }
    return false;
}

uint32_t rda5807::intResponse()
{
    if( !port->waitForReadyRead(typicalDelay) ) {
        return 0xffffffff;
    }
    int obtained = port->read((char*)rxBuffer, rxSize);
    port->readAll();
    if(obtained < rxSize) {
        return 0xffffffff;
    }
    int ret = 0;
    for(int i=0 ; i<4 ; ++i) {
        ret |= rxBuffer[i] << (3-i)*8;
    }
    return ret;
}

int rda5807::arrayResponse(uint8_t *data)
{
    if( !port->waitForReadyRead(typicalDelay) ) {
        return -1;
    }
    int obtained = port->read((char*)rxBuffer, rxSize);
    port->readAll();
    if(obtained < rxSize) {
        return -2;
    }
    for(int i=0 ; i<rxSize ; ++i) {
        data[i] = rxBuffer[i];
    }
    return 0;
}
