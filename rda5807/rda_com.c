/*
 * PC development kit
 * RDA5807 digital rf chip com port loger access interface.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "../stm32/usbcom_virtual/usb_core.h"
#include "rda5807.h"
#include "int_to_byte_array.h"
#include "rda_com.h"

int isCommand(uint8_t data, int size);
void commandHandler(uint8_t *msg);


void rdaComRxHandler(uint8_t *data, int size)
{
    if(isCommand(data,size)) {
        commandHandler(data);
    }
}


int isCommand(uint8_t data, int size)
{
    if( (size != RDA_CMD_SIZE) || (data[0] > getRssiCmd) ) {
        return 0;
    }
    return 1;
}

void commandHandler(uint8_t *msg)
{
    uint8_t txBuffer[8];
    uint16_t temp;
    switch(msg[0])
    {
        case updateConfigCmd:
            rdaUpdateConfig();
            break;
        case readCmd:
            rdaRead();
            break;
        case powerOnCmd:
            rdaPowerOn(msg[1]);
            break;
        case resetCmd:
            rdaReset(msg[1]);
            break;
        case outputHizCmd:
            rdaOutputHiz(msg[1]);
            break;
        case muteCmd:
            rdaMute(msg[1]);
            break;
        case mute2Cmd:
            rdaMute2(msg[1]);
            break;
        case monoCmd:
            rdaMono(msg[1]);
            break;
        case bassCmd:
            rdaBass(msg[1]);
            break;
        case volumeCmd:
            rdaVolume(msg[1]);
            break;
        case afcCmd:
            rdaAfc(msg[1]);
            break;
        case deEmphasis50Cmd:
            rdaDeEmphasis50(msg[1]);
            break;
        case newDemodulatorCmd:
            rdaNewDemodulator(msg[1]);
            break;
        case filterCmd:
            rdaFilter(msg[1]);
            break;
        case softBlendThresholdDbCmd:
            rdaSoftBlendThresholdDb(msg[1]);
            break;
        case seekCmd:
            rdaSeek(msg[1]);
            break;
        case tuneCmd:
            rdaTune(msg[1]);
            break;
        case endlessSeekCmd:
            rdaEndlessSeek(msg[1]);
            break;
        case seekDirCmd:
            rdaSeekDir(msg[1]);
            break;
        case seekThresholdCmd:
            rdaSeekThreshold(msg[1]);
            break;
        case oldSeekModeCmd:
            rdaOldSeekMode(msg[1]);
            break;
        case oldSeekThresholdCmd:
            rdaOldSeekThreshold(msg[1]);
            break;
        case isSeekCompleteCmd:
            txBuffer[0] = isRdaSeekComplete();
            vcpTx(txBuffer, 1);
            break;
        case isSeekFailCmd:
            txBuffer[0] = isRdaSeekFail();
            vcpTx(txBuffer, 1);
            break;
        case externalClockingCmd:
            rdaExternalClocking();
            break;
        case clkFreqCmd:
            rdaClkFreq(toUint32(msg+1));
            break;
        case quartzCmd:
            rdaQuartz(msg[1]);
            break;
        case calibrateCmd:
            rdaCalibrate(msg[1]);
            break;
        case clockInvertCmd:
            rdaClockInvert(msg[1]);
            break;
        case band50_65Cmd:
            rdaBand50_65();
            break;
        case band65_76Cmd:
            rdaBand65_76();
            break;
        case band76_91Cmd:
            rdaBand76_91();
            break;
        case band87_108Cmd:
            rdaBand87_108();
            break;
        case band76_108Cmd:
            rdaBand76_108();
            break;
        case channelSpacingCmd:
            rdaChannelSpacing(toUint32(msg+1));
            break;
        case channelNCmd:
            rdaChannelN(toUint16(msg+1));
            break;
        case channelKhzCmd:
            rdaChannelKhz(toUint32(msg+1));
            break;
        case freqDirectModeCmd:
            rdaFreqDirectMode(msg[1]);
            break;
        case freqDirectCmd:
            rdaFreqDirect(toUint32(msg+1));
            break;
        case getChannelCmd:
            temp = rdaGetChannel();
            fromUint16(txBuffer,temp);
            vcpTx(txBuffer, 2);
            break;
        case lnaCurrentUaCmd:
            rdaLnaCurrentUa(toUint16(msg+1));
            break;
        case getRdsCmd:
            rdaGetRds((uint16_t)txBuffer);
            vcpTx(txBuffer, 8);
            break;
        case rdsEnCmd:
            rdaRdsEn(msg[1]);
            break;
        case rdsFifoModeCmd:
            rdaRdsFifoMode(msg[1]);
            break;
        case rdsFifoClearCmd:
            rdaRdsFifoClear(msg[1]);
            break;
        case rbdsCmd:
            rdaRbds(msg[1]);
            break;
        case isRdsReadyCmd:
            txBuffer[0] = isRdaRdsReady();
            vcpTx(txBuffer, 1);
            break;
        case isRdsSynchronizedCmd:
            txBuffer[0] = isRdaRdsSynchronized();
            vcpTx(txBuffer, 1);
            break;
        case getRdsErrorsCmd:
            txBuffer[0] = rdaGetRdsErrors();
            vcpTx(txBuffer, 1);
            break;
        case isStereoCmd:
            txBuffer[0] = isRdaStereo();
            vcpTx(txBuffer, 1);
            break;
        case isRdsTrueStationCmd:
            txBuffer[0] = isRdaRdsTrueStation();
            vcpTx(txBuffer, 1);
            break;
        case getRssiCmd:
            txBuffer[0] = rdaGetRssi();
            vcpTx(txBuffer, 1);
            break;
    }
}
