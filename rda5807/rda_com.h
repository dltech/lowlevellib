/*
 * PC development kit
 * RDA5807 digital rf chip com port loger access interface.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define RDA_CMD_SIZE    8

enum cmdSet {
    updateConfigCmd,
    readCmd,
    powerOnCmd,
    resetCmd,
    outputHizCmd,
    muteCmd,
    mute2Cmd,
    monoCmd,
    bassCmd,
    volumeCmd,
    afcCmd,
    deEmphasis50Cmd,
    newDemodulatorCmd,
    filterCmd,
    softBlendThresholdDbCmd,
    seekCmd,
    tuneCmd,
    endlessSeekCmd,
    seekDirCmd,
    seekThresholdCmd,
    oldSeekModeCmd,
    oldSeekThresholdCmd,
    isSeekCompleteCmd,
    isSeekFailCmd,
    externalClockingCmd,
    clkFreqCmd,
    quartzCmd,
    calibrateCmd,
    clockInvertCmd,
    band50_65Cmd,
    band65_76Cmd,
    band76_91Cmd,
    band87_108Cmd,
    band76_108Cmd,
    channelSpacingCmd,
    channelNCmd,
    channelKhzCmd,
    freqDirectModeCmd,
    freqDirectCmd,
    getChannelCmd,
    lnaCurrentUaCmd,
    getRdsCmd,
    rdsEnCmd,
    rdsFifoModeCmd,
    rdsFifoClearCmd,
    rbdsCmd,
    isRdsReadyCmd,
    isRdsSynchronizedCmd,
    getRdsErrorsCmd,
    isStereoCmd,
    isRdsTrueStationCmd,
    getRssiCmd
};


void rdaComRxHandler(uint8_t *data, int size);

