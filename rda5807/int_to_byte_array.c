/*
 * PC development kit
 * RDA5807 digital rf chip com port loger access interface.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "int_to_byte_array.h"

uint32_t toUint32(uint8_t *data)
{
    return (uint32_t)(data[0] + (data[1]<<8) + (data[2]<<16) + (data[3]<<24));
}

uint16_t toUint16(uint8_t *data)
{
    return (uint16_t)(data[0] + (data[1]<<8));
}

void fromUint16(uint8_t *data, uint16_t var)
{
    data[0] = (uint8_t)var;
    data[1] = (uint8_t)(var>>8);
}
