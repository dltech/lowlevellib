#ifndef H_LIS3DH_REGS
#define H_LIS3DH_REGS
/*
 * LIS3DH MEMS digital output motion sensor. Register definitions.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LIS3DH_ADDR 0x30

#define STATUS_REG_AUX  0x07 /*  */
// 1, 2 and 3-axis data overrun. 1: a new set of data has overwritten the previous set
#define S321OR  0x80
// 3-axis data overrun.
#define S3OR    0x40
// 2-axis data overrun.
#define S2OR    0x20
// 1-axis data overrun.
#define S1OR    0x10
// 1, 2 and 3-axis new data available. 1: a new set of data is available
#define S321DA  0x08
// 3-axis new data available.
#define S3DA    0x04
// 2 -axis new data available.
#define S2DA    0x02
// 1-axis new data available.
#define S1DA    0x01

#define OUT_ADC1_L      0x08 /* Auxiliary 10-bit ADC channel 1 conversion. */
#define OUT_ADC1_H      0x09

#define OUT_ADC2_L      0x0a /* Auxiliary 10-bit ADC channel 2 conversion. */
#define OUT_ADC2_H      0x0b

#define OUT_ADC3_L      0x0c /* Auxiliary 10-bit ADC channel 3 conversion. */
#define OUT_ADC3_H      0x0d

#define WHO_AM_I        0x0f /* Device identification register. */

#define CTRL_REG0       0x1e /*  */
// Disconnect SDO/SA0 pull-up.
#define SDO_PU_DISC 0x80

#define TEMP_CFG_REG    0x1f /*  */
// Temperature sensor (T) enable. Default value: 0
#define TEMP_EN 0x40
// ADC enable. Default value: 0
#define ADC_EN  0x80

#define CTRL_REG1       0x20 /*  */
// Data rate selection.
#define ODR_PWRDN   0x00
#define ODR_1HZ     0x10
#define ODR_10HZ    0x20
#define ODR_25HZ    0x30
#define ODR_50HZ    0x40
#define ODR_100HZ   0x50
#define ODR_200HZ   0x60
#define ODR_400HZ   0x70
#define ODR_1P6KHZ  0x80
#define ODR_5P3HZ   0x90
// Low-power mode enable.
#define L_PEN       0x08
// Z-axis enable.
#define ZEN         0x04
// Y-axis enable.
#define YEN         0x02
// X-axis enable.
#define XEN         0x01

#define CTRL_REG2       0x21 /*  */
// High-pass filter mode selection.
#define HPM_MSK             0xc0
#define HPM_NORMAL          0x00
#define HPM_REF_FOR_FILTER  0x40
#define HPM_AUTORESET       0xc0
// High-pass filter cutoff frequency selection
#define HPCF_MSK            0x30
#define HPCF_SFT            4
#define HPCF_SET(n)         ((n<<HPCF_SFT)&HPCF_MSK)
// Filtered data selection.
#define FDS                 0x08
// High-pass filter enabled for CLICK function.
#define HPCLICK             0x04
// High-pass filter enabled for AOI function on interrupt 2,
#define HP_IA2              0x02
// High-pass filter enabled for AOI function on interrupt 1,
#define HP_IA1              0x01

#define CTRL_REG3       0x22 /*  */
// Click interrupt on INT1.
#define I1_CLICK    0x80
// IA1 interrupt on INT1.
#define I1_IA1      0x40
// IA2 interrupt on INT1.
#define I1_IA2      0x20
// ZYXDA interrupt on INT1.
#define I1_ZYXDA    0x10
// 321DA interrupt on INT1.
#define I1_321DA    0x08
// FIFO watermark interrupt on INT1.
#define I1_WTM      0x04
// FIFO overrun interrupt on INT1.
#define I1_OVERRUN  0x02

#define CTRL_REG4       0x23 /*  */
// Block data update. 1: output registers not updated until MSB and LSB reading
#define BDU     0x80
// Big/little endian data selection. 1: Data MSB @ lower address
#define BLE     0x40
// Full-scale selection.
#define FS2G    0x00
#define FS4G    0x10
#define FS8G    0x20
#define FS11G   0x30
// High-resolution output mode
#define HR      0x08
// Self-test enable.
#define ST0     0x02
#define ST1     0x04
// SPI serial interface mode selection. 0: 4-wire interface; 1: 3-wire interface
#define SIM     0x01

#define CTRL_REG5       0x24 /*  */
// Reboot memory content.
#define BOOT        0x80
// FIFO enable.
#define FIFO_EN     0x40
// Latch interrupt request on INT1_SRC register, with INT1_SRC (31h) register
#define LIR_INT1    0x08
// 4D enable: 4D detection is enabled on INT1 when 6D bit on INT1_CFG is set to 1.
#define D4D_INT1    0x04
// Latch interrupt request on INT2_SRC (35h) register, with INT2_SRC (35h) register
#define LIR_INT2    0x02
// 4D enable: 4D detection is enabled on INT2 pin when 6D bit on INT2_CFG (34h) is set to 1.
#define D4D_INT2    0x01

#define CTRL_REG6       0x25 /*  */
// Click interrupt on INT2 pin.
#define I2_CLICK        0x80
// Enable interrupt 1 function on INT2 pin.
#define I2_IA1          0x40
// Enable interrupt 2 function on INT2 pin.
#define I2_IA2          0x20
// Enable boot on INT2 pin.
#define I2_BOOT         0x10
// Enable activity interrupt on INT2 pin.
#define I2_ACT          0x08
// INT1 and INT2 pin polarity. 1: active-low
#define INT_POLARITY    0x02

#define REFERENCE       0x26 /* Reference value for Interrupt generation. */

#define STATUS_REG      0x27 /*  */
// X, Y and Z-axis data overrun. 1: a new set of data has overwritten the previous set
#define ZYXOR   0x80
// Z-axis data overrun. 1: a new data for the Z-axis has overwritten the previous data
#define ZOR     0x40
// Y-axis data overrun. 1: new data for the Y-axis has overwritten the previous data
#define YOR     0x20
// X-axis data overrun. 1: new data for the X-axis has overwritten the previous data
#define XOR     0x10
// X, Y and Z-axis new data available.
#define ZYXDA   0x08
// Z-axis new data available.
#define ZDA     0x04
// Y-axis new data available.
#define YDA     0x02
// X-axis new data available.
#define XDA     0x01

#define OUT_X_L         0x28 /* X-axis acceleration data.  */
#define OUT_X_H         0x29

#define OUT_Y_L         0x2a /* Y-axis acceleration data. */
#define OUT_Y_H         0x2b

#define OUT_Z_L         0x2c /* Z-axis acceleration data. */
#define OUT_Z_H         0x2d
// The value is expressed as two’s complement left-justified.

#define FIFO_CTRL_REG   0x2e /*  */
// FIFO mode selection.
#define FM_BYPASS       0x00
#define FM_FIFO         0x40
#define FM_STREAM       0x80
#define FM_STREAM_FIFO  0xc0
// Trigger selection. 0: trigger event allows triggering signal on INT1 1: INT2
#define TR              0x20
//
#define FTH_MSK         0x1f
#define FTH_SET(n)      (n&FTH_MSK)

#define FIFO_SRC_REG    0x2f /*  */
// WTM bit is set high when FIFO content exceeds watermark level
#define WTM         0x80
// OVRN bit is set high when FIFO buffer is full;
#define OVRN_FIFO   0x40
// EMPTY flag is set high when all FIFO samples have been read and FIFO is empty
#define EMPTY       0x20
// always contains the current number of unread samples stored in the FIFO buffer.
#define FSS_MSK     0x1f
#define FSS_GET(n)  (n&FSS_MSK)

#define INT1_CFG        0x30 /*  */
#define INT2_CFG        0x34 /*  */
// And/Or combination of Interrupt events.
#define AOI     0x80
// 6 direction detection function enabled.
#define F6D     0x40
// Enable interrupt generation on Z high event or on Direction recognition.
#define ZHIE    0x20
// Enable interrupt generation on Z low event or on Direction recognition.
#define ZLIE    0x10
// Enable interrupt generation on Y high event or on Direction recognition.
#define YHIE    0x08
// Enable interrupt generation on Y low event or on Direction recognition.
#define YLIE    0x04
// Enable interrupt generation on X high event or on Direction recognition.
#define XHIE    0x02
// Enable interrupt generation on X low event or on Direction recognition.
#define XLIE    0x01

#define INT1_SRC        0x31 /*  */
#define INT2_SRC        0x35 /*  */
// Interrupt active. 1: one or more interrupts have been generated
#define IA  0x40
// 1: Z high event has occurred
#define ZH  0x20
// 1: Z low event has occurred
#define ZL  0x10
// 1: Y high event has occurred
#define YH  0x08
// 1: Y low event has occurred
#define YL  0x04
// 1: X high event has occurred
#define XH  0x02
// 1: X low event has occurred
#define XL  0x01

#define INT1_THS        0x32 /* Interrupt 1 threshold. */
#define INT2_THS        0x36 /* Interrupt 2 threshold. */
#define INT1_THS_MSK    0x7f

#define INT1_DURATION   0x33 /* minimum duration of the Interrupt 1 event to be recognized. */
#define INT2_DURATION   0x37 /* minimum duration of the Interrupt 2 event to be recognized. */
// Duration time is measured in N/ODR, where N is the content of the duration register.
#define D_MSK   0x7f

#define CLICK_CFG       0x38 /*  */
// Enable interrupt double click on Z-axis.
#define ZD  0x20
// Enable interrupt single click on Z-axis.
#define ZS  0x10
// Enable interrupt double click on Y-axis.
#define YD  0x08
// Enable interrupt single click on Y-axis.
#define YS  0x04
// Enable interrupt double click on X-axis.
#define XD  0x02
// Enable interrupt single click on X-axis.
#define XS  0x01

#define CLICK_SRC       0x39 /*  */
// Interrupt active. 1: one or more interrupts have been generated
#define CIA     0x40
// Double-click enable.
#define DCLICK  0x20
// Single-click enable.
#define SCLICK  0x10
// Click sign. 0: positive detection, 1: negative detection
#define SIGN    0x08
// Z click detection.
#define Z       0x04
// Y click detection.
#define Y       0x02
// X click detection.
#define X       0x01

#define CLICK_THS       0x3a /*  */
// If the LIR_Click bit is not set, the int is kept high for the duration of the latency window.
#define LIR_CLICK   0x80
// Click threshold.
#define THS_MSK     0x7f
#define THS_SET(n)  (n&THS_MSK)

#define TIME_LIMIT      0x3b /* Click time limit. */
#define TLI_MSK 0x7f

#define TIME_LATENCY    0x3c /* Click time latency. */

#define TIME_WINDOW     0x3d /* Click time window */

#define ACT_THS         0x3e /* Sleep-to-wake, return-to-sleep activation threshold in low-power mode */
#define ACTH_MSK    0x7f

#define ACT_DUR         0x3f /* Sleep-to-wake, return-to-sleep duration */
// 1 LSb = (8*1[LSb]+1)/ODR

#endif
