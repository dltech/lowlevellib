#ifndef H_MEMORYMAP
#define H_MEMORYMAP
/*
 * LPC1768 register definitions. Memorymap.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOT_ROM_BASE               0x1fff0000
#define GPIO_BASE                   0x2009c000
#define WDT_BASE                    0x40000000
#define TIMER0_BASE                 0x40004000
#define TIMER1_BASE                 0x40008000
#define UART0_BASE                  0x4000c000
#define UART1_BASE                  0x40010000
#define PWM1_BASE                   0x40018000
#define I2C0_BASE                   0x4001c000
#define SPI_BASE                    0x40020000
#define RTC_BASE                    0x40024000
#define GPIO_INT_BASE               0x40028000
#define PIN_BASE                    0x4002c000
#define SSP1_BASE                   0x40030000
#define ADC_BASE                    0x40034000
#define CAN_ACC_FILTER_RAM_BASE     0x40038000
#define CAN_ACC_FILTER_REGS_BASE    0x4003c000
#define CAN_COMMON_BASE             0x40040000
#define CAN1_BASE                   0x40044000
#define CAN2_BASE                   0x40048000
#define I2C1_BASE                   0x4005c000
#define SSP0_BASE                   0x40088000
#define DAC_BASE                    0x4008c000
#define TIMER2_BASE                 0x40090000
#define TIMER3_BASE                 0x40094000
#define UART2_BASE                  0x40098000
#define UART3_BASE                  0x4009c000
#define I2C2_BASE                   0x400a0000
#define I2S_BASE                    0x400a8000
#define INTERRUPT_BASE              0x400b0000
#define PWM_BASE                    0x400b8000
#define QUADRATURE_ENC_BASE         0x400bc000
#define SYSTEM_BASE                 0x400fc000
#define ETHERNET_BASE               0x50000000
#define GPDMA_BASE                  0x50004000
#define USB_BASE                    0x5000c000

#endif
