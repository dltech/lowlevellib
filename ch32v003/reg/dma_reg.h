#ifndef H_DMA_REG
#define H_DMA_REG
/*
 * CH32V003 alternative library. Direct Memory Access Control (DMA)
 * module registers.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* DMA interrupt status register */
#define DMA_INTFR     MMIO32(DMA_BASE + 0x00)
// Transmission error flag for channel x
#define TEIF1   0x00000008
#define TEIF2   0x00000080
#define TEIF3   0x00000800
#define TEIF4   0x00008000
#define TEIF5   0x00080000
#define TEIF6   0x00800000
#define TEIF7   0x08000000
// Transmission halfway flag for channel x
#define HTIF1   0x00000004
#define HTIF2   0x00000040
#define HTIF3   0x00000400
#define HTIF4   0x00004000
#define HTIF5   0x00040000
#define HTIF6   0x00400000
#define HTIF7   0x04000000
// Transmission completion flag for channel x
#define TCIF1   0x00000002
#define TCIF2   0x00000020
#define TCIF3   0x00000200
#define TCIF4   0x00002000
#define TCIF5   0x00020000
#define TCIF6   0x00200000
#define TCIF7   0x02000000
// Global interrupt flag for channel x
#define GIF1    0x00000001
#define GIF2    0x00000010
#define GIF3    0x00000100
#define GIF4    0x00001000
#define GIF5    0x00010000
#define GIF6    0x00100000
#define GIF7    0x01000000

/* DMA interrupt flag clear register */
#define DMA_INTFCR     MMIO32(DMA_BASE + 0x04)
// Clear the transmission error flag for channel x
#define CTEIF1  0x00000008
#define CTEIF2  0x00000080
#define CTEIF3  0x00000800
#define CTEIF4  0x00008000
#define CTEIF5  0x00080000
#define CTEIF6  0x00800000
#define CTEIF7  0x08000000
// Clear the transmission halfway flag for channel x
#define CHTIF1  0x00000004
#define CHTIF2  0x00000040
#define CHTIF3  0x00000400
#define CHTIF4  0x00004000
#define CHTIF5  0x00040000
#define CHTIF6  0x00400000
#define CHTIF7  0x04000000
// Clear the transmission completion flag for channel x
#define CTCIF1  0x00000002
#define CTCIF2  0x00000020
#define CTCIF3  0x00000200
#define CTCIF4  0x00002000
#define CTCIF5  0x00020000
#define CTCIF6  0x00200000
#define CTCIF7  0x02000000
// Clear the global interrupt flag for channel x
#define CGIF1   0x00000001
#define CGIF2   0x00000010
#define CGIF3   0x00000100
#define CGIF4   0x00001000
#define CGIF5   0x00010000
#define CGIF6   0x00100000
#define CGIF7   0x01000000

/* DMA channel n configuration register */
#define DMA_CFGR1     MMIO32(DMA_BASE + 0x08)
#define DMA_CFGR2     MMIO32(DMA_BASE + 0x1c)
#define DMA_CFGR3     MMIO32(DMA_BASE + 0x30)
#define DMA_CFGR4     MMIO32(DMA_BASE + 0x44)
#define DMA_CFGR5     MMIO32(DMA_BASE + 0x58)
#define DMA_CFGR6     MMIO32(DMA_BASE + 0x6c)
#define DMA_CFGR7     MMIO32(DMA_BASE + 0x80)
// Memory-to-memory mode enable.
#define MEM2MEM         0x4000
// Channel priority setting.
#define PL_LOW          0x0000
#define PL_MEDIUM       0x1000
#define PL_HIGH         0x2000
#define PL_VERY_HIGH    0x3000
// Memory address data width setting.
#define MSIZE8BIT       0x0000
#define MSIZE16BIT      0x0400
#define MSIZE32BIT      0x0800
// Peripheral address data width setting.
#define PSIZE8BIT       0x0000
#define PSIZE16BIT      0x0100
#define PSIZE32BIT      0x0200
// Memory address incremental incremental mode enable.
#define MINC            0x0080
// Peripheral address incremental incremental mode enable.
#define PINC            0x0040
// DMA channel cyclic mode enable.
#define CIRC            0x0020
// Data transfer direction.
#define TEIE            0x0010
// Transmission error interrupt enable control.
#define TEIE            0x0008
// Transmission over half interrupt enable control.
#define HTIE            0x0004
// Transmission completion interrupt enable control.
#define TCIE            0x0002
// Channel enable control.
#define DMA_EN          0x0001

/* DMA channel n number of data register */
#define DMA_CNTR1     MMIO32(DMA_BASE + 0x0c)
#define DMA_CNTR2     MMIO32(DMA_BASE + 0x20)
#define DMA_CNTR3     MMIO32(DMA_BASE + 0x34)
#define DMA_CNTR4     MMIO32(DMA_BASE + 0x48)
#define DMA_CNTR5     MMIO32(DMA_BASE + 0x5c)
#define DMA_CNTR6     MMIO32(DMA_BASE + 0x70)
#define DMA_CNTR7     MMIO32(DMA_BASE + 0x84)
// Number of data transfers, range 0-65535.
#define NDT_MSK 0xffff

/* DMA channel n peripheral address register */
#define DMA_PADDR1     MMIO32(DMA_BASE + 0x10)
#define DMA_PADDR2     MMIO32(DMA_BASE + 0x24)
#define DMA_PADDR3     MMIO32(DMA_BASE + 0x38)
#define DMA_PADDR4     MMIO32(DMA_BASE + 0x4c)
#define DMA_PADDR5     MMIO32(DMA_BASE + 0x60)
#define DMA_PADDR6     MMIO32(DMA_BASE + 0x74)
#define DMA_PADDR7     MMIO32(DMA_BASE + 0x88)
// Peripheral base address

/* DMA channel n memory address register */
#define DMA_MADDR1     MMIO32(DMA_BASE + 0x14)
#define DMA_MADDR2     MMIO32(DMA_BASE + 0x28)
#define DMA_MADDR3     MMIO32(DMA_BASE + 0x3c)
#define DMA_MADDR4     MMIO32(DMA_BASE + 0x50)
#define DMA_MADDR5     MMIO32(DMA_BASE + 0x64)
#define DMA_MADDR6     MMIO32(DMA_BASE + 0x78)
#define DMA_MADDR7     MMIO32(DMA_BASE + 0x8c)
// The memory data address

#endif
