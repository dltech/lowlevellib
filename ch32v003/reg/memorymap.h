#ifndef H_MEMORYMAP
#define H_MEMORYMAP
/*
 * CH32V003 alternative library, Memorymap (map of register addresses).
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// peripherial register base addresses
#define TIM2_BASE   0x40000000
#define WWDG_BASE   0x40002c00
#define IWDG_BASE   0x40003000
#define I2C_BASE    0x40005400
#define PWR_BASE    0x40007000
#define AFIO_BASE   0x40010000
#define EXTI_BASE   0x40010400
#define PORTA_BASE  0x40010800
#define PORTC_BASE  0x40011000
#define PORTD_BASE  0x40011400
#define ADC_BASE    0x40012400
#define TIM1_BASE   0x40012c00
#define SPI_BASE    0x40013000
#define USART_BASE  0x40013800
#define DMA_BASE    0x40020000
#define RCC_BASE    0x40021000
#define FLASH_BASE  0x40022000
#define EXTEND_BASE 0x40023800
#define PFIC_BASE   0xe000e000
#define STK_BASE    0xe000f000

// flash addresses
#define CODE_FLASH  0x08000000
#define BOOT_FLASH  0x1ffff000
#define ESIG_BASE   0x1ffff700

#define  MMIO32(addr)		(*(volatile uint32_t *)(addr))

#endif
