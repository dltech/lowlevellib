#ifndef H_ADC_REG
#define H_ADC_REG
/*
 * CH32V003 alternative library. Analog-to-digital Converter (ADC)
 * module registers.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* ADC status register */
#define ADC_STATR   MMIO32(ADC_BASE + 0x00)
// Rule channel transition start state.
#define STRT    0x10
// Injection channel conversion start state.
#define JSTRT   0x08
// Injection into the end state of the channel group conversion.
#define JSTRT   0x04
// Conversion end state.
#define EOC     0x02
// Analog watchdog flag bit.
#define AWD     0x01

/* ADC control register 1 */
#define ADC_CTLR1   MMIO32(ADC_BASE + 0x04)
// Calibration voltage selection
#define CALVOL2_4AVDD   0x2000000
#define CALVOL3_4AVDD   0x4000000
// Analog watchdog function enable bit on the rule channel.
#define AWDEN           0x0800000
// Analog watchdog function enable bit on the injection channel.
#define JAWDEN          0x0400000
// Number of rule channels to be converted after external triggering.
#define DISCNUM1CH      0x0000000
#define DISCNUM2CH      0x0002000
#define DISCNUM3CH      0x0004000
#define DISCNUM4CH      0x0006000
#define DISCNUM5CH      0x0008000
#define DISCNUM6CH      0x000a000
#define DISCNUM7CH      0x000c000
#define DISCNUM8CH      0x000e000
#define DISCNUM_MSK     0x000e000
// Inject the intermittent mode enable bit on the channel.
#define JDISCEN         0x0001000
// Intermittent mode enable bit on rule channel.
#define DISCEN          0x0000800
// 1: Enable automatic injection channel group switching.
#define JAUTO           0x0000400
// 1: Use an analog watchdog on a single channel (AWDCH[4:0] selection).
#define AWDSGL          0x0000200
// Scan mode enable bit.
#define SCAN            0x0000100
// Inject the channel group end-of-conversion interrupt enable bit.
#define JEOCIE          0x0000080
// Analog watchdog interrupt enable bit.
#define AWDIE           0x0000040
// End of conversion (rule or injection channel group) interrupt enable bit.
#define EOCIE           0x0000020
// Analog watchdog channel selection bits.
#define AWDCH0          0x0000000
#define AWDCH1          0x0000001
#define AWDCH2          0x0000002
#define AWDCH3          0x0000003
#define AWDCH4          0x0000004
#define AWDCH5          0x0000005
#define AWDCH6          0x0000006
#define AWDCH7          0x0000007
#define AWDCH8          0x0000008
#define AWDCH9          0x0000009
#define AWDCH10         0x000000a
#define AWDCH11         0x000000b
#define AWDCH12         0x000000c
#define AWDCH13         0x000000d
#define AWDCH14         0x000000e
#define AWDCH15         0x000000f
#define AWDCH_MSK       0x000001f

/* ADC control register 2 */
#define ADC_CTLR2   MMIO32(ADC_BASE + 0x08)
// 1: Initiate rule channel conversion.
#define SWSTART             0x400000
// 1: initiates an injection channel transition;
#define JSWSTART            0x200000
// External trigger transition mode enable for the rule channel.
#define EXTTRIG             0x100000
// External trigger event selection for initiating rule channel conversion.
#define EXTSEL_TRGO_TIM1    0x000000
#define EXTSEL_CC1_TIM1     0x020000
#define EXTSEL_CC2_TIM1     0x040000
#define EXTSEL_TRGO_TIM2    0x060000
#define EXTSEL_CC1_TIM2     0x080000
#define EXTSEL_CC2_TIM2     0x0a0000
#define EXTSEL_PD3_PC2      0x0c0000
#define EXTSEL_SWSTART      0x0e0000
// External trigger transition mode enable for the injected channel.
#define JEXTTRIG            0x008000
// Ext trigger event selection for initiating injection channel conversion.
#define JEXTSEL_CC3_TIM1    0x000000
#define JEXTSEL_CC4_TIM1    0x001000
#define JEXTSEL_CC3_TIM2    0x002000
#define JEXTSEL_CC4_TIM2    0x003000
#define JEXTSEL_PD1_PA2     0x006000
#define JEXTSEL_JSWSTART    0x007000
// Data alignment.
#define ALIGN               0x000800
// Direct Memory Access (DMA) mode enable.
#define DMA                 0x000100
// 1: Initialization of the calibration registers.
#define RSTCAL              0x000008
// 1: Start of calibration.
#define CAL                 0x000004
// Continuous conversion enable.
#define CONT                0x000002
// On/off A/D converter
#define ADON                0x000001

/* ADC sample time register n */
#define ADC_SAMPTR1 MMIO32(ADC_BASE + 0x0c)
#define ADC_SAMPTR2 MMIO32(ADC_BASE + 0x10)
// sample time configuration for channel x.
#define SMP3CYCLES(x)   (0x0 << (3*x))
#define SMP9CYCLES(x)   (0x1 << (3*x))
#define SMP15CYCLES(x)  (0x2 << (3*x))
#define SMP30CYCLES(x)  (0x3 << (3*x))
#define SMP43CYCLES(x)  (0x4 << (3*x))
#define SMP57CYCLES(x)  (0x5 << (3*x))
#define SMP73CYCLES(x)  (0x6 << (3*x))
#define SMP241CYCLES(x) (0x7 << (3*x))

/* ADC injected channel data offset register 1 */
#define ADC_IOFR1   MMIO32(ADC_BASE + 0x14)
#define ADC_IOFR2   MMIO32(ADC_BASE + 0x18)
#define ADC_IOFR3   MMIO32(ADC_BASE + 0x1c)
#define ADC_IOFR4   MMIO32(ADC_BASE + 0x20)
// The data offset value of the injected channel x.
#define JOFFSET_MSK 0x3ff

/* ADC watchdog high threshold register */
#define ADC_WDHTR   MMIO32(ADC_BASE + 0x24)
// Analog watchdog high threshold setting value.
#define HT_MSK  0x3ff

/* ADC watchdog low threshold register */
#define ADC_WDLTR   MMIO32(ADC_BASE + 0x28)
// Analog watchdog low threshold setting value.
#define LT_MSK  0x3ff

/* ADC regular sequence register 1 */
#define ADC_RSQR1   MMIO32(ADC_BASE + 0x2c)
#define ADC_RSQR2   MMIO32(ADC_BASE + 0x30)
#define ADC_RSQR3   MMIO32(ADC_BASE + 0x34)
// Number of channels to be converted in a regular conversion sequence.
#define L1          0x000000
#define L2          0x100000
#define L3          0x200000
#define L4          0x300000
#define L5          0x400000
#define L6          0x500000
#define L7          0x600000
#define L8          0x700000
#define L9          0x800000
#define L10         0x900000
#define L11         0xa00000
#define L12         0xb00000
#define L13         0xc00000
#define L14         0xd00000
#define L15         0xe00000
#define L16         0xf00000
#define L_SFT       20
#define L_MSK       0xf00000
#define L(x)        (x << L_SFT)
// The number of the nth conversion channel in the rule sequence
#define SQ0CONV(x)  (0x0 << (5*(x-1)))
#define SQ1CONV(x)  (0x1 << (5*(x-1)))
#define SQ2CONV(x)  (0x2 << (5*(x-1)))
#define SQ3CONV(x)  (0x3 << (5*(x-1)))
#define SQ4CONV(x)  (0x4 << (5*(x-1)))
#define SQ5CONV(x)  (0x5 << (5*(x-1)))
#define SQ6CONV(x)  (0x6 << (5*(x-1)))
#define SQ7CONV(x)  (0x7 << (5*(x-1)))
#define SQ8CONV(x)  (0x8 << (5*(x-1)))
#define SQ9CONV(x)  (0x9 << (5*(x-1)))

/* ADC injected sequence register */
#define ADC_ISQR    MMIO32(ADC_BASE + 0x38)
// Inject the number of ch to be conv in the channel conversion seq.
#define JL1         0x000000
#define JL2         0x100000
#define JL3         0x200000
#define JL4         0x300000
#define JL_MSK      0x300000
#define JL_SFT      20
#define JL_SET(x)   ((x<<JL_SFT)&JL_MSK)
#define JL_GET      ((ADC_ISQR>>JL_SFT)&0x3)
// The number of the nth conversion channel in the injection seq (0-9).
#define JSQ0CONV(x) (0x0 << (5*(x-1)))
#define JSQ1CONV(x) (0x1 << (5*(x-1)))
#define JSQ2CONV(x) (0x2 << (5*(x-1)))
#define JSQ3CONV(x) (0x3 << (5*(x-1)))
#define JSQ4CONV(x) (0x4 << (5*(x-1)))
#define JSQ5CONV(x) (0x5 << (5*(x-1)))
#define JSQ6CONV(x) (0x6 << (5*(x-1)))
#define JSQ7CONV(x) (0x7 << (5*(x-1)))
#define JSQ8CONV(x) (0x8 << (5*(x-1)))
#define JSQ9CONV(x) (0x9 << (5*(x-1)))

/* ADC injected data register 1 */
#define ADC_IDATAR1 MMIO32(ADC_BASE + 0x3c)
#define ADC_IDATAR2 MMIO32(ADC_BASE + 0x40)
#define ADC_IDATAR3 MMIO32(ADC_BASE + 0x44)
#define ADC_IDATAR4 MMIO32(ADC_BASE + 0x48)
// Injection of channel conversion data (data l-aligned or r-aligned).

/* ADC regular data register */
#define ADC_RDATAR  MMIO32(ADC_BASE + 0x4c)
// Rule channel conversion data (data left-aligned or right-aligned)

/* ADC delayed data register */
#define ADC_DLYR    MMIO32(ADC_BASE + 0x50)
// External trigger source delay selection
#define DLYSRC      0x200
// External trigger delay data, delay time configuration
#define DLYVLU_MSK  0x1ff

#endif
