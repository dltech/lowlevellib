#ifndef H_EXTEN_REG
#define H_EXTEN_REG
/*
 * CH32V003 alternative library. The EXTEND extended configuration unit
 * (EXTEND_CTR register).
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Configure extended control registers */
#define EXTEN_CTR   MMIO32(EXTEND_BASE)
// OPA positive end channel selection
#define OPA_PSEL    0x40000
// OPA negative end channel selection
#define OPA_NSEL    0x20000
// OPA Enable
#define OPA_EN      0x10000
// Core voltage modes.
#define LDOTRIM     0x00400
// LOCKUP reset flag.
#define LKUPRST     0x00080
// LOCKUP monitoring function.
#define LKUPEN      0x00040

#endif
