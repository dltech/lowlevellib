#ifndef H_GPIO_REG
#define H_GPIO_REG
/*
 * CH32V003 alternative library. GPIO and Alternate Function (GPIO/AFIO)
 * module registers.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* port configuration register low */
#define GPIOA_CFGLR     MMIO32(PORTA_BASE + 0x00)
#define GPIOC_CFGLR     MMIO32(PORTC_BASE + 0x00)
#define GPIOD_CFGLR     MMIO32(PORTD_BASE + 0x00)
#define GPIOn_CFGLR(x)  MMIO32(x + 0x00)
// port x configuration in input mode
#define CNF_ANALOG(n)   0x00000000
#define CNF_FLOATING(n) ((uint32_t)(0x4 << (n*4)))
#define CNF_PUPD(n)     ((uint32_t)(0x8 << (n*4)))
#define CNF0_ANALOG     0x00000000
#define CNF0_FLOATING   0x00000004
#define CNF0_PUPD       0x00000008
#define CNF1_ANALOG     0x00000000
#define CNF1_FLOATING   0x00000040
#define CNF1_PUPD       0x00000080
#define CNF2_ANALOG     0x00000000
#define CNF2_FLOATING   0x00000400
#define CNF2_PUPD       0x00000800
#define CNF3_ANALOG     0x00000000
#define CNF3_FLOATING   0x00004000
#define CNF3_PUPD       0x00008000
#define CNF4_ANALOG     0x00000000
#define CNF4_FLOATING   0x00040000
#define CNF4_PUPD       0x00080000
#define CNF5_ANALOG     0x00000000
#define CNF5_FLOATING   0x00400000
#define CNF5_PUPD       0x00800000
#define CNF6_ANALOG     0x00000000
#define CNF6_FLOATING   0x04000000
#define CNF6_PUPD       0x08000000
#define CNF7_ANALOG     0x00000000
#define CNF7_FLOATING   0x40000000
#define CNF7_PUPD       0x80000000
// port x configuration in output mode
#define CNF_PUSH_PULL(n)        0x00000000
#define CNF_OPEN_DRAIN(n)       ((uint32_t)(0x4 << (n*4)))
#define CNF_AF_PUSH_PULL(n)     ((uint32_t)(0x8 << (n*4)))
#define CNF_AF_OPEN_DRAIN(n)    ((uint32_t)(0xc << (n*4)))
#define CNF0_PUSH_PULL          0x00000000
#define CNF0_OPEN_DRAIN         0x00000004
#define CNF0_AF_PUSH_PULL       0x00000008
#define CNF0_AF_OPEN_DRAIN      0x0000000c
#define CNF1_PUSH_PULL          0x00000000
#define CNF1_OPEN_DRAIN         0x00000040
#define CNF1_AF_PUSH_PULL       0x00000080
#define CNF1_AF_OPEN_DRAIN      0x000000c0
#define CNF2_PUSH_PULL          0x00000000
#define CNF2_OPEN_DRAIN         0x00000400
#define CNF2_AF_PUSH_PULL       0x00000800
#define CNF2_AF_OPEN_DRAIN      0x00000c00
#define CNF3_PUSH_PULL          0x00000000
#define CNF3_OPEN_DRAIN         0x00004000
#define CNF3_AF_PUSH_PULL       0x00008000
#define CNF3_AF_OPEN_DRAIN      0x0000c000
#define CNF4_PUSH_PULL          0x00000000
#define CNF4_OPEN_DRAIN         0x00040000
#define CNF4_AF_PUSH_PULL       0x00080000
#define CNF4_AF_OPEN_DRAIN      0x000c0000
#define CNF5_PUSH_PULL          0x00000000
#define CNF5_OPEN_DRAIN         0x00400000
#define CNF5_AF_PUSH_PULL       0x00800000
#define CNF5_AF_OPEN_DRAIN      0x00c00000
#define CNF6_PUSH_PULL          0x00000000
#define CNF6_OPEN_DRAIN         0x04000000
#define CNF6_AF_PUSH_PULL       0x08000000
#define CNF6_AF_OPEN_DRAIN      0x0c000000
#define CNF7_PUSH_PULL          0x00000000
#define CNF7_OPEN_DRAIN         0x40000000
#define CNF7_AF_PUSH_PULL       0x80000000
#define CNF7_AF_OPEN_DRAIN      0xc0000000
#define CNF_MASK(n)             ((uint32_t)(0xc << (n*4)))
// Port mode bits
#define MODE_INPUT(n)       0x00000000
#define MODE_OUTPUT10(n)    ((uint32_t)(0x1 << (n*4)))
#define MODE_OUTPUT2(n)     ((uint32_t)(0x2 << (n*4)))
#define MODE_OUTPUT30(n)    ((uint32_t)(0x3 << (n*4)))
#define MODE0_INPUT         0x00000000
#define MODE0_OUTPUT10      0x00000001
#define MODE0_OUTPUT2       0x00000002
#define MODE0_OUTPUT30      0x00000003
#define MODE1_INPUT         0x00000000
#define MODE1_OUTPUT10      0x00000010
#define MODE1_OUTPUT2       0x00000020
#define MODE1_OUTPUT30      0x00000030
#define MODE2_INPUT         0x00000000
#define MODE2_OUTPUT10      0x00000100
#define MODE2_OUTPUT2       0x00000200
#define MODE2_OUTPUT30      0x00000300
#define MODE3_INPUT         0x00000000
#define MODE3_OUTPUT10      0x00001000
#define MODE3_OUTPUT2       0x00002000
#define MODE3_OUTPUT30      0x00003000
#define MODE4_INPUT         0x00000000
#define MODE4_OUTPUT10      0x00010000
#define MODE4_OUTPUT2       0x00020000
#define MODE4_OUTPUT30      0x00030000
#define MODE5_INPUT         0x00000000
#define MODE5_OUTPUT10      0x00100000
#define MODE5_OUTPUT2       0x00200000
#define MODE5_OUTPUT30      0x00300000
#define MODE6_INPUT         0x00000000
#define MODE6_OUTPUT10      0x01000000
#define MODE6_OUTPUT2       0x02000000
#define MODE6_OUTPUT30      0x03000000
#define MODE7_INPUT         0x00000000
#define MODE7_OUTPUT10      0x10000000
#define MODE7_OUTPUT2       0x20000000
#define MODE7_OUTPUT30      0x30000000
#define MODE_MASK(n)        ((uint32_t)(0x3 << (n*4)))

/* port input data register */
#define GPIOA_INDR      MMIO32(PORTA_BASE + 0x08)
#define GPIOC_INDR      MMIO32(PORTC_BASE + 0x08)
#define GPIOD_INDR      MMIO32(PORTD_BASE + 0x08)
#define GPIOn_INDR(x)   MMIO32(x + 0x08)
// pin bit definitions, they are the same for IDR, ODR, BSRR, BRR, LCKR
#define GPIO0   0x0001
#define GPIO1   0x0002
#define GPIO2   0x0004
#define GPIO3   0x0008
#define GPIO4   0x0010
#define GPIO5   0x0020
#define GPIO6   0x0040
#define GPIO7   0x0080
#define GPIOALL 0xffff
#define GPIO(n) (0x1 << n)
// IDR Port input data
#define IDR(n)  (1 << n)

/* port output data register */
#define GPIOA_OUTDR     MMIO32(PORTA_BASE + 0x0c)
#define GPIOC_OUTDR     MMIO32(PORTC_BASE + 0x0c)
#define GPIOD_OUTDR     MMIO32(PORTD_BASE + 0x0c)
#define GPIOn_OUTDR(x)  MMIO32(x + 0x0c)
// ODR Port output data
#define ODR(n)  (1 << n)

/* port set/reset register */
#define GPIOA_BSHR      MMIO32(PORTA_BASE + 0x10)
#define GPIOC_BSHR      MMIO32(PORTC_BASE + 0x10)
#define GPIOD_BSHR      MMIO32(PORTD_BASE + 0x10)
#define GPIOn_BSHR(x)   MMIO32(x + 0x10)
// Port reset bit
#define BR(n)   ((uint32_t)(1 << n+16))
// Port set bit
#define BS(n)   ((uint32_t)(1 << n))

/* port reset register */
#define GPIOA_BCR       MMIO32(PORTA_BASE + 0x14)
#define GPIOC_BCR       MMIO32(PORTC_BASE + 0x14)
#define GPIOD_BCR       MMIO32(PORTD_BASE + 0x14)
#define GPIOn_BCR(x)    MMIO32(x + 0x14)

/* port configuration lock register */
#define GPIOA_LCKR      MMIO32(PORTA_BASE + 0x18)
#define GPIOC_LCKR      MMIO32(PORTC_BASE + 0x18)
#define GPIOD_LCKR      MMIO32(PORTD_BASE + 0x18)
#define GPIOn_LCKR(x)   MMIO32(x + 0x18)
// Lock key
#define LCKK    0x00010000
// Port lock bit
#define LCK_GPIO(n)   ((uint32_t)(1 << n))

/*** AFIO Register Description ***/

/* port configuration lock register */
#define AFIO_PCFR1  MMIO32(AFIO_BASE + 0x04)
// SWD port off
#define SWCFG_ON                                                    0x0000000
#define SWCFG_OFF                                                   0x4000000
// Control timer 1 channel 1 selection
#define TIM1_IREMAP                                                 0x0800000
// I2C1 remapping high bit
#define I2C1REMAP_SCL_PC2_SDA_PC1                                   0x0000000
#define I2C1REMAP_SCL_PD1_SDA_PD0                                   0x0000002
#define I2C1REMAP_SCL_PC5_SDA_PC6                                   0x0400000
// USART1 mapping configuration high
#define USART1_RM1_CK_PD4_TX_PD5_RX_PD6_CTS_PD3_RTS_PC2             0x0000000
#define USART1_RM1_CK_PD7_TX_PD0_RX_PD1_CTS_PC3_RTS_PC2_SWRX_PD0    0x0000004
#define USART1_RM1_CK_PD7_TX_PD6_RX_PD5_CTS_PC6_RTS_PC7_SWRX_PD6    0x0200000
#define USART1_RM1_CK_PC5_TX_PC0_RX_PC1_CTS_PC6_RTS_PC7_SWRX_PC0    0x0200004
// Remap bit for ADC external trigger rule conversion.
#define ADC_ETRGREG_RM                                              0x0040000
// Remap bit for ADC external trigger rule conversion.
#define ADC_ETRGINJ_RM                                              0x0020000
// Pin PA1 & PA2 remapping bit
#define PA12_RM                                                     0x0008000
// Remap bits for timer 2.
#define TIM2_RM_CH1_ETR_PD4_CH2_PD3_CH3_PC0_CH4_PD7                 0x0000000
#define TIM2_RM_CH1_ETR_PC5_CH2_PC2_CH3_PD2_CH4_PC1                 0x0000100
#define TIM2_RM_CH1_ETR_PC1_CH2_PD3_CH3_PC0_CH4_PD7                 0x0000200
#define TIM2_RM_CH1_ETR_PC1_CH2_PC7_CH3_PD6_CH4_PD5                 0x0000300
// Remap bits for timer 1.
#define TIM1_RM_DEFAULT                                             0x0000000
#define TIM1_RM_PARTIAL1                                            0x0000040
#define TIM1_RM_PARTIAL2                                            0x0000080
#define TIM1_RM_COMPLETE                                            0x00000c0
// Remapping of SPI1.
#define SPI1_RM                                                     0x0000001

/* port configuration lock register */
#define AFIO_EXTICR MMIO32(AFIO_BASE + 0x08)
// external interrupt input pin configuration bit.
#define EXTI7_PA    0x0000
#define EXTI7_PC    0x8000
#define EXTI7_PD    0xc000
#define EXTI6_PA    0x0000
#define EXTI6_PC    0x2000
#define EXTI6_PD    0x3000
#define EXTI5_PA    0x0000
#define EXTI5_PC    0x0800
#define EXTI5_PD    0x0c00
#define EXTI4_PA    0x0000
#define EXTI4_PC    0x0200
#define EXTI4_PD    0x0300
#define EXTI3_PA    0x0000
#define EXTI3_PC    0x0080
#define EXTI3_PD    0x00c0
#define EXTI2_PA    0x0000
#define EXTI2_PC    0x0020
#define EXTI2_PD    0x0030
#define EXTI1_PA    0x0000
#define EXTI1_PC    0x0008
#define EXTI1_PD    0x000c
#define EXTI0_PA    0x0000
#define EXTI0_PC    0x0002
#define EXTI0_PD    0x0003
#define EXTIx_PA(x) 0x0000
#define EXTIx_PC(x) (0x2<<(x*2))
#define EXTIx_PD(x) (0x3<<(x*2))

#endif
