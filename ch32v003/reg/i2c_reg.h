#ifndef H_I2C_REG
#define H_I2C_REG
/*
 * CH32V003 alternative library. The Internal Integrated Circuit Bus
 * (I2C) registers.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* I2C control register 1 */
#define I2C_CTLR1   MMIO32(I2C_BASE + 0x00)
// Software reset.
#define SWRST       0x8000
// Packet error checking bit.
#define PEC         0x1000
// ACK and PEC position setting bits.
#define POS         0x0800
// Acknowledge enable.
#define ACK         0x0400
// Stop generation bit.
#define STOP        0x0200
// Start generation.
#define START       0x0100
// Clock stretching disable bit.
#define NOSTRETCH   0x0080
// General call enable bit.
#define ENGC        0x0040
// PEC enable bit
#define ENPEC       0x0020
// I2C peripheral enable bit.
#define PE          0x0001

/* I2C control register 2 */
#define I2C_CTLR2   MMIO32(I2C_BASE + 0x04)
// DMA last transfer bit.
#define LAST            0x1000
// DMA requests enable bit.
#define DMAEN           0x0800
// Buffer interrupt enable bit.
#define ITBUFEN         0x0400
// Event interrupt enable bit.
#define ITEVTEN         0x0200
// Error interrupt enable bit.
#define ITERREN         0x0100
// The I2C module clock frequency field.
#define FREQ_MSK        0x003f
#define I2C_FREQ_SET    ((F_SYS/1000000)&FREQ_MSK)

/* I2C address register 1 */
#define I2C_OADDR1  MMIO32(I2C_BASE + 0x08)
// Address mode.
#define ADDMODE         0x8000
// Interface address
#define ADD_10_MSK      0x03ff
#define ADD_10_SET(x)   (x&ADD_10_MSK)
#define ADD_7_MSK       0x00fe
#define ADD_7_SET(x)    (x&ADD_7_MSK)

/* I2C address register 2 */
#define I2C_OADDR2  MMIO32(I2C_BASE + 0x0c)
// address in dual address mode
#define ADD2_MSK    0xfe
#define ADD2_SET(x) (x&ADD2_MSK)
// Dual address mode enable bit.
#define ENDUAL      0x01

/* I2C data register */
#define I2C_DATAR   MMIO32(I2C_BASE + 0x10)
// rx/tx data
#define DR_MSK  0xff

/* I2C status register 1 */
#define I2C_STAR1   MMIO32(I2C_BASE + 0x14)
// The PEC error flag bit
#define PECERR  0x1000
// Overrun and underrun flag bits.
#define OVR     0x0800
// Acknowledge failure bit.
#define AF      0x0400
// Arbitration lost bit.
#define ARLO    0x0200
// The bus error flag bit.
#define BERR    0x0100
// Data register empty bit.
#define TXE     0x0080
// Data register not empty bit.
#define RXNE    0x0040
// Stop detection bit.
#define STOPF   0x0010
// 10-bit header sent bit.
#define ADD10   0x0008
// Byte transfer finished bit.
#define BTF     0x0004
// Address sent /matched bit.
#define ADDR    0x0002
// Start bit.
#define SB      0x0001

/* I2C status register 2 */
#define I2C_STAR2   MMIO32(I2C_BASE + 0x18)
#define PEC_MSK 0xff00
#define PEC_SFT 8
#define PEC_GET ((I2C_STAR2>>PEC_SFT)&0xff)
// Dual flag.
#define DUALF   0x0080
// General call address bit.
#define GENCALL 0x0010
// Transmitter/receiver bit.
#define TRA     0x0004
// Bus busy bit.
#define BUSY    0x0002
// Master/slave bit.
#define MSL     0x0001

/* I2C clock register */
#define I2C_CKCFGR  MMIO32(I2C_BASE + 0x1c)
// Master mode selection bit.
#define F_S     0x8000
// Duty cycle in the fast mode
#define DUTY    0x4000
// Clock control register in Fm/Sm mode
#define CCR_MSK 0x0fff

#endif
