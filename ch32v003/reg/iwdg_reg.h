#ifndef H_IWDG_REG
#define H_IWDG_REG
/*
 * CH32V003 alternative library. Independent Watchdog (IWDG) module
 * registers.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Control register */
#define IWDG_CTLR   MMIO32(IWDG_BASE + 0x0)
// Operate the key value lock.
#define REG_ACC_KEY     0x5555
#define REG_START_KEY   0xcccc

/* Prescaler register */
#define IWDG_PSCR   MMIO32(IWDG_BASE + 0x4)
// IWDG clock division factor
#define PR_DIV4     0x0
#define PR_DIV8     0x1
#define PR_DIV16    0x2
#define PR_DIV32    0x3
#define PR_DIV64    0x4
#define PR_DIV128   0x5
#define PR_DIV256   0x6

/* Reload register */
#define IWDG_RLDR   MMIO32(IWDG_BASE + 0x8)
// Counter reload value.
#define RL_MSK  0xffffff

/* Status register */
#define IWDG_STATR  MMIO32(IWDG_BASE + 0xc)
// Reload value update flag bit.
#define RVU 0x2
// Clock division factor update flag bit.
#define PVU 0x1

#endif
