#ifndef H_DBG_REG
#define H_DBG_REG
/*
 * CH32V003 alternative library. Debug Support (DBG) registers.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Debug MCU Configuration Register */
#define DBGMCU_CR   0x7C0
// Timer 2 debug stop bit.
#define TIM2_STOP   0x2000
// Timer 1 debug stop bit.
#define TIM1_STOP   0x1000
// WWDG debug stop bit.
#define WWDG_STOP   0x0200
// IWDG debug stop bit.
#define IWDG_STOP   0x0100
// Debug the standby mode bits.
#define STANDBY     0x0004
// Debug sleep mode bits.
#define SLEEP       0x0001

#endif
