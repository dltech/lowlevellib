#ifndef H_SPI_REG
#define H_SPI_REG
/*
 * CH32V003 alternative library. Serial Peripheral Interface (SPI)
 * module registers.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* SPI Control register1 */
#define SPI_CTLR1   MMIO32(SPI_BASE + 0x00)
// Bidirectional data mode enable bit.
#define BIDIMODE    0x8000
// Output enable in bidirectional mode bit.
#define BIDIOE      0x4000
// Hardware CRC checksum enable bit.
#define CRCEN       0x2000
// After the next data transfer.
#define CRCNEXT     0x1000
// Data frame format bit.
#define DFF         0x0800
// The receive-only bit in 2-wire mode.
#define RXONLY      0x0400
// Software slave management bit.
#define SSM         0x0200
// Internal slave select bit.
#define SSI         0x0100
// Frame format control bit.
#define LSBFIRST    0x0080
// SPI enable bit.
#define SPE         0x0040
// Baud rate setting field.
#define BR_DIV2     0x0000
#define BR_DIV4     0x0008
#define BR_DIV8     0x0010
#define BR_DIV16    0x0018
#define BR_DIV32    0x0020
#define BR_DIV64    0x0028
#define BR_DIV128   0x0030
#define BR_DIV256   0x0038
// Master-slave setting bit.
#define MSTR        0x0004
// Clock polarity selection bit.
#define CPOL        0x0002
// Clock phase setting bit.
#define CPHA        0x0001

/* SPI Control register2 */
#define SPI_CTLR2   MMIO32(SPI_BASE + 0x04)
// Tx buffer empty interrupt enable bit.
#define TXEIE   0x80
// RX buffer not empty interrupt enable bit.
#define RXNEIE  0x40
// Error interrupt enable bit.
#define ERRIE   0x20
// SS output enable bit.
#define SSOE    0x04
// Tx buffer DMA enable bit.
#define TXDMAEN 0x02
// Rx buffer DMA enable bit.
#define RXDMAEN 0x01

/* SPI Status register */
#define SPI_STATR   MMIO32(SPI_BASE + 0x08)
// Busy flag.
#define BSY     0x80
// Overrun flag.
#define OVR     0x40
// Mode fault.
#define MODF    0x20
// CRC error flag.
#define CRCERR  0x10
// Underrun flag.
#define UDR     0x08
// Channel side.
#define CHSID   0x04
// Transmit buffer empty.
#define TXE     0x02
// Receive buffer not empty.
#define RXNE    0x01

/* SPI Data register */
#define SPI_DATAR   MMIO32(SPI_BASE + 0x0c)

/* SPI Polynomial register */
#define SPI_CRCR    MMIO32(SPI_BASE + 0x10)
// CRC polynomial.

/* SPI Receive CRC register */
#define SPI_RCRCR   MMIO32(SPI_BASE + 0x14)
// Rx CRC.

/* SPI Transmit CRC register */
#define SPI_TCRCR   MMIO32(SPI_BASE + 0x18)
// Tx CRC.

/* SPI High-speed control register */
#define SPI_HSCR    MMIO32(SPI_BASE + 0x24)
// Read enable in SPI high-speed mode.
#define HSRXEN  0x1

#endif
