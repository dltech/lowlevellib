#ifndef H_TIM_REG
#define H_TIM_REG
/*
 * CH32V003 alternative library. The Advanced-control timer Module registers.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Control register 1 */
#define TIM1_CTLR1      MMIO32(TIM1_BASE + 0x00)
#define TIM2_CTLR1      MMIO32(TIM2_BASE + 0x00)
// The capture level indication is enabled.
#define CAPLVL          0x8000
// Capture value mode configuration.
#define CAPOV           0x4000
// ratio between the (CK_INT) freq, the dead time and the sampling clock
#define CKD_TDTS_1TCK   0x0000
#define CKD_TDTS_2TCK   0x0100
#define CKD_TDTS_4TCK   0x0200
// Auto-reload preload enable bit.
#define ARPE            0x0080
// Central alignment mode selection.
#define CMS_EDGE        0x0000
#define CMS_CENTER1     0x0020
#define CMS_CENTER2     0x0040
#define CMS_CENTER3     0x0060
// Counting direction.
#define DIR             0x0010
// Single pulse mode.
#define OPM             0x0008
// Update request source
#define URS             0x0004
// Disable updates
#define UDIS            0x0002
// Enables the counter.
#define CEN             0x0001

/* Control register 2 */
#define TIM1_CTLR2      MMIO32(TIM1_BASE + 0x04)
#define TIM2_CTLR2      MMIO32(TIM2_BASE + 0x04)
// Output idle state 4.
#define OIS4                0x4000
// Output idle state 3.
#define OIS3N               0x2000
// Output idle state 3
#define OIS3                0x1000
// Output idle state 2
#define OIS2N               0x0800
// Output idle state 2
#define OIS2                0x0400
// Output idle state 1
#define OIS1N               0x0200
// Output idle state 1
#define OIS1                0x0100
// TI1 selection.
#define TI1S                0x0080
// Master mode selection
#define MMS_RESET           0x0000
#define MMS_ENABLE          0x0010
#define MMS_UPDATE          0x0020
#define MMS_COMPARE_PULSE   0x0030
#define MMS_COMPARE_OC1REF  0x0040
#define MMS_COMPARE_OC2REF  0x0050
#define MMS_COMPARE_OC3REF  0x0060
#define MMS_COMPARE_OC4REF  0x0070
// Capture the DMA selection for comparison.
#define CCDS                0x0008
// Compare capture control update selection bits.
#define CCUS                0x0004
// Compare capture preload control bits.
#define CCPC                0x0001

/* Slave mode control register */
#define TIM1_SMCFGR     MMIO32(TIM1_BASE + 0x08)
#define TIM2_SMCFGR     MMIO32(TIM2_BASE + 0x08)
// ETR trigger polarity selection
#define ETP             0x8000
// External clock mode 2 enable selection.
#define ECE             0x4000
// The external trigger signal (ETRP)
#define ETPS_OFF        0x0000
#define ETPS_DIV2       0x1000
#define ETPS_DIV4       0x2000
#define ETPS_DIV8       0x3000
// Externally triggered filtering
#define ETF_MASK        0x0f00
#define ETF_NO_FILTER   0x0000
#define ETF_CK_INT_2    0x0100
#define ETF_CK_INT_4    0x0200
#define ETF_CK_INT_8    0x0300
#define ETF_DTS_DIV2_6  0x0400
#define ETF_DTS_DIV2_8  0x0500
#define ETF_DTS_DIV4_6  0x0600
#define ETF_DTS_DIV4_8  0x0700
#define ETF_DTS_DIV8_6  0x0800
#define ETF_DTS_DIV8_8  0x0900
#define ETF_DTS_DIV16_5 0x0a00
#define ETF_DTS_DIV16_6 0x0b00
#define ETF_DTS_DIV16_8 0x0c00
#define ETF_DTS_DIV32_5 0x0d00
#define ETF_DTS_DIV32_6 0x0e00
#define ETF_DTS_DIV32_8 0x0f00
// Master/slave mode selection.
#define MSM             0x0080
// Trigger selection field
#define TS_ITR0         0x0000
#define TS_ITR1         0x0010
#define TS_ITR2         0x0020
#define TS_ITR3         0x0030
#define TS_TI1F_ED      0x0040
#define TS_TI1FP1       0x0050
#define TS_TI1FP2       0x0060
#define TS_ETRF         0x0070
// Input mode selection field.
#define SMS_INTERNAL    0x0000
#define SMS_ENCODER1    0x0001
#define SMS_ENCODER2    0x0002
#define SMS_ENCODER3    0x0003
#define SMS_RESET       0x0004
#define SMS_GATED       0x0005
#define SMS_TRIGGER     0x0006
#define SMS_EXTERNAL    0x0007

/* DMA/interrupt enable register */
#define TIM1_DMAINTENR  MMIO32(TIM1_BASE + 0x0c)
#define TIM2_DMAINTENR  MMIO32(TIM2_BASE + 0x0c)
// Trigger the DMA request enable bit.
#define TDE     0x4000
// DMA request enable bit of COM.
#define COMDE   0x2000
// Compare the DMA request enable bit of capture channel 4.
#define CC4DE   0x1000
// Compare the DMA request enable bit of capture channel 3.
#define CC3DE   0x0800
// Compare the DMA request enable bit of capture channel 2.
#define CC2DE   0x0400
// Compare the DMA request enable bit of capture channel 1.
#define CC1DE   0x0200
// Updated DMA request enable bit.
#define UDE     0x0100
// Brake interrupt enable bit.
#define BIE     0x0080
// Trigger the interrupt enable bit.
#define TIE     0x0040
// COM interrupt allow bit.
#define COMIE   0x0020
// Compare capture channel 4 interrupt enable bit.
#define CC4IE   0x0010
// Compare capture channel 3 interrupt enable bit.
#define CC3IE   0x0008
// Compare capture channel 2 interrupt enable bit.
#define CC2IE   0x0004
// Compare capture channel 1 interrupt enable bit.
#define CC1IE   0x0002
// Update the interrupt enable bit.
#define UIE     0x0001

/* Interrupt status register */
#define TIM1_INTFR      MMIO32(TIM1_BASE + 0x10)
#define TIM2_INTFR      MMIO32(TIM2_BASE + 0x10)
// Compare capture channel 4 to repeat capture flag bits.
#define CC4OF   0x1000
// Compare capture channel 3 to repeat capture flag bits.
#define CC3OF   0x0800
// Compare capture channel 2 to repeat capture flag bits.
#define CC2OF   0x0400
// The compare capture channel 1 repeat capture flag bit
#define CC1OF   0x0200
// The brake interrupt flag bit
#define BIF     0x0080
// Trigger interrupt flag bit
#define TIF     0x0040
// COM interrupt flag bit
#define COMIF   0x0020
// Compare capture channel 4 interrupt flag bits.
#define CC4IF   0x0010
// Compare capture channel 3 interrupt flag bits.
#define CC3IF   0x0008
// Compare capture channel 2 interrupt flag bits.
#define CC2IF   0x0004
// Compare capture channel 1 interrupt flag bits.
#define CC1IF   0x0002
// Update interrupt flag bit
#define UIF     0x0001

/* Event generation register */
#define TIM1_SWEVGR     MMIO32(TIM1_BASE + 0x14)
#define TIM2_SWEVGR     MMIO32(TIM2_BASE + 0x14)
// The brake event generation bit
#define BG      0x80
// The trigger event generation bit
#define TG      0x40
// Compare capture control update generation bit.
#define COMG    0x20
// Compare capture event generation bit 4.
#define CC4G    0x10
// Compare capture event generation bit 3.
#define CC3G    0x08
// Compare capture event generation bit 2.
#define CC2G    0x04
// Compare capture event generation bit 1.
#define CC1G    0x02
// Update event generation bit
#define UG      0x01

/* Compare/capture control register 1 */
#define TIM1_CHCTLR1    MMIO32(TIM1_BASE + 0x18)
#define TIM2_CHCTLR1    MMIO32(TIM2_BASE + 0x18)
// Compare capture channel 2 clear enable bit.
#define OC2CE               0x8000
// Compare Capture Channel 2 mode setting field.
#define OC2M_FROZEN         0x0000
#define OC2M_CH1_ACT        0x1000
#define OC2M_CH1_INACT      0x2000
#define OC2M_TOGGLE         0x3000
#define OC2M_FORCE_INACT    0x4000
#define OC2M_FORCE_ACT      0x5000
#define OC2M_PWM1           0x6000
#define OC2M_PWM2           0x7000
// Compare Capture Register 2 preload enable bit.
#define OC2PE               0x0800
// Compare Capture Channel 2 fast enable bit
#define OC2FE               0x0400
// Compare capture channel 2 input selection fields.
#define CC2S_OUTPUT         0x0000
#define CC2S_INPUT_TI2      0x0100
#define CC2S_INPUT_TI1      0x0200
#define CC2S_INPUT_TRC      0x0300
// Compare capture channel 1 clear enable bit.
#define OC1CE               0x0080
// Compare Capture Channel 1 mode setting field.
#define OC1M_FROZEN         0x0000
#define OC1M_CH1_ACT        0x0010
#define OC1M_CH1_INACT      0x0020
#define OC1M_TOGGLE         0x0030
#define OC1M_FORCE_INACT    0x0040
#define OC1M_FORCE_ACT      0x0050
#define OC1M_PWM1           0x0060
#define OC1M_PWM2           0x0070
// Compare Capture Register 1 preload enable bit.
#define OC1PE               0x0008
// Compare Capture Channel 1 fast enable bit
#define OC1FE               0x0004
// Compare capture channel 1 input selection fields.
#define CC1S_OUTPUT         0x0000
#define CC1S_INPUT_TI2      0x0001
#define CC1S_INPUT_TI1      0x0002
#define CC1S_INPUT_TRC      0x0003
/* Input capture mode */
// The input capture filter 2 configuration field
#define IC2F_NO_FILTER      0x0000
#define IC2F_CK_INT_2       0x1000
#define IC2F_CK_INT_4       0x2000
#define IC2F_CK_INT_8       0x3000
#define IC2F_DTS_DIV2_6     0x4000
#define IC2F_DTS_DIV2_8     0x5000
#define IC2F_DTS_DIV4_6     0x6000
#define IC2F_DTS_DIV4_7     0x7000
#define IC2F_DTS_DIV8_6     0x8000
#define IC2F_DTS_DIV8_8     0x9000
#define IC2F_DTS_DIV16_5    0xa000
#define IC2F_DTS_DIV16_6    0xb000
#define IC2F_DTS_DIV16_8    0xc000
#define IC2F_DTS_DIV32_5    0xd000
#define IC2F_DTS_DIV32_6    0xe000
#define IC2F_DTS_DIV32_8    0xf000
// Compare capture channel 2 prescaler configuration
#define IC2PSC_NO           0x0000
#define IC2PSC_2            0x0400
#define IC2PSC_4            0x0800
#define IC2PSC_8            0x0c00
// The input capture filter 1 configuration field
#define IC1F_NO_FILTER      0x0000
#define IC1F_CK_INT_2       0x1000
#define IC1F_CK_INT_4       0x2000
#define IC1F_CK_INT_8       0x3000
#define IC1F_DTS_DIV2_6     0x4000
#define IC1F_DTS_DIV2_8     0x5000
#define IC1F_DTS_DIV4_6     0x6000
#define IC1F_DTS_DIV4_7     0x7000
#define IC1F_DTS_DIV8_6     0x8000
#define IC1F_DTS_DIV8_8     0x9000
#define IC1F_DTS_DIV16_5    0xa000
#define IC1F_DTS_DIV16_6    0xb000
#define IC1F_DTS_DIV16_8    0xc000
#define IC1F_DTS_DIV32_5    0xd000
#define IC1F_DTS_DIV32_6    0xe000
#define IC1F_DTS_DIV32_8    0xf000
// Compare capture channel 1 prescaler configuration
#define IC1PSC_NO           0x0000
#define IC1PSC_2            0x0400
#define IC1PSC_4            0x0800
#define IC1PSC_8            0x0c00

/* Compare/capture control register 2 */
#define TIM1_CHCTLR2    MMIO32(TIM1_BASE + 0x1c)
#define TIM2_CHCTLR2    MMIO32(TIM2_BASE + 0x1c)
// Compare capture channel 4 clear enable bit.
#define OC4CE               0x8000
// Compare Capture Channel 4 mode setting field.
#define OC4M_FROZEN         0x0000
#define OC4M_CH1_ACT        0x1000
#define OC4M_CH1_INACT      0x2000
#define OC4M_TOGGLE         0x3000
#define OC4M_FORCE_INACT    0x4000
#define OC4M_FORCE_ACT      0x5000
#define OC4M_PWM1           0x6000
#define OC4M_PWM2           0x7000
// Compare Capture Register 4 preload enable bit.
#define OC4PE               0x0800
// Compare Capture Channel 4 fast enable bit
#define OC4FE               0x0400
// Compare capture channel 4 input selection fields.
#define CC4S_OUTPUT         0x0000
#define CC4S_INPUT_TI2      0x0100
#define CC4S_INPUT_TI1      0x0200
#define CC4S_INPUT_TRC      0x0300
// Compare capture channel 3 clear enable bit.
#define OC3CE               0x0080
// Compare Capture Channel 3 mode setting field.
#define OC3M_FROZEN         0x0000
#define OC3M_CH1_ACT        0x0010
#define OC3M_CH1_INACT      0x0020
#define OC3M_TOGGLE         0x0030
#define OC3M_FORCE_INACT    0x0040
#define OC3M_FORCE_ACT      0x0050
#define OC3M_PWM1           0x0060
#define OC3M_PWM2           0x0070
// Compare Capture Register 3 preload enable bit.
#define OC3PE               0x0008
// Compare Capture Channel 3 fast enable bit
#define OC3FE               0x0004
// Compare capture channel 3 input selection fields.
#define CC3S_OUTPUT         0x0000
#define CC3S_INPUT_TI2      0x0001
#define CC3S_INPUT_TI1      0x0002
#define CC3S_INPUT_TRC      0x0003
/* Input capture mode */
// The input capture filter 4 configuration field
#define IC4F_NO_FILTER      0x0000
#define IC4F_CK_INT_2       0x1000
#define IC4F_CK_INT_4       0x2000
#define IC4F_CK_INT_8       0x3000
#define IC4F_DTS_DIV2_6     0x4000
#define IC4F_DTS_DIV2_8     0x5000
#define IC4F_DTS_DIV4_6     0x6000
#define IC4F_DTS_DIV4_7     0x7000
#define IC4F_DTS_DIV8_6     0x8000
#define IC4F_DTS_DIV8_8     0x9000
#define IC4F_DTS_DIV16_5    0xa000
#define IC4F_DTS_DIV16_6    0xb000
#define IC4F_DTS_DIV16_8    0xc000
#define IC4F_DTS_DIV32_5    0xd000
#define IC4F_DTS_DIV32_6    0xe000
#define IC4F_DTS_DIV32_8    0xf000
// Compare capture channel 4 prescaler configuration
#define IC4PSC_NO           0x0000
#define IC4PSC_2            0x0400
#define IC4PSC_4            0x0800
#define IC4PSC_8            0x0c00
// The input capture filter 3 configuration field
#define IC3F_NO_FILTER      0x0000
#define IC3F_CK_INT_2       0x1000
#define IC3F_CK_INT_4       0x2000
#define IC3F_CK_INT_8       0x3000
#define IC3F_DTS_DIV2_6     0x4000
#define IC3F_DTS_DIV2_8     0x5000
#define IC3F_DTS_DIV4_6     0x6000
#define IC3F_DTS_DIV4_7     0x7000
#define IC3F_DTS_DIV8_6     0x8000
#define IC3F_DTS_DIV8_8     0x9000
#define IC3F_DTS_DIV16_5    0xa000
#define IC3F_DTS_DIV16_6    0xb000
#define IC3F_DTS_DIV16_8    0xc000
#define IC3F_DTS_DIV32_5    0xd000
#define IC3F_DTS_DIV32_6    0xe000
#define IC3F_DTS_DIV32_8    0xf000
// Compare capture channel 3 prescaler configuration
#define IC3PSC_NO           0x0000
#define IC3PSC_2            0x0400
#define IC3PSC_4            0x0800
#define IC3PSC_8            0x0c00

/* Compare/capture enable register */
#define TIM1_CCER       MMIO32(TIM1_BASE + 0x20)
#define TIM2_CCER       MMIO32(TIM2_BASE + 0x20)
// Compare the capture channel 4 output polarity setting bit.
#define CC4P    0x2000
// Compare capture channel 4 output enable bit.
#define CC4E    0x1000
// Compare capture channel 3 complementary output polarity setting bit.
#define CC3NP   0x0800
// Compare capture channel 3 complementary output enable bits.
#define CC3NE   0x0400
// Compare the capture channel 3 output polarity setting bit.
#define CC3P    0x0200
// Compare the capture channel 3 output enable bit.
#define CC3E    0x0100
// Compare capture channel 3 complementary output polarity setting bit.
#define CC2NP   0x0080
// Compare capture channel 3 complementary output enable bits.
#define CC2NE   0x0040
// Compare the capture channel 3 output polarity setting bit.
#define CC2P    0x0020
// Compare the capture channel 3 output enable bit.
#define CC2E    0x0010
// Compare capture channel 3 complementary output polarity setting bit.
#define CC1NP   0x0008
// Compare capture channel 3 complementary output enable bits.
#define CC1NE   0x0004
// Compare the capture channel 3 output polarity setting bit.
#define CC1P    0x0002
// Compare the capture channel 3 output enable bit.
#define CC1E    0x0001

/* Counters */
#define TIM1_CNT        MMIO32(TIM1_BASE + 0x24)
#define TIM2_CNT        MMIO32(TIM2_BASE + 0x24)
// The real-time value of the timer's counter.

/* Counting clock prescaler */
#define TIM1_PSC        MMIO32(TIM1_BASE + 0x28)
#define TIM2_PSC        MMIO32(TIM2_BASE + 0x28)
// The dividing factor of the prescaler of the timer;

/* Auto-reload value register */
#define TIM1_ATRLR      MMIO32(TIM1_BASE + 0x2c)
#define TIM2_ATRLR      MMIO32(TIM2_BASE + 0x2c)
// The value of this field will be loaded into the counter

/* Recurring count value register */
#define TIM1_RPTCR      MMIO32(TIM1_BASE + 0x30)
// The value of the repeat counter.
#define RPTCR_MSK   0xff

/* Compare/capture register 1 */
#define TIM1_CH1CVR     MMIO32(TIM1_BASE + 0x34)
#define TIM1_CH2CVR     MMIO32(TIM1_BASE + 0x38)
#define TIM1_CH3CVR     MMIO32(TIM1_BASE + 0x3c)
#define TIM1_CH4CVR     MMIO32(TIM1_BASE + 0x40)
#define TIM2_CH1CVR     MMIO32(TIM2_BASE + 0x34)
#define TIM2_CH2CVR     MMIO32(TIM2_BASE + 0x38)
#define TIM2_CH3CVR     MMIO32(TIM2_BASE + 0x3c)
#define TIM2_CH4CVR     MMIO32(TIM2_BASE + 0x40)
// The level n indication bit corresponding to the captured value
#define LEVEL       0x10000
// The value compare/capture register channel n.
#define CHCVR_MSK   0x0ffff

/* Brake and deadband registers */
#define TIM1_BDTR       MMIO32(TIM1_BASE + 0x44)
// Main output enable bit.
#define MOE             0x8000
// Auto output enable.
#define AOE             0x4000
// The brake input polarity setting bit.
#define BKP             0x2000
// Brake function enable bit.
#define BKE             0x1000
// OCx, OCxN enable output signal=1.
#define OSSR            0x0800
#define OSSI            0x0400
// Lock the function setting field.
#define LOCK_DISABLED   0x0000
#define LOCK_LEVEL1     0x0100
#define LOCK_LEVEL2     0x0200
#define LOCK_LEVEL3     0x0300
// Deadband setting bits that define the duration of the deadband
#define DTG_MSK         0x00ff

/* DMA control register */
#define TIM1_DMACFGR    MMIO32(TIM1_BASE + 0x48)
#define TIM2_DMACFGR    MMIO32(TIM2_BASE + 0x48)
// The length of the DMA continuous transmission
#define DBL_MSK     0x1f00
#define DBL_SFT     8
#define DBL_SET(x)  ((x<<DBL_SFT)&DBL_MSK)
#define DBL_GET     ((TIM1_DMACFGR>>DBL_SFT)&0x1f)
// These bits define the offset of the DMA in continuous mode
#define DBA_MSK     0x001f
#define DBA_SET(x)  (x&DBA_MSK)
#define DBA_GET     (TIM1_DMACFGR&DBA_MSK)

/* DMA address register for continuous mode */
#define TIM1_DMAADR     MMIO32(TIM1_BASE + 0x4c)
#define TIM2_DMAADR     MMIO32(TIM2_BASE + 0x4c)
// The address of the DMA in continuous mode.

#endif
