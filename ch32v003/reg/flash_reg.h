#ifndef H_FLASH_REG
#define H_FLASH_REG
/*
 * CH32V003 alternative library. Flash Memory and User Option Bytes
 * registers.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Control register */
#define FLASH_ACTLR         MMIO32(FLASH_BASE + 0x00)
// Number of FLASH wait states
#define LATENCY24   0x0
#define LATENCY48   0x1

/* FPEC key register */
#define FLASH_KEYR          MMIO32(FLASH_BASE + 0x04)
// FPEC keys for entering FPEC unlocking keys
#define KEYR_RDPRT  0x000000a5
#define KEYR_KEY1   0x45670123
#define KEYR_KEY2   0xcdef89ab

/* OBKEY register */
#define FLASH_OBKEYR        MMIO32(FLASH_BASE + 0x08)
// Option bytes key for entering the option bytes key to release OPTWRE.

/* Status register */
#define FLASH_STATR         MMIO32(FLASH_BASE + 0x0c)
// BOOT Lock
#define LOCK        0x8000
// Control the switch between user area and BOOT area
#define MODE        0x4000
// Indicates the end of the operation.
#define EOP         0x0020
// Indicates a write protection error.
#define WRPRTERR    0x0010
// Indicates busy status.
#define BUSY        0x0001

/* Configuration register */
#define FLASH_CTLR          MMIO32(FLASH_BASE + 0x10)
// BUF reset operation.
#define BUFRST  0x80000
// Cache data into BUF.
#define BUFLOAD 0x40000
// Performs a fast page (64Byte) erase operation.
#define FTER    0x20000
// Performs quick page programming operations.
#define FTPG    0x10000
// Fast programming lock.
#define FLOCK   0x08000
// Operation completion interrupt control.
#define EOPIE   0x01000
// Error status interrupt control
#define ERRIE   0x00400
// User selects word lock, software clears 0.
#define OBWRE   0x00200
// Lock. Only '1' can be written.
#define LOCK    0x00080
// Start. Set 1 to start an erase action.
#define STRT    0x00040
// Perform user-option bytes erasure.
#define OBER    0x00020
// Perform user-option bytes programming.
#define OBPG    0x00010
// Performs a full-erase operation (erases the entire user area).
#define MER     0x00004
// Perform sector erase (1K)
#define PER     0x00002
// Performs standard programming operations.
#define PG      0x00001

/* Address register */
#define FLASH_ADDR          MMIO32(FLASH_BASE + 0x14)
// The flash memory address.

/* Option byte register */
#define FLASH_OBR           MMIO32(FLASH_BASE + 0x1c)
// Data byte 1
#define DATA1_MSK       0x3fc0000
#define DATA1_SFT       18
#define DATA1_GET       ((FLASH_OBR>>DATA1_SFT)&0xff)
// Data byte 0
#define DATA0_MSK       0x003fc00
#define DATA0_SFT       10
#define DATA0_GET       ((FLASH_OBR>>DATA0_SFT)&0xff)
// Power-on startup mode.
#define STATR_MODE      0x0000080
// Configuration word reset delay time.
#define RST_MODE_MSK    0x0000060
#define RST_MODE_SFT    5
#define RST_MODE_GET    ((FLASH_OBR>>RST_MODE_SFT)&0x3)
// System reset control in Standby mode.
#define STANDY_RST      0x0000010
// Independent Watchdog (IWDG) hardware enable bit.
#define IWDG_SW         0x0000004
// Read protection status.
#define RDPRT           0x0000002
// Wrong choice of words.
#define OBERR           0x0000001

/* Write protection register */
#define FLASH_WPR           MMIO32(FLASH_BASE + 0x20)
// Flash memory write protect state.
#define WPR0    0x0001
#define WPR1    0x0002
#define WPR2    0x0004
#define WPR3    0x0008
#define WPR4    0x0010
#define WPR5    0x0020
#define WPR6    0x0040
#define WPR7    0x0080
#define WPR8    0x0100
#define WPR9    0x0200
#define WPR10   0x0400
#define WPR11   0x0800
#define WPR12   0x1000
#define WPR13   0x2000
#define WPR14   0x4000
#define WPR15   0x8000

/* Extended key register */
#define FLASH_MODEKEYR      MMIO32(FLASH_BASE + 0x24)
// Unlock the fast programming/erase mode.

/* Unlock BOOT key register */
#define FLASH_BOOT_MODEKEYR MMIO32(FLASH_BASE + 0x28)
// Unlock the BOOT area.

#endif
