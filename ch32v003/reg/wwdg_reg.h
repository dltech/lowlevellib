#ifndef H_WWDG_REG
#define H_WWDG_REG
/*
 * CH32V003 alternative library. Window Watchdog (WWDG) module registers.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Control register */
#define WWDG_CTLR   MMIO32(WWDG_BASE + 0x0)
// Window watchdog reset enable bit.
#define WDGA    0x80
// The 7-bit self-decrement counter
#define T_MSK   0x7f

/* Configuration Register */
#define WWDG_CFGR   MMIO32(WWDG_BASE + 0x4)
// Early wakeup interrupt enable bit.
#define EWI         0x200
// Window watchdog clock division selection.
#define WDGTB_DIV1  0x000
#define WDGTB_DIV2  0x080
#define WDGTB_DIV4  0x100
#define WDGTB_DIV8  0x180
// Window watchdog 7-bit window value.
#define W_MSK       0x07f

/* Status Register */
#define WWDG_STATR  MMIO32(WWDG_BASE + 0x8)
// Wake up the interrupt flag bit early.
#define EWIF    0x1


#endif
