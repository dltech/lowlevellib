#ifndef H_PFIC_REG
#define H_PFIC_REG
/*
 * CH32V003 alternative library. Programmable Fast Interrupt Controller
 * (PFIC) module registers.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/*** External Interrupt and Event Controller (EXTI) regs **/

/* Interrupt enable register */
#define EXTI_INTENR     MMIO32(EXTI_BASE + 0x00)
// Enable the interrupt request signal for external interrupt channel x
#define MR9 0x200
#define MR8 0x100
#define MR7 0x080
#define MR6 0x040
#define MR5 0x020
#define MR4 0x010
#define MR3 0x008
#define MR2 0x004
#define MR1 0x002
#define MR0 0x001

/* Event enable register */
#define EXTI_EVENR      MMIO32(EXTI_BASE + 0x04)
// Enable the event request signal for external interrupt channel x.

/* Rising edge trigger enable register */
#define EXTI_RTENR      MMIO32(EXTI_BASE + 0x08)
// Enable rising edge triggering of external interrupt channel x.
#define TR9 0x200
#define TR8 0x100
#define TR7 0x080
#define TR6 0x040
#define TR5 0x020
#define TR4 0x010
#define TR3 0x008
#define TR2 0x004
#define TR1 0x002
#define TR0 0x001

/* Falling edge trigger enable register */
#define EXTI_FTENR      MMIO32(EXTI_BASE + 0x0c)
// Enable falling edge triggering of external interrupt channel x.

/* Soft interrupt event register */
#define EXTI_SWIEVR     MMIO32(EXTI_BASE + 0x10)
// A int is set on the corresponding externally triggered channel.
#define SWIER9  0x200
#define SWIER8  0x100
#define SWIER7  0x080
#define SWIER6  0x040
#define SWIER5  0x020
#define SWIER4  0x010
#define SWIER3  0x008
#define SWIER2  0x004
#define SWIER1  0x002
#define SWIER0  0x001

/* Interrupt flag register */
#define EXTI_INTFR      MMIO32(EXTI_BASE + 0x14)
// The interrupt flag bit
#define IF9 0x200
#define IF8 0x100
#define IF7 0x080
#define IF6 0x040
#define IF5 0x020
#define IF4 0x010
#define IF3 0x008
#define IF2 0x004
#define IF1 0x002
#define IF0 0x001

/*** Programmable Fast Interrupt Controller (PFIC) regs ***/

/* PFIC interrupt enable status register 1 */
#define PFIC_ISR1       MMIO32(PFIC_BASE + 0x000)
// Interrupt current enable status.
#define INTENSTA31  0x80000000
#define INTENSTA30  0x40000000
#define INTENSTA29  0x20000000
#define INTENSTA28  0x10000000
#define INTENSTA27  0x08000000
#define INTENSTA26  0x04000000
#define INTENSTA25  0x02000000
#define INTENSTA24  0x01000000
#define INTENSTA23  0x00800000
#define INTENSTA22  0x00400000
#define INTENSTA21  0x00200000
#define INTENSTA20  0x00100000
#define INTENSTA19  0x00080000
#define INTENSTA18  0x00040000
#define INTENSTA17  0x00020000
#define INTENSTA16  0x00010000
#define INTENSTA14  0x00004000
#define INTENSTA12  0x00001000
#define INTENSTA3   0x00000008
#define INTENSTA2   0x00000004

/* PFIC interrupt enable status register 2 */
#define PFIC_ISR2       MMIO32(PFIC_BASE + 0x004)
// Interrupt current enable state.
#define INTENSTA38  0x00000040
#define INTENSTA37  0x00000020
#define INTENSTA36  0x00000010
#define INTENSTA35  0x00000008
#define INTENSTA34  0x00000004
#define INTENSTA33  0x00000002
#define INTENSTA32  0x00000001

/* PFIC interrupt pending status register 1 */
#define PFIC_IPR1       MMIO32(PFIC_BASE + 0x020)
// Interrupt the current pending status.
#define PENDSTA31   0x80000000
#define PENDSTA30   0x40000000
#define PENDSTA29   0x20000000
#define PENDSTA28   0x10000000
#define PENDSTA27   0x08000000
#define PENDSTA26   0x04000000
#define PENDSTA25   0x02000000
#define PENDSTA24   0x01000000
#define PENDSTA23   0x00800000
#define PENDSTA22   0x00400000
#define PENDSTA21   0x00200000
#define PENDSTA20   0x00100000
#define PENDSTA19   0x00080000
#define PENDSTA18   0x00040000
#define PENDSTA17   0x00020000
#define PENDSTA16   0x00010000
#define PENDSTA14   0x00004000
#define PENDSTA12   0x00001000
#define PENDSTA3    0x00000008
#define PENDSTA2    0x00000004

/* PFIC interrupt pending status register 2 */
#define PFIC_IPR2       MMIO32(PFIC_BASE + 0x024)
// Interrupt the current pending status.
#define PENDSTA38   0x00000040
#define PENDSTA37   0x00000020
#define PENDSTA36   0x00000010
#define PENDSTA35   0x00000008
#define PENDSTA34   0x00000004
#define PENDSTA33   0x00000002
#define PENDSTA32   0x00000001

/* PFIC interrupt priority threshold configuration register */
#define PFIC_ITHRESDR   MMIO32(PFIC_BASE + 0x040)
// Interrupt priority threshold setting value.
#define THRESHOLD_MSK   0xc0
#define THRESHOLD_SFT   6

/* PFIC interrupt configuration register */
#define PFIC_CFGR       MMIO32(PFIC_BASE + 0x048)
// security access identification data
#define KEYCODE1    0xfa050000
#define KEYCODE2    0xbcaf0000
#define KEYCODE3    0xbeef0000
// System reset (simultaneous writing to KEY3).
#define RESETSYS    0x00000080

/* PFIC interrupt global status register */
#define PFIC_GISR       MMIO32(PFIC_BASE + 0x04c)
// Are there any interrupts currently on hold.
#define GPENDSTA    0x200
// Are there any interrupts currently being executed.
#define GACTSTA     0x100
// Current interrupt nesting status
#define NESTSTA_MSK 0x0ff
#define NESTSTA2    0x003
#define NESTSTA1    0x001
#define NESTSTA_NO  0x000

/* PFIC VTF interrupt ID configuration register */
#define PFIC_VTFIDR     MMIO32(PFIC_BASE + 0x050)
// Configure the interrupt number of VTF interrupt 1.
#define VTFID1_MSK      0xff00
#define VTFID1_SFT      8
#define VTFID1_GET      ((n>>VTFID1_SFT)&0xff)
#define VTFID1_SET(n)   ((n<<VTFID1_SFT)&VTFID1_MSK)
// Configure the interrupt number of VTF interrupt 0.
#define VTFID0_MSK      0x00ff
#define VTFID0_GET      (PFIC_VTFIDR&VTFID0_MSK)
#define VTFID0_SET(n)   (n&VTFID0_MSK)

/* PFIC VTF interrupt 0 offset address register */
#define PFIC_VTFADDRR0  MMIO32(PFIC_BASE + 0x060)
// VTF interrupt 0 service program address
#define ADDR0_MSK       0xfffffffe
#define ADDR0_SFT       1
#define ADDR0_GET       ((PFIC_VTFADDRR0>>ADDR0_SFT)&0x7fffffff)
#define ADDR0_SET(n)    ((n<<ADDR0_SFT)&ADDR0_MSK)
// VTF interrupt 0 enable bit.
#define VTF0EN          0x00000001

/* PFIC VTF interrupt 1 offset address register */
#define PFIC_VTFADDRR1  MMIO32(PFIC_BASE + 0x064)
// VTF interrupt 1 service program address
#define ADDR1_MSK       0xfffffffe
#define ADDR1_SFT       1
#define ADDR1_GET       ((PFIC_VTFADDRR0>>ADDR0_SFT)&0x7fffffff)
#define ADDR1_SET(n)    ((n<<ADDR0_SFT)&ADDR0_MSK)
// VTF interrupt 1 enable bit.
#define VTF1EN          0x00000001

/* PFIC interrupt enable setting register 1 */
#define PFIC_IENR1      MMIO32(PFIC_BASE + 0x100)
// Interrupt enable control.
#define INTEN31     0x80000000
#define INTEN30     0x40000000
#define INTEN29     0x20000000
#define INTEN28     0x10000000
#define INTEN27     0x08000000
#define INTEN26     0x04000000
#define INTEN25     0x02000000
#define INTEN24     0x01000000
#define INTEN23     0x00800000
#define INTEN22     0x00400000
#define INTEN21     0x00200000
#define INTEN20     0x00100000
#define INTEN19     0x00080000
#define INTEN18     0x00040000
#define INTEN17     0x00020000
#define INTEN16     0x00010000
#define INTEN14     0x00004000
#define INTEN12     0x00001000

/* PFIC interrupt enable setting register 2 */
#define PFIC_IENR2      MMIO32(PFIC_BASE + 0x104)
// Interrupt enable control.
#define INTEN38     0x00000040
#define INTEN37     0x00000020
#define INTEN36     0x00000010
#define INTEN35     0x00000008
#define INTEN34     0x00000004
#define INTEN33     0x00000002
#define INTEN32     0x00000001

/* PFIC interrupt enable clear register 1 */
#define PFIC_IRER1      MMIO32(PFIC_BASE + 0x180)
// Interrupt shutdown control.
#define INTRSET31   0x80000000
#define INTRSET30   0x40000000
#define INTRSET29   0x20000000
#define INTRSET28   0x10000000
#define INTRSET27   0x08000000
#define INTRSET26   0x04000000
#define INTRSET25   0x02000000
#define INTRSET24   0x01000000
#define INTRSET23   0x00800000
#define INTRSET22   0x00400000
#define INTRSET21   0x00200000
#define INTRSET20   0x00100000
#define INTRSET19   0x00080000
#define INTRSET18   0x00040000
#define INTRSET17   0x00020000
#define INTRSET16   0x00010000
#define INTRSET14   0x00004000
#define INTRSET12   0x00001000

/* PFIC interrupt enable clear register 2 */
#define PFIC_IRER2      MMIO32(PFIC_BASE + 0x184)
// Interrupt shutdown control.
#define INTRSET38   0x00000040
#define INTRSET37   0x00000020
#define INTRSET36   0x00000010
#define INTRSET35   0x00000008
#define INTRSET34   0x00000004
#define INTRSET33   0x00000002
#define INTRSET32   0x00000001

/* PFIC interrupt pending setting register 1 */
#define PFIC_IPSR1      MMIO32(PFIC_BASE + 0x200)
// Interrupt pending setting.
#define PENDSET31   0x80000000
#define PENDSET30   0x40000000
#define PENDSET29   0x20000000
#define PENDSET28   0x10000000
#define PENDSET27   0x08000000
#define PENDSET26   0x04000000
#define PENDSET25   0x02000000
#define PENDSET24   0x01000000
#define PENDSET23   0x00800000
#define PENDSET22   0x00400000
#define PENDSET21   0x00200000
#define PENDSET20   0x00100000
#define PENDSET19   0x00080000
#define PENDSET18   0x00040000
#define PENDSET17   0x00020000
#define PENDSET16   0x00010000
#define PENDSET14   0x00004000
#define PENDSET12   0x00001000
#define PENDSET3    0x00000008
#define PENDSET2    0x00000004

/* PFIC interrupt pending setting register 2 */
#define PFIC_IPSR2      MMIO32(PFIC_BASE + 0x204)
// Interrupt pending setting.
#define PENDSET38   0x00000040
#define PENDSET37   0x00000020
#define PENDSET36   0x00000010
#define PENDSET35   0x00000008
#define PENDSET34   0x00000004
#define PENDSET33   0x00000002
#define PENDSET32   0x00000001

/* PFIC interrupt pending clear register 1 */
#define PFIC_IPRR1      MMIO32(PFIC_BASE + 0x280)
// Interrupt hang clear.
#define PENDRST31   0x80000000
#define PENDRST30   0x40000000
#define PENDRST29   0x20000000
#define PENDRST28   0x10000000
#define PENDRST27   0x08000000
#define PENDRST26   0x04000000
#define PENDRST25   0x02000000
#define PENDRST24   0x01000000
#define PENDRST23   0x00800000
#define PENDRST22   0x00400000
#define PENDRST21   0x00200000
#define PENDRST20   0x00100000
#define PENDRST19   0x00080000
#define PENDRST18   0x00040000
#define PENDRST17   0x00020000
#define PENDRST16   0x00010000
#define PENDRST14   0x00004000
#define PENDRST12   0x00001000
#define PENDRST3    0x00000008
#define PENDRST2    0x00000004

/* PFIC interrupt pending clear register 2 */
#define PFIC_IPRR2      MMIO32(PFIC_BASE + 0x284)
// Interrupt hang clear.
#define PENDRST38   0x00000040
#define PENDRST37   0x00000020
#define PENDRST36   0x00000010
#define PENDRST35   0x00000008
#define PENDRST34   0x00000004
#define PENDRST33   0x00000002
#define PENDRST32   0x00000001

/* PFIC interrupt activation status register 1 */
#define PFIC_IACTR1     MMIO32(PFIC_BASE + 0x300)
// Interrupt execution status.
#define IACTS31     0x80000000
#define IACTS30     0x40000000
#define IACTS29     0x20000000
#define IACTS28     0x10000000
#define IACTS27     0x08000000
#define IACTS26     0x04000000
#define IACTS25     0x02000000
#define IACTS24     0x01000000
#define IACTS23     0x00800000
#define IACTS22     0x00400000
#define IACTS21     0x00200000
#define IACTS20     0x00100000
#define IACTS19     0x00080000
#define IACTS18     0x00040000
#define IACTS17     0x00020000
#define IACTS16     0x00010000
#define IACTS14     0x00004000
#define IACTS12     0x00001000
#define IACTS3      0x00000008
#define IACTS2      0x00000004

/* PFIC interrupt activation status register 2 */
#define PFIC_IACTR2     MMIO32(PFIC_BASE + 0x304)
// Interrupt execution status.
#define IACTS38     0x00000040
#define IACTS37     0x00000020
#define IACTS36     0x00000010
#define IACTS35     0x00000008
#define IACTS34     0x00000004
#define IACTS33     0x00000002
#define IACTS32     0x00000001

/* PFIC interrupt priority configuration register */
#define PFIC_IPRIOR0    MMIO32(PFIC_BASE + 0x400)
#define PFIC_IPRIOR1    MMIO32(PFIC_BASE + 0x404)
#define PFIC_IPRIOR2    MMIO32(PFIC_BASE + 0x408)
#define PFIC_IPRIOR3    MMIO32(PFIC_BASE + 0x40c)
#define PFIC_IPRIOR4    MMIO32(PFIC_BASE + 0x410)
#define PFIC_IPRIOR5    MMIO32(PFIC_BASE + 0x414)
#define PFIC_IPRIOR6    MMIO32(PFIC_BASE + 0x418)
#define PFIC_IPRIOR7    MMIO32(PFIC_BASE + 0x41c)
#define PFIC_IPRIOR8    MMIO32(PFIC_BASE + 0x420)
#define PFIC_IPRIOR9    MMIO32(PFIC_BASE + 0x424)
#define PFIC_IPRIOR10   MMIO32(PFIC_BASE + 0x428)
#define PFIC_IPRIOR11   MMIO32(PFIC_BASE + 0x42c)
#define PFIC_IPRIOR12   MMIO32(PFIC_BASE + 0x430)
#define PFIC_IPRIOR13   MMIO32(PFIC_BASE + 0x434)
#define PFIC_IPRIOR14   MMIO32(PFIC_BASE + 0x438)
#define PFIC_IPRIOR15   MMIO32(PFIC_BASE + 0x43c)
#define PFIC_IPRIOR16   MMIO32(PFIC_BASE + 0x440)
#define PFIC_IPRIOR17   MMIO32(PFIC_BASE + 0x444)
#define PFIC_IPRIOR18   MMIO32(PFIC_BASE + 0x448)
#define PFIC_IPRIOR19   MMIO32(PFIC_BASE + 0x44c)
#define PFIC_IPRIOR20   MMIO32(PFIC_BASE + 0x450)
#define PFIC_IPRIOR21   MMIO32(PFIC_BASE + 0x454)
#define PFIC_IPRIOR22   MMIO32(PFIC_BASE + 0x458)
#define PFIC_IPRIOR23   MMIO32(PFIC_BASE + 0x45c)
#define PFIC_IPRIOR24   MMIO32(PFIC_BASE + 0x460)
#define PFIC_IPRIOR25   MMIO32(PFIC_BASE + 0x464)
#define PFIC_IPRIOR26   MMIO32(PFIC_BASE + 0x468)
#define PFIC_IPRIOR27   MMIO32(PFIC_BASE + 0x46c)
#define PFIC_IPRIOR28   MMIO32(PFIC_BASE + 0x470)
#define PFIC_IPRIOR29   MMIO32(PFIC_BASE + 0x474)
#define PFIC_IPRIOR30   MMIO32(PFIC_BASE + 0x478)
#define PFIC_IPRIOR31   MMIO32(PFIC_BASE + 0x47c)
#define PFIC_IPRIOR32   MMIO32(PFIC_BASE + 0x480)
#define PFIC_IPRIOR33   MMIO32(PFIC_BASE + 0x484)
#define PFIC_IPRIOR34   MMIO32(PFIC_BASE + 0x488)
#define PFIC_IPRIOR35   MMIO32(PFIC_BASE + 0x48c)
#define PFIC_IPRIOR36   MMIO32(PFIC_BASE + 0x490)
#define PFIC_IPRIOR37   MMIO32(PFIC_BASE + 0x494)
#define PFIC_IPRIOR38   MMIO32(PFIC_BASE + 0x498)
#define PFIC_IPRIOR39   MMIO32(PFIC_BASE + 0x49c)
#define PFIC_IPRIOR40   MMIO32(PFIC_BASE + 0x4a0)
#define PFIC_IPRIOR41   MMIO32(PFIC_BASE + 0x4a4)
#define PFIC_IPRIOR42   MMIO32(PFIC_BASE + 0x4a8)
#define PFIC_IPRIOR43   MMIO32(PFIC_BASE + 0x4ac)
#define PFIC_IPRIOR44   MMIO32(PFIC_BASE + 0x4b0)
#define PFIC_IPRIOR45   MMIO32(PFIC_BASE + 0x4b4)
#define PFIC_IPRIOR46   MMIO32(PFIC_BASE + 0x4b8)
#define PFIC_IPRIOR47   MMIO32(PFIC_BASE + 0x4bc)
#define PFIC_IPRIOR48   MMIO32(PFIC_BASE + 0x4c0)
#define PFIC_IPRIOR49   MMIO32(PFIC_BASE + 0x4c4)
#define PFIC_IPRIOR50   MMIO32(PFIC_BASE + 0x4c8)
#define PFIC_IPRIOR51   MMIO32(PFIC_BASE + 0x4cc)
#define PFIC_IPRIOR52   MMIO32(PFIC_BASE + 0x4d0)
#define PFIC_IPRIOR53   MMIO32(PFIC_BASE + 0x4d4)
#define PFIC_IPRIOR54   MMIO32(PFIC_BASE + 0x4d8)
#define PFIC_IPRIOR55   MMIO32(PFIC_BASE + 0x4dc)
#define PFIC_IPRIOR56   MMIO32(PFIC_BASE + 0x4e0)
#define PFIC_IPRIOR57   MMIO32(PFIC_BASE + 0x4e4)
#define PFIC_IPRIOR58   MMIO32(PFIC_BASE + 0x4e8)
#define PFIC_IPRIOR59   MMIO32(PFIC_BASE + 0x4ec)
#define PFIC_IPRIOR60   MMIO32(PFIC_BASE + 0x4f0)
#define PFIC_IPRIOR61   MMIO32(PFIC_BASE + 0x4f4)
#define PFIC_IPRIOR62   MMIO32(PFIC_BASE + 0x4f8)
#define PFIC_IPRIOR63   MMIO32(PFIC_BASE + 0x4fc)
#define PFIC_IPRIOR(n)  MMIO32(PFIC_BASE + 0x400 + (n*4))
// Number n interrupt priority configuration.
#define PRIO_7_MSK      0xff000000
#define PRIO_7_SFT      24
#define PRIO_7_GET(r)   ((r>>PRIO_7_SFT)&0xff)
#define PRIO_7_SET(n)   ((n<<PRIO_7_SFT)&PRIO_7_MSK)
#define PRIO_6_MSK      0x00ff0000
#define PRIO_6_SFT      16
#define PRIO_6_GET(r)   ((r>>PRIO_6_SFT)&0xff)
#define PRIO_6_SET(n)   ((n<<PRIO_6_SFT)&PRIO_6_MSK)
#define PRIO_5_MSK      0x0000ff00
#define PRIO_5_SFT      8
#define PRIO_5_GET(r)   ((r>>PRIO_5_SFT)&0xff)
#define PRIO_5_SET(n)   ((n<<PRIO_5_SFT)&PRIO_5_MSK)
#define PRIO_4_MSK      0x000000ff
#define PRIO_4_GET(r)   (r&PRIO_4_MSK)
#define PRIO_4_SET(n)   (n&PRIO_4_MSK)
#define PRIO_3_MSK      0xff000000
#define PRIO_3_SFT      24
#define PRIO_3_GET(r)   ((r>>PRIO_3_SFT)&0xff)
#define PRIO_3_SET(n)   ((n<<PRIO_3_SFT)&PRIO_3_MSK)
#define PRIO_2_MSK      0x00ff0000
#define PRIO_2_SFT      16
#define PRIO_2_GET(r)   ((r>>PRIO_2_SFT)&0xff)
#define PRIO_2_SET(n)   ((n<<PRIO_2_SFT)&PRIO_2_MSK)
#define PRIO_1_MSK      0x0000ff00
#define PRIO_1_SFT      8
#define PRIO_1_GET(r)   ((r>>PRIO_1_SFT)&0xff)
#define PRIO_1_SET(n)   ((n<<PRIO_1_SFT)&PRIO_1_MSK)
#define PRIO_0_MSK      0x000000ff
#define PRIO_0_GET(r)   (r&PRIO_0_MSK)
#define PRIO_0_SET(n)   (n&PRIO_0_MSK)

/* PFIC system control register */
#define PFIC_SCTLR      MMIO32(PFIC_BASE + 0xd10)
// System reset, clear 0 automatically.
#define SYSRESET    0x80000000
// Set the event to wake up the WFE case.
#define SETEVENT    0x00000020
// enabled events and all interrupts can wake up the system.
#define SEVONPEND   0x00000010
// Execute the WFI command as if it were a WFE.
#define WFITOWFE    0x00000008
// Low-power mode of the control system.
#define SLEEPDEEP   0x00000004
// System status after control leaves the
#define SLEEPONEXIT 0x00000002

/*** STK-related registers ***/

/* System count control register */
#define TK_CTLR         MMIO32(STK_BASE + 0x00)
// Software interrupt trigger enable (SWI).
#define SWIE    0x80000000
// Auto-reload count enable bit.
#define STRE    0x00000008
// Counter clock source selection bit.
#define STCLK   0x00000004
// Counter interrupt enable control bit.
#define STIE    0x00000002
// System counter enable control bit.
#define STE     0x00000001

/* System count status register */
#define STK_SR          MMIO32(STK_BASE + 0x04)
// Count value comparison flag, write 0 to clear, write 1 to invalidate.
#define CNTIF   0x1

/* System counter register */
#define STK_CNTL        MMIO32(STK_BASE + 0x08)
// The current counter count value is 32 bits.

/* Counting comparison register */
#define STK_CMPLR       MMIO32(STK_BASE + 0x10)
// Set the comparison counter value to 32 bits.

#endif
