#ifndef H_USART_REG
#define H_USART_REG
/*
 * CH32V003 alternative library. Universal Synchronous Asynchronous
 * Receiver Transmitter (USART) module registers.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* UASRT status register */
#define USART_STATR MMIO32(USART_BASE + 0x00)
// CTS state change flag.
#define CTS     0x200
// LIN Break detection flag.
#define LBD     0x100
// Send data register empty flag.
#define TXE     0x080
// Send completion flag.
#define TC      0x040
// Read data register non-empty flag
#define RXNE    0x020
// Bus idle flag.
#define IDLE    0x010
// Overload error flag.
#define ORE     0x008
// Noise error flag.
#define NE      0x004
// Frame error flag.
#define FE      0x002
// buffer communication mode.
#define PE      0x001

/* UASRT data register */
#define USART_DATAR MMIO32(USART_BASE + 0x04)
// receive data register (RDR) and send register (TDR)
#define DR_MSK  0xff

/* UASRT baud rate register */
#define USART_BRR   MMIO32(USART_BASE + 0x08)
// integer part of the dividing factor
#define DIV_M_MSK       0xfff0
#define DIV_M_SFT       4
#define DIV_M_SET(x)    ((x<<DIV_M_SFT)&DIV_M_MSK)
// fractional part of the dividing factor
#define DIV_F_MSK       0x000f
#define DIV_F_SET(x)    (x&DIV_F_MSK)
#define BRR_SET(br)     (DIV_M_SET(F_SYS/16/br) | DIV_F_SET(F_SYS/256/br))

/* UASRT control register 1 */
#define USART_CTLR1 MMIO32(USART_BASE + 0x0c)
// USART enable bit.
#define UE      0x2000
// Word long bit.
#define M       0x1000
// Wake-up bit.
#define WAKE    0x0800
// The parity bit is enabled.
#define PCE     0x0400
// Parity selection.
#define PS      0x0200
// Parity check interrupt enable bit.
#define PEIE    0x0100
// TXE interrupt enable.
#define TXEIE   0x0080
// Transmit completion interrupt enable.
#define TCIE    0x0040
// RXNE interrupt enable.
#define RXNEIE  0x0020
// IDLE interrupt enable.
#define IDLEIE  0x0010
// Transmitter enable.
#define TE      0x0008
// Receiver enable.
#define RE      0x0004
// Receiver wakeup.
#define RWU     0x0002
// Send break bit.
#define SBK     0x0001

/* UASRT control register 2 */
#define USART_CTLR2 MMIO32(USART_BASE + 0x10)
// LIN mode enable
#define LINEN   0x4000
// STOP bits.
#define STOP1   0x0000
#define STOP0P5 0x1000
#define STOP2   0x2000
#define STOP1P5 0x3000
// Clock enable.
#define CLKEN   0x0800
// Clock polarity
#define CPOL    0x0400
// Clock phase
#define CPHA    0x0200
// Last bit clock pulse
#define LBCL    0x0100
// LIN Break detection interrupt enable
#define LBDIE   0x0040
// LIN Break detection length
#define LBDL    0x0020
// Address of the USART node
#define ADD_MSK 0x000f

/* UASRT control register 3 */
#define USART_CTLR3 MMIO32(USART_BASE + 0x14)
// CTS interrupt enable bit
#define CTSIE   0x400
// CTS enable bit
#define CTSE    0x200
// RTS enable bit
#define RTSE    0x100
// DMA transmit enable bit.
#define DMAT    0x080
// DMA receive enable bit.
#define DMAR    0x040
// Smartcard mode enable bit.
#define SCEN    0x020
// Smartcard NACK enable bit.
#define NACK    0x010
// Half-duplex selection bit.
#define HDSEL   0x008
// IrDA low-power bit
#define IRLP    0x004
// IrDA enable bit
#define IREN    0x002
// Error interrupt enable bit
#define EIE     0x001

/* UASRT protection time and prescaler register */
#define USART_GPR   MMIO32(USART_BASE + 0x18)
// Guard time value.
#define GT_MSK      0xff00
#define GT_SFT      8
#define GT_SET(x)   ((x<<GT_SFT)&GT_MSK)
#define GT_GET      ((USART_GPR>>GT_SFT)&0xff)
// Prescaler value field.
#define PSC_MSK     0x00ff
#define PSC_SET(x)  (x&PSC_MSK)
#define PSC_GET     (USART_GPR&PSC_MSK)

#endif
