#ifndef H_ESIG_REG
#define H_ESIG_REG
/*
 * CH32V003 alternative library. The electronic signature registers.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Flash capacity register */
#define ESIG_FLACAP MMIO32(ESIG_BASE + 0xe0)
// Flash capacity in Kbyte.

/* UID register 1 */
#define ESIG_UNIID1 MMIO32(ESIG_BASE + 0xe8)
// The 0-31 digits of UID.

/* UID register 2 */
#define ESIG_UNIID2 MMIO32(ESIG_BASE + 0xec)
// The 32-63 digits of UID.

/* UID register 3 */
#define ESIG_UNIID3 MMIO32(ESIG_BASE + 0xf0)
// The 64-95 digits of UID.

#endif
