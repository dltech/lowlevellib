#ifndef H_RCC_REG
#define H_RCC_REG
/*
 * CH32V003 alternative library. Reset and Clock Control module registers.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Clock control register */
#define RCC_CTLR        MMIO32(RCC_BASE + 0x00)
// PLL clock-ready lock flag bit.
#define PLLRDY              0x02000000
// PLL clock enable control bit.
#define PLLON               0x01000000
// Clock security system enable control bit.
#define CSSON               0x00080000
// External high-speed crystal bypass control bit.
#define HSEBYP              0x00040000
// External high-speed crystal oscillation stabilization ready flag
#define HSERDY              0x00020000
// External high-speed crystal oscillation enable control bit.
#define HSEON               0x00010000
// Internal high-speed clock calibration values
#define HSICAL_MSK          0x0000ff00
#define HSICAL_SFT          8
#define HSICAL_GET          ((RCC_CTLR>>HSICAL_SFT)&0xff)
// Internal high-speed clock adjustment value.
#define HSITRIM_MSK         0x000000f8
#define HSITRIM_SFT         3
#define HSITRIM_GET         ((RCC_CTLR>>HSITRIM_SFT)&0x1f)
#define HSITRIM_SET(n)      ((n<<HSITRIM_SFT)&HSITRIM_MSK)
#define HSITRIM_KHZ(hhz)    (HSITRIM_SET(khz/60))
// Internal high-speed clock (24MHz) Stable Ready flag bit
#define HSIRDY              0x00000002
// Internal high-speed clock (24MHz) enable control bit.
#define HSION               0x00000001

/* Clock configuration register 0 */
#define RCC_CFGR0       MMIO32(RCC_BASE + 0x04)
// Microcontroller MCO pin clock output control.
#define MCO_NO          0x00000000
#define MCO_SYSCLK      0x04000000
#define MCO_HSI         0x05000000
#define MCO_HSE         0x06000000
#define MCO_PLL         0x07000000
// Input clock source for PLL (write only when PLL is off).
#define PLLSRC          0x00010000
// ADC clock source prescaler control
#define ADCPRE_DIV2     0x00000000
#define ADCPRE_DIV4     0x00002000
#define ADCPRE_DIV6     0x00008000
#define ADCPRE_DIV8     0x00006000
#define ADCPRE_DIV12    0x0000a000
#define ADCPRE_DIV16    0x0000e000
#define ADCPRE_DIV24    0x0000a800
#define ADCPRE_DIV32    0x0000e800
#define ADCPRE_DIV48    0x0000b000
#define ADCPRE_DIV64    0x0000f000
#define ADCPRE_DIV96    0x0000b800
#define ADCPRE_DIV128   0x0000f800
// HB clock source prescaler control.
#define HPRE_DIV2       0x00000010
#define HPRE_DIV3       0x00000020
#define HPRE_DIV4       0x00000030
#define HPRE_DIV5       0x00000040
#define HPRE_DIV6       0x00000050
#define HPRE_DIV7       0x00000060
#define HPRE_DIV8       0x00000070
#define HPRE_DIV16      0x000000b0
#define HPRE_DIV32      0x000000c0
#define HPRE_DIV64      0x000000d0
#define HPRE_DIV128     0x000000e0
#define HPRE_DIV256     0x000000f0
// System clock (SYSCLK) status (hardware set).
#define SWS_HSI         0x00000000
#define SWS_HSE         0x00000004
#define SWS_PLL         0x00000008
// Select the system clock source.
#define SW_HSI          0x00000000
#define SW_HSE          0x00000001
#define SW_PLL          0x00000002

/* Clock interrupt register */
#define RCC_INTR        MMIO32(RCC_BASE + 0x08)
// Clear the clock security system interrupt flag bit (CSSF).
#define CSSC        0x00800000
// Clear the PLL-ready interrupt flag bit.
#define PLLRDYC     0x00100000
// Clear the HSE oscillator ready interrupt flag bit.
#define HSERDYC     0x00080000
// Clear the HSI oscillator ready interrupt flag bit.
#define HSIRDYC     0x00040000
// Clear the LSI oscillator ready interrupt flag bit.
#define LSIRDYC     0x00010000
// PLL-ready interrupt enable bit.
#define PLLRDYIE    0x00001000
// HSE-ready interrupt enable bit.
#define HSERDYIE    0x00000800
// HSI-ready interrupt enable bit.
#define HSIRDYIE    0x00000400
// LSI-ready interrupt enable bit.
#define LSIRDYIE    0x00000100
// Clock security system interrupt flag bit.
#define CSSF        0x00000080
// PLL clock-ready lockout interrupt flag.
#define PLLRDYF     0x00000010
// HSE clock-ready interrupt flag.
#define HSERDYF     0x00000008
// HSI clock-ready interrupt flag.
#define HSIRDYF     0x00000004
// LSI clock-ready interrupt flag.
#define LSIRDYF     0x00000001

/* PB2 peripheral reset register */
#define RCC_APB2PRSTR   MMIO32(RCC_BASE + 0x0c)
// USART1 interface reset control.
#define USART1RST   0x4000
// SPI1 interface reset control.
#define SPI1RST     0x1000
// TIM1 module reset control.
#define TIM1RST     0x0800
// ADC1 module reset control.
#define ADC1RST     0x0200
// PD port module reset control for I/O.
#define IOPDRST     0x0020
// PC port module reset control for I/O.
#define IOPCRST     0x0010
// PA port module reset control for I/O.
#define IOPARST     0x0004
// I/O auxiliary function module reset control.
#define AFIORST     0x0001

/* PB1 peripheral reset register */
#define RCC_APB1PRSTR   MMIO32(RCC_BASE + 0x10)
// Power interface module reset control.
#define PWRRST  0x10000000
// I2C1 interface reset control.
#define I2C1RST 0x00200000
// Window watchdog reset control.
#define WWDGRST 0x00000800
// Timer 2 module reset control.
#define TIM2RST 0x00000001

/* HB peripheral clock enable register */
#define RCC_AHBPCENR    MMIO32(RCC_BASE + 0x14)
// SRAM interface module clock enable bit.
#define SRAMEN  0x04
// DMA1 module clock enable bit.
#define DMA1EN  0x01

/* PB2 peripheral clock enable register */
#define RCC_APB2PCENR   MMIO32(RCC_BASE + 0x18)
// USART1 interface clock enable bit.
#define USART1EN    0x4000
// SPI1 interface clock enable bit.
#define SPI1EN      0x1000
// TIM1 module clock enable bit.
#define TIM1EN      0x0800
// ADC1 module clock enable bit.
#define ADC1EN      0x0200
// PD port module clock enable bit for I/O.
#define IOPDEN      0x0020
// PC port module clock enable bit for I/O.
#define IOPCEN      0x0010
// PA port module clock enable bit for I/O.
#define IOPAEN      0x0004
// I/O auxiliary function module clock enable bit.
#define AFIOEN      0x0001

/* PB1 peripheral clock enable register */
#define RCC_APB1PCENR   MMIO32(RCC_BASE + 0x1c)
// Power interface module clock enable bit.
#define PWREN   0x10000000
// I2C1 interface clock enable bit.
#define I2C1EN  0x00200000
// Window watchdog clock enable bit.
#define WWDGEN  0x00000800
// Timer 2 module clock enable bit.
#define TIM2EN  0x00000001

/* Control/status register */
#define RCC_RSTSCKR     MMIO32(RCC_BASE + 0x24)
// Low-power reset flag.
#define LPWRRSTF    0x80000000
// Window watchdog reset flag.
#define WWDGRSTF    0x40000000
// Independent watchdog reset flag.
#define IWDGRSTF    0x20000000
// Software reset flag.
#define SFTRSTF     0x10000000
// Power-up/power-down reset flag.
#define PORRSTF     0x08000000
// External manual reset (NRST pin) flag.
#define PINRSTF     0x04000000
// Clear reset flag control.
#define RMVF        0x01000000
// Internal Low Speed Clock (LSI) Stable Ready flag bit
#define LSIRDY      0x00000002
// Internal low-speed clock (LSI) enable control bit.
#define LSION       0x00000001

#endif
