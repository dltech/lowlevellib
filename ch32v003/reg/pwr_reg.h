#ifndef H_PWR_REG
#define H_PWR_REG
/*
 * CH32V003 alternative library. Power Control module registers.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Power control register */
#define PWR_CTLR    MMIO32(PWR_BASE + 0x00)
// PVD voltage monitoring threshold setting.
#define PLS2P85V    0x00
#define PLS3P05V    0x20
#define PLS3P3V     0x40
#define PLS3P5V     0x60
#define PLS3P7V     0x80
#define PLS3P9V     0xa0
#define PLS4P1V     0xc0
#define PLS4P4V     0xe0
// Power supply voltage monitoring function enable flag bit
#define PVDE        0x10
// Standby/ Sleep mode selection bit in power-down deep sleep scenario.
#define PDDS        0x02

/* Power control/status register */
#define PWR_CSR     MMIO32(PWR_BASE + 0x04)
// PVD output status flag bit.
#define PVD0    0x04

/* Auto-wakeup control/status register */
#define PWR_AWUCSR  MMIO32(PWR_BASE + 0x08)
// Enable Automatic wake-up
#define AWUEN   0x02

/* Auto-wakeup window comparison value register */
#define PWR_AWUWR   MMIO32(PWR_BASE + 0x0c)
// AWU window value
#define AWUWR_MSK   0x3f

/* Auto-wakeup crossover factor register */
#define PWR_AWUPSC  MMIO32(PWR_BASE + 0x10)
// Counting time base
#define AWUPSC_OFF      0x00
#define AWUPSC_DIV2     0x02
#define AWUPSC_DIV4     0x03
#define AWUPSC_DIV8     0x04
#define AWUPSC_DIV16    0x05
#define AWUPSC_DIV32    0x06
#define AWUPSC_DIV64    0x07
#define AWUPSC_DIV128   0x08
#define AWUPSC_DIV256   0x09
#define AWUPSC_DIV512   0x0a
#define AWUPSC_DIV1024  0x0b
#define AWUPSC_DIV2048  0x0c
#define AWUPSC_DIV4096  0x0d
#define AWUPSC_DIV10240 0x0e
#define AWUPSC_DIV61440 0x0f

#endif
