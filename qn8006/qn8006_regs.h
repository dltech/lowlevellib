#ifndef H_QN8006_REGS
#define H_QN8006_REGS
/*
 * Digital FM Transceiver for Portable Devices library, registers.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/*  */

#define SYSTEM1     0x00 /* Sets device modes. */
// Receiving request (overwrites TXREQ and STNBY):
#define RXREQ       0x80
// Transmission request: 1 Enter Transmit mode.
#define TXREQ       0x40
// Channel Scan mode enable: 1 Channel Scan mode operation.
#define CHSC        0x20
// Standby mode if the chip is in IDLE and no TXREQ or RXREQ is received.
#define STNBY       0x10
// I2S enable in receiving mode: 1 Use I2S digital interface for RX audio.
#define RXI2S       0x08
// I2S enable in transmitting mode: 1 Use I2S digital signal for TX audio.
#define TXI2S       0x04
// RDS enable:
#define RDSEN       0x02
// CH (channel index) selection method:
#define CCA_CH_DIS  0x01

#define SYSTEM2     0x01 /* Sets device modes, resets. */
// Reset all registers to default values:
#define SWRST       0x80
// Reset the state to initial states and recalibrate all blocks:
#define RECAL       0x40
// Force receiver in MONO mode: 1 Forced in MONO mode.
#define FORCE_MO    0x20
// TX stereo and mono mode selection: 0 Stereo 1 Mono
#define ST_MO_TX    0x10
// Pre-emphasis and de-emphasis time constant: (μs) 0 50 1 75
#define TC          0x08
// Toggle this bit to transmit all 8 bytes in RDS0~RDS7.
#define RDSTXRDY    0x04
// Time out setting for IDLE to standby state transition: (min)
#define TMOUT1      0x00
#define TMOUT3      0x01
#define TMOUT5      0x02
#define TMOUT_INF   0x03

#define DEV_ADD     0x02 /* Sets device address. */
// RX CCA threshold MSB. See CCA register 19h [4:0].
#define RXCCAD      0x80
// Programmed device address when SEB=1: If SEB=0, the def device addr (0101011)
#define DADD_MSK    0x7f
#define DADD_SET(n) (n&DADD_MSK)
#define DADD_DEF    0x2b

#define ANACTL1     0x03 /* Analog control functions. */
// TX and RX audio mute enable:
#define MUTE_EN     0x80
// I2S MCK invert: 1 Inverted
#define I2S_SCKINV  0x40
// Reset signal of baseband data-path: (Low active) 0 Reset
#define RSTB_BB     0x20
// Select the antenna for TX channel scan mode: 1 Use the tx antenna on RFO.
#define ANT_SEL     0x10
// Crystal Frequency Selection (MHz):
#define XSEL11P3    0x00
#define XSEL12      0x01
#define XSEL12P8    0x02
#define XSEL13      0x03
#define XSEL16P4    0x04
#define XSEL18P4    0x05
#define XSEL19P2    0x06
#define XSEL22P6    0x08
#define XSEL24      0x09
#define XSEL24P6    0x0a
#define XSEL26      0x0b
#define XSEL32P7    0x0c
#define XSEL36P8    0x0d
#define XSEL38P4    0x0e
#define XSEL7P6     0x0f

#define REG_VGA     0x04 /* TX mode input impedance, crystal cap load setting. */
// TX mode input impedance for both L/R channels: (kΩ)
#define RIN10           0x00
#define RIN20           0x40
#define RIN40           0x80
#define RIN80           0xc0
// Crystal cap load setting: The loading cap on each side is: 10+XCSEL*0.32 pF
#define XCSEL_MSK       0x3f
// setup in 0.01pf
#define XCSEL_SET(n)    ((n-1000)/32)

#define CIDR1       0x05 /* Device ID numbers. */
// Chip ID for product family: 0 FM
#define CID1_MSK        0x1c
#define CID1_SFT        2
#define CID1_GET(reg)   ((reg>>CID1_SFT)&0x7)
#define CID1_FM         0x00
// Chip ID for minor revision:
#define CID2_0          0x00
#define CID2_1          0x01
#define CID2_2          0x02
#define CID2_3          0x03

#define CIDR2       0x06 /* Device ID numbers. */
// Chip ID for product ID:
#define CID3_MSK        0xf0
#define CID3_SFT        4
#define CID3_GET(reg)   ((reg>>CID3_SFT)&0xf)
#define CID3_QN8006     0x3
#define CID3_QN8006L    0x7
//
#define CID4_MSK        0x0f
#define CID4_GET(reg)   (reg&CID4_MSK)
#define CID4_A          0x00
#define CID4_B          0x01
#define CID4_C          0x02
#define CID4_D          0x03

#define I2S         0x07 /* Sets I2S parameters. */
// I2S bit width:
#define I2SBW8          0x00
#define I2SBW16         0x40
#define I2SBW24         0x80
#define I2SBW32         0xc0
// I2S data rate:
#define I2SDRATE32      0x00
#define I2SDRATE40      0x10
#define I2SDRATE44P1    0x20
#define I2SDRATE48      0x30
// I2S mode:
#define I2SMODE         0x08
// I2S format in TX mode:
#define I2SFMT_MSB      0x00
#define I2SFMT_I2S      0x01
#define I2SFMT_DSP1     0x02
#define I2SFMT_DSP2     0x03
#define I2SFMT_LSP      0x04

#define CH          0x08 /* Lower 8 bits of 10-bit channel index. */
// Lower 8 bits of 10-bit Channel Index

#define CH_START    0x09 /* Lower 8 bits of 10-bit channel scan start ch index. */
// Lower 8 bits of 10-bit CCA (channel scan) start channel index.

#define CH_STOP     0x0a /* Lower 8 bits of 10-bit channel scan stop ch index. */
// Lower 8 bits of 10-bit CCA (channel scan) stop channel index.

#define CH_STEP     0x0b /* Channel scan freq step. Highest 2 bits of ch indexes. */
// CCA (channel scan) frequency step:
#define FSTEP50KHZ      0x00
#define FSTEP100KHZ     0x40
#define FSTEP200KHZ     0x80
// High 2 bits of 10-bit CCA (ch scan) stop ch idx: Stop f is (76+CH_STP*0.05)MHz.
#define CH_STP_MSK      0x30
#define CH_STP_SFT      4
#define CH_STP_SET(n)   ((n<<CH_STP_SFT)&CH_STP_MSK)
// High 2b of 10-bit CCA (ch scan) start ch idx: Start f is (76+CH_STA*0.05)MHz.
#define CH_STA_MSK      0x0c
#define CH_STA_SFT      2
#define CH_STA_SET(n)   ((n<<CH_STA_SFT)&CH_STA_MSK)
// Highest 2 bits of 10-bit channel index: Channel freq is (76+CH*0.05) MHz.
#define CH_MSK          0x03
#define CH_SET(n)       (n&CH_MSK)

#define PAC_TARGET  0x0c /* Output power calibration control. */
// PA calibration target value. PA output target is (0.37*PAC_TARGET+68) dBuV.
// set in dBuV *100
#define PAC_TARGET_SET(n)   ((n-6800)/37)

#define TXAGC_GAIN  0x0d /* Sets TX parameters. */
// TX soft clipping enable:
#define TX_SFTCLPEN     0x80
// TX AGC Gain selection method:
#define TAGC_GAIN_SEL   0x40
// Image Rejection: 1 LO>RF, image is in upper side.
#define IMR             0x20
// TX digital gain: 0 0dB, 1 1dB
#define TXAGC_GDB       0x10
// TX input buffer gain: (dB)
#define TXAGC_GVGA4P5   0x00
#define TXAGC_GVGA6     0x01
#define TXAGC_GVGA7P5   0x02
#define TXAGC_GVGA9     0x03
#define TXAGC_GVGA10P5  0x04
#define TXAGC_GVGA12    0x05
#define TXAGC_GVGA13P5  0x06
#define TXAGC_GVGA15    0x07
#define TXAGC_GVGA16P5  0x08
#define TXAGC_GVGA18    0x09
#define TXAGC_GVGA19P5  0x0a
#define TXAGC_GVGA21    0x0b

#define TX_FDEV     0x0e /* Specify total TX frequency deviation. */
// TX frequency deviation = 0.69 kHz*TX_FEDV.
// set in Hz
#define TX_FDEV_SET(n)  (n/690)

#define GAIN_TXPLT  0x0f /* Gain of TX pilot frequency dev, I2S buffer clear. */
// I2S buffer underflow clear: User has to de-assert this bit after clearing.
#define I2SUNDFL_CLR    0x80
// I2S buffer overflow clear: User has to de-assert this bit after clearing.
#define I2SOVFL_CLR     0x40
// Gain of TX pilot to adjust pilot frequency deviation:
#define GAIN_TXPLT7P    0x1c
#define GAIN_TXPLT8P    0x20
#define GAIN_TXPLT9P    0x24
#define GAIN_TXPLT10P   0x28
// RDS RX/TX Interrupt Enable:
#define RDS_INT_EN      0x02
// TX CCA / RX CCA Interrupt Enable
#define CCA_INT_EN      0x01

#define RDSD0       0x10 /* RDS data byte 0. */
#define RDSD1       0x11 /* RDS data byte 1. */
#define RDSD2       0x12 /* RDS data byte 2. */
#define RDSD3       0x13 /* RDS data byte 3. */
#define RDSD4       0x14 /* RDS data byte 4. */
#define RDSD5       0x15 /* RDS data byte 5. */
#define RDSD6       0x16 /* RDS data byte 6. */
#define RDSD7       0x17 /* RDS data byte 7. */

#define RDSFDEV     0x18 /* Specify RDS frequency deviation, RDS mode selection. */
// RDS mode selection: 1 Received bit-stream has RDS block only, no MMBS block.
#define RDS_ONLY        0x80
// Specify RDS frequency deviation:
#define RDSFDEV_MSK     0x7f
// set in hz
#define RDSFDEV_SET(n)  ((n/350)&RDSFDEV_MSK)

#define CCA         0x19 /* Sets CCA parameters. */
// Scaling factor to determine in-band noise power to out-of-band noise power ratio.
#define TXCCAA
// Lower 5 bits of RXCCAD [5:0].
#define RXCCAD

#define STATUS1     0x1a /* Device status indicators. */
// RXCCA Status Flag: 1 RX CCA fails to find a valid channel.
#define RXCCA_FAIL  0x40
// I2S overflow indicator: 1 Overflow
#define I2SOVFL     0x20
// I2S underflow indicator: 1 Underflow
#define I2SUNDFL    0x10
// Input level saturation flag: 1 Input level too high, Channel saturates.
#define INSAT       0x08
// RX AGC settling status: 1 Settled
#define RXAGCSET    0x04
// RXAGC status: 1 AGC error
#define RXAGCERR    0x02
// Stereo receiving status: 0 Stereo
#define ST_MO_RX    0x01

#define STATUS3     0x1b /* RDS status indicators. */
// RDS RX: RDS received group updated. 0->0 or 1->1New data is in receiving.
#define RDS_RXTXUPD 0x80
// ‘E’ block (MMBS block) detected: 1 Detected
#define E_DET       0x40
// Type indicator of the RDS third block in one group: 1 C1
#define RDSC0C1     0x20
// RDS block synchronous indicator: 0 Non-synchronous
#define RDSSYNC     0x10
// Received RDS block 0 status indicator: 0 No error
#define RDS0ERR     0x08
// Received RDS block 1 status indicator: 0 No error
#define RDS1ERR     0x04
// Received RDS block 2 status indicator: 0 No error
#define RDS2ERR     0x02
// Received RDS block 3 status indicator: 0 No error
#define RDS3ERR     0x01

#define RSSISIG     0x1c /* In-band signal RSSI dBμV value. */
// In-band signal RSSI dBμV value: dBμV = RSSI - 40
#define RSSIDB_GET(reg) (reg+40)

#define RSSIMP      0x21 /* Multipath signal RSSI DB value. */
// Multipath signal RSSI (Received signal strength indicator) dB value.

#define SNR         0x22 /* Est RF CNR from noise floor around pil after FM demod.*/
// Est RF input CNR value from noise floor around the pilot after FM demodulation.

#define REG_XLT3    0x49 /* XCLK pin control. */
// Direct inject crystal oscillation from external XCLK pin. 1 external clock XCLK.
#define XTLBYP  0x10

#define REG_DAC     0x4f /* DAC output stage gain. */
// DAC output stage gain:
#define DACG3DB     0x0
#define DACG0DB     0x1
#define DACGM3DB    0x2
#define DACGM6DB    0x3

#define PAC_CAL     0x59 /* PA tuning cap calibration. */
// Manually request PA tuning cap and gain calibration 1 Reset the calibration
#define PAC_REQ         0x80
// Disable PA tuning cap calibration and use PACAP as circuit setting
#define PAC_DIS         0x40
// User-set PA Tuning cap. Each LSB is 0.3pF. The read back val is the cal result.
#define PACAP_MSK       0x3f
#define PACAP_SET(n)    ((n/3)&PACAP_MSK)

#define PAG_CAL     0x5a /* PA gain calibration. */
// Disable PA output power calibration and use IPOW, PAGAIN as circuit setting
#define PAG_DIS         0x40
// Set PA current. The read back value is the calibration result.
#define IPOW4MA         0x00
#define IPOW2MA         0x10
#define IPOW1MA         0x20
#define IPOW0P5MA       0x30
// Set PAGAIn setting. Tx out volt on the RFO pin (dBuV) is 124dBuV-1.5dB*PAGAIN.
#define PAGAIN124DBUV   0x00
#define PAGAIN122DBUV   0x01
#define PAGAIN121DBUV   0x02
#define PAGAIN119DBUV   0x03
#define PAGAIN118DBUV   0x04
#define PAGAIN116DBUV   0x05
#define PAGAIN115DBUV   0x06
#define PAGAIN113BUV    0x07
#define PAGAIN112DBUV   0x08
#define PAGAIN110DBUV   0x09
#define PAGAIN109DBUV   0x0a
#define PAGAIN107DBUV   0x0b
#define PAGAIN106DBUV   0x0c
#define PAGAIN104DBUV   0x0d
#define PAGAIN103DBUV   0x0e
#define PAGAIN101DBUV   0x0f

#endif
