/**************************************************************************//**
 * @file     ARMCM4_FP.h
 * @brief    CMSIS Core Peripheral Access Layer Header File for
 *           ARMCM4 Device (configured for CM4 with FPU)
 * @version  V5.3.1
 * @date     09. July 2018
 ******************************************************************************/
/*
 * Copyright (c) 2009-2018 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the License); you may
 * not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ARMCM4_FP_H
#define ARMCM4_FP_H

#ifdef __cplusplus
extern "C" {
#endif


/* -------------------------  Interrupt Number Definition  ------------------------ */

typedef enum IRQn
{
/* -------------------  Processor Exceptions Numbers  ----------------------------- */
  NonMaskableInt_IRQn           = -14,     /*  2 Non Maskable Interrupt */
  HardFault_IRQn                = -13,     /*  3 HardFault Interrupt */
  MemoryManagement_IRQn         = -12,     /*  4 Memory Management Interrupt */
  BusFault_IRQn                 = -11,     /*  5 Bus Fault Interrupt */
  UsageFault_IRQn               = -10,     /*  6 Usage Fault Interrupt */
  SVCall_IRQn                   =  -5,     /* 11 SV Call Interrupt */
  DebugMonitor_IRQn             =  -4,     /* 12 Debug Monitor Interrupt */
  PendSV_IRQn                   =  -2,     /* 14 Pend SV Interrupt */
  SysTick_IRQn                  =  -1,     /* 15 System Tick Interrupt */

/* -------------------  Processor Interrupt Numbers  ------------------------------ */
  Power_IRQn                    = 0,
  Clock_IRQn                    = 0,
  MPU_IRQn                      = 0,
  Radio_IRQn                    = 1,
  UART_IRQn                     = 2,
  SPI0_IRQn                     = 3,
  TWI0_IRQn                     = 3,
  TWI1_IRQn                     = 4,
  SPI1_IRQn                     = 4,
  SPIS_IRQn                     = 4,
  NFCT_IRQn                     = 5,
  GPIOTE_IRQn                   = 6,
  SAADC_IRQn   0x40007000
  Timer0_IRQn  0x40008000
  Timer1_IRQn  0x40009000
  Timer2_IRQn  0x4000a000
  RTC0_IRQn    0x4000b000
  Temp_IRQn    0x4000c000
  RNG_IRQn     0x4000d000
  ECB_IRQn     0x4000e000
  AAR_IRQn     0x4000f000
  CCM_IRQn     0x4000f000
  WDT_IRQn     0x40010000
  RTC1_IRQn    0x40011000
  QDEC_IRQn    0x40012000
  COMP_IRQn    0x40013000
  SWI0_IRQn    0x40014000
  SWI1_IRQn    0x40015000
  SWI2_IRQn    0x40016000
  SWI3_IRQn    0x40017000
  SWI4_IRQn    0x40018000
  SWI5_IRQn    0x40019000
  EGU0_IRQn    0x40014000
  EGU1_IRQn    0x40015000
  EGU2_IRQn    0x40016000
  EGU3_IRQn    0x40017000
  EGU4_IRQn    0x40018000
  EGU5_IRQn    0x40019000
  Timer3_IRQn  0x4001a000
  Timer4_IRQn  0x4001b000
  PWM0_IRQn    0x4001c000
  PDM_IRQn     0x4001d000
  NVMC_IRQn    0x4001e000
  ACL_IRQn     0x4001e000
  PPI_IRQn     0x4001f000
  MWU_IRQn     0x40020000
  PWM1_IRQn    0x40021000
  PWM2_IRQn    0x40022000
  SPI2_IRQn    0x40023000
  RTC2_IRQn    0x40024000
  I2S_IRQn     0x40025000
  FPU_IRQn     0x40026000
  USBD_IRQn    0x40027000
  UARTE1_IRQn  0x40028000
  PWM3_IRQn    0x4002d000
  SPI3_IRQn    0x4002f000
Interrupt0_IRQn               =   0,
  Interrupt1_IRQn               =   1,
  Interrupt2_IRQn               =   2,
  Interrupt3_IRQn               =   3,
  Interrupt4_IRQn               =   4,
  Interrupt5_IRQn               =   5,
  Interrupt6_IRQn               =   6,
  Interrupt7_IRQn               =   7,
  Interrupt8_IRQn               =   8,
  Interrupt9_IRQn               =   9
  /* Interrupts 10 .. 224 are left out */
} IRQn_Type;


/* ================================================================================ */
/* ================      Processor and Core Peripheral Section     ================ */
/* ================================================================================ */

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif


/* --------  Configuration of Core Peripherals  ----------------------------------- */
#define __CM4_REV                 0x0001U   /* Core revision r0p1 */
#define __MPU_PRESENT             1U        /* MPU present */
#define __VTOR_PRESENT            1U        /* VTOR present */
#define __NVIC_PRIO_BITS          3U        /* Number of Bits used for Priority Levels */
#define __Vendor_SysTickConfig    0U        /* Set to 1 if different SysTick Config is used */
#define __FPU_PRESENT             1U        /* FPU present */

#include "core_cm4.h"                       /* Processor and core peripherals */
#include "system_ARMCM4.h"                  /* System Header */


/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050))
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif


#ifdef __cplusplus
}
#endif

#endif  /* ARMCM4_FP_H */
