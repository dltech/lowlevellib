#ifndef H_SPIM_REG
#define H_SPIM_REG
/*
 * Standard library for Nordic NRF52
 * Register macro for Serial Peripheral Interface Master (SPIM) Master
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"
#include "pattern.h"

/*************Tasks************************/
/* Start SPI transaction */
#define STARTSPIM0              MMIO32(SPIM0 + 0x010)
#define STARTSPIM1              MMIO32(SPIM1 + 0x010)
#define STARTSPIM2              MMIO32(SPIM2 + 0x010)
#define STARTSPIM3              MMIO32(SPIM3 + 0x010)
/* Stop SPI transaction */
#define STOPSPIM0               MMIO32(SPIM0 + 0x014)
#define STOPSPIM1               MMIO32(SPIM1 + 0x014)
#define STOPSPIM2               MMIO32(SPIM2 + 0x014)
#define STOPSPIM3               MMIO32(SPIM3 + 0x014)
/* Suspend SPI transaction */
#define SUSPENDSPIM0            MMIO32(SPIM0 + 0x01c)
#define SUSPENDSPIM1            MMIO32(SPIM1 + 0x01c)
#define SUSPENDSPIM2            MMIO32(SPIM2 + 0x01c)
#define SUSPENDSPIM3            MMIO32(SPIM3 + 0x01c)
/* Resume SPI transaction */
#define RESUMESPIM0             MMIO32(SPIM0 + 0x020)
#define RESUMESPIM1             MMIO32(SPIM1 + 0x020)
#define RESUMESPIM2             MMIO32(SPIM2 + 0x020)
#define RESUMESPIM3             MMIO32(SPIM3 + 0x020)

/************Events************************/
/* SPI transaction has stopped */
#define STOPPEDSPIM0            MMIO32(SPIM0 + 0x104)
#define STOPPEDSPIM1            MMIO32(SPIM1 + 0x104)
#define STOPPEDSPIM2            MMIO32(SPIM2 + 0x104)
#define STOPPEDSPIM3            MMIO32(SPIM3 + 0x104)
/* End of RXD buffer reached */
#define ENDRXSPIM0              MMIO32(SPIM0 + 0x110)
#define ENDRXSPIM1              MMIO32(SPIM1 + 0x110)
#define ENDRXSPIM2              MMIO32(SPIM2 + 0x110)
#define ENDRXSPIM3              MMIO32(SPIM3 + 0x110)
/* End of RXD buffer and TXD buffer reached */
#define ENDSPIM0                MMIO32(SPIM0 + 0x118)
#define ENDSPIM1                MMIO32(SPIM1 + 0x118)
#define ENDSPIM2                MMIO32(SPIM2 + 0x118)
#define ENDSPIM3                MMIO32(SPIM3 + 0x118)
/* End of TXD buffer reached */
#define ENDTXSPIM0              MMIO32(SPIM0 + 0x120)
#define ENDTXSPIM1              MMIO32(SPIM1 + 0x120)
#define ENDTXSPIM2              MMIO32(SPIM2 + 0x120)
#define ENDTXSPIM3              MMIO32(SPIM3 + 0x120)
/* Transaction started */
#define STARTEDSPIM0            MMIO32(SPIM0 + 0x14c)
#define STARTEDSPIM1            MMIO32(SPIM1 + 0x14c)
#define STARTEDSPIM2            MMIO32(SPIM2 + 0x14c)
#define STARTEDSPIM3            MMIO32(SPIM3 + 0x14c)

/************Registers*********************/
/* Shortcuts between local events and tasks */
#define SHORTSSPIM0             MMIO32(SPIM0 + 0x200)
#define SHORTSSPIM1             MMIO32(SPIM1 + 0x200)
#define SHORTSSPIM2             MMIO32(SPIM2 + 0x200)
#define SHORTSSPIM3             MMIO32(SPIM3 + 0x200)
// Shortcut between event END and task START
#define END_START   0x20000

/* Enable interrupt */
#define INTENSETSPIM0           MMIO32(SPIM0 + 0x304)
#define INTENSETSPIM1           MMIO32(SPIM1 + 0x304)
#define INTENSETSPIM2           MMIO32(SPIM2 + 0x304)
#define INTENSETSPIM3           MMIO32(SPIM3 + 0x304)
// Write '1' to enable interrupt for event STOPPED
#define STOPPEDSPM  0x00002
// Write '1' to enable interrupt for event ENDRX
#define ENDRXSPM    0x00010
// Write '1' to enable interrupt for event END
#define ENDSPM      0x00040
// Write '1' to enable interrupt for event ENDTX
#define ENDTXSPM    0x00100
// Write '1' to enable interrupt for event STARTED
#define STARTEDSPM  0x80000

/* Disable interrupt */
#define INTENCLRSPIM0           MMIO32(SPIM0 + 0x308)
#define INTENCLRSPIM1           MMIO32(SPIM1 + 0x308)
#define INTENCLRSPIM2           MMIO32(SPIM2 + 0x308)
#define INTENCLRSPIM3           MMIO32(SPIM3 + 0x308)
// Write '1' to disable interrupt for event STOPPED
// Write '1' to disable interrupt for event ENDRX
// Write '1' to disable interrupt for event END
// Write '1' to disable interrupt for event ENDTX
// Write '1' to disable interrupt for event STARTED

/* Stall status for EasyDMA RAM accesses. */
#define STALLSTATSPIM0          MMIO32(SPIM0 + 0x400)
#define STALLSTATSPIM1          MMIO32(SPIM1 + 0x400)
#define STALLSTATSPIM2          MMIO32(SPIM2 + 0x400)
#define STALLSTATSPIM3          MMIO32(SPIM3 + 0x400)
// Stall status for EasyDMA RAM reads
#define TXSPIM  0x1
// Stall status for EasyDMA RAM writes
#define RXSPIM  0x2

/* Enable SPIM */
#define ENABLESPIM0             MMIO32(SPIM0 + 0x500)
#define ENABLESPIM1             MMIO32(SPIM1 + 0x500)
#define ENABLESPIM2             MMIO32(SPIM2 + 0x500)
#define ENABLESPIM3             MMIO32(SPIM3 + 0x500)
// Enable or disable SPIM
#define ENABLESPIM  0x7

/* Pin select for SCK */
#define PSELSCKSPIM0            MMIO32(SPIM0 + 0x508)
#define PSELSCKSPIM1            MMIO32(SPIM1 + 0x508)
#define PSELSCKSPIM2            MMIO32(SPIM2 + 0x508)
#define PSELSCKSPIM3            MMIO32(SPIM3 + 0x508)

/* Pin select for MOSI signal */
#define PSELMOSISPIM0           MMIO32(SPIM0 + 0x50c)
#define PSELMOSISPIM1           MMIO32(SPIM1 + 0x50c)
#define PSELMOSISPIM2           MMIO32(SPIM2 + 0x50c)
#define PSELMOSISPIM3           MMIO32(SPIM3 + 0x50c)

/* Pin select for MISO signal */
#define PSELMISOSPIM0           MMIO32(SPIM0 + 0x510)
#define PSELMISOSPIM1           MMIO32(SPIM1 + 0x510)
#define PSELMISOSPIM2           MMIO32(SPIM2 + 0x510)
#define PSELMISOSPIM3           MMIO32(SPIM3 + 0x510)

/* Pin select for CSN */
#define PSELCSNSPIM0            MMIO32(SPIM0 + 0x514)
#define PSELCSNSPIM1            MMIO32(SPIM1 + 0x514)
#define PSELCSNSPIM2            MMIO32(SPIM2 + 0x514)
#define PSELCSNSPIM3            MMIO32(SPIM3 + 0x514)

/* SPI frequency. Accuracy depends on the HFCLK source selected. */
#define FREQUENCYSPIM0          MMIO32(SPIM0 + 0x524)
#define FREQUENCYSPIM1          MMIO32(SPIM1 + 0x524)
#define FREQUENCYSPIM2          MMIO32(SPIM2 + 0x524)
#define FREQUENCYSPIM3          MMIO32(SPIM3 + 0x524)
// SPI master data rate
#define FREQUENCYK125   0x02000000
#define FREQUENCYK250   0x04000000
#define FREQUENCYK500   0x08000000
#define FREQUENCYM1     0x10000000
#define FREQUENCYM2     0x20000000
#define FREQUENCYM4     0x40000000
#define FREQUENCYM8     0x80000000
#define FREQUENCYM16    0x0a000000
#define FREQUENCYM32    0x14000000

/* Data pointer */
#define RXD_PTRSPIM0            MMIO32(SPIM0 + 0x534)
#define RXD_PTRSPIM1            MMIO32(SPIM1 + 0x534)
#define RXD_PTRSPIM2            MMIO32(SPIM2 + 0x534)
#define RXD_PTRSPIM3            MMIO32(SPIM3 + 0x534)
// PTR[31:0]

/* Maximum number of bytes in receive buffer */
#define RXD_MAXCNTSPIM0         MMIO32(SPIM0 + 0x538)
#define RXD_MAXCNTSPIM1         MMIO32(SPIM1 + 0x538)
#define RXD_MAXCNTSPIM2         MMIO32(SPIM2 + 0x538)
#define RXD_MAXCNTSPIM3         MMIO32(SPIM3 + 0x538)
// MAXCNT[15:0]

/* Number of bytes transferred in the last transaction */
#define RXD_AMOUNTSPIM0         MMIO32(SPIM0 + 0x53c)
#define RXD_AMOUNTSPIM1         MMIO32(SPIM1 + 0x53c)
#define RXD_AMOUNTSPIM2         MMIO32(SPIM2 + 0x53c)
#define RXD_AMOUNTSPIM3         MMIO32(SPIM3 + 0x53c)
// AMOUNT[15:0]

/* EasyDMA list type */
#define RXD_LISTSPIM0           MMIO32(SPIM0 + 0x540)
#define RXD_LISTSPIM1           MMIO32(SPIM1 + 0x540)
#define RXD_LISTSPIM2           MMIO32(SPIM2 + 0x540)
#define RXD_LISTSPIM3           MMIO32(SPIM3 + 0x540)
// 0 Disable EasyDMA list, 1 Use array list
#define LIST    0x1

/* Data pointer */
#define TXD_PTRSPIM0            MMIO32(SPIM0 + 0x544)
#define TXD_PTRSPIM1            MMIO32(SPIM1 + 0x544)
#define TXD_PTRSPIM2            MMIO32(SPIM2 + 0x544)
#define TXD_PTRSPIM3            MMIO32(SPIM3 + 0x544)
// PTR[31:0]

/* Number of bytes in transmit buffer */
#define TXD_MAXCNTSPIM0         MMIO32(SPIM0 + 0x548)
#define TXD_MAXCNTSPIM1         MMIO32(SPIM1 + 0x548)
#define TXD_MAXCNTSPIM2         MMIO32(SPIM2 + 0x548)
#define TXD_MAXCNTSPIM3         MMIO32(SPIM3 + 0x548)
// MAXCNT[15:0]

/* Number of bytes transferred in the last transaction */
#define TXD_AMOUNTSPIM0         MMIO32(SPIM0 + 0x54c)
#define TXD_AMOUNTSPIM1         MMIO32(SPIM1 + 0x54c)
#define TXD_AMOUNTSPIM2         MMIO32(SPIM2 + 0x54c)
#define TXD_AMOUNTSPIM3         MMIO32(SPIM3 + 0x54c)
// AMOUNT[15:0]

/* EasyDMA list type */
#define TXD_LISTSPIM0           MMIO32(SPIM0 + 0x550)
#define TXD_LISTSPIM1           MMIO32(SPIM1 + 0x550)
#define TXD_LISTSPIM2           MMIO32(SPIM2 + 0x550)
#define TXD_LISTSPIM3           MMIO32(SPIM3 + 0x550)
// 0 Disable EasyDMA list, 1 Use array list

/* Configuration register */
#define CONFIGSPIM0             MMIO32(SPIM0 + 0x554)
#define CONFIGSPIM1             MMIO32(SPIM1 + 0x554)
#define CONFIGSPIM2             MMIO32(SPIM2 + 0x554)
#define CONFIGSPIM3             MMIO32(SPIM3 + 0x554)
// Bit order
#define ORDERSPIM   0x1
// Serial clock (SCK) phase
#define CPHASPIM    0x2
// Serial clock (SCK) polarity
#define CPOLSPIM    0x4

/* Sample delay for input serial data on MISO */
#define IFTIMING_RXDELAYSPIM0   MMIO32(SPIM0 + 0x560)
#define IFTIMING_RXDELAYSPIM1   MMIO32(SPIM1 + 0x560)
#define IFTIMING_RXDELAYSPIM2   MMIO32(SPIM2 + 0x560)
#define IFTIMING_RXDELAYSPIM3   MMIO32(SPIM3 + 0x560)
// delay from the the sampl edge of SCK until the input data RXDELAY[2:0]

/* Minimum duration between edge of CSN and edge of SCK and minimum duration */
#define IFTIMING_CSNDURSPIM0    MMIO32(SPIM0 + 0x564)
#define IFTIMING_CSNDURSPIM1    MMIO32(SPIM1 + 0x564)
#define IFTIMING_CSNDURSPIM2    MMIO32(SPIM2 + 0x564)
#define IFTIMING_CSNDURSPIM3    MMIO32(SPIM3 + 0x564)
// The value is specified in n of 64 MHz clk cycles (15.625ns). CSNDUR[7:0]

/* Polarity of CSN output */
#define CSNPOLSPIM0             MMIO32(SPIM0 + 0x568)
#define CSNPOLSPIM1             MMIO32(SPIM1 + 0x568)
#define CSNPOLSPIM2             MMIO32(SPIM2 + 0x568)
#define CSNPOLSPIM3             MMIO32(SPIM3 + 0x568)
// Active low (idle state high), Active high (idle state low)
#define CSNPOL_HIGH 0x1

/* Pin select for DCX signal */
#define PSELDCXSPIM0            MMIO32(SPIM0 + 0x56c)
#define PSELDCXSPIM1            MMIO32(SPIM1 + 0x56c)
#define PSELDCXSPIM2            MMIO32(SPIM2 + 0x56c)
#define PSELDCXSPIM3            MMIO32(SPIM3 + 0x56c)

/* DCX configuration */
#define DCXCNTSPIM0             MMIO32(SPIM0 + 0x570)
#define DCXCNTSPIM1             MMIO32(SPIM1 + 0x570)
#define DCXCNTSPIM2             MMIO32(SPIM2 + 0x570)
#define DCXCNTSPIM3             MMIO32(SPIM3 + 0x570)
// The number of command bytes preceding the data bytes. DCXCNT[3:0]

/* Byte transmitted after TXD.MAXCNT bytes have been transmitted */
#define ORCSPIM0                MMIO32(SPIM0 + 0x5c0)
#define ORCSPIM1                MMIO32(SPIM1 + 0x5c0)
#define ORCSPIM2                MMIO32(SPIM2 + 0x5c0)
#define ORCSPIM3                MMIO32(SPIM3 + 0x5c0)
// ORC[7:0]

#endif
