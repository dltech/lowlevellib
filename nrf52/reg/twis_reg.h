#ifndef H_TWIS_REG
#define H_TWIS_REG
/*
 * Standard library for Nordic NRF52
 * Register macro for I2C compatible Two Wire Interface slave
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/*  */
#define TWIS0    MMIO32(TWIS0 + 0x)
#define TWIS1    MMIO32(TWIS1 + 0x)

/*************Tasks************************/
/* Stop TWI transaction */
#define STOPTWIS0       MMIO32(TWIS0 + 0x014)
#define STOPTWIS1       MMIO32(TWIS1 + 0x014)
/* Suspend TWI transaction */
#define SUSPENDTWIS0    MMIO32(TWIS0 + 0x01c)
#define SUSPENDTWIS1    MMIO32(TWIS1 + 0x01c)
/* Resume TWI transaction */
#define RESUMETWIS0     MMIO32(TWIS0 + 0x020)
#define RESUMETWIS1     MMIO32(TWIS1 + 0x020)
/* Prepare the TWI slave to respond to a write command */
#define PREPARERXTWIS0  MMIO32(TWIS0 + 0x030)
#define PREPARERXTWIS1  MMIO32(TWIS1 + 0x030)
/* Prepare the TWI slave to respond to a read command */
#define PREPARETXTWIS0  MMIO32(TWIS0 + 0x034)
#define PREPARETXTWIS1  MMIO32(TWIS1 + 0x034)

/************Events************************/
/* TWI stopped */
#define STOPPEDTWIS0    MMIO32(TWIS0 + 0x104)
#define STOPPEDTWIS1    MMIO32(TWIS1 + 0x104)
/* TWI error */
#define ERRORTWIS0      MMIO32(TWIS0 + 0x124)
#define ERRORTWIS1      MMIO32(TWIS1 + 0x124)
/* Receive sequence started */
#define RXSTARTEDTWIS0  MMIO32(TWIS0 + 0x14c)
#define RXSTARTEDTWIS1  MMIO32(TWIS1 + 0x14c)
/* Transmit sequence started */
#define TXSTARTEDTWIS0  MMIO32(TWIS0 + 0x150)
#define TXSTARTEDTWIS1  MMIO32(TWIS1 + 0x150)
/* Write command received */
#define WRITETWIS0      MMIO32(TWIS0 + 0x164)
#define WRITETWIS1      MMIO32(TWIS1 + 0x164)
/* Read command received */
#define READTWIS0       MMIO32(TWIS0 + 0x168)
#define READTWIS1       MMIO32(TWIS1 + 0x168)

/************Registers*********************/
/* Shortcut register */
#define SHORTSTWIS0     MMIO32(TWIS0 + 0x200)
#define SHORTSTWIS1     MMIO32(TWIS1 + 0x200)
// Shortcut between event WRITE and task SUSPEND
#define WRITE_SUSPEND   0x2000
// Shortcut between event READ and task SUSPEND
#define READ_SUSPEND    0x4000

/* Enable or disable interrupt */
#define INTENTWIS0      MMIO32(TWIS0 + 0x300)
#define INTENTWIS1      MMIO32(TWIS1 + 0x300)
// Enable or disable interrupt for event STOPPED
#define STOPPED     0x0000002
// Enable or disable interrupt for event ERROR
#define ERRORT      0x0000200
// Enable or disable interrupt for event RXSTARTED
#define RXSTARTED   0x0080000
// Enable or disable interrupt for event TXSTARTED
#define TXSTARTED   0x0100000
// Enable or disable interrupt for event WRITE
#define WRITETWIS   0x2000000
// Enable or disable interrupt for event READ
#define READTWIS    0x4000000

/* Enable interrupt */
#define INTENSETTWIS0   MMIO32(TWIS0 + 0x304)
#define INTENSETTWIS1   MMIO32(TWIS1 + 0x304)
// Write '1' to enable interrupt for event STOPPED
// Write '1' to enable interrupt for event ERROR
// Write '1' to enable interrupt for event RXSTARTED
// Write '1' to enable interrupt for event TXSTARTED
// Write '1' to enable interrupt for event WRITE
// Write '1' to enable interrupt for event READ

/* Disable interrupt */
#define INTENCLRTWIS0   MMIO32(TWIS0 + 0x308)
#define INTENCLRTWIS1   MMIO32(TWIS1 + 0x308)
// Write '1' to disable interrupt for event STOPPED
// Write '1' to disable interrupt for event ERROR
// Write '1' to disable interrupt for event RXSTARTED
// Write '1' to disable interrupt for event TXSTARTED
// Write '1' to disable interrupt for event WRITE
// Write '1' to disable interrupt for event READ

/* Error source */
#define ERRORSRCTWIS0   MMIO32(TWIS0 + 0x4d0)
#define ERRORSRCTWIS1   MMIO32(TWIS1 + 0x4d0)
// RX buffer overflow detected, and prevented
#define OVERFLOW        0x1
// NACK sent after receiving a data byte
#define DNACK           0x4
// TX buffer over-read detected, and prevented
#define OVERREADTWIS    0x8

/* Status register indicating which address had a match */
#define MATCHTWIS0      MMIO32(TWIS0 + 0x4d4)
#define MATCHTWIS1      MMIO32(TWIS1 + 0x4d4)
// Which of the addresses in {ADDRESS} matched the incoming address MATCH[0]

/* Enable TWI */
#define ENABLETWIS0     MMIO32(TWIS0 + 0x500)
#define ENABLETWIS1     MMIO32(TWIS1 + 0x500)
// Enable or disable TWIS
#define ENABLETW    0x9

/* Pin select for SCL */
#define PSELSCLTWIS0    MMIO32(TWIS0 + 0x508)
#define PSELSCLTWIS1    MMIO32(TWIS1 + 0x508)

/* Pin select for SDA */
#define PSELSDATWIS0    MMIO32(TWIS0 + 0x50c)
#define PSELSDATWIS1    MMIO32(TWIS1 + 0x50c)

/* Data pointer */
#define RXD_PTRTWIS0    MMIO32(TWIS0 + 0x534)
#define RXD_PTRTWIS1    MMIO32(TWIS1 + 0x534)
// PTR[31:0]

/* Maximum number of bytes in receive buffer */
#define RXD_MAXCNTTWIS0 MMIO32(TWIS0 + 0x538)
#define RXD_MAXCNTTWIS1 MMIO32(TWIS1 + 0x538)
// MAXCNT[15:0]

/* Number of bytes transferred in the last transaction */
#define RXD_AMOUNTTWIS0 MMIO32(TWIS0 + 0x53c)
#define RXD_AMOUNTTWIS1 MMIO32(TWIS1 + 0x53c)
// In case of NACK error, includes the NACK'ed byte. AMOUNT[15:0]

/* EasyDMA list type */
#define RXD_LISTTWIS0   MMIO32(TWIS0 + 0x540)
#define RXD_LISTTWIS1   MMIO32(TWIS1 + 0x540)
// List type
#define DISABLED    0x0
#define ARRAYLIST   0x1

/* Data pointer */
#define TXD_PTRTWIS0    MMIO32(TWIS0 + 0x544)
#define TXD_PTRTWIS1    MMIO32(TWIS1 + 0x544)
// PTR[31:0]

/* Maximum number of bytes in transmit buffer */
#define TXD_MAXCNTTWIS0 MMIO32(TWIS0 + 0x548)
#define TXD_MAXCNTTWIS1 MMIO32(TWIS1 + 0x548)
// MAXCNT[15:0]

/* Number of bytes transferred in the last transaction */
#define TXD_AMOUNTTWIS0 MMIO32(TWIS0 + 0x54c)
#define TXD_AMOUNTTWIS1 MMIO32(TWIS1 + 0x54c)
// In case of NACK error, includes the NACK'ed byte. AMOUNT[15:0]

/* EasyDMA list type */
#define TXD_LISTTWIS0   MMIO32(TWIS0 + 0x550)
#define TXD_LISTTWIS1   MMIO32(TWIS1 + 0x550)
// List type

/* TWI slave address n */
#define ADDRESS0TWIS0   MMIO32(TWIS0 + 0x588)
#define ADDRESS0TWIS1   MMIO32(TWIS1 + 0x588)
#define ADDRESS1TWIS0   MMIO32(TWIS0 + 0x58c)
#define ADDRESS1TWIS1   MMIO32(TWIS1 + 0x58c)
// Address used in the TWI transfer. ADDRESS[6:0]

/* Configuration register for the address match mechanism */
#define CONFIGTWIS0     MMIO32(TWIS0 + 0x594)
#define CONFIGTWIS1     MMIO32(TWIS1 + 0x594)
// Enable or disable address matching on ADDRESS[0]
#define ADDRESS0_ENABLED    0x1
// Enable or disable address matching on ADDRESS[1]
#define ADDRESS0_ENABLED    0x2

/* Over-read character. */
#define ORCTWIS0        MMIO32(TWIS0 + 0x5c0)
#define ORCTWIS1        MMIO32(TWIS1 + 0x5c0)
// Character sent out in case of an overread of the transmit buffer. ORC[7:0]

#endif
