#ifndef H_MWU_REG
#define H_MWU_REG
/*
 * Standard library for Nordic NRF52
 * The Memory watch unit (MWU) registers.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/************Events************************/
/* Write access to region n detected */
#define REGION0WA           MMIO32(MWU + 0x100)
#define REGION1WA           MMIO32(MWU + 0x108)
#define REGION2WA           MMIO32(MWU + 0x110)
#define REGION3WA           MMIO32(MWU + 0x118)
/* Read access to region n detected */
#define REGION0RA           MMIO32(MWU + 0x104)
#define REGION1RA           MMIO32(MWU + 0x10c)
#define REGION2RA           MMIO32(MWU + 0x114)
#define REGION3RA           MMIO32(MWU + 0x11c)
/* Write access to peripheral region n detected */
#define PREGION0WA          MMIO32(MWU + 0x160)
#define PREGION1WA          MMIO32(MWU + 0x168)
/* Read access to peripheral region n detected */
#define PREGION0RA          MMIO32(MWU + 0x164)
#define PREGION1RA          MMIO32(MWU + 0x16c)

/************Registers*********************/
/* Enable or disable interrupt */
#define INTENMWU            MMIO32(MWU + 0x300)
// Enable or disable interrupt for event REGION0WA
#define REGION0WA   0x01
// Enable or disable interrupt for event REGION0RA
#define REGION0RA   0x02
// Enable or disable interrupt for event REGION1WA
#define REGION1WA   0x04
// Enable or disable interrupt for event REGION1RA
#define REGION1RA   0x08
// Enable or disable interrupt for event REGION2WA
#define REGION2WA   0x10
// Enable or disable interrupt for event REGION2RA
#define REGION2RA   0x20
// Enable or disable interrupt for event REGION3WA
#define REGION3WA   0x40
// Enable or disable interrupt for event REGION3RA
#define REGION3RA   0x80
// Enable or disable interrupt for event PREGION0WA
#define PREGION0WA  0x01000000
// Enable or disable interrupt for event PREGION0RA
#define PREGION0RA  0x02000000
// Enable or disable interrupt for event PREGION1WA
#define PREGION1WA  0x04000000
// Enable or disable interrupt for event PREGION1RA
#define PREGION1RA  0x08000000

/* Enable interrupt */
#define INTENSETMWU         MMIO32(MWU + 0x304)
// Write '1' to enable interrupt for event REGION0WA
// Write '1' to enable interrupt for event REGION0RA
// Write '1' to enable interrupt for event REGION1WA
// Write '1' to enable interrupt for event REGION1RA
// Write '1' to enable interrupt for event REGION2WA
// Write '1' to enable interrupt for event REGION2RA
// Write '1' to enable interrupt for event REGION3WA
// Write '1' to enable interrupt for event REGION3RA
// Write '1' to enable interrupt for event PREGION0WA
// Write '1' to enable interrupt for event PREGION0RA
// Write '1' to enable interrupt for event PREGION1WA
// Write '1' to enable interrupt for event PREGION1RA

/* Disable interrupt */
#define INTENCLRMWU         MMIO32(MWU + 0x308)
// Write '1' to disable interrupt for event REGION0WA
// Write '1' to disable interrupt for event REGION0RA
// Write '1' to disable interrupt for event REGION1WA
// Write '1' to disable interrupt for event REGION1RA
// Write '1' to disable interrupt for event REGION2WA
// Write '1' to disable interrupt for event REGION2RA
// Write '1' to disable interrupt for event REGION3WA
// Write '1' to disable interrupt for event REGION3RA
// Write '1' to disable interrupt for event PREGION0WA
// Write '1' to disable interrupt for event PREGION0RA
// Write '1' to disable interrupt for event PREGION1WA
// Write '1' to disable interrupt for event PREGION1RA

/* Enable or disable interrupt */
#define NMIEN               MMIO32(MWU + 0x320)
// Enable or disable interrupt for event REGION0WA
// Enable or disable interrupt for event REGION0RA
// Enable or disable interrupt for event REGION1WA
// Enable or disable interrupt for event REGION1RA
// Enable or disable interrupt for event REGION2WA
// Enable or disable interrupt for event REGION2RA
// Enable or disable interrupt for event REGION3WA
// Enable or disable interrupt for event REGION3RA
// Enable or disable interrupt for event PREGION0WA
// Enable or disable interrupt for event PREGION0RA
// Enable or disable interrupt for event PREGION1WA
// Enable or disable interrupt for event PREGION1RA

/* Enable interrupt */
#define NMIENSET            MMIO32(MWU + 0x324)
// Write '1' to enable interrupt for event REGION0WA
// Write '1' to enable interrupt for event REGION0RA
// Write '1' to enable interrupt for event REGION1WA
// Write '1' to enable interrupt for event REGION1RA
// Write '1' to enable interrupt for event REGION2WA
// Write '1' to enable interrupt for event REGION2RA
// Write '1' to enable interrupt for event REGION3WA
// Write '1' to enable interrupt for event REGION3RA
// Write '1' to enable interrupt for event PREGION0WA
// Write '1' to enable interrupt for event PREGION0RA
// Write '1' to enable interrupt for event PREGION1WA
// Write '1' to enable interrupt for event PREGION1RA

/* Disable interrupt */
#define NMIENCLR            MMIO32(MWU + 0x328)
// Write '1' to disable interrupt for event REGION0WA
// Write '1' to disable interrupt for event REGION0RA
// Write '1' to disable interrupt for event REGION1WA
// Write '1' to disable interrupt for event REGION1RA
// Write '1' to disable interrupt for event REGION2WA
// Write '1' to disable interrupt for event REGION2RA
// Write '1' to disable interrupt for event REGION3WA
// Write '1' to disable interrupt for event REGION3RA
// Write '1' to disable interrupt for event PREGION0WA
// Write '1' to disable interrupt for event PREGION0RA
// Write '1' to disable interrupt for event PREGION1WA
// Write '1' to disable interrupt for event PREGION1RA

/* write access detected while corresponding subregion was enabled */
#define PERREGION0SUBSTATWA MMIO32(MWU + 0x400)
#define PERREGION1SUBSTATWA MMIO32(MWU + 0x408)
// Subregion i in region n (write '1' to clear)
#define SR(i)   (1<<i)
#define SR0     0x00000001
#define SR1     0x00000002
#define SR2     0x00000004
#define SR3     0x00000008
#define SR4     0x00000010
#define SR5     0x00000020
#define SR6     0x00000040
#define SR7     0x00000080
#define SR8     0x00000100
#define SR9     0x00000200
#define SR10    0x00000400
#define SR11    0x00000800
#define SR12    0x00001000
#define SR13    0x00002000
#define SR14    0x00004000
#define SR15    0x00008000
#define SR16    0x00010000
#define SR17    0x00020000
#define SR18    0x00040000
#define SR19    0x00080000
#define SR20    0x00100000
#define SR21    0x00200000
#define SR22    0x00400000
#define SR23    0x00800000
#define SR24    0x01000000
#define SR25    0x02000000
#define SR26    0x04000000
#define SR27    0x08000000
#define SR28    0x10000000
#define SR29    0x20000000
#define SR30    0x40000000
#define SR31    0x80000000

/* read access detected while corresponding subregion was enabled for watching */
#define PERREGION0SUBSTATRA MMIO32(MWU + 0x404)
#define PERREGION1SUBSTATRA MMIO32(MWU + 0x40c)
// Subregion i in region n (write '1' to clear)


/* Enable/disable regions watch */
#define REGIONEN            MMIO32(MWU + 0x510)
// Enable/disable write access watch in region[0]
#define RGN0WA  0x01
// Enable/disable read access watch in region[0]
#define RGN0RA  0x02
// Enable/disable write access watch in region[1]
#define RGN1WA  0x04
// Enable/disable read access watch in region[1]
#define RGN1RA  0x08
// Enable/disable write access watch in region[2]
#define RGN2WA  0x10
// Enable/disable read access watch in region[2]
#define RGN2RA  0x20
// Enable/disable write access watch in region[3]
#define RGN3WA  0x40
// Enable/disable read access watch in region[3]
#define RGN3RA  0x80
// Enable/disable write access watch in PREGION[0]
#define PRGN0WA 0x1000000
// Enable/disable read access watch in PREGION[0]
#define PRGN0RA 0x2000000
// Enable/disable write access watch in PREGION[1]
#define PRGN1WA 0x4000000
// Enable/disable read access watch in PREGION[1]
#define PRGN1RA 0x8000000

/* Enable regions watch */
#define REGIONENSET         MMIO32(MWU + 0x514)
// Enable write access watch in region[0]
// Enable read access watch in region[0]
// Enable write access watch in region[1]
// Enable read access watch in region[1]
// Enable write access watch in region[2]
// Enable read access watch in region[2]
// Enable write access watch in region[3]
// Enable read access watch in region[3]
// Enable write access watch in PREGION[0]
// Enable read access watch in PREGION[0]
// Enable write access watch in PREGION[1]
// Enable read access watch in PREGION[1]

/* Disable regions watch */
#define REGIONENCLR         MMIO32(MWU + 0x518)
// Disable write access watch in region[0]
// Disable read access watch in region[0]
// Disable write access watch in region[1]
// Disable read access watch in region[1]
// Disable write access watch in region[2]
// Disable read access watch in region[2]
// Disable write access watch in region[3]
// Disable read access watch in region[3]
// Disable write access watch in PREGION[0]
// Disable read access watch in PREGION[0]
// Disable write access watch in PREGION[1]
// Disable read access watch in PREGION[1]

/* Start address for region n */
#define REGION0START        MMIO32(MWU + 0x600)
#define REGION0START        MMIO32(MWU + 0x610)
#define REGION0START        MMIO32(MWU + 0x620)
#define REGION0START        MMIO32(MWU + 0x630)
// Start address for region

/* End address of region n */
#define REGION0END          MMIO32(MWU + 0x604)
#define REGION0END          MMIO32(MWU + 0x614)
#define REGION0END          MMIO32(MWU + 0x624)
#define REGION0END          MMIO32(MWU + 0x634)
// End address of region.

/* Reserved for future use */
#define PREGION0START       MMIO32(MWU + 0x6c0)
#define PREGION1START       MMIO32(MWU + 0x6d0)
#define PREGION0END         MMIO32(MWU + 0x6c4)
#define PREGION0END         MMIO32(MWU + 0x6d4)

/* Subregions of region 0 */
#define PREGION0SUBS        MMIO32(MWU + 0x6c8)
#define PREGION0SUBS        MMIO32(MWU + 0x6d8)
// Include or exclude subregion i in region

#endif
