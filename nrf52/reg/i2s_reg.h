#ifndef H_I2S_REG
#define H_I2S_REG
/*
 * Standard library for Nordic NRF52
 * I2S (Inter-IC Sound) interface module registers.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/*************Tasks************************/
/* Starts continuous I2S transfer. Also starts MCK generator */
#define STARTI2S        MMIO32(I2S + 0x000)
/* Stops I2S transfer. Also stops MCK generator. */
#define STOPI2S         MMIO32(I2S + 0x004)

/************Events************************/
/* generated for every RXTXD.MAXCNT words */
#define RXPTRUPDI2S     MMIO32(I2S + 0x104)
/* I2S transfer stopped. */
#define STOPPEDI2S      MMIO32(I2S + 0x108)
/* generated for every RXTXD.MAXCNT words */
#define TXPTRUPDI2S     MMIO32(I2S + 0x114)

/************Registers*********************/
/* Enable or disable interrupt */
#define INTENI2S        MMIO32(I2S + 0x300)
// Enable or disable interrupt for event RXPTRUPD
#define RXPTRUPD    0x02
// Enable or disable interrupt for event STOPPED
#define STOPPED     0x04
// Enable or disable interrupt for event TXPTRUPD
#define TXPTRUPD    0x20

/* Enable interrupt */
#define INTENSETI2S     MMIO32(I2S + 0x304)
// Write '1' to enable interrupt for event RXPTRUPD
// Write '1' to enable interrupt for event STOPPED
// Write '1' to enable interrupt for event TXPTRUPD

/* Disable interrupt */
#define INTENCLRI2S     MMIO32(I2S + 0x308)
// Write '1' to disable interrupt for event RXPTRUPD
// Write '1' to disable interrupt for event STOPPED
// Write '1' to disable interrupt for event TXPTRUPD

/* Enable I2S module. */
#define ENABLEI2S       MMIO32(I2S + 0x500)
// Enable I2S module.
#define ENABLE_I2S  0x1

/* I2S mode. */
#define CONFIG_MODE     MMIO32(I2S + 0x504)
// I2S mode.
#define MODE_SLAVE  0x1
#define MODE_MASTER 0x0

/* Reception (RX) enable. */
#define CONFIG_RXEN     MMIO32(I2S + 0x508)
// Reception (RX) enable.
#define RXEN_I2S    0x1

/* Transmission (TX) enable. */
#define CONFIG_TXEN     MMIO32(I2S + 0x50c)
// Transmission (TX) enable.
#define TXEN_I2S    0x1

/* Master clock generator enable. */
#define CONFIG_MCKEN    MMIO32(I2S + 0x510)
// Master clock generator running and MCK output on PSEL.MCK.
#define MCKEN   0x1

/* Master clock generator frequency. */
#define CONFIG_MCKFREQ  MMIO32(I2S + 0x514)
// Master clock generator frequency.
#define MCKFREQ32MDIV8      0x20000000
#define MCKFREQ32MDIV10     0x18000000
#define MCKFREQ32MDIV11     0x16000000
#define MCKFREQ32MDIV15     0x11000000
#define MCKFREQ32MDIV16     0x10000000
#define MCKFREQ32MDIV21     0x0c000000
#define MCKFREQ32MDIV23     0x0b000000
#define MCKFREQ32MDIV30     0x08800000
#define MCKFREQ32MDIV31     0x08400000
#define MCKFREQ32MDIV32     0x08000000
#define MCKFREQ32MDIV42     0x06000000
#define MCKFREQ32MDIV63     0x04100000
#define MCKFREQ32MDIV125    0x020c0000

/* MCK/LRCK ratio. */
#define CONFIG_RATIO    MMIO32(I2S + 0x518)
// MCK / LRCK ratio.
#define RATIO32X    0x0
#define RATIO48X    0x1
#define RATIO64X    0x2
#define RATIO96X    0x3
#define RATIO128X   0x4
#define RATIO192X   0x5
#define RATIO256X   0x6
#define RATIO384X   0x7
#define RATIO512X   0x8

/* Sample width. */
#define CONFIG_SWIDTH   MMIO32(I2S + 0x51c)
// Sample width.
#define SWIDTH8BIT  0x0
#define SWIDTH16BIT 0x1
#define SWIDTH24BIT 0x2

/* Alignment of sample within a frame. */
#define CONFIG_ALIGN    MMIO32(I2S + 0x520)
// Left-aligned. or Right-aligned.
#define ALIGN_LEFT  0x0
#define ALIGN_RIGHT 0x1

/* Frame format. */
#define CONFIG_FORMAT   MMIO32(I2S + 0x524)
// Original I2S or Alternate format.
#define FORMAT_I2S      0x0
#define FORMAT_ALIGNED  0x1

/* Enable channels. */
#define CONFIG_CHANNELS MMIO32(I2S + 0x528)
// Enable channels.
#define CHANNELS_STEREO 0x0
#define CHANNELS_LEFT   0x1
#define CHANNELS_RIGHT  0x2

/* Receive buffer RAM start address. */
#define RXD_PTR         MMIO32(I2S + 0x538)
// Receive buffer Data RAM start address. PTR[0:31]

/* Transmit buffer RAM start address. */
#define TXD_PTR         MMIO32(I2S + 0x540)
// Transmit buffer Data RAM start address. PTR[0:31]

/* Size of RXD and TXD buffers. */
#define RXTXD_MAXCNT    MMIO32(I2S + 0x550)
// Size of RXD and TXD buffers in number of 32 bit words.
#define MAXCNT_MSK  0x3fff

/* Pin select for MCK signal. */
#define PSEL_MCK        MMIO32(I2S + 0x560)
// Pin number
#define PINI_MSK    0x1f
// Port number
#define PORTI       0x20
// Connection
#define DISCONNECT  0x80000000

/* Pin select for SCK signal. */
#define PSEL_SCK        MMIO32(I2S + 0x564)
// Pin number
// Port number
// Connection

/* Pin select for LRCK signal. */
#define PSEL_LRCK       MMIO32(I2S + 0x568)
// Pin number
// Port number
// Connection

/* Pin select for SDIN signal. */
#define PSEL_SDIN       MMIO32(I2S + 0x56c)
// Pin number
// Port number
// Connection

/* Pin select for SDOUT signal. */
#define PSEL_SDOUT      MMIO32(I2S + 0x570)
// Pin number
// Port number
// Connection

#endif
