#ifndef H_CTRL_AP_REG
#define H_CTRL_AP_REG
/*
 * Standard library for Nordic NRF52
 * Macro definitions for control access port (CTRL-AP) Registers
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Soft reset triggered through CTRL-AP */
#define RESETCTRL_AP            MMIO32(0x000)
// Reset is active. Device is held in reset.
#define RESET_CTRL_AP   0x1

/* Erase all */
#define ERASEALLCTRL_AP         MMIO32(0x004)
// Erase all flash and RAM
#define ERASEALL        0x1

/* Status register for the ERASEALL operation */
#define ERASEALLSTATUSCTRL_AP   MMIO32(0x008)
// ERASEALL is busy (on-going)
#define ERASEALLSTATUS  0x1

/* Status register for access port protection */
#define APPROTECTSTATUSCTRL_AP  MMIO32(0x00c)
// Access port protection not enabled
#define APPROTECTSTATUS

/* CTRL-AP identification register, IDR */
#define IDRCTRL_AP              MMIO32(0x0fc)
// AP identification
#define APID_MSK        0x000000ff
#define APID_GET        (IDRCTRL_AP&APID_MSK)
// Access port (AP) class
#define CLASS_MSK       0x0000f000
#define CLASS_SFT       13
#define CLASS_GET       ((IDRCTRL_AP>>CLASS_SFT)&0xf)
#define MEMAP           0x8000
#define NOT_DEFINED     0x0000
// JEDEC JEP106 identity code
#define JEP106ID_MSK    0x00ff0000
#define JEP106ID_SFT    17
#define JEP106ID_GET    ((IDRCTRL_AP>>JEP106ID_SFT)&0xff)
// JEDEC JEP106 continuation code
#define JEP106CONT_MSK  0x0f000000
#define JEP106CONT_SFT  24
#define JEP106CONT_GET  ((IDRCTRL_AP>>JEP106CONT_SFT)&0xf)
// Revision
#define REVISION_MSK    0xf0000000
#define REVISION_SFT    28
#define REVISION_GET    ((IDRCTRL_AP>>REVISION_SFT)&0xf)
