#ifndef H_RADIO_REG
#define H_RADIO_REG
/*
 * Standard library for Nordic NRF52
 * Register macro for 2.4 GHz Radio
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"
#include "pattern.h"

/*************Tasks************************/
/* Enable RADIO in TX mode */
#define TXENRADIO           MMIO32(RADIO + 0x000)
/* Enable RADIO in RX mode */
#define RXENRADIO           MMIO32(RADIO + 0x004)
/* Start RADIO */
#define STARTRADIO          MMIO32(RADIO + 0x008)
/* Stop RADIO */
#define STOPRADIO           MMIO32(RADIO + 0x00c)
/* Disable RADIO */
#define DISABLERADIO        MMIO32(RADIO + 0x010)
/* Start the RSSI and take one single sample of the receive signal strength */
#define RSSISTARTRADIO      MMIO32(RADIO + 0x014)
/* Stop the RSSI measurement */
#define RSSISTOPRADIO       MMIO32(RADIO + 0x018)
/* Start the bit counter */
#define BCSTARTRADIO        MMIO32(RADIO + 0x01c)
/* Stop the bit counter */
#define BCSTOPRADIO         MMIO32(RADIO + 0x020)
/* Stop the bit counter */
#define EDSTARTRADIO        MMIO32(RADIO + 0x024)
/* Stop the bit counter */
#define EDSTOPRADIO         MMIO32(RADIO + 0x028)
/* Stop the bit counter */
#define CCASTARTRADIO       MMIO32(RADIO + 0x02c)
/* Stop the bit counter */
#define CCASTOPRADIO        MMIO32(RADIO + 0x030)

/************Events************************/
/* RADIO has ramped up and is ready to be started */
#define READYRADIO          MMIO32(RADIO + 0x100)
/* Address sent or received */
#define ADDRESSRADIO        MMIO32(RADIO + 0x104)
/* Packet payload sent or received */
#define PAYLOADRADIO        MMIO32(RADIO + 0x108)
/* Packet sent or received */
#define ENDRADIO            MMIO32(RADIO + 0x10c)
/* RADIO has been disabled */
#define DISABLEDRADIO       MMIO32(RADIO + 0x110)
/* A device address match occurred on the last received packet */
#define DEVMATCHRADIO       MMIO32(RADIO + 0x114)
/* No device address match occurred on the last received packet */
#define DEVMISSRADIO        MMIO32(RADIO + 0x118)
/* Sampling of receive signal strength complete. */
#define RSSIENDRADIO        MMIO32(RADIO + 0x11c)
/* Bit counter reached bit count value */
#define BCMATCHRADIO        MMIO32(RADIO + 0x128)
/* Packet received with CRC ok */
#define CRCOKRADIO          MMIO32(RADIO + 0x130)
/* Packet received with CRC error */
#define CRCERRORRADIO       MMIO32(RADIO + 0x134)
/* IEEE 802.15.4 length field received */
#define FRAMESTARTRADIO     MMIO32(RADIO + 0x138)
/* Sampling of energy detection complete. */
#define EDENDRADIO          MMIO32(RADIO + 0x13c)
/* The sampling of energy detection has stopped */
#define EDSTOPPEDRADIO      MMIO32(RADIO + 0x140)
/* Wireless medium in idle - clear to send */
#define CCAIDLERADIO        MMIO32(RADIO + 0x144)
/* Wireless medium in idle - clear to send */
#define CCABUSYRADIO        MMIO32(RADIO + 0x148)
/* Wireless medium busy - do not send */
#define CCASTOPPEDRADIO     MMIO32(RADIO + 0x14c)
/* The CCA has stopped */
#define RATEBOOSTRADIO      MMIO32(RADIO + 0x150)
/* Ble_LR CI field rx, rx mode is ch from Ble_LR125Kbit to Ble_LR500Kbit. */
#define TXREADYRADIO        MMIO32(RADIO + 0x154)
/* RADIO has ramped up and is ready to be started RX path */
#define RXREADYRADIO        MMIO32(RADIO + 0x158)
/* MAC header match found */
#define MHRMATCHRADIO       MMIO32(RADIO + 0x15c)
/* Generated when last bit is sent on air */
#define PHYENDRADIO         MMIO32(RADIO + 0x16c)
/* CTE is present (early warning right after receiving CTEInfo byte) */
#define CTEPRESENTRADIO     MMIO32(RADIO + 0x170)

/************Registers*********************/
/* Shortcut register */
#define SHORTSRADIO         MMIO32(RADIO + 0x200)
// Shortcut between READY event and START task
#define READY_START         0x000001
// Shortcut between END event and DISABLE task
#define END_DISABLE         0x000002
// Shortcut between DISABLED event and TXEN task
#define DISABLED_TXEN       0x000004
// Shortcut between DISABLED event and RXEN task
#define DISABLED_RXEN       0x000008
// Shortcut between ADDRESS event and RSSISTART task
#define ADDRESS_RSSISTART   0x000010
// Shortcut between END event and START task
#define END_START           0x000020
// Shortcut between ADDRESS event and BCSTART task
#define ADDRESS_BCSTART     0x000040
// Shortcut between DISABLED event and RSSISTOP task
#define DISABLED_RSSISTOP   0x000100
// Shortcut between event RXREADY and task CCASTART
#define RXREADY_CCASTART    0x000800
// Shortcut between event CCAIDLE and task TXEN
#define CCAIDLE_TXEN        0x001000
// Shortcut between event CCABUSY and task DISABLE
#define CCABUSY_DISABLE     0x002000
// Shortcut between event FRAMESTART and task BCSTART
#define FRAMESTART_BCSTART  0x004000
// Shortcut between event READY and task EDSTART
#define READY_EDSTART       0x008000
// Shortcut between event EDEND and task DISABLE
#define EDEND_DISABLE       0x010000
// Shortcut between event CCAIDLE and task STOP
#define CCAIDLE_STOP        0x020000
// Shortcut between event TXREADY and task START
#define TXREADY_START       0x040000
// Shortcut between event RXREADY and task START
#define RXREADY_START       0x080000
// Shortcut between event PHYEND and task DISABLE
#define PHYEND_DISABLE      0x100000
// Shortcut between event PHYEND and task START
#define PHYEND_START        0x200000

/* Enable interrupt */
#define INTENSETRADIO       MMIO32(RADIO + 0x304)
// Write '1' to Enable interrupt on READY event.
#define READYR      0x00000001
// Write '1' to Enable interrupt on ADDRESS event.
#define ADDRESSR    0x00000002
// Write '1' to Enable interrupt on PAYLOAD event.
#define PAYLOADR    0x00000004
// Write '1' to Enable interrupt on END event.
#define ENDR        0x00000008
// Write '1' to Enable interrupt on DISABLED event.
#define DISABLEDR   0x00000010
// Write '1' to Enable interrupt on DEVMATCH event.
#define DEVMATCHR   0x00000020
// Write '1' to Enable interrupt on DEVMISS event.
#define DEVMISSR    0x00000040
// Write '1' to Enable interrupt on RSSIEND event.
#define RSSIENDR    0x00000080
// Write '1' to Enable interrupt on BCMATCH event.
#define BCMATCHR    0x00000400
// Write '1' to enable interrupt for event CRCOK
#define CRCOKR      0x00001000
// Write '1' to enable interrupt for event CRCERROR
#define CRCERRORR   0x00002000
// Write '1' to enable interrupt for event FRAMESTART
#define FRAMESTARTR 0x00004000
// Write '1' to enable interrupt for event EDEND
#define EDENDR      0x00008000
// Write '1' to enable interrupt for event EDSTOPPED
#define EDSTOPPEDR  0x00010000
// Write '1' to enable interrupt for event CCAIDLE
#define CCAIDLER    0x00020000
// Write '1' to enable interrupt for event CCABUSY
#define CCABUSYR    0x00040000
// Write '1' to enable interrupt for event CCASTOPPED
#define CCASTOPPEDR 0x00080000
// Write '1' to enable interrupt for event RATEBOOST
#define RATEBOOSTR  0x00100000
// Write '1' to enable interrupt for event TXREADY
#define TXREADYR    0x00200000
// Write '1' to enable interrupt for event RXREADY
#define RXREADYR    0x00400000
// Write '1' to enable interrupt for event MHRMATCH
#define MHRMATCHR   0x00800000
// Write '1' to enable interrupt for event PHYEND
#define PHYENDR     0x08000000
// Write '1' to enable interrupt for event CTEPRESENT
#define CTEPRESENTR 0x10000000

/* Disable interrupt */
#define INTENCLRRADIO       MMIO32(RADIO + 0x308)
// Write '1' to Clear interrupt on READY event.
// Write '1' to Clear interrupt on ADDRESS event.
// Write '1' to Clear interrupt on PAYLOAD event.
// Write '1' to Clear interrupt on END event.
// Write '1' to Clear interrupt on DISABLED event.
// Write '1' to Clear interrupt on DEVMATCH event.
// Write '1' to Clear interrupt on DEVMISS event.
// Write '1' to Clear interrupt on RSSIEND event.
// Write '1' to Clear interrupt on BCMATCH event.
// Write '1' to disable interrupt for event CRCOK
// Write '1' to disable interrupt for event CRCERROR
// Write '1' to disable interrupt for event FRAMESTART
// Write '1' to disable interrupt for event EDEND
// Write '1' to disable interrupt for event EDSTOPPED
// Write '1' to disable interrupt for event CCAIDLE
// Write '1' to disable interrupt for event CCABUSY
// Write '1' to disable interrupt for event CCASTOPPED
// Write '1' to disable interrupt for event RATEBOOST
// Write '1' to disable interrupt for event TXREADY
// Write '1' to disable interrupt for event RXREADY
// Write '1' to disable interrupt for event MHRMATCH
// Write '1' to disable interrupt for event PHYEND
// Write '1' to disable interrupt for event CTEPRESENT

/* CRC status */
#define CRCSTATUSRADIO      MMIO32(RADIO + 0x400)
// CRC status of packet received
#define CRCSTATUSR   0x1

/* Received address */
#define RXMATCHRADIO        MMIO32(RADIO + 0x408)
// Logical address of which previous packet was received. RXMATCH[2:0]

/* CRC field of previously received packet */
#define RXCRCRADIO          MMIO32(RADIO + 0x40c)
// CRC field of previously received packet. RXCRC[23:0]

/* Device address match index */
#define DAIRADIO            MMIO32(RADIO + 0x410)
// Device address match index. DAI[2:0]

/* Payload status */
#define PDUSTATRADIO        MMIO32(RADIO + 0x414)
// Status on payload length vs. PCNF1.MAXLEN
#define PDUSTAT         0x1
// Status on what rate packet is received with in Long Range
#define CISTAT_LR125K   0x0
#define CISTAT_LR500K   0x2

/* CTEInfo parsed from received packet */
#define CTESTATUSRADIO      MMIO32(RADIO + 0x44c)
// CTETime parsed from packet
#define CTETIME_MSK 0x1f
#define CTETIME_GET (CTESTATUSRADIO&CTETIME_MSK)
// RFU parsed from packet
#define RFU         0x20
// CTEType parsed from packet
#define CTETYPE_MSK 0xc0
#define CTETYPE_SFT 6
#define CTETYPE_GET ((CTESTATUSRADIO>>CTETYPE_SFT)&0x3)

/* DFE status information */
#define DFESTATUSRADIO      MMIO32(RADIO + 0x458)
// Internal state of switching state machine
#define SWITCHINGSTATE_IDLE         0x00
#define SWITCHINGSTATE_OFFSET       0x01
#define SWITCHINGSTATE_GUARD        0x02
#define SWITCHINGSTATE_REF          0x03
#define SWITCHINGSTATE_SWITCHING    0x04
#define SWITCHINGSTATE_ENDING       0x05
// Internal state of sampling state machine
#define SAMPLINGSTATE               0x10

/* Packet pointer */
#define PACKETPTRRADIO      MMIO32(RADIO + 0x504)
// Packet address to be used for the next transmission or reception. PACKETPTR[]

/* Frequency */
#define FREQUENCYRADIO      MMIO32(RADIO + 0x508)
// Radio channel frequency.
#define FREQUENCY_MHZ(n)    (n+2400)
// Channel map selection.
#define MAP                 0x100

/* Output power */
#define TXPOWERRADIO        MMIO32(RADIO + 0x50c)
// RADIO output power in number of dBm.
#define TXPOWER_P8DBM   0x08
#define TXPOWER_P7DBM   0x07
#define TXPOWER_P6DBM   0x06
#define TXPOWER_P5DBM   0x05
#define TXPOWER_P4DBM   0x04
#define TXPOWER_P3DBM   0x03
#define TXPOWER_P2DBM   0x02
#define TXPOWER_0DBM    0x00
#define TXPOWER_M4DBM   0xfc
#define TXPOWER_M8DBM   0xf8
#define TXPOWER_M12DBM  0xf4
#define TXPOWER_M16DBM  0xf0
#define TXPOWER_M20DBM  0xec
#define TXPOWER_M30DBM  0xe2
#define TXPOWER_M40DBM  0xd8

/* Data rate and modulation */
#define MODERADIO           MMIO32(RADIO + 0x510)
// Radio data rate and modulation setting.
#define MODE_Nrf_1Mbit      0x0
#define MODE_Nrf_2Mbit      0x1
#define MODE_Ble_1Mbit      0x3
#define MODE_Ble_2Mbit      0x4
#define MODE_LR125Kbit      0x5
#define MODE_LR500Kbit      0x6
#define MODE_IEEE802154_250 0xf

/* Packet configuration register 0 */
#define PCNF0RADIO          MMIO32(RADIO + 0x514)
// Length on air of LENGTH field in number of bits.
#define LFLEN_SFT   0
#define LFLEN_MSK   0xf
// Length on air of S0 field in number of bytes.
#define S0LEN_SFT   8
#define S0LEN_MSK   0x1
// Length on air of S1 field in number of bits.
#define S1LEN_SFT   16
#define S1LEN_MSK   0xf

/* Packet configuration register 1 */
#define PCNF1RADIO          MMIO32(RADIO + 0x518)
// Maximum length of packet payload.
#define MAXLEN_SFT  0
#define MAXLEN_MSK  0xff
// Static length in number of bytes
#define STATLEN_SFT 8
#define STATLEN_MSK 0xff
// Base address length in number of bytes
#define BALEN_SFT   16
#define BALEN_MSK   0x7
// On air endianness of packet. Little = 0. Big = 1.
#define ENDIAN      0x1000000
// Enable or disable packet whitening.
#define WHITEENR    0x2000000

/* Base address n */
#define BASE0RADIO          MMIO32(RADIO + 0x51c)
#define BASE1RADIO          MMIO32(RADIO + 0x520)
// Radio base address n. BASE0[31:0]

/* Prefixes bytes for logical addresses 0-3 */
#define PREFIX0RADIO        MMIO32(RADIO + 0x524)
// Address prefix n
#define AP_MSK  0xff
#define AP0_SFT 0
#define AP1_SFT 8
#define AP2_SFT 16
#define AP3_SFT 24

/* Prefixes bytes for logical addresses 4-7 */
#define PREFIX1RADIO        MMIO32(RADIO + 0x528)
// Address prefix n
#define AP4_SFT 0
#define AP5_SFT 8
#define AP6_SFT 16
#define AP7_SFT 24

/* Transmit address select */
#define TXADDRESSRADIO      MMIO32(RADIO + 0x52c)
// Logical address to be used when transmitting a packet. TXADDRESS[2:0]

/* Receive address select */
#define RXADDRESSESRADIO    MMIO32(RADIO + 0x530)
// Enable or disable reception on logical address 0.
#define ADDR0   0x01
// Enable or disable reception on logical address 1.
#define ADDR1   0x02
// Enable or disable reception on logical address 2.
#define ADDR2   0x04
// Enable or disable reception on logical address 3.
#define ADDR3   0x08
// Enable or disable reception on logical address 4.
#define ADDR4   0x10
// Enable or disable reception on logical address 5.
#define ADDR5   0x20
// Enable or disable reception on logical address 6.
#define ADDR6   0x40
// Enable or disable reception on logical address 7.
#define ADDR7   0x80

/* CRC configuration */
#define CRCCNFRADIO         MMIO32(RADIO + 0x534)
// CRC length in number of bytes.
#define LEN_DIS             0x000
#define LEN_ONE             0x001
#define LEN_TWO             0x002
#define LEN_THREE           0x003
// Include or exclude packet address field out of CRC calculation.
#define SKIPADDR_INCLUDE    0x000
#define SKIPADDR_SKIP       0x100
#define SKIPADDR_IEEE       0x200

/* CRC polynomial */
#define CRCPOLYRADIO        MMIO32(RADIO + 0x538)
// CRC polynomial. ex for 8 bit CRC poly: x8 + x7 + x3 + x2 + 1 = 1 1000 1101.

/* CRC initial value */
#define CRCINITRADIO        MMIO32(RADIO + 0x53c)
// Initial value for CRC calculation. CRCINIT[23:0]

/* Inter Frame Spacing in us */
#define TIFSRADIO           MMIO32(RADIO + 0x544)
// Inter Frame Spacing in us. TIFS[7:0]

/* RSSI sample */
#define RSSISAMPLERADIO     MMIO32(RADIO + 0x548)
// RSSI sample result. RSSISAMPLE[6:0]

/* Current radio state */
#define STATERADIO          MMIO32(RADIO + 0x550)
// Current radio state
#define STATE_DISABLED      0x0
#define STATE_RXRU          0x1
#define STATE_RXIDLE        0x2
#define STATE_RX            0x3
#define STATE_RXDISABLED    0x4
#define STATE_TXRU          0x9
#define STATE_TXIDLE        0xa
#define STATE_TX            0xb
#define STATE_TXDISABLED    0xc

/* Data whitening initial value */
#define DATAWHITEIVRADIO    MMIO32(RADIO + 0x554)
// Data whitening initial value.
#define DATAWHITEIV6    0x01
#define DATAWHITEIV5    0x02
#define DATAWHITEIV4    0x04
#define DATAWHITEIV3    0x08
#define DATAWHITEIV2    0x10
#define DATAWHITEIV1    0x20

/* Bit counter compare */
#define BCCRADIO            MMIO32(RADIO + 0x560)
// Bit counter compare. BCC[31:0]

/* Device address base segment n */
#define DAB0RADIO           MMIO32(RADIO + 0x600)
#define DAB1RADIO           MMIO32(RADIO + 0x604)
#define DAB2RADIO           MMIO32(RADIO + 0x608)
#define DAB3RADIO           MMIO32(RADIO + 0x60c)
#define DAB4RADIO           MMIO32(RADIO + 0x610)
#define DAB5RADIO           MMIO32(RADIO + 0x614)
#define DAB6RADIO           MMIO32(RADIO + 0x618)
#define DAB7RADIO           MMIO32(RADIO + 0x61c)
// Device address base segment n. DAB[31:0]

/* Device address prefix n */
#define DAP0RADIO           MMIO32(RADIO + 0x620)
#define DAP1RADIO           MMIO32(RADIO + 0x624)
#define DAP2RADIO           MMIO32(RADIO + 0x628)
#define DAP3RADIO           MMIO32(RADIO + 0x62c)
#define DAP4RADIO           MMIO32(RADIO + 0x630)
#define DAP5RADIO           MMIO32(RADIO + 0x634)
#define DAP6RADIO           MMIO32(RADIO + 0x638)
#define DAP7RADIO           MMIO32(RADIO + 0x63c)
// Device address prefix n. DAP[15:0]

/* Device address match configuration */
#define DACNFRADIO          MMIO32(RADIO + 0x640)
// Enable or disable device address matching using device address 0
#define ENA0     0x0001
// Enable or disable device address matching using device address 1
#define ENA1     0x0002
// Enable or disable device address matching using device address 2
#define ENA2     0x0004
// Enable or disable device address matching using device address 3
#define ENA3     0x0008
// Enable or disable device address matching using device address 4
#define ENA4     0x0010
// Enable or disable device address matching using device address 5
#define ENA5     0x0020
// Enable or disable device address matching using device address 6
#define ENA6     0x0040
// Enable or disable device address matching using device address 7
#define ENA7     0x0080
// TxAdd for device address 0
#define TXADD0   0x0100
// TxAdd for device address 1
#define TXADD1   0x0200
// TxAdd for device address 2
#define TXADD2   0x0400
// TxAdd for device address 3
#define TXADD3   0x0800
// TxAdd for device address 4
#define TXADD4   0x1000
// TxAdd for device address 5
#define TXADD5   0x2000
// TxAdd for device address 6
#define TXADD6   0x4000
// TxAdd for device address 7
#define TXADD7   0x8000

/* Search pattern configuration */
#define MHRMATCHCONFRADIO   MMIO32(RADIO + 0x644)
// Search pattern configuration MHRMATCHCONF[31:0]

/* Pattern mask */
#define MHRMATCHMASRADIO    MMIO32(RADIO + 0x648)
// Pattern mask MHRMATCHMAS[31:0]

/* Radio mode configuration register 0 */
#define MODECNF0RADIO       MMIO32(RADIO + 0x650)
// Radio ramp-up time
#define RU_FAST     0x001
// Default TX value
#define DTX_B1      0x000
#define DTX_B0      0x100
#define DTX_CENTER  0x200

/* IEEE 802.15.4 start of frame delimiter */
#define SFDRADIO            MMIO32(RADIO + 0x660)
// SFD[7:0]

/* IEEE 802.15.4 energy detect loop count */
#define EDCNTRADIO          MMIO32(RADIO + 0x664)
// EDCNT[20:0]

/* IEEE 802.15.4 energy detect level */
#define EDSAMPLERADIO       MMIO32(RADIO + 0x668)
// Register value must be converted to IEEE 802.15.4 range EDLVL[0:127]

/* IEEE 802.15.4 clear channel assessment control */
#define CCACTRLRADIO        MMIO32(RADIO + 0x66c)
// CCA mode of operation
#define CCAMODE_EdMode              0x00000000
#define CCAMODE_CarrierMode         0x00000001
#define CCAMODE_CarrierAndEdMode    0x00000002
#define CCAMODE_CarrierOrEdMode     0x00000003
#define CCAMODE_EdModeTest1         0x00000004
// CCA energy busy threshold.
#define CCAEDTHRES_MSK              0x0000ff00
#define CCAEDTHRES_SFT              8
#define CCAEDTHRES_GET              ((CCACTRLRADIO>>CCAEDTHRES_SFT)&0xff)
#define CCAEDTHRES_SET(x)           ((x<<CCACORRTHRES_SFT)&CCACORRTHRES_MSK)
// CCA correlator busy threshold.
#define CCACORRTHRES_MSK            0x00ff0000
#define CCACORRTHRES_SFT            16
#define CCACORRTHRES_GET            ((CCACTRLRADIO>>CCAEDTHRES_SFT)&0xff)
#define CCACORRTHRES_SET(x)         ((x<<CCACORRTHRES_SFT)&CCACORRTHRES_MSK)
// Limit for occurances above CCACORRTHRES.
#define CCACORRCNT_MSK              0xff000000
#define CCACORRCNT_SFT              24
#define CCACORRCNT_GET              ((CCACTRLRADIO>>CCAEDTHRES_SFT)&0xff)
#define CCACORRCNT_SET(x)           ((x<<CCACORRTHRES_SFT)&CCACORRTHRES_MSK)

/* Whether to use Angle-of-Arrival (AOA) or Angle-of-Departure (AOD) */
#define DFEMODERADIO        MMIO32(RADIO + 0x900)
// Direction finding operation mode
#define DFEOPMODE_DISABLED  0x0
#define DFEOPMODE_AOD       0x2
#define DFEOPMODE_AOA       0x3

/* Configuration for CTE inline mode */
#define CTEINLINECONFRADIO  MMIO32(RADIO + 0x904)
// Enable parsing of CTEInfo from received packet in BLE modes
#define CTEINLINECTRLEN             0x00000001
// CTEInfo is S1 byte or not
#define CTEINFOINS1                 0x00000008
// Sampling/switching if CRC is not OK
#define CTEERRORHANDLING            0x00000010
// Max range of CTETime
#define CTETIMEVALIDRANGE20         0x00000000
#define CTETIMEVALIDRANGE31         0x00000040
#define CTETIMEVALIDRANGE63         0x00000080
// Spacing between samples for the samples in the SWITCHING period
#define CTEINLINERXMODE1US_4US      0x00000400
#define CTEINLINERXMODE1US_2US      0x00000800
#define CTEINLINERXMODE1US_1US      0x00000c00
#define CTEINLINERXMODE1US_500NS    0x00001000
#define CTEINLINERXMODE1US_250NS    0x00001400
#define CTEINLINERXMODE1US_125NS    0x00001800
// Spacing between samples for the samples in the SWITCHING period
#define CTEINLINERXMODE2US_4US      0x00002000
#define CTEINLINERXMODE2US_2US      0x00004000
#define CTEINLINERXMODE2US_1US      0x00006000
#define CTEINLINERXMODE2US_500NS    0x00008000
#define CTEINLINERXMODE2US_250NS    0x0000a000
#define CTEINLINERXMODE2US_125NS    0x0000c000
// S0 bit pattern to match
#define S0CONF_MSK                  0x00ff0000
#define S0CONF_SFT                  16
#define S0CONF_GET                  ((CTEINLINECONFRADIO>>S0CONF_SFT)&0xff)
#define S0CONF_SET(x)               ((x<<S0CONF_SFT)&S0CONF_MSK)
// S0 bit mask to set which bit to match
#define S0MASK_MSK                  0xff000000
#define S0MASK_SFT                  24
#define S0MASK_GET                  ((CTEINLINECONFRADIO>>S0MASK_SFT)&0xff)
#define S0MASK_SET(x)               ((x<<S0MASK_SFT)&S0MASK_MSK)

/* Various configuration for Direction finding */
#define DFECTRL1RADIO       MMIO32(RADIO + 0x910)
// Length of the AoA/AoD procedure in number of 8 us units
#define NUMBEROF8US_MSK         0x000003f
#define NUMBEROF8US_GET         (DFECTRL1RADIO&NUMBEROF8US_MSK)
#define NUMBEROF8US_SET(x)      (x&NUMBEROF8US_MSK)
// Add CTE extension and do antenna switching/sampling in this extension
#define DFEINEXTENSION          0x0000080
// Interval between every time the antenna is changed in the SWITCHING state
#define TSWITCHSPACING4US       0x0000100
#define TSWITCHSPACING2US       0x0000200
#define TSWITCHSPACING1US       0x0000300
// Interval between samples in the REFERENCE period
#define TSAMPLESPACINGREF4US    0x0001000
#define TSAMPLESPACINGREF2US    0x0002000
#define TSAMPLESPACINGREF1US    0x0003000
#define TSAMPLESPACINGREF500NS  0x0004000
#define TSAMPLESPACINGREF250NS  0x0005000
#define TSAMPLESPACINGREF125NS  0x0006000
// Whether to sample I/Q or magnitude/phase
#define SAMPLETYPE              0x0008000
// Interval between samples in the SWITCHING period when CTEINLINECTRLEN is 0
#define TSAMPLESPACING4US       0x0010000
#define TSAMPLESPACING2US       0x0020000
#define TSAMPLESPACING1US       0x0030000
#define TSAMPLESPACING500NS     0x0040000
#define TSAMPLESPACING250NS     0x0050000
#define TSAMPLESPACING125NS     0x0060000
// Repeat every antenna pattern N times. 0=Do not repeat (1 time in total)
#define REPEATPATTERN_MSK       0x0f00000
#define REPEATPATTERN_SFT       20
#define REPEATPATTERN_GET       ((DFECTRL1RADIO>>REPEATPATTERN_SFT)&0xf)
#define REPEATPATTERN_SET(x)    ((x<<REPEATPATTERN_SFT)&REPEATPATTERN_MSK)
// Gain will be lowered by the specified number of gain steps
#define AGCBACKOFFGAIN_MSK      0xf000000
#define AGCBACKOFFGAIN_SFT      24
#define AGCBACKOFFGAIN_GET      ((DFECTRL1RADIO>>AGCBACKOFFGAIN_SFT)&0xf)
#define AGCBACKOFFGAIN_SET(x)   ((x<<AGCBACKOFFGAIN_SFT)&AGCBACKOFFGAIN_MSK)

/* Start offset for Direction finding */
#define DFECTRL2RADIO       MMIO32(RADIO + 0x914)
// Signed value offset after the end of the CRC before starting switching
#define TSWITCHOFFSET_MSK       0x0001fff
#define TSWITCHOFFSET_GET       (DFECTRL2RADIO&TSWITCHOFFSET_MSK)
#define TSWITCHOFFSET_SET(x)    (x&TSWITCHOFFSET_MSK)
// Signed value offset before starting sampling in number of 16M cycles
#define TSAMPLEOFFSET_MSK       0xfff0000
#define TSAMPLEOFFSET_SFT       16
#define TSAMPLEOFFSET_GET       ((DFECTRL2RADIO>>TSAMPLEOFFSET_SFT)&0xfff)
#define TSAMPLEOFFSET_SET(x)    ((x<<TSAMPLEOFFSET_SFT)&TSAMPLEOFFSET_MSK)

/* GPIO patterns to be used for each antenna */
#define SWITCHPATTERNRADIO  MMIO32(RADIO + 0x928)
// Fill array of GPIO patterns for antenna control
#define SWITCHPATTERN_MSK   0xff

/* Clear the GPIO pattern array for antenna control */
#define CLEARPATTERNRADIO   MMIO32(RADIO + 0x92c)
// Clear the GPIO pattern
#define CLEARPATTERN    0x1

/* Pin select for DFE pin n */
#define PSEL_DFEGPIO0RADIO  MMIO32(RADIO + 0x930)
#define PSEL_DFEGPIO1RADIO  MMIO32(RADIO + 0x934)
#define PSEL_DFEGPIO2RADIO  MMIO32(RADIO + 0x938)
#define PSEL_DFEGPIO3RADIO  MMIO32(RADIO + 0x93c)
#define PSEL_DFEGPIO4RADIO  MMIO32(RADIO + 0x940)
#define PSEL_DFEGPIO5RADIO  MMIO32(RADIO + 0x944)
#define PSEL_DFEGPIO6RADIO  MMIO32(RADIO + 0x948)
#define PSEL_DFEGPIO7RADIO  MMIO32(RADIO + 0x94c)
// Pin number
// Port number
// Connection

/* Data pointer */
#define DFEPACKET_PTRRADIO  MMIO32(RADIO + 0x950)
// Data pointer DFEPACKET.PTR[0:31]

/* Maximum number of buffer words to transfer */
#define DFEPACKET_MAXCNT    MMIO32(RADIO + 0x954)
// Maximum number of buffer words to transfer
#define DFEPACKET_MAXCNT_MSK    0x3fff

/* Number of samples transferred in the last transaction */
#define DFEPACKET_AMOUNT    MMIO32(RADIO + 0x958)
// Number of samples transferred in the last transaction
#define DFEPACKET_AMOUNT_MSK    0xffff

/* Peripheral power control */
#define POWERRADIO          MMIO32(RADIO + 0xffc)
// Peripheral power control.
#define POWERR  0x1

#endif
