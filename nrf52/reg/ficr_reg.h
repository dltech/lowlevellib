#ifndef H_FICR_REG
#define H_FICR_REG
/*
 * Standard library for Nordic NRF52
 * Register macro for Factory Information Configuration Registers
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/************Registers*********************/
/* Code memory page size */
#define CODEPAGESIZE    MMIO32(FICR + 0x10)
// Code memory page size. CODEPAGESIZE[31:0]

/* Code memory size */
#define CODESIZE        MMIO32(FICR + 0x14)
// Code memory size in number of pages CODESIZE[31:0]

/* Device identifier */
#define DEVICEID0       MMIO32(FICR + 0x60)
#define DEVICEID1       MMIO32(FICR + 0x64)
// 64 bit unique device identifier DEVICEID[31:0]

/* Encryption Root, word n */
#define ER0             MMIO32(FICR + 0x80)
#define ER1             MMIO32(FICR + 0x84)
#define ER2             MMIO32(FICR + 0x88)
#define ER3             MMIO32(FICR + 0x8c)
// Encryption Root, word n. ER[31:0]

/* Identity Root, word n */
#define IR0             MMIO32(FICR + 0x90)
#define IR1             MMIO32(FICR + 0x94)
#define IR2             MMIO32(FICR + 0x98)
#define ER3             MMIO32(FICR + 0x9c)
// Identity Root, word n. IR[31:0]

/* Device address type */
#define DEVICEADDRTYPE  MMIO32(FICR + 0xa0)
// Device address type
#define DEVICEADDRTYPE_PUBLIC   0
#define DEVICEADDRTYPE_RANDOM   1

/* Device address */
#define DEVICEADDR0     MMIO32(FICR + 0xa4)
#define DEVICEADDR1     MMIO32(FICR + 0xa8)
// 48 bit device address. DEVICEADDR[31:0]

/* Part code */
#define INFO_PART       MMIO32(FICR + 0x100)
// Part code
#define N52833      0x52833
#define N52840      0x52840
#define UNSPECIFIED 0xffffffff

/* Build code (hardware version and production configuration) */
#define INFO_VARIANT    MMIO32(FICR + 0x104)
// Build code (hardware version and production configuration). Encoded as ASCII.
#define AAAA    0x41414141
#define AAAB    0x41414142
// Unspecified 0xFFFFFFFF

/* Package option */
#define INFO_PACKAGE    MMIO32(FICR + 0x108)
// Package option
#define QD  0x2007  // QDxx - 40-pin QFN
#define QI  0x2004  // QIxx - 73-pin aQFN
#define CJ  0x2008  // CJxx - WLCSP
// Unspecified 0xFFFFFFFF

/* RAM variant */
#define INFO_RAM        MMIO32(FICR + 0x10c)
// RAM variant in kbytes
#define RAM_K16     0x010
#define RAM_K32     0x020
#define RAM_K64     0x040
#define RAM_K128    0x080
#define RAM_K256    0x100
// Unspecified 0xFFFFFFFF

/* Flash variant */
#define INFO_FLASH      MMIO32(FICR + 0x110)
// Flash variant in kbytes
#define FLASH_K128  0x080
#define FLASH_K256  0x100
#define FLASH_K512  0x200
#define FLASH_K1024 0x400
#define FLASH_K2048 0x800
// Unspecified 0xFFFFFFFF

/* Reserved */
#define INFO_UNUSED8_0  MMIO32(FICR + 0x114)
#define INFO_UNUSED8_1  MMIO32(FICR + 0x118)
#define INFO_UNUSED8_2  MMIO32(FICR + 0x11c)

/* Production test signature x */
#define PRODTEST0       MMIO32(FICR + 0x350)
#define PRODTEST1       MMIO32(FICR + 0x354)
#define PRODTEST2       MMIO32(FICR + 0x358)
// Production tests done or not done
#define DONE    0xbb42319f
#define MOTDONE 0xffffffff

/* Slope definition Ax */
#define TEMP_A0         MMIO32(FICR + 0x404)
#define TEMP_A1         MMIO32(FICR + 0x408)
#define TEMP_A2         MMIO32(FICR + 0x40c)
#define TEMP_A3         MMIO32(FICR + 0x410)
#define TEMP_A4         MMIO32(FICR + 0x414)
#define TEMP_A5         MMIO32(FICR + 0x418)
// A (slope definition) register.
#define TEMP_A_MSK  0xfff

/* Y-intercept Bx */
#define TEMP_B0         MMIO32(FICR + 0x41c)
#define TEMP_B1         MMIO32(FICR + 0x420)
#define TEMP_B2         MMIO32(FICR + 0x424)
#define TEMP_B3         MMIO32(FICR + 0x428)
#define TEMP_B4         MMIO32(FICR + 0x42c)
#define TEMP_B5         MMIO32(FICR + 0x430)
// B (y-intercept)
#define TEMP_B_MSK  0xfff

/* Segment end Tx */
#define TEMP_T0         MMIO32(FICR + 0x434)
#define TEMP_T1         MMIO32(FICR + 0x438)
#define TEMP_T2         MMIO32(FICR + 0x43c)
#define TEMP_T3         MMIO32(FICR + 0x440)
#define TEMP_T4         MMIO32(FICR + 0x444)
// T (segment end) register
#define TEMP_T_MSK  0xff

/* Default header for NFC tag. */
#define NFC_TAGHEADER0  MMIO32(FICR + 0x450)
// Default Manufacturer ID: Nordic Semiconductor ASA has ICM 0x5F
#define MFGID_MSK   0x000000ff
#define MFGID       0x5f
// Unique identifier byte 1
#define UD1_MSK     0x0000ff00
#define UD1_SFT     8
#define UD1_GET     ((NFC_TAGHEADER0<<UD1_SFT)&0xff)
// Unique identifier byte 2
#define UD2_MSK     0x00ff0000
#define UD2_SFT     16
#define UD2_GET     ((NFC_TAGHEADER0<<UD2_SFT)&0xff)
// Unique identifier byte 3
#define UD3_MSK     0xff000000
#define UD3_SFT     24
#define UD3_GET     ((NFC_TAGHEADER0<<UD3_SFT)&0xff)

#define NFC_TAGHEADER1  MMIO32(FICR + 0x454)
// Unique identifier byte 4
#define UD4_MSK     0x000000ff
#define UD4_GET     (NFC_TAGHEADER1&UD4_MSK)
// Unique identifier byte 5
#define UD5_MSK     0x0000ff00
#define UD5_SFT     8
#define UD5_GET     ((NFC_TAGHEADER1<<UD5_SFT)&0xff)
// Unique identifier byte 6
#define UD6_MSK     0x00ff0000
#define UD6_SFT     16
#define UD6_GET     ((NFC_TAGHEADER1<<UD6_SFT)&0xff)
// Unique identifier byte 7
#define UD7_MSK     0xff000000
#define UD7_SFT     24
#define UD7_GET     ((NFC_TAGHEADER1<<UD7_SFT)&0xff)

#define NFC_TAGHEADER2  MMIO32(FICR + 0x458)
// Unique identifier byte 4
#define UD8_MSK     0x000000ff
#define UD8_GET     (NFC_TAGHEADER2&UD8_MSK)
// Unique identifier byte 5
#define UD9_MSK     0x0000ff00
#define UD9_SFT     8
#define UD9_GET     ((NFC_TAGHEADER2<<UD9_SFT)&0xff)
// Unique identifier byte 6
#define UD10_MSK     0x00ff0000
#define UD10_SFT     16
#define UD10_GET     ((NFC_TAGHEADER2<<UD10_SFT)&0xff)
// Unique identifier byte 7
#define UD11_MSK     0xff000000
#define UD11_SFT     24
#define UD11_GET     ((NFC_TAGHEADER2<<UD11_SFT)&0xff)

#define NFC_TAGHEADER3  MMIO32(FICR + 0x45c)
// Unique identifier byte 4
#define UD12_MSK     0x000000ff
#define UD12_GET     (NFC_TAGHEADER3&UD14_MSK)
// Unique identifier byte 5
#define UD13_MSK     0x0000ff00
#define UD13_SFT     8
#define UD13_GET     ((NFC_TAGHEADER3<<UD14_SFT)&0xff)
// Unique identifier byte 6
#define UD14_MSK     0x00ff0000
#define UD14_SFT     16
#define UD14_GET     ((NFC_TAGHEADER3<<UD14_SFT)&0xff)
// Unique identifier byte 7
#define UD15_MSK     0xff000000
#define UD15_SFT     24
#define UD15_GET     ((NFC_TAGHEADER3<<UD15_SFT)&0xff)

#endif
