#ifndef H_UICR_REG
#define H_UICR_REG
/*
 * Standard library for Nordic NRF52
 * Macro definitions for User Information Configuration Registers
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/************Registers*********************/

/* Reserved for Nordic firmware design */
#define NRFFW(n)        MMIO32(UICR + 0x14 + n*4)
/* Reserved for Nordic hardware design */
#define NRFHW(n)        MMIO32(UICR + 0x50 + n*4)
/* Reserved for customer */
#define CUSTOMER(n)     MMIO32(UICR + 0x80 + n*4)

/* Mapping of the nRESET function (see POWER chapter for details) */
#define PSELRESET0      MMIO32(UICR + 0x200)
#define PSELRESET1      MMIO32(UICR + 0x204)
// GPIO pin number onto which nRESET is exposed
#define PIN_MSK 0x1f
// Port number onto which nRESET is exposed
#define PORT    0x20
// Connection
#define CONNECT 0x10000000

/* Access port protection */
#define APPROTECT       MMIO32(UICR + 0x208)
// Enable or disable access port protection.
#define PALL_EN     0xff
#define PALL_DIS    0x00

/* Setting of pins dedicated to NFC functionality: NFC antenna or GPIO */
#define NFCPINS         MMIO32(UICR + 0x20c)
// Setting of pins dedicated to NFC functionality
#define PROTECT_NFC 0x1

/* Processor debug control */
#define DEBUGCTRL       MMIO32(UICR + 0x210)
// Configure CPU non-intrusive debug features
#define CPUNIDEN    0x00ff
#define CPUNIDDIS   0x0000
// Configure CPU flash patch and breakpoint (FPB) unit behavior
#define CPUFPBEN    0xff00
#define CPUFPBDIS   0x0000

/* Output voltage from REG0 regulator stage. */
#define REGOUT0         MMIO32(UICR + 0x304)
// Output voltage from REG0 regulator stage.
#define VOUT1V8         0x0
#define VOUT2V1         0x1
#define VOUT2V4         0x2
#define VOUT2V7         0x3
#define VOUT3V0         0x4
#define VOUT3V3         0x5
#define VOUT_DEFAULT    0x7 // 1.8

#endif
