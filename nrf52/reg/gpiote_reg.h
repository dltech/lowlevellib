#ifndef H_GPIOTE_REG
#define H_GPIOTE_REG
/*
 * Standard library for Nordic NRF52
 * Register macro for GPIO tasks and events
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/*************Tasks************************/
/* Task for writing to pin specified in CONFIG[n].PSEL. Action on pin is conf in
CONFIG[n].POLARITY. */
#define OUT0GPIOTE      MMIO32(GPIOTE + 0x000)
#define OUT1GPIOTE      MMIO32(GPIOTE + 0x004)
#define OUT2GPIOTE      MMIO32(GPIOTE + 0x008)
#define OUT3GPIOTE      MMIO32(GPIOTE + 0x00c)
#define OUT4GPIOTE      MMIO32(GPIOTE + 0x010)
#define OUT5GPIOTE      MMIO32(GPIOTE + 0x014)
#define OUT6GPIOTE      MMIO32(GPIOTE + 0x018)
#define OUT7GPIOTE      MMIO32(GPIOTE + 0x01c)
/* Task for writing to pin specified in CONFIG[n].PSEL. */
#define SET0GPIOTE      MMIO32(GPIOTE + 0x030)
#define SET1GPIOTE      MMIO32(GPIOTE + 0x034)
#define SET2GPIOTE      MMIO32(GPIOTE + 0x038)
#define SET3GPIOTE      MMIO32(GPIOTE + 0x03c)
#define SET4GPIOTE      MMIO32(GPIOTE + 0x040)
#define SET5GPIOTE      MMIO32(GPIOTE + 0x044)
#define SET6GPIOTE      MMIO32(GPIOTE + 0x048)
#define SET7GPIOTE      MMIO32(GPIOTE + 0x04c)
/* Task for writing to pin specified in CONFIG[n].PSEL. */
#define CLR0GPIOTE      MMIO32(GPIOTE + 0x060)
#define CLR1GPIOTE      MMIO32(GPIOTE + 0x064)
#define CLR2GPIOTE      MMIO32(GPIOTE + 0x068)
#define CLR3GPIOTE      MMIO32(GPIOTE + 0x06c)
#define CLR4GPIOTE      MMIO32(GPIOTE + 0x070)
#define CLR5GPIOTE      MMIO32(GPIOTE + 0x074)
#define CLR6GPIOTE      MMIO32(GPIOTE + 0x078)
#define CLR7GPIOTE      MMIO32(GPIOTE + 0x07c)

/************Events************************/
/* Event generated from pin specified in CONFIG[n].PSEL */
#define IN0GPIOTE       MMIO32(GPIOTE + 0x100)
#define IN1GPIOTE       MMIO32(GPIOTE + 0x104)
#define IN2GPIOTE       MMIO32(GPIOTE + 0x108)
#define IN3GPIOTE       MMIO32(GPIOTE + 0x10c)
#define IN4GPIOTE       MMIO32(GPIOTE + 0x110)
#define IN5GPIOTE       MMIO32(GPIOTE + 0x114)
#define IN6GPIOTE       MMIO32(GPIOTE + 0x118)
#define IN7GPIOTE       MMIO32(GPIOTE + 0x11c)
/* Event generated from multiple input pins */
#define PORTGPIOTE      MMIO32(GPIOTE + 0x17c)

/************Registers*********************/
/* Enable interrupt */
#define INTENSETGPIOTE  MMIO32(GPIOTE + 0x304)
// Write '1' to enable interrupt for event
#define IN0G    0x01
#define IN1G    0x02
#define IN2G    0x04
#define IN3G    0x08
#define IN4G    0x10
#define IN5G    0x20
#define IN6G    0x40
#define IN7G    0x80
// Write '1' to enable interrupt for event PORT
#define PORTG   0x80000000

/* Disable interrupt */
#define INTENCLRGPIOTE  MMIO32(GPIOTE + 0x308)
// Write '1' to Clear interrupt on IN[0] event.
// Write '1' to Clear interrupt on IN[1] event.
// Write '1' to Clear interrupt on IN[2] event.
// Write '1' to Clear interrupt on IN[3] event.
// Write '1' to Clear interrupt on PORT event.

/* Configuration for OUT[n] task and IN[n] event */
#define CONFIG0GPIOTE   MMIO32(GPIOTE + 0x510)
#define CONFIG1GPIOTE   MMIO32(GPIOTE + 0x514)
#define CONFIG2GPIOTE   MMIO32(GPIOTE + 0x518)
#define CONFIG3GPIOTE   MMIO32(GPIOTE + 0x51c)
#define CONFIG4GPIOTE   MMIO32(GPIOTE + 0x520)
#define CONFIG5GPIOTE   MMIO32(GPIOTE + 0x524)
#define CONFIG6GPIOTE   MMIO32(GPIOTE + 0x528)
#define CONFIG7GPIOTE   MMIO32(GPIOTE + 0x52c)
// Mode
#define MODE_DISG   0x0
#define MODE_EVENTG 0x1
#define MODE_TASKG  0x3
// Pin number associated with OUT[n] task and IN[n] event.
#define PSEL_SFT    8
#define PSEL_MSK    0x1f
// GPIO number associated with SET[n], CLR[n], and OUT[n]
#define PORT_NUMG   0x2000
// Operation on input that shall trigger IN[n] event.
#define POLARITY_NONE   0x00000
#define POLARITY_LOTOHI 0x10000
#define POLARITY_HITOLO 0x20000
#define POLARITY_TOGGLE 0x30000
// Initial value of the output when the GPIOTE channel is configured.
#define OUTINIT     0x100000

#endif
