#ifndef H_TEMP_REG
#define H_TEMP_REG
/*
 * Standard library for Nordic NRF52
 * Register macro for Temperature sensor
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/*************Tasks************************/
/* Start temperature measurement */
#define STARTTEMP       MMIO32(TEMP + 0x000)
/* Stop temperature measurement */
#define STOPTEMP        MMIO32(TEMP + 0x004)

/************Events************************/
/* Temperature measurement complete, data ready */
#define DATARDYTEMP     MMIO32(TEMP + 0x100)

/************Registers*********************/
/* Enable interrupt */
#define INTENSETTEMP    MMIO32(TEMP + 0x304)
// Write '1' to enable interrupt for event DATARDY
#define DATARDYT    0x1

/* Disable interrupt */
#define INTENCLRTEMP    MMIO32(TEMP + 0x308)
// Write '1' to disable interrupt for event DATARDY

/* Temperature in °C */
#define TEMPTEMP        MMIO32(TEMP + 0x508)
// Temperature in °C Die temp in °C, 2's complement format, 0.25 °C TEMP[31:0]

/* Slope of nth piece wise linear function */
#define A0TEMP          MMIO32(TEMP + 0x520)
#define A1TEMP          MMIO32(TEMP + 0x524)
#define A2TEMP          MMIO32(TEMP + 0x528)
#define A3TEMP          MMIO32(TEMP + 0x52c)
#define A4TEMP          MMIO32(TEMP + 0x530)
#define A5TEMP          MMIO32(TEMP + 0x534)
// AN[11:0]

/* y-intercept of nth piece wise linear function */
#define B0TEMP          MMIO32(TEMP + 0x540)
#define B1TEMP          MMIO32(TEMP + 0x544)
#define B2TEMP          MMIO32(TEMP + 0x548)
#define B3TEMP          MMIO32(TEMP + 0x54c)
#define B4TEMP          MMIO32(TEMP + 0x550)
#define B5TEMP          MMIO32(TEMP + 0x554)
// BN[13:0]

/* End point of ntx piece wise linear function */
#define T0TEMP          MMIO32(TEMP + 0x560)
#define T1TEMP          MMIO32(TEMP + 0x564)
#define T2TEMP          MMIO32(TEMP + 0x568)
#define T3TEMP          MMIO32(TEMP + 0x56c)
#define T4TEMP          MMIO32(TEMP + 0x570)
// TN[7:0]

#endif
