#ifndef H_MEMORYMAP
#define H_MEMORYMAP
/*
 * Standard library for Nordic NRF52
 * Memory map for peripherial
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define FICR    0x10000000
#define UICR    0x10001000
#define POWER   0x40000000
#define CLOCK   0x40000000
#define MPU     0x40000000
#define RADIO   0x40001000
#define UART    0x40002000
#define UARTE0  0x40002000
#define SPI0    0x40003000
#define SPIM0   0x40003000
#define TWI0    0x40003000
#define TWIM0   0x40003000
#define TWIS0   0x40003000
#define TWI1    0x40004000
#define TWIM1   0x40004000
#define TWIS1   0x40004000
#define SPI1    0x40004000
#define SPIM1   0x40004000
#define SPIS1   0x40004000
#define SPIS    0x40004000
#define NFCT    0x40005000
#define GPIOTE  0x40006000
#define SAADC   0x40007000
#define TIMER0  0x40008000
#define TIMER1  0x40009000
#define TIMER2  0x4000a000
#define RTC0    0x4000b000
#define TEMP    0x4000c000
#define RNG     0x4000d000
#define ECB     0x4000e000
#define AAR     0x4000f000
#define CCM     0x4000f000
#define WDT     0x40010000
#define RTC1    0x40011000
#define QDEC    0x40012000
#define COMP    0x40013000
#define SWI0    0x40014000
#define SWI1    0x40015000
#define SWI2    0x40016000
#define SWI3    0x40017000
#define SWI4    0x40018000
#define SWI5    0x40019000
#define EGU0    0x40014000
#define EGU1    0x40015000
#define EGU2    0x40016000
#define EGU3    0x40017000
#define EGU4    0x40018000
#define EGU5    0x40019000
#define TIMER3  0x4001a000
#define TIMER4  0x4001b000
#define PWM0    0x4001c000
#define PDM     0x4001d000
#define NVMC    0x4001e000
#define ACL     0x4001e000
#define PPI     0x4001f000
#define MWU     0x40020000
#define PWM1    0x40021000
#define PWM2    0x40022000
#define SPI2    0x40023000
#define SPIM2   0x40023000
#define SPIS2   0x40023000
#define RTC2    0x40024000
#define I2S     0x40025000
#define FPU     0x40026000
#define USBD    0x40027000
#define UARTE1  0x40028000
#define PWM3    0x4002d000
#define SPIM3   0x4002f000
#define GPIO0   0x50000000
#define GPIO1   0x50000300

// access to register with specified address
#define  MMIO32(addr)		(*(volatile uint32_t *)(addr))

#endif
