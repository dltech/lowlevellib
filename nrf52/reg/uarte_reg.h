#ifndef H_UARTE_REG
#define H_UARTE_REG
/*
 * Standard peripherial library for Nordic NRF52
 * Register macro for Universal Asynchronous Receiver/Transmitter with EasyDMA
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/*************Tasks************************/
/* Start UART receiver */
#define STARTRXUARTE0       MMIO32(UARTE0 + 0x000)
#define STARTRXUARTE1       MMIO32(UARTE1 + 0x000)
/* Stop UART receiver */
#define STOPRXUARTE0        MMIO32(UARTE0 + 0x004)
#define STOPRXUARTE1        MMIO32(UARTE1 + 0x004)
/* Start UART transmitter */
#define STARTTXUARTE0       MMIO32(UARTE0 + 0x008)
#define STARTTXUARTE1       MMIO32(UARTE1 + 0x008)
/* Stop UART transmitter */
#define STOPTXUARTE0        MMIO32(UARTE0 + 0x00c)
#define STOPTXUARTE1        MMIO32(UARTE1 + 0x00c)
/* Flush RX FIFO into RX buffer */
#define FLUSHRXUARTE0       MMIO32(UARTE0 + 0x02c)
#define FLUSHRXUARTE1       MMIO32(UARTE1 + 0x02c)

/************Events************************/
/* CTS is activated (set low). Clear To Send. */
#define CTSUARTE0           MMIO32(UARTE0 + 0x100)
#define CTSUARTE1           MMIO32(UARTE1 + 0x100)
/* CTS is deactivated (set high). Not Clear To Send. */
#define NCTSUARTE0          MMIO32(UARTE0 + 0x104)
#define NCTSUARTE1          MMIO32(UARTE1 + 0x104)
/* Data received in RXD */
#define RXRDYUARTE0         MMIO32(UARTE0 + 0x108)
#define RXRDYUARTE1         MMIO32(UARTE1 + 0x108)
/* Receive buffer is filled up */
#define ENDRXUARTE0         MMIO32(UARTE0 + 0x110)
#define ENDRXUARTE1         MMIO32(UARTE1 + 0x110)
/* Data sent from TXD */
#define TXRDYUARTE0         MMIO32(UARTE0 + 0x11c)
#define TXRDYUARTE1         MMIO32(UARTE1 + 0x11c)
/* Last TX byte transmitted */
#define ENDTXUARTE0         MMIO32(UARTE0 + 0x120)
#define ENDTXUARTE1         MMIO32(UARTE1 + 0x120)
/* Error detected */
#define ERRORUARTE0         MMIO32(UARTE0 + 0x124)
#define ERRORUARTE1         MMIO32(UARTE1 + 0x124)
/* Receiver timeout */
#define RXTOUARTE0          MMIO32(UARTE0 + 0x144)
#define RXTOUARTE1          MMIO32(UARTE1 + 0x144)
/* UART receiver has started */
#define RXSTARTEDUARTE0     MMIO32(UARTE0 + 0x14c)
#define RXSTARTEDUARTE1     MMIO32(UARTE1 + 0x14c)
/* UART transmitter has started */
#define TXSTARTEDUARTE0     MMIO32(UARTE0 + 0x150)
#define TXSTARTEDUARTE1     MMIO32(UARTE1 + 0x150)
/* Transmitter stopped */
#define TXSTOPPEDUARTE0     MMIO32(UARTE0 + 0x158)
#define TXSTOPPEDUARTE1     MMIO32(UARTE1 + 0x158)

/************Registers*********************/
/* Enable or disable interrupt */
#define SHORTSUARTE0        MMIO32(UARTE0 + 0x200)
#define SHORTSUARTE1        MMIO32(UARTE1 + 0x200)
// Shortcut between event ENDRX and task STARTRX
#define ENDRX_STARTRX   0x20
// Shortcut between event ENDRX and task STOPRX
#define ENDRX_STOPRX    0x40

/* Enable or disable interrupt */
#define INTENUARTE0         MMIO32(UARTE0 + 0x300)
#define INTENUARTE1         MMIO32(UARTE1 + 0x300)
// Enable or disable interrupt for event CTS
#define CTS         0x000001
// Enable or disable interrupt for event NCTS
#define NCTS        0x000002
// Enable or disable interrupt for event RXDRDY
#define RXDRDY      0x000004
// Enable or disable interrupt for event ENDRX
#define ENDRX       0x000010
// Enable or disable interrupt for event TXDRDY
#define TXDRDY      0x000080
// Enable or disable interrupt for event ENDTX
#define ENDTX       0x000100
// Enable or disable interrupt for event ERROR
#define ERRORU      0x000200
// Enable or disable interrupt for event RXTO
#define RXTO        0x020000
// Enable or disable interrupt for event RXSTARTED
#define RXSTARTED   0x080000
// Enable or disable interrupt for event TXSTARTED
#define TXSTARTED   0x100000
// Enable or disable interrupt for event TXSTOPPED
#define TXSTOPPED   0x400000

/* Enable interrupt */
#define INTENSETUARTE0      MMIO32(UARTE0 + 0x304)
#define INTENSETUARTE1      MMIO32(UARTE1 + 0x304)
// Write '1' to enable interrupt for event CTS
// Write '1' to enable interrupt for event NCTS
// Write '1' to enable interrupt for event RXDRDY
// Write '1' to enable interrupt for event ENDRX
// Write '1' to enable interrupt for event TXDRDY
// Write '1' to enable interrupt for event ENDTX
// Write '1' to enable interrupt for event ERROR
// Write '1' to enable interrupt for event RXTO
// Write '1' to enable interrupt for event RXSTARTED
// Write '1' to enable interrupt for event TXSTARTED
// Write '1' to enable interrupt for event TXSTOPPED

/* Disable interrupt */
#define INTENCLRUARTE0      MMIO32(UARTE0 + 0x308)
#define INTENCLRUARTE1      MMIO32(UARTE1 + 0x308)
// Write '1' to disable interrupt for event CTS
// Write '1' to disable interrupt for event NCTS
// Write '1' to disable interrupt for event RXDRDY
// Write '1' to disable interrupt for event ENDRX
// Write '1' to disable interrupt for event TXDRDY
// Write '1' to disable interrupt for event ENDTX
// Write '1' to disable interrupt for event ERROR
// Write '1' to disable interrupt for event RXTO
// Write '1' to disable interrupt for event RXSTARTED
// Write '1' to disable interrupt for event TXSTARTED
// Write '1' to disable interrupt for event TXSTOPPED

/* Error source */
#define ERRORSRCUARTE0      MMIO32(UARTE0 + 0x480)
#define ERRORSRCUARTE1      MMIO32(UARTE1 + 0x480)
// Overrun error
#define OVERRUNE    0x1
// Parity error
#define PARITYE     0x2
// Framing error occurred
#define FRAMINGE    0x4
// Break condition
#define BREAKE      0x8

/* Enable UART */
#define ENABLEUARTE0        MMIO32(UARTE0 + 0x500)
#define ENABLEUARTE1        MMIO32(UARTE1 + 0x500)
// Enable or disable UART
#define ENABLEE 0x8

/* Pin select for RTS */
#define PSELRTSUARTE0       MMIO32(UARTE0 + 0x508)
#define PSELRTSUARTE1       MMIO32(UARTE1 + 0x508)

/* Pin select for TXD */
#define PSELTXDUARTE0       MMIO32(UARTE0 + 0x50c)
#define PSELTXDUARTE1       MMIO32(UARTE1 + 0x50c)

/* Pin select for CTS */
#define PSELCTSUARTE0       MMIO32(UARTE0 + 0x510)
#define PSELCTSUARTE1       MMIO32(UARTE1 + 0x510)

/* Pin select for RXD */
#define PSELRXDUARTE0       MMIO32(UARTE0 + 0x514)
#define PSELRXDUARTE1       MMIO32(UARTE1 + 0x514)

/* Baud rate */
#define BAUDRATEUARTE0      MMIO32(UARTE0 + 0x524)
#define BAUDRATEUARTE1      MMIO32(UARTE1 + 0x524)
// Baud-rate
#define Baud1200    0x0004f000
#define Baud2400    0x0009D000
#define Baud4800    0x0013B000
#define Baud9600    0x00275000
#define Baud14400   0x003af000
#define Baud19200   0x004EA000
#define Baud28800   0x0075c000
#define Baud31250   0x00800000
#define Baud38400   0x009D0000
#define Baud56000   0x00E50000
#define Baud57600   0x00EB0000
#define Baud76800   0x013A9000
#define Baud115200  0x01D60000
#define Baud230400  0x03b00000
#define Baud250000  0x04000000
#define Baud460800  0x07400000
#define Baud921600  0x0f000000
#define Baud1M      0x10000000

/* Data pointer */
#define RXD_PTRUARTE0       MMIO32(UARTE0 + 0x534)
#define RXD_PTRUARTE1       MMIO32(UARTE1 + 0x534)
// PTR[31:0]

/* Maximum number of bytes in receive buffer */
#define RXD_MAXCNTUARTE0    MMIO32(UARTE0 + 0x538)
#define RXD_MAXCNTUARTE1    MMIO32(UARTE1 + 0x538)
// MAXCNT[15:0]

/* Number of bytes transferred in the last transaction */
#define RXD_AMOUNTUARTE0    MMIO32(UARTE0 + 0x53c)
#define RXD_AMOUNTUARTE1    MMIO32(UARTE1 + 0x53c)
// AMOUNT[15:0]

/* Data pointer */
#define TXD_PTRUARTE0       MMIO32(UARTE0 + 0x544)
#define TXD_PTRUARTE1       MMIO32(UARTE1 + 0x544)
// PTR[31:0]

/* Maximum number of bytes in transmit buffer */
#define TXD_MAXCNTUARTE0    MMIO32(UARTE0 + 0x548)
#define TXD_MAXCNTUARTE1    MMIO32(UARTE1 + 0x548)
// MAXCNT[15:0]

/* Number of bytes transferred in the last transaction */
#define TXD_AMOUNTUARTE0    MMIO32(UARTE0 + 0x54c)
#define TXD_AMOUNTUARTE1    MMIO32(UARTE1 + 0x54c)
// AMOUNT[15:0]

/* Configuration of parity and hardware flow control */
#define CONFIGUARTUARTE0    MMIO32(UARTE0 + 0x56c)
#define CONFIGUARTUARTE1    MMIO32(UARTE1 + 0x56c)
// Hardware flow control
#define HWFCE       0x001
// Parity
#define PARITY_INCE 0x007
// Stop bits
#define STOPE       0x010
// Even or odd parity type
#define PARITYTYPEE 0x100

#endif
