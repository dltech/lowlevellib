#ifndef H_ACL_REG
#define H_ACL_REG
/*
 * Standard library for Nordic NRF52
 * The Access control lists (ACL) peripheral registers
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Configure the word-aligned start address of region x to protect */
#define ACL0ADDR MMIO32(ACL + 0x800)
#define ACL1ADDR MMIO32(ACL + 0x810)
#define ACL2ADDR MMIO32(ACL + 0x820)
#define ACL3ADDR MMIO32(ACL + 0x830)
#define ACL4ADDR MMIO32(ACL + 0x840)
#define ACL5ADDR MMIO32(ACL + 0x850)
#define ACL6ADDR MMIO32(ACL + 0x860)
#define ACL7ADDR MMIO32(ACL + 0x870)
// Address must point to a flash page boundary. RW1ADDR[31:0]

/* Size of region to protect counting from address ACL[x].ADDR. */
#define ACL0SIZE MMIO32(ACL + 0x804)
#define ACL1SIZE MMIO32(ACL + 0x814)
#define ACL2SIZE MMIO32(ACL + 0x824)
#define ACL3SIZE MMIO32(ACL + 0x834)
#define ACL4SIZE MMIO32(ACL + 0x844)
#define ACL5SIZE MMIO32(ACL + 0x854)
#define ACL6SIZE MMIO32(ACL + 0x864)
#define ACL7SIZE MMIO32(ACL + 0x874)
// Size of flash region n in bytes. Must be a multiple of the flash page size.

/* Acc perm for reg 0 as def by start addr ACL[0].ADDR and size ACL[0].SIZE */
#define ACL0PERM MMIO32(ACL + 0x808)
#define ACL1PERM MMIO32(ACL + 0x818)
#define ACL2PERM MMIO32(ACL + 0x828)
#define ACL3PERM MMIO32(ACL + 0x838)
#define ACL4PERM MMIO32(ACL + 0x848)
#define ACL5PERM MMIO32(ACL + 0x858)
#define ACL6PERM MMIO32(ACL + 0x868)
#define ACL7PERM MMIO32(ACL + 0x878)
// Configure write and erase permissions for region n.
#define RW1WRITE    0x2
// Configure read permissions for region n.
#define RW1READ     0x4

#endif
