#ifndef H_GPIO_REG
#define H_GPIO_REG
/*
 * Standard library for Nordic NRF52
 * Register macro for General-Purpose Input/Output
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/************Registers*********************/
/* Write GPIO port */
#define OUTGPIO0          MMIO32(GPIO0 + 0x504)
#define OUTGPIO1          MMIO32(GPIO1 + 0x504)
// 0 = Pin driver is low, 1 = Pin driver is high
#define PIN0     0x00000001
#define PIN1     0x00000002
#define PIN2     0x00000004
#define PIN3     0x00000008
#define PIN4     0x00000010
#define PIN5     0x00000020
#define PIN6     0x00000040
#define PIN7     0x00000080
#define PIN8     0x00000100
#define PIN9     0x00000200
#define PIN10    0x00000400
#define PIN11    0x00000800
#define PIN12    0x00001000
#define PIN13    0x00002000
#define PIN14    0x00004000
#define PIN15    0x00008000
#define PIN16    0x00010000
#define PIN17    0x00020000
#define PIN18    0x00040000
#define PIN19    0x00080000
#define PIN20    0x00100000
#define PIN21    0x00200000
#define PIN22    0x00400000
#define PIN23    0x00800000
#define PIN24    0x01000000
#define PIN25    0x02000000
#define PIN26    0x04000000
#define PIN27    0x08000000
#define PIN28    0x10000000
#define PIN29    0x20000000
#define PIN30    0x40000000
#define PIN31    0x80000000

/* Set individual bits in GPIO port */
#define OUTSETGPIO0       MMIO32(GPIO0 + 0x508)
#define OUTSETGPIO1       MMIO32(GPIO1 + 0x508)
// Read: 0 = pin driver is low, 1 = pin driver is high, writing a '1' sets high

/* Clear individual bits in GPIO port */
#define OUTCLRGPIO0       MMIO32(GPIO0 + 0x50c)
#define OUTCLRGPIO1       MMIO32(GPIO1 + 0x50c)
// Read: 0 = pin driver is low, 1 = pin driver is high, writing a '1' sets low

/* Read GPIO port */
#define INGPIO0           MMIO32(GPIO0 + 0x510)
#define INGPIO1           MMIO32(GPIO1 + 0x510)
// 0 = Pin input is low, 1 = Pin input is high.

/* Direction of GPIO pins */
#define DIRGPIO0          MMIO32(GPIO0 + 0x514)
#define DIRGPIO1          MMIO32(GPIO1 + 0x514)
// 0 = Pin set as input, 1 = Pin set as output.

/* DIR set register */
#define DIRSETGPIO0       MMIO32(GPIO0 + 0x518)
#define DIRSETGPIO1       MMIO32(GPIO1 + 0x518)
// writing a '1' sets pin to output

/* DIR clear register */
#define DIRCLR0           MMIO32(GPIO0 + 0x51c)
#define DIRCLR1           MMIO32(GPIO1 + 0x51c)
// writing a '1' sets pin to input

/* indicating what GPIO pins that have met the crit in the PIN_CNF[n].SENSE */
#define LATCH0            MMIO32(GPIO0 + 0x520)
#define LATCH1            MMIO32(GPIO1 + 0x520)
// Status on whether PINi has met criteria set in PIN_CNFi.SENSE register.

/* Select between default DETECT signal behaviour and LDETECT mode */
#define DETECTMODE0       MMIO32(GPIO0 + 0x524)
#define DETECTMODE1       MMIO32(GPIO1 + 0x524)
// Select between default DETECT signal behaviour and LDETECT mode
#define DETECTMODE_EN   0x1

/* Configuration of GPIO pins */
#define PIN_CNF0GPIO0     MMIO32(GPIO0 + 0x700)
#define PIN_CNF0GPIO1     MMIO32(GPIO1 + 0x700)
#define PIN_CNF1GPIO0     MMIO32(GPIO0 + 0x704)
#define PIN_CNF1GPIO1     MMIO32(GPIO1 + 0x704)
#define PIN_CNF2GPIO0     MMIO32(GPIO0 + 0x708)
#define PIN_CNF2GPIO1     MMIO32(GPIO1 + 0x708)
#define PIN_CNF3GPIO0     MMIO32(GPIO0 + 0x70c)
#define PIN_CNF3GPIO1     MMIO32(GPIO1 + 0x70c)
#define PIN_CNF4GPIO0     MMIO32(GPIO0 + 0x710)
#define PIN_CNF4GPIO1     MMIO32(GPIO1 + 0x710)
#define PIN_CNF5GPIO0     MMIO32(GPIO0 + 0x714)
#define PIN_CNF5GPIO1     MMIO32(GPIO1 + 0x714)
#define PIN_CNF6GPIO0     MMIO32(GPIO0 + 0x718)
#define PIN_CNF6GPIO1     MMIO32(GPIO1 + 0x718)
#define PIN_CNF7GPIO0     MMIO32(GPIO0 + 0x71c)
#define PIN_CNF7GPIO1     MMIO32(GPIO1 + 0x71c)
#define PIN_CNF8GPIO0     MMIO32(GPIO0 + 0x720)
#define PIN_CNF8GPIO1     MMIO32(GPIO1 + 0x720)
#define PIN_CNF9GPIO0     MMIO32(GPIO0 + 0x724)
#define PIN_CNF9GPIO1     MMIO32(GPIO1 + 0x724)
#define PIN_CNF10GPIO0    MMIO32(GPIO0 + 0x728)
#define PIN_CNF10GPIO1    MMIO32(GPIO1 + 0x728)
#define PIN_CNF11GPIO0    MMIO32(GPIO0 + 0x72c)
#define PIN_CNF11GPIO1    MMIO32(GPIO1 + 0x72c)
#define PIN_CNF12GPIO0    MMIO32(GPIO0 + 0x730)
#define PIN_CNF12GPIO1    MMIO32(GPIO1 + 0x730)
#define PIN_CNF13GPIO0    MMIO32(GPIO0 + 0x734)
#define PIN_CNF13GPIO1    MMIO32(GPIO1 + 0x734)
#define PIN_CNF14GPIO0    MMIO32(GPIO0 + 0x738)
#define PIN_CNF14GPIO1    MMIO32(GPIO1 + 0x738)
#define PIN_CNF15GPIO0    MMIO32(GPIO0 + 0x73c)
#define PIN_CNF15GPIO1    MMIO32(GPIO1 + 0x73c)
#define PIN_CNF16GPIO0    MMIO32(GPIO0 + 0x740)
#define PIN_CNF16GPIO1    MMIO32(GPIO1 + 0x740)
#define PIN_CNF17GPIO0    MMIO32(GPIO0 + 0x744)
#define PIN_CNF17GPIO1    MMIO32(GPIO1 + 0x744)
#define PIN_CNF18GPIO0    MMIO32(GPIO0 + 0x748)
#define PIN_CNF18GPIO1    MMIO32(GPIO1 + 0x748)
#define PIN_CNF19GPIO0    MMIO32(GPIO0 + 0x74c)
#define PIN_CNF19GPIO1    MMIO32(GPIO1 + 0x74c)
#define PIN_CNF20GPIO0    MMIO32(GPIO0 + 0x750)
#define PIN_CNF20GPIO1    MMIO32(GPIO1 + 0x750)
#define PIN_CNF21GPIO0    MMIO32(GPIO0 + 0x754)
#define PIN_CNF21GPIO1    MMIO32(GPIO1 + 0x754)
#define PIN_CNF22GPIO0    MMIO32(GPIO0 + 0x758)
#define PIN_CNF22GPIO1    MMIO32(GPIO1 + 0x758)
#define PIN_CNF23GPIO0    MMIO32(GPIO0 + 0x75c)
#define PIN_CNF23GPIO1    MMIO32(GPIO1 + 0x75c)
#define PIN_CNF24GPIO0    MMIO32(GPIO0 + 0x760)
#define PIN_CNF24GPIO1    MMIO32(GPIO1 + 0x760)
#define PIN_CNF25GPIO0    MMIO32(GPIO0 + 0x764)
#define PIN_CNF25GPIO1    MMIO32(GPIO1 + 0x764)
#define PIN_CNF26GPIO0    MMIO32(GPIO0 + 0x768)
#define PIN_CNF26GPIO1    MMIO32(GPIO1 + 0x768)
#define PIN_CNF27GPIO0    MMIO32(GPIO0 + 0x76c)
#define PIN_CNF27GPIO1    MMIO32(GPIO1 + 0x76c)
#define PIN_CNF28GPIO0    MMIO32(GPIO0 + 0x770)
#define PIN_CNF28GPIO1    MMIO32(GPIO1 + 0x770)
#define PIN_CNF29GPIO0    MMIO32(GPIO0 + 0x774)
#define PIN_CNF29GPIO1    MMIO32(GPIO1 + 0x774)
#define PIN_CNF30GPIO0    MMIO32(GPIO0 + 0x778)
#define PIN_CNF30GPIO1    MMIO32(GPIO1 + 0x778)
#define PIN_CNF31GPIO0    MMIO32(GPIO0 + 0x77c)
#define PIN_CNF31GPIO1    MMIO32(GPIO1 + 0x77c)
// Pin direction
#define DIRG    0x1
// Connect or disconnect input buffer
#define INPUTG  0x2
// Pull configuration
#define PULL_DIS    0x0
#define PULL_UP     0xc
#define PULL_DN     0x4
// Drive configuration
#define DRIVE_S0S1  0x000
#define DRIVE_H0S1  0x100
#define DRIVE_S0H1  0x200
#define DRIVE_H0H1  0x300
#define DRIVE_D0S1  0x400
#define DRIVE_D0H1  0x500
#define DRIVE_S0D1  0x600
#define DRIVE_H0D1  0x700
// Pin sensing mechanism
#define SENSE_DIS   0x00000
#define SENSE_HI    0x20000
#define SENSE_LOW   0x30000

#endif
