#ifndef H_UARTE_REG
#define H_UARTE_REG
/*
 * Standard peripherial library for Nordic NRF52
 * Register macro for Universal serial bus device (USBD)
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/*************Tasks************************/
/* Captures the EPIN[n].PTR and EPIN[n].MAXCNT registers values */
#define STARTEPIN0USBD  MMIO32(USBD + 0x004)
#define STARTEPIN1USBD  MMIO32(USBD + 0x008)
#define STARTEPIN2USBD  MMIO32(USBD + 0x00c)
#define STARTEPIN3USBD  MMIO32(USBD + 0x010)
#define STARTEPIN4USBD  MMIO32(USBD + 0x014)
#define STARTEPIN5USBD  MMIO32(USBD + 0x018)
#define STARTEPIN6USBD  MMIO32(USBD + 0x01c)
#define STARTEPIN7USBD  MMIO32(USBD + 0x020)
/* Captures the ISOIN.PTR and ISOIN.MAXCNT registers values */
#define STARTISOINUSBD  MMIO32(USBD + 0x024)
/* Captures the EPOUT[n].PTR and EPOUT[n].MAXCNT registers values */
#define STARTEPOUT0USBD MMIO32(USBD + 0x028)
#define STARTEPOUT1USBD MMIO32(USBD + 0x02c)
#define STARTEPOUT2USBD MMIO32(USBD + 0x030)
#define STARTEPOUT3USBD MMIO32(USBD + 0x034)
#define STARTEPOUT4USBD MMIO32(USBD + 0x038)
#define STARTEPOUT5USBD MMIO32(USBD + 0x03c)
#define STARTEPOUT6USBD MMIO32(USBD + 0x040)
#define STARTEPOUT7USBD MMIO32(USBD + 0x044)
/* Captures the ISOOUT.PTR and ISOOUT.MAXCNT registers values */
#define STARTISOOUTUSBD MMIO32(USBD + 0x048)
/* Allows OUT data stage on control endpoint 0 */
#define EP0RCVOUTUSBD   MMIO32(USBD + 0x04c)
/* Allows status stage on control endpoint 0 */
#define EP0STATUSUSBD   MMIO32(USBD + 0x050)
/* Stalls data and status stage on control endpoint 0 */
#define EP0STALLUSBD    MMIO32(USBD + 0x054)
/* Forces D+ and D- lines into the state defined in the DPDMVALUE register */
#define DPDMDRIVEUSBD   MMIO32(USBD + 0x058)
/* Stops forcing D+ and D- lines into any state (USB engine takes control) */
#define DPDMNODRIVEUSBD MMIO32(USBD + 0x05c)

/************Events************************/
/* Signals that a USB reset condition has been detected on USB lines */
#define USBRESETUSBD    MMIO32(USBD + 0x100)
/* Confirms that the EPIN[n].PTR and EPIN[n].MAXCNT */
#define STARTEDUSBD     MMIO32(USBD + 0x104)
/* The whole EPIN[n] buffer has been consumed. */
#define ENDEPIN0USBD    MMIO32(USBD + 0x108)
#define ENDEPIN1USBD    MMIO32(USBD + 0x10c)
#define ENDEPIN2USBD    MMIO32(USBD + 0x110)
#define ENDEPIN3USBD    MMIO32(USBD + 0x114)
#define ENDEPIN4USBD    MMIO32(USBD + 0x118)
#define ENDEPIN5USBD    MMIO32(USBD + 0x11c)
#define ENDEPIN6USBD    MMIO32(USBD + 0x120)
#define ENDEPIN7USBD    MMIO32(USBD + 0x124)
/* An acknowledged data transfer has taken place on the control endpoint */
#define EP0DATADONEUSBD MMIO32(USBD + 0x128)
/* The whole ISOIN buffer has been consumed. */
#define ENDISOINUSBD    MMIO32(USBD + 0x12c)
/* The whole EPOUT[n] buffer has been consumed. */
#define ENDEPOUT0USBD   MMIO32(USBD + 0x130)
#define ENDEPOUT1USBD   MMIO32(USBD + 0x134)
#define ENDEPOUT2USBD   MMIO32(USBD + 0x138)
#define ENDEPOUT3USBD   MMIO32(USBD + 0x13c)
#define ENDEPOUT4USBD   MMIO32(USBD + 0x140)
#define ENDEPOUT5USBD   MMIO32(USBD + 0x144)
#define ENDEPOUT6USBD   MMIO32(USBD + 0x148)
#define ENDEPOUT7USBD   MMIO32(USBD + 0x14c)
/* The whole ISOOUT buffer has been consumed */
#define ENDISOOUTUSBD   MMIO32(USBD + 0x150)
/* Signals that a SOF condition has been detected on USB lines */
#define SOFUSBD         MMIO32(USBD + 0x154)
/* An event or an error not covered by specific events has occurred. */
#define USBEVENTUSBD    MMIO32(USBD + 0x158)
/* A valid SETUP token has been received (and ack) on the control endpoint */
#define EP0SETUPUSBD    MMIO32(USBD + 0x15c)
/* A data tx has occurred on a data endp, indicated by the EPDATASTATUS reg */
#define EPDATAUSBD      MMIO32(USBD + 0x160)

/************Registers*********************/
/* Shortcuts between local events and tasks */
#define SHORTSUSBD          MMIO32(USBD + 0x200)
// Shortcut between event EP0DATADONE and task
#define EP0DATADONE_STARTEPIN0  0x01
// Shortcut between event EP0DATADONE and task
#define EP0DATADONE_STARTEPOUT0 0x02
// Shortcut between event EP0DATADONE and task EP0STATUS
#define EP0DATADONE_EP0STATUS   0x04
// Shortcut between event ENDEPOUT[0] and task EP0STATUS
#define ENDEPOUT0_EP0STATUS     0x08
// Shortcut between event ENDEPOUT[0] and task EP0RCVOUT
#define ENDEPOUT0_EP0RCVOUT     0x10

/* Enable or disable interrupt */
#define INTENUSBD           MMIO32(USBD + 0x300)
// Enable or disable interrupt for event USBRESET
#define USBRESET    0x0000001
// Enable or disable interrupt for event STARTED
#define STARTEDUSB  0x0000002
// Enable or disable interrupt for event ENDEPIN0
#define ENDEPIN0    0x0000004
// Enable or disable interrupt for event ENDEPIN1
#define ENDEPIN1    0x0000008
// Enable or disable interrupt for event ENDEPIN2
#define ENDEPIN2    0x0000010
// Enable or disable interrupt for event ENDEPIN3
#define ENDEPIN3    0x0000020
// Enable or disable interrupt for event ENDEPIN4
#define ENDEPIN4    0x0000040
// Enable or disable interrupt for event ENDEPIN5
#define ENDEPIN5    0x0000080
// Enable or disable interrupt for event ENDEPIN6
#define ENDEPIN6    0x0000100
// Enable or disable interrupt for event ENDEPIN7
#define ENDEPIN7    0x0000200
// Enable or disable interrupt for event EP0DATADONE
#define EP0DATADONE 0x0000400
// Enable or disable interrupt for event ENDISOIN
#define ENDISOIN    0x0000800
// Enable or disable interrupt for event ENDEPOUT0
#define ENDEPOUT0   0x0001000
// Enable or disable interrupt for event ENDEPOUT1
#define ENDEPOUT1   0x0002000
// Enable or disable interrupt for event ENDEPOUT2
#define ENDEPOUT2   0x0004000
// Enable or disable interrupt for event ENDEPOUT3
#define ENDEPOUT3   0x0008000
// Enable or disable interrupt for event ENDEPOUT4
#define ENDEPOUT4   0x0010000
// Enable or disable interrupt for event ENDEPOUT5
#define ENDEPOUT5   0x0020000
// Enable or disable interrupt for event ENDEPOUT6
#define ENDEPOUT6   0x0040000
// Enable or disable interrupt for event ENDEPOUT7
#define ENDEPOUT7   0x0080000
// Enable or disable interrupt for event ENDISOOUT
#define ENDISOOUT   0x0100000
// Enable or disable interrupt for event SOF
#define SOF         0x0200000
// Enable or disable interrupt for event USBEVENT
#define USBEVENT    0x0400000
// Enable or disable interrupt for event EP0SETUP
#define EP0SETUP    0x0800000
// Enable or disable interrupt for event EPDATA
#define EPDATA      0x1000000

/* Enable interrupt */
#define INTENSETUSBD        MMIO32(USBD + 0x304)
// Write '1' to enable interrupt for event USBRESET
// Write '1' to enable interrupt for event STARTED
// Write '1' to enable interrupt for event ENDEPIN[i]
// Write '1' to enable interrupt for event EP0DATADONE
// Write '1' to enable interrupt for event ENDISOIN
// Write '1' to enable interrupt for event ENDEPOUT[i]
// Write '1' to enable interrupt for event ENDISOOUT
// Write '1' to enable interrupt for event SOF
// Write '1' to enable interrupt for event USBEVENT
// Write '1' to enable interrupt for event EP0SETUP
// Write '1' to enable interrupt for event EPDATA

/* Disable interrupt */
#define INTENCLRUSBD        MMIO32(USBD + 0x308)
// Write '1' to disable interrupt for event USBRESET
// Write '1' to disable interrupt for event STARTED
// Write '1' to disable interrupt for event ENDEPIN[i]
// Write '1' to disable interrupt for event EP0DATADONE
// Write '1' to disable interrupt for event ENDISOIN
// Write '1' to disable interrupt for event ENDEPOUT[i]
// Write '1' to disable interrupt for event ENDISOOUT
// Write '1' to disable interrupt for event SOF
// Write '1' to disable interrupt for event USBEVENT
// Write '1' to disable interrupt for event EP0SETUP
// Write '1' to disable interrupt for event EPDATA

/* Details on what caused the USBEVENT event */
#define EVENTCAUSEUSBD      MMIO32(USBD + 0x400)
// CRC error was detected on isochronous OUT endpoint 8.
#define ISOOUTCRC       0x001
// USB lines have been idle long enough for the device to enter suspend.
#define SUSPENDUSB      0x100
// Signals that a RESUME condition has been detected on USB lines.
#define RESUMEUSB       0x200
// USB MAC has been woken up and operational. Write '1' to clear.
#define USBWUALLOWED    0x400
// USB device is ready for normal operation. Write '1' to clear.
#define READYUSB        0x800

/* IN endpoint halted status. */
#define HALTED_EPIN0USBD    MMIO32(USBD + 0x420)
#define HALTED_EPIN1USBD    MMIO32(USBD + 0x424)
#define HALTED_EPIN2USBD    MMIO32(USBD + 0x428)
#define HALTED_EPIN3USBD    MMIO32(USBD + 0x42c)
#define HALTED_EPIN4USBD    MMIO32(USBD + 0x430)
#define HALTED_EPIN5USBD    MMIO32(USBD + 0x434)
#define HALTED_EPIN6USBD    MMIO32(USBD + 0x438)
#define HALTED_EPIN7USBD    MMIO32(USBD + 0x43c)
// Can be used as is as response to a GetStatus() request to endpoint.
#define HALTED  0x1

/* OUT endpoint halted status */
#define HALTED_EPOUT0USBD   MMIO32(USBD + 0x444)
#define HALTED_EPOUT1USBD   MMIO32(USBD + 0x448)
#define HALTED_EPOUT2USBD   MMIO32(USBD + 0x44c)
#define HALTED_EPOUT3USBD   MMIO32(USBD + 0x450)
#define HALTED_EPOUT4USBD   MMIO32(USBD + 0x454)
#define HALTED_EPOUT5USBD   MMIO32(USBD + 0x458)
#define HALTED_EPOUT6USBD   MMIO32(USBD + 0x45c)
#define HALTED_EPOUT7USBD   MMIO32(USBD + 0x460)
// Can be used as is as response to a GetStatus() request to endpoint.

/* Provides information on which endpoint's EasyDMA regs have been captured */
#define EPSTATUSUSBD        MMIO32(USBD + 0x468)
// Captured state of endpoint's EasyDMA registers.
#define EPIN0   0x0000001
#define EPIN1   0x0000002
#define EPIN2   0x0000004
#define EPIN3   0x0000008
#define EPIN4   0x0000010
#define EPIN5   0x0000020
#define EPIN6   0x0000040
#define EPIN7   0x0000080
#define EPIN8   0x0000100
// Captured state of endpoint's EasyDMA registers.
#define EPOUT0  0x0010000
#define EPOUT1  0x0020000
#define EPOUT2  0x0040000
#define EPOUT3  0x0080000
#define EPOUT4  0x0100000
#define EPOUT5  0x0200000
#define EPOUT6  0x0400000
#define EPOUT7  0x0800000
#define EPOUT8  0x100000

/* Provides info on which endpoint(s) an acknowledged data tx has occurred */
#define EPDATASTATUSUSBD    MMIO32(USBD + 0x46c)
// Acknowledged data transfer on this IN endpoint.
// Acknowledged data transfer on this OUT endpoint.

/* Device USB address */
#define USBADDRUSBD         MMIO32(USBD + 0x470)
// ADDR[6:0]

/* SETUP data, byte 0, bmRequestType */
#define BMREQUESTTYPEUSBD   MMIO32(USBD + 0x480)
// Data transfer type
#define RECIPIENT_DEVICE    0x00
#define RECIPIENT_INTERFACE 0x01
#define RECIPIENT_ENDPOINT  0x02
#define RECIPIENT_OTHER     0x03
// Data transfer type
#define TYPE_STANDARD       0x00
#define TYPE_CLASS          0x20
#define TYPE_VENDOR         0x40
// Data transfer direction
#define DIRECTION           0x80

/* SETUP data, byte 1, bRequest */
#define BREQUESTUSBD        MMIO32(USBD + 0x484)
// Values provided for standard requests only
#define STD_GET_STATUS          0x0
#define STD_CLEAR_FEATURE       0x1
#define STD_SET_FEATURE         0x3
#define STD_SET_ADDRESS         0x5
#define STD_GET_DESCRIPTOR      0x6
#define STD_SET_DESCRIPTOR      0x7
#define STD_GET_CONFIGURATION   0x8
#define STD_SET_CONFIGURATION   0x9
#define STD_GET_INTERFACE       0xa
#define STD_SET_INTERFACE       0xb
#define STD_SYNCH_FRAME         0xc

/* SETUP data, wValue */
#define WVALUELUSBD         MMIO32(USBD + 0x488)
#define WVALUEHUSBD         MMIO32(USBD + 0x48c)
// WVALUEL[7:0], WVALUEH[7:0]

/* SETUP data, wIndex */
#define WINDEXLUSBD         MMIO32(USBD + 0x490)
#define WINDEXHUSBD         MMIO32(USBD + 0x494)
// WINDEXL[7:0], WINDEXH[7:0]

/* SETUP data,  wLength */
#define WLENGTHLUSBD        MMIO32(USBD + 0x498)
#define WLENGTHHUSBD        MMIO32(USBD + 0x49c)
// WLENGTHL[7:0], WLENGTHH[7:0]

/* Number of bytes received last in the data stage of this OUT endpoint */
#define SIZE_EPOUT0USBD     MMIO32(USBD + 0x4a0)
#define SIZE_EPOUT1USBD     MMIO32(USBD + 0x4a4)
#define SIZE_EPOUT2USBD     MMIO32(USBD + 0x4a8)
#define SIZE_EPOUT3USBD     MMIO32(USBD + 0x4ac)
#define SIZE_EPOUT4USBD     MMIO32(USBD + 0x4b0)
#define SIZE_EPOUT5USBD     MMIO32(USBD + 0x4b4)
#define SIZE_EPOUT6USBD     MMIO32(USBD + 0x4b8)
#define SIZE_EPOUT7USBD     MMIO32(USBD + 0x4bc)
// SIZE[6:0]

/* Number of bytes received last on this ISO OUT data endpoint */
#define SIZE_ISOOUTUSBD     MMIO32(USBD + 0x4c0)
// Number of bytes received last on this ISO OUT data endpoint
#define SIZEUSB_MSK 0x003ff
// Zero-length data packet received
#define ZERO        0x10000

/* Enable USB */
#define ENABLEUSBD          MMIO32(USBD + 0x500)
// 1 = USB peripheral is enabled

/* Control of the USB pull-up */
#define USBPULLUPUSBD       MMIO32(USBD + 0x504)
// Control of the USB pull-up on the D+ line
#define CONNECT_ENABLED 0x1

/* State D+ and D- lines will be forced into by the DPDMDRIVE task. */
#define DPDMVALUEUSBD       MMIO32(USBD + 0x508)
// State D+ and D- lines will be forced into by the DPDMDRIVE task.
#define STATE_RESUME    0x1
#define STATE_J         0x2
#define STATE_K         0x4

/* Data toggle control and status */
#define DTOGGLEUSBD         MMIO32(USBD + 0x50c)
// Select bulk endpoint number
#define EP_MSK      0x007
// Selects IN or OUT endpoint
#define IO          0x080
// Data toggle value
#define VALUE_NOP   0x000
#define VALUE_DATA0 0x100
#define VALUE_DATA1 0x200

/* Endpoint IN enable */
#define EPINENUSBD          MMIO32(USBD + 0x510)
// Enable IN endpoint i
#define IN0     0x001
#define IN1     0x002
#define IN2     0x004
#define IN3     0x008
#define IN4     0x010
#define IN5     0x020
#define IN6     0x040
#define IN7     0x080
// Enable ISO IN endpoint
#define ISOIN   0x100

/* Endpoint OUT enable */
#define EPOUTENUSBD         MMIO32(USBD + 0x514)
// Enable OUT endpoint i
#define OUT0    0x001
#define OUT1    0x002
#define OUT2    0x004
#define OUT3    0x008
#define OUT4    0x010
#define OUT5    0x020
#define OUT6    0x040
#define OUT7    0x080
// Enable ISO OUT endpoint 8
#define ISOOUT  0x100

/* STALL endpoints */
#define EPSTALLUSBD         MMIO32(USBD + 0x518)
// Select endpoint number
// Selects IN or OUT endpoint
// Stall selected endpoint
#define STALL   0x100

/* Controls the split of ISO buffers */
#define ISOSPLITUSBD        MMIO32(USBD + 0x51c)
// Controls the split of ISO buffers
#define SPLIT_OneDir    0x00
#define SPLIT_HalfIN    0x80

/* Returns the current value of the start of frame counter */
#define FRAMECNTRUSBD       MMIO32(USBD + 0x520)
// FRAMECNTR[10:0]

/* Controls USBD peripheral low power mode during USB suspend */
#define LOWPOWERUSBD        MMIO32(USBD + 0x52c)
// enter low power mode
#define LOWPOWER    0x1

/* response of the ISO IN endpoint to an IN token when no data is ready */
#define ISOINCONFIGUSBD     MMIO32(USBD + 0x530)
// Endpoint responds with a zero-length data packet in that case
#define RESPONSE    0x1

/* Data pointer */
#define EPIN0PTRUSBD        MMIO32(USBD + 0x600)
#define EPIN1PTRUSBD        MMIO32(USBD + 0x614)
#define EPIN2PTRUSBD        MMIO32(USBD + 0x628)
#define EPIN3PTRUSBD        MMIO32(USBD + 0x63c)
#define EPIN4PTRUSBD        MMIO32(USBD + 0x650)
#define EPIN5PTRUSBD        MMIO32(USBD + 0x664)
#define EPIN6PTRUSBD        MMIO32(USBD + 0x678)
#define EPIN7PTRUSBD        MMIO32(USBD + 0x68c)
// PTR[31:0]

/* Maximum number of bytes to transfer */
#define EPIN0MAXCNTUSBD     MMIO32(USBD + 0x604)
#define EPIN1MAXCNTUSBD     MMIO32(USBD + 0x618)
#define EPIN2MAXCNTUSBD     MMIO32(USBD + 0x62c)
#define EPIN3MAXCNTUSBD     MMIO32(USBD + 0x640)
#define EPIN4MAXCNTUSBD     MMIO32(USBD + 0x654)
#define EPIN5MAXCNTUSBD     MMIO32(USBD + 0x668)
#define EPIN6MAXCNTUSBD     MMIO32(USBD + 0x67c)
#define EPIN7MAXCNTUSBD     MMIO32(USBD + 0x690)
// MAXCNT[6:0]

/* Number of bytes transferred in the last transaction */
#define EPIN0AMOUNTUSBD     MMIO32(USBD + 0x608)
#define EPIN1AMOUNTUSBD     MMIO32(USBD + 0x61c)
#define EPIN2AMOUNTUSBD     MMIO32(USBD + 0x630)
#define EPIN3AMOUNTUSBD     MMIO32(USBD + 0x644)
#define EPIN4AMOUNTUSBD     MMIO32(USBD + 0x658)
#define EPIN5AMOUNTUSBD     MMIO32(USBD + 0x66c)
#define EPIN6AMOUNTUSBD     MMIO32(USBD + 0x680)
#define EPIN7AMOUNTUSBD     MMIO32(USBD + 0x694)
// AMOUNT[6:0]

/* Data pointer */
#define ISOIN_PTRUSBD       MMIO32(USBD + 0x6a0)
// PTR[31:0]

/* Maximum number of bytes to transfer */
#define ISOIN_MAXCNTUSBD    MMIO32(USBD + 0x6a4)
// MAXCNT[9:0]

/* Number of bytes transferred in the last transaction */
#define ISOIN_AMOUNTUSBD    MMIO32(USBD + 0x6a8)
// AMOUNT[9:0]

/* Data pointer */
#define EPOUT0PTRUSBD       MMIO32(USBD + 0x700)
#define EPOUT1PTRUSBD       MMIO32(USBD + 0x714)
#define EPOUT2PTRUSBD       MMIO32(USBD + 0x728)
#define EPOUT3PTRUSBD       MMIO32(USBD + 0x73c)
#define EPOUT4PTRUSBD       MMIO32(USBD + 0x750)
#define EPOUT5PTRUSBD       MMIO32(USBD + 0x764)
#define EPOUT6PTRUSBD       MMIO32(USBD + 0x778)
#define EPOUT7PTRUSBD       MMIO32(USBD + 0x78c)
// PTR[31:0]

/* Maximum number of bytes to transfer */
#define EPOUT0MAXCNTUSBD    MMIO32(USBD + 0x704)
#define EPOUT1MAXCNTUSBD    MMIO32(USBD + 0x718)
#define EPOUT2MAXCNTUSBD    MMIO32(USBD + 0x72c)
#define EPOUT3MAXCNTUSBD    MMIO32(USBD + 0x740)
#define EPOUT4MAXCNTUSBD    MMIO32(USBD + 0x754)
#define EPOUT5MAXCNTUSBD    MMIO32(USBD + 0x768)
#define EPOUT6MAXCNTUSBD    MMIO32(USBD + 0x77c)
#define EPOUT7MAXCNTUSBD    MMIO32(USBD + 0x790)
// MAXCNT[6:0]

/* Number of bytes transferred in the last transaction */
#define EPOUT0AMOUNTUSBD    MMIO32(USBD + 0x708)
#define EPOUT1AMOUNTUSBD    MMIO32(USBD + 0x71c)
#define EPOUT2AMOUNTUSBD    MMIO32(USBD + 0x730)
#define EPOUT3AMOUNTUSBD    MMIO32(USBD + 0x744)
#define EPOUT4AMOUNTUSBD    MMIO32(USBD + 0x758)
#define EPOUT5AMOUNTUSBD    MMIO32(USBD + 0x76c)
#define EPOUT6AMOUNTUSBD    MMIO32(USBD + 0x780)
#define EPOUT7AMOUNTUSBD    MMIO32(USBD + 0x794)
// AMOUNT[6:0]

/* Data pointer */
#define ISOOUT_PTRUSBD      MMIO32(USBD + 0x7a0)
// PTR[31:0]

/* Maximum number of bytes to transfer */
#define ISOOUT_MAXCNTUSBD   MMIO32(USBD + 0x7a4)
// MAXCNT[9:0]

/* Number of bytes transferred in the last transaction */
#define ISOOUT_AMOUNTUSBD   MMIO32(USBD + 0x7a8)
// AMOUNT[9:0]

#endif
