#ifndef H_SPIS_REG
#define H_SPIS_REG
/*
 * Standard library for Nordic NRF52
 * Register macro for SPI Slave
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/*************Tasks************************/
/* Acquire SPI semaphore */
#define ACQUIRESPIS0    MMIO32(SPIS0 + 0x024)
#define ACQUIRESPIS1    MMIO32(SPIS1 + 0x024)
#define ACQUIRESPIS2    MMIO32(SPIS2 + 0x024)
/* Release SPI semaphore, enabling the SPI slave to acquire it */
#define RELEASESPIS0    MMIO32(SPIS0 + 0x028)
#define RELEASESPIS1    MMIO32(SPIS1 + 0x028)
#define RELEASESPIS2    MMIO32(SPIS2 + 0x028)

/************Events************************/
/* Granted transaction completed */
#define ENDSPIS0        MMIO32(SPIS0 + 0x104)
#define ENDSPIS1        MMIO32(SPIS1 + 0x104)
#define ENDSPIS2        MMIO32(SPIS2 + 0x104)
/* End of RXD buffer reached */
#define ENDRXSPIS0      MMIO32(SPIS0 + 0x110)
#define ENDRXSPIS1      MMIO32(SPIS1 + 0x110)
#define ENDRXSPIS2      MMIO32(SPIS2 + 0x110)
/* Semaphore acquired */
#define ACQUIREDSPIS0   MMIO32(SPIS0 + 0x128)
#define ACQUIREDSPIS1   MMIO32(SPIS1 + 0x128)
#define ACQUIREDSPIS2   MMIO32(SPIS2 + 0x128)

/************Registers*********************/
/* Shortcuts between local events and tasks */
#define SHORTSSPIS0     MMIO32(SPIS0 + 0x200)
#define SHORTSSPIS1     MMIO32(SPIS1 + 0x200)
#define SHORTSSPIS2     MMIO32(SPIS2 + 0x200)
// Shortcut between END event and ACQUIRE task
#define END_ACQUIRE 0x4

/* Enable interrupt */
#define INTENSETSPIS0   MMIO32(SPIS0 + 0x304)
#define INTENSETSPIS1   MMIO32(SPIS1 + 0x304)
#define INTENSETSPIS2   MMIO32(SPIS2 + 0x304)
// Write '1' to enable interrupt for event END
#define ENDSPIS         0x002
// Write '1' to enable interrupt for event ENDRX
#define ENDRXSPIS       0x010
// Write '1' to enable interrupt for event ACQUIRED
#define ACQUIREDSPIS    0x400

/* Disable interrupt */
#define INTENCLRSPIS0   MMIO32(SPIS0 + 0x308)
#define INTENCLRSPIS1   MMIO32(SPIS1 + 0x308)
#define INTENCLRSPIS2   MMIO32(SPIS2 + 0x308)
// Write '1' to disable interrupt for event END
// Write '1' to disable interrupt for event ENDRX
// Write '1' to disable interrupt for event ACQUIRED

/* Semaphore status register */
#define SEMSTATSPIS0    MMIO32(SPIS0 + 0x400)
#define SEMSTATSPIS1    MMIO32(SPIS1 + 0x400)
#define SEMSTATSPIS2    MMIO32(SPIS2 + 0x400)
// Semaphore status
#define SEMSTAT_FREE        0x0
#define SEMSTAT_CPU         0x1
#define SEMSTAT_SPIS        0x2
#define SEMSTAT_CPUPending  0x3

/* Status from last transaction */
#define STATUSSPIS0     MMIO32(SPIS0 + 0x440)
#define STATUSSPIS1     MMIO32(SPIS1 + 0x440)
#define STATUSSPIS2     MMIO32(SPIS2 + 0x440)
// TX buffer over-read detected, and prevented.
#define OVERREADS    0x1
// RX buffer overflow detected, and prevented.
#define OVERFLOWS    0x2

/* Enable SPI slave */
#define ENABLESPIS0     MMIO32(SPIS0 + 0x500)
#define ENABLESPIS1     MMIO32(SPIS1 + 0x500)
#define ENABLESPIS2     MMIO32(SPIS2 + 0x500)
// Enable or disable SPI slave.
#define ENABLESPIS  0x2

/* Pin select for SCK */
#define PSELSCKSPIS0    MMIO32(SPIS0 + 0x508)
#define PSELSCKSPIS1    MMIO32(SPIS1 + 0x508)
#define PSELSCKSPIS2    MMIO32(SPIS2 + 0x508)

/* Pin select for MISO */
#define PSELMISOSPIS0   MMIO32(SPIS0 + 0x50c)
#define PSELMISOSPIS1   MMIO32(SPIS1 + 0x50c)
#define PSELMISOSPIS2   MMIO32(SPIS2 + 0x50c)

/* Pin select for MOSI */
#define PSELMOSISPIS0   MMIO32(SPIS0 + 0x510)
#define PSELMOSISPIS1   MMIO32(SPIS1 + 0x510)
#define PSELMOSISPIS2   MMIO32(SPIS2 + 0x510)

/* Pin select for CSN */
#define PSELCSNSPIS0    MMIO32(SPIS0 + 0x514)
#define PSELCSNSPIS1    MMIO32(SPIS1 + 0x514)
#define PSELCSNSPIS2    MMIO32(SPIS2 + 0x514)

/* RXD data pointer */
#define RXD_PTRSPIS0    MMIO32(SPIS0 + 0x534)
#define RXD_PTRSPIS1    MMIO32(SPIS1 + 0x534)
#define RXD_PTRSPIS2    MMIO32(SPIS2 + 0x534)
// RXD data pointer. RXDPTR[31:0]

/* Maximum number of bytes in receive buffer */
#define RXD_MAXCNTSPIS0 MMIO32(SPIS0 + 0x538)
#define RXD_MAXCNTSPIS1 MMIO32(SPIS1 + 0x538)
#define RXD_MAXCNTSPIS2 MMIO32(SPIS2 + 0x538)
// Maximum number of bytes in receive buffer. MAXCNT[15:0]

/* Number of bytes received in last granted transaction */
#define RXD_AMOUNTSPIS0 MMIO32(SPIS0 + 0x53c)
#define RXD_AMOUNTSPIS1 MMIO32(SPIS1 + 0x53c)
#define RXD_AMOUNTSPIS2 MMIO32(SPIS2 + 0x53c)
// Number of bytes received in the last granted transaction. AMOUNT[15:0]

/* EasyDMA list type */
#define RXD_LISTSPIS0   MMIO32(SPIS0 + 0x540)
#define RXD_LISTSPIS0   MMIO32(SPIS1 + 0x540)
#define RXD_LISTSPIS0   MMIO32(SPIS2 + 0x540)
// Disable EasyDMA list, Use array list
#define LISTSPIS    0x1

/* TXD data pointer */
#define TXD_PTRSPIS0    MMIO32(SPIS0 + 0x544)
#define TXD_PTRSPIS1    MMIO32(SPIS1 + 0x544)
#define TXD_PTRSPIS2    MMIO32(SPIS2 + 0x544)
// TXD data pointer. TXDPTR[31:0]

/* Maximum number of bytes in transmit buffer */
#define TXD_MAXCNTSPIS0 MMIO32(SPIS0 + 0x548)
#define TXD_MAXCNTSPIS1 MMIO32(SPIS1 + 0x548)
#define TXD_MAXCNTSPIS2 MMIO32(SPIS2 + 0x548)
// Maximum number of bytes in transmit buffer. MAXCNT[15:0]

/* Number of bytes transmitted in last granted transaction */
#define TXD_AMOUNTSPIS0 MMIO32(SPIS0 + 0x54c)
#define TXD_AMOUNTSPIS1 MMIO32(SPIS1 + 0x54c)
#define TXD_AMOUNTSPIS2 MMIO32(SPIS2 + 0x54c)
// Number of bytes transmitted in last granted transaction. AMOUNT[15:0]

/* EasyDMA list type */
#define TXD_LISTSPIS0   MMIO32(SPIS0 + 0x550)
#define TXD_LISTSPIS1   MMIO32(SPIS1 + 0x550)
#define TXD_LISTSPIS2   MMIO32(SPIS2 + 0x550)
// Disable EasyDMA list, Use array list

/* Configuration register */
#define CONFIGSPIS0     MMIO32(SPIS0 + 0x554)
#define CONFIGSPIS1     MMIO32(SPIS1 + 0x554)
#define CONFIGSPIS2     MMIO32(SPIS2 + 0x554)
// Bit order
#define ORDERSPIS   0x1
// Serial clock (SCK) phase
#define CPHASPIS    0x2
// Serial clock (SCK) polarity
#define CPOLSPIS    0x4

/* Default character. Character clocked out in case of an ignored transaction*/
#define DEFSPIS0        MMIO32(SPIS0 + 0x55c)
#define DEFSPIS1        MMIO32(SPIS1 + 0x55c)
#define DEFSPIS2        MMIO32(SPIS2 + 0x55c)
// Default character. Character clocked out in case of an ignored transaction.

/* Over-read character */
#define ORCSPIS0        MMIO32(SPIS0 + 0x5c0)
#define ORCSPIS1        MMIO32(SPIS1 + 0x5c0)
#define ORCSPIS2        MMIO32(SPIS2 + 0x5c0)
//Over-read character. Char clocked out after an over-read of the tx buffr.

#endif
