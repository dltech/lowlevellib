#ifndef H_ADC_REG
#define H_ADC_REG
/*
 * Standard library for Nordic NRF52
 * Register macro for Successive approximation analog-to-digital converter
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/*************Tasks************************/
/* Starts the SAADC and prepares the result buffer in RAM */
#define STARTADC            MMIO32(SAADC + 0x000)
/* Takes one SAADC sample */
#define SAMPLEADC           MMIO32(SAADC + 0x004)
/* Stops the SAADC and terminates all on-going conversions */
#define STOPADC             MMIO32(SAADC + 0x008)
/* Starts offset auto-calibration */
#define CALIBRATEOFFSET     MMIO32(SAADC + 0x00c)

/************Events************************/
/* The SAADC has started */
#define STARTEDADC          MMIO32(SAADC + 0x100)
/* The SAADC has filled up the result buffer */
#define ENDADC              MMIO32(SAADC + 0x104)
/* A conversion task has been completed. */
#define DONEADC             MMIO32(SAADC + 0x108)
/* Result ready for transfer to RAM */
#define RESULTDONEADC       MMIO32(SAADC + 0x10c)
/* Calibration is complete */
#define CALIBRATEDONEADC    MMIO32(SAADC + 0x110)
/* The SAADC has stopped */
#define STOPPEDADC          MMIO32(SAADC + 0x114)
/* Last result is equal or above CH[n].LIMIT.HIGH */
#define CH0LIMITHADC        MMIO32(SAADC + 0x118)
#define CH1LIMITHADC        MMIO32(SAADC + 0x120)
#define CH2LIMITHADC        MMIO32(SAADC + 0x128)
#define CH3LIMITHADC        MMIO32(SAADC + 0x130)
#define CH4LIMITHADC        MMIO32(SAADC + 0x138)
#define CH5LIMITHADC        MMIO32(SAADC + 0x140)
#define CH6LIMITHADC        MMIO32(SAADC + 0x148)
#define CH7LIMITHADC        MMIO32(SAADC + 0x150)
/* Last result is equal or below CH[n].LIMIT.LOW */
#define CH0LIMITLADC        MMIO32(SAADC + 0x11c)
#define CH1LIMITLADC        MMIO32(SAADC + 0x124)
#define CH2LIMITLADC        MMIO32(SAADC + 0x12c)
#define CH3LIMITLADC        MMIO32(SAADC + 0x134)
#define CH4LIMITLADC        MMIO32(SAADC + 0x13c)
#define CH5LIMITLADC        MMIO32(SAADC + 0x144)
#define CH6LIMITLADC        MMIO32(SAADC + 0x14c)
#define CH7LIMITLADC        MMIO32(SAADC + 0x154)

/************Registers*********************/
/* Enable or disable interrupt */
#define INTENADC            MMIO32(SAADC + 0x300)
// Enable or disable interrupt for event STARTED
#define STARTEDA        0x000001
// Enable or disable interrupt for event END
#define ENDA            0x000002
// Enable or disable interrupt for event DONE
#define DONEA           0x000004
// Enable or disable interrupt for event RESULTDONE
#define RESULTDONE      0x000008
// Enable or disable interrupt for event CALIBRATEDONE
#define CALIBRATEDONE   0x000010
// Enable or disable interrupt for event STOPPED
#define STOPPED         0x000020
// Enable or disable interrupt for event CH0LIMITH
#define CH0LIMITH       0x000040
// Enable or disable interrupt for event CH0LIMITL
#define CH0LIMITL       0x000080
// Enable or disable interrupt for event CH1LIMITH
#define CH1LIMITH       0x000100
// Enable or disable interrupt for event CH1LIMITL
#define CH1LIMITL       0x000200
// Enable or disable interrupt for event CH2LIMITH
#define CH2LIMITH       0x000400
// Enable or disable interrupt for event CH2LIMITL
#define CH2LIMITL       0x000800
// Enable or disable interrupt for event CH3LIMITH
#define CH3LIMITH       0x001000
// Enable or disable interrupt for event CH3LIMITL
#define CH3LIMITL       0x002000
// Enable or disable interrupt for event CH4LIMITH
#define CH4LIMITH       0x004000
// Enable or disable interrupt for event CH4LIMITL
#define CH4LIMITL       0x008000
// Enable or disable interrupt for event CH5LIMITH
#define CH5LIMITH       0x010000
// Enable or disable interrupt for event CH5LIMITL
#define CH5LIMITL       0x020000
// Enable or disable interrupt for event CH6LIMITH
#define CH6LIMITH       0x040000
// Enable or disable interrupt for event CH6LIMITL
#define CH6LIMITL       0x080000
// Enable or disable interrupt for event CH7LIMITH
#define CH7LIMITH       0x100000
// Enable or disable interrupt for event CH7LIMITL
#define CH7LIMITL       0x200000

/* Enable interrupt */
#define INTENSETADC         MMIO32(SAADC + 0x304)
// Write '1' to enable interrupt for event STARTED
// Write '1' to enable interrupt for event END
// Write '1' to enable interrupt for event DONE
// Write '1' to enable interrupt for event RESULTDONE
// Write '1' to enable interrupt for event CALIBRATEDONE
// Write '1' to enable interrupt for event STOPPED
// Write '1' to enable interrupt for event CH0LIMITH
// Write '1' to enable interrupt for event CH0LIMITL
// Write '1' to enable interrupt for event CH1LIMITH
// Write '1' to enable interrupt for event CH1LIMITL
// Write '1' to enable interrupt for event CH2LIMITH
// Write '1' to enable interrupt for event CH2LIMITL
// Write '1' to enable interrupt for event CH3LIMITH
// Write '1' to enable interrupt for event CH3LIMITL
// Write '1' to enable interrupt for event CH4LIMITH
// Write '1' to enable interrupt for event CH4LIMITL
// Write '1' to enable interrupt for event CH5LIMITH
// Write '1' to enable interrupt for event CH5LIMITL
// Write '1' to enable interrupt for event CH6LIMITH
// Write '1' to enable interrupt for event CH6LIMITL
// Write '1' to enable interrupt for event CH7LIMITH
// Write '1' to enable interrupt for event CH7LIMITL

/* Disable interrupt */
#define INTENCLRADC         MMIO32(SAADC + 0x308)
// Write '1' to disable interrupt for event STARTED
// Write '1' to disable interrupt for event END
// Write '1' to disable interrupt for event DONE
// Write '1' to disable interrupt for event RESULTDONE
// Write '1' to disable interrupt for event CALIBRATEDONE
// Write '1' to disable interrupt for event STOPPED
// Write '1' to disable interrupt for event CH0LIMITH
// Write '1' to disable interrupt for event CH0LIMITL
// Write '1' to disable interrupt for event CH1LIMITH
// Write '1' to disable interrupt for event CH1LIMITL
// Write '1' to disable interrupt for event CH2LIMITH
// Write '1' to disable interrupt for event CH2LIMITL
// Write '1' to disable interrupt for event CH3LIMITH
// Write '1' to disable interrupt for event CH3LIMITL
// Write '1' to disable interrupt for event CH4LIMITH
// Write '1' to disable interrupt for event CH4LIMITL
// Write '1' to disable interrupt for event CH5LIMITH
// Write '1' to disable interrupt for event CH5LIMITL
// Write '1' to disable interrupt for event CH6LIMITH
// Write '1' to disable interrupt for event CH6LIMITL
// Write '1' to disable interrupt for event CH7LIMITH
// Write '1' to disable interrupt for event CH7LIMITL

/* Status */
#define STATUSADC           MMIO32(SAADC + 0x400)
// 1 = ADC is busy. Conversion in progress.
#define BUSYA   0x1

/* Enable or disable SAADC */
#define ENABLEADC           MMIO32(SAADC + 0x500)

/* Input positive pin selection for CH[n] */
#define CH0PSELP            MMIO32(SAADC + 0x510)
#define CH1PSELP            MMIO32(SAADC + 0x520)
#define CH2PSELP            MMIO32(SAADC + 0x530)
#define CH3PSELP            MMIO32(SAADC + 0x540)
#define CH4PSELP            MMIO32(SAADC + 0x550)
#define CH5PSELP            MMIO32(SAADC + 0x560)
#define CH6PSELP            MMIO32(SAADC + 0x570)
#define CH7PSELP            MMIO32(SAADC + 0x580)
// Analog positive input channel
#define PSELP_NC        0x0
#define PSELP_AIN0      0x1
#define PSELP_AIN1      0x2
#define PSELP_AIN2      0x3
#define PSELP_AIN3      0x4
#define PSELP_AIN4      0x5
#define PSELP_AIN5      0x6
#define PSELP_AIN6      0x7
#define PSELP_AIN7      0x8
#define PSELP_VDD       0x9
#define PSELP_VDDHDIV5  0xd

/* Input negative pin selection for CH[n] */
#define CH0PSELN            MMIO32(SAADC + 0x514)
#define CH1PSELN            MMIO32(SAADC + 0x524)
#define CH2PSELN            MMIO32(SAADC + 0x534)
#define CH3PSELN            MMIO32(SAADC + 0x544)
#define CH4PSELN            MMIO32(SAADC + 0x554)
#define CH5PSELN            MMIO32(SAADC + 0x564)
#define CH6PSELN            MMIO32(SAADC + 0x574)
#define CH7PSELN            MMIO32(SAADC + 0x584)
// Analog negative input, enables differential channel

/* Input configuration for CH[n] */
#define CH0CONFIG           MMIO32(SAADC + 0x518)
#define CH1CONFIG           MMIO32(SAADC + 0x528)
#define CH2CONFIG           MMIO32(SAADC + 0x538)
#define CH3CONFIG           MMIO32(SAADC + 0x548)
#define CH4CONFIG           MMIO32(SAADC + 0x558)
#define CH5CONFIG           MMIO32(SAADC + 0x568)
#define CH6CONFIG           MMIO32(SAADC + 0x578)
#define CH7CONFIG           MMIO32(SAADC + 0x588)
// Positive channel resistor control
#define RESP_BYPASS     0x0000000
#define RESP_PULLDOWN   0x0000001
#define RESP_PULLUP     0x0000002
#define RESP_VDD1_2     0x0000003
// Negative channel resistor control
#define RESN_BYPASS     0x0000000
#define RESN_PULLDOWN   0x0000010
#define RESN_PULLUP     0x0000020
#define RESN_VDD1_2     0x0000030
// Gain control
#define GAIN1_6         0x0000000
#define GAIN1_5         0x0000100
#define GAIN1_4         0x0000200
#define GAIN1_3         0x0000300
#define GAIN1_2         0x0000400
#define GAIN1           0x0000500
#define GAIN2           0x0000600
#define GAIN4           0x0000700
// Reference control
#define REFSEL          0x0001000
// Acquisition time, the time the SAADC uses to sample the input voltage
#define TACQ3US         0x0000000
#define TACQ5US         0x0010000
#define TACQ10US        0x0020000
#define TACQ15US        0x0030000
#define TACQ20US        0x0040000
#define TACQ40US        0x0050000
// Enable differential mode
#define MODE_DIFF       0x0100000
// Enable burst mode
#define BURST           0x1000000

/* High/low limits for event monitoring of a channel */
#define CH0LIMIT            MMIO32(SAADC + 0x51c)
#define CH1LIMIT            MMIO32(SAADC + 0x52c)
#define CH2LIMIT            MMIO32(SAADC + 0x53c)
#define CH3LIMIT            MMIO32(SAADC + 0x54c)
#define CH4LIMIT            MMIO32(SAADC + 0x55c)
#define CH5LIMIT            MMIO32(SAADC + 0x56c)
#define CH6LIMIT            MMIO32(SAADC + 0x57c)
#define CH7LIMIT            MMIO32(SAADC + 0x58c)
// Low level limit [-32768 to +32767]
#define LOWA_MSK        0x0000ffff
#define LOWA_GET(n)     (n&LOWA_MSK)
#define LOWA_SET(x)     (x&LOWA_MSK)
// High level limit [-32768 to +32767]
#define HIGHA_MSK       0xffff0000
#define HIGHA_SFT       16
#define HIGHA_GET(n)    ((n>>HIGHA_SFT)&0xffff)
#define HIGHA_SET(x)    ((x<<HIGHA_SFT)&HIGHA_MSK)

/* Resolution configuration */
#define RESOLUTION          MMIO32(SAADC + 0x5f0)
// Set the resolution.
#define VAL8BIT     0x0
#define VAL10BIT    0x1
#define VAL12BIT    0x2
#define VAL14BIT    0x3

/* Oversampling configuration. */
#define OVERSAMPLE          MMIO32(SAADC + 0x5f4)
// Oversample control
#define OVERSAMPLE_BYPASS   0x0
#define OVERSAMPLE2X        0x1
#define OVERSAMPLE4X        0x2
#define OVERSAMPLE8X        0x3
#define OVERSAMPLE16X       0x4
#define OVERSAMPLE32X       0x5
#define OVERSAMPLE64X       0x6
#define OVERSAMPLE128X      0x7
#define OVERSAMPLE256X      0x8

/* Controls normal or continuous sample rate */
#define SAMPLERATE          MMIO32(SAADC + 0x5f8)
// Capture and compare value. Sample rate is 16 MHz/CC
#define CC_MSK      0x07ff
#define CC_GET      (SAMPLERATE&CC_MSK)
#define CC_SET(x)   (x&CC_MSK)
// Select mode for sample rate control
#define MODEA       0x1000

/* Data pointer */
#define RESULT_PTR          MMIO32(SAADC + 0x62c)

/* Maximum number of 16-bit samples to be written to output RAM buffer */
#define RESULT_MAXCNT       MMIO32(SAADC + 0x630)
// MAXCNT[14:0]

/* Number of 16-bit samples wr to output RAM buffer since the prev START */
#define RESULT_AMOUNT       MMIO32(SAADC + 0x634)
// This register can be read after an END or STOPPED event. AMOUNT[14:0]

#endif
