#ifndef H_PWM_REG
#define H_PWM_REG
/*
 * Standard library for Nordic NRF52
 * The pulse with modulation (PWM) module Registers
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/************Events************************/
/* Stops PWM pulse generation on all ch at the end of current PWM period */
#define STOPPWM0            MMIO32(PWM0 + 0x004)
#define STOPPWM1            MMIO32(PWM1 + 0x004)
#define STOPPWM2            MMIO32(PWM2 + 0x004)
#define STOPPWM3            MMIO32(PWM3 + 0x004)
/* Loads the first PWM value on all enabled channels from sequence n */
#define SEQSTART0PWM0       MMIO32(PWM0 + 0x008)
#define SEQSTART0PWM1       MMIO32(PWM1 + 0x008)
#define SEQSTART0PWM2       MMIO32(PWM2 + 0x008)
#define SEQSTART0PWM3       MMIO32(PWM3 + 0x008)
#define SEQSTART1PWM0       MMIO32(PWM0 + 0x00c)
#define SEQSTART1PWM1       MMIO32(PWM1 + 0x00c)
#define SEQSTART1PWM2       MMIO32(PWM2 + 0x00c)
#define SEQSTART1PWM3       MMIO32(PWM3 + 0x00c)
/* Steps by one value in the current sequence on all enabled channels */
#define NEXTSTEPPWM0        MMIO32(PWM0 + 0x010)
#define NEXTSTEPPWM1        MMIO32(PWM1 + 0x010)
#define NEXTSTEPPWM2        MMIO32(PWM2 + 0x010)
#define NEXTSTEPPWM3        MMIO32(PWM3 + 0x010)
/* Response to STOP task, emitted when PWM pulses are no longer generated */
#define STOPPEDPWM0         MMIO32(PWM0 + 0x104)
#define STOPPEDPWM1         MMIO32(PWM1 + 0x104)
#define STOPPEDPWM2         MMIO32(PWM2 + 0x104)
#define STOPPEDPWM3         MMIO32(PWM3 + 0x104)
/* First PWM period started on sequence n */
#define SEQSTARTED0PWM0     MMIO32(PWM0 + 0x108)
#define SEQSTARTED0PWM1     MMIO32(PWM1 + 0x108)
#define SEQSTARTED0PWM2     MMIO32(PWM2 + 0x108)
#define SEQSTARTED0PWM3     MMIO32(PWM3 + 0x108)
#define SEQSTARTED1PWM0     MMIO32(PWM0 + 0x10c)
#define SEQSTARTED1PWM1     MMIO32(PWM1 + 0x10c)
#define SEQSTARTED1PWM2     MMIO32(PWM2 + 0x10c)
#define SEQSTARTED1PWM3     MMIO32(PWM3 + 0x10c)
/* Emitted at end of every sequence n */
#define SEQEND0PWM0         MMIO32(PWM0 + 0x110)
#define SEQEND0PWM1         MMIO32(PWM1 + 0x110)
#define SEQEND0PWM2         MMIO32(PWM2 + 0x110)
#define SEQEND0PWM3         MMIO32(PWM3 + 0x110)
#define SEQEND1PWM0         MMIO32(PWM0 + 0x114)
#define SEQEND1PWM1         MMIO32(PWM1 + 0x114)
#define SEQEND1PWM2         MMIO32(PWM2 + 0x114)
#define SEQEND1PWM3         MMIO32(PWM3 + 0x114)
/* Emitted at the end of each PWM period */
#define PWMPERIODENDPWM0    MMIO32(PWM0 + 0x118)
#define PWMPERIODENDPWM1    MMIO32(PWM1 + 0x118)
#define PWMPERIODENDPWM2    MMIO32(PWM2 + 0x118)
#define PWMPERIODENDPWM3    MMIO32(PWM3 + 0x118)
/* Concatenated seqs have been played the amount of times def in LOOP.CNT */
#define LOOPSDONEPWM0       MMIO32(PWM0 + 0x11c)
#define LOOPSDONEPWM1       MMIO32(PWM1 + 0x11c)
#define LOOPSDONEPWM2       MMIO32(PWM2 + 0x11c)
#define LOOPSDONEPWM3       MMIO32(PWM3 + 0x11c)

/************Registers*********************/
/* Shortcuts between local events and tasks */
#define SHORTSPWM0          MMIO32(PWM0 + 0x200)
#define SHORTSPWM1          MMIO32(PWM1 + 0x200)
#define SHORTSPWM2          MMIO32(PWM2 + 0x200)
#define SHORTSPWM3          MMIO32(PWM3 + 0x200)
// Shortcut between event SEQEND[0] and task STOP
#define SEQEND0_STOP        0x01
// Shortcut between event SEQEND[1] and task STOP
#define SEQEND1_STOP        0x02
// Shortcut between event LOOPSDONE and task SEQSTART[0]
#define LOOPSDONE_SEQSTART0 0x04
// Shortcut between event LOOPSDONE and task SEQSTART[1]
#define LOOPSDONE_SEQSTART1 0x08
// Shortcut between event LOOPSDONE and task STOP
#define LOOPSDONE_STOP      0x10

/* Enable or disable interrupt */
#define INTENPWM0           MMIO32(PWM0 + 0x300)
#define INTENPWM1           MMIO32(PWM1 + 0x300)
#define INTENPWM2           MMIO32(PWM2 + 0x300)
#define INTENPWM3           MMIO32(PWM3 + 0x300)
// Enable or disable interrupt for event STOPPED
#define STOPPED         0x02
// Enable or disable interrupt for event SEQSTARTED[i]
#define SEQSTARTED0     0x04
#define SEQSTARTED1     0x08
// Enable or disable interrupt for event SEQEND[i]
#define SEQEND0         0x10
#define SEQEND1         0x20
// Enable or disable interrupt for event PWMPERIODEND
#define PWMPERIODEND    0x40
// Enable or disable interrupt for event LOOPSDONE
#define LOOPSDONE       0x80

/* Enable interrupt */
#define INTENSETPWM0        MMIO32(PWM0 + 0x304)
#define INTENSETPWM1        MMIO32(PWM1 + 0x304)
#define INTENSETPWM2        MMIO32(PWM2 + 0x304)
#define INTENSETPWM3        MMIO32(PWM3 + 0x304)
// Write '1' to enable interrupt for event STOPPED
// Write '1' to enable interrupt for event SEQSTARTED[i]
// Write '1' to enable interrupt for event SEQEND[i]
// Write '1' to enable interrupt for event PWMPERIODEND
// Write '1' to enable interrupt for event LOOPSDONE

/* Disable interrupt */
#define INTENCLRPWM0        MMIO32(PWM0 + 0x308)
#define INTENCLRPWM1        MMIO32(PWM1 + 0x308)
#define INTENCLRPWM2        MMIO32(PWM2 + 0x308)
#define INTENCLRPWM3        MMIO32(PWM3 + 0x308)
// Write '1' to disable interrupt for event STOPPED
// Write '1' to disable interrupt for event SEQSTARTED[i]
// Write '1' to disable interrupt for event SEQEND[i]
// Write '1' to disable interrupt for event PWMPERIODEND
// Write '1' to disable interrupt for event LOOPSDONE

/* PWM module enable register */
#define ENABLEPWM0          MMIO32(PWM0 + 0x500)
#define ENABLEPWM1          MMIO32(PWM1 + 0x500)
#define ENABLEPWM2          MMIO32(PWM2 + 0x500)
#define ENABLEPWM3          MMIO32(PWM3 + 0x500)
// Enable or disable PWM module
#define ENABLE_PWM  0x1

/* Selects operating mode of the wave counter */
#define MODEPWM0            MMIO32(PWM0 + 0x504)
#define MODEPWM1            MMIO32(PWM1 + 0x504)
#define MODEPWM2            MMIO32(PWM2 + 0x504)
#define MODEPWM3            MMIO32(PWM3 + 0x504)
// Selects up mode or up-and-down mode for the counter
#define UPDOWN  0x1

/* Value up to which the pulse generator counter counts */
#define COUNTERTOPPWM0      MMIO32(PWM0 + 0x508)
#define COUNTERTOPPWM1      MMIO32(PWM1 + 0x508)
#define COUNTERTOPPWM2      MMIO32(PWM2 + 0x508)
#define COUNTERTOPPWM3      MMIO32(PWM3 + 0x508)
// Value up to which the pulse generator counter counts.
#define COUNTERTOP_MSK  0x7fff

/* Configuration for PWM_CLK */
#define PRESCALERPWM0       MMIO32(PWM0 + 0x50c)
#define PRESCALERPWM1       MMIO32(PWM1 + 0x50c)
#define PRESCALERPWM2       MMIO32(PWM2 + 0x50c)
#define PRESCALERPWM3       MMIO32(PWM3 + 0x50c)
// Prescaler of PWM_CLK
#define PRESCALER_PWM_DIV1      0x0
#define PRESCALER_PWM_DIV2      0x1
#define PRESCALER_PWM_DIV4      0x2
#define PRESCALER_PWM_DIV8      0x3
#define PRESCALER_PWM_DIV16     0x4
#define PRESCALER_PWM_DIV32     0x5
#define PRESCALER_PWM_DIV64     0x6
#define PRESCALER_PWM_DIV128    0x7

/* Configuration of the decoder */
#define DECODERPWM0         MMIO32(PWM0 + 0x510)
#define DECODERPWM1         MMIO32(PWM1 + 0x510)
#define DECODERPWM2         MMIO32(PWM2 + 0x510)
#define DECODERPWM3         MMIO32(PWM3 + 0x510)
// How a sequence is read from RAM and spread to the compare register
#define LOAD_COMMON     0x000
#define LOAD_GROUPED    0x001
#define LOAD_INDIVIDUAL 0x002
#define LOAD_WAVEFORM   0x003
// Selects source for advancing the active sequence
#define MODE_NEXTSTEP   0x100

/* Number of playbacks of a loop */
#define LOOPPWM0            MMIO32(PWM0 + 0x514)
#define LOOPPWM1            MMIO32(PWM1 + 0x514)
#define LOOPPWM2            MMIO32(PWM2 + 0x514)
#define LOOPPWM3            MMIO32(PWM3 + 0x514)
// Number of playbacks of pattern cycles
#define LOOP_CNT_PWM_MSK    0xffff

/* Beginning address in RAM of this sequence */
#define SEQ0PTRPWM0         MMIO32(PWM0 + 0x520)
#define SEQ0PTRPWM1         MMIO32(PWM1 + 0x520)
#define SEQ0PTRPWM2         MMIO32(PWM2 + 0x520)
#define SEQ0PTRPWM3         MMIO32(PWM3 + 0x520)
#define SEQ1PTRPWM0         MMIO32(PWM0 + 0x540)
#define SEQ1PTRPWM1         MMIO32(PWM1 + 0x540)
#define SEQ1PTRPWM2         MMIO32(PWM2 + 0x540)
#define SEQ1PTRPWM3         MMIO32(PWM3 + 0x540)
// Beginning address in RAM of this sequence

/* Number of values (duty cycles) in this sequence */
#define SEQ0CNTPWM0         MMIO32(PWM0 + 0x524)
#define SEQ0CNTPWM1         MMIO32(PWM1 + 0x524)
#define SEQ0CNTPWM2         MMIO32(PWM2 + 0x524)
#define SEQ0CNTPWM3         MMIO32(PWM3 + 0x524)
#define SEQ1CNTPWM0         MMIO32(PWM0 + 0x544)
#define SEQ1CNTPWM1         MMIO32(PWM1 + 0x544)
#define SEQ1CNTPWM2         MMIO32(PWM2 + 0x544)
#define SEQ1CNTPWM3         MMIO32(PWM3 + 0x544)
// Number of values (duty cycles) in this sequence
#define SEQ_CNT_PWM_MSK 0x7fff

/* Number of add PWM periods between samples loaded into compare register */
#define SEQ0REFRESHPWM0     MMIO32(PWM0 + 0x528)
#define SEQ0REFRESHPWM1     MMIO32(PWM1 + 0x528)
#define SEQ0REFRESHPWM2     MMIO32(PWM2 + 0x528)
#define SEQ0REFRESHPWM3     MMIO32(PWM3 + 0x528)
#define SEQ1REFRESHPWM0     MMIO32(PWM0 + 0x548)
#define SEQ1REFRESHPWM1     MMIO32(PWM1 + 0x548)
#define SEQ1REFRESHPWM2     MMIO32(PWM2 + 0x548)
#define SEQ1REFRESHPWM3     MMIO32(PWM3 + 0x548)
// Number of additional PWM periods between samples loaded into compare reg
#define REFRESH_CNT_PWM_MSK 0xffffff

/* Time added after the sequence */
#define SEQ0ENDDELAYPWM0    MMIO32(PWM0 + 0x52c)
#define SEQ0ENDDELAYPWM1    MMIO32(PWM1 + 0x52c)
#define SEQ0ENDDELAYPWM2    MMIO32(PWM2 + 0x52c)
#define SEQ0ENDDELAYPWM3    MMIO32(PWM3 + 0x52c)
#define SEQ1ENDDELAYPWM0    MMIO32(PWM0 + 0x54c)
#define SEQ1ENDDELAYPWM1    MMIO32(PWM1 + 0x54c)
#define SEQ1ENDDELAYPWM2    MMIO32(PWM2 + 0x54c)
#define SEQ1ENDDELAYPWM3    MMIO32(PWM3 + 0x54c)
// Time added after the sequence in PWM periods
#define END_CNT_PWM_MSK 0xffffff

/* Output pin select for PWM channel n */
#define PSEL_OUT0PWM0       MMIO32(PWM0 + 0x560)
#define PSEL_OUT0PWM1       MMIO32(PWM1 + 0x560)
#define PSEL_OUT0PWM2       MMIO32(PWM2 + 0x560)
#define PSEL_OUT0PWM3       MMIO32(PWM3 + 0x560)
#define PSEL_OUT1PWM0       MMIO32(PWM0 + 0x564)
#define PSEL_OUT1PWM1       MMIO32(PWM1 + 0x564)
#define PSEL_OUT1PWM2       MMIO32(PWM2 + 0x564)
#define PSEL_OUT1PWM3       MMIO32(PWM3 + 0x564)
#define PSEL_OUT2PWM0       MMIO32(PWM0 + 0x568)
#define PSEL_OUT2PWM1       MMIO32(PWM1 + 0x568)
#define PSEL_OUT2PWM2       MMIO32(PWM2 + 0x568)
#define PSEL_OUT2PWM3       MMIO32(PWM3 + 0x568)
#define PSEL_OUT3PWM0       MMIO32(PWM0 + 0x56c)
#define PSEL_OUT3PWM1       MMIO32(PWM1 + 0x56c)
#define PSEL_OUT3PWM2       MMIO32(PWM2 + 0x56c)
#define PSEL_OUT3PWM3       MMIO32(PWM3 + 0x56c)
// Pin number
#define PINPWM_MSK  0x1f
// Port number
#define PORTPWM     0x20
// Connection
#define CONNECTPWM  0x80000000

#endif
