#ifndef H_PATTERN
#define H_PATTERN
/*
 * Standard library for NRF52
 * Repetitive macro from register definitions.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* port select (PSEL) registers */
// Pin number [0..31]
#define PIN_MSK     0x1f
#define PIN_SET(x)  (x&PIN_MSK)
// Port number
#define PORT        0x20
// Connection
#define CONNECT     0x80000000

/* Enable bit (always first) */
#define ENABLE  0x1


#endif
