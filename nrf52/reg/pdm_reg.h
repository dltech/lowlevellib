#ifndef H_PDM_REG
#define H_PDM_REG
/*
 * Standard library for Nordic NRF52
 * Pulse density modulation interface module registers.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/*************Tasks************************/
/* Starts continuous PDM transfer */
#define STARTPDM        MMIO32(PDM + 0x000)
/* Stops PDM transfer */
#define STOPPDM         MMIO32(PDM + 0x004)

/************Events************************/
/* PDM transfer has started */
#define STARTEDPDM      MMIO32(PDM + 0x100)
/* PDM transfer has finished */
#define STOPPEDPDM      MMIO32(PDM + 0x104)
/* The PDM has written the last sample specified by SAMPLE.MAXCNT to Data RAM */
#define ENDPDM          MMIO32(PDM + 0x108)


/************Registers*********************/
/* Enable or disable interrupt */
#define INTENPDM        MMIO32(PDM + 0x300)
// Enable or disable interrupt for event STARTED
#define STARTEDP    0x1
// Enable or disable interrupt for event STOPPED
#define STOPPEDP    0x2
// Enable or disable interrupt for event END
#define ENDP        0x4

/* Enable interrupt */
#define INTENSETPDM     MMIO32(PDM + 0x304)
// Write '1' to enable interrupt for event STARTED
// Write '1' to enable interrupt for event STOPPED
// Write '1' to enable interrupt for event END

/* Disable interrupt */
#define INTENCLRPDM     MMIO32(PDM + 0x308)
// Write '1' to disable interrupt for event STARTED
// Write '1' to disable interrupt for event STOPPED
// Write '1' to disable interrupt for event END

/* PDM module enable register */
#define ENABLEPDM       MMIO32(PDM + 0x500)
// Enable or disable PDM module
#define ENABLE_PDM  0x1

/* PDM clock generator control */
#define PDMCLKCTRL      MMIO32(PDM + 0x504)
// PDM_CLK frequency configuration
#define FREQ1000K       0x08000000
#define FREQ_DEFAULT    0x08400000
#define FREQ1067K       0x08800000
#define FREQ1231K       0x09800000
#define FREQ1280K       0x0a000000
#define FREQ1333K       0x0a800000

/* Defines the routing of the connected PDM microphones' signals */
#define MODEPDM         MMIO32(PDM + 0x508)
// Mono or stereo operation
#define OPERATION   0x1
// Defines on which PDM_CLK edge left (or mono) is sampled
#define EDGE        0x2

/* Left output gain adjustment */
#define GAINL           MMIO32(PDM + 0x518)
// Left output gain adjustment, in 0.5 dB steps, around the default module gain
#define GAIN_MIN        0x00
#define GAIN_DEFAULT    0x28
#define GAIN_MAX        0x50
#define GAIN_MSK        0x7f
#define GAIN_CALC(db)   (((db*2)+GAIN_DEFAULT)&GAIN_MSK)
#define GAINM_CALC(db)  ((db*-2)&GAIN_MSK)

/* Right output gain adjustment */
#define GAINR           MMIO32(PDM + 0x51c)
// Right output gain adjustment, in 0.5 dB steps, around the def module gain

/* Selects the ratio between PDM_CLK and output sample rate. */
#define RATIO           MMIO32(PDM + 0x520)
// Selects the ratio between PDM_CLK and output sample rate
#define RATIO64 0x0
#define RATIO80 0x1

/* Pin number configuration for PDM CLK signal */
#define PSEL_CLK        MMIO32(PDM + 0x540)
// Pin number
#define PINP_MSK    0x1f
// Port number
#define PORTP       0x20
// Connection
#define CONNECTP    0x80000000

/* Pin number configuration for PDM DIN signal */
#define PSEL_DIN        MMIO32(PDM + 0x544)

/* RAM address pointer to write samples to with EasyDMA */
#define SAMPLE_PTR      MMIO32(PDM + 0x560)
// Address to write PDM samples to over DMA

/* Number of samples to allocate memory for in EasyDMA mode */
#define SAMPLE_MAXCNT   MMIO32(PDM + 0x564)
// Length of DMA RAM allocation in number of samples
#define BUFFSIZE_MSK    0x7fff

#endif
