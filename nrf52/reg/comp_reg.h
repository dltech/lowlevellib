#ifndef H_COMP_REG
#define H_COMP_REG
/*
 * Standard library for Nordic NRF52
 * The comparator (COMP) registers.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/*************Tasks************************/
/* Start comparator */
#define STARTCOMP       MMIO32(COMP + 0x000)
/* Stop comparator */
#define STOPCOMP        MMIO32(COMP + 0x004)
/* Sample comparator value */
#define SAMPLECOMP      MMIO32(COMP + 0x008)

/************Events************************/
/* COMP is ready and output is valid */
#define READYCOMP       MMIO32(COMP + 0x100)
/* Downward crossing */
#define DOWNCOMP        MMIO32(COMP + 0x104)
/* Upward crossing */
#define UPCOMP          MMIO32(COMP + 0x108)
/* Downward or upward crossing */
#define CROSSCOMP       MMIO32(COMP + 0x10c)

/************Registers*********************/
/* Shortcuts between local events and tasks */
#define SHORTSCOMP      MMIO32(COMP + 0x200)
// Shortcut between event READY and task SAMPLE
#define READY_SAMPLE    0x01
// Shortcut between event READY and task STOP
#define READY_STOP      0x02
// Shortcut between event DOWN and task STOP
#define DOWN_STOP       0x04
// Shortcut between event UP and task STOP
#define UP_STOP         0x08
// Shortcut between event CROSS and task STOP
#define CROSS_STOP      0x10

/* Enable or disable interrupt */
#define INTENCOMP       MMIO32(COMP + 0x300)
// Enable or disable interrupt for event READY
// Enable or disable interrupt for event DOWN
// Enable or disable interrupt for event UP
// Enable or disable interrupt for event CROSS

/* Enable interrupt */
#define INTENSETCOMP    MMIO32(COMP + 0x304)
// Write '1' to enable interrupt for event READY Enable
// Write '1' to enable interrupt for event DOWN Enable
// Write '1' to enable interrupt for event UP Enable
// Write '1' to enable interrupt for event CROSS Enable

/* Disable interrupt */
#define INTENCLRCOMP    MMIO32(COMP + 0x308)
// Write '1' to disable interrupt for event READY Disable
// Write '1' to disable interrupt for event DOWN Disable
// Write '1' to disable interrupt for event UP Disable
// Write '1' to disable interrupt for event CROSS Disable

/* Compare result */
#define RESULTCOMP      MMIO32(COMP + 0x400)
// Result of last compare. Decision point SAMPLE task.
#define RESULTCOMP_EN   0x1

/* COMP enable */
#define ENABLECOMP      MMIO32(COMP + 0x500)
// Enable or disable COMP
#define ENABLECOMP_EN   0x2

/* Pin select */
#define PSELCOMP        MMIO32(COMP + 0x504)
// Analog pin select
#define PSEL0   0x0
#define PSEL1   0x1
#define PSEL2   0x2
#define PSEL3   0x3
#define PSEL4   0x4
#define PSEL5   0x5
#define PSEL6   0x6
#define PSEL7   0x7

/* Reference source select for single-ended mode */
#define REFSELCOMP      MMIO32(COMP + 0x508)
// Reference select
#define REFSEL1V2   0x0
#define REFSEL1V8   0x1
#define REFSEL2V4   0x2
#define REFSEL_VDD  0x4
#define REFSEL_AREF 0x5

/* External reference select */
#define EXTREFSELCOMP   MMIO32(COMP + 0x50c)
// External analog reference select
#define EXTREFSEL0  0x0
#define EXTREFSEL1  0x1
#define EXTREFSEL2  0x2
#define EXTREFSEL3  0x3
#define EXTREFSEL4  0x4
#define EXTREFSEL5  0x5
#define EXTREFSEL6  0x6
#define EXTREFSEL7  0x7

/* Threshold configuration for hysteresis unit */
#define THCOMP          MMIO32(COMP + 0x530)
// VDOWN = (THDOWN+1)/64*VREF
#define THDOWN_MSK  0x003f
#define THDOWN_GET
#define THDOWN_SET
// VUP = (THUP+1)/64*VREF
#define THUP_MSK    0x3f00
#define THUP_SFT    8
#define THUP_GET    ((THCOMP>>THUP_SFT)&0x3f)
#define THUP_SET(n) (THCOMP + ((n<<THUP_SFT)&THUP_MSK))

/* Mode configuration */
#define MODECOMP        MMIO32(COMP + 0x534)
// Speed and power modes
#define SP_LOW      0x000
#define SP_NORMAL   0x001
#define SP_HIGH     0x002
// Main operation modes
#define MAIN_SE     0x000
#define MAIN_DIFF   0x100


/* Comparator hysteresis enable */
#define HYSTCOMP        MMIO32(COMP + 0x538)
// Comparator hysteresis
#define HYSTCOMP_EN 0x1
