#ifndef H_NFCT_REG
#define H_NFCT_REG
/*
 * Standard library for Nordic NRF52
 * The Memory watch unit (MWU) registers.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/*************Tasks************************/
/* Activate NFCT peripheral for incoming and outgoing frames */
#define ACTIVATENFCT            MMIO32(NFCT + 0x000)
/* Disable NFCT peripheral */
#define DISABLENFCT             MMIO32(NFCT + 0x004)
/* Enable NFC sense field mode, change state to sense mode */
#define SENSENFCT               MMIO32(NFCT + 0x008)
/* Start transmission of an outgoing frame, change state to transmit */
#define STARTTXNFCT             MMIO32(NFCT + 0x00c)
/* Initializes the EasyDMA for receive. */
#define ENABLERXDATANFCT        MMIO32(NFCT + 0x01c)
/* Force state machine to IDLE state */
#define GOIDLENFCT              MMIO32(NFCT + 0x024)
/* Force state machine to SLEEP_A state */
#define GOSLEEPNFCT             MMIO32(NFCT + 0x028)

/************Events************************/
/* The NFCT peripheral is ready to receive and send frames */
#define READYNFCT               MMIO32(NFCT + 0x100)
/* Remote NFC field detected */
#define FIELDDETECTEDNFCT       MMIO32(NFCT + 0x104)
/* Remote NFC field lost */
#define FIELDLOSTNFCT           MMIO32(NFCT + 0x108)
/* Marks the start of the first symbol of a transmitted frame */
#define TXFRAMESTARTNFCT        MMIO32(NFCT + 0x10c)
/* Marks the end of the last transmitted on-air symbol of a frame */
#define TXFRAMEENDNFCT          MMIO32(NFCT + 0x110)
/* Marks the end of the first symbol of a received frame */
#define RXFRAMESTARTNFCT        MMIO32(NFCT + 0x114)
/* Received data has been checked (CRC, parity) and transferred to RAM */
#define RXFRAMEENDNFCT          MMIO32(NFCT + 0x118)
/* NFC error reported. */
#define ERRORNFCT               MMIO32(NFCT + 0x11c)
/* NFC RX frame error reported. */
#define RXERRORNFCT             MMIO32(NFCT + 0x128)
/* RX buffer (as defined by PACKETPTR and MAXLEN) in Data RAM full. */
#define ENDRXNFCT               MMIO32(NFCT + 0x12c)
/* Trans of data in RAM has ended, and EasyDMA has ended acc the TX buffer */
#define ENDTXNFCT               MMIO32(NFCT + 0x130)
/* Auto collision resolution process has started */
#define AUTOCOLRESSTARTEDNFCT   MMIO32(NFCT + 0x138)
/* NFC auto collision resolution error reported. */
#define COLLISIONNFCT           MMIO32(NFCT + 0x148)
/* NFC auto collision resolution successfully completed */
#define SELECTEDNFCT            MMIO32(NFCT + 0x14c)
/* EasyDMA is ready to receive or send frames. */
#define STARTEDNFCT             MMIO32(NFCT + 0x150)

/************Registers*********************/
/* Shortcuts between local events and tasks */
#define SHORTS                  MMIO32(NFCT + 0x200)
// Shortcut between event FIELDDETECTED and task ACTIVATE
#define FIELDDETECTED_ACTIVATE  0x01
// Shortcut between event FIELDLOST and task SENSE
#define FIELDLOST_SENSE         0x02
// Shortcut between event TXFRAMEEND and task ENABLERXDATA
#define TXFRAMEEND_ENABLERXDATA 0x20

/* Enable or disable interrupt */
#define INTENNFCT               MMIO32(NFCT + 0x300)
// Enable or disable interrupt for event READY
#define READYN              0x01
// Enable or disable interrupt for event FIELDDETECTED
#define FIELDDETECTED       0x02
// Enable or disable interrupt for event FIELDLOST
#define FIELDLOST           0x04
// Enable or disable interrupt for event TXFRAMESTART
#define TXFRAMESTART        0x08
// Enable or disable interrupt for event TXFRAMEEND
#define TXFRAMEEND          0x10
// Enable or disable interrupt for event RXFRAMESTART
#define RXFRAMESTART        0x20
// Enable or disable interrupt for event RXFRAMEEND
#define RXFRAMEEND          0x40
// Enable or disable interrupt for event ERROR
#define ERRORN              0x80
// Enable or disable interrupt for event RXERROR
#define RXERRORN            0x000400
// Enable or disable interrupt for event ENDRX
#define ENDRX               0x000800
// Enable or disable interrupt for event ENDTX
#define ENDTX               0x001000
// Enable or disable interrupt for event AUTOCOLRESSTARTED
#define AUTOCOLRESSTARTED   0x004000
// Enable or disable interrupt for event COLLISION
#define COLLISION           0x040000
// Enable or disable interrupt for event SELECTED
#define SELECTED            0x080000
// Enable or disable interrupt for event STARTED
#define STARTED             0x100000

/* Enable interrupt */
#define INTENSETNFCT            MMIO32(NFCT + 0x304)
// Write '1' to enable interrupt for event READY
// Write '1' to enable interrupt for event FIELDDETECTED
// Write '1' to enable interrupt for event FIELDLOST
// Write '1' to enable interrupt for event TXFRAMESTART
// Write '1' to enable interrupt for event TXFRAMEEND
// Write '1' to enable interrupt for event RXFRAMESTART
// Write '1' to enable interrupt for event RXFRAMEEND
// Write '1' to enable interrupt for event ERROR
// Write '1' to enable interrupt for event RXERROR
// Write '1' to enable interrupt for event ENDRX
// Write '1' to enable interrupt for event ENDTX
// Write '1' to enable interrupt for event AUTOCOLRESSTARTED
// Write '1' to enable interrupt for event COLLISION
// Write '1' to enable interrupt for event SELECTED
// Write '1' to enable interrupt for event STARTED

/* Disable interrupt */
#define INTENCLRNFCT            MMIO32(NFCT + 0x308)
// Write '1' to disable interrupt for event READY
// Write '1' to disable interrupt for event FIELDDETECTED
// Write '1' to disable interrupt for event FIELDLOST
// Write '1' to disable interrupt for event TXFRAMESTART
// Write '1' to disable interrupt for event TXFRAMEEND
// Write '1' to disable interrupt for event RXFRAMESTART
// Write '1' to disable interrupt for event RXFRAMEEND
// Write '1' to disable interrupt for event ERROR
// Write '1' to disable interrupt for event RXERROR
// Write '1' to disable interrupt for event ENDRX
// Write '1' to disable interrupt for event ENDTX
// Write '1' to disable interrupt for event AUTOCOLRESSTARTED
// Write '1' to disable interrupt for event COLLISION
// Write '1' to disable interrupt for event SELECTED
// Write '1' to disable interrupt for event STARTED

/* NFC Error Status register */
#define ERRORSTATUS             MMIO32(NFCT + 0x404)
// No STARTTX task triggered before expiration of the time set in FRAMEDELAYMAX
#define FRAMEDELAYTIMEOUT   0x1

/* Result of last incoming frame */
#define FRAMESTATUS_RX          MMIO32(NFCT + 0x40c)
// No valid end of frame (EoF) detected
#define CRCERROR        0x1
// Parity status of received frame
#define PARITYSTATUS    0x4
// Overrun detected
#define OVERRUN         0x8

/* NfcTag state register */
#define NFCTAGSTATE             MMIO32(NFCT + 0x410)
// NfcTag state
#define NFCTAGSTATE_DISABLED    0x0
#define NFCTAGSTATE_RAMPUP      0x2
#define NFCTAGSTATE_IDLE        0x3
#define NFCTAGSTATE_RECEIVE     0x4
#define NFCTAGSTATE_RFAMEDELAY  0x5
#define NFCTAGSTATE_TRANSMIT    0x6

/* Sleep state during automatic collision resolution */
#define SLEEPSTATE              MMIO32(NFCT + 0x420)
// NfcTag state
#define SLEEPA  0x1

/* Indicates the presence or not of a valid field */
#define FIELDPRESENT            MMIO32(NFCT + 0x43c)
// Indicates if a valid field is present. Available only in the activated state.
#define FIELDPRESENT_DET    0x1
// Indicates if the low level has locked to the field
#define LOCKDETECT          0x2

/* Minimum frame delay */
#define FRAMEDELAYMIN           MMIO32(NFCT + 0x504)
// Minimum frame delay in number of 13.56 MHz clocks
#define FRAMEDELAYMIN_MSK   0xffff

/* Maximum frame delay */
#define FRAMEDELAYMAX           MMIO32(NFCT + 0x508)
// Maximum frame delay in number of 13.56 MHz clocks
#define FRAMEDELAYMAX_MSK   0xfffff

/* Configuration register for the Frame Delay Timer */
#define FRAMEDELAYMODE          MMIO32(NFCT + 0x50c)
// Configuration register for the Frame Delay Timer
#define MODE_FREERUN    0x0
#define MODE_WINDOW     0x1
#define MODE_EXACTVAL   0x2
#define MODE_WINDOWGRID 0x3

/* Packet pointer for TXD and RXD data storage in Data RAM */
#define PACKETPTR               MMIO32(NFCT + 0x510)
// Packet pointer for TXD and RXD data storage in Data RAM.

/* Size of the RAM buffer allocated to TXD and RXD data storage each */
#define MAXLEN                  MMIO32(NFCT + 0x514)
// Size of the RAM buffer allocated to TXD and RXD data storage each
#define MAXLEN_MAX  0x101

/* Configuration of outgoing frames */
#define TXD_FRAMECONFIG         MMIO32(NFCT + 0x518)
// Indicates if parity is added to the frame
#define PARITY      0x01
// Discarding unused bits at start or end of a frame
#define DISCARDMODE 0x02
// Adding SoF or not in TX frames
#define SOF         0x04
// CRC mode for outgoing frames
#define CRCMODETX   0x10

/* Size of outgoing frame */
#define TXD_AMOUNT              MMIO32(NFCT + 0x51c)
// Number of bits in the last or first byte read from RAM
#define TXDATABITS_MSK      0x7
#define TXDATABITS_GET      (TXD_AMOUNT&TXDATABITS_MSK)
#define TXDATABITS_SET(x)   (x&TXDATABITS_MSK)
// Number of complete bytes that shall be included in the frame
#define TXDATABYTES_MSK     0xff8
#define TXDATABYTES_SFT     3
#define TXDATABYTES_GET     ((TXD_AMOUNT>>TXDATABYTES_SFT)&0x1ff)
#define TXDATABYTES_SET(x)  ((x<<TXDATABYTES_SFT)&TXDATABYTES_MSK)

/* Configuration of incoming frames */
#define RXD_FRAMECONFIG         MMIO32(NFCT + 0x520)
// Indicates if parity expected in RX frame
#define PARITY      0x01
// SoF expected or not in RX frames
#define SOF         0x04
// CRC mode for incoming frames
#define CRCMODERX   0x10

/* Size of last incoming frame */
#define RXD_AMOUNT              MMIO32(NFCT + 0x524)
// Number of bits in the last or first byte read from RAM
#define RXDATABITS_MSK      0x7
#define RXDATABITS_GET      (RXD_AMOUNT&RXDATABITS_MSK)
#define RXDATABITS_SET(x)   (x&RXDATABITS_MSK)
// Number of complete bytes that shall be included in the frame
#define RXDATABYTES_MSK     0xff8
#define RXDATABYTES_SFT     3
#define RXDATABYTES_GET     ((RXD_AMOUNT>>RXDATABYTES_SFT)&0x1ff)
#define RXDATABYTES_SET(x)  ((x<<RXDATABYTES_SFT)&RXDATABYTES_MSK)

/* Enables the modulation output to a GPIO pin which can be conn to a second */
#define MODULATIONCTRL          MMIO32(NFCT + 0x52c)
// Configuration of modulation control.
#define MODULATION_INALID               0x0
#define MODULATION_INTERNAL             0x1
#define MODULATION_ModToGpio            0x2
#define MODULATION_InternalAndModToGpio 0x3

/* Pin select for Modulation control. */
#define MODULATIONPSEL          MMIO32(NFCT + 0x538)
// Pin number
#define PINN_MSK    0x1f
// Port number
#define PORTN       0x20
// Connection
#define DISCONNECTN 0x80000000

/* Last NFCID1 part (4, 7 or 10 bytes ID) */
#define NFCID1_LAST             MMIO32(NFCT + 0x590)
// NFCID1 byte Z
#define NFCID1_Z_MSK    0x000000ff
#define NFCID1_Z_GET    (NFCID1_LAST&NFCID1_Z_MSK)
#define NFCID1_Z_SET(x) (x&NFCID1_Z_MSK)
// NFCID1 byte Y
#define NFCID1_Y_MSK    0x0000ff00
#define NFCID1_Y_SFT    8
#define NFCID1_Y_GET    ((NFCID1_LAST>>NFCID1_Y_SFT)&0xff)
#define NFCID1_Y_SET(x) ((x<<NFCID1_Y_SFT)&NFCID1_Y_MSK)
// NFCID1 byte X
#define NFCID1_X_MSK    0x00ff0000
#define NFCID1_X_SFT    16
#define NFCID1_X_GET    ((NFCID1_LAST>>NFCID1_X_SFT)&0xff)
#define NFCID1_X_SET(x) ((x<<NFCID1_X_SFT)&NFCID1_X_MSK)
// NFCID1 byte W
#define NFCID1_W_MSK    0xff000000
#define NFCID1_W_SFT    16
#define NFCID1_W_GET    ((NFCID1_LAST>>NFCID1_W_SFT)&0xff)
#define NFCID1_W_SET(x) ((x<<NFCID1_W_SFT)&NFCID1_W_MSK)

/* Second last NFCID1 part (7 or 10 bytes ID) */
#define NFCID1_2ND_LAST         MMIO32(NFCT + 0x594)
// NFCID1 byte V
#define NFCID1_V_MSK    0x0000ff
#define NFCID1_V_GET    (NFCID1_2ND_LAST&NFCID1_V_MSK)
#define NFCID1_V_SET(x) (x&NFCID1_V_MSK)
// NFCID1 byte U
#define NFCID1_U_MSK    0x00ff00
#define NFCID1_U_SFT    8
#define NFCID1_U_GET    ((NFCID1_2ND_LAST>>NFCID1_U_SFT)&0xff)
#define NFCID1_U_SET(x) ((x<<NFCID1_U_SFT)&NFCID1_U_MSK)
// NFCID1 byte T
#define NFCID1_T_MSK    0xff0000
#define NFCID1_T_SFT    16
#define NFCID1_T_GET    ((NFCID1_2ND_LAST>>NFCID1_T_SFT)&0xff)
#define NFCID1_T_SET(x) ((x<<NFCID1_T_SFT)&NFCID1_T_MSK)

/* Third last NFCID1 part (10 bytes ID) */
#define NFCID1_3RD_LAST         MMIO32(NFCT + 0x598)
// NFCID1 byte S
#define NFCID1_S_MSK    0x0000ff
#define NFCID1_S_GET    (NFCID1_3RD_LAST&NFCID1_S_MSK)
#define NFCID1_S_SET(x) (x&NFCID1_S_MSK)
// NFCID1 byte R
#define NFCID1_R_MSK    0x00ff00
#define NFCID1_R_SFT    8
#define NFCID1_R_GET    ((NFCID1_3RD_LAST>>NFCID1_R_SFT)&0xff)
#define NFCID1_R_SET(x) ((x<<NFCID1_R_SFT)&NFCID1_R_MSK)
// NFCID1 byte Q
#define NFCID1_Q_MSK    0xff0000
#define NFCID1_Q_SFT    16
#define NFCID1_Q_GET    ((NFCID1_3RD_LAST>>NFCID1_Q_SFT)&0xff)
#define NFCID1_Q_SET(x) ((x<<NFCID1_Q_SFT)&NFCID1_Q_MSK)

/* Controls the auto collision resolution function. */
#define AUTOCOLRESCONFIG        MMIO32(NFCT + 0x59c)
// Enables/disables auto collision resolution
#define MODE_EN 0x1

/* NFC-A SENS_RES auto-response settings */
#define SENSRES                 MMIO32(NFCT + 0x5a0)
// Bit frame SDD as defined by the b5:b1 of byte 1 in SENS_RES response
#define BITFRAMESDD_MSK     0x001f
#define BITFRAMESDD_GET     (SENSRES&BITFRAMESDD_MSK)
#define BITFRAMESDD_SET(x)  (x&BITFRAMESDD_MSK)
#define BITFRAMESDD00000    0x0000
#define BITFRAMESDD00001    0x0001
#define BITFRAMESDD00010    0x0002
#define BITFRAMESDD00100    0x0004
#define BITFRAMESDD01000    0x0008
#define BITFRAMESDD10000    0x0010
// Reserved for future use. Shall be 0.
#define RFU5                0x0020
// NFCID1 size. This value is used by the auto collision resolution engine.
#define NFCIDSIZE_SINGLE    0x0000
#define NFCIDSIZE_DOUBLE    0x0040
#define NFCIDSIZE_TRIPLE    0x0080
// Tag platform configuration as defined by the b4:b1 of byte 2 in SENS_RES
#define PLATFCONFIG_MSK     0x0f00
#define PLATFCONFIG_SFT     8
#define PLATFCONFIG_GET     ((SENSRES>>PLATFCONFIG_SFT)&0xf)
#define PLATFCONFIG_SET(x)  ((x<<PLATFCONFIG_SFT)&PLATFCONFIG_MSK)
// Reserved for future use. Shall be 0.
#define RFU74_MSK           0xf000
#define RFU74_SFT           12
#define RFU74_GET           ((SENSRES>>RFU74_SFT)&0xf)
#define RFU74_SET(x)        ((x<<RFU74_SFT)&RFU74_MSK)

/* NFC-A SEL_RES auto-response settings */
#define SELRES                  MMIO32(NFCT + 0x5a4)
// Reserved for future use. Shall be 0.
#define RFU10_MSK       0x03
#define RFU10_GET       (SELRES&RFU10_MSK)
#define RFU10_SET(x)    (x&RFU10_MSK)
// Cascade as defined by the b3 of SEL_RES response in the NFC Forum
#define CASCADE         0x04
// Reserved for future use. Shall be 0.
#define RFU43_MSK       0x18
#define RFU43_SFT       3
#define RFU43_GET       ((SELRES>>RFU43_SFT)&0x3)
#define RFU43_SET(x)    ((x<<RFU43_SFT)&RFU43_MSK)
// Protocol as defined by the b7:b6 of SEL_RES response in the NFC Forum
#define PROTOCOL_MSK    0x60
#define PROTOCOL_SFT    5
#define PROTOCOL_GET    ((SELRES>>PROTOCOL_SFT)&0x3)
#define PROTOCOL_SET(x) ((x<<PROTOCOL_SFT)&PROTOCOL_MSK)
// Reserved for future use. Shall be 0.
#define RFU7            0x80

#endif
