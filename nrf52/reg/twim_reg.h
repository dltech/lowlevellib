#ifndef H_TWIM_REG
#define H_TWIM_REG
/*
 * Standard library for Nordic NRF52
 * Register macro for I2C compatible Two Wire Interface master
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/*************Tasks************************/
/* Start TWI receive sequence */
#define STARTRXTWIM0     MMIO32(TWIM0 + 0x000)
#define STARTRXTWIM1     MMIO32(TWIM1 + 0x000)
/* Start TWI transmit sequence */
#define STARTTXTWIM0     MMIO32(TWIM0 + 0x008)
#define STARTTXTWIM1     MMIO32(TWIM1 + 0x008)
/* Stop TWI transaction */
#define STOPTWIM0        MMIO32(TWIM0 + 0x014)
#define STOPTWIM1        MMIO32(TWIM1 + 0x014)
/* Suspend TWI transaction */
#define SUSPENDTWIM0     MMIO32(TWIM0 + 0x01c)
#define SUSPENDTWIM1     MMIO32(TWIM1 + 0x01c)
/* Resume TWI transaction */
#define RESUMETWIM0      MMIO32(TWIM0 + 0x020)
#define RESUMETWIM1      MMIO32(TWIM1 + 0x020)

/************Events************************/
/* TWI stopped */
#define STOPPEDTWIM0     MMIO32(TWIM0 + 0x104)
#define STOPPEDTWIM1     MMIO32(TWIM1 + 0x104)
/* TWI error */
#define ERRORTWIM0       MMIO32(TWIM0 + 0x124)
#define ERRORTWIM1       MMIO32(TWIM1 + 0x124)
/* TWI entered the suspended state */
#define SUSPENDEDTWIM0   MMIO32(TWIM0 + 0x148)
#define SUSPENDEDTWIM1   MMIO32(TWIM1 + 0x148)
/* Receive sequence started */
#define RXSTARTEDTWIM0   MMIO32(TWIM0 + 0x14c)
#define RXSTARTEDTWIM1   MMIO32(TWIM1 + 0x14c)
/* Transmit sequence started */
#define TXSTARTEDTWIM0   MMIO32(TWIM0 + 0x150)
#define TXSTARTEDTWIM1   MMIO32(TWIM1 + 0x150)
/* Byte boundary, starting to receive the last byte */
#define LASTRXTWIM0      MMIO32(TWIM0 + 0x15c)
#define LASTRXTWIM1      MMIO32(TWIM1 + 0x15c)
/* Byte boundary, starting to transmit the last byte */
#define LASTTXTWIM0      MMIO32(TWIM0 + 0x160)
#define LASTTXTWIM1      MMIO32(TWIM1 + 0x160)

//
#define

/************Registers*********************/
/* Shortcut register */
#define SHORTSTWIM0      MMIO32(TWIM0 + 0x200)
#define SHORTSTWIM1      MMIO32(TWIM1 + 0x200)
// Shortcut between event LASTTX and task STARTRX
#define LASTTX_STARTRX  0x0080
// Shortcut between event LASTTX and task SUSPEND
#define LASTTX_SUSPEND  0x0100
// Shortcut between event LASTTX and task STOP
#define LASTTX_STOP     0x0200
// Shortcut between event LASTRX and task STARTTX
#define LASTRX_STARTTX  0x0400
// Shortcut between event LASTRX and task SUSPEND
#define LASTRX_SUSPEND  0x0800
// Shortcut between event LASTRX and task STOP
#define LASTRX_STOP     0x1000

/* Enable or disable interrupt */
#define INTENTWIM0       MMIO32(TWIM0 + 0x300)
#define INTENTWIM1       MMIO32(TWIM1 + 0x300)
// Enable or disable interrupt for event STOPPED
#define STOPPED     0x0000002
// Enable or disable interrupt for event ERROR
#define ERRORT      0x0000200
// Enable or disable interrupt for event SUSPENDED
#define SUSPENDED   0x0040000
// Enable or disable interrupt for event RXSTARTED
#define RXSTARTED   0x0080000
// Enable or disable interrupt for event TXSTARTED
#define TXSTARTED   0x0100000
// Enable or disable interrupt for event LASTRX
#define LASTRX      0x0800000
// Enable or disable interrupt for event LASTTX
#define LASTTX      0x1000000

/* Enable interrupt */
#define INTENSETTWIM0    MMIO32(TWIM0 + 0x304)
#define INTENSETTWIM1    MMIO32(TWIM1 + 0x304)
// Write '1' to enable interrupt for event STOPPED
// Write '1' to enable interrupt for event ERROR
// Write '1' to enable interrupt for event SUSPENDED
// Write '1' to enable interrupt for event RXSTARTED
// Write '1' to enable interrupt for event TXSTARTED
// Write '1' to enable interrupt for event LASTRX
// Write '1' to enable interrupt for event LASTTX

/* Disable interrupt */
#define INTENCLRTWIM0    MMIO32(TWIM0 + 0x308)
#define INTENCLRTWIM1    MMIO32(TWIM1 + 0x308)
// Write '1' to disable interrupt for event STOPPED
// Write '1' to disable interrupt for event ERROR
// Write '1' to disable interrupt for event SUSPENDED
// Write '1' to disable interrupt for event RXSTARTED
// Write '1' to disable interrupt for event TXSTARTED
// Write '1' to disable interrupt for event LASTRX
// Write '1' to disable interrupt for event LASTTX

/* Error source */
#define ERRORSRCTWIM0    MMIO32(TWIM0 + 0x4c4)
#define ERRORSRCTWIM1    MMIO32(TWIM1 + 0x4c4)
// Overrun error
#define OVERRUNT    0x1
// NACK received after sending the address (write '1' to clear)
#define ANACK       0x2
// NACK received after sending a data byte (write '1' to clear)
#define DNACK       0x4

/* Enable TWI */
#define ENABLETWIM0      MMIO32(TWIM0 + 0x500)
#define ENABLETWIM1      MMIO32(TWIM1 + 0x500)
// Enable or disable TWIM
#define ENABLETW    0x6

/* Pin select for SCL */
#define PSELSCLTWIM0     MMIO32(TWIM0 + 0x508)
#define PSELSCLTWIM1     MMIO32(TWIM1 + 0x508)

/* Pin select for SDA */
#define PSELSDATWIM0     MMIO32(TWIM0 + 0x50c)
#define PSELSDATWIM1     MMIO32(TWIM1 + 0x50c)

/* TWI frequency */
#define FREQUENCYTWIM0   MMIO32(TWIM0 + 0x524)
#define FREQUENCYTWIM1   MMIO32(TWIM1 + 0x524)
// TWI master clock frequency
#define FREQUENCY100K   0x01980000
#define FREQUENCY250K   0x04000000
#define FREQUENCY400K   0x06400000

/* Data pointer */
#define RXD_PTRTWIM0     MMIO32(TWIM0 + 0x534)
#define RXD_PTRTWIM1     MMIO32(TWIM1 + 0x534)
// PTR[31:0]

/* Maximum number of bytes in receive buffer */
#define RXD_MAXCNTTWIM0  MMIO32(TWIM0 + 0x538)
#define RXD_MAXCNTTWIM1  MMIO32(TWIM1 + 0x538)
// MAXCNT[15:0]

/* Number of bytes transferred in the last transaction */
#define RXD_AMOUNTTWIM0  MMIO32(TWIM0 + 0x53c)
#define RXD_AMOUNTTWIM1  MMIO32(TWIM1 + 0x53c)
// In case of NACK error, includes the NACK'ed byte. AMOUNT[15:0]

/* EasyDMA list type */
#define RXD_LISTTWIM0    MMIO32(TWIM0 + 0x540)
#define RXD_LISTTWIM1    MMIO32(TWIM1 + 0x540)
// List type
#define DISABLED    0x0
#define ARRAYLIST   0x1

/* Data pointer */
#define TXD_PTRTWIM0     MMIO32(TWIM0 + 0x544)
#define TXD_PTRTWIM1     MMIO32(TWIM1 + 0x544)
// PTR[31:0]

/* Maximum number of bytes in transmit buffer */
#define TXD_MAXCNTTWIM0  MMIO32(TWIM0 + 0x548)
#define TXD_MAXCNTTWIM1  MMIO32(TWIM1 + 0x548)
// MAXCNT[15:0]

/* Number of bytes transferred in the last transaction */
#define TXD_AMOUNTTWIM0  MMIO32(TWIM0 + 0x54c)
#define TXD_AMOUNTTWIM1  MMIO32(TWIM1 + 0x54c)
// In case of NACK error, includes the NACK'ed byte. AMOUNT[15:0]

/* EasyDMA list type */
#define TXD_LISTTWIM0    MMIO32(TWIM0 + 0x550)
#define TXD_LISTTWIM1    MMIO32(TWIM1 + 0x550)
// List type

/* Address used in the TWI transfer */
#define ADDRESSTWIM0     MMIO32(TWIM0 + 0x588)
#define ADDRESSTWIM1     MMIO32(TWIM1 + 0x588)
// Address used in the TWI transfer. ADDRESS[6:0]

#endif
