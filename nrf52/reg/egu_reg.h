#ifndef H_CLOCK_REG
#define H_CLOCK_REG
/*
 * Standard library for Nordic NRF52
 * Event generator unit (EGU) registers.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/*************Tasks************************/
/* Trigger x for triggering the corresponding TRIGGERED[x] event */
#define TRIGGER0EGU0        MMIO32(EGU0 + 0x000)
#define TRIGGER0EGU1        MMIO32(EGU1 + 0x000)
#define TRIGGER0EGU2        MMIO32(EGU2 + 0x000)
#define TRIGGER0EGU3        MMIO32(EGU3 + 0x000)
#define TRIGGER0EGU4        MMIO32(EGU4 + 0x000)
#define TRIGGER0EGU5        MMIO32(EGU5 + 0x000)
#define TRIGGER1EGU0        MMIO32(EGU0 + 0x004)
#define TRIGGER1EGU1        MMIO32(EGU1 + 0x004)
#define TRIGGER1EGU2        MMIO32(EGU2 + 0x004)
#define TRIGGER1EGU3        MMIO32(EGU3 + 0x004)
#define TRIGGER1EGU4        MMIO32(EGU4 + 0x004)
#define TRIGGER1EGU5        MMIO32(EGU5 + 0x004)
#define TRIGGER2EGU0        MMIO32(EGU0 + 0x008)
#define TRIGGER2EGU1        MMIO32(EGU1 + 0x008)
#define TRIGGER2EGU2        MMIO32(EGU2 + 0x008)
#define TRIGGER2EGU3        MMIO32(EGU3 + 0x008)
#define TRIGGER2EGU4        MMIO32(EGU4 + 0x008)
#define TRIGGER2EGU5        MMIO32(EGU5 + 0x008)
#define TRIGGER3EGU0        MMIO32(EGU0 + 0x00c)
#define TRIGGER3EGU1        MMIO32(EGU1 + 0x00c)
#define TRIGGER3EGU2        MMIO32(EGU2 + 0x00c)
#define TRIGGER3EGU3        MMIO32(EGU3 + 0x00c)
#define TRIGGER3EGU4        MMIO32(EGU4 + 0x00c)
#define TRIGGER3EGU5        MMIO32(EGU5 + 0x00c)
#define TRIGGER4EGU0        MMIO32(EGU0 + 0x010)
#define TRIGGER4EGU1        MMIO32(EGU1 + 0x010)
#define TRIGGER4EGU2        MMIO32(EGU2 + 0x010)
#define TRIGGER4EGU3        MMIO32(EGU3 + 0x010)
#define TRIGGER4EGU4        MMIO32(EGU4 + 0x010)
#define TRIGGER4EGU5        MMIO32(EGU5 + 0x010)
#define TRIGGER5EGU0        MMIO32(EGU0 + 0x014)
#define TRIGGER5EGU1        MMIO32(EGU1 + 0x014)
#define TRIGGER5EGU2        MMIO32(EGU2 + 0x014)
#define TRIGGER5EGU3        MMIO32(EGU3 + 0x014)
#define TRIGGER5EGU4        MMIO32(EGU4 + 0x014)
#define TRIGGER5EGU5        MMIO32(EGU5 + 0x014)
#define TRIGGER6EGU0        MMIO32(EGU0 + 0x018)
#define TRIGGER6EGU1        MMIO32(EGU1 + 0x018)
#define TRIGGER6EGU2        MMIO32(EGU2 + 0x018)
#define TRIGGER6EGU3        MMIO32(EGU3 + 0x018)
#define TRIGGER6EGU4        MMIO32(EGU4 + 0x018)
#define TRIGGER6EGU5        MMIO32(EGU5 + 0x018)
#define TRIGGER7EGU0        MMIO32(EGU0 + 0x01c)
#define TRIGGER7EGU1        MMIO32(EGU1 + 0x01c)
#define TRIGGER7EGU2        MMIO32(EGU2 + 0x01c)
#define TRIGGER7EGU3        MMIO32(EGU3 + 0x01c)
#define TRIGGER7EGU4        MMIO32(EGU4 + 0x01c)
#define TRIGGER7EGU5        MMIO32(EGU5 + 0x01c)
#define TRIGGER8EGU0        MMIO32(EGU0 + 0x020)
#define TRIGGER8EGU1        MMIO32(EGU1 + 0x020)
#define TRIGGER8EGU2        MMIO32(EGU2 + 0x020)
#define TRIGGER8EGU3        MMIO32(EGU3 + 0x020)
#define TRIGGER8EGU4        MMIO32(EGU4 + 0x020)
#define TRIGGER8EGU5        MMIO32(EGU5 + 0x020)
#define TRIGGER9EGU0        MMIO32(EGU0 + 0x024)
#define TRIGGER9EGU1        MMIO32(EGU1 + 0x024)
#define TRIGGER9EGU2        MMIO32(EGU2 + 0x024)
#define TRIGGER9EGU3        MMIO32(EGU3 + 0x024)
#define TRIGGER9EGU4        MMIO32(EGU4 + 0x024)
#define TRIGGER9EGU5        MMIO32(EGU5 + 0x024)
#define TRIGGER10EGU0       MMIO32(EGU0 + 0x028)
#define TRIGGER10EGU1       MMIO32(EGU1 + 0x028)
#define TRIGGER10EGU2       MMIO32(EGU2 + 0x028)
#define TRIGGER10EGU3       MMIO32(EGU3 + 0x028)
#define TRIGGER10EGU4       MMIO32(EGU4 + 0x028)
#define TRIGGER10EGU5       MMIO32(EGU5 + 0x028)
#define TRIGGER11EGU0       MMIO32(EGU0 + 0x02c)
#define TRIGGER11EGU1       MMIO32(EGU1 + 0x02c)
#define TRIGGER11EGU2       MMIO32(EGU2 + 0x02c)
#define TRIGGER11EGU3       MMIO32(EGU3 + 0x02c)
#define TRIGGER11EGU4       MMIO32(EGU4 + 0x02c)
#define TRIGGER11EGU5       MMIO32(EGU5 + 0x02c)
#define TRIGGER12EGU0       MMIO32(EGU0 + 0x030)
#define TRIGGER12EGU1       MMIO32(EGU1 + 0x030)
#define TRIGGER12EGU2       MMIO32(EGU2 + 0x030)
#define TRIGGER12EGU3       MMIO32(EGU3 + 0x030)
#define TRIGGER12EGU4       MMIO32(EGU4 + 0x030)
#define TRIGGER12EGU5       MMIO32(EGU5 + 0x030)
#define TRIGGER13EGU0       MMIO32(EGU0 + 0x034)
#define TRIGGER13EGU1       MMIO32(EGU1 + 0x034)
#define TRIGGER13EGU2       MMIO32(EGU2 + 0x034)
#define TRIGGER13EGU3       MMIO32(EGU3 + 0x034)
#define TRIGGER13EGU4       MMIO32(EGU4 + 0x034)
#define TRIGGER13EGU5       MMIO32(EGU5 + 0x034)
#define TRIGGER14EGU0       MMIO32(EGU0 + 0x038)
#define TRIGGER14EGU1       MMIO32(EGU1 + 0x038)
#define TRIGGER14EGU2       MMIO32(EGU2 + 0x038)
#define TRIGGER14EGU3       MMIO32(EGU3 + 0x038)
#define TRIGGER14EGU4       MMIO32(EGU4 + 0x038)
#define TRIGGER14EGU5       MMIO32(EGU5 + 0x038)
#define TRIGGER15EGU0       MMIO32(EGU0 + 0x03c)
#define TRIGGER15EGU1       MMIO32(EGU1 + 0x03c)
#define TRIGGER15EGU2       MMIO32(EGU2 + 0x03c)
#define TRIGGER15EGU3       MMIO32(EGU3 + 0x03c)
#define TRIGGER15EGU4       MMIO32(EGU4 + 0x03c)
#define TRIGGER15EGU5       MMIO32(EGU5 + 0x03c)
#define TRIGGEREGU0(n)      MMIO32(EGU0 + (0x4*n))
#define TRIGGEREGU1(n)      MMIO32(EGU1 + (0x4*n))
#define TRIGGEREGU2(n)      MMIO32(EGU2 + (0x4*n))
#define TRIGGEREGU3(n)      MMIO32(EGU3 + (0x4*n))
#define TRIGGEREGU4(n)      MMIO32(EGU4 + (0x4*n))
#define TRIGGEREGU5(n)      MMIO32(EGU5 + (0x4*n))

/************Events************************/
/* Event number x generated by triggering the corresponding TRIGGER[x] task */
#define TRIGGERED0EGU0      MMIO32(EGU0 + 0x100)
#define TRIGGERED0EGU1      MMIO32(EGU1 + 0x100)
#define TRIGGERED0EGU2      MMIO32(EGU2 + 0x100)
#define TRIGGERED0EGU3      MMIO32(EGU3 + 0x100)
#define TRIGGERED0EGU4      MMIO32(EGU4 + 0x100)
#define TRIGGERED0EGU5      MMIO32(EGU5 + 0x100)
#define TRIGGERED1EGU0      MMIO32(EGU0 + 0x104)
#define TRIGGERED1EGU1      MMIO32(EGU1 + 0x104)
#define TRIGGERED1EGU2      MMIO32(EGU2 + 0x104)
#define TRIGGERED1EGU3      MMIO32(EGU3 + 0x104)
#define TRIGGERED1EGU4      MMIO32(EGU4 + 0x104)
#define TRIGGERED1EGU5      MMIO32(EGU5 + 0x104)
#define TRIGGERED2EGU0      MMIO32(EGU0 + 0x108)
#define TRIGGERED2EGU1      MMIO32(EGU1 + 0x108)
#define TRIGGERED2EGU2      MMIO32(EGU2 + 0x108)
#define TRIGGERED2EGU3      MMIO32(EGU3 + 0x108)
#define TRIGGERED2EGU4      MMIO32(EGU4 + 0x108)
#define TRIGGERED2EGU5      MMIO32(EGU5 + 0x108)
#define TRIGGERED3EGU0      MMIO32(EGU0 + 0x10c)
#define TRIGGERED3EGU1      MMIO32(EGU1 + 0x10c)
#define TRIGGERED3EGU2      MMIO32(EGU2 + 0x10c)
#define TRIGGERED3EGU3      MMIO32(EGU3 + 0x10c)
#define TRIGGERED3EGU4      MMIO32(EGU4 + 0x10c)
#define TRIGGERED3EGU5      MMIO32(EGU5 + 0x10c)
#define TRIGGERED4EGU0      MMIO32(EGU0 + 0x110)
#define TRIGGERED4EGU1      MMIO32(EGU1 + 0x110)
#define TRIGGERED4EGU2      MMIO32(EGU2 + 0x110)
#define TRIGGERED4EGU3      MMIO32(EGU3 + 0x110)
#define TRIGGERED4EGU4      MMIO32(EGU4 + 0x110)
#define TRIGGERED4EGU5      MMIO32(EGU5 + 0x110)
#define TRIGGERED5EGU0      MMIO32(EGU0 + 0x114)
#define TRIGGERED5EGU1      MMIO32(EGU1 + 0x114)
#define TRIGGERED5EGU2      MMIO32(EGU2 + 0x114)
#define TRIGGERED5EGU3      MMIO32(EGU3 + 0x114)
#define TRIGGERED5EGU4      MMIO32(EGU4 + 0x114)
#define TRIGGERED5EGU5      MMIO32(EGU5 + 0x114)
#define TRIGGERED6EGU0      MMIO32(EGU0 + 0x118)
#define TRIGGERED6EGU1      MMIO32(EGU1 + 0x118)
#define TRIGGERED6EGU2      MMIO32(EGU2 + 0x118)
#define TRIGGERED6EGU3      MMIO32(EGU3 + 0x118)
#define TRIGGERED6EGU4      MMIO32(EGU4 + 0x118)
#define TRIGGERED6EGU5      MMIO32(EGU5 + 0x118)
#define TRIGGERED7EGU0      MMIO32(EGU0 + 0x11c)
#define TRIGGERED7EGU1      MMIO32(EGU1 + 0x11c)
#define TRIGGERED7EGU2      MMIO32(EGU2 + 0x11c)
#define TRIGGERED7EGU3      MMIO32(EGU3 + 0x11c)
#define TRIGGERED7EGU4      MMIO32(EGU4 + 0x11c)
#define TRIGGERED7EGU5      MMIO32(EGU5 + 0x11c)
#define TRIGGERED8EGU0      MMIO32(EGU0 + 0x120)
#define TRIGGERED8EGU1      MMIO32(EGU1 + 0x120)
#define TRIGGERED8EGU2      MMIO32(EGU2 + 0x120)
#define TRIGGERED8EGU3      MMIO32(EGU3 + 0x120)
#define TRIGGERED8EGU4      MMIO32(EGU4 + 0x120)
#define TRIGGERED8EGU5      MMIO32(EGU5 + 0x120)
#define TRIGGERED9EGU0      MMIO32(EGU0 + 0x124)
#define TRIGGERED9EGU1      MMIO32(EGU1 + 0x124)
#define TRIGGERED9EGU2      MMIO32(EGU2 + 0x124)
#define TRIGGERED9EGU3      MMIO32(EGU3 + 0x124)
#define TRIGGERED9EGU4      MMIO32(EGU4 + 0x124)
#define TRIGGERED9EGU5      MMIO32(EGU5 + 0x124)
#define TRIGGERED10EGU0     MMIO32(EGU0 + 0x128)
#define TRIGGERED10EGU1     MMIO32(EGU1 + 0x128)
#define TRIGGERED10EGU2     MMIO32(EGU2 + 0x128)
#define TRIGGERED10EGU3     MMIO32(EGU3 + 0x128)
#define TRIGGERED10EGU4     MMIO32(EGU4 + 0x128)
#define TRIGGERED10EGU5     MMIO32(EGU5 + 0x128)
#define TRIGGERED11EGU0     MMIO32(EGU0 + 0x12c)
#define TRIGGERED11EGU1     MMIO32(EGU1 + 0x12c)
#define TRIGGERED11EGU2     MMIO32(EGU2 + 0x12c)
#define TRIGGERED11EGU3     MMIO32(EGU3 + 0x12c)
#define TRIGGERED11EGU4     MMIO32(EGU4 + 0x12c)
#define TRIGGERED11EGU5     MMIO32(EGU5 + 0x12c)
#define TRIGGERED12EGU0     MMIO32(EGU0 + 0x130)
#define TRIGGERED12EGU1     MMIO32(EGU1 + 0x130)
#define TRIGGERED12EGU2     MMIO32(EGU2 + 0x130)
#define TRIGGERED12EGU3     MMIO32(EGU3 + 0x130)
#define TRIGGERED12EGU4     MMIO32(EGU4 + 0x130)
#define TRIGGERED12EGU5     MMIO32(EGU5 + 0x130)
#define TRIGGERED13EGU0     MMIO32(EGU0 + 0x134)
#define TRIGGERED13EGU1     MMIO32(EGU1 + 0x134)
#define TRIGGERED13EGU2     MMIO32(EGU2 + 0x134)
#define TRIGGERED13EGU3     MMIO32(EGU3 + 0x134)
#define TRIGGERED13EGU4     MMIO32(EGU4 + 0x134)
#define TRIGGERED13EGU5     MMIO32(EGU5 + 0x134)
#define TRIGGERED14EGU0     MMIO32(EGU0 + 0x138)
#define TRIGGERED14EGU1     MMIO32(EGU1 + 0x138)
#define TRIGGERED14EGU2     MMIO32(EGU2 + 0x138)
#define TRIGGERED14EGU3     MMIO32(EGU3 + 0x138)
#define TRIGGERED14EGU4     MMIO32(EGU4 + 0x138)
#define TRIGGERED14EGU5     MMIO32(EGU5 + 0x138)
#define TRIGGERED15EGU0     MMIO32(EGU0 + 0x13c)
#define TRIGGERED15EGU1     MMIO32(EGU1 + 0x13c)
#define TRIGGERED15EGU2     MMIO32(EGU2 + 0x13c)
#define TRIGGERED15EGU3     MMIO32(EGU3 + 0x13c)
#define TRIGGERED15EGU4     MMIO32(EGU4 + 0x13c)
#define TRIGGERED15EGU5     MMIO32(EGU5 + 0x13c)
#define TRIGGEREDEGU0(n)    MMIO32(EGU0 + 0x100 + (n*0x4))
#define TRIGGEREDEGU1(n)    MMIO32(EGU1 + 0x100 + (n*0x4))
#define TRIGGEREDEGU2(n)    MMIO32(EGU2 + 0x100 + (n*0x4))
#define TRIGGEREDEGU3(n)    MMIO32(EGU3 + 0x100 + (n*0x4))
#define TRIGGEREDEGU4(n)    MMIO32(EGU4 + 0x100 + (n*0x4))
#define TRIGGEREDEGU5(n)    MMIO32(EGU5 + 0x100 + (n*0x4))

/************Registers*********************/
/* Enable or disable interrupt */
#define INTENEGU0           MMIO32(EGU0 + 0x300)
#define INTENEGU1           MMIO32(EGU1 + 0x300)
#define INTENEGU2           MMIO32(EGU2 + 0x300)
#define INTENEGU3           MMIO32(EGU3 + 0x300)
#define INTENEGU4           MMIO32(EGU4 + 0x300)
#define INTENEGU5           MMIO32(EGU5 + 0x300)
// Enable or disable interrupt for event TRIGGERED[i]
#define TRIGGERED(n)    (1<<n)
#define TRIGGERED0      0x0001
#define TRIGGERED1      0x0002
#define TRIGGERED2      0x0004
#define TRIGGERED3      0x0008
#define TRIGGERED4      0x0010
#define TRIGGERED5      0x0020
#define TRIGGERED6      0x0040
#define TRIGGERED7      0x0080
#define TRIGGERED8      0x0100
#define TRIGGERED9      0x0200
#define TRIGGERED10     0x0400
#define TRIGGERED11     0x0800
#define TRIGGERED12     0x1000
#define TRIGGERED13     0x2000
#define TRIGGERED14     0x4000
#define TRIGGERED15     0x8000

/* Enable interrupt */
#define INTENSETEGU0        MMIO32(EGU0 + 0x304)
#define INTENSETEGU1        MMIO32(EGU1 + 0x304)
#define INTENSETEGU2        MMIO32(EGU2 + 0x304)
#define INTENSETEGU3        MMIO32(EGU3 + 0x304)
#define INTENSETEGU4        MMIO32(EGU4 + 0x304)
#define INTENSETEGU5        MMIO32(EGU5 + 0x304)
// Write '1' to enable interrupt for event TRIGGERED[i]

/* Disable interrupt */
#define INTENCLREGU0        MMIO32(EGU0 + 0x308)
#define INTENCLREGU1        MMIO32(EGU1 + 0x308)
#define INTENCLREGU2        MMIO32(EGU2 + 0x308)
#define INTENCLREGU3        MMIO32(EGU3 + 0x308)
#define INTENCLREGU4        MMIO32(EGU4 + 0x308)
#define INTENCLREGU5        MMIO32(EGU5 + 0x308)
// Write '1' to disable interrupt for event TRIGGERED[i]

#endif
